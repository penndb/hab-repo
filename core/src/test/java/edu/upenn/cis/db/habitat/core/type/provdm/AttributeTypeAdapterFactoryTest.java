/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.provdm;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import edu.upenn.cis.db.habitat.test.TestUtil;

/**
 * @author John Frommeyer
 *
 */
public class AttributeTypeAdapterFactoryTest {

	@Test
	public void roundTrip() {

		final Gson gson = new GsonBuilder()
				.registerTypeAdapterFactory(new AttributeTypeAdapterFactory())
				.create();

		final List<Attribute> expectedAttributes = TestUtil
				.newTestAttributeList();
		for (final Attribute expectedAttribute : expectedAttributes) {
			final String json = gson.toJson(expectedAttribute);

			final Attribute deserialized = gson.fromJson(json, Attribute.class);
			assertEquals(expectedAttribute.getType(), deserialized.getType());
			assertEquals(expectedAttribute.getName(), deserialized.getName());
			assertEquals(expectedAttribute.getValue(), deserialized.getValue());
		}
	}

}
