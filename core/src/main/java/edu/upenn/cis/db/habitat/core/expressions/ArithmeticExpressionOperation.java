package edu.upenn.cis.db.habitat.core.expressions;

import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;

public interface ArithmeticExpressionOperation<T> {

	public static enum ArithmeticOperation {Add, Subtract, Multiply, Divide, Negate, Eval, Cast, Literal, Variable};
	
	public static ArithmeticOperation getComparison(String compName) {
		if (compName.toLowerCase().equals("add") || compName.toLowerCase().equals("+")) {
			return ArithmeticOperation.Add;
		} else if (compName.toLowerCase().equals("subtract") || compName.toLowerCase().equals("-")) {
			return ArithmeticOperation.Subtract;
		} else if (compName.toLowerCase().equals("multiply") || compName.toLowerCase().equals("*")) {
			return ArithmeticOperation.Multiply;
		} else if (compName.toLowerCase().equals("divide") || compName.toLowerCase().equals("/")) {
			return ArithmeticOperation.Divide;
		} else if (compName.toLowerCase().equals("negate") || compName.toLowerCase().equals("- ")) {
			return ArithmeticOperation.Negate;
		} else if (compName.toLowerCase().equals("eval")) {
			return ArithmeticOperation.Eval;
		} else if (compName.toLowerCase().equals("cast")) {
			return ArithmeticOperation.Cast;
		} else if (compName.toLowerCase().equals("literal")) {
			return ArithmeticOperation.Literal;
		} else if (compName.toLowerCase().equals("variable")) {
			return ArithmeticOperation.Variable;
		}
		throw new UnsupportedOperationException(compName);
	}
	
	public static String getStandardName(ArithmeticOperation op) {
		switch (op) {
		case Add:
			return "+";
		case Subtract:
			return "-";
		case Multiply:
			return "*";
		case Divide:
			return "/";
		case Negate:
			return "- ";
		case Eval:
			return "eval";
		case Cast:
			return "cast";
		case Literal:
			return "literal";
		case Variable:
			return "variable";
		}
		throw new UnsupportedOperationException(op.name());
	}

	/**
	 * Bind the predicate to this tuple
	 * 
	 * @param tuple
	 */
	public void bind(TupleWithSchema<String> tuple);
	
	/**
	 * Get a computed result
	 * 
	 * @param fieldName
	 * @return
	 */
	public T getValue();

	public List<ArithmeticExpressionOperation<?>> getChildren();
	
	public Class<T> getType();
	
	public ArithmeticOperation getOp();

	public Set<String> getSymbolsReferenced();
	
	public void rename(Map<String,String> map);
}
