/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.core.expressions;

import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;

public abstract class UnaryTupleExtractorWithLookup<T,S> implements UnaryTupleExtractor<T,S> {
	TupleWithSchema<String> tuple = null;

	@Override
	public void bind(TupleWithSchema<String> tuple) {
		this.tuple = tuple;
	}

	@Override
	public S getValue(String fieldName) {
		return (S) tuple.getValue(fieldName);
	}

}
