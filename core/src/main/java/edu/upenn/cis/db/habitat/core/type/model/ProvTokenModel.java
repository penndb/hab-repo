/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.model;

import static com.google.common.base.Preconditions.checkNotNull;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonCreator;

import edu.upenn.cis.db.habitat.core.type.ProvIntToken;
import edu.upenn.cis.db.habitat.core.type.ProvStringToken;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.ProvUniqueToken;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvQualifiedNameToken;
import edu.upenn.cis.db.habitat.core.type.provdm.QualifiedName;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * @author John Frommeyer
 *
 */
@ApiModel(description = "A prov token expression",
		parent = ProvSpecifierModel.class)
public final class ProvTokenModel implements ProvSpecifierModel {

	private static final Pattern intPattern = Pattern.compile("\\p{Digit}+");
	private static final Pattern uuidPattern = Pattern
			.compile("\\p{XDigit}{8}+-\\p{XDigit}{4}+-\\p{XDigit}{4}+-\\p{XDigit}{4}+-\\p{XDigit}{12}+");

	public static final String TOKEN_VALUE_PROP_NAME = "tokenValue";

	@ApiModelProperty(name = TOKEN_VALUE_PROP_NAME,
			required = true)
	public final String tokenValue;

	@JsonCreator
	public ProvTokenModel(String tokenValue) {
		this.tokenValue = checkNotNull(tokenValue);
	}

	@Override
	public ProvToken toProvSpecifier() throws HabitatServiceException {
		return toProvToken(tokenValue);
	}

	public static ProvToken toProvToken(String value)
			throws HabitatServiceException {
		final Matcher intMatcher = intPattern.matcher(value);
		if (intMatcher.matches()) {
			return new ProvIntToken(Integer.valueOf(value));
		}
		final Matcher uuidMatcher = uuidPattern.matcher(value);
		if (uuidMatcher.matches()) {
			final ProvUniqueToken uniqueToken = new ProvUniqueToken();
			uniqueToken.setUuid(UUID.fromString(value));
			return uniqueToken;
		}
		if (value.startsWith("{")) {
			final int namespaceDelimiterPos = value.indexOf("}");
			if (namespaceDelimiterPos > 0
					&& namespaceDelimiterPos < value.length()) {
				final QualifiedName qualifiedName = QualifiedName.from(value);
				return new ProvQualifiedNameToken(qualifiedName);
			}
		}
		return new ProvStringToken(value);
	}

	public static ProvTokenModel from(ProvToken provToken) {
		checkNotNull(provToken);
		if (provToken instanceof ProvIntToken) {
			final ProvIntToken intToken = (ProvIntToken) provToken;
			return new ProvTokenModel(intToken.getInt().toString());
		}

		if (provToken instanceof ProvStringToken) {
			final ProvStringToken stringToken = (ProvStringToken) provToken;
			return new ProvTokenModel(stringToken.getString());
		}

		if (provToken instanceof ProvUniqueToken) {
			final ProvUniqueToken uniqueToken = (ProvUniqueToken) provToken;
			return new ProvTokenModel(uniqueToken.getUuid().toString());
		}

		if (provToken instanceof ProvQualifiedNameToken) {
			final ProvQualifiedNameToken qnToken = (ProvQualifiedNameToken) provToken;
			return new ProvTokenModel(qnToken.getValue().toString());
		}

		throw new IllegalArgumentException("unknown ProvToken type: ["
				+ provToken.getClass() + "]");
	}

	public static List<ProvTokenModel> from(List<ProvToken> provTokens) {
		return provTokens
				.stream()
				.map(ProvTokenModel::from)
				.collect(Collectors.toList());
	}

	@Override
	public String toString() {
		return tokenValue;
	}
}
