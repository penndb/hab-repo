/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.model;

import static com.google.common.base.Preconditions.checkNotNull;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Set;

import edu.upenn.cis.db.habitat.core.type.BooleanField;
import edu.upenn.cis.db.habitat.core.type.DoubleField;
import edu.upenn.cis.db.habitat.core.type.Field;
import edu.upenn.cis.db.habitat.core.type.IntField;
import edu.upenn.cis.db.habitat.core.type.LongField;
import edu.upenn.cis.db.habitat.core.type.MultiField;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifierField;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.ProvTokenField;
import edu.upenn.cis.db.habitat.core.type.StringField;
import edu.upenn.cis.db.habitat.core.type.provdm.QualifiedName;
import edu.upenn.cis.db.habitat.core.type.provdm.QualifiedNameField;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * @author John Frommeyer
 *
 */
@ApiModel(description = "A field in a TupleModel",
		discriminator = FieldModel.TYPE_PROP_NAME,
		subTypes = { BooleanFieldModel.class, DoubleFieldModel.class,
				IntegerFieldModel.class, StringFieldModel.class,
				ProvTokenFieldModel.class, ProvSpecifierFieldModel.class,
				LongFieldModel.class, QualifiedNameFieldModel.class,
				MultiFieldModel.class })
public interface FieldModel {

	public static enum Type {
		BOOLEAN(Boolean.class),
		DOUBLE(Double.class),
		INTEGER(Integer.class),
		STRING(String.class),
		PROV_TOKEN(ProvToken.class),
		PROV_SPECIFIER(ProvSpecifier.class),
		LONG(Long.class),
		QUALIFIED_NAME(QualifiedName.class),
		MULTI(Set.class);

		public final Class<?> javaType;

		private Type(Class<?> javaType) {
			this.javaType = checkNotNull(javaType);
		}
	}

	public static final String TYPE_PROP_NAME = "type";

	@ApiModelProperty(required = true)
	String getName();

	@ApiModelProperty(required = true, name = TYPE_PROP_NAME)
	Type getType();

	@ApiModelProperty(required = true)
	Object getValue();

	Field<?> toField() throws HabitatServiceException;

	public static FieldModel from(String name, Field<?> field) {
		if (field instanceof BooleanField) {
			return BooleanFieldModel.from(name, (BooleanField) field);
		}
		if (field instanceof DoubleField) {
			return DoubleFieldModel.from(name, (DoubleField) field);
		}
		if (field instanceof IntField) {
			return IntegerFieldModel.from(name, (IntField) field);
		}
		if (field instanceof LongField) {
			return LongFieldModel.from(name, (LongField) field);
		}
		if (field instanceof StringField) {
			return StringFieldModel.from(name, (StringField) field);
		}
		if (field instanceof ProvTokenField) {
			return ProvTokenFieldModel.from(name, (ProvTokenField) field);
		}
		if (field instanceof ProvSpecifierField) {
			return ProvSpecifierFieldModel.from(name,
					(ProvSpecifierField) field);
		}
		if (field instanceof QualifiedNameField) {
			return QualifiedNameFieldModel.from(name,
					(QualifiedNameField) field);
		}
		if (field instanceof MultiField) {
			return MultiFieldModel.from(name, (MultiField) field);
		}

		throw new IllegalArgumentException(
				"cannot convert unknown Field type: " + field.getClass());
	}
}
