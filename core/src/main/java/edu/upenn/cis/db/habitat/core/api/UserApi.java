/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.api;

import java.sql.SQLException;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * Basic API for user credential storage, retrieval.  Does not prescribe (1) whether
 * the implementation is local or remote, or (2) whether the implementation tests for
 * permissions on every operation.
 * 
 * @author zives
 *
 */
public interface UserApi {
	public static final String Root = "(root)";
	
	/**
	 * Given an existing user account, add information about a credential they may have,
	 * either in the local system or in an external federated account. 
	 * 
	 * @param username The unique username of the individual
	 * @param service The external service (eg Google, GitHub) or "local"
	 * @param typeOrProtocol Optional descriptor of protocol type, eg, HTTP
	 * @param passwordOrToken The password/token we should store for this system
	 * @return
	 * @throws HabitatServiceException
	 */
	public boolean addCredential(
			String username, 
			String service, 
			String typeOrProtocol, 
			String passwordOrToken) throws HabitatServiceException;
	
	/**
	 * Create a group within an Organization
	 * 
	 * @param group
	 * @return
	 */
	public int addGroup(
			int orgId, 
			String name, 
			String parentGroup) throws HabitatServiceException;
	
	/**
	 * Register a new organization
	 * 
	 * @param org
	 * @returns new org ID
	 */
	public int addOrganization(
			String name, 
			String street1, 
			String street2,
			String city, 
			String state, 
			String zip, 
			String country, 
			String phone, 
			String web)
					 throws HabitatServiceException;
	
	/**
	 * Adds a subgroup to a group.
	 * -> Currently the group must have a single parent, so this will replace an existing parent <-
	 * 
	 * @param username
	 * @param group
	 * @return
	 * @throws SQLException
	 */
	public boolean addSubgroupToGroup(
			String subgroup, 
			String group) throws HabitatServiceException;

	/**
	 * Adds a user to a group.
	 * 
	 * @param username
	 * @param group
	 * @return
	 * @throws HabitatServiceException
	 */
	public boolean addUserToGroup(String username, String group) throws HabitatServiceException;

	/**
	 * Add a user to an organization.  The user can currently only have a single organization.
	 * 
	 * @param username
	 * @param orgid
	 * @return
	 * @throws SQLException
	 */
	public boolean addUserToOrganization(
			String username, 
			int orgid) throws HabitatServiceException;

	/**
	 * Returns the set of groups (by name) for which this group is a member / subgroup
	 * 
	 * @param group
	 * @return
	 * @throws HabitatServiceException
	 */
	Set<String> getParentGroups(String group) throws HabitatServiceException;

	/**
	 * Returns the set of groups (by ID) for which this group is a member / subgroup
	 * 
	 * @param group
	 * @return
	 * @throws HabitatServiceException
	 */
	Set<Integer> getParentGroupIds(Integer group) throws HabitatServiceException;

	/**
	 * Returns the set of groups (by ID) in which a user is directly a member.  
	 * 
	 * Does not follow transitive closure of parent groups.
	 * 
	 * @param username
	 * @return
	 * @throws HabitatServiceException
	 */
	Set<Integer> getGroupIdsForUser(String username) throws HabitatServiceException;

	/**
	 * Returns the set of groups (by name) in which a user is directly a member.  
	 * 
	 * Does not follow transitive closure of parent groups.
	 * 
	 * @param username
	 * @return
	 * @throws HabitatServiceException
	 */
	Set<String> getGroupsForUser(String username) throws HabitatServiceException;

	/**
	 * Gets the numeric unique ID for a group, by its unique name
	 * 
	 * @param groupName
	 * @return
	 * @throws HabitatServiceException
	 */
	Integer getGroupIdFrom(String groupName) throws HabitatServiceException;
	
	String getGroupFromId(Integer id) throws HabitatServiceException;

	/**
	 * Gets the numeric unique ID for an organization, by its unique name
	 * 
	 * @param organization
	 * @return
	 * @throws HabitatServiceException
	 */
	Integer getOrganizationIdFrom(String organization) throws HabitatServiceException;
	
	/**
	 * Gets the name of a given organization, given its ID
	 * 
	 * @param id
	 * @return
	 * @throws HabitatServiceException
	 */
	String getOrganizationFromId(Integer id) throws HabitatServiceException;

	/**
	 * Gets the connection's session token
	 * 
	 * @return
	 */
	public String getSessionToken();

	/**
	 * Gets the username under which we have made the connection -- and with which all
	 * permissions are associated
	 * @return
	 */
	public String getSessionUsername();

	/**
	 * Gets a unique user ID from the username.  The username is the definitive key.
	 * @param username
	 * @return
	 * @throws HabitatServiceException
	 */
	Integer getUserIdFrom(String username) throws HabitatServiceException;

	/**
	 * Looks up the user's username, given a numeric user ID
	 * 
	 * @param id
	 * @return
	 * @throws HabitatServiceException
	 */
	String getUserFromId(Integer id) throws HabitatServiceException;

	/**
	 * Gets the user's metadata as key-value properties
	 * 
	 * @param username
	 * @return
	 * @throws HabitatServiceException
	 */
	public Map<String,Object> getUserInfo(String username) throws HabitatServiceException;

	/**
	 * Tests whether the user is (transitively) a member of the group (by group ID)
	 * 
	 * @param username
	 * @param groupId
	 * @return
	 * @throws HabitatServiceException
	 */
	public boolean isUserInGroup(String username, int groupId) throws HabitatServiceException;

	/**
	 * Tests whether the user is (transitively) a member of the group (by group name)
	 * 
	 * @param username
	 * @param groupId
	 * @return
	 * @throws HabitatServiceException
	 */
	public boolean isUserInGroup(String username, String group) throws HabitatServiceException;

	/**
	 * Tests whether the user is in an organization
	 * 
	 * @param username
	 * @param orgId
	 * @return
	 * @throws HabitatServiceException
	 */
	public boolean isUserInOrganization(String username, int orgId) throws HabitatServiceException;

	/**
	 * Validates a user credential
	 * 
	 * @param username
	 * @param token
	 * @return
	 * @throws HabitatServiceException
	 */
	public boolean isUserCredentialValid(
			String username, 
			String token) throws HabitatServiceException;

	/**
	 * Tests that we have a valid registered account for the user with the service
	 * 
	 * @param username
	 * @param service
	 * @return
	 * @throws HabitatServiceException
	 */
	boolean isUserValid(String username, String service) throws HabitatServiceException;
	
	/**
	 * Given an existing user account, replace information about a credential they may have,
	 * either in the local system or in an external federated account. 
	 * 
	 * @param username The unique username of the individual
	 * @param service The external service (eg Google, GitHub) or "local"
	 * @param typeOrProtocol Optional descriptor of protocol type, eg, HTTP
	 * @param oldToken The previous token within "local", i.e., their internal password
	 * @param passwordOrToken The password/token we should store for this system
	 * @return
	 * @throws HabitatServiceException
	 */
	public boolean updateCredential(
			String username, 
			String service, 
			String typeOrProtocol, 
			String oldToken, 
			String token) throws HabitatServiceException;
	
	/**
	 * Update user properties
	 * 
	 * @param username
	 * @param first
	 * @param last
	 * @param street1
	 * @param street2
	 * @param city
	 * @param state
	 * @param zip
	 * @param country
	 * @param phone
	 * @param primaryEmail
	 * @param title
	 * @param organization
	 * @return
	 * @throws HabitatServiceException
	 */
	boolean updateUser(
			String username, 
			String first, 
			String last, 
			String street1, 
			String street2, 
			String city,
			String state, 
			String zip, 
			String country, 
			String phone, 
			String primaryEmail, 
			String title,
			String organization) throws HabitatServiceException;

}
