/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.model;

import static com.google.common.collect.Maps.transformValues;
import io.swagger.annotations.ApiModelProperty;

import java.util.HashMap;
import java.util.Map;

import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.StructuredValue;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * @author John Frommeyer
 *
 */
public class ProvNodeMapModel {

	@ApiModelProperty(required = true)
	public final Map<String, StoreNodeModel> nodes = new HashMap<>();

	public ProvNodeMapModel(Map<String, StoreNodeModel> nodes) {
		this.nodes.putAll(nodes);
	}

	public Map<String, StructuredData<String, Object>> toProvNodeMap()
			throws HabitatServiceException {
		final Map<String, StructuredData<String, Object>> res = new HashMap<>();
		for (final Map.Entry<String, StoreNodeModel> modelEntry : this.nodes
				.entrySet()) {
			final StructuredData<String, Object> data = new StructuredValue<>();
			data.put("location", modelEntry.getValue().provSpecifier);
			if (modelEntry.getValue().tupleWithSchema != null) {
				data.put("tuple", modelEntry.getValue().tupleWithSchema
						.toTupleWithSchema());
			}
			res.put(modelEntry.getKey(), data);
		}
		return res;
	}

	@Override
	public String toString() {
		return "ProvNodeMapModel [nodes=" + nodes + "]";
	}

	public static ProvNodeMapModel from(
			Map<String, StructuredData<String, Object>> nodes2) {
		Map<String, StoreNodeModel> modelMap = transformValues(
				nodes2,
				(StructuredData<String, Object> data) -> {
					final Object locationObject = data.get("location");
					if (!(locationObject instanceof ProvSpecifier)) {
						throw new IllegalStateException(
								String.format(
										"Expected ProvSpecifier under key 'location'. Got %s of class %s instead",
										locationObject,
										locationObject.getClass()));
					}
					final ProvSpecifier specifier = (ProvSpecifier) locationObject;

					final Object tupleObject = data.get("tuple");
					if (tupleObject != null && !(tupleObject instanceof TupleWithSchema)) {
						throw new IllegalStateException(
								String.format(
										"Expected TupleWithSchema under key 'tuple'. Got %s of class %s instead",
										tupleObject,
										tupleObject.getClass()));
					}
					@SuppressWarnings("unchecked")
					final TupleWithSchema<String> tuple = (TupleWithSchema<String>) tupleObject;
					final TupleWithSchemaModel tupleModel = tuple == null ? null
							: TupleWithSchemaModel.from(tuple);
					return new StoreNodeModel(
							ProvSpecifierModel.from(specifier),
							tupleModel);
				});
		return new ProvNodeMapModel(modelMap);
	}

}
