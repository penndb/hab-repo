/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.core.type;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ProvStringToken implements ProvToken {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String uuid;
	
	public ProvStringToken() {
		uuid = null;
	}
	
	public ProvStringToken(String value) {
		uuid = value;
	}
	
	public String getString() {
		return uuid;
	}
	
	public void setString(String uuid) {
		this.uuid = uuid;
	}
	
	public String toString() {
		return uuid;
	}
	
	@Override
	public int hashCode() {
		return uuid.hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ProvIntToken) && !(o instanceof ProvStringToken)) {
			return false;
		}
		
		if (o instanceof ProvIntToken)
			return ((ProvIntToken)o).uuid.compareTo(Integer.valueOf(uuid)) == 0;
		else
			return ((ProvStringToken)o).compareTo(this) == 0;
	}

	@Override
	public int compareTo(ProvSpecifier o) {
		if (o instanceof ProvStringToken)
			return uuid.compareTo(((ProvStringToken)o).uuid);
		if (o instanceof ProvIntToken)
			return Integer.valueOf(uuid).compareTo(((ProvIntToken)o).uuid);
		else
			return toString().compareTo(o.toString());
	}

	@JsonIgnore
	@Override
	public StructuredData<String, Object> getTuple() {
		StructuredData<String, Object> ret = new StructuredValue<>();
		ret.put("location", this);
		return ret;
	}
}
