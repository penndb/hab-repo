/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.provdm;

import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Sets.newHashSet;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Multimaps;

import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.Field;
import edu.upenn.cis.db.habitat.core.type.MultiField;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * @author John Frommeyer
 *
 */
public abstract class HasAttributes {
	private final List<Attribute> attributes;

	public HasAttributes(List<Attribute> attributes) {
		this.attributes = new ArrayList<>(attributes);
	}

	@ApiModelProperty(required = false)
	public List<Attribute> getAttributes() {
		return attributes;
	}

	public void addAttribute(QualifiedName key, QualifiedName value) {
		attributes.add(new QualifiedNameAttribute(key, value));
	}

	public void addAttribute(QualifiedName key, String value) {
		attributes.add(new StringAttribute(key, value));
	}

	public void addAttribute(QualifiedName key, Long value) {
		attributes.add(new LongAttribute(key, value));
	}

	public void addAttribute(QualifiedName key, Integer value) {
		attributes.add(new LongAttribute(key, value.longValue()));
	}

	public TupleWithSchema<String> toTupleWithSchema()
			throws HabitatServiceException {

		final ImmutableListMultimap<QualifiedName, Attribute> index = Multimaps
				.index(attributes, Attribute::getName);

		final List<Field<? extends Object>> fields = new ArrayList<>();
		final List<String> fieldNames = new ArrayList<>();
		final List<Class<?>> types = new ArrayList<>();

		for (final Map.Entry<QualifiedName, Collection<Attribute>> entry : index
				.asMap().entrySet()) {
			final String fieldName = entry.getKey().toString();
			final Collection<Attribute> attrValues = entry.getValue();
			final Field<?> field;
			if (attrValues.size() == 1) {
				field = attrValues.iterator().next().toField();
			} else {
				final HashSet<Object> values = newHashSet(transform(
						attrValues, Attribute::getValue));
				field = new MultiField<>(values);
			}
			fields.add(field);
			fieldNames.add(fieldName);
			types.add(field.getType());
		}

		// for (Attribute attribute : attributes) {
		// fields.add(attribute.toField());
		// fieldNames.add(attribute.getName().toString());
		// types.add(attribute.getType().javaType);
		// }
		final BasicSchema basicSchema = new BasicSchema(
				"attributeToTuple",
				fieldNames,
				types);
		final TupleWithSchema<String> tupleWithSchema = new BasicTuple(
				basicSchema,
				fields);
		return tupleWithSchema;
	}
}
