/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.core.expressions;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashSet;
import java.util.Set;

import edu.upenn.cis.db.habitat.core.expressions.PredicateOperations.ComparisonOperation;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;

/**
 * A comparison predicate evaluated over fields and/or literals in a single input
 * tuple
 * 
 * @author ZacharyIves
 *
 */
public class BinaryFieldComparisonPredicate extends BinaryTuplePredicateWithLookup {
	String fieldName;
	PredicateOperations.ComparisonOperation op;
	String field2;

	/**
	 * Comparison of field (left arg) with string (which must be quoted if it's
	 * a literal, else it's a second field name).
	 * Supports URL-encoded strings for representing special characters.
	 * 
	 * @param fieldName
	 * @param comparison
	 * @param target
	 * @throws UnsupportedEncodingException
	 */
	public BinaryFieldComparisonPredicate(String fieldName, 
			PredicateOperations.ComparisonOperation comparison, String target) throws UnsupportedEncodingException {
		
		this.fieldName = fieldName;
		this.op = comparison;
		
		field2 = target;
	}

	/**
	 * Null test
	 * 
	 * @param fieldName
	 * @param comparison
	 */
	public BinaryFieldComparisonPredicate(String fieldName, 
			PredicateOperations.ComparisonOperation comparison) {
		
		this.fieldName = fieldName;
		this.op = comparison;
		
		if (op != ComparisonOperation.NotNull && op != ComparisonOperation.IsNull)
			throw new UnsupportedOperationException();
	}
	
	@Override
	public void swapLeftAndRight() {
		String temp = fieldName;
		fieldName = field2;
		field2 = temp;
	}

	@Override
	public boolean test(TupleWithSchema<String> left, TupleWithSchema<String> right) {
		Object lField = this.getValue(fieldName);
		Object rField = field2 == null ? null : this.getValue(field2);
		
		switch (op) {
		case Equal:
			// Null is incomparable with null
			return lField != null && rField != null && lField.equals(rField);
		case NotEqual:
			// Null is incomparable with null
			return lField != null && rField != null && !lField.equals(rField);
			
		case NotNull:
			return lField != null;
		case IsNull:
			return lField == null;
			
		case Greater:
			{
				Comparable cLeft = (Comparable)lField;
				Comparable cRight = (Comparable)rField;
				// Null is incomparable with null
				return cLeft != null && cRight != null && cLeft.compareTo(cRight) > 0;
			}
		case GreaterEqual:
			{
				Comparable cLeft = (Comparable)lField;
				Comparable cRight = (Comparable)rField;
				// Null is incomparable with null
				return cLeft != null && cRight != null && cLeft.compareTo(cRight) >= 0;
			}
		case Less:
			{
				Comparable cLeft = (Comparable)lField;
				Comparable cRight = (Comparable)rField;
				// Null is incomparable with null
				return cLeft != null && cRight != null && cLeft.compareTo(cRight) < 0;
			}
		case LessEqual:
			{
				Comparable cLeft = (Comparable)lField;
				Comparable cRight = (Comparable)rField;
				// Null is incomparable with null
				return cLeft != null && cRight != null && cLeft.compareTo(cRight) <= 0;
			}
		}

		return false;
	}

	@Override
	public String toString() {
		if (field2 == null)
			return PredicateOperations.getStandardName(op) + "(" + fieldName + ")";
		else 
			return fieldName + " " + PredicateOperations.getStandardName(op) + " " + field2;
	}

	@Override
	public Set<String> getSymbolsReferencedLeft() {
		Set<String> ret = new HashSet<String>();
		if (isLeftAttribute(fieldName))
			ret.add(fieldName);
		
		if (field2 != null && !field2.isEmpty() && field2.charAt(0) != '\'' && field2.charAt(0) != '\"')
			if (isRightAttribute(field2))
				ret.add(field2);
		
		return ret;
	}

	@Override
	public Set<String> getSymbolsReferencedRight() {
		Set<String> ret = new HashSet<String>();
		if (isRightAttribute(fieldName))
			ret.add(fieldName);
		if (field2 != null && !field2.isEmpty() && field2.charAt(0) != '\'' && field2.charAt(0) != '\"')
			if (isRightAttribute(field2))
				ret.add(field2);
		
		return ret;
	}
	
	public String getLeft() {
		return fieldName;
	}
	
	public String getRight() {
		return field2;
	}
}
