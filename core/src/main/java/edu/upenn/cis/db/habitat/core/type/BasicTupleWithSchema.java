/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;

public class BasicTupleWithSchema<K> extends StructuredValue<K, Field<? extends Object>>
	implements MutableTupleWithSchema<K> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Schema<K,Class<? extends Object>> schema;
	List<Field<? extends Object>> fields;
	
	public BasicTupleWithSchema(Schema<K, Class<? extends Object>> schema, List<Field<? extends Object>> fields) {
		super();
		this.schema = schema;

		setFields(fields);
	}
	
	
	public List<K> getKeys() {
		return getSchema().getKeys();
	}

	public Schema<K, Class<? extends Object>> getSchema() {
		return schema;
	}

	public void setSchema(Schema<K, Class<? extends Object>> schema) {
		this.schema = schema;
	}

	public List<Field<? extends Object>> getFields() {
		return fields;
	}

	public void setFields(List<Field<? extends Object>> fields) {
		// Store fields as a list
		this.fields = fields;
		
		int siz = schema.getKeys().size();
		int siz2 = fields.size();
		if (siz > siz2)
			siz = siz2;
		// And in the map
		for (int i = 0; i < siz; i++)
			put(schema.getKeys().get(i), fields.get(i));
	}

	public <E> void setValueAt(int index, E value) {
		Field<E> f = (Field<E>) fields.get(index);//super.get(schema.getKeys().get(index));
		
		if (f != null)
			try {
				f.setValue(value);
			} catch (ClassCastException e) {
				e.printStackTrace();
			}
	}

	public Object getValueAt(int index) {
		Field<? extends Object> f = fields.get(index);//super.get(schema.getKeys().get(index));
		if (f != null)
			return f.getValue();
		else
			return null;
	}

	public <E >void setValue(K key, E value) {
		Field<E> f = (Field<E>)super.get(key);
		try {
			f.setValue(value);
		} catch (Exception e) {
			e.printStackTrace();
		}

		//if (!containsKey(key))
			put(key, f);
	}

	public Object getValue(K key) {
		if (super.get(key) != null)
			return super.get(key).getValue();
		else
			return null;//throw new RuntimeException("Schema " + getSchema() + " is missing element " + key);
	}

	public String toCSV() {
		StringBuilder builder = new StringBuilder();
		
		for (int i = 0; i < fields.size(); i++) {
			if (i > 0)
				builder.append(',');

			Object f = fields.get(i).getValue();
			if (f == null) {
				builder.append("null");
			} else if (f instanceof String)
				builder.append("\"" + f.toString() + "\"");
			else
				builder.append(f.toString());
		}
		
		return builder.toString();
	}

	public BasicTupleWithSchema<K> project(String name, List<K> projKeys) {
		Schema<K,Class<? extends Object>> next = getSchema().project(name, projKeys);

		BasicTupleWithSchema<K> ret = next.createTuple();
		int projTo = 0;
		for (int i = 0; i < getSchema().getKeys().size(); i++) {
			if (projKeys.contains(getSchema().getKeyAt(i))) {
				Field<Object> f = (Field<Object>) ret.getFields().get(projTo); 
				f.setValue(getValueAt(i));
				
				projTo++;
			}
		}
		return ret;
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		builder.append(getSchema().getName());
		builder.append('(');
		
		int siz = getKeys().size();
		if (siz > getFields().size())
			siz = getFields().size();
		
		for (int i = 0; i < siz; i++) {
			if (i > 0)
				builder.append(',');
			
			builder.append(getValueAt(i));
		}
		
		builder.append(')');
		return builder.toString();
	}
	
	Map<Schema, int[]> projMap = new HashMap<>();
	
	public <E> BasicTupleWithSchema<K> project(Schema<K,Class<E>> next, Function<Schema, BasicTupleWithSchema<K>> getTuple) {
		BasicTupleWithSchema<K> ret = getTuple.apply(next);//.createTuple();
//		ret.setSchema((Schema)next);
		
		int[] projectThese = projMap.get(next);
		if (projectThese == null) {
			projectThese = new int[ret.getKeys().size()];
			int projTo = 0;
			for (int i = 0; i < getSchema().getKeys().size(); i++) {
				if (ret.getKeys().contains(getSchema().getKeyAt(i))) {
					projectThese[projTo++] = i;
				}
			}
			if (projTo < ret.getKeys().size()) {
				throw new RuntimeException("Missing source for attributes in " + ret.getKeys());
			}
			
			projMap.put(next,  projectThese);
		}
		
		for (int i = 0; i < projectThese.length; i++) {
			Field<E> field = (Field<E>) ret.getFields().get(i);
			field.setValue((E)getValueAt(projectThese[i]));
		}
		
//		int projTo = 0;
//		for (int i = 0; i < getSchema().getKeys().size(); i++) {
//			if (ret.getKeys().contains(getSchema().getKeyAt(i))) {
//				Field<E> field = (Field<E>) ret.getFields().get(projTo);
//				field.setValue((E)getValueAt(i));
//				
//				projTo++;
//			}
//		}
		return ret;
	}


	@Override
	public MutableTupleWithSchema<K> getMutable() {
		return this;
	}


	@Override
	public <E> boolean augment(BasicSchema outputSchema, Iterable<E> extractions, List<String> opAndFields,
			Collection<MutableTupleWithSchema<String>> outputs, Supplier<ProvToken> tokenSupplier,
			BinaryOperator<ProvSpecifier> combiner, List<MutableTupleWithSchema<String>> recycled) {
		throw new UnsupportedOperationException();
	}


	@Override
	public MutableTupleWithSchema<String> concat(TupleWithSchema<String> rt, BasicSchema basicSchema,
			BinaryOperator<ProvSpecifier> combiner) {
		throw new UnsupportedOperationException();
	}


	@Override
	public MutableTupleWithSchema<String> concat(TupleWithSchema<String> rt, BasicSchema basicSchema,
			BinaryOperator<ProvSpecifier> combiner, MutableTupleWithSchema<String> ret) {
		throw new UnsupportedOperationException();
	}
	
}
