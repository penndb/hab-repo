/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.provdm;

import com.google.common.annotations.VisibleForTesting;


/**
 * @author John Frommeyer
 *
 */
public final class ProvDmConstants {

	public static final String PROV_DM_NAMESPACE =
			"http://www.w3.org/ns/prov#";

	public static final String PENN_PROV_NAMESPACE =
			"http://pennprovenance.net/ns/prov#";

	public static final QualifiedName TYPE_ATTRIBUTE = newProvName("type");

	public static final QualifiedName LABEL_ATTRIBUTE = newProvName("label");

	public static final QualifiedName LOCATION_ATTRIBUTE = newProvName("location");

	public static final QualifiedName ROLE_ATTRIBUTE = newProvName("role");

	public static final QualifiedName VALUE_ATTRIBUTE = newProvName("value");

	// Entity types
	public static final QualifiedName BUNDLE = newProvName("Bundle");
	public static final QualifiedName COLLECTION = newProvName("Collection");
	public static final QualifiedName EMPTY_COLLECTION = newProvName("EmptyCollection");
	public static final QualifiedName PLAN = newProvName("Plan");

	// Agent types
	public static final QualifiedName ORGANIZATION = newProvName("Organization");
	public static final QualifiedName PERSON = newProvName("Person");
	public static final QualifiedName SOFTWARE_AGENT = newProvName("SoftwareAgent");

	// Derivation types
	public static final QualifiedName PRIMARY_SOURCE = newProvName("PrimarySource");
	public static final QualifiedName QUOTATION = newProvName("Quotation");
	public static final QualifiedName REVISION = newProvName("Revision");

	private static QualifiedName newProvName(String localPart) {
		return new QualifiedName(
				PROV_DM_NAMESPACE, localPart);
	}

	@VisibleForTesting
	public static final QualifiedName PROV_DM_TYPE_QN = new QualifiedName(
			PENN_PROV_NAMESPACE, "provDmName");

	private ProvDmConstants() {}
}
