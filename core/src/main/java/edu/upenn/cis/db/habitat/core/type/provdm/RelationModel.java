/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.provdm;

import static com.google.common.base.Preconditions.checkNotNull;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Nullable;

import com.google.common.annotations.VisibleForTesting;

import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvDmRelation.AdditionalProperty;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * A relation in a PROV DM graph.
 * 
 * @author John Frommeyer
 *
 */
@ApiModel(description = "a relation")
public class RelationModel extends HasAttributes {

	@VisibleForTesting
	public static final QualifiedName PROV_DM_RELATION_ID_QN = new QualifiedName(
			ProvDmConstants.PENN_PROV_NAMESPACE, "provDmRelationId");
	@VisibleForTesting
	public static final QualifiedName PROV_DM_TIME_QN = new QualifiedName(
			ProvDmConstants.PENN_PROV_NAMESPACE, "provDmTime");

	@VisibleForTesting
	public static final QualifiedName PROV_DM_GENERATION_ID_QN = new QualifiedName(
			ProvDmConstants.PENN_PROV_NAMESPACE, "provDmGenerationId");

	@VisibleForTesting
	public static final QualifiedName PROV_DM_USAGE_ID_QN = new QualifiedName(
			ProvDmConstants.PENN_PROV_NAMESPACE, "provDmUsageId");

	private final ProvDmRelation type;
	private final QualifiedName subjectId;
	private final QualifiedName objectId;
	private final QualifiedName secondaryObjectId;
	private final QualifiedName relationId;
	private final Date time;
	private final QualifiedName generationId;
	private final QualifiedName usageId;

	private RelationModel(
			ProvDmRelation relation,
			QualifiedName subject,
			QualifiedName object,
			List<Attribute> attributes,
			@Nullable QualifiedName secondaryObject,
			@Nullable QualifiedName relationId,
			@Nullable Date time,
			@Nullable QualifiedName generationId,
			@Nullable QualifiedName usageId) {
		super(attributes);
		this.type = checkNotNull(relation);
		this.subjectId = checkNotNull(subject);
		this.objectId = checkNotNull(object);
		if (secondaryObject != null
				&& !this.type
						.hasAdditionalProperty(AdditionalProperty.SECONDARY_OBJECT)) {
			throw new IllegalArgumentException(String.format(
					"relation %s does not have a secondary object",
					this.type.name()));
		}
		this.secondaryObjectId = secondaryObject;
		if (relationId != null
				&& !this.type
						.hasAdditionalProperty(AdditionalProperty.ID)) {
			throw new IllegalArgumentException(String.format(
					"relation %s does not have an id",
					this.type.name()));
		}
		this.relationId = relationId;
		this.time = time;
		if (time != null
				&& !this.type
						.hasAdditionalProperty(AdditionalProperty.TIME)) {
			throw new IllegalArgumentException(String.format(
					"relation %s does not have a time",
					this.type.name()));
		}
		this.generationId = generationId;
		if (generationId != null
				&& !this.type
						.hasAdditionalProperty(AdditionalProperty.GENERATION_ID)) {
			throw new IllegalArgumentException(String.format(
					"relation %s does not have a generation id",
					this.type.name()));
		}
		this.usageId = usageId;
		if (usageId != null
				&& !this.type
						.hasAdditionalProperty(AdditionalProperty.USAGE_ID)) {
			throw new IllegalArgumentException(String.format(
					"relation %s does not have a usage id",
					this.type.name()));
		}
	}

	private RelationModel(
			ProvDmRelation relation,
			QualifiedName subject,
			QualifiedName object) {
		this(relation,
				subject,
				object,
				new ArrayList<>(),
				null,
				null,
				null,
				null,
				null);
	}

	public static RelationModel newWasGeneratedBy(
			QualifiedName entity,
			QualifiedName activity,
			List<Attribute> attributes,
			@Nullable QualifiedName relationId,
			@Nullable Date generationTime) {
		return new RelationModel(
				ProvDmRelation.GENERATION,
				entity,
				activity,
				attributes,
				null,
				relationId,
				generationTime,
				null,
				null);
	}

	public static RelationModel newUsed(
			QualifiedName activity,
			QualifiedName entity,
			List<Attribute> attributes,
			@Nullable QualifiedName relationId,
			@Nullable Date time) {
		return new RelationModel(
				ProvDmRelation.USAGE,
				activity,
				entity,
				attributes,
				null,
				relationId,
				time,
				null,
				null);
	}

	public static RelationModel newWasInformedBy(
			QualifiedName informed,
			QualifiedName informant,
			List<Attribute> attributes,
			@Nullable QualifiedName relationId) {
		return new RelationModel(
				ProvDmRelation.COMMUNICATION,
				informed,
				informant,
				attributes,
				null,
				relationId,
				null,
				null,
				null);
	}

	public static RelationModel newWasStartedBy(
			QualifiedName activity,
			QualifiedName trigger,
			List<Attribute> attributes,
			@Nullable QualifiedName starter,
			@Nullable QualifiedName relationId,
			@Nullable Date time) {
		return new RelationModel(
				ProvDmRelation.START,
				activity,
				trigger,
				attributes,
				starter,
				relationId,
				time,
				null,
				null);
	}

	public static RelationModel newWasEndedBy(
			QualifiedName activity,
			QualifiedName trigger,
			List<Attribute> attributes,
			@Nullable QualifiedName ender,
			@Nullable QualifiedName relationId,
			@Nullable Date time) {
		return new RelationModel(
				ProvDmRelation.END,
				activity,
				trigger,
				attributes,
				ender,
				relationId,
				time,
				null,
				null);
	}

	public static RelationModel newWasInvalidatedBy(
			QualifiedName entity,
			QualifiedName activity,
			List<Attribute> attributes,
			@Nullable QualifiedName relationId,
			@Nullable Date time) {

		return new RelationModel(
				ProvDmRelation.INVALIDATION,
				entity,
				activity,
				attributes,
				null,
				relationId,
				time,
				null,
				null);
	}

	public static RelationModel newWasDerivedFrom(
			QualifiedName generatedEntity,
			QualifiedName usedEntity,
			List<Attribute> attributes,
			@Nullable QualifiedName activity,
			@Nullable QualifiedName relationId,
			@Nullable QualifiedName generationId,
			@Nullable QualifiedName usageId) {
		return new RelationModel(
				ProvDmRelation.DERIVATION,
				generatedEntity,
				usedEntity,
				attributes,
				activity,
				relationId,
				null,
				generationId,
				usageId);
	}

	public static RelationModel newWasAttributedTo(
			QualifiedName entity,
			QualifiedName agent,
			List<Attribute> attributes,
			@Nullable QualifiedName relationId) {
		return new RelationModel(
				ProvDmRelation.ATTRIBUTION,
				entity,
				agent,
				attributes,
				null,
				relationId,
				null,
				null,
				null);
	}

	public static RelationModel newWasAnnotatedBy(QualifiedName annotId, QualifiedName sourceId,
			QualifiedName key, String value) {
		List<Attribute> attribs = new ArrayList<Attribute>();
		attribs.add(new StringAttribute(key, value));
		return new RelationModel(
				ProvDmRelation.ANNOTATED,
				annotId,
				sourceId,
				attribs,
				null,
				null,
				null,
				null,
				null);
	}
	public static RelationModel newWasAssociatedWith(
			QualifiedName activity,
			QualifiedName agent,
			List<Attribute> attributes,
			@Nullable QualifiedName plan,
			@Nullable QualifiedName relationId) {
		return new RelationModel(
				ProvDmRelation.ASSOCIATION,
				activity,
				agent,
				attributes,
				plan,
				relationId,
				null,
				null,
				null);
	}

	public static RelationModel newActedOnBehalfOf(
			QualifiedName delegate,
			QualifiedName responsible,
			List<Attribute> attributes,
			@Nullable QualifiedName activity,
			@Nullable QualifiedName relationId) {
		return new RelationModel(
				ProvDmRelation.DELEGATION,
				delegate,
				responsible,
				attributes,
				activity,
				relationId,
				null,
				null,
				null);
	}

	public static RelationModel newWasInfluencedBy(
			QualifiedName influencee,
			QualifiedName influencer,
			List<Attribute> attributes,
			@Nullable QualifiedName relationId) {
		return new RelationModel(
				ProvDmRelation.INFLUENCE,
				influencee,
				influencer,
				attributes,
				null,
				relationId,
				null,
				null,
				null);
	}

	public static RelationModel newHasMember(
			QualifiedName collection,
			QualifiedName member) {
		return new RelationModel(ProvDmRelation.MEMBERSHIP, collection, member);
	}

	public static RelationModel newSpecializationOf(
			QualifiedName specific,
			QualifiedName generic) {
		return new RelationModel(ProvDmRelation.SPECIALIZATION, specific, generic);
	}

	public static RelationModel newAlternateOf(
			QualifiedName alternate1,
			QualifiedName alternate2) {
		return new RelationModel(ProvDmRelation.ALTERNATE, alternate1, alternate2);
	}

	@ApiModelProperty(required = true)
	public ProvDmRelation getType() {
		return type;
	}

	@ApiModelProperty(required = true)
	public QualifiedName getSubjectId() {
		return subjectId;
	}

	@ApiModelProperty(required = true)
	public QualifiedName getObjectId() {
		return objectId;
	}

	@ApiModelProperty(
			required = false,
			value = "It is an error to include attributes in a MEMEBERSHIP, ALTERNATE, or SPECIALIZATION relation")
	public List<Attribute> getAttributes() {
		return super.getAttributes();
	}

	@ApiModelProperty(
			required = false,
			value = "It is an error to include a secondary object in a relation other than ASSOCIATION, DERIVATION, START, END, or DELEGATION")
	public QualifiedName getSecondaryObjectId() {
		return secondaryObjectId;
	}

	@ApiModelProperty(
			required = false,
			value = "It is an error to include attributes in a MEMEBERSHIP, ALTERNATE, or SPECIALIZATION relation")
	public QualifiedName getRelationId() {
		return relationId;
	}

	@ApiModelProperty(
			required = false,
			value = "It is an error to include time in a relation other than GENERATION, USAGE, START, END, or INVALIDATION")
	public Date getTime() {
		return time;
	}

	@ApiModelProperty(
			required = false,
			value = "It is an error to include a generation id in a relation other than DERIVATION")
	public QualifiedName getGenerationId() {
		return generationId;
	}

	@ApiModelProperty(
			required = false,
			value = "It is an error to include a usage id in a relation other than DERIVATION")
	public QualifiedName getUsageId() {
		return usageId;
	}

	@Override
	public TupleWithSchema<String> toTupleWithSchema()
			throws HabitatServiceException {
		addAttribute(ProvDmConstants.PROV_DM_TYPE_QN, type.name());
		if (relationId != null) {
			addAttribute(PROV_DM_RELATION_ID_QN, relationId);
		}
		if (time != null) {
			addAttribute(PROV_DM_TIME_QN, time.getTime());
		}
		if (generationId != null) {
			addAttribute(PROV_DM_GENERATION_ID_QN, generationId);
		}
		if (usageId != null) {
			addAttribute(PROV_DM_USAGE_ID_QN, usageId);
		}
		return super.toTupleWithSchema();
	}

	@Override
	public void addAttribute(QualifiedName key, QualifiedName value) {
		checkQualifiedNameAttribute(key);
		super.addAttribute(key, value);
	}

	@Override
	public void addAttribute(QualifiedName key, Long value) {
		checkIntegralAttribute(key);
		super.addAttribute(key, value);
	}

	@Override
	public void addAttribute(QualifiedName key, Integer value) {
		checkIntegralAttribute(key);
		super.addAttribute(key, value);
	}

	private void checkIntegralAttribute(QualifiedName key) {
		if (key.equals(PROV_DM_TIME_QN)
				&& !type.hasAdditionalProperty(AdditionalProperty.TIME)) {
			throw new IllegalArgumentException(String.format(
					"relation %s does not have a time property",
					type.name()));
		}
	}

	private void checkQualifiedNameAttribute(QualifiedName key) {
		if (key.equals(PROV_DM_RELATION_ID_QN)
				&& !type.hasAdditionalProperty(AdditionalProperty.ID)) {
			throw new IllegalArgumentException(String.format(
					"relation %s does not have an id property",
					type.name()));
		}

		if (key.equals(PROV_DM_GENERATION_ID_QN)
				&& !type
						.hasAdditionalProperty(AdditionalProperty.GENERATION_ID)) {
			throw new IllegalArgumentException(String.format(
					"relation %s does not have an generation id property",
					type.name()));
		}

		if (key.equals(PROV_DM_USAGE_ID_QN)
				&& !type
						.hasAdditionalProperty(AdditionalProperty.USAGE_ID)) {
			throw new IllegalArgumentException(String.format(
					"relation %s does not have an usage id property",
					type.name()));
		}
	}

}
