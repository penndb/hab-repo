/*
 * Copyright 2016 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.provdm;

import java.util.EnumSet;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Nullable;

import com.google.common.collect.ImmutableMap;

/**
 * @author John Frommeyer
 *
 */
public enum ProvDmType {

	ENTITY(
			"entity",
			ProvDmRelation.ENTITY_RELATIONS,
			null,
			null),
	BUNDLE(
			"bundle",
			ProvDmRelation.ENTITY_RELATIONS,
			ENTITY,
			ProvDmConstants.BUNDLE),
	COLLECTION(
			"collection",
			ProvDmRelation.ENTITY_RELATIONS,
			ENTITY,
			ProvDmConstants.COLLECTION),
	EMPTY_COLLECTION(
			"empty collection",
			ProvDmRelation.ENTITY_RELATIONS,
			ENTITY,
			ProvDmConstants.EMPTY_COLLECTION),
	PLAN(
			"plan",
			ProvDmRelation.ENTITY_RELATIONS,
			ENTITY,
			ProvDmConstants.PLAN),
	ACTIVITY(
			"activity",
			EnumSet.of(ProvDmRelation.USAGE,
					ProvDmRelation.START,
					ProvDmRelation.END,
					ProvDmRelation.COMMUNICATION,
					ProvDmRelation.ASSOCIATION,
					ProvDmRelation.GENERATION,
					ProvDmRelation.INVALIDATION,
					ProvDmRelation.DERIVATION,
					ProvDmRelation.DELEGATION),
			null,
			null),
	AGENT(
			"agent",
			ProvDmRelation.AGENT_RELATIONS,
			null,
			null),
	ORGANIZATION(
			"organization",
			ProvDmRelation.AGENT_RELATIONS,
			AGENT,
			ProvDmConstants.ORGANIZATION),
	PERSON(
			"person",
			ProvDmRelation.AGENT_RELATIONS,
			AGENT,
			ProvDmConstants.PERSON),
	SOFTWARE_AGENT(
			"software agent",
			ProvDmRelation.AGENT_RELATIONS,
			AGENT,
			ProvDmConstants.SOFTWARE_AGENT);

	public static final Map<String, ProvDmType> PROV_DM_NAME_TO_TYPE;

	static {
		final ImmutableMap.Builder<String, ProvDmType> nameToTypeBuilder = ImmutableMap
				.builder();
		for (final ProvDmType type : ProvDmType.values()) {
			nameToTypeBuilder.put(type.provDmName, type);
		}
		PROV_DM_NAME_TO_TYPE = nameToTypeBuilder.build();
	}

	private final String provDmName;
	private final EnumSet<ProvDmRelation> relations;
	private final ProvDmType superType;
	private final Optional<QualifiedName> subtypeQualifiedName;

	private ProvDmType(
			String provDmName,
			EnumSet<ProvDmRelation> relations,
			@Nullable ProvDmType superType,
			@Nullable QualifiedName subtypeQualifiedName) {
		this.provDmName = provDmName;
		this.relations = relations;
		this.superType = superType;
		this.subtypeQualifiedName = Optional.ofNullable(subtypeQualifiedName);
	}

	public String getProvDmName() {
		return provDmName;
	}

	public EnumSet<ProvDmRelation> getRelations() {
		return relations;
	}

	public ProvDmType getSuperType() {
		return superType == null ? this : superType;
	}

	public Optional<QualifiedName> getQualifiedName() {
		return subtypeQualifiedName;
	}

}
