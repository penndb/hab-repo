/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.core.expressions;

public class PredicateOperations {

	public static enum ComparisonOperation {Equal, Greater, Less, GreaterEqual, LessEqual, NotEqual, IsNull, NotNull};
	
//	public static enum IntegerOperation {Negate, Plus, Minus, Times, Div, Mod};
	
//	public static enum FloatOperations {Negate, Plus, Minus, Times, Div};
	
	public static enum BooleanOperation {Not, And, Or, Xor};
	
	public static ComparisonOperation getComparison(String compName) {
		if (compName.toLowerCase().equals("equal") || compName.toLowerCase().equals("equals") ||
				compName.equals("==") || compName.equals("=")) {
			return ComparisonOperation.Equal;
		} else if (compName.toLowerCase().equals("notequal") || compName.toLowerCase().equals("ne")
				|| compName.equals("!=") || compName.equals("<>")) {
			return ComparisonOperation.NotEqual;
		} else if (compName.toLowerCase().equals("less") || compName.toLowerCase().equals("lessthan")
				|| compName.equals("<") || compName.equals("&lt;")) {
			return ComparisonOperation.Less;
		} else if (compName.toLowerCase().equals("greater") || compName.toLowerCase().equals("greaterthan")
				|| compName.equals(">") || compName.equals("&gt;")) {
			return ComparisonOperation.Greater;
		} else if (compName.toLowerCase().equals("lessequal") 
				|| compName.equals("<=") || compName.equals("&lt;=")) {
			return ComparisonOperation.LessEqual;
		} else if (compName.toLowerCase().equals("greaterequal")  
				|| compName.equals(">=") || compName.equals("&gt;=")) {
			return ComparisonOperation.GreaterEqual;
		}
		throw new UnsupportedOperationException(compName);
	}
	
	public static String getStandardName(ComparisonOperation op) {
		switch (op) {
		case Equal:
			return "==";
		case NotEqual:
			return "!=";
		case Less:
			return "<";
		case Greater:
			return ">";
		case LessEqual:
			return "<=";
		case GreaterEqual:
			return ">=";
		case IsNull:
			return "IsNull";
		case NotNull:
			return "NotNull";
		}
		throw new UnsupportedOperationException(op.name());
	}

	public static String getStandardName(BooleanOperation op) {
		switch (op) {
		case And:
			return "&&";
		case Or:
			return "||";
		case Not:
			return "Not";
		case Xor:
			return "Xor";
		}
		throw new UnsupportedOperationException(op.name());
	}
}
