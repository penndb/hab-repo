/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.model;

import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

/**
 * 
 * @author John Frommeyer
 *
 */
public class ProvSpecifierTypeAdapterFactory implements TypeAdapterFactory {

	@Override
	public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
		if (!ProvSpecifierModel.class.isAssignableFrom(type.getRawType())) {
			return null;
		}

		TypeAdapter<JsonElement> jsonElementAdapter = gson
				.getAdapter(JsonElement.class);
		TypeAdapter<ProvLocationModel> locationAdapter = gson
				.getDelegateAdapter(this,
						TypeToken.get(ProvLocationModel.class));
		TypeAdapter<ProvTokenModel> tokenAdapter = gson.getDelegateAdapter(
				this, TypeToken.get(ProvTokenModel.class));

		@SuppressWarnings("unchecked")
		final TypeAdapter<T> specifierAdapter = (TypeAdapter<T>) new ProvSpecifierTypeAdapter(
				jsonElementAdapter, locationAdapter, tokenAdapter).nullSafe();
		return specifierAdapter;

	}

	private static class ProvSpecifierTypeAdapter extends
			TypeAdapter<ProvSpecifierModel> {

		private final TypeAdapter<JsonElement> jsonElementAdapter;
		private final TypeAdapter<ProvLocationModel> locationAdapter;
		private final TypeAdapter<ProvTokenModel> tokenAdapter;

		ProvSpecifierTypeAdapter(TypeAdapter<JsonElement> jsonElementAdapter,
				TypeAdapter<ProvLocationModel> locationAdapter,
				TypeAdapter<ProvTokenModel> tokenAdapter) {
			this.jsonElementAdapter = jsonElementAdapter;
			this.locationAdapter = locationAdapter;
			this.tokenAdapter = tokenAdapter;
		}

		@Override
		public void write(JsonWriter out, ProvSpecifierModel value)
				throws IOException {
			if (ProvLocationModel.class.isAssignableFrom(value.getClass())) {
				locationAdapter.write(out, (ProvLocationModel) value);
			} else if (ProvTokenModel.class.isAssignableFrom(value.getClass())) {
				tokenAdapter.write(out, (ProvTokenModel) value);
			}
		}

		@Override
		public ProvSpecifierModel read(JsonReader in) throws IOException {
			final JsonObject specifierJson = jsonElementAdapter.read(in)
					.getAsJsonObject();
			if (specifierJson.has(ProvTokenModel.TOKEN_VALUE_PROP_NAME)) {
				return tokenAdapter.fromJsonTree(specifierJson);
			}
			if (specifierJson.has(ProvLocationModel.FIELD_PROP_NAME)) {
				return locationAdapter.fromJsonTree(specifierJson);
			}
			throw new IllegalArgumentException(
					"unable to deserialize ProvSpecifierModel: "
							+ specifierJson.getAsString());

		}
	}

}
