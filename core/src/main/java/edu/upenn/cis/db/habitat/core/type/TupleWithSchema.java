/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type;

import java.util.Collection;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * A basic tuple with a schema consisting of keys of type K.
 * Assumed to be immutable.
 * 
 * @author zives
 *
 * @param <K>
 */
public interface TupleWithSchema<K> extends StructuredData<K,Field<? extends Object>> {
	public List<K> getKeys();

	public Schema<K, Class<? extends Object>> getSchema();

	public List<Field<? extends Object>> getFields();

	public Object getValue(K key);

	public Object getValueAt(int index);

	public String toCSV();

	public TupleWithSchema<K> project(String name, List<K> projKeys);
	
	public <E> MutableTupleWithSchema<K> project(Schema<K,Class<E>> next,
			Function<Schema, BasicTupleWithSchema<K>> getTuple);

	public MutableTupleWithSchema<K> getMutable();

	public <E> boolean augment(BasicSchema outputSchema, 
			Iterable<E> extractions,
			List<String> opAndFields,
			Collection<MutableTupleWithSchema<String>> outputs,
			Supplier<ProvToken> tokenSupplier,
			BinaryOperator<ProvSpecifier> combiner,
			List<MutableTupleWithSchema<String>> recycled);

	public MutableTupleWithSchema<String> concat(TupleWithSchema<String> rt, BasicSchema basicSchema, 
			BinaryOperator<ProvSpecifier> combiner);

	public MutableTupleWithSchema<String> concat(TupleWithSchema<String> rt, BasicSchema basicSchema, 
			BinaryOperator<ProvSpecifier> combiner, MutableTupleWithSchema<String> ret);
}
