/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class NaryProvExpression extends ProvExpression {
	List<ProvSpecifier> children = new ArrayList<>();
	
	String cache = null;

	NaryProvExpression() {
		super(ProvOperation.PROV_SUM);
	}
	
	public NaryProvExpression(ProvOperation operation, List<ProvSpecifier> children) {
		super(operation);
		this.children.addAll(children);
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public List<ProvSpecifier> getChildren() {
		return Collections.unmodifiableList(children);
	}

	public void setChildren(List<ProvSpecifier> newChildren) {
		children.clear();
		children.addAll(newChildren);
	}

	public String toString() {
		if (cache != null)
			return cache;
		
		StringBuilder builder = new StringBuilder();
		
		builder.append('(');
		
		if (!children.isEmpty()) {
			builder.append(children.get(0).toString());
			for (int i = 1; i < children.size(); i++) {
				switch (getOperation()) {
				case PROV_MULT:
					builder.append(" (*) ");
					break;
				case PROV_SUM:
					builder.append(" (+) ");
					break;
				case PROV_COMP:
					builder.append(" (o) ");
				default:
					throw new UnsupportedOperationException("Unknown operation type");
				}
				builder.append(children.get(i).toString());
			}
		}
		
		builder.append(')');
		
		cache = builder.toString();
		
		return cache;
	}

	@Override
	public int compareTo(ProvSpecifier o) {
		return toString().compareTo(o.toString());
	}

	@JsonIgnore
	@Override
	public StructuredData<String, Object> getTuple() {
		StructuredData<String, Object> ret = new StructuredValue<>();
		ret.put("location", this);
		return ret;
	}
}
