package edu.upenn.cis.db.habitat.core.expressions;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;

public class UnaryArithmeticExpressionOperation<T> implements ArithmeticExpressionOperation<T> {
	Class<T> returnType;
	ArithmeticOperation op;
	List<ArithmeticExpressionOperation<?>> children = new ArrayList<>();;
	

	public UnaryArithmeticExpressionOperation(Class<T> returnType, ArithmeticOperation op, ArithmeticExpressionOperation<?> child) {
		children.add(child);
		this.op = op;
		this.returnType = returnType;
	}


	@Override
	public void bind(TupleWithSchema<String> tuple) {
		children.get(0).bind(tuple);
	}

	@Override
	public T getValue() {
		Object childValue = children.get(0).getValue();
		switch (op) {
		case Cast:
			if (returnType == Integer.class) {
				return (childValue instanceof Integer) ? (T)childValue : (T)Integer.valueOf(childValue.toString());
			} else if (returnType == Double.class) {
				return (childValue instanceof Double) ? (T)childValue : (T)Double.valueOf(childValue.toString());
			} else if (returnType == Date.class) {
				return (childValue instanceof Date) ? (T)childValue : (T)Date.valueOf(childValue.toString());
			} else if (returnType == String.class) {
				return (childValue instanceof String) ? (T)childValue : (T)(childValue.toString());
			} else if (returnType == Boolean.class) {
				return (childValue instanceof Boolean) ? (T)childValue : (T)Boolean.valueOf(childValue.toString());
			} else if (returnType == Long.class) {
				return (childValue instanceof Long) ? (T)childValue : (T)Long.valueOf(childValue.toString());
			} else
				throw new UnsupportedOperationException("Unsupported type: " + returnType.getName());
		case Eval:
		case Literal:
		case Variable:
			return (T)children.get(0).getValue();
		case Negate:
		default:
			throw new UnsupportedOperationException("Unsupported unary operation " + op.name()); 
		}
	}

	@Override
	public List<ArithmeticExpressionOperation<?>> getChildren() {
		return children;
	}

	@Override
	public Class<T> getType() {
		return returnType;
	}

	public String toString() {
		return ArithmeticExpressionOperation.getStandardName(getOp()) + "(" + getChildren().get(0).toString() + ")";
	}


	@Override
	public ArithmeticOperation getOp() {
		return op;
	}

	@Override
	public Set<String> getSymbolsReferenced() {
		return children.get(0).getSymbolsReferenced();
	}

	@Override
	public void rename(Map<String, String> map) {
		children.get(0).rename(map);
	}
}