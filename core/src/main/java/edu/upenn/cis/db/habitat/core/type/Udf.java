package edu.upenn.cis.db.habitat.core.type;

import java.util.ArrayList;
import java.util.List;

import edu.upenn.cis.db.habitat.core.expressions.ArithmeticExpressionOperation;

public class Udf<T,R,S> implements Metadata {
	BasicSchema outputSchema;
	BasicSchema argumentNames;
	List<ArithmeticExpressionOperation<S>> arguments = new ArrayList<>();
	String name;
	JavaUdf<T,R,S> function;
	
	boolean isAggregate = false;
	boolean isTableValued = false;
	
	public Udf(String name, BasicSchema argumentNames, List<ArithmeticExpressionOperation<S>> arguments, 
			BasicSchema outputSchema) {
		this.outputSchema = outputSchema;
		this.argumentNames = argumentNames;
		this.arguments = arguments;
		this.name = name;
	}
	
	public Udf(String name, BasicSchema argumentNames, List<ArithmeticExpressionOperation<S>>arguments, 
			BasicSchema outputSchema, JavaUdf<T,R,S> function) {
		this.outputSchema = outputSchema;
		this.arguments = arguments;
		this.argumentNames = argumentNames;
		this.name = name;
		this.function = function;
	}

	public Udf() {
		
	}
	
	@Override
	public String getName() {
		return name;
	}

	public BasicSchema getOutputSchema() {
		return outputSchema;
	}

	public void setOutputSchema(BasicSchema outputSchema) {
		this.outputSchema = outputSchema;
	}

	public boolean isAggregate() {
		return isAggregate;
	}

	public void setAggregate(boolean isAggregate) {
		this.isAggregate = isAggregate;
	}

	public boolean isTableValued() {
		return isTableValued;
	}

	public void setTableValued(boolean isTableValued) {
		this.isTableValued = isTableValued;
	}

	public void setName(String name) {
		this.name = name;
	}

	public JavaUdf<T,R,S> getFunction() {
		return function;
	}

	public void setFunction(JavaUdf<T,R,S> function) {
		this.function = function;
	}

	public List<ArithmeticExpressionOperation<S>> getArguments() {
		return arguments;
	}

	public void setArguments(List<ArithmeticExpressionOperation<S>> arguments) {
		this.arguments = arguments;
	}

	public BasicSchema getArgumentNames() {
		return argumentNames;
	}

	public void setArgumentNames(BasicSchema argumentNames) {
		this.argumentNames = argumentNames;
	}

	public String toString() {
		return getName() + "(" + getArguments().toString() + "): " + outputSchema;
	}
}
