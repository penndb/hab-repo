/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;
import java.util.OptionalLong;
import java.util.Set;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.type.provdm.QualifiedName;
import edu.upenn.cis.db.habitat.core.type.provdm.QualifiedNameField;

public class BasicSchema extends Schema<String, Class<? extends Object>> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	BasicSchema() {
		super("");
	}

	public BasicSchema(String name) {
		super(name);
	}

	public BasicSchema(String name, List<String> fields, List<Class<? extends Object>> types) {
		super(name, fields, types);
	}

	@Override
	public BasicTuple createTuple() {
		List<Field<? extends Object>> fields = new ArrayList<Field<? extends Object>>();
		for (int i = 0; i < getKeys().size(); i++) {
			final Class<? extends Object> type = getTypeAt(i);
			addField(type, fields);
		}

		count.incrementAndGet();
		return new BasicTuple(this, fields);
	}

	public BasicTuple createTuple(List<Field<? extends Object>> fields) {
		BasicTuple ret = new BasicTuple(this, fields);
		
		// Pad any additional fields that weren't already set
		for (int i = fields.size(); i < ret.getKeys().size(); i++) {
			final Class<? extends Object> type = getTypeAt(i);
			addField(type, fields);
		}

		count.incrementAndGet();
		return ret;
	}
	
	private void addField(Class<?> type, List<Field<? extends Object>> fields) {
		if (type == Integer.class) {
			fields.add(new IntField(null));
		} else if (type == Long.class || type == OptionalLong.class) {
			fields.add(new LongField(null));
		} else if (type == String.class) {
			fields.add(new StringField(null));
		} else if (type == Double.class || type == OptionalDouble.class) {
			fields.add(new DoubleField(null));
		} else if (type == Boolean.class) {
			fields.add(new BooleanField(null));
		} else if (type == Date.class) {
			fields.add(new DateField(null));
		} else if (ProvToken.class.isAssignableFrom(type)) {
			fields.add(new ProvTokenField());
		} else if (ProvSpecifier.class.isAssignableFrom(type)) {
			fields.add(new ProvSpecifierField());
		} else if (type == QualifiedName.class) {
			fields.add(new QualifiedNameField());
		} else if (Set.class.isAssignableFrom(type)) {
			fields.add(new MultiField<>());
		} else
			throw new UnsupportedOperationException("Do not have constructor for type " + type.toString());
	}
	
	public void writeFields(byte[] array, int startPos, List<Field<? extends Object>> fields) {
		
	}
	
	public BasicSchema rename(Map<String,String> fields) {
		List<String> keys2 = new ArrayList<>();
		List<Class<? extends Object>> types2 = new ArrayList<>();
		
		keys2.addAll(keys);
		types2.addAll(types);
		
		keys2.replaceAll(f -> {
			if (!fields.containsKey(f))
				return f;
			else
				return fields.get(f); 
		});
		
		return new BasicSchema(getName(), keys2, types2);
	}

	@Override
	public BasicSchema project(String name, List<String> projKeys) {
		List<String> keys2 = new ArrayList<>();
		List<Class<? extends Object>> types2 = new ArrayList<>();
		
		for (int i = 0; i < keys.size(); i++) {
			if (projKeys.contains(keys.get(i))) {
				keys2.add(keys.get(i));
				types2.add(types.get(i));
			}
		}
		
		BasicSchema ret = new BasicSchema(name, keys2, types2);

		// If all of the lookup keys are preserved by the projection
		// then we still have a key
		boolean keysPreserved = true;
		List<Integer> lookup2 = new ArrayList<>();
		for (Integer i: lookupKey) {
			if (!projKeys.contains(keys.get(i)))
				keysPreserved = false;
			else
				lookup2.add(keys2.indexOf(keys.get(i)));
		}
		if (keysPreserved)
			ret.lookupKey.addAll(lookup2);
		
		return ret;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();

		builder.append(getName());
		builder.append('(');
		for (int i = 0; i < keys.size(); i++) {
			if (i > 0)
				builder.append(',');
			
			builder.append(keys.get(i).toString());
			builder.append(':');
			builder.append(types.get(i).getSimpleName());
		}
		builder.append(')');
		
		return builder.toString();
	}

	@Override
	public BasicSchema augment(String name, List<String> fields, List<Class<? extends Object>> types) {
		List<String> keys2 = new ArrayList<>();
		List<Class<? extends Object>> types2 = new ArrayList<>();
		
		keys2.addAll(keys);
		types2.addAll(this.types);
		keys2.addAll(fields);
		types2.addAll(types);
		
		BasicSchema ret = new BasicSchema(name, keys2, types2);

		List<Integer> lookup2 = new ArrayList<>();
		if (!lookupKey.isEmpty()) {
			lookup2.addAll(lookupKey);
			for (String f: fields)
				lookup2.add(keys2.indexOf(f));
			ret.lookupKey = lookup2;
		}
		return ret;
	}

	public BasicSchema concat(String name, BasicSchema rs) {
		List<String> newKeys = new ArrayList<>();
		List<Class<? extends Object>> newTypes = new ArrayList<>();
		
		newKeys.addAll(keys);
		newTypes.addAll(types);
		for (int i = 0; i < rs.getKeys().size(); i++) {
			String k= rs.getKeys().get(i);

			// Don't concatenate 
			if ((newKeys.contains(ProvenanceApi.Provenance) && k.equals(ProvenanceApi.Provenance)) || 
					(newKeys.contains(ProvenanceApi.Token) && k.equals(ProvenanceApi.Token)))
				continue;
			
			if (newKeys.contains(k)) {
				newKeys.add(k + "_2");
				newTypes.add(rs.getTypes().get(i));
			} else {
				newKeys.add(k);
				newTypes.add(rs.getTypes().get(i));
			}
		}

		BasicSchema ret = new BasicSchema(name, newKeys, newTypes);
		return ret;
	}

}
