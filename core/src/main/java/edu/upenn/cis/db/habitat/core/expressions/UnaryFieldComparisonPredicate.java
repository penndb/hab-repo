/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.core.expressions;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.db.habitat.core.expressions.PredicateOperations.ComparisonOperation;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;

/**
 * A comparison predicate evaluated over fields and/or literals in a single input
 * tuple
 * 
 * @author ZacharyIves
 *
 */
public class UnaryFieldComparisonPredicate extends UnaryTuplePredicateWithLookup {
	String fieldName;
	PredicateOperations.ComparisonOperation op;
	String field2;
	Object literal;

	/**
	 * Comparison of field with object value
	 * 
	 * @param fieldName
	 * @param comparison
	 * @param target
	 */
	public UnaryFieldComparisonPredicate(String fieldName, 
			PredicateOperations.ComparisonOperation comparison, Object target){
		
		this.fieldName = fieldName;
		this.op = comparison;
		
		literal = target;
	}

	/**
	 * Null test
	 * 
	 * @param fieldName
	 * @param comparison
	 */
	public UnaryFieldComparisonPredicate(String fieldName, 
			PredicateOperations.ComparisonOperation comparison) {
		
		this.fieldName = fieldName;
		this.op = comparison;
		
		if (op != ComparisonOperation.NotNull && op != ComparisonOperation.IsNull)
			throw new UnsupportedOperationException();
	}

	/**
	 * Comparison of field (left arg) with string (which must be quoted if it's
	 * a literal, else it's a second field name).
	 * Supports URL-encoded strings for representing special characters.
	 * 
	 * @param fieldName
	 * @param comparison
	 * @param target
	 * @throws UnsupportedEncodingException
	 */
	public UnaryFieldComparisonPredicate(String fieldName, 
			PredicateOperations.ComparisonOperation comparison, String target) throws UnsupportedEncodingException {
		
		this.fieldName = fieldName;
		this.op = comparison;
		
		// If it's quoted, then treat it as a string literal
		if (target.startsWith("'") || target.startsWith("\"")) {
			literal = URLDecoder.decode(target.substring(1, target.length() - 1), "UTF-8");
		} else
			field2 = target;
	}

	/**
	 * Comparison of field (left arg) with string (which must be quoted if it's
	 * a literal, else it's a second field name).
	 * Supports URL-encoded strings for representing special characters.
	 * 
	 * @param fieldName
	 * @param comparison
	 * @param target
	 * @throws UnsupportedEncodingException
	 */
	public UnaryFieldComparisonPredicate(String fieldName, 
			PredicateOperations.ComparisonOperation comparison, String target, boolean isString) throws UnsupportedEncodingException {
		
		this.fieldName = fieldName;
		this.op = comparison;
		
		// If it's quoted, then treat it as a string literal
		if (target.startsWith("'") || target.startsWith("\"")) {
			literal = URLDecoder.decode(target.substring(1, target.length() - 1), "UTF-8");
		} else if (isString)
			literal = target;
		else
			field2 = target;
	}
	@Override
	public boolean test(TupleWithSchema<String> t) {
		Object left = this.getValue(fieldName);
		Object right;
		if (literal != null)
			right = literal;
		else if (field2 != null)
			right = this.getValue(field2);
		
		// This should only happen for IsNull / NotNull
		else
			right = null;
		
		switch (op) {
		case Equal:
			// Null is incomparable with null
			return left != null && right != null && left.equals(right);
		case NotEqual:
			// Null is incomparable with null
			return left != null && right != null && !left.equals(right);
			
		case NotNull:
			return left != null;
		case IsNull:
			return left == null;
			
			
		case Greater:
			{
				Comparable cLeft = (Comparable)left;
				Comparable cRight = (Comparable)right;
				
				// Make the left + right the same type
				if (cLeft != null && cRight != null && cLeft instanceof Double && cRight instanceof Integer)
					cRight = new Double((Integer)cRight);
				
				if (cLeft != null && cRight != null && cLeft instanceof Integer && cRight instanceof Double)
					cRight = ((Double)cRight).intValue();
				
				// Null is incomparable with null
				return cLeft != null && cRight != null && cLeft.compareTo(cRight) > 0;
			}
		case GreaterEqual:
			{
				Comparable cLeft = (Comparable)left;
				Comparable cRight = (Comparable)right;

				// Make the left + right the same type
				if (cLeft != null && cRight != null && cLeft instanceof Double && cRight instanceof Integer)
					cRight = new Double((Integer)cRight);
				
				if (cLeft != null && cRight != null && cLeft instanceof Integer && cRight instanceof Double)
					cRight = ((Double)cRight).intValue();
				// Null is incomparable with null
				return cLeft != null && cRight != null && cLeft.compareTo(cRight) >= 0;
			}
		case Less:
			{
				Comparable cLeft = (Comparable)left;
				Comparable cRight = (Comparable)right;

				// Make the left + right the same type
				if (cLeft != null && cRight != null && cLeft instanceof Double && cRight instanceof Integer)
					cRight = new Double((Integer)cRight);
				
				if (cLeft != null && cRight != null && cLeft instanceof Integer && cRight instanceof Double)
					cRight = ((Double)cRight).intValue();
				// Null is incomparable with null
				return cLeft != null && cRight != null && cLeft.compareTo(cRight) < 0;
			}
		case LessEqual:
			{
				Comparable cLeft = (Comparable)left;
				Comparable cRight = (Comparable)right;

				// Make the left + right the same type
				if (cLeft != null && cRight != null && cLeft instanceof Double && cRight instanceof Integer)
					cRight = new Double((Integer)cRight);
				
				if (cLeft != null && cRight != null && cLeft instanceof Integer && cRight instanceof Double)
					cRight = ((Double)cRight).intValue();
				// Null is incomparable with null
				return cLeft != null && cRight != null && cLeft.compareTo(cRight) <= 0;
			}
		}

		return false;
	}

	@Override
	public String toString() {
		if (literal == null && field2 == null)
			return PredicateOperations.getStandardName(op) + "(" + fieldName + ")";
		else if (literal != null)
			return fieldName + " " + PredicateOperations.getStandardName(op)  + " '" + literal.toString() + "'";
		else 
			return fieldName + " " + PredicateOperations.getStandardName(op)  + " (" + field2 + ")";
	}

	@Override
	public Set<String> getSymbolsReferenced() {
		Set<String> ret = new HashSet<String>();
		ret.add(fieldName);
		if (field2 != null && !field2.isEmpty() && field2.charAt(0) != '\'' && field2.charAt(0) != '\"')
			ret.add(field2);
		
		return ret;
	}

	@Override
	public void rename(Map<String, String> symbolTable) {
		if (symbolTable.containsKey(fieldName))
			fieldName = symbolTable.get(fieldName);

		if (field2 != null && symbolTable.containsKey(field2))
			field2= symbolTable.get(field2);
	}

	public PredicateOperations.ComparisonOperation getOp() {
		return op;
	}

	public void setOp(PredicateOperations.ComparisonOperation op) {
		this.op = op;
	}
	
	
}
