/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.model;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.transform;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;

import edu.upenn.cis.db.habitat.core.type.BinaryProvExpression;
import edu.upenn.cis.db.habitat.core.type.NaryProvExpression;
import edu.upenn.cis.db.habitat.core.type.ProvExpression;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier.ProvOperation;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * @author John Frommeyer
 *
 */
@ApiModel(description = "A prov expression",
		parent = ProvSpecifierModel.class)
public class ProvExpressionModel implements ProvSpecifierModel {

	public final static String OPERATION_PROP_NAME = "operation";
	public final static String OPERANDS_PROP_NAME = "operands";

	@ApiModelProperty(name = OPERATION_PROP_NAME, required = true)
	public final String operation;

	@ApiModelProperty(name = OPERANDS_PROP_NAME)
	public final List<ProvSpecifierModel> operands = new ArrayList<>();

	public ProvExpressionModel(
			String operation,
			List<ProvSpecifierModel> operands) {
		this.operation = checkNotNull(operation);
		this.operands.addAll(operands);
	}

	@Override
	public ProvExpression toProvSpecifier() throws HabitatServiceException {
		final ProvOperation operation = ProvOperation.valueOf(this.operation);
		final List<ProvSpecifier> operands = new ArrayList<>();
		for (final ProvSpecifierModel operand : this.operands) {
			operands.add(operand.toProvSpecifier());
		}
		if (operands.size() == 2) {
			return new BinaryProvExpression(operation, operands.get(0),
					operands.get(1));
		} else {
			return new NaryProvExpression(operation, operands);
		}
	}

	public static ProvExpressionModel from(ProvExpression provExpression) {
		final ProvOperation operation = provExpression.getOperation();
		final List<ProvSpecifier> operands = new ArrayList<>();
		if (provExpression instanceof BinaryProvExpression) {
			final BinaryProvExpression binaryOp = (BinaryProvExpression) provExpression;
			operands.add(binaryOp.getLeft());
			operands.add(binaryOp.getRight());
		} else if (provExpression instanceof NaryProvExpression) {
			final NaryProvExpression nOp = (NaryProvExpression) provExpression;
			operands.addAll(nOp.getChildren());
		} else {
			throw new IllegalArgumentException("unknown ProvExpression type: ["
					+ provExpression.getClass() + "]");
		}
		final List<ProvSpecifierModel> operandModels = transform(operands,
				ProvSpecifierModel::from);
		return new ProvExpressionModel(operation.name(), operandModels);
	}
}
