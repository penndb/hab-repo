/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import com.google.common.collect.ImmutableList;

public class SubgraphInstance {
	public static class NodeInstance {
		private final String id;
		private final TupleWithSchemaModel tuple;

		public NodeInstance(String id, TupleWithSchemaModel tuple) {
			this.id = checkNotNull(id);
			this.tuple = checkNotNull(tuple);
		}

		public String getId() {
			return id;
		}

		public TupleWithSchemaModel getTuple() {
			return tuple;
		}

		@Override
		public String toString() {
			return "NodeInstance [id=" + id + ", tuple=" + tuple + "]";
		}

	}

	public static class LinkInstance {
		private final String sourceId;
		private final String targetId;
		private final String type;
		private final TupleWithSchemaModel tuple;

		public LinkInstance(String sourceId, 
				String targetId,
				String type,
				TupleWithSchemaModel tuple) {
			this.sourceId = checkNotNull(sourceId);
			this.targetId = checkNotNull(targetId);
			this.type = checkNotNull(type);
			this.tuple = checkNotNull(tuple);
		}

		public String getSourceId() {
			return sourceId;
		}

		public String getTargetId() {
			return targetId;
		}
		
		public String getType() {
			return type;
		}

		public TupleWithSchemaModel getTuple() {
			return tuple;
		}

		@Override
		public String toString() {
			return "LinkInstance [sourceId=" + sourceId + ", targetId="
					+ targetId + ", type=" + type + ", tuple=" + tuple + "]";
		}

	}

	public static class RankInstance {
		public final List<NodeInstance> nodes;

		public RankInstance(List<NodeInstance> nodes) {
			this.nodes = ImmutableList.copyOf(nodes);
		}

		@Override
		public String toString() {
			return "RankInstance [nodes=" + nodes + "]";
		}

	}

	private final List<RankInstance> ranks;
	private final List<LinkInstance> links;
	private final long maxTimestamp;

	public SubgraphInstance(List<RankInstance> ranks,
			List<LinkInstance> links,
			long maxTimestamp) {
		this.ranks = ImmutableList.copyOf(ranks);
		this.links = ImmutableList.copyOf(links);
		checkArgument(maxTimestamp >= 0);
		this.maxTimestamp = maxTimestamp;
	}

	public List<RankInstance> getRanks() {
		return ranks;
	}

	public List<LinkInstance> getLinks() {
		return links;
	}

	/**
	 * Returns the creation time of the youngest node in this subgraph in
	 * milliseconds since Epoch. Only those nodes which correspond to a
	 * {@code useSince: true} node in the corresponding template.
	 * 
	 * @return the creation time of the youngest node in this subgraph in
	 *         milliseconds since Epoch
	 */
	public long getMaxTimestamp() {
		return maxTimestamp;
	}

	@Override
	public String toString() {
		return "SubgraphInstance [ranks=" + ranks + ", links=" + links + "]";
	}
}