/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.model;

import static com.google.common.base.Preconditions.checkArgument;
import edu.upenn.cis.db.habitat.core.type.ID;
import edu.upenn.cis.db.habitat.core.type.PlanBasedID;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * @author John Frommeyer
 *
 */
public class IDModel {

	public final String id;

	public IDModel(String id) {
		checkArgument(id != null && !id.trim().isEmpty(),
				"ID must be not be null or empty");
		this.id = id;
	}

	public ID toID() throws HabitatServiceException {
		final String[] split = id.split("\\.");
		if (split.length != 2) {
			throw new HabitatServiceException("Invalid ID format: [" + id
					+ "]. ID format should be string.integer.");
		}
		final String prefix = split[0].trim();
		try {
			final int count = Integer.valueOf(split[1]);
			return new PlanBasedID(prefix, count);
		} catch (NumberFormatException e) {
			throw new HabitatServiceException("Invalid ID format: [" + id
					+ "]. ID format should be string.integer.");
		}

	}

	public static IDModel from(ID id) {
		checkArgument(id instanceof PlanBasedID,
				"Can only convert PlanBasedIDs");
		final PlanBasedID planId = (PlanBasedID) id;
		return new IDModel(planId.getName());
	}

}
