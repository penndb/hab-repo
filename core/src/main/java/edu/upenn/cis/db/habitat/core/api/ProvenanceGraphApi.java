/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.api;

import java.util.List;

import javax.annotation.Nullable;

import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate;
import edu.upenn.cis.db.habitat.core.webservice.HabitatSecurityException;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * Extension of ProvenanceApi with ability to create graph.
 * 
 * @author John Frommeyer
 *
 */
public interface ProvenanceGraphApi extends ProvenanceApi {
	/**
	 * If resourceName is available, creates a new graph, and assigns admin
	 * permissions to the logged-in user
	 * 
	 * @param resourceName Name of the graph resource
	 * @throws HabitatServiceException if resourceName is null, empty, or too
	 *             long
	 * @throws HabitatSecurityException if resourceName is not available
	 */
	void createProvenanceGraph(String resourceName)
			throws HabitatServiceException;

	/**
	 * If resourceName is available, creates a new graph, and assigns admin
	 * permissions to the logged-in user. If resourceName is taken but user is
	 * admin, clears the graph
	 * 
	 * @param resourceName Name of the graph resource
	 * @throws HabitatServiceException if resourceName is null, empty, or too
	 *             long
	 * @throws HabitatSecurityException if resourceName is not available
	 */
	void createOrResetProvenanceGraph(String resourceName)
			throws HabitatServiceException;

	/**
	 * Returns a representation of the graph {@code resourceName} as a list of
	 * subgraphs with the same shape as {@code template}. The list will contain
	 * no more than {@code limit} subgraphs. Any nodes corresponding to
	 * variables marked {@code useSince} in the template will be "older" than
	 * {@code since}. If {@code since} is null, a representation with
	 * {@code maxTimestamp} set to an appropriate value will be returned.
	 * 
	 * @param resourceName
	 * @param template
	 * @param limit
	 * @param since
	 * @return a representation of the graph {@code resourceName} as a list of
	 *         subgraphs
	 * @throws HabitatServiceException
	 */
	List<SubgraphInstance> getSubgraphs(
			String resourceName,
			SubgraphTemplate template,
			int limit,
			@Nullable Long since) throws HabitatServiceException;

	void storeSubgraph(String resourceName, SubgraphInstance subgraph)
			throws HabitatServiceException;

	void storeSubgraphTemplate(String resourceName, SubgraphTemplate template)
			throws HabitatServiceException;

	SubgraphTemplate getSubgraphTemplate(String resourceName)
			throws HabitatServiceException;
}
