package edu.upenn.cis.db.habitat.core.type;

import java.io.Serializable;

/**
 * A basic metadata element, including a schema
 * but potentially also indices, keys, constraints
 * 
 * @author zives
 *
 */
public interface Metadata extends Serializable {
	public String getName();
}
