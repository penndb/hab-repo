/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.model;

import static java.util.Collections.emptyList;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Arrays;
import java.util.Collection;

import edu.upenn.cis.db.habitat.core.type.ID;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * @author John Frommeyer
 *
 */
@ApiModel(description = "A prov location expression",
		parent = ProvSpecifierModel.class)
public class ProvLocationModel implements ProvSpecifierModel {

	public final static String STREAM_PROP_NAME = "stream";
	public final static String FIELD_PROP_NAME = "field";
	public final static String POSITION_PROP_NAME = "position";

	@ApiModelProperty(name = STREAM_PROP_NAME)
	public final IDModel stream;

	@ApiModelProperty(name = FIELD_PROP_NAME, required = true)
	public final String field;

	@ApiModelProperty(name = POSITION_PROP_NAME, required = true)
	public final Integer[] position;

	public ProvLocationModel(
			IDModel stream,
			String field,
			Integer[] position) {
		this.stream = stream;
		this.field = field;
		this.position = position;
	}

	@Override
	public ProvLocation toProvSpecifier() throws HabitatServiceException {
		final ID streamID = stream == null ? null : stream.toID();
		final Collection<Integer> positionCollection = position == null ? emptyList()
				: Arrays.asList(position);
		return new ProvLocation(streamID, field, positionCollection);
	}

	public static ProvLocationModel from(ProvLocation provLocation) {
		final ID otherStream = provLocation.getStream();
		final String otherField = provLocation.getField();
		final Collection<Integer> otherPosition = provLocation.getPosition();
		final ProvLocationModel provLocationModel = otherStream == null ? new ProvLocationModel(
				null,
				otherField,
				otherPosition.toArray(new Integer[0]))
				:
				new ProvLocationModel(
						IDModel.from(otherStream),
						otherField,
						otherPosition.toArray(new Integer[0]));
		return provLocationModel;
	}
}
