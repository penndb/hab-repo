/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.core.type;

public class PlanBasedID implements ID {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	final String name;
	
	public PlanBasedID() {
		name = "";
	}
	
	public PlanBasedID(String prefix, int count) {
		name = prefix + "." + count;
	}

	@Override
	public int compareTo(ID o) {
		if (o instanceof PlanBasedID) {
			return name.compareTo(((PlanBasedID)o).name);
		} else
			return name.toString().compareTo(o.toString());
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof PlanBasedID) {
			return equals2((PlanBasedID)o) && ((PlanBasedID)o).equals2(this);
		} else
			return false;
	}
	
	boolean equals2(PlanBasedID o) {
		return name.equals(o.name);
	}
	
	@Override
	public int hashCode() {
		return name.hashCode();
	}
	
	@Override
	public String toString() {
		return name;
	}

	public String getName() {
		return name;
	}

}
