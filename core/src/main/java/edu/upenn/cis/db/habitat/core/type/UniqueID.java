/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.core.type;

import java.util.UUID;

public class UniqueID implements ID {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	final UUID u;
	
	public UniqueID() {
		u = UUID.randomUUID();
	}

	@Override
	public int compareTo(ID o) {
		if (o instanceof UniqueID) {
			return u.compareTo(((UniqueID)o).u);
		} else
			return u.toString().compareTo(o.toString());
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof UniqueID) {
			return equals2((UniqueID)o) && ((UniqueID)o).equals2(this);
		} else
			return false;
	}
	
	boolean equals2(UniqueID o) {
		return u.equals(o.u);
	}
	
	@Override
	public int hashCode() {
		return u.hashCode();
	}
	
	@Override
	public String toString() {
		return u.toString();
	}
}
