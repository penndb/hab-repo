/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.provdm;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * @author John Frommeyer
 *
 */
public class Namespace {

	private final URI iri;

	@SuppressWarnings("unused")
	// For Jackson
	private Namespace() {
		iri = null;
	}

	public Namespace(String namespaceIri) {
		try {
			iri = new URI(namespaceIri);
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}
	}

	public URI getIri() {
		return iri;
	}

	@Override
	public String toString() {
		return "{" + iri + "}";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((iri == null) ? 0 : iri.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Namespace)) {
			return false;
		}
		Namespace other = (Namespace) obj;
		if (iri == null) {
			if (other.iri != null) {
				return false;
			}
		} else if (!iri.equals(other.iri)) {
			return false;
		}
		return true;
	}

}
