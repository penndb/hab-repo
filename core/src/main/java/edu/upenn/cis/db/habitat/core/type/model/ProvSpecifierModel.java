/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.model;

import static com.google.common.base.Preconditions.checkNotNull;
import io.swagger.annotations.ApiModel;
import edu.upenn.cis.db.habitat.core.type.ProvExpression;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * @author John Frommeyer
 *
 */
@ApiModel(description = "Either a ProvTokenModel, ProvLocationModel, or ProvExpressionModel",
		subTypes = { ProvLocationModel.class, ProvTokenModel.class, ProvExpressionModel.class })
public interface ProvSpecifierModel {

	ProvSpecifier toProvSpecifier()
			throws HabitatServiceException;

	public static ProvSpecifierModel from(ProvSpecifier specifier) {
		checkNotNull(specifier);

		if (specifier instanceof ProvToken) {
			return ProvTokenModel.from((ProvToken) specifier);
		}

		if (specifier instanceof ProvLocation) {
			return ProvLocationModel.from((ProvLocation) specifier);
		}
		
		if (specifier instanceof ProvExpression) {
			return ProvExpressionModel.from((ProvExpression) specifier);
		}

		throw new IllegalArgumentException("unknown ProvSpecifier type: ["
				+ specifier.getClass() + "]");

	}

}
