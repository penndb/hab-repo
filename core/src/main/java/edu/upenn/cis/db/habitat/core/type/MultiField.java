/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 * @author John Frommeyer
 * @param <T>
 *
 */
public class MultiField<T> implements Field<Set<T>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Set<T> value;

	MultiField() {}

	public MultiField(Set<T> value) {
		this.value = new HashSet<>(checkNotNull(value));
	}

	@SuppressWarnings("unchecked")
	@Override
	@JsonIgnore
	public Class<Set<T>> getType() {
		return (Class<Set<T>>) value.getClass();
	}

	@Override
	public Set<T> getValue() {
		return value;
	}

	@Override
	public void setValue(Set<T> value) {
		this.value = new HashSet<>(checkNotNull(value));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof MultiField)) {
			return false;
		}
		MultiField<?> other = (MultiField<?>) obj;
		if (value == null) {
			if (other.value != null) {
				return false;
			}
		} else if (!value.equals(other.value)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		if (getValue() != null)
			return getValue().toString();
		else
			return "(null)";
	}
}
