/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.provdm;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import com.google.gson.Gson;

/**
 * 
 * @author John Frommeyer
 *
 */
public class QualifiedName implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Gson gson = new Gson();

	private final String namespace;
	private final String localPart;

	@SuppressWarnings("unused")
	// For Jackson
	private QualifiedName() {
		namespace = null;
		localPart = null;
	}

	public QualifiedName(String namespace, String localPart) {
		checkNotNull(namespace);
		this.namespace = namespace.trim();
		checkArgument(!this.namespace.isEmpty());
		checkNotNull(localPart);
		this.localPart = localPart.trim();
		checkArgument(!this.localPart.isEmpty());
	}

	public String getNamespace() {
		return namespace;
	}

	public String getLocalPart() {
		return localPart;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((localPart == null) ? 0 : localPart.hashCode());
		result = prime * result
				+ ((namespace == null) ? 0 : namespace.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof QualifiedName)) {
			return false;
		}
		QualifiedName other = (QualifiedName) obj;
		if (localPart == null) {
			if (other.localPart != null) {
				return false;
			}
		} else if (!localPart.equals(other.localPart)) {
			return false;
		}
		if (namespace == null) {
			if (other.namespace != null) {
				return false;
			}
		} else if (!namespace.equals(other.namespace)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "{" + this.namespace + "}" + localPart;
	}

	public static QualifiedName from(String name) {
		final String msgFormat = "illegal argument: [%s]: argument must be of form {namespace}localPart as returned by QualifiedName.toString()";
		checkArgument(
				name.startsWith("{") && name.contains("}"),
				msgFormat,
				name);
		final String[] split = name.split("}");
		if (split.length == 2) {
			final String namespace = split[0].substring(1);
			return new QualifiedName(namespace, split[1]);
		} else {
			final QualifiedName qn = gson.fromJson(name, QualifiedName.class);
			return qn;
		}
	}
}
