/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ProvLocation implements ProvSpecifier {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	final static Set<Integer> emptyColl = new HashSet<>();
	
	ID stream;
	String field;
	Collection<Integer> position;
	
	String cache = null;
	
	public ProvLocation() {
		super();
		this.stream = null;
		field = "";
		position = emptyColl;
	}
	
	public ProvLocation(ID stream, String field, Collection<Integer> position) {
		super();
		this.stream = stream;
		this.field = field;
		this.position = position;
	}
	public ProvLocation(String field, Collection<Integer> position) {
		super();
		this.stream = null;
		this.field = field;
		this.position = position;
	}
	
	public ProvLocation(String field, Integer position) {
		super();
		this.stream = null;
		this.field = field;
		this.position = new ArrayList<>();
		this.position.add(position);
	}

	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public Collection<Integer> getPosition() {
		return position;
	}
	public void setPosition(Collection<Integer> position) {
		this.position = position;
	}
	
	
	public ID getStream() {
		return stream;
	}
	public void setStream(ID stream) {
		this.stream = stream;
	}
	@Override
	public String toString() {
		if (cache != null)
			return cache;
		
		
		if (stream != null)
			cache = stream + "." + field + ":" + position;
		else
			cache = field + ":" + position;
		
		return cache;
	}

	@Override
	public int compareTo(ProvSpecifier o) {
		return toString().compareTo(o.toString());
	}

	@JsonIgnore
	@Override
	public StructuredData<String, Object> getTuple() {
		StructuredData<String, Object> ret = new StructuredValue<>();
		ret.put("location", this);
		return ret;
	}
}
