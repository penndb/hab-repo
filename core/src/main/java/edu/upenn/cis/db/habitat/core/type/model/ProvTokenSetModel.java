/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.model;

import static com.google.common.base.Preconditions.checkNotNull;
import io.swagger.annotations.ApiModelProperty;

import java.util.HashSet;
import java.util.Set;

import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * @author John Frommeyer
 *
 */
public class ProvTokenSetModel {

	@ApiModelProperty(required = true)
	public final Set<ProvTokenModel> tokens = new HashSet<>();

	public ProvTokenSetModel(Set<ProvTokenModel> tokens) {
		this.tokens.addAll(tokens);
	}

	public Set<ProvToken> toProvTokens() throws HabitatServiceException {
		final Set<ProvToken> provTokens = new HashSet<>();
		for (final ProvTokenModel model : tokens) {
			provTokens.add(model.toProvSpecifier());
		}
		return provTokens;
	}

	public static ProvTokenSetModel from(Iterable<ProvToken> tokens) {
		checkNotNull(tokens);
		final Set<ProvTokenModel> models = new HashSet<>();
		tokens.forEach(token -> {
			models.add(ProvTokenModel.from(token));
		});
		return new ProvTokenSetModel(models);
	}

	@Override
	public String toString() {
		return "ProvTokenSetModel [tokens=" + tokens + "]";
	}
	
}
