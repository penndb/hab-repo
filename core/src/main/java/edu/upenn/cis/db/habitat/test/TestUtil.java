/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.test;

import static com.google.common.collect.Lists.newArrayList;
import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;

import edu.upenn.cis.db.habitat.core.api.ProvenanceGraphApi;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.BooleanField;
import edu.upenn.cis.db.habitat.core.type.DoubleField;
import edu.upenn.cis.db.habitat.core.type.Field;
import edu.upenn.cis.db.habitat.core.type.LongField;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifierField;
import edu.upenn.cis.db.habitat.core.type.ProvTokenField;
import edu.upenn.cis.db.habitat.core.type.ProvUniqueToken;
import edu.upenn.cis.db.habitat.core.type.Schema;
import edu.upenn.cis.db.habitat.core.type.StringField;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.model.ProvLocationModel;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance.LinkInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance.NodeInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance.RankInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate.LinkInfo;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate.NodeInfo;
import edu.upenn.cis.db.habitat.core.type.model.TupleWithSchemaModel;
import edu.upenn.cis.db.habitat.core.type.provdm.Attribute;
import edu.upenn.cis.db.habitat.core.type.provdm.BooleanAttribute;
import edu.upenn.cis.db.habitat.core.type.provdm.DoubleAttribute;
import edu.upenn.cis.db.habitat.core.type.provdm.LongAttribute;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvDmConstants;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvQualifiedNameToken;
import edu.upenn.cis.db.habitat.core.type.provdm.QualifiedName;
import edu.upenn.cis.db.habitat.core.type.provdm.QualifiedNameAttribute;
import edu.upenn.cis.db.habitat.core.type.provdm.QualifiedNameField;
import edu.upenn.cis.db.habitat.core.type.provdm.StringAttribute;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * @author John Frommeyer
 *
 */
public class TestUtil {

	private static final Random rnd = new Random();

	public static final String TEST_NAMESPACE = "http://example.com/";

	public static final QualifiedName newQualifiedName() {
		return new QualifiedName(TEST_NAMESPACE, UUID.randomUUID().toString());
	}

	public static void populateTestFields(List<String> fieldNames,
			List<Field<?>> fields) {

		final ImmutableList<String> testNames = ImmutableList.of(
				"boolean_field",
				"double__field",
				"long_field1",
				"long_field2",
				"string_field",
				"prov_token_field",
				"prov_specifier_field",
				"qualified_name_field");
		fieldNames.addAll(testNames);

		final String uuid = UUID.randomUUID().toString();
		final ImmutableList<Field<?>> testFields = ImmutableList.of(
				new BooleanField(rnd.nextBoolean()),
				new DoubleField(rnd.nextDouble()),
				new LongField((long) rnd.nextInt()),
				new LongField(1000L + Integer.MAX_VALUE),
				new StringField(UUID.randomUUID().toString()),
				new ProvTokenField(new ProvUniqueToken()),
				new ProvSpecifierField(
						new ProvLocation(
								uuid + ".source", newArrayList(
										rnd.nextInt(), rnd.nextInt(),
										rnd.nextInt()))),
				new QualifiedNameField(new QualifiedName(
						TEST_NAMESPACE, UUID.randomUUID().toString())));
		fields.addAll(testFields);
	}

	public static BasicTuple newTestTuple(
			List<String> fieldNames,
			List<Field<?>> fields) {
		populateTestFields(fieldNames, fields);
		final List<Class<?>> fieldTypes = fields.stream()
				.map((Field<?> field) -> field.getType())
				.collect(Collectors.toList());

		final String uuid = UUID.randomUUID().toString();
		final BasicSchema testSchema = new BasicSchema(
				"TEST_SCHEMA_" + uuid,
				fieldNames,
				fieldTypes);
		testSchema.addLookupKeyAt(rnd.nextInt(fields.size()));
		final BasicTuple testTuple = testSchema.createTuple(fields);
		return testTuple;
	}

	public static BasicTuple newTestTuple() {
		return newTestTuple(new ArrayList<>(), new ArrayList<>());
	}

	public static List<Attribute> newTestAttributeList() {
		final String uuid = UUID.randomUUID().toString();
		final QualifiedName booleanAttr = new QualifiedName(
				TEST_NAMESPACE,
				"boolean_field_" + uuid);
		final QualifiedName doubleAttr = new QualifiedName(
				TEST_NAMESPACE,
				"double_field_" + uuid);
		final QualifiedName longAttr1 = new QualifiedName(
				TEST_NAMESPACE,
				"long_attr1_" + uuid);
		final QualifiedName longAttr2 = new QualifiedName(
				TEST_NAMESPACE,
				"long_attr2_" + uuid);
		final QualifiedName stringAttr = new QualifiedName(
				TEST_NAMESPACE,
				"string_attr_" + uuid);
		final QualifiedName qnAttr = new QualifiedName(
				TEST_NAMESPACE,
				"qualified_name_attr_" + uuid);

		final ImmutableList<Attribute> fields = ImmutableList.of(
				new BooleanAttribute(booleanAttr, rnd.nextBoolean()),
				new DoubleAttribute(doubleAttr, rnd.nextDouble()),
				new LongAttribute(longAttr1, (long) rnd.nextInt()),
				new LongAttribute(longAttr2, 1000L + Integer.MAX_VALUE),
				new StringAttribute(stringAttr, UUID.randomUUID().toString()),
				new QualifiedNameAttribute(qnAttr, new QualifiedName(
						TEST_NAMESPACE, UUID.randomUUID()
								.toString())));
		return fields;
	}

	public static ProvLocation newTestLocation() {
		final Collection<Integer> position = newArrayList(rnd.nextInt(),
				rnd.nextInt());
		final ProvLocation location = new ProvLocation(
				UUID.randomUUID().toString(),
				position);
		return location;
	}

	public static ProvLocationModel newTestLocationModel() {
		return ProvLocationModel.from(newTestLocation());
	}

	public static TupleWithSchemaModel newTestTupleWithSchemaModel() {
		return TupleWithSchemaModel.from(newTestTuple());
	}

	public static void assertProvLocationsEqual(ProvLocation expected,
			ProvLocation actual) {
		assertEquals(expected.getStream(), actual.getStream());
		assertEquals(expected.getField(), actual.getField());
		assertEquals(expected.getPosition(), actual.getPosition());
	}

	public static void assertProvLocationsEqual(ProvLocation expected,
			ProvSpecifier actualSpecifer) {
		assertTrue(actualSpecifer instanceof ProvLocation);
		final ProvLocation actual = (ProvLocation) actualSpecifer;
		assertProvLocationsEqual(expected, actual);
	}

	public static void assertTuplesEquals(
			TupleWithSchema<String> expectedTuple,
			TupleWithSchema<String> actualTuple) {
		final Schema<String, Class<? extends Object>> expectedSchema = expectedTuple
				.getSchema();
		final Schema<String, Class<? extends Object>> actualSchema = actualTuple
				.getSchema();

		assertEquals(expectedSchema.getArity(), actualSchema.getArity());
		assertEquals(expectedSchema.getName(), actualSchema.getName());
		assertEquals(expectedSchema.getTypes(), actualSchema.getTypes());
		assertEquals(expectedSchema.getKeys(), actualSchema.getKeys());
		assertEquals(expectedSchema.getLookupKeys(),
				actualSchema.getLookupKeys());

		assertEquals(expectedTuple.getFields().size(), actualTuple.getFields()
				.size());
		for (int i = 0; i < expectedTuple.getFields().size(); i++) {
			final Field<? extends Object> expectedField = expectedTuple
					.getFields().get(i);
			final Field<? extends Object> actualField = actualTuple.getFields()
					.get(i);
			assertFieldsEqual(expectedField, actualField);
		}
	}

	public static void assertFieldsEqual(Field<? extends Object> expectedField,
			Field<? extends Object> actualField) {
		assertEquals(expectedField.getType(), actualField.getType());
		if (expectedField.getValue() instanceof ProvLocation) {
			// No .equals for ProvLocation
			final ProvLocation expectedLocation = (ProvLocation) expectedField
					.getValue();
			final ProvLocation actualLocation = (ProvLocation) actualField
					.getValue();
			assertEquals(expectedLocation.getField(),
					actualLocation.getField());
			assertEquals(expectedLocation.getPosition(),
					actualLocation.getPosition());
			assertEquals(expectedLocation.getStream(),
					actualLocation.getStream());
		} else {
			assertEquals(expectedField.getValue(), actualField.getValue());
		}
	}

	public static void assertSubgraphInstancesEqual(
			List<SubgraphInstance> expectedInstances,
			List<SubgraphInstance> actualInstances)
			throws HabitatServiceException {

		assertEquals(expectedInstances.size(), actualInstances.size());
		for (int i = 0; i < expectedInstances.size(); i++) {
			final SubgraphInstance expectedInstance = expectedInstances
					.get(i);
			final SubgraphInstance actualInstance = actualInstances
					.get(i);
			assertEquals(expectedInstance.getRanks().size(),
					actualInstance.getRanks().size());
			for (int r = 0; r < expectedInstance.getRanks().size(); r++) {
				final RankInstance expectedRank = expectedInstance
						.getRanks().get(r);
				final RankInstance actualRank = actualInstance
						.getRanks().get(r);
				assertEquals(expectedRank.nodes.size(),
						actualRank.nodes.size());
				for (int n = 0; n < expectedRank.nodes.size(); n++) {
					final NodeInstance expectedNode = expectedRank.nodes
							.get(n);
					final NodeInstance actualNode = actualRank.nodes
							.get(n);
					assertEquals(expectedNode.getId(),
							actualNode.getId());
					assertTuplesEquals(
							expectedNode.getTuple()
									.toTupleWithSchema(),
							actualNode.getTuple()
									.toTupleWithSchema());
				}
			}
		}
	}

	/**
	 * Returns a {@link SubgraphInstance} conforming to the passed in template.
	 * 
	 * @param template
	 * @param nodeInstanceLookup a map of template ids to the node instance that
	 *            the returned instance should use instead of randomly generated
	 *            node instance.
	 * @return a {@link SubgraphInstance} conforming to the passed in template
	 */
	public static SubgraphInstance templateToTestInstance(
			SubgraphTemplate template,
			Map<String, Optional<NodeInstance>> nodeInstanceLookup) {
		Map<String, Optional<NodeInstance>> templateToNodeInstance = new HashMap<>(
				nodeInstanceLookup);
		final List<RankInstance> ranks = new ArrayList<>();
		final List<LinkInstance> links = new ArrayList<>();
		for (final List<NodeInfo> rank : template.getRanks()) {
			final List<NodeInstance> nodeInstances = new ArrayList<>();
			for (final NodeInfo nodeTemplate : rank) {
				final Optional<NodeInstance> realNode = templateToNodeInstance
						.computeIfAbsent(nodeTemplate.getId(), templateId -> {
							final String realNodeId = newQualifiedName()
									.toString();
							final TupleWithSchemaModel tuple = TupleWithSchemaModel
									.from(newTestTuple(
											newArrayList(
													ProvDmConstants.PROV_DM_TYPE_QN
															.toString()),
											newArrayList(
													new StringField(nodeTemplate
															.getType()))));
							final NodeInstance realNodeValue = new NodeInstance(
									realNodeId, tuple);
							return Optional.of(realNodeValue);
						});
				if (realNode.isPresent()) {
					nodeInstances.add(realNode.get());
				} else {
					// Add nothing. It's an optional node and this is telling us
					// to not include it.
					assertTrue(nodeTemplate.isOptional());
				}

			}
			if (!nodeInstances.isEmpty()) {
				ranks.add(new RankInstance(nodeInstances));
			}
		}

		for (final LinkInfo linkTemplate : template.getLinks()) {
			final String templateSourceId = linkTemplate
					.getSourceId();
			final String templateTargetId = linkTemplate
					.getTargetId();
			final Optional<NodeInstance> sourceInstance = templateToNodeInstance
					.get(templateSourceId);
			final Optional<NodeInstance> targetInstance = templateToNodeInstance
					.get(templateTargetId);
			if (sourceInstance.isEmpty() || targetInstance.isEmpty()) {
				// Can't add link instance involving an optional node which is
				// missing.
				continue;
			}

			final String sourceId = sourceInstance.get()
					.getId();
			final String targetId = targetInstance.get()
					.getId();
			final String linkType = linkTemplate.getType();
			final TupleWithSchemaModel tuple = TupleWithSchemaModel
					.from(newTestTuple());
			links.add(new LinkInstance(sourceId, targetId, linkType, tuple));
		}
		return new SubgraphInstance(ranks, links, 0);
	}

	public static void storeSubgraphInstance(
			final String graph,
			final SubgraphInstance expectedInstance,
			final ProvenanceGraphApi store)
			throws HabitatServiceException {

		for (final RankInstance rankInstance : expectedInstance
				.getRanks()) {
			for (final NodeInstance nodeInstance : rankInstance.nodes) {
				store.storeProvenanceNode(
						graph,
						new ProvQualifiedNameToken(
								QualifiedName.from(nodeInstance
										.getId())),
						nodeInstance.getTuple().toTupleWithSchema(),
						new ProvLocation());
			}
		}

		for (final LinkInstance linkInstance : expectedInstance
				.getLinks()) {

			store.storeProvenanceLink(
					graph,
					new ProvQualifiedNameToken(
							QualifiedName.from(linkInstance
									.getSourceId())),
					linkInstance.getType(),
					new ProvQualifiedNameToken(
							QualifiedName.from(linkInstance
									.getTargetId())),
					linkInstance.getTuple().toTupleWithSchema());
		}
	}

}
