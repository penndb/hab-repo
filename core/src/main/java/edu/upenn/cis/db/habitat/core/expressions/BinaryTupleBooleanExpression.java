/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.core.expressions;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Set;

import edu.upenn.cis.db.habitat.core.expressions.PredicateOperations.BooleanOperation;
import edu.upenn.cis.db.habitat.core.expressions.PredicateOperations.ComparisonOperation;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;

public class BinaryTupleBooleanExpression extends BinaryTuplePredicateWithLookup {
	PredicateOperations.BooleanOperation op;
	BinaryTuplePredicateWithLookup lPred;
	BinaryTuplePredicateWithLookup rPred;

	public BinaryTupleBooleanExpression( 
			BooleanOperation operation, BinaryTuplePredicateWithLookup left, BinaryTuplePredicateWithLookup right){
		
		this.lPred = left;
		this.rPred = right;
		this.op = operation;
		
		if (op == PredicateOperations.BooleanOperation.Not)
			throw new UnsupportedOperationException();
	}

	
	@Override
	public void bind(TupleWithSchema<String> lTuple, TupleWithSchema<String> rTuple) {
		super.bind(lTuple, rTuple);
		lPred.bind(lTuple, rTuple);
		rPred.bind(lTuple, rTuple);
	}

	@Override
	public boolean test(TupleWithSchema<String> lTuple, TupleWithSchema<String> rTuple) {
		Boolean lResult = lPred.test(lTuple, rTuple);
		// Don't evaluate right (1) if we are doing an OR
		// and we can short-circuit, (2) if we are doing an AND and we can short-circuit
		Boolean rResult = (lResult && op == PredicateOperations.BooleanOperation.Or) ||
				(!lResult && op == PredicateOperations.BooleanOperation.And) ?
						null : 
				rPred.test(lTuple, rTuple);
		
		switch (op) {
		case Not:
			return !lResult;
		case And:
			if (rResult == null)
				return lResult;
			else
				return lResult && rResult;
		case Or:
			if (rResult == null)
				return lResult;
			else
				return lResult || rResult;
		case Xor:
			return lResult ^ rResult;
		}

		return false;
	}

	@Override
	public String toString() {
		if (rPred == null)
			return PredicateOperations.getStandardName(op) + "(" + lPred.toString() + ")";
		else
			return "(" + lPred.toString() + ") " + PredicateOperations.getStandardName(op) + " (" + rPred.toString() + ")";
	}

	@Override
	public Set<String> getSymbolsReferencedLeft() {
		Set<String> ret = lPred.getSymbolsReferencedLeft();
		
		if (right != null)
			ret.addAll(rPred.getSymbolsReferencedLeft());
		
		return ret;
	}

	@Override
	public Set<String> getSymbolsReferencedRight() {
		Set<String> ret = lPred.getSymbolsReferencedRight();
		
		if (right != null)
			ret.addAll(rPred.getSymbolsReferencedRight());
		
		return ret;
	}

	public PredicateOperations.BooleanOperation getOperation() {
		return op;
	}
	
	public BinaryTuplePredicateWithLookup getLeft() {
		return lPred;

	}
	
	public BinaryTuplePredicateWithLookup getRight() {
		return rPred;
	}


	@Override
	public void swapLeftAndRight() {
		BinaryTuplePredicateWithLookup pred = lPred;
		lPred = rPred;
		rPred = pred;
	}
}
