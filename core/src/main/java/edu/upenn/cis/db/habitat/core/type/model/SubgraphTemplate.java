/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.model;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.emptyList;

import java.util.List;

import com.google.common.collect.ImmutableList;

public class SubgraphTemplate {
	public static class NodeInfo {
		private final String id;
		private final String type;
		private final boolean useSince;
		private final boolean optional;

		public NodeInfo(String id, String type, boolean useSince) {
			this(id, type, useSince, false);
		}

		public NodeInfo(String id, String type, boolean useSince,
				boolean optional) {
			this.id = checkNotNull(id);
			this.type = checkNotNull(type);
			this.useSince = useSince;
			this.optional = optional;
		}

		public String getId() {
			return id;
		}

		public String getType() {
			return type;
		}

		/**
		 * Returns true if the creation time of this node should be compared to
		 * the {@code since} value in the template.
		 * 
		 * @return true if the creation time of this node should be compared to
		 *         the {@code since} value in the template
		 */
		public boolean useSince() {
			return useSince;
		}

		public boolean getUseSince() {
			return useSince;
		}

		public boolean isOptional() {
			return optional;
		}

		@Override
		public String toString() {
			return "NodeInfo [id=" + id + ", type=" + type + ", useSince="
					+ useSince + ", optional=" + optional + "]";
		}

	}

	public static class LinkInfo {
		private final String sourceId;
		private final String targetId;
		private final String type;

		public LinkInfo(String sourceId, String targetId, String type) {
			this.sourceId = checkNotNull(sourceId);
			this.targetId = checkNotNull(targetId);
			this.type = checkNotNull(type);
		}

		public String getSourceId() {
			return sourceId;
		}

		public String getTargetId() {
			return targetId;
		}

		public String getType() {
			return type;
		}

		@Override
		public String toString() {
			return "LinkInfo [sourceId=" + sourceId + ", targetId=" + targetId
					+ ", type=" + type + "]";
		}

	}

	private final List<List<NodeInfo>> ranks;
	private final List<SubgraphTemplate.LinkInfo> links;
	private final List<String> orderBy;

	public SubgraphTemplate(
			List<List<NodeInfo>> ranks,
			List<SubgraphTemplate.LinkInfo> links,
			List<String> orderBy) {
		this.ranks = ImmutableList.copyOf(ranks);
		this.links = ImmutableList.copyOf(links);
		this.orderBy = ImmutableList.copyOf(orderBy);
	}

	public SubgraphTemplate(
			List<List<NodeInfo>> ranks,
			List<SubgraphTemplate.LinkInfo> links) {
		this(ranks, links, emptyList());
	}

	public List<List<NodeInfo>> getRanks() {
		return ranks;
	}

	public List<SubgraphTemplate.LinkInfo> getLinks() {
		return links;
	}

	public List<String> getOrderBy() {
		return orderBy;
	}

	@Override
	public String toString() {
		return "SubgraphTemplate [ranks=" + ranks + ", links=" + links
				+ ", orderBy=" + orderBy + "]";
	}
}
