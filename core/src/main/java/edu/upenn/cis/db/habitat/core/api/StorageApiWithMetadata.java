/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.api;

import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.TransactionSupplier;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

public interface StorageApiWithMetadata<K,V,B> extends StorageApi<K,V,B> {

	/**
	 * Retrieve node keys by metadata
	 * 
	 * @param metadataField
	 * @param pred
	 * @return
	 */
	public Iterable<K> getKeysMatching(String resource, String metadataField, Map<K,? extends V> match) throws HabitatServiceException;
	
	/**
	 * Transactionally retrieve node keys by metadata
	 * 
	 * @param metadataField
	 * @param pred
	 * @return
	 */
	public Iterable<K> getKeysMatching(TransactionSupplier supplier, String resource, String metadataField, Map<K,? extends V> match) throws HabitatServiceException;

	/**
	 * Get out-edges with associated metadata
	 * 
	 * @param key1
	 * @param metadata
	 */
	public Iterable<Pair<K,StructuredData<K,V>>> getLinksFrom(String resource, K key1) throws HabitatServiceException;
	
	/**
	 * Transactionally get out-edges with associated metadata
	 * 
	 * @param key1
	 * @param metadata
	 */
	Iterable<Pair<K, StructuredData<K,V>>> getLinksFrom(TransactionSupplier supplier, String resource, K key1)
			throws HabitatServiceException;

	/**
	 * Get in-edges with associated metadata
	 * 
	 * @param key1
	 * @param metadata
	 */
	public Iterable<Pair<K,StructuredData<K,V>>> getLinksTo(String resource, K key1) throws HabitatServiceException;
	
	/**
	 * Transactionally get in-edges with associated metadata
	 * 
	 * @param key1
	 * @param metadata
	 */
	public Iterable<Pair<K,StructuredData<K,V>>> getLinksTo(TransactionSupplier supplier, String resource, K key1) throws HabitatServiceException;

	/**
	 * Store an edge with associated metadata
	 * 
	 * @param key1
	 * @param key2
	 * @param metadata
	 */
	public void link(String resource, K key1, String label, K key2, StructuredData<K,? extends V> metadata) throws HabitatServiceException;
	
	/**
	 * Transactionally store an edge with associated metadata
	 * 
	 * @param key1
	 * @param key2
	 * @param metadata
	 */
	public void link(TransactionSupplier supplier, String resource, K key1, String label, K key2, StructuredData<K,? extends V> metadata) throws HabitatServiceException;

	/**
	 * Store an object / node and its metadata
	 * 
	 * @param key
	 * @param refToValue
	 * @param metadata
	 * @throws HabitatServiceException 
	 */
	public void storeReference(String resource, K key, String refToValue, StructuredData<K,? extends V> metadata) throws HabitatServiceException;

	/**
	 * Transactionally store an object / node and its metadata
	 * 
	 * @param key
	 * @param value
	 * @param metadata
	 * @throws HabitatServiceException 
	 */
	public void storeReference(TransactionSupplier supplier, String resource, K key, String refToValue, StructuredData<K,? extends V> metadata) throws HabitatServiceException;
	
}
