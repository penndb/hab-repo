/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import edu.upenn.cis.db.habitat.core.type.provdm.QualifiedName;
import edu.upenn.cis.db.habitat.core.type.provdm.QualifiedNameField;

public class Schema<K,T> implements Metadata {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected  String name;
	
	protected List<K> keys = new ArrayList<>();
	protected List<T> types = new ArrayList<>();
	@JsonProperty
	protected  List<Integer> lookupKey = new ArrayList<>();
	
	Integer ourHash = null; 
	
	static List<Schema<?,?>> schemas = new ArrayList<>();
	protected AtomicInteger count = new AtomicInteger(0);
	
	public Schema(String name, List<K> fields, List<T> types) {
		super();
		this.name = name;
		this.keys.addAll(fields);
		this.types.addAll(types);
		schemas.add(this);
	}
	public Schema(String name) {
		super();
		this.name = name;
		schemas.add(this);
	}
	
	public Schema<K,T> project(String name, List<K> projKeys) {
		List<K> keys2 = new ArrayList<>();
		List<T> types2 = new ArrayList<>();
		
		for (int i = 0; i < keys.size(); i++) {
			if (projKeys.contains(keys.get(i))) {
				keys2.add(keys.get(i));
				types2.add(types.get(i));
			}
		}
		
		Schema<K,T> ret = new Schema<K,T>(name, keys2, types2);

		// If all of the lookup keys are preserved by the projection
		// then we still have a key
		boolean keysPreserved = true;
		List<Integer> lookup2 = new ArrayList<>();
		for (Integer i: lookupKey) {
			if (!projKeys.contains(keys.get(i)))
				keysPreserved = false;
			else
				lookup2.add(keys2.indexOf(keys.get(i)));
		}
		if (keysPreserved)
			ret.lookupKey.addAll(lookup2);
		
		return ret;
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Schema))
			return false;
		
		Schema<K,T> second = (Schema<K,T>)o;
		
		if (getArity() != second.getArity())
			return false;
		
		if (!keys.equals(second.keys))
			return false;
		
		if (!types.equals(second.types))
			return false;

		if (!lookupKey.equals(second.lookupKey))
			return false;

		return true;
	}
	
	@Override
	public int hashCode() {
		if (ourHash != null)
			return ourHash;
		else {
			ourHash = name.hashCode() % keys.hashCode();
			return ourHash;
		}
	}

	public void addField(K key, T type) {
		keys.add(key);
		types.add(type);
	}
	
	public int indexOf(K key) {
		return keys.indexOf(key);
	}
	
	public List<K> getKeys() {
		return keys;
	}
	public K getKeyAt(int inx) {
		return keys.get(inx);
	}
	public void setKeys(List<K> fields) {
		this.keys.clear();
		this.keys.addAll(fields);
	}
	public void setKeyAt(int inx, K field) {
		this.keys.set(inx, field);
	}
	
	public List<T> getTypes() {
		return types;
	}
	public T getTypeAt(int inx) {
		return types.get(inx);
	}
	public void setTypes(List<T> types) {
		this.types.clear();
		this.types.addAll(types);
	}
	public void setTypeAt(int inx, T type) {
		this.types.set(inx, type);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@JsonIgnore
	// This is handled by annotating lookupKey list of ints. 
	// Otherwise Jackson can't deserialize.
	public List<K> getLookupKeys() {
		List<K> ret = new ArrayList<>();
		for (Integer i: lookupKey)
			ret.add(keys.get(i));
		
		return ret;
	}
	
	public void setLookupKeys(List<K> keys) {
		this.lookupKey.clear();
		for (K key: keys)
			addLookupKey(key);
	}
	
	public void addLookupKey(K key) {
		if (keys.contains(key)) {
			lookupKey.add(keys.indexOf(key));
		} else
			throw new RuntimeException("Unable to find field " + key.toString());
	}

	public void addLookupKeyAt(int i) {
		lookupKey.add(i);
	}
	
	public BasicTupleWithSchema<K> createTuple() {
		List<Field<?>> fields = new ArrayList<>();
		for (int i = 0; i < getKeys().size(); i++) {
			if (getTypeAt(i) == Integer.class) {
				fields.add(new IntField(null));
			} else if (getTypeAt(i) == Long.class) {
				fields.add(new LongField(null));
			} else if (getTypeAt(i) == String.class) {
				fields.add(new StringField(null));
			} else if (getTypeAt(i) == Double.class) {
				fields.add(new DoubleField(null));
			} else if (getTypeAt(i) == Boolean.class) {
				fields.add(new BooleanField(null));
			} else if (getTypeAt(i) == ProvToken.class) {
				fields.add(new ProvTokenField());
			} else if (getTypeAt(i) == ProvSpecifier.class || getTypeAt(i) == ProvLocation.class) {
				fields.add(new ProvSpecifierField());
			} else if (getTypeAt(i) == QualifiedName.class) {
				fields.add(new QualifiedNameField());
			} else if (getTypeAt(i) == HashSet.class) {
				fields.add(new MultiField<>());
			} else
				throw new UnsupportedOperationException("Do not have constructor for type " + getTypeAt(i).toString());
		}

		count.incrementAndGet();
		return new BasicTupleWithSchema<K>((Schema<K, Class<? extends Object>>) this, fields);
	}
	
	public String toCSV() {
		StringBuilder builder = new StringBuilder();
		
		for (int i = 0; i < keys.size(); i++) {
			if (i > 0)
				builder.append(',');
			
			builder.append(keys.get(i).toString());
		}
		
		return builder.toString();
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();

		builder.append(getName());
		builder.append('(');
		for (int i = 0; i < keys.size(); i++) {
			if (i > 0)
				builder.append(',');
			
			builder.append(keys.get(i).toString());
			builder.append(':');
			builder.append(types.get(i).toString());
		}
		builder.append(')');
		
		return builder.toString();
	}
	
	@JsonIgnore
	public int getArity() {
		return keys.size();
	}
	public Schema<K,T> augment(String name, List<K> fields, List<T> types) {
		List<K> keys2 = new ArrayList<>();
		List<T> types2 = new ArrayList<>();
		
		keys2.addAll(keys);
		types2.addAll(this.types);
		keys2.addAll(fields);
		types2.addAll(types);
		
		Schema<K,T> ret = new Schema<K,T>(name, keys2, types2);

		List<Integer> lookup2 = new ArrayList<>();
		if (!lookupKey.isEmpty()) {
			lookup2.addAll(lookupKey);
			for (K f: fields)
				lookup2.add(keys2.indexOf(f));
			ret.lookupKey = lookup2;
		}
		return ret;
	}
	
	@JsonIgnore
	public int getTuplesProduced() {
		return count.get();
	}
	
	public static List<Schema<?,?>> getAllSchemas() {
		return Schema.schemas;
	}

	public boolean remove(K field) {
		int inx = keys.indexOf(field);
		
		if (inx >= 0) {
			keys.remove(inx);
			types.remove(inx);
			
			// Remove the index position from the lookup
			// key if it is there!
			Integer index = inx;
			lookupKey.remove(index);
			return true;
		} else
			return false;
	}

}
