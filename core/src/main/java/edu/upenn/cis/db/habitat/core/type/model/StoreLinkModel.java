/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.singletonList;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

import com.google.common.collect.ImmutableList;

import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;

/**
 * @author John Frommeyer
 *
 */
public class StoreLinkModel {

	@ApiModelProperty(required = true)
	public final List<ProvTokenModel> from;

	@ApiModelProperty(required = true)
	public final ProvTokenModel to;

	@ApiModelProperty
	public final String label;

	@ApiModelProperty
	public final TupleWithSchemaModel tupleWithSchema;

	/**
	 * Creates a body for the storeLink methods. {@code label} and
	 * {@code tupleWithSchema} cannot both be null.
	 * 
	 * @param from a non-empty list of tokens
	 * @param to a non-null token
	 * @param label
	 */
	public StoreLinkModel(
			List<ProvTokenModel> from,
			ProvTokenModel to,
			String label) {
		this.from = checkNotNull(from);
		checkArgument(!from.isEmpty());
		this.to = checkNotNull(to);
		this.label = checkNotNull(label);
		this.tupleWithSchema = null;
	}

	/**
	 * Creates a body for the storeLink methods. {@code label} and
	 * {@code tupleWithSchema} cannot both be null.
	 * 
	 * @param from a non-empty list of tokens
	 * @param to a non-null token
	 * @param tupleWithSchema
	 */
	public StoreLinkModel(
			List<ProvTokenModel> from,
			ProvTokenModel to,
			TupleWithSchemaModel tupleWithSchema) {
		this.from = checkNotNull(from);
		checkArgument(!from.isEmpty());
		this.to = checkNotNull(to);
		this.label = null;
		this.tupleWithSchema = checkNotNull(tupleWithSchema);
	}

	/**
	 * Creates a body for the storeLink methods. {@code label} and
	 * {@code tupleWithSchema} cannot both be null.
	 * 
	 * @param from a non-empty list of tokens
	 * @param to a non-null token
	 * @param tupleWithSchema
	 */
	public StoreLinkModel(
			List<ProvTokenModel> from,
			String label,
			ProvTokenModel to,
			TupleWithSchemaModel tupleWithSchema) {
		this.from = checkNotNull(from);
		checkArgument(!from.isEmpty());
		this.to = checkNotNull(to);
		this.label = label;
		this.tupleWithSchema = checkNotNull(tupleWithSchema);
	}

	public static StoreLinkModel from(ProvToken from, ProvToken to, String label) {
		return new StoreLinkModel(
				singletonList(ProvTokenModel.from(from)),
				ProvTokenModel.from(to),
				label);
	}

	public static StoreLinkModel from(ProvToken from, String label, ProvToken to,
			TupleWithSchema<String> tuple) {
		return new StoreLinkModel(
				singletonList(ProvTokenModel.from(from)),
				label,
				ProvTokenModel.from(to),
				TupleWithSchemaModel.from(tuple));
	}

	public static StoreLinkModel from(ProvToken fromLeft, ProvToken fromRight,
			ProvToken to, String label) {
		return new StoreLinkModel(
				ImmutableList.of(
						ProvTokenModel.from(fromLeft),
						ProvTokenModel.from(fromRight)),
				ProvTokenModel.from(to),
				label);
	}

	public static StoreLinkModel from(ProvToken fromLeft, ProvToken fromRight,
			String label,
			ProvToken to, TupleWithSchema<String> tuple) {
		return new StoreLinkModel(
				ImmutableList.of(
						ProvTokenModel.from(fromLeft),
						ProvTokenModel.from(fromRight)),
				label,
				ProvTokenModel.from(to),
				TupleWithSchemaModel.from(tuple));
	}
	
	public static StoreLinkModel from(List<ProvToken> from, ProvToken to, String label) {
		return new StoreLinkModel(
				ProvTokenModel.from(from),
				ProvTokenModel.from(to),
				label);
	}

	public static StoreLinkModel from(List<ProvToken> from, String label, ProvToken to,
			TupleWithSchema<String> tuple) {
		return new StoreLinkModel(
				ProvTokenModel.from(from),
				label,
				ProvTokenModel.from(to),
				TupleWithSchemaModel.from(tuple));
	}

}
