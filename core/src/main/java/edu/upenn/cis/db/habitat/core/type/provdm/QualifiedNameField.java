/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.provdm;

import com.fasterxml.jackson.annotation.JsonIgnore;

import edu.upenn.cis.db.habitat.core.type.Field;

public class QualifiedNameField implements Field<QualifiedName> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	QualifiedName value;

	public QualifiedNameField() {}

	public QualifiedNameField(QualifiedName value) {
		this.value = value;
	}

	@JsonIgnore
	@Override
	public Class<QualifiedName> getType() {
		return QualifiedName.class;
	}

	@Override
	public QualifiedName getValue() {
		return value;
	}

	public void setValue(QualifiedName value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof QualifiedNameField)) {
			return false;
		}
		QualifiedNameField other = (QualifiedNameField) obj;
		if (value == null) {
			if (other.value != null) {
				return false;
			}
		} else if (!value.equals(other.value)) {
			return false;
		}
		return true;
	}


	@Override
	public String toString() {
		if (getValue() != null)
			return getValue().toString();
		else
			return "(null)";
	}
}
