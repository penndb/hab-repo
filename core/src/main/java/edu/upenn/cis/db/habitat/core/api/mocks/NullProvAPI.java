/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.api.mocks;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

public class NullProvAPI implements ProvenanceApi {
	final static Logger logger = LogManager.getLogger(NullProvAPI.class);

	@Override
	public void storeProvenanceNode(String resource, ProvToken token, ProvSpecifier location) {
	}

	@Override
	public ProvSpecifier getProvenanceLocation(String resource, ProvToken token) {
		return null;
	}

	@Override
	public void storeProvenanceLink(String resource, ProvToken from, String label, ProvToken to) {
	}

	@Override
	public Set<ProvToken> getConnectedTo(String resource, ProvToken to) {
		return null;
	}

	@Override
	public Set<ProvToken> getConnectedFrom(String resource, ProvToken from) {
		return null;
	}

	@Override
	public Set<ProvToken> getConnectedTo(String resource, ProvToken to, String label) {
		return null;
	}

	@Override
	public Set<ProvToken> getConnectedFrom(String resource, ProvToken from, String label) {
		return null;
	}

	@Override
	public void storeProvenanceLink(String resource, ProvToken fromLeft, ProvToken fromRight, String label, ProvToken to) {
	}

	@Override
	public void storeProvenanceLink(String resource, List<ProvToken> inputProv, String label, ProvToken to) {
	}

	@Override
	public void storeProvenanceNode(String resource, ProvToken token, TupleWithSchema<String> tuple, ProvSpecifier location) {
	}

	@Override
	public TupleWithSchema<String> getProvenanceData(String resource, ProvToken token) {
		return null;
	}

	@Override
	public void storeProvenanceLink(String resource, ProvToken from, String label,
			ProvToken to, TupleWithSchema<String> tuple) {
	}

	@Override
	public void storeProvenanceLink(String resource, ProvToken fromLeft,
			ProvToken fromRight, String label, ProvToken to, 
			TupleWithSchema<String> tuple) {
	}

	@Override
	public void storeProvenanceLink(String resource, List<ProvToken> inputProv, String label,
			ProvToken to,
			TupleWithSchema<String> tuple) throws HabitatServiceException {
	}

	@Override
	public Iterable<TupleWithSchema<String>> getEdgesTo(String resource, ProvToken to) {
		return null;
	}

	@Override
	public Iterable<TupleWithSchema<String>> getEdgesFrom(String resource, ProvToken from) {
		return null;
	}

	@Override
	public Map<String, StructuredData<String, Object>> getProvenanceNodes(String resource)
			throws HabitatServiceException {
		return null;
	}

	@Override
	public Map<String, List<TupleWithSchema<String>>> getEdgeMapFrom(String resource, ProvToken from) throws HabitatServiceException {
		return null;
	}

	@Override
	public Map<String, List<TupleWithSchema<String>>> getEdgeMapTo(String resource, ProvToken to) throws HabitatServiceException {
		return null;
	}

}
