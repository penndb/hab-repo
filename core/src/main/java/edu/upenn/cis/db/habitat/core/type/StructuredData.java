/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type;

import java.util.Map;
import java.util.function.Function;

/**
 * Basic interface to structured data, which is a map from keys (attribute names) to values.
 * See StructuredValue as the main implementation.
 * 
 * @author zives
 *
 * @param <K>
 * @param <V>
 */
public interface StructuredData<K,V> extends Map<K, V> {
	public <V2> StructuredData<K,V2> createConverted(Function<V,V2> converter);
}
