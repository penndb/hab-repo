/*
 * Copyright 2016 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.provdm;

import java.util.EnumSet;
import java.util.Map;

import com.google.common.collect.ImmutableMap;

/**
 * @author John Frommeyer
 *
 */
public enum ProvDmRelation {

	GENERATION(
			"generation",
			"wasGeneratedBy",
			EnumSet.of(AdditionalProperty.ATTRIBUTES,
					AdditionalProperty.ID,
					AdditionalProperty.TIME)),
	USAGE(
			"usage",
			"used",
			EnumSet.of(AdditionalProperty.ATTRIBUTES,
					AdditionalProperty.ID,
					AdditionalProperty.TIME)),
	COMMUNICATION(
			"communication",
			"wasInformedBy",
			EnumSet.of(AdditionalProperty.ATTRIBUTES,
					AdditionalProperty.ID)),
	START(
			"start",
			"wasStartedBy",
			EnumSet.of(AdditionalProperty.ATTRIBUTES,
					AdditionalProperty.ID,
					AdditionalProperty.TIME,
					AdditionalProperty.SECONDARY_OBJECT)),
	END(
			"end",
			"wasEndedBy",
			EnumSet.of(AdditionalProperty.ATTRIBUTES,
					AdditionalProperty.ID,
					AdditionalProperty.TIME,
					AdditionalProperty.SECONDARY_OBJECT)),
	INVALIDATION(
			"invalidation",
			"wasInvalidatedBy",
			EnumSet.of(AdditionalProperty.ATTRIBUTES,
					AdditionalProperty.ID,
					AdditionalProperty.TIME)),
	DERIVATION(
			"derivation",
			"wasDerivedFrom",
			EnumSet.of(AdditionalProperty.ATTRIBUTES,
					AdditionalProperty.ID,
					AdditionalProperty.SECONDARY_OBJECT,
					AdditionalProperty.GENERATION_ID,
					AdditionalProperty.USAGE_ID)),
	/*
	 * REVISION( "revision", null, Kind.PROV_DERIVATION), QUOTATION(
	 * "quotation", null, Kind.PROV_DERIVATION), PRIMARY_SOURCE(
	 * "primary source", null, Kind.PROV_DERIVATION),
	 */
	ATTRIBUTION(
			"attribution",
			"wasAttributedTo",
			EnumSet.of(AdditionalProperty.ID,
					AdditionalProperty.ATTRIBUTES)),
	ASSOCIATION(
			"association",
			"wasAssociatedWith",
			EnumSet.of(AdditionalProperty.ID,
					AdditionalProperty.ATTRIBUTES,
					AdditionalProperty.SECONDARY_OBJECT)),
	DELEGATION(
			"delegation",
			"actedOnBehalfOf",
			EnumSet.of(AdditionalProperty.ID,
					AdditionalProperty.ATTRIBUTES,
					AdditionalProperty.SECONDARY_OBJECT)),
	INFLUENCE(
			"influence",
			"wasInfluencedBy",
			EnumSet.of(AdditionalProperty.ID,
					AdditionalProperty.ATTRIBUTES)),
	SPECIALIZATION(
			"specialization",
			"specializationOf",
			EnumSet.noneOf(AdditionalProperty.class)),
	ALTERNATE(
			"alternate",
			"alternateOf",
			EnumSet.noneOf(AdditionalProperty.class)),
	ANNOTATED(
			"_annotated",
			"_annotated",
			EnumSet.noneOf(AdditionalProperty.class)),
	MEMBERSHIP(
			"membership",
			"hadMember",
			EnumSet.noneOf(AdditionalProperty.class));

	public static final Map<String, ProvDmRelation> PROV_DM_NAME_TO_RELATION;

	static {
		final ImmutableMap.Builder<String, ProvDmRelation> builder = ImmutableMap
				.builder();
		for (final ProvDmRelation type : ProvDmRelation.values()) {
			builder.put(type.provDmName, type);
		}
		PROV_DM_NAME_TO_RELATION = builder.build();
	}

	private final String provDmName;
	private final String provNRelation;
	private final EnumSet<AdditionalProperty> additionalProperties;

	public static final EnumSet<ProvDmRelation> ENTITY_RELATIONS = EnumSet.of(
			DERIVATION,
			ALTERNATE,
			SPECIALIZATION,
			USAGE,
			START,
			END,
			GENERATION,
			INVALIDATION,
			ATTRIBUTION,
			ASSOCIATION,
			MEMBERSHIP);
	public static final EnumSet<ProvDmRelation> AGENT_RELATIONS = EnumSet.of(
			DELEGATION,
			ATTRIBUTION,
			ASSOCIATION);

	public static enum AdditionalProperty {
		ATTRIBUTES, ID, SECONDARY_OBJECT, TIME, GENERATION_ID, USAGE_ID;
	}

	private ProvDmRelation(
			String provDmName,
			String provNRelation,
			EnumSet<AdditionalProperty> additionalProperties) {
		this.provDmName = provDmName;
		this.provNRelation = provNRelation;
		this.additionalProperties = additionalProperties;
	}

	public String getProvDmName() {
		return provDmName;
	}

	public String getProvNRelation() {
		return provNRelation;
	}

	public EnumSet<AdditionalProperty> getAdditionalProperties() {
		return additionalProperties;
	}

	public boolean hasAdditionalProperty(AdditionalProperty prop) {
		return additionalProperties.contains(prop);
	}

}
