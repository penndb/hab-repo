/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.api;

import java.util.Date;
import java.util.List;

import javax.annotation.Nullable;

import edu.upenn.cis.db.habitat.core.type.model.ProvSpecifierModel;
import edu.upenn.cis.db.habitat.core.type.provdm.Attribute;
import edu.upenn.cis.db.habitat.core.type.provdm.NodeModel;
import edu.upenn.cis.db.habitat.core.type.provdm.QualifiedName;
import edu.upenn.cis.db.habitat.core.type.provdm.RelationModel;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * @author John Frommeyer
 *
 */
public interface ProvDmApi {

	void createProvenanceGraph(String graphName) throws HabitatServiceException;

	void createOrResetProvenanceGraph(String string) throws HabitatServiceException;

	void storeNode(String graph,
			QualifiedName provId,
			NodeModel nodeModel)
			throws HabitatServiceException;
	
	List<Attribute> getNodeAnnotations(String graph,
			QualifiedName provId)
					throws HabitatServiceException;

	List<QualifiedName> getNodesFrom(String graph,
			QualifiedName provId, String edgeLabel)
					throws HabitatServiceException;

	List<QualifiedName> getNodesTo(String graph,
			QualifiedName provId, String edgeLabel)
					throws HabitatServiceException;

	void storeRelation(String graph,
			RelationModel relationModel,
			String label)
			throws HabitatServiceException;

	void entity(String graph,
			QualifiedName provId,
			List<Attribute> attributes,
			@Nullable ProvSpecifierModel location)
			throws HabitatServiceException;

	void entity(String graph,
			QualifiedName provId,
			@Nullable ProvSpecifierModel location)
			throws HabitatServiceException;

	void collection(String graph,
			QualifiedName provId,
			List<Attribute> attributes,
			@Nullable ProvSpecifierModel location)
			throws HabitatServiceException;

	void collection(String graph,
			QualifiedName provId,
			@Nullable ProvSpecifierModel location)
			throws HabitatServiceException;

	void activity(String graph,
			QualifiedName provId,
			List<Attribute> attributes,
			@Nullable Date startTime,
			@Nullable Date endTime,
			@Nullable ProvSpecifierModel location)
			throws HabitatServiceException;

	void activity(String graph,
			QualifiedName provId,
			@Nullable Date startTime,
			@Nullable Date endTime,
			@Nullable ProvSpecifierModel location)
			throws HabitatServiceException;

	void agent(String graph,
			QualifiedName provId,
			List<Attribute> attributes,
			@Nullable ProvSpecifierModel location)
			throws HabitatServiceException;

	void agent(String graph,
			QualifiedName provId,
			@Nullable ProvSpecifierModel location)
			throws HabitatServiceException;

	void wasGeneratedBy(String graph,
			QualifiedName entityId,
			QualifiedName activityId,
			@Nullable QualifiedName relationId,
			@Nullable Date generationTime,
			List<Attribute> attributes)
			throws HabitatServiceException;

	void wasGeneratedBy(String graph,
			QualifiedName entityId,
			QualifiedName activityId,
			@Nullable QualifiedName relationId,
			@Nullable Date generationTime)
			throws HabitatServiceException;

	void used(String graph,
			QualifiedName activityId,
			QualifiedName entityId,
			@Nullable QualifiedName relationId,
			@Nullable Date usageTime,
			List<Attribute> attributes)
			throws HabitatServiceException;

	void used(String graph,
			QualifiedName activityId,
			QualifiedName entityId,
			@Nullable QualifiedName relationId,
			@Nullable Date usageTime)
			throws HabitatServiceException;

	void wasInformedBy(String graph,
			QualifiedName informedActivityId,
			QualifiedName informantActivityId,
			@Nullable QualifiedName relationId,
			List<Attribute> attributes)
			throws HabitatServiceException;

	void wasInformedBy(String graph,
			QualifiedName informedActivityId,
			QualifiedName informantActivityId,
			@Nullable QualifiedName relationId)
			throws HabitatServiceException;

	void wasStartedBy(String graph,
			QualifiedName startedActivityId,
			QualifiedName triggerEntityId,
			@Nullable QualifiedName relationId,
			@Nullable Date time,
			List<Attribute> attributes)
			throws HabitatServiceException;

	void wasStartedBy(String graph,
			QualifiedName startedActivityId,
			QualifiedName triggerEntityId,
			@Nullable QualifiedName relationId,
			@Nullable Date time)
			throws HabitatServiceException;

	void wasStartedBy(String graph,
			QualifiedName startedActivityId,
			QualifiedName triggerEntityId,
			@Nullable QualifiedName starterActivityId,
			@Nullable QualifiedName relationId,
			@Nullable Date time,
			List<Attribute> attributes)
			throws HabitatServiceException;

	void wasStartedBy(String graph,
			QualifiedName startedActivityId,
			QualifiedName triggerEntityId,
			@Nullable QualifiedName starterActivityId,
			@Nullable QualifiedName relationId,
			@Nullable Date time)
			throws HabitatServiceException;

	void wasEndedBy(String graph,
			QualifiedName endedActivityId,
			QualifiedName triggerEntityId,
			@Nullable QualifiedName relationId,
			@Nullable Date time,
			List<Attribute> attributes)
			throws HabitatServiceException;

	void wasEndedBy(String graph,
			QualifiedName endedActivityId,
			QualifiedName triggerEntityId,
			@Nullable QualifiedName relationId,
			@Nullable Date time)
			throws HabitatServiceException;

	void wasEndedBy(String graph,
			QualifiedName endedActivityId,
			QualifiedName triggerEntityId,
			@Nullable QualifiedName enderActivityId,
			@Nullable QualifiedName relationId,
			@Nullable Date time,
			List<Attribute> attributes)
			throws HabitatServiceException;

	void wasEndedBy(String graph,
			QualifiedName endedActivityId,
			QualifiedName triggerEntityId,
			@Nullable QualifiedName enderActivityId,
			@Nullable QualifiedName relationId,
			@Nullable Date time)
			throws HabitatServiceException;

	void wasInvalidatedBy(String graph,
			QualifiedName invalidatedEntityId,
			QualifiedName invalidatorActivityId,
			@Nullable QualifiedName relationId,
			@Nullable Date time,
			List<Attribute> attributes)
			throws HabitatServiceException;

	void wasInvalidatedBy(String graph,
			QualifiedName invalidatedEntityId,
			QualifiedName invalidatorActivityId,
			@Nullable QualifiedName relationId,
			@Nullable Date time)
			throws HabitatServiceException;

	void wasDerivedFrom(String graph,
			QualifiedName generatedEntityId,
			QualifiedName usedEntityId,
			@Nullable QualifiedName activityId,
			@Nullable QualifiedName relationId,
			@Nullable QualifiedName generationId,
			@Nullable QualifiedName usageId,
			List<Attribute> attributes)
			throws HabitatServiceException;

	void wasDerivedFrom(String graph,
			QualifiedName generatedEntityId,
			QualifiedName usedEntityId,
			@Nullable QualifiedName activityId,
			@Nullable QualifiedName relationId,
			@Nullable QualifiedName generationId,
			@Nullable QualifiedName usageId)
			throws HabitatServiceException;

	void wasDerivedFrom(String graph,
			QualifiedName generatedEntityId,
			QualifiedName usedEntityId,
			@Nullable QualifiedName relationId,
			@Nullable QualifiedName generationId,
			@Nullable QualifiedName usageId,
			List<Attribute> attributes)
			throws HabitatServiceException;

	void wasDerivedFrom(String graph,
			QualifiedName generatedEntityId,
			QualifiedName usedEntityId,
			@Nullable QualifiedName relationId,
			@Nullable QualifiedName generationId,
			@Nullable QualifiedName usageId)
			throws HabitatServiceException;

	void wasAttributedTo(String graph,
			QualifiedName entityId,
			QualifiedName agentId,
			@Nullable QualifiedName relationId,
			List<Attribute> attributes)
			throws HabitatServiceException;

	void wasAttributedTo(String graph,
			QualifiedName entityId,
			QualifiedName agentId,
			@Nullable QualifiedName relationId)
			throws HabitatServiceException;

	void wasAssociatedWith(String graph,
			QualifiedName activityId,
			QualifiedName agentId,
			@Nullable QualifiedName planId,
			@Nullable QualifiedName relationId,
			List<Attribute> attributes)
			throws HabitatServiceException;

	void wasAssociatedWith(String graph,
			QualifiedName activityId,
			QualifiedName agentId,
			@Nullable QualifiedName planId,
			@Nullable QualifiedName relationId)
			throws HabitatServiceException;

	void wasAssociatedWith(String graph,
			QualifiedName activityId,
			QualifiedName agentId,
			@Nullable QualifiedName relationId,
			List<Attribute> attributes)
			throws HabitatServiceException;

	void wasAssociatedWith(String graph,
			QualifiedName activityId,
			QualifiedName agentId,
			@Nullable QualifiedName relationId)
			throws HabitatServiceException;

	void actedOnBehalfOf(String graph,
			QualifiedName delegateAgentId,
			QualifiedName responsibleAgentId,
			@Nullable QualifiedName activityId,
			@Nullable QualifiedName relationId,
			List<Attribute> attributes)
			throws HabitatServiceException;

	void actedOnBehalfOf(String graph,
			QualifiedName delegateAgentId,
			QualifiedName responsibleAgentId,
			@Nullable QualifiedName activityId,
			@Nullable QualifiedName relationId)
			throws HabitatServiceException;

	void actedOnBehalfOf(String graph,
			QualifiedName delegateAgentId,
			QualifiedName responsibleAgentId,
			@Nullable QualifiedName relationId,
			List<Attribute> attributes)
			throws HabitatServiceException;

	void actedOnBehalfOf(String graph,
			QualifiedName delegateAgentId,
			QualifiedName responsibleAgentId,
			@Nullable QualifiedName relationId)
			throws HabitatServiceException;

	void wasInfluencedBy(String graph,
			QualifiedName influenceeId,
			QualifiedName influencerId,
			@Nullable QualifiedName relationId,
			List<Attribute> attributes)
			throws HabitatServiceException;

	void wasInfluencedBy(String graph,
			QualifiedName influenceeId,
			QualifiedName influencerId,
			@Nullable QualifiedName relationId)
			throws HabitatServiceException;

	void specializationOf(String graph,
			QualifiedName specificEntity, QualifiedName genericEntity)
			throws HabitatServiceException;

	void alternateOf(String graph,
			QualifiedName alternate1,
			QualifiedName alternate2)
			throws HabitatServiceException;

	void hadMember(String graph,
			QualifiedName collection, QualifiedName entity)
			throws HabitatServiceException;

	void annotateNode(String graph, QualifiedName sourceId, QualifiedName annotId, QualifiedName key, String value)
			throws HabitatServiceException;

}
