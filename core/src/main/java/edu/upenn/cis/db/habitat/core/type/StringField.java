/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class StringField implements Field<String> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String value;
	
	StringField() { }
	
	public StringField(String value) {
		this.value = value;
	}

	@JsonIgnore
	@Override
	public Class<String> getType() {
		return String.class;
	}

	@Override
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	@Override
	public int hashCode() {
		return getValue().hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof Field)
			return getValue().equals(((Field<?>)o).getValue().toString());
		else
			return getValue().equals(o);
	}

	@Override
	public String toString() {
		if (getValue() != null)
			return getValue().toString();
		else
			return "(null)";
	}
}
