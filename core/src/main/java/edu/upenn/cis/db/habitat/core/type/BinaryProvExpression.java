/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class BinaryProvExpression extends ProvExpression {
	ProvSpecifier left;
	ProvSpecifier right;
	
	String cache = null;
	
	BinaryProvExpression() {
		super(ProvOperation.PROV_SUM);
		left = null;
		right = null;
	}

	public BinaryProvExpression(ProvOperation operation, ProvSpecifier left, ProvSpecifier right) {
		super(operation);
		setLeft(left);
		setRight(right);
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ProvSpecifier getLeft() {
		return left;
	}

	public void setLeft(ProvSpecifier left) {
		this.left = left;
	}

	public ProvSpecifier getRight() {
		return right;
	}

	public void setRight(ProvSpecifier right) {
		this.right = right;
	}

	@Override
	public String toString() {
		if (cache != null)
			return cache;
		
		StringBuilder builder = new StringBuilder();
		
		builder.append('(');
		
		builder.append(left.toString());
		switch (getOperation()) {
		case PROV_MULT:
			builder.append(" (*) ");
			break;
		case PROV_SUM:
			builder.append(" (+) ");
			break;
		case PROV_COMP:
			builder.append(" (o) ");
			break;
			
		default:
			throw new UnsupportedOperationException("Unknown operation type");
		}
		builder.append(right.toString());
		
		builder.append(')');
		
		cache = builder.toString();
		
		return cache;
	}

	@Override
	public int compareTo(ProvSpecifier o) {
		return toString().compareTo(o.toString());
	}

	@JsonIgnore
	@Override
	public StructuredData<String, Object> getTuple() {
		StructuredData<String, Object> ret = new StructuredValue<>();
		ret.put("location", this);
		return ret;
	}
}
