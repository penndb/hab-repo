package edu.upenn.cis.db.habitat.core.expressions;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;

public class BinaryArithmeticExpressionOperation<T> implements ArithmeticExpressionOperation<T> {
	Class<T> returnType;
	ArithmeticOperation op;
	List<ArithmeticExpressionOperation<?>> children = new ArrayList<>();
	

	public BinaryArithmeticExpressionOperation(Class<T> returnType, ArithmeticOperation op, ArithmeticExpressionOperation<?> left, ArithmeticExpressionOperation<?> right) {
		children.add(left);
		children.add(right);
		this.op = op;
		this.returnType = returnType;
	}


	@Override
	public void bind(TupleWithSchema<String> tuple) {
		children.get(0).bind(tuple);
		children.get(1).bind(tuple);
	}

	@Override
	public T getValue() {
		Object leftValue = children.get(0).getValue();
		Object rightValue = children.get(0).getValue();
		switch (op) {
		case Add:
			if (returnType == Integer.class) {
				Integer left = ((leftValue instanceof Integer) ? (Integer)leftValue : Integer.valueOf(leftValue.toString()));
				Integer right = ((rightValue instanceof Integer) ? (Integer)rightValue : Integer.valueOf(rightValue.toString()));
				return (T)Integer.valueOf(left + right);
			} else if (returnType == Double.class) {
				Double left = ((leftValue instanceof Double) ? (Double)leftValue : Double.valueOf(leftValue.toString()));
				Double right = ((rightValue instanceof Double) ? (Double)rightValue : Double.valueOf(rightValue.toString()));
				return (T)Double.valueOf(left + right);
			} else if (returnType == String.class) {
				String left = ((leftValue instanceof String) ? (String)leftValue : leftValue.toString());
				String right = ((rightValue instanceof String) ? (String)rightValue : rightValue.toString());
				return (T)String.valueOf(left + right);
			} else if (returnType == Long.class) {
				Long left = ((leftValue instanceof Long) ? (Long)leftValue : Long.valueOf(leftValue.toString()));
				Long right = ((rightValue instanceof Long) ? (Long)rightValue : Long.valueOf(rightValue.toString()));
				return (T)Long.valueOf(left + right);
			} else
				throw new UnsupportedOperationException("Unsupported type: " + returnType.getName());
		case Subtract:
			if (returnType == Integer.class) {
				Integer left = ((leftValue instanceof Integer) ? (Integer)leftValue : Integer.valueOf(leftValue.toString()));
				Integer right = ((rightValue instanceof Integer) ? (Integer)rightValue : Integer.valueOf(rightValue.toString()));
				return (T)Integer.valueOf(left - right);
			} else if (returnType == Double.class) {
				Double left = ((leftValue instanceof Double) ? (Double)leftValue : Double.valueOf(leftValue.toString()));
				Double right = ((rightValue instanceof Double) ? (Double)rightValue : Double.valueOf(rightValue.toString()));
				return (T)Double.valueOf(left - right);
			} else if (returnType == Long.class) {
				Long left = ((leftValue instanceof Long) ? (Long)leftValue : Long.valueOf(leftValue.toString()));
				Long right = ((rightValue instanceof Long) ? (Long)rightValue : Long.valueOf(rightValue.toString()));
				return (T)Long.valueOf(left - right);
			} else
				throw new UnsupportedOperationException("Unsupported type: " + returnType.getName());
		case Multiply:
			if (returnType == Integer.class) {
				Integer left = ((leftValue instanceof Integer) ? (Integer)leftValue : Integer.valueOf(leftValue.toString()));
				Integer right = ((rightValue instanceof Integer) ? (Integer)rightValue : Integer.valueOf(rightValue.toString()));
				return (T)Integer.valueOf(left * right);
			} else if (returnType == Double.class) {
				Double left = ((leftValue instanceof Double) ? (Double)leftValue : Double.valueOf(leftValue.toString()));
				Double right = ((rightValue instanceof Double) ? (Double)rightValue : Double.valueOf(rightValue.toString()));
				return (T)Double.valueOf(left * right);
			} else if (returnType == Long.class) {
				Long left = ((leftValue instanceof Long) ? (Long)leftValue : Long.valueOf(leftValue.toString()));
				Long right = ((rightValue instanceof Long) ? (Long)rightValue : Long.valueOf(rightValue.toString()));
				return (T)Long.valueOf(left * right);
			} else
				throw new UnsupportedOperationException("Unsupported type: " + returnType.getName());
		case Divide:
			if (returnType == Integer.class) {
				Integer left = ((leftValue instanceof Integer) ? (Integer)leftValue : Integer.valueOf(leftValue.toString()));
				Integer right = ((rightValue instanceof Integer) ? (Integer)rightValue : Integer.valueOf(rightValue.toString()));
				return (T)Integer.valueOf(left / right);
			} else if (returnType == Double.class) {
				Double left = ((leftValue instanceof Double) ? (Double)leftValue : Double.valueOf(leftValue.toString()));
				Double right = ((rightValue instanceof Double) ? (Double)rightValue : Double.valueOf(rightValue.toString()));
				return (T)Double.valueOf(left / right);
			} else if (returnType == Long.class) {
				Long left = ((leftValue instanceof Long) ? (Long)leftValue : Long.valueOf(leftValue.toString()));
				Long right = ((rightValue instanceof Long) ? (Long)rightValue : Long.valueOf(rightValue.toString()));
				return (T)Long.valueOf(left / right);
			} else
				throw new UnsupportedOperationException("Unsupported type: " + returnType.getName());
		default:
			throw new UnsupportedOperationException("Unsupported unary operation " + op.name()); 
		}
	}

	@Override
	public List<ArithmeticExpressionOperation<?>> getChildren() {
		return children;
	}

	@Override
	public Class<T> getType() {
		return returnType;
	}

	public String toString() {
		return "(" + getChildren().get(0).toString() + ")" +
				ArithmeticExpressionOperation.getStandardName(getOp()) 
				+ "(" + getChildren().get(1).toString() + ")"; 
	}


	@Override
	public ArithmeticOperation getOp() {
		return op;
	}

	@Override
	public Set<String> getSymbolsReferenced() {
		Set<String> ret = new HashSet<String>();
		for (ArithmeticExpressionOperation<?> child: children)
			ret.addAll(child.getSymbolsReferenced());
		
		return ret;
	}

	@Override
	public void rename(Map<String, String> map) {
		for (ArithmeticExpressionOperation<?> child: children)
			child.rename(map);
	}
}