/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.Supplier;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;

public class BasicTuple extends BasicTupleWithSchema<String> {
	
	public BasicTuple() {
		super(new BasicSchema(), new ArrayList<>());
	}

	public BasicTuple(BasicSchema schema, List<Field<? extends Object>> fields) {
		super(schema, fields);
	}
	
	public BasicSchema getSchema() {
		return (BasicSchema) super.getSchema();
	}	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public BasicTuple project(String name, List<String> projKeys) {
		BasicSchema next = getSchema().project(name, projKeys);

		BasicTuple ret = next.createTuple();
		int projTo = 0;
		int siz = getSchema().getKeys().size();
		for (int i = 0; i < siz; i++) {
			if (projKeys.contains(getSchema().getKeyAt(i))) {
				Object src = getValueAt(i);
				@SuppressWarnings("unchecked")
				Field<Object> field = (Field<Object>) ret.getFields().get(projTo);
				field.setValue(src);
				
				projTo++;
			}
		}
		return ret;
	}

	public BasicTuple project(BasicSchema next) {
		BasicTuple ret = next.createTuple();
		int projTo = 0;
		int siz = getSchema().getKeys().size();
		for (int i = 0; i < siz; i++) {
			if (next.getKeys().contains(getSchema().getKeyAt(i))) {
				Object src = getValueAt(i);
				@SuppressWarnings("unchecked")
				Field<Object> field = (Field<Object>) ret.getFields().get(projTo);
				field.setValue(src);
				
				projTo++;
			}
		}
		return ret;
	}

	@Override
	public <E> boolean augment(BasicSchema outputSchema, 
			Iterable<E> extractions,
			List<String> opAndFields,
			Collection<MutableTupleWithSchema<String>> outputs,
			Supplier<ProvToken> tokenSupplier,
			BinaryOperator<ProvSpecifier> combiner,
			List<MutableTupleWithSchema<String>> recycled) {
		
		Iterator<E> iter = extractions.iterator();
		boolean isAny = false;
		
		while (iter.hasNext()) {
			E next = iter.next();
			
			MutableTupleWithSchema<String> tuple = recycled.isEmpty() ? outputSchema.createTuple() : recycled.remove(0);
			
			// Copy all but the provenance fields
			int siz = getKeys().size();
			for (int i = 0; i < siz; i++) {
//				if (!getKeys().get(i).equals(ProvenanceApi.Provenance) && 
//						!getKeys().get(i).equals(ProvenanceApi.Token)) {
					tuple.setValueAt(i, getValueAt(i));
//				}
			}
//			tuple.setValue(ProvenanceApi.Provenance, null);
//			tuple.setValue(ProvenanceApi.Token, null);
			
			updateTuple(next, tuple, combiner, tokenSupplier, tuple.getFields().size() - 1);
			
			// If we need to supply provenance token or location
//			if (tuple.getValue(ProvenanceApi.Token) == null) {
				Object tok = tokenSupplier.get();//new ProvToken();//getValue("token");
				if (tok != null)
					tuple.setValue(ProvenanceApi.Token, tok);
//			}
//			if (tuple.getValue(ProvenanceApi.Provenance) == null) {
				Object prov = getValue(ProvenanceApi.Provenance);
				if (prov != null)
					tuple.setValue(ProvenanceApi.Provenance, prov);
//			}
			
			outputs.add(tuple);
			isAny = true;
		}
		return isAny;
	}
	
	<E> void updateTuple(
			E valueToAdd, 
			MutableTupleWithSchema<String> tuple, 
			BinaryOperator<ProvSpecifier> combiner, 
			Supplier<ProvToken> tokenSupplier,
			int fieldIndex) {
		
		// If it is a tuple, then look up each of the fields by name
		if (valueToAdd instanceof BasicTuple) {
			BasicTuple tup = (BasicTuple)valueToAdd;
			
			for (int i = 0; i < tup.getFields().size(); i++) {
				updateTuple(tup.getValueAt(i), tuple, combiner,
						tokenSupplier,
						tuple.getKeys().indexOf(tup.getKeys().get(i)));
			}
			return;
			
			// If we had a list of length n, assume it is the last n items ending at
			// the fieldIndex
		} else if (valueToAdd instanceof Collection) {
			Collection<?> coll = (Collection<?>)valueToAdd;
			
			int inx = fieldIndex - coll.size() + 1;
			for (Object obj: coll) {
				updateTuple(obj, tuple, combiner, tokenSupplier, inx++);
			}
			return;
			// If we pulled a provenance specifier, then unnest it and update
			// our provenance token / location
		} else if (valueToAdd instanceof ProvExtraction) {
			ProvExtraction extr = (ProvExtraction) valueToAdd;
			tuple.setValue(ProvenanceApi.Token, tokenSupplier.get());//new ProvToken());//extr.getToken());
			tuple.setValue(ProvenanceApi.Provenance, 
					combiner.apply((ProvSpecifier)getValue(ProvenanceApi.Provenance), 
							extr.getLocation()));
			
			updateTuple(extr.getExtraction(), tuple, combiner, tokenSupplier, fieldIndex);
			return;
		} else if (valueToAdd instanceof ProvToken) {
			ProvToken extr = (ProvToken) valueToAdd;
			tuple.setValue(ProvenanceApi.Token, extr);
			
			return;
		} else if (valueToAdd instanceof ProvSpecifier) {
			ProvSpecifier extr = (ProvSpecifier) valueToAdd;
			tuple.setValue(ProvenanceApi.Provenance, 
					combiner.apply((ProvSpecifier)getValue(ProvenanceApi.Provenance), extr));
			
			return;
		}
//		System.out.println(valueToAdd + " ... " + next);
		
		// Set the value -- we assume it's the last one!
		tuple.setValueAt(fieldIndex, valueToAdd);
	}

	public MutableTupleWithSchema<String> concat(TupleWithSchema<String> rt, BasicSchema basicSchema, 
			BinaryOperator<ProvSpecifier> combiner) {
		BasicTuple ret = basicSchema.createTuple();
		
		return concat(rt, basicSchema, combiner, ret);
	}
	
	public MutableTupleWithSchema<String> concat(TupleWithSchema<String> rt, BasicSchema basicSchema, 
			BinaryOperator<ProvSpecifier> combiner, MutableTupleWithSchema<String> ret) {
		
		int siz = getKeys().size();
		for (int i = 0; i < siz; i++) {
			ret.setValueAt(i, getValueAt(i));
		}
		
		int siz2 = rt.getKeys().size();
		int j = 0;
		for (int i = 0; i + j < siz2; i++) {
			while (i+j < siz2 && rt.getKeys().get(i+j).equals(ProvenanceApi.Provenance) ||
					rt.getKeys().get(i+j).equals(ProvenanceApi.Token))
				j++;
			ret.setValueAt(i + siz, rt.getValueAt(i+j));
		}

		Object prov = getValue(ProvenanceApi.Provenance);
		Object prov2 = rt.getValue(ProvenanceApi.Provenance);
		
		if (prov != null && prov2 != null)
			ret.setValue(ProvenanceApi.Provenance, 
					combiner.apply((ProvSpecifier)prov, (ProvSpecifier)prov2));

		return ret;
	}

}
