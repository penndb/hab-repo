/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.model;

import static com.google.common.base.Preconditions.checkNotNull;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * @author John Frommeyer
 *
 */
public class ProvEdgeSetModel {

	@ApiModelProperty(required = true)
	public final Set<ProvEdgeModel> edges = new HashSet<>();

	public ProvEdgeSetModel(Set<ProvEdgeModel> tuples) {
		this.edges.addAll(tuples);
	}

	public static ProvEdgeSetModel from(
			Map<String, List<TupleWithSchema<String>>> edgeMap) {
		checkNotNull(edgeMap);
		final Set<ProvEdgeModel> models = new HashSet<>();
		edgeMap.forEach((token, data) -> {
			// Current impl for Neo4J gives every edge a tuple, but I don't
			// think the API requires it, so trying to deal with it here.
			// Multiple no-tuple edges will be collapsed into one.
			if (data.isEmpty()) {
				models.add(ProvEdgeModel.from(token, null));
			} else {
				data.forEach(tuple ->
						models.add(ProvEdgeModel.from(token, tuple)));
			}
		});
		return new ProvEdgeSetModel(models);
	}

	@Override
	public String toString() {
		return "ProvEdgeSetModel [edges=" + edges + "]";
	}

	public Map<String, List<TupleWithSchema<String>>> toProvEdgeMap()
			throws HabitatServiceException {
		final Map<String, List<TupleWithSchema<String>>> edgeMap = new HashMap<>();
		for (final ProvEdgeModel edge : edges) {
			final List<TupleWithSchema<String>> tuples = edgeMap
					.computeIfAbsent(edge.endpointProvToken,
							token -> new ArrayList<>());
			// Current impl for Neo4J gives every edge a tuple, but I don't
			// think the API requires it, so trying to deal with it here.
			// Multiple no-tuple edges will be collapsed into one.
			if (edge.tupleWithSchema != null) {
				tuples.add(
						edge.tupleWithSchema.toTupleWithSchema());
			}
		}
		return edgeMap;
	}

}
