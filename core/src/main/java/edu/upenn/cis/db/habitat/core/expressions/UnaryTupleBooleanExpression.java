/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.core.expressions;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.db.habitat.core.expressions.PredicateOperations.BooleanOperation;
import edu.upenn.cis.db.habitat.core.expressions.PredicateOperations.ComparisonOperation;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;

public class UnaryTupleBooleanExpression extends UnaryTuplePredicateWithLookup {
	PredicateOperations.BooleanOperation op;
	UnaryTuplePredicateWithLookup left;
	UnaryTuplePredicateWithLookup right;

	/**
	 * Boolean negation or similar predicate
	 * 
	 * @param operation
	 * @param child
	 */
	public UnaryTupleBooleanExpression( 
			BooleanOperation operation, UnaryTuplePredicateWithLookup child){
		
		this.left = child;
		this.op = operation;
		
		if (op != PredicateOperations.BooleanOperation.Not)
			throw new UnsupportedOperationException();
	}

	public UnaryTupleBooleanExpression( 
			BooleanOperation operation, UnaryTuplePredicateWithLookup left, UnaryTuplePredicateWithLookup right){
		
		this.left = left;
		this.right = right;
		this.op = operation;
		
		if (op == PredicateOperations.BooleanOperation.Not)
			throw new UnsupportedOperationException();
	}

	
	@Override
	public void bind(TupleWithSchema<String> tuple) {
		this.tuple = tuple;
		left.bind(tuple);
		if (right != null)
			right.bind(tuple);
	}

	@Override
	public boolean test(TupleWithSchema<String> t) {
		Boolean lResult = left.test(t);
		// Don't evaluate right (1) if it doesn't exist, (2) if we are doing an OR
		// and we can short-circuit, (3) if we are doing an AND and we can short-circuit
		Boolean rResult = right == null || 
				(lResult && op == PredicateOperations.BooleanOperation.Or) || 
				(!lResult && op == PredicateOperations.BooleanOperation.And)
				? null : right.test(t);
		
		switch (op) {
		case Not:
			return !lResult;
		case And:
			if (rResult == null)
				return lResult;
			else
				return lResult && rResult;
		case Or:
			if (rResult == null)
				return lResult;
			else
				return lResult || rResult;
		case Xor:
			return lResult ^ rResult;
		}

		return false;
	}

	@Override
	public String toString() {
		if (right == null)
			return PredicateOperations.getStandardName(op) + "(" + left.toString() + ")";
		else
			return "(" + left.toString() + ") " + PredicateOperations.getStandardName(op)  +
					" (" + right.toString() + ")";
	}

	@Override
	public Set<String> getSymbolsReferenced() {
		Set<String> ret = left.getSymbolsReferenced();
		
		if (right != null)
			ret.addAll(right.getSymbolsReferenced());
		
		return ret;
	}
	
	public PredicateOperations.BooleanOperation getOperation() {
		return op;
	}
	
	public UnaryTuplePredicateWithLookup getLeft() {
		return left;
	}
	
	public UnaryTuplePredicateWithLookup getRight() {
		return right;
	}

	@Override
	public void rename(Map<String, String> symbolTable) {
		if (left != null)
			left.rename(symbolTable);
		if (right != null)
			right.rename(symbolTable);
	}
}
