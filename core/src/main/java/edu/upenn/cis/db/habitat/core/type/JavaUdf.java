package edu.upenn.cis.db.habitat.core.type;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import edu.upenn.cis.db.habitat.core.expressions.ArithmeticExpressionOperation;

public abstract class JavaUdf<T,R,S> implements Function<T, R>, Serializable {
	
	String name = "";
	BasicSchema arguments;
	List<ArithmeticExpressionOperation<S>> expressions = new ArrayList<>();
	BasicSchema outputSchema;
	
	boolean isTableValued;
	
	public JavaUdf(String name, BasicSchema arguments, List<ArithmeticExpressionOperation<S>> expressions, BasicSchema outputSchema) {
		this.name = name;
		this.arguments = arguments;
		this.expressions = expressions;
		this.outputSchema = outputSchema;
		isTableValued = true;
	}
	
	public JavaUdf(String name, BasicSchema arguments, ArithmeticExpressionOperation<S> expression, BasicSchema outputSchema) {
		this.name = name;
		this.arguments = arguments;
		this.expressions = new ArrayList<ArithmeticExpressionOperation<S>>();
		this.expressions.add(expression);
		this.outputSchema = outputSchema;
		isTableValued = true;
	}

	public abstract Function<R,S> computeInput();

	public abstract R apply(T t);

	public BasicSchema getArguments() {
		return arguments;
	}

	public void setArguments(BasicSchema arguments) {
		this.arguments = arguments;
	}

	public BasicSchema getOutputSchema() {
		return outputSchema;
	}

	public void setOutputSchema(BasicSchema outputSchema) {
		this.outputSchema = outputSchema;
	}

	public boolean isTableValued() {
		return isTableValued;
	}

	public void setTableValued(boolean isTableValued) {
		this.isTableValued = isTableValued;
	}

	public List<ArithmeticExpressionOperation<S>> getExpressions() {
		return expressions;
	}

	public void setExpressions(List<ArithmeticExpressionOperation<S>> expressions) {
		this.expressions = expressions;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/** Instante an instance of the UDF with the input expression **/
	public abstract JavaUdf<T,R,S> instantiate(List<ArithmeticExpressionOperation<S>> op);

	public String toString() {
		return getName().toString() + "(" + getExpressions().toString() + "): " + getOutputSchema().toString();
	}
}
