package edu.upenn.cis.db.habitat.core.expressions;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;

public class ArithmeticExpressionLiteral<T> implements ArithmeticExpressionOperation<T> {
	
	T value;
	Class<T> retType;
	
	public ArithmeticExpressionLiteral(Class<T> returnType, T value) {
		this.value = value;
		this.retType = returnType;
	}
	
	final static List<ArithmeticExpressionOperation<?>> nochildren = new ArrayList<>();

	@Override
	public void bind(TupleWithSchema<String> tuple) {
		// Do nothing since it's a literal
	}

	@Override
	public T getValue() {
		return value;
	}

	@Override
	public List<ArithmeticExpressionOperation<?>> getChildren() {
		return nochildren;
	}

	@Override
	public Class<T> getType() {
		return retType;
	}

	@Override
	public String toString() {
		return getValue().toString();
	}

	@Override
	public ArithmeticOperation getOp() {
		return ArithmeticOperation.Literal;
	}

	@Override
	public Set<String> getSymbolsReferenced() {
		return new HashSet<String>();
	}

	@Override
	public void rename(Map<String, String> map) {
		
	}
}
