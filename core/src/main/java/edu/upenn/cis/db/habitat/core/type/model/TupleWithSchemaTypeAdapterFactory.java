/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.model;

import java.io.IOException;
import java.util.Map;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import edu.upenn.cis.db.habitat.core.type.model.FieldModel.Type;

/**
 * 
 * @author John Frommeyer
 *
 */
public class TupleWithSchemaTypeAdapterFactory implements TypeAdapterFactory {

	@Override
	public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
		if (!TupleWithSchemaModel.class
				.isAssignableFrom(typeToken.getRawType())) {
			return null;
		}

		final TypeAdapter<TupleWithSchemaModel> tupleWithSchemaAdapter = gson
				.getDelegateAdapter(this,
						new TypeToken<TupleWithSchemaModel>() {
						});
		final TypeAdapter<JsonElement> jsonElementAdapter = gson
				.getAdapter(JsonElement.class);
		final ImmutableMap.Builder<FieldModel.Type, TypeAdapter<? extends FieldModel>> builder = ImmutableMap
				.builder();
		builder.put(Type.BOOLEAN,
				gson.getAdapter(
						new TypeToken<BooleanFieldModel>() {
						}));
		builder.put(Type.INTEGER,
				gson.getAdapter(
						new TypeToken<IntegerFieldModel>() {
						}));
		builder.put(Type.LONG,
				gson.getAdapter(new TypeToken<LongFieldModel>() {
				}));
		builder.put(Type.DOUBLE,
				gson.getAdapter(
						new TypeToken<DoubleFieldModel>() {
						}));
		builder.put(Type.STRING,
				gson.getAdapter(
						new TypeToken<StringFieldModel>() {
						}));
		builder.put(Type.PROV_TOKEN,
				gson.getAdapter(
						new TypeToken<ProvTokenFieldModel>() {
						}));
		builder.put(Type.PROV_SPECIFIER,
				gson.getAdapter(
						new TypeToken<ProvSpecifierFieldModel>() {
						}));
		builder.put(Type.QUALIFIED_NAME,
				gson.getAdapter(
						new TypeToken<QualifiedNameFieldModel>() {
						}));
		builder.put(Type.MULTI,
				gson.getAdapter(
						new TypeToken<MultiFieldModel<?>>() {
						}));

		@SuppressWarnings("unchecked")
		final TypeAdapter<T> specifierAdapter = (TypeAdapter<T>) new TupleWithSchemaTypeAdapter(
				tupleWithSchemaAdapter,
				jsonElementAdapter,
				builder.build())
				.nullSafe();
		return specifierAdapter;

	}

	private static class TupleWithSchemaTypeAdapter extends
			TypeAdapter<TupleWithSchemaModel> {

		private final TypeAdapter<TupleWithSchemaModel> tupleWithSchemaAdapter;
		private final TypeAdapter<JsonElement> jsonElementAdapter;
		private final Map<FieldModel.Type, TypeAdapter<? extends FieldModel>> fieldAdapterMap;

		public TupleWithSchemaTypeAdapter(
				TypeAdapter<TupleWithSchemaModel> tupleWithSchemaAdapter,
				TypeAdapter<JsonElement> jsonElementAdapter,
				Map<FieldModel.Type, TypeAdapter<? extends FieldModel>> fieldAdapterMap) {
			this.tupleWithSchemaAdapter = tupleWithSchemaAdapter;
			this.jsonElementAdapter = jsonElementAdapter;
			this.fieldAdapterMap = fieldAdapterMap;
		}

		@Override
		public void write(JsonWriter out, TupleWithSchemaModel value)
				throws IOException {
			tupleWithSchemaAdapter.write(out, value);
		}

		@Override
		public TupleWithSchemaModel read(JsonReader in) throws IOException {
			final JsonObject tupleWithSchemaJson = jsonElementAdapter.read(in)
					.getAsJsonObject();
			final String schemaName = tupleWithSchemaJson.getAsJsonPrimitive(
					TupleWithSchemaModel.SCHEMA_NAME_PROP_NAME).getAsString();
			final ImmutableList.Builder<FieldModel> tupleBuilder = ImmutableList
					.builder();
			final JsonArray fieldsJson = tupleWithSchemaJson
					.getAsJsonArray(TupleWithSchemaModel.TUPLE_PROP_NAME);
			for (final JsonElement fieldJson : fieldsJson) {

				final JsonElement typeJson = fieldJson.getAsJsonObject()
						.get(FieldModel.TYPE_PROP_NAME);
				final FieldModel.Type type = FieldModel.Type.valueOf(typeJson
						.getAsString());
				final TypeAdapter<? extends FieldModel> fieldAdapter = fieldAdapterMap
						.get(type);
				if (fieldAdapter == null) {
					throw new IllegalArgumentException(
							"unable to deserialize TupleWithSchemaModel: "
									+ tupleWithSchemaJson);
				}
				final FieldModel fieldModel = fieldAdapter
						.fromJsonTree(fieldJson);
				tupleBuilder.add(fieldModel);
			}

			final JsonArray lookupKeysJson = tupleWithSchemaJson
					.getAsJsonArray(TupleWithSchemaModel.SCHEMA_LOOKUP_KEYS_PROP_NAME);
			final ImmutableList.Builder<String> lookupKeysBuilder = ImmutableList
					.builder();
			for (final JsonElement lookupKeyJson : lookupKeysJson) {
				lookupKeysBuilder.add(lookupKeyJson.getAsString());
			}
			return new TupleWithSchemaModel(schemaName,
					lookupKeysBuilder.build(), tupleBuilder.build());

		}
	}

}
