/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.provdm;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.emptyList;

import java.util.ArrayList;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import java.util.List;

import javax.annotation.Nullable;

import com.google.common.annotations.VisibleForTesting;

import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.model.ProvSpecifierModel;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * A node in a PROV DM graph.
 * 
 * @author John Frommeyer
 *
 */
@ApiModel(description = "an Entity, Activity, or Agent in the PROV model")
public class NodeModel extends HasAttributes {

	@VisibleForTesting
	public static final QualifiedName PROV_DM_START_TIME_QN = new QualifiedName(
			ProvDmConstants.PENN_PROV_NAMESPACE, "provDmStartTime");
	@VisibleForTesting
	public static final QualifiedName PROV_DM_END_TIME_QN = new QualifiedName(
			ProvDmConstants.PENN_PROV_NAMESPACE, "provDmEndTime");

	private final ProvDmType type;
	private final ProvSpecifierModel location;
	private final Date startTime;
	private final Date endTime;

	public NodeModel(ProvDmType nodeType,
			List<Attribute> attributes,
			@Nullable Date startTime,
			@Nullable Date endTime,
			@Nullable ProvSpecifierModel location) {
		super(attributes);
		this.type = checkNotNull(nodeType);
		if ((startTime != null || endTime != null)
				&& !this.type.equals(ProvDmType.ACTIVITY)) {
			throw new IllegalArgumentException(
					String.format(
							"Cannot include start or end times in type %s. Start and end time are properties of %s only.",
							this.type.name(),
							ProvDmType.ACTIVITY.name()));
		}
		this.location = location;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public static NodeModel newEntity(
			List<Attribute> attributes,
			@Nullable ProvSpecifierModel location) {
		return new NodeModel(
				ProvDmType.ENTITY,
				attributes,
				null,
				null,
				location);
	}

	public static NodeModel newAnnotation(QualifiedName key, String value) {
		Attribute a = new StringAttribute(key, value);
		List<Attribute> attributes = new ArrayList<Attribute>();
		attributes.add(a);
		return new NodeModel(
				ProvDmType.ENTITY,
				attributes,
				null,
				null,
				null);
	}

	public static NodeModel newEntity(
			@Nullable ProvSpecifierModel location) {
		return newEntity(
				emptyList(),
				location);
	}

	public static NodeModel newCollection(
			List<Attribute> attributes,
			@Nullable ProvSpecifierModel location) {
		return new NodeModel(
				ProvDmType.COLLECTION,
				attributes,
				null,
				null,
				location);
	}

	public static NodeModel newCollection(
			@Nullable ProvSpecifierModel location) {
		return newCollection(
				emptyList(),
				location);
	}

	public static NodeModel newActivity(
			List<Attribute> attributes,
			@Nullable Date startTime,
			@Nullable Date endTime,
			@Nullable ProvSpecifierModel location) {
		return new NodeModel(ProvDmType.ACTIVITY, attributes, startTime,
				endTime, location);
	}

	public static NodeModel newActivity(
			@Nullable Date startTime,
			@Nullable Date endTime,
			@Nullable ProvSpecifierModel location) {
		return newActivity(emptyList(), startTime,
				endTime, location);
	}

	public static NodeModel newAgent(
			List<Attribute> attributes,
			@Nullable ProvSpecifierModel location) {
		return new NodeModel(
				ProvDmType.AGENT,
				attributes,
				null,
				null,
				location);
	}

	public static NodeModel newAgent(
			@Nullable ProvSpecifierModel location) {
		return newAgent(
				emptyList(),
				location);
	}

	@ApiModelProperty(required = true)
	public ProvDmType getType() {
		return type;
	}

	@ApiModelProperty(required = false)
	public ProvSpecifierModel getLocation() {
		return location;
	}

	public void addAttribute(QualifiedName key, Long value) {
		checkIntegralAttribute(key);
		super.addAttribute(key, value);
	}

	public void addAttribute(QualifiedName key, Integer value) {
		checkIntegralAttribute(key);
		super.addAttribute(key, value);
	}

	private void checkIntegralAttribute(QualifiedName key) {
		if (key.equals(PROV_DM_START_TIME_QN)
				&& !this.type.equals(ProvDmType.ACTIVITY)) {
			throw new IllegalArgumentException(
					String.format(
							"Cannot include start time in type %s. Start time is a property of %s only.",
							this.type.name(),
							ProvDmType.ACTIVITY.name()));
		}
		if (key.equals(PROV_DM_END_TIME_QN)
				&& !this.type.equals(ProvDmType.ACTIVITY)) {
			throw new IllegalArgumentException(
					String.format(
							"Cannot include end time in type %s. End time is a property of %s only.",
							this.type.name(),
							ProvDmType.ACTIVITY.name()));
		}
	}

	public TupleWithSchema<String> toTupleWithSchema()
			throws HabitatServiceException {

		addProvType();

		if (startTime != null) {
			addAttribute(PROV_DM_START_TIME_QN, startTime.getTime());
		}

		if (endTime != null) {
			addAttribute(PROV_DM_END_TIME_QN, endTime.getTime());
		}

		return super.toTupleWithSchema();
	}

	@ApiModelProperty(
			required = false,
			value = "it is an error to include start time unless type is ACTIVITY")
	public Date getStartTime() {
		return startTime;
	}

	@ApiModelProperty(
			required = false,
			value = "it is an error to include end time unless type is ACTIVITY")
	public Date getEndTime() {
		return endTime;
	}

	private void addProvType() {
		final ProvDmType declaredType = type;
		final ProvDmType superType = declaredType.getSuperType();
		addAttribute(ProvDmConstants.PROV_DM_TYPE_QN,
				superType
						.name());
		if (!declaredType.equals(superType)) {
			addAttribute(ProvDmConstants.TYPE_ATTRIBUTE,
					declaredType.getQualifiedName().get());
		}
	}

}
