package edu.upenn.cis.db.habitat.core.type;

import java.util.HashMap;
import java.util.function.Function;

/**
 * Basic implementation of a structured data element, which is a special case of a hashmap
 * from keys to values.
 * 
 * @author zives
 *
 * @param <K>
 * @param <V>
 */
public class StructuredValue<K, V> extends HashMap<K, V> implements StructuredData<K,V> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public <V2> StructuredData<K,V2> createConverted(Function<V,V2> converter) {
		StructuredValue<K,V2> ret = new StructuredValue<>();
		
		for (K key: keySet()) {
			ret.put(key, converter.apply(get(key)));
		}
		
		return ret;
	}

}
