/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.api;

import java.util.Set;

import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * Basic API for granting and revoking permissions on objects.  Permissions may be granted to
 * groups or to users.
 * 
 * @author zives
 *
 */
public interface PermissionApi {
	static final String UserPrefix = "_USR";
	static final String OrgPrefix = "_ORG";
	static final String GroupPrefix = "_GRP";
	
	static final String ReadPermission = "Read";
	static final String WritePermission = "Write";
	static final String GrantPermission = "Grant";
	static final String AdminPermission = "Admin";
	
	/**
	 * Create a new permission type, with a unique ID.
	 * 
	 * @param name
	 * @return
	 * @throws HabitatServiceException
	 */
	int addPermission(String name) throws HabitatServiceException;
	
	/**
	 * Assert that if I have a permission, I automatically get another permission.
	 * e.g., write permission entails read permission.
	 * 
	 * @param myPermission
	 * @param entailedPermission
	 * @throws HabitatServiceException
	 */
	void addEntailedPermission(String myPermission, String entailedPermission) throws HabitatServiceException;

	/**
	 * Look up the permission ID for a named permission type
	 * 
	 * @param permission
	 * @return
	 * @throws HabitatServiceException
	 */
	int getPermissionIdFrom(String permission) throws HabitatServiceException;
	
	/**
	 * Get the permission name for a permission ID
	 * 
	 * @param permissionId
	 * @return
	 * @throws HabitatServiceException
	 */
	String getPermissionFromId(int permissionId) throws HabitatServiceException;

	/**
	 * Test if the key is already used (independent of whether we have permissions on it)
	 * 
	 * @param object
	 * @return
	 * @throws HabitatServiceException
	 */
	public boolean isClaimedAsObjectId(String object) throws HabitatServiceException;
	
	/**
	 * Return the set of named permissions associated with an object, for a named group
	 * 
	 * @param object
	 * @param username
	 * @return
	 * @throws HabitatServiceException
	 */
	public Set<String> getGroupPermissionsOn(String object, String groupname) throws HabitatServiceException;

	/**
	 * Return the set of named permissions associated with an object, for a named user
	 * 
	 * @param object
	 * @param username
	 * @return
	 * @throws HabitatServiceException
	 */
	public Set<String> getUserPermissionsOn(String object, String username) throws HabitatServiceException;

	/**
	 * Tests if the user (directly or indirectly) has a particular permission (by perm ID) on 
	 * an object
	 * 
	 * @param object
	 * @param permId
	 * @param username
	 * @return
	 * @throws HabitatServiceException
	 */
	public boolean hasPermission(String object, int permId, String username) throws HabitatServiceException;
	
	/**
	 * Tests if the user (directly or indirectly) has a particular permission (by perm name) 
	 * on an object
	 * 
	 * @param object
	 * @param permId
	 * @param username
	 * @return
	 * @throws HabitatServiceException
	 */
	public boolean hasPermission(String object, String permission, String username) throws HabitatServiceException;

	/**
	 * Grants permission on an object to a group
	 * 
	 * @param object
	 * @param permission
	 * @param groupname
	 * @return
	 * @throws HabitatServiceException
	 */
	public boolean grantGroupPermissionOn(String object, String permission, String groupname) throws HabitatServiceException;
	
	/**
	 * Grants permission on an object to a user
	 * 
	 * @param object
	 * @param permission
	 * @param groupname
	 * @return
	 * @throws HabitatServiceException
	 */
	public boolean grantUserPermissionOn(String object, String permission, String username) throws HabitatServiceException;

	/**
	 * Deletes an object and its permissions from the permissions database
	 * 
	 * @param object
	 */
	public void removeKey(String object);

	/**
	 * Revokes a named permission on an object from a group
	 * 
	 * @param object
	 * @param permission
	 * @param groupname
	 * @return
	 * @throws HabitatServiceException
	 */
	public boolean revokeGroupPermissionOn(String object, String permission, String groupname) throws HabitatServiceException;

	/**
	 * Revokes a named permission on an object from a user
	 * @param object
	 * @param permission
	 * @param username
	 * @return
	 * @throws HabitatServiceException
	 */
	public boolean revokeUserPermissionOn(String object, String permission, String username) throws HabitatServiceException;

	/**
	 * Requests a token to authenticate the session
	 *  
	 * @return
	 */
	public String getSessionToken();
	
	/**
	 * Requests info on the username of the account being used for connection security 
	 * @return
	 */
	public String getSessionUsername();
	
	/**
	 * Returns the maximum allowable length for an object name
	 * 
	 * @return the maximum allowable length for an object name
	 */
	public int getMaxObjectNameLength();
	
	/**
	 * Returns the set of object names for which the given user has the given permission.
	 * 
	 * @param permission
	 * @param username
	 * @return
	 */
	public Set<String> getObjectIds(String permission, String username) throws HabitatServiceException;

	/**
	 * Returns true if the object exists
	 * @param resourceName
	 * @return
	 */
	boolean existsForUse(String resourceName, String username);

}
