/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.provdm;

import static com.google.common.base.Preconditions.checkNotNull;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import edu.upenn.cis.db.habitat.core.type.Field;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * @author John Frommeyer
 *
 */
@ApiModel(description = "A PROV attribute",
		discriminator = Attribute.TYPE_PROP_NAME,
		subTypes = {
				BooleanAttribute.class,
				DoubleAttribute.class,
				StringAttribute.class,
				IntAttribute.class,
				LongAttribute.class,
				QualifiedNameAttribute.class
		})
public interface Attribute {

	public static enum Type {
		BOOLEAN(Boolean.class),
		DOUBLE(Double.class),
		INTEGER(Integer.class),
		STRING(String.class),
		LONG(Long.class),
		QUALIFIED_NAME(QualifiedName.class);

		public final Class<?> javaType;

		private Type(Class<?> javaType) {
			this.javaType = checkNotNull(javaType);
		}
	}

	public static final String TYPE_PROP_NAME = "type";

	@ApiModelProperty(required = true)
	QualifiedName getName();

	@ApiModelProperty(required = true, name = TYPE_PROP_NAME)
	Type getType();

	@ApiModelProperty(required = true)
	Object getValue();
	
	Field<?> toField() throws HabitatServiceException;

}
