/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.api;

import java.util.List;
import java.util.Map;

import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * General interface for storing and retrieving provenance information.  Provenance is
 * assumed to be encapsulated through a *token* -- essentially a unique tuple ID --
 * a *location specifier* -- essentially a derivation tree -- and optionally a *tuple*
 * of data (if the data is to be stored inline). 
 * 
 * @author zives
 *
 */
public interface ProvenanceApi {
	public static final String Token = "token";
	public static final String Provenance = "prov";
	
	/**
	 * Store a provenance token with its location (expression)
	 * 
	 * @param token
	 * @param location
	 * @throws HabitatServiceException 
	 */
	public void storeProvenanceNode(String resource, ProvToken token, ProvSpecifier location) throws HabitatServiceException;
	
	/**
	 * Store a provenance token, location, and tuple
	 * 
	 * @param token
	 * @param location
	 * @throws HabitatServiceException 
	 */
	public void storeProvenanceNode(String resource, ProvToken token, TupleWithSchema<String> tuple, ProvSpecifier location) throws HabitatServiceException;

	/**
	 * Given a token, return its provenance location
	 * 
	 * @param token
	 * @return
	 * @throws HabitatServiceException 
	 */
	public ProvSpecifier getProvenanceLocation(String resource, ProvToken token) throws HabitatServiceException;
	
	/**
	 * Given a token, return its associated tuple
	 * 
	 * @param token
	 * @return
	 * @throws HabitatServiceException 
	 */
	public TupleWithSchema<String> getProvenanceData(String resource, ProvToken token) throws HabitatServiceException;
	
	/**
	 * Returns the provenance graph's nodes.
	 * 
	 * @return
	 * @throws HabitatServiceException
	 */
	public Map<String, StructuredData<String, Object>> getProvenanceNodes(String resource) throws HabitatServiceException;
	
	/**
	 * Store a directed provenance link with a label
	 * 
	 * @param from
	 * @param to
	 * @param label
	 * @throws HabitatServiceException 
	 */
	public void storeProvenanceLink(String resource, ProvToken from, String label, ProvToken to) throws HabitatServiceException;
	
	/**
	 * Store a directed provenance link with a tuple
	 * 
	 * @param from
	 * @param to
	 * @param label
	 * @throws HabitatServiceException 
	 */
	public void storeProvenanceLink(String resource, ProvToken from, String label, ProvToken to, TupleWithSchema<String> tuple) throws HabitatServiceException;
	
	/**
	 * Store a directed provenance link for a join, with a label
	 * 
	 * @param from
	 * @param to
	 * @param label
	 * @throws HabitatServiceException 
	 */
	public void storeProvenanceLink(String resource, ProvToken fromLeft, ProvToken fromRight, 
			String label, ProvToken to) throws HabitatServiceException;
	
	public void storeProvenanceLink(String resource, ProvToken fromLeft, ProvToken fromRight, 
			String label, ProvToken to, TupleWithSchema<String> tuple) throws HabitatServiceException;
	
	/**
	 * Store a directed provenance link for aggregation, with a label
	 * 
	 * @param from
	 * @param to
	 * @param label
	 * @throws HabitatServiceException 
	 */
	public void storeProvenanceLink(String resource, List<ProvToken> inputProv, String label, ProvToken to) throws HabitatServiceException;

	/**
	 * Store a directed provenance link for agregation, with a tuple
	 * 
	 * @param from
	 * @param to
	 * @param tuple
	 * @throws HabitatServiceException 
	 */
	public void storeProvenanceLink(String resource, List<ProvToken> inputProv, String label, ProvToken to,
			TupleWithSchema<String> tuple) throws HabitatServiceException;

	/**
	 * Outgoing labeled provenance links
	 * 
	 * @param from
	 * @param label
	 * @return
	 */
	public Iterable<ProvToken> getConnectedTo(String resource, ProvToken to) throws HabitatServiceException;

	/**
	 * Outgoing labeled provenance links
	 * 
	 * @param from
	 * @param label
	 * @return
	 */
	public Iterable<ProvToken> getConnectedTo(String resource, ProvToken to, String label) throws HabitatServiceException;

	/**
	 * Outgoing labeled provenance link tuples
	 * 
	 * @param from
	 * @param label
	 * @return
	 */
	public Iterable<TupleWithSchema<String>> getEdgesTo(String resource, ProvToken to) throws HabitatServiceException;

	/**
	 * Incoming labeled provenance links
	 * 
	 * @param to
	 * @param label
	 * @return
	 */
	public Iterable<ProvToken> getConnectedFrom(String resource, ProvToken from) throws HabitatServiceException;

	/**
	 * Incoming labeled provenance links
	 * 
	 * @param to
	 * @param label
	 * @return
	 */
	public Iterable<ProvToken> getConnectedFrom(String resource, ProvToken from, String label) throws HabitatServiceException;

	/**
	 * Incoming labeled provenance link tuples
	 * 
	 * @param to
	 * @param label
	 * @return
	 * @throws HabitatServiceException 
	 */
	public Iterable<TupleWithSchema<String>> getEdgesFrom(String resource, ProvToken from) throws HabitatServiceException;

	/**
	 * Returns map of string representation of ProvToken to edge data. The edges are those going from {@code from} to 
	 * the keys in the returned map.
	 * 
	 * @param from
	 * @return
	 * @throws HabitatServiceException
	 */
	Map<String, List<TupleWithSchema<String>>> getEdgeMapFrom(String resource, ProvToken from)
			throws HabitatServiceException;
	
	/**
	 * Returns map of string representation of ProvToken to edge data. The edges are those going from 
	 * the keys in the returned map to {@code to}.
	 * 
	 * @param to
	 * @return
	 * @throws HabitatServiceException
	 */
	Map<String, List<TupleWithSchema<String>>> getEdgeMapTo(String resource, ProvToken to)
			throws HabitatServiceException;


//	void annotateProvenanceNode(String resource, ProvQualifiedNameToken idToken, String key, Serializable value)
//	throws HabitatServiceException;
//
//	Serializable getProvenanceNodeAnnotation(String resource, ProvQualifiedNameToken idToken, String key)
//			throws HabitatServiceException;
}
