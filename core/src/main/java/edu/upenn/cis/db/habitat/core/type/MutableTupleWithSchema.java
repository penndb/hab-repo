package edu.upenn.cis.db.habitat.core.type;

import java.util.List;

/**
 * A tuple whose schema and/or fields are mutable
 * 
 * @author zives
 *
 * @param <K>
 */
public interface MutableTupleWithSchema<K> extends TupleWithSchema<K> {

	public void setFields(List<Field<? extends Object>> fields);

	public void setSchema(Schema<K, Class<? extends Object>> schema);

	public <E >void setValue(K key, E value);

	public <E> void setValueAt(int index, E value);
}
