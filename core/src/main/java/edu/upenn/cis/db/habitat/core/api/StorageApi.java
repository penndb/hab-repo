/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.api;

import java.io.InputStream;
import java.net.URL;
import java.sql.SQLException;
import java.util.function.Function;

import javax.annotation.Nullable;

import org.apache.commons.lang3.tuple.Pair;

import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.TransactionSupplier;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

public interface StorageApi<K,V,B> {
	/**
	 * The type of the storage service
	 * 
	 * @author zives
	 *
	 */
	public enum StoreClass {BLOB, GRAPH, RELATION, TIMESERIES, ANNOTATED_BLOB}; 
	
	/**
	 * Used to convert between serialized and de-serialized object form
	 * 
	 * @author zives
	 *
	 * @param <S>
	 * @param <V>
	 */
	public interface ObjectSerializer<V,S> {
		public V getObject(S serializedObject);
		public S getSerializedObject(V object);
	}
	
	/**
	 * Within a namespace/resource, associate the metadata with the key
	 *  
	 * @param resource
	 * @param key
	 * @param metadata
	 * @throws HabitatServiceException
	 */
	public void addMetadata(String resource, K key, StructuredData<K,? extends V> metadata) throws HabitatServiceException;

	public void addMetadata(String resource, K key, StructuredData<K,? extends V> metadata, @Nullable String specialLabel) throws HabitatServiceException;

	/**
	 * Within a namespace/resource, transactionally associate the metadata with the key
	 * 
	 * @param supplier
	 * @param resource
	 * @param key
	 * @param metadata
	 * @throws HabitatServiceException
	 */
	public void addMetadata(TransactionSupplier supplier, String resource, K key, StructuredData<K,? extends V> metadata) throws HabitatServiceException;

	public void addMetadata(TransactionSupplier supplier, String resource, K key, StructuredData<K,? extends V> metadata, @Nullable String specialLabel) throws HabitatServiceException;
	
	public void connect() throws SQLException;
	
	/**
	 * Execute a function within a transaction block
	 * 
	 * @param transactionFn
	 * @return
	 */
	public boolean execTransaction(String resource, Function<TransactionSupplier,Boolean> transactionFn) throws HabitatServiceException;

	/**
	 * Get the cardinality of the objects within the namespace/resource
	 * 
	 * @param resource
	 * @return
	 * @throws HabitatServiceException
	 */
	public int getCount(String resource) throws HabitatServiceException;
	
	/**
	 * Retrieve node extent / raw data
	 * 
	 * @param key
	 * @return
	 * @throws HabitatServiceException 
	 */
	public B getData(String resource, K key) throws HabitatServiceException;

	/**
	 * Transactionally retrieve node extent / raw data
	 * 
	 * @param key
	 * @return
	 * @throws HabitatServiceException 
	 */
	public B getData(TransactionSupplier supplier, String resource, K key) throws HabitatServiceException;
	
	/**
	 * Retrieve node metadata
	 * 
	 * @param key
	 * @return
	 */
	public StructuredData<K,V> getMetadata(String resource, K key) throws HabitatServiceException;

	/**
	 * Transactionally retrieve node metadata
	 * 
	 * @param key
	 * @return
	 */
	public StructuredData<K,V> getMetadata(TransactionSupplier supplier, String resource, K key) throws HabitatServiceException;

	/**
	 * Retrieve nodes in resource
	 * 
	 * @param resource
	 * @return
	 */
	public Iterable<Pair<K,StructuredData<K,V>>> getMetadataForResource(String resource) throws HabitatServiceException;

	/**
	 * Transactionally retrieve nodes in resource
	 * 
	 * @param resource
	 * @return
	 */
	public Iterable<Pair<K,StructuredData<K,V>>> getMetadataForResource(TransactionSupplier supplier, String resource) throws HabitatServiceException;

	/**
	 * Get information about the type of store
	 * 
	 * @return
	 */
	public StoreClass getStoreClass();
	
	/**
	 * Does this store support fine-grained metadata?
	 * @return
	 */
	public boolean isSuitableForMetadata();
	
	/**
	 * Does this store accept objects of this size/type?
	 * @param obj
	 * @return
	 */
	public boolean isSuitableToStore(Object obj);

	/**
	 * Delete a node within a namespace / resource
	 * 
	 * @param resource
	 * @param key
	 * @throws HabitatServiceException
	 */
	public void removeKey(String resource, K key) throws HabitatServiceException;
	
	/**
	 * Transactionally delete a node within a namespace / resource
	 * 
	 * @param resource
	 * @param key
	 * @throws HabitatServiceException
	 */
	public void removeKey(TransactionSupplier supplier, String resource, K key) throws HabitatServiceException;

	/**
	 * Delete a metadata field for an object within a namespace / resource
	 * 
	 * @param resource
	 * @param key
	 * @throws HabitatServiceException
	 */
	public void removeMetadata(String resource, K key, String metadataField) throws HabitatServiceException;
	
	/**
	 * Transactionally delete a metadata field for an object within a namespace / resource
	 * 
	 * @param resource
	 * @param key
	 * @throws HabitatServiceException
	 */
	public void removeMetadata(TransactionSupplier supplier, String resource, K key, String metadataField) throws HabitatServiceException;

	/**
	 * Update a metadata field for an object within a namespace / resource
	 * 
	 * @param resource
	 * @param key
	 * @throws HabitatServiceException
	 */
	public void replaceMetadata(String resource, K key, StructuredData<K,? extends V> metadata) throws HabitatServiceException;
	
	/**
	 * Transactionally update a metadata field for an object within a namespace / resource
	 * 
	 * @param resource
	 * @param key
	 * @throws HabitatServiceException
	 */
	public void replaceMetadata(TransactionSupplier supplier, String resource, K key, StructuredData<K,? extends V> metadata) throws HabitatServiceException;
	
	/**
	 * Store an object / node and its metadata
	 * 
	 * @param key
	 * @param value
	 * @param metadata
	 * @throws HabitatServiceException 
	 */
	public void store(String resource, K key, B value, StructuredData<K,? extends V> metadata) throws HabitatServiceException;

	/**
	 * Transactionally store an object / node and its metadata
	 * 
	 * @param key
	 * @param value
	 * @param metadata
	 * @throws HabitatServiceException 
	 */
	public void store(TransactionSupplier supplier, String resource, K key, B value, StructuredData<K,? extends V> metadata) throws HabitatServiceException;

	/**
	 * Store a stream as a node and its metadata
	 * 
	 * @param key
	 * @param value
	 * @param metadata
	 * @throws HabitatServiceException 
	 */
	public void storeFile(String resource, K key, InputStream file, int streamLength, 
			StructuredData<K,? extends V> metadata) throws HabitatServiceException;
	
	/**
	 * Transactionally store a stream as a node and its metadata
	 * 
	 * @param key
	 * @param value
	 * @param metadata
	 * @throws HabitatServiceException 
	 */
	public void storeFile(TransactionSupplier supplier, String resource, K key, 
			InputStream file, int streamLength, StructuredData<K,? extends V> metadata) throws HabitatServiceException;
	
	/**
	 * Store the contents of an URL and its metadata
	 * 
	 * @param key
	 * @param value
	 * @param metadata
	 * @throws HabitatServiceException 
	 */
	public void storeUrlContents(String resource, K key, URL url, StructuredData<K,? extends V> metadata) throws HabitatServiceException;
	
	/**
	 * Transactionally store the contents of an URL and its metadata
	 * 
	 * @param key
	 * @param value
	 * @param metadata
	 * @throws HabitatServiceException 
	 */
	public void storeUrlContents(TransactionSupplier supplier, String resource, K key, URL url, StructuredData<K,? extends V> metadata) throws HabitatServiceException;

	/**
	 * Tests whether the keys and values are legal
	 * 
	 * @param metadata
	 * @return
	 */
	boolean isDataLegal(StructuredData<K, ? extends V> metadata);


}
