/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.provdm;

import java.io.IOException;
import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import edu.upenn.cis.db.habitat.core.type.provdm.Attribute.Type;

/**
 * 
 * @author John Frommeyer
 *
 */
public class AttributeTypeAdapterFactory implements TypeAdapterFactory {

	@Override
	public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
		if (!Attribute.class
				.isAssignableFrom(typeToken.getRawType())) {
			return null;
		}

		final TypeAdapter<JsonElement> jsonElementAdapter = gson
				.getAdapter(JsonElement.class);
		final ImmutableMap.Builder<Attribute.Type, TypeAdapter<? extends Attribute>> builder = ImmutableMap
				.builder();
		builder.put(Type.BOOLEAN,
				gson.getDelegateAdapter(this,
						new TypeToken<BooleanAttribute>() {
						}));
		builder.put(Type.LONG,
				gson.getDelegateAdapter(this, new TypeToken<LongAttribute>() {
				}));
		builder.put(Type.INTEGER,
				gson.getDelegateAdapter(this, new TypeToken<IntAttribute>() {
				}));
		builder.put(Type.DOUBLE,
				gson.getDelegateAdapter(this,
						new TypeToken<DoubleAttribute>() {
						}));
		builder.put(Type.STRING,
				gson.getDelegateAdapter(this,
						new TypeToken<StringAttribute>() {
						}));
		builder.put(Type.QUALIFIED_NAME,
				gson.getDelegateAdapter(this,
						new TypeToken<QualifiedNameAttribute>() {
						}));

		@SuppressWarnings("unchecked")
		final TypeAdapter<T> attributeTypeAdapter = (TypeAdapter<T>) new AttributeTypeAdapter(
				jsonElementAdapter,
				builder.build())
				.nullSafe();
		return attributeTypeAdapter;

	}

	private static class AttributeTypeAdapter extends
			TypeAdapter<Attribute> {

		private final TypeAdapter<JsonElement> jsonElementAdapter;
		private final Map<Attribute.Type, TypeAdapter<? extends Attribute>> attributeAdapterMap;

		public AttributeTypeAdapter(
				TypeAdapter<JsonElement> jsonElementAdapter,
				Map<Attribute.Type, TypeAdapter<? extends Attribute>> attributeAdapterMap) {
			this.jsonElementAdapter = jsonElementAdapter;
			this.attributeAdapterMap = attributeAdapterMap;
		}

		@SuppressWarnings("unchecked")
		@Override
		public void write(JsonWriter out,
				Attribute value)
				throws IOException {
			final TypeAdapter<? extends Attribute> adapter = attributeAdapterMap
					.get(value.getType());
			((TypeAdapter<Attribute>) adapter).write(out, value);
		}

		@Override
		public Attribute read(JsonReader in) throws IOException {
			final JsonObject attributeJson = jsonElementAdapter.read(in)
					.getAsJsonObject();

			final JsonElement typeJson = attributeJson.getAsJsonObject()
					.get(Attribute.TYPE_PROP_NAME);
			final Attribute.Type type = Attribute.Type.valueOf(typeJson
					.getAsString());
			final TypeAdapter<? extends Attribute> attributeAdapter = attributeAdapterMap
					.get(type);
			if (attributeAdapter == null) {
				throw new IllegalArgumentException(
						"unable to deserialize Attribute: "
								+ attributeJson);
			}
			final Attribute attribute = attributeAdapter
					.fromJsonTree(attributeJson);

			return attribute;

		}
	}

}
