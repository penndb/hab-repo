/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.core.expressions;

import java.util.Set;

import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;

public abstract class BinaryTuplePredicateWithLookup implements BinaryTuplePredicate {
	TupleWithSchema<String> left = null;
	TupleWithSchema<String> right = null;

	@Override
	public void bind(TupleWithSchema<String> left, TupleWithSchema<String> right) {
		this.left = left;
		this.right = right;
	}
	
	@Override
	public Object getValue(String fieldName) {
		Object o = left.getValue(fieldName);
		if (o == null) {
			o = right.getValue(fieldName);
			if (o == null && fieldName.endsWith("_2"))
				o = right.getValue(fieldName.substring(0, fieldName.length() - 2));
			
			return o;
		} else
			return o;
	}

	public boolean isLeftAttribute(String fieldName) {
		return left.getKeys().contains(fieldName);
	}

	public boolean isRightAttribute(String fieldName) {
		return right.getKeys().contains(fieldName) ||
				(fieldName.endsWith("_2") && 
						right.getKeys().contains(fieldName.substring(0, fieldName.length() - 2)));
	}

	public abstract Set<String> getSymbolsReferencedLeft();
	
	public abstract Set<String> getSymbolsReferencedRight();
}
