/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.model;

import static com.google.common.base.Preconditions.checkNotNull;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Set;

import edu.upenn.cis.db.habitat.core.type.Field;
import edu.upenn.cis.db.habitat.core.type.MultiField;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * @author John Frommeyer
 *
 */
@ApiModel(description = "a multi field", parent = FieldModel.class)
public final class MultiFieldModel<T> implements FieldModel {

	private static final String typeName = "MULTI";

	private final Type type = Type.valueOf(typeName);
	private final String name;
	private final Set<T> value;

	public MultiFieldModel(String name, Set<T> value) {
		this.name = checkNotNull(name);
		this.value = value;
	}

	@Override
	public Field<?> toField() throws HabitatServiceException {
		return new MultiField<>(value);
	}

	public static <S> MultiFieldModel<S> from(String name, MultiField<S> field) {
		return new MultiFieldModel<>(name,
				field.getValue());
	}

	@Override
	@ApiModelProperty(required = true, allowableValues = typeName,
			name = FieldModel.TYPE_PROP_NAME)
	public Type getType() {
		return type;
	}

	@Override
	@ApiModelProperty(required = true)
	public Set<T> getValue() {
		return value;
	}

	@Override
	@ApiModelProperty(required = true)
	public String getName() {
		return name;
	}
}
