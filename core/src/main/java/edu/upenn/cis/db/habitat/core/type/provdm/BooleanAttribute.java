/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.provdm;

import static com.google.common.base.Preconditions.checkNotNull;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import edu.upenn.cis.db.habitat.core.type.BooleanField;
import edu.upenn.cis.db.habitat.core.type.Field;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * @author John Frommeyer
 *
 */
@ApiModel(description = "a boolean-valued attribute", parent = Attribute.class)
public final class BooleanAttribute implements Attribute {

	private static final String typeName = "BOOLEAN";

	private final Type type = Type.valueOf(typeName);
	private final QualifiedName name;
	private final Boolean value;

	public BooleanAttribute(QualifiedName name, Boolean value) {
		this.name = checkNotNull(name);
		this.value = value;
	}

	public static BooleanAttribute from(QualifiedName name,
			BooleanField field) {
		return new BooleanAttribute(name,
				field.getValue());
	}

	@Override
	@ApiModelProperty(required = true, allowableValues = typeName,
			name = Attribute.TYPE_PROP_NAME)
	public Type getType() {
		return type;
	}

	@Override
	@ApiModelProperty(required = true)
	public Boolean getValue() {
		return value;
	}

	@Override
	@ApiModelProperty(required = true)
	public QualifiedName getName() {
		return name;
	}

	@Override
	public Field<Boolean> toField() throws HabitatServiceException {
		return new BooleanField(value);
	}
}
