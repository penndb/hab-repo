/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.api.mocks;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.StructuredValue;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

public class InMemoryProvAPI implements ProvenanceApi {
	final static Logger logger = LogManager.getLogger(InMemoryProvAPI.class);

	Map<ProvToken, ProvSpecifier> nodes = new HashMap<>();
	MultiValuedMap<ProvToken,Pair<ProvToken,String>> edgesFrom = new HashSetValuedHashMap<>();
	MultiValuedMap<ProvToken,Pair<ProvToken,String>> edgesTo = new HashSetValuedHashMap<>();

	Map<ProvToken, TupleWithSchema<String>> nodeTuples = new HashMap<>();
	MultiValuedMap<ProvToken,Pair<ProvToken,TupleWithSchema<?>>> edgeTuplesFrom = new HashSetValuedHashMap<>();
	MultiValuedMap<ProvToken,Pair<ProvToken,TupleWithSchema<?>>> edgeTuplesTo = new HashSetValuedHashMap<>();

	@Override
	public void storeProvenanceNode(String resource, ProvToken token, ProvSpecifier location) {
//		logger.debug("Node " + location + " @" + token);
		nodes.put(token, location);
	}

	@Override
	public ProvSpecifier getProvenanceLocation(String resource, ProvToken token) {
		return nodes.get(token);
	}

	@Override
	public void storeProvenanceLink(String resource, ProvToken from, String label, ProvToken to) {
//		logger.debug("Edge (@" + from + "," + label + ", @" + to + ")");
		edgesFrom.put(from, new ImmutablePair<ProvToken,String>(to, label));
		edgesTo.put(to, new ImmutablePair<ProvToken,String>(from, label));
	}

	@Override
	public Set<ProvToken> getConnectedTo(String resource, ProvToken to) {
		Collection<Pair<ProvToken,String>> edges = edgesTo.get(to);
		
		Set<ProvToken> ret = 
				edges.parallelStream()//.filter(item -> item.getRight().equals(label))
				.map(pair -> pair.getLeft()).collect(Collectors.toSet());
		return ret;
	}

	@Override
	public Set<ProvToken> getConnectedFrom(String resource, ProvToken from) {
		Collection<Pair<ProvToken,String>> edges = edgesFrom.get(from);
		
		Set<ProvToken> ret = 
				edges.parallelStream()//.filter(item -> item.getRight().equals(label))
				.map(pair -> pair.getLeft()).collect(Collectors.toSet());
		return ret;
	}

	@Override
	public Set<ProvToken> getConnectedTo(String resource, ProvToken to, String label) {
		Collection<Pair<ProvToken,String>> edges = edgesTo.get(to);
		
		Set<ProvToken> ret = 
				edges.parallelStream().filter(item -> item.getRight().equals(label))
				.map(pair -> pair.getLeft()).collect(Collectors.toSet());
		return ret;
	}

	@Override
	public Set<ProvToken> getConnectedFrom(String resource, ProvToken from, String label) {
		Collection<Pair<ProvToken,String>> edges = edgesFrom.get(from);
		
		Set<ProvToken> ret = 
				edges.parallelStream().filter(item -> item.getRight().equals(label))
				.map(pair -> pair.getLeft()).collect(Collectors.toSet());
		return ret;
	}

	@Override
	public void storeProvenanceLink(String resource, ProvToken fromLeft, ProvToken fromRight, String label, ProvToken to) {
		storeProvenanceLink(resource, fromLeft, label, to);
		storeProvenanceLink(resource, fromRight, label, to);
	}

	@Override
	public void storeProvenanceLink(String resource, List<ProvToken> inputProv, String label, ProvToken to) {
		for (ProvToken input: inputProv) {
			storeProvenanceLink(resource, input, label, to);
		}
		
	}

	@Override
	public void storeProvenanceNode(String resource, ProvToken token, TupleWithSchema<String> tuple, ProvSpecifier location) {
		storeProvenanceNode(resource, token, location);
		nodeTuples.put(token, tuple);
	}

	@Override
	public TupleWithSchema<String> getProvenanceData(String resource, ProvToken token) {
		return (TupleWithSchema<String>) nodeTuples.get(token);
	}

	@Override
	public void storeProvenanceLink(String resource, ProvToken from, String label, ProvToken to, TupleWithSchema<String> tuple) {
		edgeTuplesFrom.put(from, new ImmutablePair<>(to, tuple));
		edgeTuplesTo.put(to, new ImmutablePair<>(from, tuple));
	}

	@Override
	public void storeProvenanceLink(String resource, ProvToken fromLeft, ProvToken fromRight, String label, ProvToken to, 
			TupleWithSchema<String> tuple) {
		storeProvenanceLink(resource, fromLeft, label, to, tuple);
		storeProvenanceLink(resource, fromRight, label, to, tuple);
	}

	@Override
	public void storeProvenanceLink(String resource, List<ProvToken> inputProv, String label, ProvToken to,
			TupleWithSchema<String> tuple) throws HabitatServiceException {
		for (ProvToken input: inputProv) {
			storeProvenanceLink(resource, input, label, to, tuple);
		}
	}

	@Override
	public Iterable<TupleWithSchema<String>> getEdgesTo(String resource, ProvToken to) {
		Collection<Pair<ProvToken,TupleWithSchema<?>>> edges = edgeTuplesTo.get(to);
		
		@SuppressWarnings("unchecked")
		Set<TupleWithSchema<String>> ret = 
				edges.parallelStream()
				.map(pair -> (TupleWithSchema<String>)pair.getRight()).collect(Collectors.toSet());
		return ret;
	}

	@Override
	public Iterable<TupleWithSchema<String>> getEdgesFrom(String resource, ProvToken from) {
		Collection<Pair<ProvToken,TupleWithSchema<?>>> edges = edgeTuplesFrom.get(from);
		
		@SuppressWarnings("unchecked")
		Set<TupleWithSchema<String>> ret = 
				edges.parallelStream()
				.map(pair -> (TupleWithSchema<String>)pair.getRight()).collect(Collectors.toSet());
		return ret;
	}

	@Override
	public Map<String, StructuredData<String, Object>> getProvenanceNodes(
			String resource)
			throws HabitatServiceException {
		final Map<String, StructuredData<String, Object>> map = new HashMap<>();
		this.nodes.forEach((ProvToken t, ProvSpecifier s) -> {
			final StructuredData<String, Object> data = new StructuredValue<>();
			data.put("location", s);
			final TupleWithSchema<String> tuple = this.nodeTuples.get(t);
			if (tuple != null) {
				data.put("tuple", tuple);
			}
			map.put(t.toString(), data);
		});
		return map;
	}

	@Override
	public Map<String, List<TupleWithSchema<String>>> getEdgeMapFrom(
			String resource, 
			ProvToken from) throws HabitatServiceException {
		final Collection<Pair<ProvToken, TupleWithSchema<?>>> edgesFrom = edgeTuplesFrom.get(from);
		@SuppressWarnings("unchecked")
		final Map<String, List<TupleWithSchema<String>>> edgeMap =
				edgesFrom
				.parallelStream()
				.collect(groupingBy(pair -> pair.getLeft().toString(),
						mapping(pair -> (TupleWithSchema<String>)pair.getRight(),
						toList())));
		return edgeMap;
		
	}

	@Override
	public Map<String, List<TupleWithSchema<String>>> getEdgeMapTo(
			String resource, 
			ProvToken to) throws HabitatServiceException {
		final Collection<Pair<ProvToken, TupleWithSchema<?>>> edgesFrom = edgeTuplesTo.get(to);
		@SuppressWarnings("unchecked")
		final Map<String, List<TupleWithSchema<String>>> edgeMap =
				edgesFrom
				.parallelStream()
				.collect(groupingBy(pair -> pair.getLeft().toString(),
						mapping(pair -> (TupleWithSchema<String>)pair.getRight(),
						toList())));
		return edgeMap;		
	}

}
