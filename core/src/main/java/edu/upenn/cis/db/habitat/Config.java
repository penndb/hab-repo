/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.yaml.snakeyaml.Yaml;

/**
 * Basic config info, which uses a YAML file from the classpath.
 * The original versions are in src/test/resources and src/main/resources.
 * 
 * @author zives
 *
 */
public class Config {
	static Map<String, Object> config = null;

	final static Logger logger = LogManager.getLogger(Config.class);

	/**
	 * Nested map (replaces with a blank map if this doesn't exist)
	 * 
	 * @param map
	 * @param key
	 * @return
	 */
	public static Map<String, Object> getSubMap(Map<String, ?> map, String key) {
		@SuppressWarnings("unchecked")
		Map<String,Object> ret = (Map<String, Object>)map.get(key);
		
		if (ret == null)
			return new HashMap<String,Object>();
		else
			return ret;
	}
	
	/**
	 * Value from a leaf-level map.  If there is a ${XYZ} entry then look it up as
	 * a system environment variable.  Will throw a runtime exception if this isn't found.
	 * 
	 * @param map
	 * @param key
	 * @return
	 */
	public static String getValue(Map<String,?> map, String key) {
		if (!map.containsKey(key))
			//return "";
			throw new IllegalArgumentException("Key " + key + " not found in " + map.keySet());
		
		String val = map.get(key).toString();
		
		if (val.startsWith("${")) {
			String lookup = val.substring(2, val.lastIndexOf('}') - 1);
			if (lookup != null) {
				String ret = System.getenv(lookup);
				
				if (ret == null) {
					logger.error("Unable to find environment variable " + lookup);
					throw new RuntimeException("Cannot find required environment variable");
				}
			}
		}
		return val;
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String,Object> loadFromFileSystem(String path) throws IOException {
		final Path configPath = Paths.get(path);
		try (final Reader configReader = Files.newBufferedReader(configPath)) {
			Yaml yaml = new Yaml();
			return (Map<String,Object>) yaml.load(configReader);
		}
	}
	
	public static void setConfigFromFileSystem(String path) throws IOException {
		if (config == null) {
			config = loadFromFileSystem(path);
		}
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String,Object> loadFromStream(String path) throws FileNotFoundException {
		InputStream input = ClassLoader.class.getResourceAsStream(path);
		if (input == null)
			input = Config.class.getResourceAsStream(path);
	    Yaml yaml = new Yaml();
	    return (Map<String,Object>) yaml.load(input);
	}
	
	/**
	 * Parse the config YAML file.
	 * 
	 * @param path
	 * @return
	 */
	public static Map<String,Object> getConfig(String path) {
		if (config == null) {
			try {
				config = loadFromStream(path);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				config = new HashMap<String,Object>();
			}
		}
		
		return config;
	}
	
	public static Map<String,Object> getConfig() {
		return getConfig("/config.yaml");
	}
	
	public static String getPostgresUser() {
		return getValue(getRdbms(), "user");
	}
	public static String getRdbmsPassword() {
		return getValue(getRdbms(), "password");
	}
	public static String getRdbmsHost() {
		return getValue(getRdbms(), "host");
	}
	public static String getNeo4jUser() {
		return getValue(getGraph(), "user");
	}
	public static String getNeo4jPassword() {
		return getValue(getGraph(), "password");
	}
	public static String getNeo4jHost() {
		return getValue(getGraph(), "host");
	}
	public static String getProvGraph() {
		String prov = getValue(getGraph(), "prov");
		
		if (prov == null)
			return "graph";
		else
			return prov;
	}
	
	public static String getAwsUser() {
		return getValue(getBlob(), "user");
	}

	public static String getAwsPassword() {
		return getValue(getBlob(), "password");
	}

	public static String getAwsHost() {
		return getValue(getBlob(), "host");
	}

	public static String getAwsRegion() {
		return getValue(getBlob(), "region");
	}

	public static String getRdbmsNamespace() {
		return getValue(getRdbms(), "namespace");
	}
	
	public static Map<String,Object> getRdbms() {
		return getSubMap(getConfig(), "rdbms");
	}
	
	public static Map<String,Object> getGraph() {
		return getSubMap(getConfig(), "graph");
	}

	public static Map<String,Object> getBlob() {
		return getSubMap(getConfig(), "blob");
	}
	
	public static Map<String,Object> getQueryEngine() {
		return getSubMap(getConfig(), "query");
	}

	public static int getServerPort() {
		return Integer.valueOf(getValue(getSubMap(getConfig(), "server"), "port"));
	}

	public static int getMaxThreads() {
		return Integer.valueOf(getValue(getSubMap(getConfig(), "server"), "maxThreads"));
	}

	public static int getTimeoutMsec() {
		return Integer.valueOf(getValue(getSubMap(getConfig(), "server"), "timeoutMsec"));
	}

	public static String getSalt() {
		String salt = getValue(getSubMap(getConfig(), "server"), "salt");
		
		salt = salt + salt;
		if (salt == null || salt.isEmpty())
			salt = "abhdsfkj33ihgcsdf";
		else
			salt = "abhdsfkj33ihgcsdf" + salt;
		
		salt = salt + salt;
		salt = salt + salt;
		
		return salt;
	}
	
	public static String getServer() {
		try {
			String val = getValue(getSubMap(getConfig(), "server"), "server");
			return val + ":" + getServerPort();
		} catch (IllegalArgumentException ie) {
			return "localhost:" + getServerPort();
		}
	}
	
	public static String getServerUrl() {
		final String val = getServer();
		return "http://" + val;
	}
	
	public static String getExternalLocation() {
		final Map<String, Object> serverMap = getSubMap(getConfig(), "server");
		if (serverMap.containsKey("externalLocation")) {
			return getValue(serverMap, "externalLocation");
		}
		return null;
	}

	public static Integer getMaxLength() {
		return 255;
	}

	public static Integer getWriteBufferLength() {
		try {
			String wb = getValue(getQueryEngine(), "writeBuffer");
			if (wb != null && !wb.isEmpty())
				return Integer.valueOf(wb);
			else
				return -1;
		} catch (IllegalArgumentException ie) {
			return -1;
		}
	}
}
