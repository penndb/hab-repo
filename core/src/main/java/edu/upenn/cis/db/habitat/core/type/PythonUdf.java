package edu.upenn.cis.db.habitat.core.type;

import java.util.List;

import edu.upenn.cis.db.habitat.core.expressions.ArithmeticExpressionOperation;

public class PythonUdf<R,S,T> extends Udf<R,S,T> {
	String sourceCode;
	
	public PythonUdf(String name, BasicSchema argumentNames, 
			List<ArithmeticExpressionOperation<T>> arguments, BasicSchema outputSchema, String sourceCode) {
		super(name, argumentNames, arguments, outputSchema);
		
		this.sourceCode = sourceCode;
	}

	public String getSourceCode() {
		return sourceCode;
	}

	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}

	
}
