/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.sql.Date;
import edu.upenn.cis.db.habitat.core.type.Field;

public class DateField implements Field<Date> {
	Date value;
	
	DateField() {}
	
	public DateField(Date value) {
		this.value = value;
	}
	
	@JsonIgnore
	@Override
	public Class<Date> getType() {
		return Date.class;
	}

	@Override
	public Date getValue() {
		return value;
	}

	public void setValue(Date value) {
		this.value = value;
	}
	
	@Override
	public int hashCode() {
		return getValue().hashCode();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object o) {
		if (o instanceof Field)
			return getValue().equals(((Field<? extends Object>)o).getValue());
		else
			return getValue().equals(o);
	}
	
	@Override
	public String toString() {
		return getValue().toString();
	}
}
