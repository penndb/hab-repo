/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.webservice;

import spark.Route;
import spark.TemplateViewRoute;

public class RouteSpecifier {
	public static enum RequestType {GET, PUT, POST, DELETE};
	
	RequestType requestType;
	boolean isSecure;
	
	Route route = null;
	TemplateViewRoute templateRoute = null;
	
	
	
	public RouteSpecifier(RequestType requestType, boolean isSecure, Route route) {
		super();
		this.requestType = requestType;
		this.isSecure = isSecure;
		this.route = route;
	}
	
	
	public RouteSpecifier(RequestType requestType, boolean isSecure, TemplateViewRoute templateRoute) {
		super();
		this.requestType = requestType;
		this.isSecure = isSecure;
		this.templateRoute = templateRoute;
	}
	
	public RequestType getRequestType() {
		return requestType;
	}
	public void setRequestType(RequestType requestType) {
		this.requestType = requestType;
	}
	public boolean isSecure() {
		return isSecure;
	}
	public void setSecure(boolean isSecure) {
		this.isSecure = isSecure;
	}
	public Route getRoute() {
		return route;
	}
	public void setRoute(Route route) {
		this.route = route;
	}
	public TemplateViewRoute getTemplateRoute() {
		return templateRoute;
	}
	public void setTemplateRoute(TemplateViewRoute templateRoute) {
		this.templateRoute = templateRoute;
	}
	
	public boolean isTemplateRoute() {
		return this.templateRoute != null;
	}
	
	public boolean isRegularRoute() {
		return !isTemplateRoute();
	}
}
