package edu.upenn.cis.db.habitat.core.expressions;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;

public class ArithmeticExpressionVariable<T> implements ArithmeticExpressionOperation<T> {
	
	String name;
	Class<T> retType;
	TupleWithSchema<String> tuple = null;
	
	public ArithmeticExpressionVariable(Class<T> returnType, String name) {
		this.name = name;
		this.retType = returnType;
	}
	
	final static List<ArithmeticExpressionOperation<?>> nochildren = new ArrayList<>();

	@Override
	public void bind(TupleWithSchema<String> tuple) {
		this.tuple = tuple;
	}

	@Override
	public T getValue() {
		return (T) tuple.getValue(this.name);
	}

	@Override
	public List<ArithmeticExpressionOperation<?>> getChildren() {
		return nochildren;
	}

	@Override
	public Class<T> getType() {
		return retType;
	}

	public String toString() {
		return name;
	}

	@Override
	public ArithmeticOperation getOp() {
		return ArithmeticOperation.Variable;
	}

	@Override
	public Set<String> getSymbolsReferenced() {
		Set<String> ret = new HashSet<String>();
		ret.add(name);
		
		return ret;
	}

	@Override
	public void rename(Map<String, String> map) {
		if (map.containsKey(name))
			name = map.get(name);
	}
}
