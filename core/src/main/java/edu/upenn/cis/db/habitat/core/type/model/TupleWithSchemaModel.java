/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.core.type.model;

import static com.google.common.base.Preconditions.checkNotNull;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.Field;
import edu.upenn.cis.db.habitat.core.type.Schema;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * @author John Frommeyer
 *
 */
@ApiModel(description = "a tuple")
public class TupleWithSchemaModel {

	public static final String SCHEMA_NAME_PROP_NAME = "schemaName";
	public static final String SCHEMA_LOOKUP_KEYS_PROP_NAME = "lookupKeys";
	public static final String TUPLE_PROP_NAME = "tuple";

	@ApiModelProperty(required = true, name = SCHEMA_NAME_PROP_NAME)
	public final String schemaName;
	
	@ApiModelProperty(required = true, name = SCHEMA_LOOKUP_KEYS_PROP_NAME)
	public final List<String> lookupKeys;

	@ApiModelProperty(required = true, name = TUPLE_PROP_NAME)
	public final List<FieldModel> tuple;

	public TupleWithSchemaModel(
			String schema,
			List<String> lookupKeys,
			List<FieldModel> tuple) {
		this.tuple = ImmutableList.copyOf(tuple);
		this.schemaName = checkNotNull(schema);
		this.lookupKeys = ImmutableList.copyOf(lookupKeys);
	}

	public TupleWithSchema<String> toTupleWithSchema()
			throws HabitatServiceException {
		final List<Field<? extends Object>> fields = new ArrayList<>();
		final List<String> fieldNames = new ArrayList<>();
		final List<Class<?>> types = new ArrayList<>();
		for (FieldModel fieldModel : tuple) {
			fields.add(fieldModel.toField());
			fieldNames.add(fieldModel.getName());
			types.add(fieldModel.getType().javaType);
		}
		final BasicSchema basicSchema = new BasicSchema(
				schemaName,
				fieldNames,
				types);
		basicSchema.setLookupKeys(lookupKeys);
		final TupleWithSchema<String> tupleWithSchema = new BasicTuple(
				basicSchema,
				fields);
		return tupleWithSchema;
	}

	public static TupleWithSchemaModel from(TupleWithSchema<String> tuple) {
		final Builder<FieldModel> builder = ImmutableList.builder();
		final Schema<String, Class<? extends Object>> schema = tuple
				.getSchema();
		final String schemaName = schema.getName();
		final List<String> lookupKeys = schema.getLookupKeys();
		for (final String key : schema.getKeys()) {
			final Field<? extends Object> field = tuple.get(key);
			final FieldModel fieldModel = FieldModel.from(key, field);
			builder.add(fieldModel);
		}
		final TupleWithSchemaModel model = new TupleWithSchemaModel(
				schemaName,
				lookupKeys,
				builder.build());
		return model;
	}

}
