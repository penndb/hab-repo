/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.test;

import java.net.URISyntaxException;
import java.net.URL;

/**
 * @author John Frommeyer
 *
 */
public class Util {

	public static String getResourcePath(
			ClassLoader classLoader,
			String resource) {
		// Convert URL to URI to decode URL encoding. For example convert spaces
		// back to spaces from '%20'.
		final URL resourceUrl = classLoader.getResource(resource);
		try {
			String path = resourceUrl.toURI().getPath();
			
			// Windows path like "/C:/"
			if (path.length() > 3 && path.charAt(2) == ':') {
				path = path.substring(1);
			}
			return path;
		} catch (URISyntaxException e) {
			throw new IllegalArgumentException("not a valid URI: ["
					+ resourceUrl + "]");
		}
	}

	private Util() {
		throw new Error("Don't instantiate me");
	}

}
