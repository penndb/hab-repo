/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.auth;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Map;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;

import edu.upenn.cis.db.habitat.auth.backends.plugins.AuthModule;
import edu.upenn.cis.db.habitat.auth.backends.plugins.HabitatRequestScopeModule;
import edu.upenn.cis.db.habitat.auth.backends.plugins.SimpleScope;
import edu.upenn.cis.db.habitat.auth.backends.plugins.TemporaryDbModule;
import edu.upenn.cis.db.habitat.core.api.PermissionApi;
import edu.upenn.cis.db.habitat.core.api.UserApi;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.secure.ProtectedAuthBackend;

public class TestBackend {
	UserApiLocal backend;
	PermissionApi perms;
	
	private SimpleScope habitatRequestScope;
	
	@Before
	public void setUp() throws Exception {
		Injector injector = Guice.createInjector(
				new AuthModule(),
				new TemporaryDbModule(),
				new HabitatRequestScopeModule()
				);

		habitatRequestScope = injector
				.getInstance(Key.get(
						SimpleScope.class,
						Names.named(HabitatRequestScopeModule.HABITAT_REQUEST_SCOPE_NAME)));

		backend = injector.getInstance(ProtectedAuthBackend.class);

		perms = injector.getInstance(PermissionApi.class);

		habitatRequestScope
				.makeScopedRequest(
						UserApi.Root,
						() -> {
							backend.createTables();
						});
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Validate that we can add users locally, with salt etc.
	 */
	@Test
	public void testLocalUserRegistration() {
		try {
			habitatRequestScope
					.makeScopedRequest(
							UserApi.Root,
							() -> {
								backend.addUserAndOrganization("#user",
										"first",
										"last",
										"street1", "street2", "city", "state",
										"zip", "country", "phone", "userEmail",
										"title", "organization", "orgPhone",
										"orgWeb");

								backend.getOrganizationIdFrom("organization");

								backend.addCredential("#user", "local", "http",
										"123");

								assertTrue(backend
										.isUserValid("#user", "local"));
								assertFalse(backend.isUserValid("#user",
										"remote"));
							});
		} catch (HabitatServiceException e) {
			e.printStackTrace();
			fail();
		}
	}

	/**
	 * Validate that we can authorize users who were remotely authenticated
	 */
	@Test
	public void testDelegateRegistration() {
		try {
			habitatRequestScope
			.makeScopedRequest(
					UserApi.Root,
					() -> {
						backend.addUserAndOrganization("#user3", "first",
								"last",
								"street1", "street2", "city", "state",
								"zip", "country", "phone", "userEmail",
								"title", "organization", "orgPhone", "orgWeb");
						backend.addCredential("#user3", "local", "http", "pass");
						assertTrue(backend.isUserValid("#user3", "local"));

						backend.getOrganizationIdFrom("organization");

						backend.addCredential("#user3", "fake", "http", "123");

						assertTrue(backend.isUserValid("#user3", "fake"));
						assertTrue(backend.isUserValid("#user3", "fake"));
					});
		} catch (HabitatServiceException e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testPermissions() throws HabitatServiceException {
		habitatRequestScope
		.makeScopedRequest(
				UserApi.Root,
				() -> {
					assertTrue(backend.addUserAndOrganization("#user7",
							"first", "last",
							"street1", "street2", "city", "state",
							"zip", "country", "phone", "userEmail", "title",
							"organization", "orgPhone", "orgWeb"));
					assertTrue(backend.getUserIdFrom("#user7") > -1);
				});
		
		
		perms.grantUserPermissionOn("12345", "perm1", "#user7");
		
		Set<String> permSet = perms.getUserPermissionsOn("12345", "#user7");
		
		assertTrue(permSet.size() == 1);
		assertTrue(permSet.contains("perm1"));
		
		final Set<String> user7Perm1Objects = perms.getObjectIds("perm1", "#user7");
		assertEquals(1, user7Perm1Objects.size());
		assertTrue(user7Perm1Objects.contains("12345"));
		
		final Set<String> user7NotAPermObjects = perms.getObjectIds("notAPerm", "#user7");
		assertTrue(user7NotAPermObjects.isEmpty());
	}
	
	@Test
	public void getObjects() throws HabitatServiceException {
		final String authzedUser = "authzedUser";
		final String unauthzedUser = "unauthzedUser";
		habitatRequestScope
		.makeScopedRequest(
				UserApi.Root,
				() -> {
					assertTrue(backend.addUserAndOrganization(authzedUser,
							"first", "last",
							"street1", "street2", "city", "state",
							"zip", "country", "phone", "userEmail", "title",
							"organization", "orgPhone", "orgWeb"));
					assertTrue(backend.getUserIdFrom(authzedUser) > -1);
					
					assertTrue(backend.addUserAndOrganization(unauthzedUser,
							"first", "last",
							"street1", "street2", "city", "state",
							"zip", "country", "phone", "userEmail", "title",
							"organization", "orgPhone", "orgWeb"));
					assertTrue(backend.getUserIdFrom(unauthzedUser) > -1);
				});
		
		
		final String adminObject = "adminObject";
		final String readObject = "readObject";
		
		perms.grantUserPermissionOn(adminObject, PermissionApi.AdminPermission, authzedUser);
		perms.grantUserPermissionOn(readObject, PermissionApi.ReadPermission, authzedUser);
		

		final Set<String> actualAdminObjects = perms.getObjectIds(PermissionApi.AdminPermission, authzedUser);
		assertEquals(1, actualAdminObjects.size());
		assertTrue(actualAdminObjects.contains(adminObject));
		
		final Set<String> actualReadObjects = perms.getObjectIds(PermissionApi.ReadPermission, authzedUser);
		assertEquals(2, actualReadObjects.size());
		assertTrue(actualReadObjects.contains(adminObject));
		assertTrue(actualReadObjects.contains(readObject));
		
		final Set<String> unauthzedUserObjects = perms.getObjectIds(PermissionApi.AdminPermission, unauthzedUser);
		assertTrue(unauthzedUserObjects.isEmpty());
	}
	
	@Test
	public void testGroups() throws HabitatServiceException {
		habitatRequestScope.makeScopedRequest(UserApi.Root,
				() -> {
					backend.addUserAndOrganization("#user6", "first", "last",
							"street1", "street2", "city", "state",
							"zip", "country", "phone", "userEmail", "title",
							"organization", "orgPhone", "orgWeb");
					backend.addCredential("#user6", "local", "http", "pass");

					int orgId = backend.getOrganizationIdFrom("organization");

					int group1 = backend.addGroup(orgId, "Group1", null);
					int group2 = backend.addGroup(orgId, "Group2", null);
					int group3 = backend.addGroup(orgId, "Group3", null);

					// Group2 inside group1
				backend.addSubgroupToGroup("Group2", "Group1");

				backend.addUserToGroup("#user6", "Group2");

				Set<String> groups = backend.getGroupsForUser("#user6");
				Set<String> parents = backend.getParentGroups("Group2");

				assertTrue(groups.contains("Group2"));
				assertTrue(parents.contains("Group1"));

				assertTrue(backend.isUserInGroup("#user6", group2));
				assertTrue(backend.isUserInGroup("#user6", group1));
				assertFalse(backend.isUserInGroup("#user6", group3));
			});
	}
	
	
	@Test
	public void testAdminUpdateAccounts() throws HabitatServiceException {
		habitatRequestScope.makeScopedRequest(
				UserApi.Root,
				() -> {
					backend.addUserAndOrganization("#user4", "first", "last",
							"street1", "street2", "city", "state",
							"zip", "country", "phone", "userEmail", "title",
							"organization", "orgPhone", "orgWeb");
					backend.addCredential("#user4", "local", "http", "pass");
					backend.addUserAndOrganization("#user5", "first", "last",
							"street1", "street2", "city", "state",
							"zip", "country", "phone", "userEmail", "title",
							"organization", "orgPhone", "orgWeb");
					backend.addCredential("#user5", "local", "http", "pass");

					int orgId = backend.getOrganizationIdFrom("organization");
					Map<String, Object> properties = backend
							.getUserInfo("#user4");
					assertTrue((Integer) properties.get("organization") == orgId);
				});


		habitatRequestScope.makeScopedRequest(
				"#user4",
				() -> {
					backend.updateUser("#user4", "first2", "last",
							"street1", "street2", "city", "state",
							"zip", "country", "phone", "userEmail", "title",
							"organization2");

					Map<String, Object> properties = backend
							.getUserInfo("#user4");

					int orgId = backend.getOrganizationIdFrom("organization2");
					assertTrue((Integer) properties.get("organization") == orgId);

					// We should not have permissions to update this account
					try {
						backend.updateUser("#user5", "first2", "last",
								"street1", "street2", "city", "state",
								"zip", "country", "phone", "userEmail",
								"title", "organization2");

						fail();
					} catch (Exception e) {

					}
				});
	}
}
