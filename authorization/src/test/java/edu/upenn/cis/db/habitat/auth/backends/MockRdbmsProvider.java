/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.auth.backends;

import java.sql.SQLException;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.jooq.tools.jdbc.MockConnection;
import org.jooq.tools.jdbc.MockDataProvider;
import org.jooq.tools.jdbc.MockExecuteContext;
import org.jooq.tools.jdbc.MockResult;

import edu.upenn.cis.db.habitat.auth.providers.RdbmsProvider;

public class MockRdbmsProvider implements RdbmsProvider {

	@Override
	public DSLContext get() {
		MockDataProvider provider = new MockDataProvider() {

			@Override
			public MockResult[] execute(MockExecuteContext ctx) throws SQLException {
				// TODO Auto-generated method stub
				return new MockResult[0];
			}
			
		};
		
		MockConnection connection = new MockConnection(provider);
		
		DSLContext context = DSL.using(connection, SQLDialect.POSTGRES);
		
		return context;
	}

}
