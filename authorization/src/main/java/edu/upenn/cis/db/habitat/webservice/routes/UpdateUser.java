/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;

import edu.upenn.cis.db.habitat.auth.AuthServices;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;
import edu.upenn.cis.db.habitat.secure.ProtectedAuthBackend;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;
import spark.Request;
import spark.Response;
import spark.Route;

@Api(tags = AuthServices.PLUGIN_NAME)
@Path("/auth/credentials/{username}")
@Produces("application/json")
public class UpdateUser implements Route {
	final static Logger logger = LogManager.getLogger(UpdateUser.class);
	
	ProtectedAuthBackend backend;
	AuthServices caller;
	
	public UpdateUser(ProtectedAuthBackend backend, AuthServices services) {
		this.backend = backend;
		caller = services;
	}
	
	/**
	 * Updates an existing user, using the parameters in the input map
	 * 
	 * @param reqMap
	 * @param response
	 * @return ResponseError if there is an issue in creating the user
	 * @throws HabitatServiceException 
	 */
	private Object updateUser(Map<String,Object> reqMap, Response response) throws HabitatServiceException {
		if (reqMap.get("username") == null || ((String)reqMap.get("username")).isEmpty() ||
				reqMap.get("password") == null || ((String)reqMap.get("password")).isEmpty() ||
				reqMap.get("email") == null  || ((String)reqMap.get("email")).isEmpty()) {
			response.status(400);
			return new ResponseError("Missing required username, email, and/or password");
		}
		
		if (backend.updateUser(
				(String)reqMap.get("username"), 
				(String)reqMap.get("firstname"), 
				(String)reqMap.get("lastname"), 
				(String)reqMap.get("street1"), 
				(String)reqMap.get("street2"), 
				(String)reqMap.get("city"), 
				(String)reqMap.get("state"), 
				(String)reqMap.get("zip"), 
				(String)reqMap.get("country"), 
				(String)reqMap.get("phone"), 
				(String)reqMap.get("primaryemail"), 
				(String)reqMap.get("title"), 
				(String)reqMap.get("organization"))) {
			

			reqMap.remove("password");
			return reqMap;
		} else {
			response.status(401);
			return new ResponseError("Unable to create user " + reqMap.get("username") + " -- perhaps this is an illegal name or the account exists?");
		}
	}
	
	@PUT
	@ApiOperation(value = "Updates user properties", nickname="UpdateUser",
			authorizations = { @Authorization(value="jwt")})
	@ApiImplicitParams({ //
		@ApiImplicitParam(required = true, dataType="string", name="username", paramType = "path"), //
		@ApiImplicitParam(
				required = true, 
				dataTypeClass=AuthTypes.UserInfo.class, 
				name="userfields",
				paramType = "body",
				examples= @Example(value = {
						@ExampleProperty(mediaType="application/json",
								value="{email:'my@mail',password='password',firstname='Bob',lastname='Smith',organization='Penn'}")})), //
	}) //
	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "Success", response=AuthTypes.UserInfo.class), //
			@ApiResponse(code = 400, message = "Invalid input data", response=ResponseError.class), //
			@ApiResponse(code = 401, message = "Unauthorized", response=ResponseError.class), //
	})
	@Override
	public Object handle(
			@ApiParam(hidden=true) Request request, 
			@ApiParam(hidden=true) Response response) throws Exception {
    	String username = request.params("username");

    	logger.debug("Request to add user " + username);
    	logger.debug(request.body());
    	Map<String,Object> map = caller.getProfileMap(request, response);
		
		request.session(true);
		map.put("sessionId", request.session().id());

		if (!request.contentType().equals("application/json")) {
			throw new UnsupportedOperationException();
		}
		@SuppressWarnings("unchecked")
		Map<String, Object> reqMap = new Gson().fromJson(request.body(), HashMap.class);
		reqMap.putAll(map);
		reqMap.put("username", username);
		
    	Map<String,Object> info = caller.getProfileMap(request, response);
    	if (info.containsKey("username")) {
    		if (!backend.isAdminFor((String)info.get("username"), username)) {
        		response.status(401);
        		return new ResponseError("Can only update properties as admin or on own account -- " + username + " doesn't match " + info.get("username"));
    		}
    	} else {
    		response.status(401);
    		return new ResponseError("Cannot find credentials -- are you logged in?");
    	}
		

		return updateUser(reqMap, response);
	}

}
