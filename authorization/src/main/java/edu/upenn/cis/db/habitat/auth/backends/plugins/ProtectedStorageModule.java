/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.auth.backends.plugins;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.name.Names;

import edu.upenn.cis.db.habitat.core.api.StorageApi;
import edu.upenn.cis.db.habitat.core.api.StorageApiWithMetadata;
import edu.upenn.cis.db.habitat.repository.storage.GraphStore;
import edu.upenn.cis.db.habitat.repository.storage.StorageApiLocal;
import edu.upenn.cis.db.habitat.repository.storage.StorageApiWithMetadataLocal;
import edu.upenn.cis.db.habitat.secure.ProtectedGraphStore;
import edu.upenn.cis.db.habitat.secure.ProtectedStorageBackend;

/**
 * @author John Frommeyer
 *
 */
public class ProtectedStorageModule extends AbstractModule {

	private Class<? extends GraphStore<String>> unprotectedGraphStoreClass;
	private Class<? extends StorageApiWithMetadataLocal<String, Object, ?>> unprotectedStorageApiClass;

	public ProtectedStorageModule(
			Class<? extends GraphStore<String>> unprotectedGraphStoreImplClass) {
		this(unprotectedGraphStoreImplClass, unprotectedGraphStoreImplClass);
	}

	public ProtectedStorageModule(
			Class<? extends GraphStore<String>> unprotectedGraphStoreImplClass,
			Class<? extends StorageApiWithMetadataLocal<String, Object, ?>> unprotectedStorageApiClass) {
		this.unprotectedGraphStoreClass = checkNotNull(unprotectedGraphStoreImplClass);
		this.unprotectedStorageApiClass = checkNotNull(unprotectedStorageApiClass);
	}

	@Override
	protected void configure() {
		bind(new TypeLiteral<GraphStore<String>>() {
		}).to(new TypeLiteral<ProtectedGraphStore<String>>() {
		});

		bind(new TypeLiteral<GraphStore<String>>() {
		})
				.annotatedWith(
						Names.named(ProtectedGraphStore.UNPROTECTED_GRAPH_STORE))
				.to(unprotectedGraphStoreClass);
		
		bind(new TypeLiteral<StorageApi<String, Object, ?>>() {
		}).to(new TypeLiteral<ProtectedStorageBackend<String, Object, ?>>() {
		});
		
		bind(new TypeLiteral<StorageApiLocal<String, Object, ?>>() {
		}).to(new TypeLiteral<ProtectedStorageBackend<String, Object, ?>>() {
		});
		
		bind(new TypeLiteral<StorageApiWithMetadata<String, Object, ?>>() {
		}).to(new TypeLiteral<ProtectedStorageBackend<String, Object, ?>>() {
		});
		
		bind(new TypeLiteral<StorageApiWithMetadataLocal<String, Object, ?>>() {
		}).to(new TypeLiteral<ProtectedStorageBackend<String, Object, ?>>() {
		});

		bind(new TypeLiteral<StorageApiWithMetadataLocal<String, Object, ?>>() {
		})
				.annotatedWith(
						Names.named(ProtectedStorageBackend.UNPROTECTED_STORAGE_API))
				.to(unprotectedStorageApiClass);
	}
}
