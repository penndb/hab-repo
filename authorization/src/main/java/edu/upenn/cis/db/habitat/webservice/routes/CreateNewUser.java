/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.exception.DataAccessException;

import com.google.gson.Gson;

import edu.upenn.cis.db.habitat.Config;
import edu.upenn.cis.db.habitat.auth.AuthServices;
import edu.upenn.cis.db.habitat.core.api.PermissionApi;
import edu.upenn.cis.db.habitat.core.api.UserApi;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;
import spark.Request;
import spark.Response;
import spark.Route;

@Api(tags = AuthServices.PLUGIN_NAME)
@Path("/auth/credentials/{username}")
@Produces("application/json")
public class CreateNewUser implements Route {
	final static Logger logger = LogManager.getLogger(CreateNewUser.class);
	
	UserApi backend;
	AuthServices caller;
	PermissionApi perms;
	
	public CreateNewUser(UserApi backend, PermissionApi perms, AuthServices services) {
		this.backend = backend;
		this.perms = perms;
		caller = services;
	}
	
	/**
	 * Adds a new user, using the parameters in the input map
	 * 
	 * @param reqMap
	 * @param response
	 * @return ResponseError if there is an issue in creating the user
	 */
	private Object addNewUser(Map<String,Object> reqMap, Response response) {
		if (reqMap.get("username") == null || ((String)reqMap.get("username")).isEmpty() ||
				reqMap.get("password") == null || ((String)reqMap.get("password")).isEmpty() ||
				reqMap.get("email") == null  || ((String)reqMap.get("email")).isEmpty()) {
			response.status(400);
			return new ResponseError("Missing required username, email, and/or password");
		}
		
		if (caller.addUser(reqMap,
				(String)reqMap.get("service"), 
				(String)reqMap.get("token"))) {
			try {
				backend.addCredential((String)reqMap.get("username"), 
						"local", 
						"http",
						(String)reqMap.get("password"));
				
				// Grant them permission on the default provenance graphs
				perms.grantUserPermissionOn("", PermissionApi.WritePermission, (String)reqMap.get("username"));
				perms.grantUserPermissionOn(Config.getProvGraph(), PermissionApi.WritePermission, (String)reqMap.get("username"));
			} catch (DataAccessException | HabitatServiceException dae) {
				logger.debug(dae.getMessage());
				response.status(401);
				return new ResponseError("Unable to create user " + reqMap.get("username") + " -- perhaps this is an illegal name or the account exists?");
			}
			try {
				backend.addCredential((String)reqMap.get("username"), 
					"google", 
					"http",
					null);
			} catch (DataAccessException | HabitatServiceException dae) {
				logger.debug(dae.getMessage());
				response.status(401);
				return new ResponseError("Unable to authorize user " + reqMap.get("username") + " for Google");
			}
			try {
				backend.addCredential((String)reqMap.get("username"), 
					"github", 
					"http",
					null);
			} catch (DataAccessException | HabitatServiceException dae) {
				logger.debug(dae.getMessage());
				response.status(401);
				return new ResponseError("Unable to authorize user " + reqMap.get("username") + " for GitHub");
			}
			reqMap.remove("password");
			return reqMap;
		} else {
			response.status(401);
			return new ResponseError("Unable to create user " + reqMap.get("username") + " -- perhaps this is an illegal name or the account exists?");
		}
	}
	
	@POST
	@ApiOperation(value = "Creates a new user", nickname="CreateNewUser")
	@ApiImplicitParams({ //
		@ApiImplicitParam(required = true, dataType="string", name="username", paramType = "path"), //
		@ApiImplicitParam(
				required = true, 
				dataTypeClass=AuthTypes.UserInfo.class, 
				name="userfields",
				paramType = "body",
				examples= @Example(value = {
						@ExampleProperty(mediaType="application/json",
								value="{email:'my@mail',password='password',firstname='Bob',lastname='Smith',organization='Penn'}")})), //
	}) //
	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "Success", response=AuthTypes.UserInfo.class), //
			@ApiResponse(code = 400, message = "Invalid input data", response=ResponseError.class), //
			@ApiResponse(code = 401, message = "Unauthorized", response=ResponseError.class), //
	})
	@Override
	public Object handle(
			@ApiParam(hidden=true) Request request, 
			@ApiParam(hidden=true) Response response) throws Exception {
    	String username = request.params("username");

    	logger.debug("Request to add user " + username);
    	logger.debug(request.body());
    	Map<String,Object> map = caller.getProfileMap(request, response);
		
		request.session(true);
		map.put("sessionId", request.session().id());

		if (!request.contentType().equals("application/json")) {
			throw new UnsupportedOperationException();
		}
		@SuppressWarnings("unchecked")
		Map<String, Object> reqMap = new Gson().fromJson(request.body(), HashMap.class);
		reqMap.putAll(map);
		reqMap.put("username", username);

		return addNewUser(reqMap, response);
	}

}
