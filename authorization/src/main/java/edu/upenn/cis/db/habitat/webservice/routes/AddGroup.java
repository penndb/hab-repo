/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import java.util.Map;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;

import edu.upenn.cis.db.habitat.auth.AuthServices;
import edu.upenn.cis.db.habitat.core.api.UserApi;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;
import edu.upenn.cis.db.habitat.webservice.routes.AuthTypes.GroupDetails;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;
import spark.Request;
import spark.Response;
import spark.Route;

@Api(tags = AuthServices.PLUGIN_NAME)
@Path("/auth/organizations/{organization}/group/{group}")
@Produces("application/json")
public class AddGroup implements Route {
	final static Logger logger = LogManager.getLogger(AddGroup.class);
	
	UserApi backend;
	AuthServices caller;
	
	public AddGroup(UserApi backend, AuthServices services) {
		this.backend = backend;
		caller = services;
	}
	
	@POST
	@ApiOperation(value = "Adds a permissions group to an organization", nickname="AddGroup",
			authorizations = { @Authorization(value="jwt")})
	@ApiImplicitParams({ //
		@ApiImplicitParam(required = true, dataType="integer", name="organization", paramType = "path"), //
		@ApiImplicitParam(required = true, dataType="string", name="group", paramType = "path"), //
		@ApiImplicitParam(
				required = true, 
				dataTypeClass=AuthTypes.GroupDetails.class, 
				name="details",
				paramType = "body",
				examples= @Example(value = {
						@ExampleProperty(mediaType="application/json",
								value="{parentGroupId:1}")})), //
	}) //
	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "Success", response=Integer.class), //
			@ApiResponse(code = 400, message = "Invalid input data", response=ResponseError.class), //
			@ApiResponse(code = 401, message = "Unauthorized", response=ResponseError.class), //
	})
	@Override
	public Object handle(
			@ApiParam(hidden=true) Request request, 
			@ApiParam(hidden=true) Response response) throws Exception {
    	Integer orgId = Integer.valueOf(request.params("organization"));
    	String groupname = request.params("group");
    	
    	if (orgId == null) {
    		response.status(400);
    		return new ResponseError("Illegal organization specified");
    	}

    	logger.debug("Request to add group " + groupname + " to organization " + orgId);
    	logger.debug(request.body());

    	GroupDetails details = null;

		try {
			details = new Gson().fromJson(request.body(), GroupDetails.class);
		} catch (Exception e) {
			response.status(400);
			return new ResponseError("Unable to parse message body for group details");
		}
		
		if (groupname == null || groupname.isEmpty()) {
			response.status(400);
			return new ResponseError("Invalid group name");
		}
		
    	Map<String,Object> info = caller.getProfileMap(request, response);
    	if (info.containsKey("organization")) {
    		if (!info.get("organization").equals(orgId)) {
        		response.status(401);
        		return new ResponseError("Can only add groups to your own organization");
    		}
    	} else {
    		response.status(401);
    		return new ResponseError("Cannot find credentials -- are you logged in?");
    	}
    	
    	int id = backend.addGroup(orgId, groupname, details.parentGroup); 
    	if (id >= 0) {
    		response.body(Integer.toString(id));
    		return true;
    	} else {
    		response.status(401);
    		return new ResponseError("Unable to create group " + groupname);
    	}
	}

}
