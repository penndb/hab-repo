/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.auth;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.exception.DataAccessException;
import org.pac4j.core.profile.CommonProfile;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.inject.Inject;

import edu.upenn.cis.db.habitat.Config;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;
import edu.upenn.cis.db.habitat.core.webservice.RestPlugin;
import edu.upenn.cis.db.habitat.core.webservice.RouteSpecifier;
import edu.upenn.cis.db.habitat.core.webservice.RouteSpecifier.RequestType;
import edu.upenn.cis.db.habitat.util.HtmlPost;
import edu.upenn.cis.db.habitat.util.SparkUserProfiles;

/**
 * Main API for user-oriented services.
 * 
 * @author zives
 *
 */
public class AuthBrowserServices implements RestPlugin {
	final static Logger logger = LogManager.getLogger(AuthBrowserServices.class);
	
	final private UserApiLocal backend;

	private final TemplateViewRoute addUserRoute = (Request request, Response response) -> {
		logger.debug(request.body());
		Map<String, Object> map = getProfileMap(request,
				response);

		request.session(true);
		map.put("sessionId", request.session().id());

		Map<String, Object> reqMap;
		if (request.contentType().equals(
				"application/x-www-form-urlencoded")) {
			reqMap = HtmlPost.asMap(request.body());
		} else if (request.contentType().equals(
				"application/json")) {
			throw new UnsupportedOperationException();
		} else
			throw new UnsupportedOperationException();

		reqMap.putAll(map);
		
		Object retResult = addNewUser(reqMap, response);

		if (retResult instanceof ResponseError) {
			return new ModelAndView(reqMap,
					"loginForm.mustache");
		} else {
			addVisualizationText(reqMap);
			return new ModelAndView(reqMap,
					"protectedIndex.mustache");
		}
	};

	private final TemplateViewRoute googleLoginRoute = (Request request, Response response) -> {
		Map<String,Object> map = getProfileMap(request, response);
		map.put("service", "google");
		
		request.session(true);
		map.put("sessionId", request.session().id());
		if (!existsInService((String)map.get("username"), 
				"google")) {
			map.put("callbackUrl", "/web/addUser");
			return new ModelAndView(map, "loginForm.mustache");
		} else {
			addVisualizationText(map);
			return new ModelAndView(map, "protectedIndex.mustache");
		}
	};

	private final TemplateViewRoute signupRoute = (Request request, Response response) -> {
		Map<String,Object> map = getProfileMap(request, response);
			
		map.put("callbackUrl", "/web/addUser");
		return new ModelAndView(map, "loginForm.mustache");
	};
	
	@Inject public AuthBrowserServices(UserApiLocal backend) {
		this.backend = backend;
	}

	public boolean addUser(Map<String,Object> request, String service, String token) {
		try {
			if (!backend.addUserAndOrganization((String)request.get("username"), 
					(String)request.get("firstname"), 
					(String)request.get("lastname"), 
					(String)request.get("street1"), 
					(String)request.get("street2"), 
					(String)request.get("city"), 
					(String)request.get("state"), 
					(String)request.get("zip"), 
					(String)request.get("country"), 
					(String)request.get("phone"), 
					(String)request.get("email"), 
					(String)request.get("title"), 
					(String)request.get("organization"),
					"", ""))
					return false;

			if (service != null && token != null)
				backend.addCredential((String)request.get("username"), service, 
					"http", token);
		} catch (HabitatServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return true;
		}
		return true;
	}
	
	/**
	 * Adds a new user, using the parameters in the input map
	 * 
	 * @param reqMap
	 * @param response
	 * @return ResponseError if there is an issue in creating the user
	 */
	private Object addNewUser(Map<String,Object> reqMap, Response response) {
		if (reqMap.get("username") == null || ((String)reqMap.get("username")).isEmpty() ||
				reqMap.get("password") == null || ((String)reqMap.get("password")).isEmpty() ||
				reqMap.get("email") == null || ((String)reqMap.get("email")).isEmpty())
			throw new IllegalArgumentException("Missing required username and/or password");
		
		if (addUser(reqMap,
				(String)reqMap.get("service"), 
				(String)reqMap.get("token"))) {
			try {
				backend.addCredential((String)reqMap.get("username"), 
						"local", 
						"http",
						(String)reqMap.get("password"));
			} catch (DataAccessException | HabitatServiceException dae) {
				logger.debug(dae.getMessage());
				return new ResponseError("Unable to create user " + reqMap.get("username") + " -- perhaps this is an illegal name or the account exists?");
			}
			try {
				backend.addCredential((String)reqMap.get("username"), 
					"google", 
					"http",
					null);
			} catch (DataAccessException | HabitatServiceException dae) {
				logger.debug(dae.getMessage());
				return new ResponseError("Unable to authorize user " + reqMap.get("username") + " for Google");
			}
			try {
				backend.addCredential((String)reqMap.get("username"), 
					"github", 
					"http",
					null);
			} catch (DataAccessException | HabitatServiceException dae) {
				logger.debug(dae.getMessage());
				return new ResponseError("Unable to authorize user " + reqMap.get("username") + " for GitHub");
			}
			return reqMap;
		} else
			return new ResponseError("Unable to create user " + reqMap.get("username") + " -- perhaps this is an illegal name or the account exists?");
	}
	
	/**
	 * Does the user exist with a valid credential?
	 * 
	 * @param fields
	 * @param service
	 * @return
	 */
	private boolean existsInService(String user, String service) {
		try {
			if (backend.isUserValid(
					user,
					service))
				return true;
		} catch (HabitatServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	private void addVisualizationText(Map<String, Object> context) {
		final String visualizationText = "Your provenance is visualized <a href=\"http://localhost:7474/browser\">here</a>";
		final String vizAppLink = Config.getExternalLocation() == null
				? "."
				: " and <a href=\"/viz\">here</a>.";
		context.put("visualizationText", visualizationText + vizAppLink);
	}

	public Map<String, Object> getProfileMap(Request request, Response response) {
		List<CommonProfile> profiles = SparkUserProfiles.getProfiles(request, response);

		final Map<String,Object> map = new HashMap<>();
		
		for (CommonProfile prof: profiles) {
			map.put("username", prof.getDisplayName());
			map.put("firstname", prof.getFirstName());
			map.put("lastname", prof.getFamilyName());
			map.put("email", prof.getEmail());
			map.put("token", prof.getAttribute("access_token"));
			map.putAll(prof.getAttributes());
		}
		map.put("profiles", profiles);
		
		return map;
	}
	
	@Override
	public Multimap<String, RouteSpecifier> getSpecifiers() {
		Multimap<String, RouteSpecifier> ret = HashMultimap.create();
		ret.put("/google/login",
				new RouteSpecifier(
						RequestType.GET,
						false,
						googleLoginRoute));
		ret.put("/signup",
				new RouteSpecifier(
						RequestType.GET,
						false,
						signupRoute));
		ret.put("/addUser",
				new RouteSpecifier(
						RequestType.POST,
						false,
						addUserRoute));

		return ret;
	}

}
