/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.auth;

import org.pac4j.core.authorization.generator.AuthorizationGenerator;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.ProvidesIntoMap;
import com.google.inject.multibindings.StringMapKey;

import edu.upenn.cis.db.habitat.auth.pac4j.HabitatAuthorizationGenerator;
import edu.upenn.cis.db.habitat.core.webservice.RestPlugin;

/**
 * Enables injection of authorization services and the HabitatAuthorizationGenerator.
 * 
 * @author ZacharyIves
 *
 */
public class AuthServiceModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(AuthServices.class);
		bind(AuthorizationGenerator.class).to(HabitatAuthorizationGenerator.class);
	}

	@ProvidesIntoMap
	@StringMapKey("/auth")
//	@Named("services")
	RestPlugin provideUserService(AuthServices us) {
		return us;
	}
}
