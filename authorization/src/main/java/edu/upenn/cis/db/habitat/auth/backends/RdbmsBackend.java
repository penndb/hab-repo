/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.auth.backends;

import static edu.upenn.cis.db.habitat.auth.data.Tables.CREDENTIALS;
import static edu.upenn.cis.db.habitat.auth.data.Tables.GROUPS;
import static edu.upenn.cis.db.habitat.auth.data.Tables.GROUP_MEMBERS;
import static edu.upenn.cis.db.habitat.auth.data.Tables.GROUP_PERMISSIONS;
import static edu.upenn.cis.db.habitat.auth.data.Tables.ORGANIZATIONS;
import static edu.upenn.cis.db.habitat.auth.data.Tables.PERMISSIONS;
import static edu.upenn.cis.db.habitat.auth.data.Tables.PERMISSION_ENTAILS;
import static edu.upenn.cis.db.habitat.auth.data.Tables.USERS;
import static edu.upenn.cis.db.habitat.auth.data.Tables.USER_PERMISSIONS;
import static org.jooq.impl.DSL.constraint;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Result;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.SQLDataType;

import com.google.inject.Inject;

import edu.upenn.cis.db.habitat.auth.UserApiLocal;
import edu.upenn.cis.db.habitat.core.api.PermissionApi;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.auth.data.tables.Groups;
import edu.upenn.cis.db.habitat.auth.data.tables.Permissions;
import edu.upenn.cis.db.habitat.util.SparkUserProfiles;

public class RdbmsBackend implements UserApiLocal, PermissionApi {
	final static Logger logger = LogManager.getLogger(RdbmsBackend.class);

	DSLContext create;
	
	@Inject
	public RdbmsBackend(DSLContext context) throws HabitatServiceException {
		this.create = context;
	}

	@Override
	public boolean addCredential(
			String username, 
			String service, 
			String typeOrProtocol, 
			String token) throws HabitatServiceException {
		int userId = getUserIdFrom(username);

		if (service.equals("local")) {
			String encrypted = SparkUserProfiles.hash(username, token);
			logger.debug("Encrypting user password " + token + " to " + encrypted);
			return create.insertInto(CREDENTIALS, CREDENTIALS.USERID, CREDENTIALS.SERVICE, 
					CREDENTIALS.TYPE_OR_PROTOCOL, CREDENTIALS.TOKEN)
					.values(userId, service, typeOrProtocol, encrypted)
					.execute() > 0;
		} else {
			return create.insertInto(CREDENTIALS, CREDENTIALS.USERID, CREDENTIALS.SERVICE, 
					CREDENTIALS.TYPE_OR_PROTOCOL)
					.values(userId, service, typeOrProtocol)
					.execute() > 0;
		}
	}
	
	
	@Override
	public int addGroup(int orgId, String name, String parentGroup) throws HabitatServiceException {
		if (parentGroup != null) {
			try {
				Integer parentGroupId = this.getGroupIdFrom(parentGroup);
				
				if (parentGroupId < 0)
					throw new HabitatServiceException("Parent group " + parentGroup + " not found");
				
				create.insertInto(GROUPS, GROUPS.NAME, GROUPS.ORG_ID, GROUPS.PARENT_GROUP_ID)
					.values(name, (int)orgId, parentGroupId)
					.execute();
			} catch(DataAccessException e) {
				logger.debug("Group is a duplicate? " + e.getMessage());
			}
		} else {
			try {
				create.insertInto(GROUPS, GROUPS.NAME, GROUPS.ORG_ID)
					.values(name, (int)orgId)
					.execute();
			} catch(DataAccessException e) {
				logger.debug("Group is a duplicate? " + e.getMessage());
			}
		}
		
		Record1<Integer> groupId = create.select(GROUPS.GROUP_ID)
				.from (GROUPS)
				.where(GROUPS.NAME.eq(name))//.and(GROUPS.ORG_ID.eq((int)orgId)))
				.fetchOne();
		
		return (groupId == null) ? -1 : (int)groupId.value1();
	}

	@Override
	public int addOrganization(String name, String street1, String street2, String city, String state, String zip,
			String country, String phone, String web) {
		try {
			create.insertInto(ORGANIZATIONS, ORGANIZATIONS.NAME, ORGANIZATIONS.STREET1, 
					ORGANIZATIONS.STREET2, ORGANIZATIONS.CITY, ORGANIZATIONS.STATE, ORGANIZATIONS.ZIP, ORGANIZATIONS.COUNTRY, ORGANIZATIONS.PHONE, ORGANIZATIONS.WEB)
			.values(name, street1, street2, city, state, zip, country, phone, web)
			.execute();
		} catch (DataAccessException e) {
			logger.debug("Organization is a duplicate? " + e.getMessage());
		}

		Record1<Integer> value = create.select(ORGANIZATIONS.ID)
				.from(ORGANIZATIONS)
				.where(ORGANIZATIONS.NAME.eq(name))
				.fetchOne();
		
		return (value == null) ? -1 : value.value1();
	}
	
	@Override
	public int addPermission(String name) {
		try {
			create.insertInto(PERMISSIONS, PERMISSIONS.PERM_NAME)
			.values(name)
			.execute();
		} catch (DataAccessException e) {
			logger.debug("Permission is a duplicate? " + e.getMessage());
		}
		
		Record1<Integer> value = create.select(PERMISSIONS.PERM_ID)
				.from(PERMISSIONS)
				.where(PERMISSIONS.PERM_NAME.eq(name))
				.fetchOne();
		
		return (value == null) ? -1 : value.value1();
	}

	@Override
	public boolean addSubgroupToGroup(String childGroup, String group) {

		try {
			int groupId = this.getGroupIdFrom(group);
			
			return create.update(GROUPS)
				.set(GROUPS.PARENT_GROUP_ID, groupId)
				.where(GROUPS.NAME.eq(childGroup))
				.execute() > 0;
		} catch (Exception e) {
			logger.debug("Unable to add subgroup to group: " + e.getMessage());
		}

		return false;
	}

	@Override
	public boolean addUser(String username, String first, String last, String street1, String street2, String city,
			String state, String zip, String country, String phone, String primaryEmail, String title,
			String organization) {

		Integer orgId = null;
		
		if (organization != null) {
			try {
				orgId = getOrganizationIdFrom(organization);
			} catch (HabitatServiceException e) {
				// If the organization didn't exist, try to insert it
				addOrganization(organization, street1, street2, city, state, zip, country, phone, "");
			}
		}
		
		try {
			return create.insertInto(USERS, USERS.USERNAME, USERS.FIRSTNAME, USERS.LASTNAME,
					USERS.STREET1, USERS.STREET2, USERS.CITY, USERS.STATE, USERS.ZIP,
					USERS.COUNTRY, USERS.PHONE, USERS.PRIMARYEMAIL, USERS.TITLE,
					USERS.ORGANIZATION)
			.values(username, first, last, street1, street2, city, state, zip, country, phone,
					primaryEmail, title, orgId)
			.execute() > 0;
		} catch (DataAccessException e) {
			logger.debug("User is a duplicate? " + e.getMessage());
			return false;
		}
	}

	// TODO: turn this into a transaction, https://www.jooq.org/doc/3.8/manual/sql-execution/transaction-management/
	@Override
	public boolean addUserAndOrganization(String username, String firstname, String lastname,
			String street1, String street2, String city, String state, String zip,
			String country, String phone, String userEmail, String title, String
			organization, String orgPhone, String orgWeb) throws HabitatServiceException {
		Integer orgId ;
		
		if (organization == null)
			orgId = null;
		else {
			try {
				orgId = getOrganizationIdFrom(organization);
			} catch (HabitatServiceException e) {
				orgId = (int)addOrganization(organization, 
						street1, 
						street2, 
						city, 
						state, 
						zip, 
						country, 
						orgPhone, orgWeb);
			}
		}


		if (!addUser(username, 
				firstname, 
				lastname, 
				street1, 
				street2, 
				city, 
				state, 
				zip, 
				country, 
				phone, 
				userEmail, 
				title, 
				organization))
			return false;
		if (orgId == null || addUserToOrganization(username, orgId)) {
			return true;
		} else
			return false;
	}
	
	@Override
	public boolean addUserToGroup(String username, String group) {
		try {
			int userId = this.getUserIdFrom(username);
			int groupId = this.getGroupIdFrom(group);
			
			return create.insertInto(GROUP_MEMBERS,
					GROUP_MEMBERS.GROUP_ID, GROUP_MEMBERS.USER_ID)
					.values(groupId, userId)
					.execute() > 0;
		} catch (Exception e) {
			logger.debug("Could not add user to group: " + e.getMessage());
			return false;
		}
	}

	@Override
	public boolean addUserToOrganization(String username, int orgid) {
		return create.update(USERS)
			.set(USERS.ORGANIZATION, (int)orgid)
			.where(USERS.USERNAME.eq(username))
			.execute() > 0;
	}

	@Override
	public void createTables() throws HabitatServiceException {
		create.createTableIfNotExists("organizations")
		.column("id", SQLDataType.INTEGER.identity(true))
		.column("name", SQLDataType.VARCHAR.nullable(false).length(40))
		.column("street1", SQLDataType.VARCHAR.length(40))
		.column("street2", SQLDataType.VARCHAR.length(40))
		.column("city", SQLDataType.VARCHAR.length(42))
		.column("state", SQLDataType.VARCHAR.length(40))
		.column("zip", SQLDataType.VARCHAR.length(15))
		.column("country", SQLDataType.VARCHAR.length(30))
		.column("phone", SQLDataType.VARCHAR.length(20))
		.column("web", SQLDataType.VARCHAR.length(30))
		.constraints(
				constraint("PK_ORG").primaryKey("id"),
				constraint("UN_NAME").unique("name")
				)
		.execute();
		
		create.createTableIfNotExists("users")
		.column("userid", SQLDataType.INTEGER.identity(true))
		.column("username", SQLDataType.VARCHAR.nullable(false).length(20))
		.column("firstname", SQLDataType.VARCHAR.length(20))
		.column("lastname", SQLDataType.VARCHAR.length(20))
		.column("street1", SQLDataType.VARCHAR.length(40))
		.column("street2", SQLDataType.VARCHAR.length(40))
		.column("city", SQLDataType.VARCHAR.length(42))
		.column("state", SQLDataType.VARCHAR.length(40))
		.column("zip", SQLDataType.VARCHAR.length(15))
		.column("country", SQLDataType.VARCHAR.length(30))
		.column("phone", SQLDataType.VARCHAR.length(30))
		.column("primaryemail", SQLDataType.VARCHAR.length(30))
		.column("title", SQLDataType.VARCHAR.length(30))
		.column("organization", SQLDataType.INTEGER)
		.constraints(
				constraint("PK_USER").primaryKey("userid"),
				constraint("PK_UNIQUENAME").unique("username"),
				constraint("FK_USER_ORG").foreignKey("organization").references("organizations", "id")
				)
		.execute();
		
		create.createTableIfNotExists("permissions")
		.column("perm_id", SQLDataType.INTEGER.identity(true))
		.column("perm_name", SQLDataType.VARCHAR.length(20).nullable(false))
		.constraints(
				constraint("PK_PERM").primaryKey("perm_id"),
				constraint("UN_PERM").unique("perm_name")
		)
		.execute();
		
		create.createTableIfNotExists("permission_entails")
		.column("perm_id", SQLDataType.INTEGER)
		.column("derived_perm_id", SQLDataType.INTEGER)
		.constraints(
				constraint("FK_PERM_1").foreignKey("perm_id").references("permissions", "perm_id"),
				constraint("FK_PERM_2").foreignKey("derived_perm_id").references("permissions", "perm_id")
		)
		.execute();

		create.createTableIfNotExists("user_permissions")
		.column("user_id", SQLDataType.INTEGER.nullable(false))
		.column("object_id", SQLDataType.VARCHAR.length(32).nullable(false))
		.column("perm_type", SQLDataType.INTEGER)
		.constraints(
				constraint("FK_USER_PERM_PERM").foreignKey("perm_type").references("permissions", "perm_id"),
				constraint("FK_USER_PERM_USER").foreignKey("user_id").references("users", "userid")
				)
		.execute();

		create.createTableIfNotExists("groups")
		.column("group_id", SQLDataType.INTEGER.identity(true))
		.column("org_id", SQLDataType.INTEGER)
		.column("name", SQLDataType.VARCHAR.length(30))
		.column("parent_group_id", SQLDataType.INTEGER)
		.constraints(
				constraint("PK_GROUPS").primaryKey("group_id"),
				constraint("FK_GROUP_ORG").foreignKey("org_id").references("organizations", "id"),
				constraint("FK_GROUP_PARENT").foreignKey("parent_group_id").references("groups", "group_id")
				)
		.execute();

		create.createTableIfNotExists("credentials")
		.column("userid", SQLDataType.INTEGER.nullable(false))
		.column("service", SQLDataType.VARCHAR.length(10))
		.column("type_or_protocol", SQLDataType.VARCHAR.length(10))
		.column("token", SQLDataType.VARCHAR.length(160))
		.constraints(
				constraint("PK_CRED").primaryKey("userid", "service"),
				constraint("FK_CRED_USER").foreignKey("userid").references("users", "userid")
				)
		.execute();
		
		create.createTableIfNotExists("group_members")
		.column("group_id", SQLDataType.INTEGER.nullable(false))
		.column("user_id", SQLDataType.INTEGER.nullable(false))
		.constraints(
				constraint("PK_GMEMBERS").primaryKey("group_id", "user_id"),
				constraint("FK_GMEMBER_USER").foreignKey("user_id").references("users", "userid"),
				constraint("FK_GMEMBER_GROUP").foreignKey("group_id").references("groups", "group_id")
				)
		.execute();

		create.createTableIfNotExists("group_permissions")
		.column("group_id", SQLDataType.INTEGER.nullable(false))
		.column("object_id", SQLDataType.VARCHAR.length(32).nullable(false))
		.column("perm_type", SQLDataType.INTEGER)
		.constraints(
				constraint("FK_GROUP_PERM_PERM").foreignKey("perm_type").references("permissions", "perm_id"),
				constraint("FK_GROUP_PERM_USER").foreignKey("group_id").references("groups", "group_id")
				)
		.execute();

		for (Schema schema: create.meta().getSchemas())
			logger.debug("Schema: " + schema.getName());
		
		for (Table<?> table: create.meta().getTables()) {
			String columns = "";
			
			for (Field<?> f: table.fields())
				columns = columns + f.getName() + ": " + f.getDataType().getTypeName() + " ";
			
			logger.debug("Table:" + table.getSchema().getName() + " . " + table.getName() + "(" + columns + ")");
		}
		
		logger.debug("Added tables");
		
		this.addPermission("Read");

		this.addPermission("Write");

		this.addPermission("Grant");

		this.addPermission("Admin");


		this.addEntailedPermission("Admin", "Write");
		this.addEntailedPermission("Admin", "Grant");
		this.addEntailedPermission("Admin", "Read");
		this.addEntailedPermission("Grant", "Write");
		this.addEntailedPermission("Grant", "Read");
		this.addEntailedPermission("Write", "Read");
	}

	@Override
	public String getGroupFromId(Integer id) throws HabitatServiceException {
		Record1<String> groupId = create.select(GROUPS.NAME)
				.from(GROUPS)
				.where(GROUPS.GROUP_ID.eq(id))
				.fetchOne();
		
		if (groupId == null)
			throw new HabitatServiceException("Group not found or insufficient permissions: " + id);
		
		return groupId.value1();
	}
	
	@Override
	public Integer getGroupIdFrom(String groupName) throws HabitatServiceException {
		Record1<Integer> groupId = create.select(GROUPS.GROUP_ID)
				.from(GROUPS)
				.where(GROUPS.NAME.eq(groupName))
				.fetchOne();
		
		if (groupId == null)
			throw new HabitatServiceException("Group not found or insufficient permissions: " + groupName);
		
		return groupId.value1();
	}

	@Override
	public Set<Integer> getGroupIdsForUser(String username) {
		Set<Integer> ret = new HashSet<>();
		
		Result<Record1<Integer>> perms = create.select(GROUP_MEMBERS.GROUP_ID)
				.from(USERS.join(
						GROUP_MEMBERS)
					.on(USERS.USERID.eq(GROUP_MEMBERS.USER_ID)))
				.where(USERS.USERNAME.eq(username))
				.fetch();
		
		perms.listIterator().forEachRemaining(p -> ret.add(p.value1()));

		return ret;
		
	}

	@Override
	public Set<String> getGroupPermissionsOn(String object, String username) throws HabitatServiceException {
		Result<Record1<String>> perms = create.select(PERMISSIONS.PERM_NAME)
				.from(GROUPS.join(
						GROUP_PERMISSIONS.join(PERMISSIONS).on(
						GROUP_PERMISSIONS.PERM_TYPE.eq(PERMISSIONS.PERM_ID))
					).on(GROUPS.GROUP_ID.eq(GROUP_PERMISSIONS.GROUP_ID)))
				.where(USER_PERMISSIONS.OBJECT_ID.eq(object))
				.fetch();
		
		Set<String> ret = new HashSet<>();
		perms.listIterator().forEachRemaining(p -> ret.add(p.value1()));

		return ret;
	}

	
	@Override
	public Set<String> getGroupsForUser(String username) {
		Set<String> ret = new HashSet<>();
		
		Result<Record1<String>> perms = create.select(GROUPS.NAME)
				.from(USERS.join(
						GROUP_MEMBERS.join(GROUPS)
						.on(GROUP_MEMBERS.GROUP_ID.eq(GROUPS.GROUP_ID))
					).on(USERS.USERID.eq(GROUP_MEMBERS.USER_ID)))
				.where(USERS.USERNAME.eq(username))
				.fetch();
		
		perms.listIterator().forEachRemaining(p -> ret.add(p.value1()));

		return ret;
		
	}

	@Override
	public String getOrganizationFromId(Integer id) throws HabitatServiceException {
		Record1<String> orgId = create.select(ORGANIZATIONS.NAME)
				.from(ORGANIZATIONS)
				.where(ORGANIZATIONS.ID.eq(id))
				.fetchOne();
		
		if (orgId == null)
			throw new HabitatServiceException("Organization not found or insufficient permissions: " + id);
		
		return orgId.value1();
	}

	@Override
	public Integer getOrganizationIdFrom(String organization) throws HabitatServiceException {
		Record1<Integer> orgId = create.select(ORGANIZATIONS.ID)
				.from(ORGANIZATIONS)
				.where(ORGANIZATIONS.NAME.eq(organization))
				.fetchOne();
		
		if (orgId == null)
			throw new HabitatServiceException("Organization not found or insufficient permissions: " + organization);
		
		return orgId.value1();
	}

	@Override
	public Set<Integer> getParentGroupIds(Integer group) {
		Set<Integer> ret = new HashSet<>();
		
		Groups g1 = GROUPS.as("g1");
		Groups g2 = GROUPS.as("g2");
		
		Result<Record1<Integer>> parents = create.select(g2.GROUP_ID)
				.from(g1.join(g2)
						.on(g1.PARENT_GROUP_ID.eq(g2.GROUP_ID)))
				.where(g1.GROUP_ID.eq(group))
				.fetch();
		
		parents.listIterator().forEachRemaining(p -> ret.add(p.value1()));

		return ret;
	}

	@Override
	public Set<String> getParentGroups(String group) {
		Set<String> ret = new HashSet<>();
		
		Groups g1 = GROUPS.as("g1");
		Groups g2 = GROUPS.as("g2");
		
		Result<Record1<String>> parents = create.select(g2.NAME)
				.from(g1.join(g2)
						.on(g1.PARENT_GROUP_ID.eq(g2.GROUP_ID)))
				.where(g1.NAME.eq(group))
				.fetch();
		
		parents.listIterator().forEachRemaining(p -> ret.add(p.value1()));

		return ret;
	}

	@Override
	public String getPermissionFromId(int permissionId) throws HabitatServiceException {
		Record1<String> permId = create.select(PERMISSIONS.PERM_NAME)
				.from(PERMISSIONS)
				.where(PERMISSIONS.PERM_ID.eq(permissionId))
				.fetchOne();
		
		if (permId == null)
			throw new HabitatServiceException("Permission type not found: " + permissionId);
		
		return permId.value1();
	}

	@Override
	public int getPermissionIdFrom(String permission) throws HabitatServiceException {
		Record1<Integer> permId = create.select(PERMISSIONS.PERM_ID)
				.from(PERMISSIONS)
				.where(PERMISSIONS.PERM_NAME.eq(permission))
				.fetchOne();
		
		if (permId == null)
			throw new HabitatServiceException("Permission type not found: " + permission);
		
		return permId.value1();

	}

	@Override
	public String getSessionToken() {
		return "";
	}

	@Override
	public String getSessionUsername() {
		return "(root)";
	}

	@Override
	public String getUserFromId(Integer id) throws HabitatServiceException {
		Record1<String> userId = create.select(USERS.USERNAME)
				.from(USERS)
				.where(USERS.USERID.eq(id))
				.fetchOne();
		
		if (userId == null || userId.value1() == null)
			throw new HabitatServiceException("Unauthorized or user does not exist");
		
		return userId.value1();
	}

	@Override
	public Integer getUserIdFrom(String username) throws HabitatServiceException {
		Record1<Integer> userId = create.select(USERS.USERID)
				.from(USERS)
				.where(USERS.USERNAME.eq(username))
				.fetchOne();
		
		if (userId == null || userId.value1() == null)
			throw new HabitatServiceException("Unauthorized or user does not exist");
		
		return userId.value1();
	}

	/**
	 * Returns user key-value pairs if the user is valid, else an empty map
	 */
	@Override
	public Map<String, Object> getUserInfo(String username) {
		Map<String,Object> ret = new HashMap<>();
		
		Record userInfo = create.select()
			.from(USERS)
			.where(USERS.USERNAME.eq(username))
			.fetchOne();
		
		if (userInfo != null) {
			ret.put("username", username);
			
			for (Field<?> key: userInfo.fields())
				if (!key.getName().toLowerCase().equals("password"))
				ret.put(key.getName(), userInfo.getValue(key));
		}
		
		return ret;
	}

	@Override
	public Set<String> getUserPermissionsOn(String object, String username) {
		Result<Record1<String>> perms = create.select(PERMISSIONS.PERM_NAME)
				.from(USERS.join(
						USER_PERMISSIONS.join(PERMISSIONS)
						.on(USER_PERMISSIONS.PERM_TYPE.eq(PERMISSIONS.PERM_ID))
					).on(USERS.USERID.eq(USER_PERMISSIONS.USER_ID)))
				.where(USER_PERMISSIONS.OBJECT_ID.eq(object))
				.fetch();
		
		Set<String> ret = new HashSet<>();
		perms.listIterator().forEachRemaining(p -> ret.add(p.value1()));

		return ret;
	}

	@Override
	public boolean grantGroupPermissionOn(String object, String permission, String groupname) {
		try {
			int groupId = this.getGroupIdFrom(groupname);
			int permId = this.getPermissionIdFrom(permission);
			
			return create.insertInto(GROUP_PERMISSIONS, GROUP_PERMISSIONS.GROUP_ID, GROUP_PERMISSIONS.OBJECT_ID, GROUP_PERMISSIONS.PERM_TYPE)
				.values(groupId, object, permId)
				.execute() > 0;
		} catch (Exception e) {
			logger.debug("Unable to grant group permission for " + groupname + " on " + object + ": " + e.getMessage());
			return false;
		}
	}
	
	@Override
	public boolean grantUserPermissionOn(String object, String permission, String username) {
		final String m = "grantUserPermissionOn(...)";
		try {
			int userId = this.getUserIdFrom(username);
			int permId = -1;
			
			try {
				permId = this.getPermissionIdFrom(permission);
			} catch (Exception e) {
				permId = this.addPermission(permission);
			}
			logger.debug("{}; Granting permission {}({}) to {}({}) on {}",
					m,
					permission,
					permId,
					username,
					userId,
					object);
			
			// See if we need to update
			Record1<Integer> perms = create.select(PERMISSIONS.PERM_ID)
					.from(USERS.join(
							USER_PERMISSIONS.join(PERMISSIONS)
							.on(USER_PERMISSIONS.PERM_TYPE.eq(PERMISSIONS.PERM_ID))
						).on(USERS.USERID.eq(USER_PERMISSIONS.USER_ID)))
					.where(USER_PERMISSIONS.OBJECT_ID.eq(object))
					.fetchOne();
			
			// Permission wasn't expressly assigned to object
			if (perms == null) {
				return create.insertInto(USER_PERMISSIONS, USER_PERMISSIONS.USER_ID, USER_PERMISSIONS.OBJECT_ID, USER_PERMISSIONS.PERM_TYPE)
					.values(userId, object, permId)
					.execute() > 0;
					
			// Specific permission wasn't assigned to object  
			} else if (((Integer)perms.get(0)).intValue() != permId) {
				return create.update(USER_PERMISSIONS)
						.set(USER_PERMISSIONS.PERM_TYPE, permId)
						.where(USER_PERMISSIONS.OBJECT_ID.equal(object).and(USER_PERMISSIONS.USER_ID.eq(userId)))
						.execute() > 0;
			} else
				return true;
		} catch (Exception e) {
			logger.debug("Unable to grant permission for " + username + " on " + object + ": " + e.getMessage());
			return false;
		}
	}

	@Override
	public boolean hasPermission(String object, int permId, String username) throws HabitatServiceException {
		return create.select()
				.from(USERS.join(USER_PERMISSIONS)
					.on(USERS.USERID.eq(USER_PERMISSIONS.USER_ID)))
				.where(USER_PERMISSIONS.OBJECT_ID.eq(object).and
						(USER_PERMISSIONS.PERM_TYPE.eq(permId))
						.and(USERS.USERNAME.eq(username)))
				.execute() > 0 
			||
				(create.select()
						.from(USERS.join(USER_PERMISSIONS)
								.on(USERS.USERID.eq(USER_PERMISSIONS.USER_ID))
								.join(PERMISSION_ENTAILS).on(USER_PERMISSIONS.PERM_TYPE.eq(PERMISSION_ENTAILS.PERM_ID)))
							.where(USER_PERMISSIONS.OBJECT_ID.eq(object).and
									(PERMISSION_ENTAILS.DERIVED_PERM_ID.eq(permId))
									.and(USERS.USERNAME.eq(username)))
							.execute() > 0
				);
		
	}

	@Override
	public boolean hasPermission(String object, String permission,
			String username) throws HabitatServiceException {
		return create
				.select(PERMISSIONS.PERM_NAME)
				.from(USERS.join(
						USER_PERMISSIONS.join(PERMISSIONS)
								.on(USER_PERMISSIONS.PERM_TYPE
										.eq(PERMISSIONS.PERM_ID))
						).on(USERS.USERID.eq(USER_PERMISSIONS.USER_ID)))
				.where(USER_PERMISSIONS.OBJECT_ID.eq(object)
						.and(PERMISSIONS.PERM_NAME.eq(permission))
						.and(USERS.USERNAME.eq(username)))
				.execute() > 0
				|| create
						.select(p1.PERM_NAME)
						.from(USERS
								.join(
										USER_PERMISSIONS.join(p1)
												.on(USER_PERMISSIONS.PERM_TYPE
														.eq(p1.PERM_ID))
								)
								.on(USERS.USERID.eq(USER_PERMISSIONS.USER_ID))
								.join(PERMISSION_ENTAILS)
								.on(PERMISSION_ENTAILS.PERM_ID.eq(p1.PERM_ID))
								.join(p2)
								.on(PERMISSION_ENTAILS.DERIVED_PERM_ID
										.eq(p2.PERM_ID)))
						.where(USER_PERMISSIONS.OBJECT_ID.eq(object).and
								(p2.PERM_NAME.eq(permission))
								.and(USERS.USERNAME.eq(username)))
						.execute() > 0;
	}

	@Override
	public void initialize(String namespace) throws HabitatServiceException {
	}

	@Override
	public boolean isClaimedAsObjectId(String object) throws HabitatServiceException {
		return create.select()
				.from(USER_PERMISSIONS)
				.where(USER_PERMISSIONS.OBJECT_ID.eq(object))
				.execute() > 0;
	}

	@Override
	public boolean isUserCredentialValid(String username, String token) {
		Result<Record1<String>> rec = create.select(CREDENTIALS.TOKEN)
				.from(USERS.join(CREDENTIALS).on(USERS.USERID.eq(CREDENTIALS.USERID)))
				.where(USERS.USERNAME.eq(username).and(CREDENTIALS.SERVICE.eq("local")))//service)))
				.fetch();

		if (rec.isEmpty())
			return false;
		else
			return SparkUserProfiles.isMatch(rec.iterator().next().value1(), 
					username, token);
	}

	@Override
	public boolean isUserInGroup(String username, int groupId) throws HabitatServiceException {

		if (create.select().from(GROUP_MEMBERS.join(USERS).on(GROUP_MEMBERS.USER_ID.eq(USERS.USERID)))
				.where(GROUP_MEMBERS.GROUP_ID.eq(groupId)
						.and(USERS.USERNAME.eq(username)))
				.execute() > 0)
			return true;
		
		
		Integer find = groupId;
		Set<Integer> groupIds = getGroupIdsForUser(username);
		while (groupIds != null && !groupIds.isEmpty()) {
			if (groupIds.contains(find))
				return true;
			
			Set<Integer> nextGroups = new HashSet<>();
			for (Integer id : groupIds)
				nextGroups.addAll(getParentGroupIds(id));
			
			groupIds = nextGroups;
		}
		return false;
	}
	
	@Override
	public boolean isUserInGroup(String username, String group) throws HabitatServiceException {

		if (create.select().from(
				GROUPS.join(GROUP_MEMBERS).on(GROUPS.GROUP_ID.eq(GROUP_MEMBERS.GROUP_ID))
						.join(USERS)
						.on(GROUP_MEMBERS.USER_ID.eq(USERS.USERID)))
				.where(GROUPS.NAME.eq(group)
						.and(USERS.USERNAME.eq(username)))
				.execute() > 0)
			return true;
		
		
		Set<String> groups = getGroupsForUser(username);
		while (groups != null && !groups.isEmpty()) {
			if (groups.contains(group))
				return true;
			
			Set<String> nextGroups = new HashSet<>();
			for (String id : groups)
				nextGroups.addAll(getParentGroups(id));
			
			groups = nextGroups;
		}
		return false;
	}
	
	@Override
	public boolean isUserInOrganization(String username, int orgId) throws HabitatServiceException {
		return ((Integer)getUserInfo(username).get("organization")) == orgId;
	}

	@Override
	public boolean isUserValid(String username, String service) {
		
		if (service.equals("local")) {
			return create.select()
					.from(USERS)
					.where(USERS.USERNAME.eq(username))
					.execute() > 0;
		} else {
			return create.select()
					.from(USERS.join(CREDENTIALS).on(USERS.USERID.eq(CREDENTIALS.USERID)))
					.where(USERS.USERNAME.eq(username).and(CREDENTIALS.SERVICE.eq(service)))
					.execute() > 0;
			
		}
	}

	@Override
	public void removeKey(String object) {
		create.delete(USER_PERMISSIONS)
				.where(USER_PERMISSIONS.OBJECT_ID.eq(object))
				.execute();
	}

	@Override
	public boolean revokeGroupPermissionOn(String object, String permission, String groupname)  {
		try {
			int gid = getGroupIdFrom(groupname); 
			int permissionId = getPermissionIdFrom(permission);
			return create.deleteFrom(GROUP_PERMISSIONS)
				.where(GROUP_PERMISSIONS.GROUP_ID.eq(gid),
						GROUP_PERMISSIONS.PERM_TYPE.eq(permissionId),
						GROUP_PERMISSIONS.OBJECT_ID.eq(object))
				.execute() > 0;
		} catch (Exception e) {
			logger.debug("Unable to revoke group permission for " + groupname + " on " + object + ": " + e.getMessage());
			return false;
		}
	}

	@Override
	public boolean revokeUserPermissionOn(String object, String permission, String username) {
		try {
			int userId = this.getUserIdFrom(username);
			int permId = this.getPermissionIdFrom(permission);
			
			return create.deleteFrom(USER_PERMISSIONS)
					.where(USER_PERMISSIONS.USER_ID.eq(userId))
					.and(USER_PERMISSIONS.OBJECT_ID.eq(object))
					.and(USER_PERMISSIONS.PERM_TYPE.eq(permId))
				.execute() > 0;
		} catch (Exception e) {
			logger.debug("Unable to revoke permission for " + username + " on " + object + ": " + e.getMessage());
			return false;
		}
	}

	@Override
	public boolean updateCredential(String username, String service, String typeOrProtocol, String oldToken,
			String token) throws HabitatServiceException {
		int userId = getUserIdFrom(username);
		
		if (isUserCredentialValid(username, oldToken)) {	
			if (service.equals("local")) {
				String encrypted = SparkUserProfiles.hash(username, token);
				logger.debug("Encrypting user password " + token + " to " + encrypted);
				return create.insertInto(CREDENTIALS, CREDENTIALS.USERID, CREDENTIALS.SERVICE, 
						CREDENTIALS.TYPE_OR_PROTOCOL, CREDENTIALS.TOKEN)
					.values(userId, service, typeOrProtocol, encrypted)
					.execute() > 0;
			} else {
				return create.insertInto(CREDENTIALS, CREDENTIALS.USERID, CREDENTIALS.SERVICE, 
						CREDENTIALS.TYPE_OR_PROTOCOL, CREDENTIALS.TOKEN)
					.values(userId, service, typeOrProtocol, token)
					.execute() > 0;
			}
		} else
			return false;
	}

	@Override
	public boolean updateUser(String username, String first, String last, String street1, String street2, String city,
			String state, String zip, String country, String phone, String primaryEmail, String title,
			String organization) {

		long orgId = 0;
		try {
			orgId = getOrganizationIdFrom(organization);
		} catch (HabitatServiceException e) {
			// If the organization didn't exist, try to insert it
			orgId = addOrganization(organization, street1, street2, city, state, zip, country, phone, "");
		}
		
			try {
				return create.update(USERS)
						.set(USERS.FIRSTNAME, first)
						.set(USERS.LASTNAME, last)
						.set(USERS.STREET1, street1)
						.set(USERS.STREET2, street2)
						.set(USERS.CITY, city)
						.set(USERS.STATE, state)
						.set(USERS.ZIP, zip)
						.set(USERS.COUNTRY, country)
						.set(USERS.PHONE, phone)
						.set(USERS.PRIMARYEMAIL, primaryEmail)
						.set(USERS.TITLE, title)
						.set(USERS.ORGANIZATION, (int)orgId)
						.where(USERS.USERNAME.eq(username))
				.execute() > 0;
			} catch (DataAccessException e) {
				logger.debug("User is not updatable? " + e.getMessage());
				return false;
			}
	}
	
	static Permissions p1 = PERMISSIONS.as("p1");
	static Permissions p2 = PERMISSIONS.as("p2");
	
	/**
	 * Given a permission, what other permissions do I automatically have?
	 * 
	 * @param myPermission
	 * @return
	 */
	Set<String> getEntailedPermissions(String myPermission) {
		Set<String> ret = new HashSet<>();
		ret.add(myPermission);
		

		Result<Record1<String>> perms = create.select(p2.PERM_NAME)
			.from(p1.join(
					PERMISSION_ENTAILS.join(p2).on(PERMISSION_ENTAILS.DERIVED_PERM_ID.eq(p2.PERM_ID))
					).on(p1.PERM_ID.eq(PERMISSION_ENTAILS.PERM_ID)))
		.where(p1.PERM_NAME.eq(myPermission))
		.fetch();
		
		Iterator<Record1<String>> iter = perms.iterator();
		while (iter.hasNext())
			ret.add(iter.next().value1());
		
		return ret;
	}

	/**
	 * Use transitive inference (via join) to figure out that permission X entails permission Y
	 */
	@Override
	public void addEntailedPermission(String myPermission, String entailedPermission) throws HabitatServiceException {
		try {
			create.insertInto(PERMISSION_ENTAILS, PERMISSION_ENTAILS.PERM_ID, PERMISSION_ENTAILS.DERIVED_PERM_ID)
			.select(create.select(p1.PERM_ID, p2.PERM_ID)
				.from(p1.crossJoin(p2))
				.where(p1.PERM_NAME.eq(myPermission).and(p2.PERM_NAME.eq(entailedPermission))))
			.execute();
		} catch (DataAccessException e) {
			logger.debug("Permission is a duplicate? " + e.getMessage());
		}
	}

	@Override
	public int getMaxObjectNameLength() {
		return USER_PERMISSIONS.OBJECT_ID.getDataType().length();
	}

	@Override
	public Set<String> getObjectIds(String permission, String username) {
		final Set<String> objects = new HashSet<>();
		final Result<Record1<String>> result = create.select(USER_PERMISSIONS.OBJECT_ID)
				.from(USERS.join(
						USER_PERMISSIONS.join(PERMISSIONS)
								.on(USER_PERMISSIONS.PERM_TYPE
										.eq(PERMISSIONS.PERM_ID))
						).on(USERS.USERID.eq(USER_PERMISSIONS.USER_ID)))
				.where(PERMISSIONS.PERM_NAME.eq(permission)
						.and(USERS.USERNAME.eq(username)))
				.union(create.select(USER_PERMISSIONS.OBJECT_ID)
								.from(USERS
										.join(
												USER_PERMISSIONS
														.join(p1)
														.on(USER_PERMISSIONS.PERM_TYPE
																.eq(p1.PERM_ID))
										)
										.on(USERS.USERID
												.eq(USER_PERMISSIONS.USER_ID))
										.join(PERMISSION_ENTAILS)
										.on(PERMISSION_ENTAILS.PERM_ID
												.eq(p1.PERM_ID))
										.join(p2)
										.on(PERMISSION_ENTAILS.DERIVED_PERM_ID
												.eq(p2.PERM_ID)))
								.where(p2.PERM_NAME.eq(permission)
										.and(USERS.USERNAME.eq(username))))
				.fetch();
		result.iterator().forEachRemaining(record -> objects.add(record.value1()));
		return objects;
	}

	@Override
	public boolean existsForUse(String resourceName, String username) {
			return create.select()
					.from(USER_PERMISSIONS)
					.where(USER_PERMISSIONS.OBJECT_ID.eq(resourceName))
							.execute() > 0 
					;
	}

}
