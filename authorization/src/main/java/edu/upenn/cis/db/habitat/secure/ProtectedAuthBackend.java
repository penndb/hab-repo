/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.secure;

import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;

import edu.upenn.cis.db.habitat.auth.UserApiLocal;
import edu.upenn.cis.db.habitat.core.api.PermissionApi;
import edu.upenn.cis.db.habitat.core.api.UserApi;
import edu.upenn.cis.db.habitat.core.webservice.HabitatSecurityException;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * Backend that checks user authorization before permitting an operation
 * 
 * @author zives
 *
 */
public class ProtectedAuthBackend implements UserApiLocal {
	
	static class UsernameHolder {
	    @Inject(optional=true) @Named("username") Provider<String> value = () -> null;
	}
	
	final static Logger logger = LogManager.getLogger(ProtectedAuthBackend.class);
	static final String ADMIN = "Admin";
	final UserApiLocal lowLevelApi;
	final PermissionApi permApi;
	
	private final Provider<String> usernameProvider;
	
	@Inject
	public ProtectedAuthBackend(
			UserApiLocal backend, 
			PermissionApi permApi,
			UsernameHolder usernameProviderHolder) {
		this.lowLevelApi = backend;
		this.permApi = permApi;
		this.usernameProvider = usernameProviderHolder.value;
	}

	@Override
	public boolean addCredential(String username, String service, String typeOrProtocol, String passwordOrToken)
			throws HabitatServiceException {

		if (isAdminFor(username))
			return lowLevelApi.addCredential(username, service, typeOrProtocol, passwordOrToken);
		else
			throw new HabitatSecurityException("Unauthorized operation on " + username);
	}
	
	@Override
	public int addGroup(int orgId, String name, String parentGroup) throws HabitatServiceException {
		if (isAdminForOrganization(orgId))
			return lowLevelApi.addGroup(orgId, name, parentGroup);
		else
			throw new HabitatSecurityException("Unauthorized to add group to organization " +
					orgId);
	}
	
	@Override
	public int addOrganization(String name, String street1, String street2, String city, String state, String zip,
			String country, String phone, String web) throws HabitatServiceException {
		if (isSiteAdmin())
			return lowLevelApi.addOrganization(name, street1, street2, city, state, zip, country, phone, web);
		else
			throw new HabitatSecurityException("Unauthorized to add organization");
	}

	@Override
	public boolean addSubgroupToGroup(String subgroup, String group) throws HabitatServiceException {
		if (isAdminForGroup(lowLevelApi.getGroupIdFrom(group)))
			return lowLevelApi.addSubgroupToGroup(subgroup, group);
		else
			throw new HabitatSecurityException("Unauthorized to add group " + subgroup + " to group " + group);
	}

	@Override
	public boolean addUser(String username, String first, String last, String street1, String street2, String city,
			String state, String zip, String country, String phone, String primaryEmail, String title,
			String organization) throws HabitatServiceException {
		if (isSiteAdmin())
			return lowLevelApi.addUser(username, first, last, street1, street2, city, state, zip, country, phone, primaryEmail, title, organization);
		else
			throw new HabitatSecurityException("Unauthorized to create blank user " + username);
	}

	@Override
	public boolean addUserAndOrganization(String username, String firstname, String lastname, String street1,
			String street2, String city, String state, String zip, String country, String phone, String userEmail,
			String title, String organization, String orgPhone, String orgWeb) throws HabitatServiceException {
		if (isSiteAdmin())
			return lowLevelApi.addUserAndOrganization(username, firstname, lastname, street1, street2, city, state, zip, country, phone, userEmail, title, organization, orgPhone, orgWeb);
		else
			throw new HabitatServiceException("Unauthorized to create blank user " + username);
	}

	@Override
	public boolean addUserToGroup(String username, String group) throws HabitatServiceException {
		if (isAdminForGroup(lowLevelApi.getGroupIdFrom(group)))
			return lowLevelApi.addUserToGroup(username, group);
		else
			throw new HabitatSecurityException("Unauthorized to add user " + username + " to group " + group);
	}

	@Override
	public boolean addUserToOrganization(String username, int orgId) throws HabitatServiceException {
		if (isAdminForOrganization(orgId))
			return lowLevelApi.addUserToOrganization(username, orgId);
		else
			throw new HabitatSecurityException("Unauthorized to add user to organization " +
					orgId);
	}

	@Override
	public void createTables() throws HabitatServiceException {
		if (isSiteAdmin())
			lowLevelApi.createTables();
	}

	@Override
	public Integer getGroupIdFrom(String groupName) throws HabitatServiceException {
		return lowLevelApi.getGroupIdFrom(groupName);
	}

	@Override
	public Integer getOrganizationIdFrom(String organization) throws HabitatServiceException {
		return lowLevelApi.getOrganizationIdFrom(organization);
	}

	@Override
	public String getOrganizationFromId(Integer id) throws HabitatServiceException {
		if (isAdminForOrganization(id))
			return lowLevelApi.getOrganizationFromId(id);
		else
			throw new HabitatSecurityException("Unauthorized for organization id " + id);
	}

	@Override
	public String getSessionToken() {
		return lowLevelApi.getSessionToken();
	}

	@Override
	public String getSessionUsername() {
		final String user = usernameProvider.get();
		if (user != null)
			return user;
		
		return lowLevelApi.getSessionUsername();
	}

	@Override
	public Integer getUserIdFrom(String username) throws HabitatServiceException {
		if (isAdminFor(username))
			return lowLevelApi.getUserIdFrom(username);
		else
			throw new HabitatSecurityException("Unauthorized operation on " + username);
	}

	@Override
	public Map<String, Object> getUserInfo(String username) throws HabitatServiceException {
		if (isAdminFor(username))
			return lowLevelApi.getUserInfo(username);
		else
			throw new HabitatSecurityException("Unauthorized operation on " + username);
	}

	@Override
	public void initialize(String namespace) throws HabitatServiceException {
		if (isSiteAdmin())
			lowLevelApi.initialize(namespace);;
	}
	
	public boolean isAdminFor(String connectedUser, String username) throws HabitatServiceException {
		return (connectedUser.equals(username) ||
				isSiteAdmin(connectedUser) || 
				permApi.hasPermission(PermissionApi.UserPrefix + ":" + username, 
						"admin", connectedUser));
	}

	public boolean isAdminForGroup(String connectedUser, int groupId) throws HabitatServiceException {
		return (isSiteAdmin(connectedUser) || 
				permApi.hasPermission(PermissionApi.GroupPrefix + ":" + 
						groupId, 
						"admin", connectedUser));
	}

	public  boolean isAdminForGroup(String user, String groupname) throws HabitatServiceException {
		return (isSiteAdmin(user) || 
				permApi.hasPermission(PermissionApi.GroupPrefix + ":" + 
						lowLevelApi.getGroupIdFrom(groupname), 
						"admin", user));
	}

	public boolean isAdminForOrganization(String user, int orgId) throws HabitatServiceException {
		return (isSiteAdmin(user) || 
				permApi.hasPermission(PermissionApi.OrgPrefix + ":" + 
						orgId, 
						"admin", user));
	}

	public boolean isAdminForOrganization(String user, String orgname) throws HabitatServiceException {
		return (isSiteAdmin(user) || 
				permApi.hasPermission(PermissionApi.OrgPrefix + ":" + 
						lowLevelApi.getOrganizationIdFrom(orgname), 
						"admin", user));
	}

	public boolean isAdminFor(String username) throws HabitatServiceException {
		String theUser = getSessionUsername(); 
		
		return isAdminFor(theUser, username);
	}

	public  boolean isAdminForGroup(int groupId) throws HabitatServiceException {
		String theUser = getSessionUsername(); 
		
		return isAdminForGroup(theUser, groupId);
	}

	public  boolean isAdminForGroup(String groupname) throws HabitatServiceException {
		return isAdminForGroup(getSessionUsername(), groupname);
	}

	public  boolean isAdminForOrganization(int orgId) throws HabitatServiceException {
		return isAdminForOrganization(getSessionUsername(), orgId);
	}

	public boolean isAdminForOrganization(String orgname) throws HabitatServiceException {
		return isAdminForOrganization(getSessionUsername(), orgname);
	}

	@Override
	public boolean isUserInGroup(String username, int groupId) throws HabitatServiceException {
		if (isAdminForGroup(groupId) || isAdminFor(username))
			return lowLevelApi.isUserInGroup(username, groupId);
		return false;
	}

	@Override
	public boolean isUserInGroup(String username, String group) throws HabitatServiceException {
		if (isAdminForGroup(group) || isAdminFor(username))
			return lowLevelApi.isUserInGroup(username, group);
		return false;
	}


	@Override
	public boolean isUserInOrganization(String username, int orgId) throws HabitatServiceException {
		return lowLevelApi.isUserInOrganization(username, orgId);
	}

	public static boolean isSiteAdmin(String user) {
		return (user.equals(UserApi.Root));
	}

	public boolean isSiteAdmin() {
		return isSiteAdmin(getSessionUsername());
	}

	@Override
	public boolean isUserCredentialValid(String username, String token) throws HabitatServiceException {
		return lowLevelApi.isUserCredentialValid(username, token);
	}

	@Override
	public boolean isUserValid(String username, String service) throws HabitatServiceException {
		return lowLevelApi.isUserValid(username, service);
	}

	@Override
	public boolean updateCredential(String username, String service, String typeOrProtocol, String oldToken,
			String token) throws HabitatServiceException {
		if (isAdminFor(username))
			return lowLevelApi.updateCredential(username, service, typeOrProtocol, oldToken, token);
		else
			throw new HabitatSecurityException("Unauthorized operation on " + username);
	}

	@Override
	public boolean updateUser(String username, String first, String last, String street1, String street2, String city,
			String state, String zip, String country, String phone, String primaryEmail, String title,
			String organization) throws HabitatServiceException {
		if (isAdminFor(username))
			return lowLevelApi.updateUser(username, first, last, street1, street2, city, state, zip, country, phone, primaryEmail, title, organization);
		else
			throw new HabitatServiceException("Unauthorized operation on " + username);
	}

	@Override
	public Set<String> getParentGroups(String group) throws HabitatServiceException {
		int id = getGroupIdFrom(group);
		if (isAdminForGroup(group) || isUserInGroup(getSessionUsername(), id))
			return lowLevelApi.getParentGroups(group);
		else
			throw new HabitatSecurityException("Unauthorized operation on " + group);
	}

	@Override
	public Set<Integer> getParentGroupIds(Integer group) throws HabitatServiceException {
		if (isAdminForGroup(group) || isUserInGroup(getSessionUsername(), group))
			return lowLevelApi.getParentGroupIds(group);
		else
			throw new HabitatSecurityException("Unauthorized operation on group ID " + group);
	}

	@Override
	public Set<Integer> getGroupIdsForUser(String username) throws HabitatServiceException {
		if (isAdminFor(username))
			return lowLevelApi.getGroupIdsForUser(username);
		else
			throw new HabitatSecurityException("Unauthorized operation on " + username);
	}

	@Override
	public Set<String> getGroupsForUser(String username) throws HabitatServiceException {
		if (isAdminFor(username))
			return lowLevelApi.getGroupsForUser(username);
		else
			throw new HabitatSecurityException("Unauthorized operation on " + username);
	}

	@Override
	public String getGroupFromId(Integer id) throws HabitatServiceException {
		if (isAdminForGroup(id))
			return lowLevelApi.getGroupFromId(id);
		else
			throw new HabitatSecurityException("Unauthorized for group id " + id);
	}

	@Override
	public String getUserFromId(Integer id) throws HabitatServiceException {
		String user = lowLevelApi.getUserFromId(id);
		
		if (isAdminFor(user))
			return user;
		else
			throw new HabitatSecurityException("Unauthorized for user id " + id);
	}

}
