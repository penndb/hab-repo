/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.auth.backends.plugins;

import com.google.inject.AbstractModule;

import edu.upenn.cis.db.habitat.auth.UserApiLocal;
import edu.upenn.cis.db.habitat.auth.backends.RdbmsBackend;
import edu.upenn.cis.db.habitat.core.api.PermissionApi;
import edu.upenn.cis.db.habitat.core.api.UserApi;
import edu.upenn.cis.db.habitat.secure.ProtectedAuthBackend;

public class AuthModule extends AbstractModule {

	@Override
	protected void configure() {
		// Prefer RDBMS backend for user APIs
		bind (UserApi.class).to(RdbmsBackend.class);
		
		// Ditto RDBMS backend for local instantiation of user APIs
		bind (UserApiLocal.class).to(RdbmsBackend.class);
		
		// Permissions should route to RDBMS as well
		bind (PermissionApi.class).to(RdbmsBackend.class);
		
		// Register the protected backend
		bind(ProtectedAuthBackend.class);
	}

}
