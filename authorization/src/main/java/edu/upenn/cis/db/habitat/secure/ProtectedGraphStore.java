/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.secure;

import java.io.InputStream;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.commons.lang3.tuple.Pair;

import com.google.common.util.concurrent.Striped;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;

import edu.upenn.cis.db.habitat.core.api.PermissionApi;
import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.TransactionSupplier;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate;
import edu.upenn.cis.db.habitat.core.webservice.HabitatSecurityException;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.repository.storage.GraphStore;

/**
 * Graph store that checks authorization against the user's permissions.
 * The graph name is the resource, and must be authorized.
 * 
 * @author zives
 *
 * @param <K>
 */
public class ProtectedGraphStore<K> implements GraphStore<K> {
	
	public static final String UNPROTECTED_GRAPH_STORE = "UnprotectedGraphStore";
	
	private static final Striped<Lock> createGraphLocks = Striped.lazyWeakLock(1000);
	final PermissionApi permissions;
	final GraphStore<K> lowLevel;
	final Provider<String> userProvider;
	private final int maxObjectNameLength;
	
	@Inject
	public ProtectedGraphStore(
			@Named(UNPROTECTED_GRAPH_STORE) GraphStore<K> lowLevel, 
			PermissionApi permissions,
			@Named("username") Provider<String> authorizedUserProvider) {
		this.permissions = permissions;
		this.lowLevel = lowLevel;
		this.userProvider = authorizedUserProvider;
		maxObjectNameLength = this.permissions.getMaxObjectNameLength();
	}

	@Override
	public Iterable<K> getKeysMatching(String resource, String metadataField, Map<K, ? extends Object> match)
			throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.ReadPermission, userProvider.get()))
			return lowLevel.getKeysMatching(resource, metadataField, match);
		else
			throw new HabitatSecurityException("Unauthorized to read " + resource);
	}

	@Override
	public Iterable<K> getKeysMatching(TransactionSupplier supplier, String resource, String metadataField,
			Map<K, ? extends Object> match) throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.ReadPermission, userProvider.get()))
			return lowLevel.getKeysMatching(supplier, resource, metadataField, match);
		else
			throw new HabitatSecurityException("Unauthorized to read " + resource);
	}

	@Override
	public Iterable<Pair<K, StructuredData<K, Object>>> getLinksFrom(String resource, K key1)
			throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.ReadPermission, userProvider.get()))
			return lowLevel.getLinksFrom(resource, key1);
		else
			throw new HabitatSecurityException("Unauthorized to read " + resource);
	}

	@Override
	public Iterable<Pair<K, StructuredData<K, Object>>> getLinksFrom(TransactionSupplier supplier, String resource,
			K key1) throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.ReadPermission, userProvider.get()))
			return lowLevel.getLinksFrom(supplier, resource, key1);
		else
			throw new HabitatSecurityException("Unauthorized to read " + resource);
	}

	@Override
	public Iterable<Pair<K, StructuredData<K, Object>>> getLinksTo(String resource, K key1)
			throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.ReadPermission, userProvider.get()))
			return lowLevel.getLinksTo(resource, key1);
		else
			throw new HabitatSecurityException("Unauthorized to read " + resource);
	}

	@Override
	public Iterable<Pair<K, StructuredData<K, Object>>> getLinksTo(TransactionSupplier supplier, String resource,
			K key1) throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.ReadPermission, userProvider.get()))
			return lowLevel.getLinksTo(supplier, resource, key1);
		else
			throw new HabitatSecurityException("Unauthorized to read " + resource);
	}

	@Override
	public void link(String resource, K key1, String label, K key2, StructuredData<K, ? extends Object> metadata)
			throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.WritePermission, userProvider.get()))
			lowLevel.link(resource, key1, label, key2, metadata);
		else
			throw new HabitatSecurityException("Unauthorized to update " + resource);
	}

	@Override
	public void link(TransactionSupplier supplier, String resource, K key1, String label, K key2, 
			StructuredData<K, ? extends Object> metadata) throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.WritePermission, userProvider.get()))
			lowLevel.link(supplier, resource, key1, label, key2, metadata);
		else
			throw new HabitatSecurityException("Unauthorized to update " + resource);
	}

	@Override
	public void storeReference(String resource, K key, String refToValue, StructuredData<K, ? extends Object> metadata)
			throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.WritePermission, userProvider.get()))
			lowLevel.storeReference(resource, key, refToValue, metadata);
		else
			throw new HabitatSecurityException("Unauthorized to update " + resource);
	}

	@Override
	public void storeReference(TransactionSupplier supplier, String resource, K key, String refToValue,
			StructuredData<K, ? extends Object> metadata) throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.WritePermission, userProvider.get()))
			lowLevel.storeReference(supplier, resource, key, refToValue, metadata);
		else
			throw new HabitatSecurityException("Unauthorized to update " + resource);
	}

	@Override
	public void addMetadata(String resource, K key, StructuredData<K, ? extends Object> metadata)
			throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.WritePermission, userProvider.get()))
			lowLevel.addMetadata(resource, key, metadata);
		else
			throw new HabitatSecurityException(userProvider.get() + " unauthorized to update " + resource);
	}

	@Override
	public void addMetadata(TransactionSupplier supplier, String resource, K key,
			StructuredData<K, ? extends Object> metadata) throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.WritePermission, userProvider.get()))
			lowLevel.addMetadata(supplier, resource, key, metadata);
		else
			throw new HabitatSecurityException("Unauthorized to update " + resource);
	}

	@Override
	public void addMetadata(String resource, K key, StructuredData<K, ? extends Object> metadata, String specialLabel)
			throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.WritePermission, userProvider.get()))
			lowLevel.addMetadata(resource, key, metadata, specialLabel);
		else
			throw new HabitatSecurityException(userProvider.get() + " unauthorized to update " + resource);
	}

	@Override
	public void addMetadata(TransactionSupplier supplier, String resource, K key,
			StructuredData<K, ? extends Object> metadata, String specialLabel) throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.WritePermission, userProvider.get()))
			lowLevel.addMetadata(supplier, resource, key, metadata, specialLabel);
		else
			throw new HabitatSecurityException("Unauthorized to update " + resource);
	}
	
	@Override
	public void connect() throws SQLException {
		lowLevel.connect();
	}

	@Override
	public boolean execTransaction(String resource, Function<TransactionSupplier, Boolean> transactionFn)
			throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.WritePermission, userProvider.get()))
			return lowLevel.execTransaction(resource, transactionFn);
		else
			throw new HabitatSecurityException("Unauthorized to update " + resource);
	}

	@Override
	public int getCount(String resource) throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.ReadPermission, userProvider.get()))
			return lowLevel.getCount(resource);
		else
			throw new HabitatSecurityException("Unauthorized to read " + resource);
	}

	@Override
	public Object getData(String resource, K key) throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.ReadPermission, userProvider.get()))
			return lowLevel.getData(resource, key);
		else
			throw new HabitatSecurityException("Unauthorized to read " + resource);
	}

	@Override
	public Object getData(TransactionSupplier supplier, String resource, K key) throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.ReadPermission, userProvider.get()))
			return lowLevel.getData(supplier, resource, key);
		else
			throw new HabitatSecurityException("Unauthorized to read " + resource);
	}

	@Override
	public StructuredData<K, Object> getMetadata(String resource, K key) throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.ReadPermission, userProvider.get()))
			return lowLevel.getMetadata(resource, key);
		else
			throw new HabitatSecurityException("Unauthorized to read " + resource);
	}

	@Override
	public StructuredData<K, Object> getMetadata(TransactionSupplier supplier, String resource, K key)
			throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.ReadPermission, userProvider.get()))
			return lowLevel.getMetadata(supplier, resource, key);
		else
			throw new HabitatSecurityException("Unauthorized to read " + resource);
	}

	@Override
	public Iterable<Pair<K, StructuredData<K, Object>>> getMetadataForResource(String resource)
			throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.ReadPermission, userProvider.get()))
			return lowLevel.getMetadataForResource(resource);
		else
			throw new HabitatSecurityException("Unauthorized to read " + resource);
	}

	@Override
	public Iterable<Pair<K, StructuredData<K, Object>>> getMetadataForResource(TransactionSupplier supplier,
			String resource) throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.ReadPermission, userProvider.get()))
			return lowLevel.getMetadataForResource(supplier, resource);
		else
			throw new HabitatSecurityException("Unauthorized to read " + resource);
	}

	@Override
	public edu.upenn.cis.db.habitat.core.api.StorageApi.StoreClass getStoreClass() {
		return StoreClass.GRAPH;
	}

	@Override
	public boolean isSuitableForMetadata() {
		return lowLevel.isSuitableForMetadata();
	}

	@Override
	public boolean isSuitableToStore(Object obj) {
		return lowLevel.isSuitableToStore(obj);
	}

	@Override
	public void removeKey(String resource, K key) throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.WritePermission, userProvider.get()))
			lowLevel.removeKey(resource, key);
		else
			throw new HabitatSecurityException("Unauthorized to update " + resource);
	}

	@Override
	public void removeKey(TransactionSupplier supplier, String resource, K key) throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.WritePermission, userProvider.get()))
			lowLevel.removeKey(supplier, resource, key);
		else
			throw new HabitatSecurityException("Unauthorized to update " + resource);
	}

	@Override
	public void removeMetadata(String resource, K key, String metadataField) throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.WritePermission, userProvider.get()))
			lowLevel.removeMetadata(resource, key, metadataField);
		else
			throw new HabitatSecurityException("Unauthorized to update " + resource);
	}

	@Override
	public void removeMetadata(TransactionSupplier supplier, String resource, K key, String metadataField)
			throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.WritePermission, userProvider.get()))
			lowLevel.removeMetadata(supplier, resource, key, metadataField);
		else
			throw new HabitatSecurityException("Unauthorized to update " + resource);
	}

	@Override
	public void replaceMetadata(String resource, K key, StructuredData<K, ? extends Object> metadata)
			throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.WritePermission, userProvider.get()))
			lowLevel.replaceMetadata(resource, key, metadata);
		else
			throw new HabitatSecurityException("Unauthorized to update " + resource);
	}

	@Override
	public void replaceMetadata(TransactionSupplier supplier, String resource, K key,
			StructuredData<K, ? extends Object> metadata) throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.WritePermission, userProvider.get()))
			lowLevel.replaceMetadata(supplier, resource, key, metadata);
		else
			throw new HabitatSecurityException("Unauthorized to update " + resource);
	}

	@Override
	public void store(String resource, K key, Object value, StructuredData<K, ? extends Object> metadata)
			throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.WritePermission, userProvider.get()))
			lowLevel.store(resource, key, value, metadata);
		else
			throw new HabitatSecurityException("Unauthorized to update " + resource);
	}

	@Override
	public void store(TransactionSupplier supplier, String resource, K key, Object value,
			StructuredData<K, ? extends Object> metadata) throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.WritePermission, userProvider.get()))
			lowLevel.store(supplier, resource, key, value, metadata);
		else
			throw new HabitatSecurityException("Unauthorized to update " + resource);
	}

	@Override
	public void storeFile(String resource, K key, InputStream file, int streamLength,
			StructuredData<K, ? extends Object> metadata) throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.WritePermission, userProvider.get()))
			lowLevel.storeFile(resource, key, file, streamLength, metadata);
		else
			throw new HabitatSecurityException("Unauthorized to update " + resource);
	}

	@Override
	public void storeFile(TransactionSupplier supplier, String resource, K key, InputStream file, int streamLength,
			StructuredData<K, ? extends Object> metadata) throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.WritePermission, userProvider.get()))
			lowLevel.storeFile(supplier, resource, key, file, streamLength, metadata);
		else
			throw new HabitatSecurityException("Unauthorized to update " + resource);
	}

	@Override
	public void storeUrlContents(String resource, K key, URL url, StructuredData<K, ? extends Object> metadata)
			throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.WritePermission, userProvider.get()))
			lowLevel.storeUrlContents(resource, key, url, metadata);
		else
			throw new HabitatSecurityException("Unauthorized to update " + resource);
	}

	@Override
	public void storeUrlContents(TransactionSupplier supplier, String resource, K key, URL url,
			StructuredData<K, ? extends Object> metadata) throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.WritePermission, userProvider.get()))
			lowLevel.storeUrlContents(supplier, resource, key, url, metadata);
		else
			throw new HabitatSecurityException("Unauthorized to update " + resource);
	}

	@Override
	public boolean isDataLegal(StructuredData<K, ? extends Object> metadata) {
		return lowLevel.isDataLegal(metadata);
	}

	@Override
	public String getStoreUuid() {
		return lowLevel.getStoreUuid();
	}

	@Override
	public String getSuperuser() {
		return lowLevel.getSuperuser();
	}

	@Override
	public String getHost() {
		return lowLevel.getHost();
	}

	@Override
	public int getPort() {
		return lowLevel.getPort();
	}

	@Override
	public String getDatabase() {
		return lowLevel.getDatabase();
	}

	@Override
	public void createOrResetGraph(String resourceName) throws HabitatServiceException {
		if (resourceName == null || resourceName.trim().isEmpty()) {
			throw new HabitatServiceException(
					"resource name cannot be null or empty");
		}
		resourceName = resourceName.trim();
		if (resourceName.length() > maxObjectNameLength) {
			throw new HabitatServiceException(String.format(
					"resource name length %s too long. Must be <= %d",
					resourceName.length(),
					maxObjectNameLength));
		}
		final String username = userProvider.get();
		if (permissions.hasPermission(
				resourceName, 
				PermissionApi.AdminPermission, 
				username)) {
			lowLevel.createOrResetGraph(resourceName);
			return;
		}
		final Lock lock = createGraphLocks.get(resourceName);
		if (!lock.tryLock()) {
			throw new HabitatSecurityException("Could not lock graph name: ["
					+ resourceName + "]");
		}
		try {
			final boolean claimed = permissions
					.isClaimedAsObjectId(resourceName);
			if (!claimed) {
				permissions.grantUserPermissionOn(resourceName,
						PermissionApi.AdminPermission, username);

				lowLevel.createOrResetGraph(resourceName);
			} else {
				final boolean alreadyAdmin =
						permissions.hasPermission(
								resourceName,
								PermissionApi.AdminPermission,
								username);
				if (alreadyAdmin) {
					lowLevel.createOrResetGraph(resourceName);
				} else {
					throw new HabitatSecurityException("Graph name ["
							+ resourceName + "] not available");
				}
			}
		} finally {
			lock.unlock();
		}
	}

	@Override
	public void createGraph(String resourceName) throws HabitatServiceException {
		if (resourceName == null || resourceName.trim().isEmpty()) {
			throw new HabitatServiceException(
					"resource name cannot be null or empty");
		}
		resourceName = resourceName.trim();
		if (resourceName.length() > maxObjectNameLength) {
			throw new HabitatServiceException(String.format(
					"resource name length %s too long. Must be <= %d",
					resourceName.length(),
					maxObjectNameLength));
		}
		final String username = userProvider.get();
		if (permissions.hasPermission(
				resourceName, 
				PermissionApi.AdminPermission, 
				username)) {
			return;
		}
		final Lock lock = createGraphLocks.get(resourceName);
		if (!lock.tryLock()) {
			throw new HabitatSecurityException("Could not lock graph name: ["
					+ resourceName + "]");
		}
		try {
			final boolean claimed = permissions
					.isClaimedAsObjectId(resourceName);
			if (!claimed) {
				permissions.grantUserPermissionOn(resourceName,
						PermissionApi.AdminPermission, username);

				lowLevel.createGraph(resourceName);
			} else {
				final boolean alreadyAdmin =
						permissions.hasPermission(
								resourceName,
								PermissionApi.AdminPermission,
								username);
				if (!alreadyAdmin) {
					throw new HabitatSecurityException("Graph name ["
							+ resourceName + "] not available");
				}
			}
		} finally {
			lock.unlock();
		}
	}
	
	@Override
	public List<SubgraphInstance> getSubgraphs(String resource,
			SubgraphTemplate template,
			int limit,
			Long since) throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.ReadPermission, userProvider.get())) {
			final Iterable<SubgraphInstance> subgraphs = lowLevel.getSubgraphs(resource, template, limit, since);
			return StreamSupport.stream(subgraphs.spliterator(), true).collect(Collectors.toList());
		} else {
			throw new HabitatSecurityException("Unauthorized to update " + resource);
		}
	}

	@Override
	public List<SubgraphInstance> getSubgraphs(TransactionSupplier supplier,
			String resource, SubgraphTemplate template, int limit, Long since)
			throws HabitatServiceException {
		if (permissions.hasPermission(resource, PermissionApi.ReadPermission, userProvider.get())) {
			final Iterable<SubgraphInstance> subgraphs = lowLevel.getSubgraphs(supplier, resource, template, limit, since);
			return StreamSupport.stream(subgraphs.spliterator(), true).collect(Collectors.toList());
		} else {
			throw new HabitatSecurityException("Unauthorized to update " + resource);
		}		
	}
	
}
