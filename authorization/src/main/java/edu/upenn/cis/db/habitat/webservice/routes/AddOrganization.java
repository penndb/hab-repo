/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;

import edu.upenn.cis.db.habitat.auth.AuthServices;
import edu.upenn.cis.db.habitat.core.api.UserApi;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;
import edu.upenn.cis.db.habitat.webservice.routes.AuthTypes.OrgDetails;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;
import spark.Request;
import spark.Response;
import spark.Route;

@Api(tags = AuthServices.PLUGIN_NAME)
@Path("/auth/organizations/{orgname}")
@Produces("application/json")
public class AddOrganization implements Route {
	final static Logger logger = LogManager.getLogger(AddOrganization.class);
	
	UserApi backend;
	AuthServices caller;
	
	public AddOrganization(UserApi backend, AuthServices services) {
		this.backend = backend;
		caller = services;
	}
	
	@POST
	@ApiOperation(value = "Adds an organization", nickname="AddOrganization",
			authorizations = { @Authorization(value="jwt")})
	@ApiImplicitParams({ //
		@ApiImplicitParam(required = true, dataType="string", name="orgname", paramType = "path"), //
		@ApiImplicitParam(
				required = true, 
				dataTypeClass=AuthTypes.OrgDetails.class, 
				name="details",
				paramType = "body",
				examples= @Example(value = {
						@ExampleProperty(mediaType="application/json",
								value="{street1:'s1',street2:'s2',city:'c',state:'st',zip:'zip',country:'c',phone:'p',web:'http://'}")})), //
	}) //
	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "Success", response=Integer.class), //
			@ApiResponse(code = 400, message = "Invalid input data", response=ResponseError.class), //
			@ApiResponse(code = 401, message = "Unauthorized", response=ResponseError.class), //
	})
	@Override
	public Object handle(
			@ApiParam(hidden=true) Request request, 
			@ApiParam(hidden=true) Response response) throws Exception {
    	String orgname = request.params("organization");
    	
    	logger.debug("Request to add organization " + orgname);
    	logger.debug(request.body());

    	OrgDetails details = null;

		try {
			details = new Gson().fromJson(request.body(), OrgDetails.class);
		} catch (Exception e) {
			response.status(400);
			return new ResponseError("Unable to parse message body for organization");
		}
		
		if (orgname == null || orgname.isEmpty()) {
			response.status(400);
			return new ResponseError("Invalid organization name");
		}
		
    	// TODO: revisit whether we can just add organizations??
    	
    	int id = backend.addOrganization(
    			orgname, 
    			details.street1, 
    			details.street2, 
    			details.city, 
    			details.state, 
    			details.zip, 
    			details.country, 
    			details.phone, 
    			details.web); 
    	if (id >= 0) {
    		response.body(Integer.toString(id));
    		return true;
    	} else {
    		response.status(401);
    		return new ResponseError("Unable to create organization " + orgname);
    	}
	}

}
