/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.db.habitat.core.api.PermissionApi;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;
import edu.upenn.cis.db.habitat.permissions.PermissionServices;
import edu.upenn.cis.db.habitat.secure.ProtectedAuthBackend;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import spark.Request;
import spark.Response;
import spark.Route;

@Api(tags = PermissionServices.PLUGIN_NAME)
@Path("/perms/name/{name}/id")
@Produces("application/json")
public class GetPermissionId implements Route {
	final static Logger logger = LogManager.getLogger(GetPermissionId.class);
	
	final ProtectedAuthBackend backend;
	final PermissionApi perms;
	final PermissionServices caller;
	
	public GetPermissionId(ProtectedAuthBackend backend, PermissionApi perms, PermissionServices services) {
		this.backend = backend;
		this.perms = perms;
		caller = services;
	}
	
	@GET
	@ApiOperation(value = "Gets a permission's ID from its name", nickname="GetPermissionId",
			authorizations = { @Authorization(value="jwt")})
	@ApiImplicitParams({ //
		@ApiImplicitParam(required = true, dataType="string", name="name", paramType = "path"), //
	}) //
	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "Success", response=Integer.class), //
			@ApiResponse(code = 400, message = "Invalid input data", response=ResponseError.class), //
			@ApiResponse(code = 401, message = "Unauthorized", response=ResponseError.class), //
	})
	@Override
	public Object handle(
			@ApiParam(hidden=true) Request request, 
			@ApiParam(hidden=true) Response response) throws Exception {
    	String permName = request.params("name");
    	
    	if (permName == null) {
    		response.status(400);
    		return new ResponseError("Illegal permission name provided");
    	}

    	logger.debug("Request to get permission " + permName);
    	logger.debug(request.body());
		if (permName == null || permName.isEmpty()) {
			response.status(400);
			return new ResponseError("Invalid group");
		}
		
    	Map<String,Object> info = caller.getProfileMap(request, response);
    	if (info.containsKey("username")) {
    	} else {
    		response.status(401);
    		return new ResponseError("Cannot find credentials -- are you logged in?");
    	}
    	
    	Integer id = perms.getPermissionIdFrom(permName); 
    	if (id != null && id > 0) {
    		response.body(String.valueOf(id));
    		return true;
    	} else {
    		response.status(401);
    		return new ResponseError("Unable to look up permission " + permName);
    	}
	}

}
