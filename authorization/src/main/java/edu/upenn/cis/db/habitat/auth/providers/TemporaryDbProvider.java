/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.auth.providers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.conf.RenderMapping;
import org.jooq.conf.Settings;
import org.jooq.impl.DSL;

/**
 * The temporary DB provider is a simple database purely for testing.
 * It uses H2 to create a database (and before starting, it
 * deletes the database if it exists)
 * 
 * @author ZacharyIves
 *
 */
public class TemporaryDbProvider implements RdbmsProvider {
	
	String file = "./test;database_to_upper=false";
	String url = "jdbc:h2:" + file;
	Connection conn;
	final static Logger logger = LogManager.getLogger(PostgresProvider.class);

	static boolean alreadyRunning = false;

	@Override
	public synchronized DSLContext get() {
		try {
			// Wipe the test database!

			if (!alreadyRunning)
				Files.deleteIfExists(Paths.get("test.mv.db"));
			
			alreadyRunning = true;
			Class.forName("org.h2.Driver");
			conn = DriverManager.getConnection(url, 
					"sa", 
					"");
			
			DSLContext create = DSL.using(conn, SQLDialect.H2);
			create.createSchemaIfNotExists("public");
			Settings settings = new Settings()
					.withRenderMapping(new RenderMapping()
							.withDefaultSchema("public"))
					.withRenderSchema(true);
			
			return DSL.using(conn, SQLDialect.H2, settings);
			
		} catch (SQLException | ClassNotFoundException e) {
			logger.error("SQL Error in connecting to DBMS backend: " + e.getMessage());
			System.exit(1);
		} catch (IOException e) {
			logger.error("File I/O Error in connecting to DBMS backend: " + e.getMessage());
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}
}
