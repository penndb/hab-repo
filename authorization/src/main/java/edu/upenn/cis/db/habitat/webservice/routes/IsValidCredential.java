/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import java.util.Map;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;

import edu.upenn.cis.db.habitat.auth.AuthServices;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;
import edu.upenn.cis.db.habitat.secure.ProtectedAuthBackend;
import edu.upenn.cis.db.habitat.webservice.routes.AuthTypes.UserCredentials;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import spark.Request;
import spark.Response;
import spark.Route;

@Api(tags = AuthServices.PLUGIN_NAME)
@Path("/auth/services/local/user/{username}/credential")
@Produces("application/json")
public class IsValidCredential implements Route {
	final static Logger logger = LogManager.getLogger(IsValidCredential.class);
	
	ProtectedAuthBackend backend;
	AuthServices caller;
	
	public IsValidCredential(ProtectedAuthBackend backend, AuthServices services) {
		this.backend = backend;
		caller = services;
	}
	
	@POST
	@ApiOperation(value = "Returns whether a local user credential is valid", nickname="IsValidCredential",
			authorizations = { @Authorization(value="jwt")})
	@ApiImplicitParams({ //
		@ApiImplicitParam(required = true, dataType="string", name="username", paramType = "path"), //
		@ApiImplicitParam(required = true, dataTypeClass=AuthTypes.UserCredentials.class, 
		name="credentials", paramType = "body"), //
	}) //
	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "Success", response=Boolean.class), //
			@ApiResponse(code = 400, message = "Invalid input data", response=ResponseError.class), //
			@ApiResponse(code = 401, message = "Unauthorized", response=ResponseError.class), //
	})
	@Override
	public Object handle(
			@ApiParam(hidden=true) Request request, 
			@ApiParam(hidden=true) Response response) throws Exception {
    	String username = request.params("username");
    	String token = null;
    	UserCredentials credentials;
		try {
			credentials = new Gson().fromJson(request.body(), UserCredentials.class);
			token = credentials.password;
		} catch (Exception e) {
			response.status(400);
			return new ResponseError("Unable to parse message body for password");
		}

    	
    	if (username == null || username.isEmpty() || token == null || token.isEmpty()) {
    		response.status(400);
    		return new ResponseError("Invalid parameters");
    	}
    	Map<String,Object> info = caller.getProfileMap(request, response);
    	if (info.containsKey("username")) {
//    		if (!info.get("username").equals(username)) {
    		if (!backend.isAdminFor((String)info.get("username"), username)) {
        		response.status(401);
        		return new ResponseError("Unauthorized or no profile");
    		}
    	} else {
    		response.status(401);
    		return new ResponseError("Unauthorized or no profile");
    	}
    	
    	logger.debug("Request to validate password for user " + username);

    	if (backend.isUserCredentialValid(username, token))
    		return Boolean.valueOf(true);
    	else
    		return Boolean.valueOf(false);
	}

}
