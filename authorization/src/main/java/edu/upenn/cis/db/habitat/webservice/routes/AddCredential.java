/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import java.util.Map;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;

import edu.upenn.cis.db.habitat.auth.AuthServices;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;
import edu.upenn.cis.db.habitat.secure.ProtectedAuthBackend;
import edu.upenn.cis.db.habitat.webservice.routes.AuthTypes.UserCredentials;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;
import spark.Request;
import spark.Response;
import spark.Route;

@Api(tags = AuthServices.PLUGIN_NAME)
@Path("/auth/services/{service}/user/{username}")
@Produces("application/json")
public class AddCredential implements Route {
	final static Logger logger = LogManager.getLogger(AddCredential.class);
	
	ProtectedAuthBackend backend;
	AuthServices caller;
	
	public AddCredential(ProtectedAuthBackend backend, AuthServices services) {
		this.backend = backend;
		caller = services;
	}
	
	@POST
	@ApiOperation(value = "Adds a service credential for the user", nickname="AddCredential",
			authorizations = { @Authorization(value="jwt")})
	@ApiImplicitParams({ //
		@ApiImplicitParam(required = true, dataType="string", name="service", paramType = "path"), //
		@ApiImplicitParam(required = true, dataType="string", name="username", paramType = "path"), //
		@ApiImplicitParam(
				required = true, 
				dataTypeClass=AuthTypes.UserCredentials.class, 
				name="credentials",
				paramType = "body",
				examples= @Example(value = {
						@ExampleProperty(mediaType="application/json",
								value="{service:'local',password='password'}")})), //
	}) //
	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "Success", response=AuthTypes.UserInfo.class), //
			@ApiResponse(code = 400, message = "Invalid input data", response=ResponseError.class), //
			@ApiResponse(code = 401, message = "Unauthorized", response=ResponseError.class), //
	})
	@Override
	public Object handle(
			@ApiParam(hidden=true) Request request, 
			@ApiParam(hidden=true) Response response) throws Exception {
    	String service = request.params("service");
    	String username = request.params("username");

    	logger.debug("Request to add user credential " + username);
    	logger.debug(request.body());

    	UserCredentials credentials = null;
    	String token = null;
		try {
			credentials = new Gson().fromJson(request.body(), UserCredentials.class);
			token = credentials.password;
		} catch (Exception e) {
			response.status(400);
			return new ResponseError("Unable to parse message body for password");
		}
		
		if (service == null || service.isEmpty()) {
			response.status(400);
			return new ResponseError("Invalid service for credential");
		}
		
    	Map<String,Object> info = caller.getProfileMap(request, response);
    	if (info.containsKey("username")) {
    		
        	String requestor = (String)info.get("username");
    		if (!backend.isUserValid(requestor, "local") ||
    				!backend.isAdminFor(requestor, username)) {
        		response.status(401);
        		return new ResponseError("Can only add credentials to your own account or one you administer");
    		}
    	} else {
    		response.status(401);
    		return new ResponseError("Cannot find credentials -- are you logged in?");
    	}
    	
    	if (!service.equals("local") && backend.addCredential(username, service, "http", token)) {
    		return backend.getUserInfo(username);
    	} else {
    		response.status(401);
    		return new ResponseError("Unable to create credential for " + service + " for " + username);
    	}
	}

}
