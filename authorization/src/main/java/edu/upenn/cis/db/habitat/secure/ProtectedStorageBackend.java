/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.secure;

import java.io.InputStream;
import java.net.URL;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;
import java.util.function.Function;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import edu.upenn.cis.db.habitat.core.api.PermissionApi;
import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.TransactionSupplier;
import edu.upenn.cis.db.habitat.core.webservice.HabitatSecurityException;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.repository.storage.StorageApiWithMetadataLocal;
import edu.upenn.cis.db.habitat.util.FilterIterableKey;
import edu.upenn.cis.db.habitat.util.FilterIterableKeyTuple;

public class ProtectedStorageBackend<K,V,B> implements StorageApiWithMetadataLocal<K,V,B> {
	
	public static final String UNPROTECTED_STORAGE_API = "UnprotectedStorageApiWithMetadataLocal";
	
	final static Logger logger = LogManager.getLogger(ProtectedStorageBackend.class);
	
	final StorageApiWithMetadataLocal<K,V,B> lowLevelApi;
	final PermissionApi permission;
	final ProtectedAuthBackend auth;
	
	@Inject
	public ProtectedStorageBackend(
			ProtectedAuthBackend auth, 
			PermissionApi perms, 
			@Named(UNPROTECTED_STORAGE_API) StorageApiWithMetadataLocal<K,V,B> basicApi) {
		this.auth = auth;
		this.permission = perms;
		lowLevelApi = basicApi;
	}

	@Override
	public void addMetadata(String resource, K key, StructuredData<K,? extends V> metadata) throws HabitatServiceException {
		if (isNewOrOverwritableByUser(key)) {
			lowLevelApi.addMetadata(resource, key, metadata);
			permission.grantUserPermissionOn(key.toString(), PermissionApi.AdminPermission, auth.getSessionUsername());
			permission.grantUserPermissionOn(key.toString(), PermissionApi.WritePermission, auth.getSessionUsername());
		} else {
			logger.error("Unauthorized add request on " + key.toString());
			throw new HabitatSecurityException("Unauthorized request on " + key.toString());
		}
	}

	@Override
	public void addMetadata(TransactionSupplier supplier, String resource, K key, StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		if (isNewOrOverwritableByUser(key)) {
			lowLevelApi.addMetadata(supplier, resource, key, metadata);
			permission.grantUserPermissionOn(key.toString(), PermissionApi.AdminPermission, auth.getSessionUsername());
			permission.grantUserPermissionOn(key.toString(), PermissionApi.WritePermission, auth.getSessionUsername());
		} else {
			logger.error("Unauthorized add request on " + key.toString());
			throw new HabitatSecurityException("Unauthorized request on " + key.toString());
		}
	}

	@Override
	public void addMetadata(String resource, K key, StructuredData<K,? extends V> metadata,
			String specialLabel) throws HabitatServiceException {
		if (isNewOrOverwritableByUser(key)) {
			lowLevelApi.addMetadata(resource, key, metadata, specialLabel);
			permission.grantUserPermissionOn(key.toString(), PermissionApi.AdminPermission, auth.getSessionUsername());
			permission.grantUserPermissionOn(key.toString(), PermissionApi.WritePermission, auth.getSessionUsername());
		} else {
			logger.error("Unauthorized add request on " + key.toString());
			throw new HabitatSecurityException("Unauthorized request on " + key.toString());
		}
	}

	@Override
	public void addMetadata(TransactionSupplier supplier, String resource, K key, StructuredData<K, ? extends V> metadata,
			String specialLabel) throws HabitatServiceException {
		if (isNewOrOverwritableByUser(key)) {
			lowLevelApi.addMetadata(supplier, resource, key, metadata, specialLabel);
			permission.grantUserPermissionOn(key.toString(), PermissionApi.AdminPermission, auth.getSessionUsername());
			permission.grantUserPermissionOn(key.toString(), PermissionApi.WritePermission, auth.getSessionUsername());
		} else {
			logger.error("Unauthorized add request on " + key.toString());
			throw new HabitatSecurityException("Unauthorized request on " + key.toString());
		}
	}

	@Override
	public void connect() throws SQLException {
		lowLevelApi.connect();
	}

	@Override
	public boolean execTransaction(String resource, Function<TransactionSupplier, Boolean> transactionFn) throws HabitatServiceException {
		return lowLevelApi.execTransaction(resource, transactionFn);
	}

	/**
	 * Returns a count of only those objects we have permission on! 
	 */
	@Override
	public int getCount(String resource) throws HabitatServiceException {
		Iterator<Pair<K,StructuredData<K,V>>> iter = this.getMetadataForResource(resource).iterator();
		
		int count = 0;
		while (iter.hasNext()) {
			iter.next();
			count++;
		}
		return count;
	}

	@Override
	public B getData(String resource, K key) throws HabitatServiceException {
		if (isReadableByUser(key))
			return lowLevelApi.getData(resource, key);
		else {
			logger.error("Unauthorized get request on " + key.toString());
			throw new HabitatSecurityException("Unauthorized request on " + key.toString());
		}
	}

	@Override
	public B getData(TransactionSupplier supplier, String resource, K key) throws HabitatServiceException {
		if (isReadableByUser(key))
			return lowLevelApi.getData(supplier, resource, key);
		else
			throw new HabitatSecurityException("Unauthorized get request on " + key.toString());
	}
	
	@Override
	public String getDatabase() {
		return lowLevelApi.getDatabase();
	}

	@Override
	public String getHost() {
		return lowLevelApi.getHost();
	}

	
	@Override
	public Iterable<K> getKeysMatching(String resource, String metadataField, Map<K, ? extends V> match) throws HabitatServiceException {
		Iterable<K> child = lowLevelApi.getKeysMatching(resource, metadataField, match);
		
		return new FilterIterableKey<K>(
				child,
				data -> {
					try {
						return this.isReadableByUser(data);
					} catch (HabitatServiceException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();

						return false;
					}
				}
				);
	}

	@Override
	public Iterable<K> getKeysMatching(TransactionSupplier supplier, String resource, 
			String metadataField, Map<K, ? extends V> match) throws HabitatServiceException {
		Iterable<K> child =  lowLevelApi.getKeysMatching(supplier, resource, metadataField, match);
		return new FilterIterableKey<K>(
				child,
				data -> {
					try {
						return this.isReadableByUser(data);
					} catch (HabitatServiceException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();

						return false;
					}
				}
				);
	}

	@Override
	public Iterable<Pair<K, StructuredData<K,V>>> getLinksFrom(String resource, K key1) throws HabitatServiceException {
		if (isReadableByUser(key1))
			return lowLevelApi.getLinksFrom(resource, key1);
		else
			throw new HabitatSecurityException("Unauthorized get request on " + key1.toString());
	}

	@Override
	public Iterable<Pair<K, StructuredData<K,V>>> getLinksFrom(TransactionSupplier supplier, String resource, K key1)
			throws HabitatServiceException {
		if (isReadableByUser(key1))
			return lowLevelApi.getLinksFrom(supplier, resource, key1);
		else
			throw new HabitatSecurityException("Unauthorized get request on " + key1.toString());
	}

	@Override
	public Iterable<Pair<K, StructuredData<K,V>>> getLinksTo(String resource, K key1) throws HabitatServiceException {
		if (isReadableByUser(key1))
			return lowLevelApi.getLinksTo(resource, key1);
		else
			throw new HabitatSecurityException("Unauthorized get request on " + key1.toString());
	}

	@Override
	public Iterable<Pair<K, StructuredData<K,V>>> getLinksTo(TransactionSupplier supplier, String resource, K key1)
			throws HabitatServiceException {
		if (isReadableByUser(key1))
			return lowLevelApi.getLinksTo(supplier, resource, key1);
		else
			throw new HabitatSecurityException("Unauthorized get request on " + key1.toString());
	}

	@Override
	public StructuredData<K,V> getMetadata(String resource, K key) throws HabitatServiceException {
		if (isReadableByUser(key))
			return lowLevelApi.getMetadata(resource, key);
		else {
			logger.error("Unauthorized get request on " + key.toString());
			throw new HabitatSecurityException("Unauthorized request on " + key.toString());
		}

	}

	@Override
	public StructuredData<K,V> getMetadata(TransactionSupplier supplier, String resource, K key) throws HabitatServiceException {
		if (isReadableByUser(key))
			return lowLevelApi.getMetadata(supplier, resource, key);
		else {
			logger.error("Unauthorized get request on " + key.toString());
			throw new HabitatSecurityException("Unauthorized request on " + key.toString());
		}
	}
	
	@Override
	public Iterable<Pair<K,StructuredData<K,V>>> getMetadataForResource(String resource) throws HabitatServiceException {
		return new FilterIterableKeyTuple<K,V>(
				lowLevelApi.getMetadataForResource(resource),
				data -> {
					try {
						return this.isReadableByUser(((Pair<K, StructuredData<K,V>>) data).getLeft());
					} catch (HabitatServiceException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();

						return false;
					}
				}
				);
	}

	@Override
	public Iterable<Pair<K,StructuredData<K,V>>> getMetadataForResource(TransactionSupplier supplier, String resource)
			throws HabitatServiceException {
		return new FilterIterableKeyTuple<K,V>(lowLevelApi.getMetadataForResource(supplier, resource),
				data -> {
					try {
						return this.isReadableByUser(((Pair<K, StructuredData<K,V>>) data).getLeft());
					} catch (HabitatServiceException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						return false;
					}
				}
				);
	}

	@Override
	public int getPort() {
		return lowLevelApi.getPort();
	}

	@Override
	public edu.upenn.cis.db.habitat.core.api.StorageApi.StoreClass getStoreClass() {
		return lowLevelApi.getStoreClass();
	}

	@Override
	public String getStoreUuid() {
		return lowLevelApi.getStoreUuid();
	}

	@Override
	public String getSuperuser() {
		return lowLevelApi.getSuperuser();
	}

	@Override
	public boolean isDataLegal(StructuredData<K, ? extends V> metadata) {
		return lowLevelApi.isDataLegal(metadata);
	}

	private boolean isNewOrOverwritableByUser(K object) throws HabitatServiceException {
		if (!permission.isClaimedAsObjectId(object.toString())
				|| isOverwriteableByUser(object))
			return true;

		return false;
	}

	private boolean isOverwriteableByUser(K object) throws HabitatServiceException {
		if (auth.isSiteAdmin())
			return true;
		
		if (permission.hasPermission(object.toString(), 
				PermissionApi.WritePermission, 
				auth.getSessionUsername()))
			return true;
		
		if (permission.hasPermission(object.toString(), 
				PermissionApi.AdminPermission, 
				auth.getSessionUsername()))
			return true;

		return false;
	}

	private boolean isReadableByUser(K object) throws HabitatServiceException {
		return isOverwriteableByUser(object) ||
				permission.hasPermission(object.toString(), 
						PermissionApi.ReadPermission, 
						auth.getSessionUsername());
	}

	@Override
	public boolean isSuitableForMetadata() {
		return lowLevelApi.isSuitableForMetadata();
	}

	@Override
	public boolean isSuitableToStore(Object obj) {
		return lowLevelApi.isSuitableToStore(obj);
	}

	@Override
	public void link(String resource, K key1, String label, K key2, 
			StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		if (isOverwriteableByUser(key1) && isOverwriteableByUser(key2)) {
			lowLevelApi.link(resource, key1, label, key2, metadata);
		} else {
			logger.error("Unauthorized link request on " + key1.toString() + " and " + key2.toString());
			throw new HabitatSecurityException("Unauthorized request on " + key1.toString() + " and " + key2.toString());
		}
	}

	@Override
	public void link(TransactionSupplier supplier, String resource, K key1, String label, K key2, 
			StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		if (isOverwriteableByUser(key1) && isOverwriteableByUser(key2)) {
			lowLevelApi.link(supplier, resource, key1, label, key2, metadata);
		} else {
			logger.error("Unauthorized link request on " + key1.toString() + " and " + key2.toString());
			throw new HabitatSecurityException("Unauthorized request on " + key1.toString() + " and " + key2.toString());
		}
	}

	@Override
	public void removeKey(String resource, K key) throws HabitatServiceException {
		if (isOverwriteableByUser(key)) {
			lowLevelApi.removeKey(resource, key);
			permission.removeKey(key.toString());
		} else {
			logger.error("Unauthorized remove request on " + key.toString());
			throw new HabitatSecurityException("Unauthorized request on " + key.toString());
		}
	}

	@Override
	public void removeKey(TransactionSupplier supplier, String resource, K key) throws HabitatServiceException {
		if (isOverwriteableByUser(key)) {
			lowLevelApi.removeKey(resource, key);
			permission.removeKey(key.toString());
		} else {
			logger.error("Unauthorized remove request on " + key.toString());
			throw new HabitatSecurityException("Unauthorized request on " + key.toString());
		}
	}

	@Override
	public void removeMetadata(String resource, K key, String metadataField) throws HabitatServiceException {
		if (isOverwriteableByUser(key)) {
			lowLevelApi.removeMetadata(resource, key, metadataField);
		} else {
			logger.error("Unauthorized request on " + key.toString());
			throw new HabitatSecurityException("Unauthorized request on " + key.toString());
		}
	}

	@Override
	public void removeMetadata(TransactionSupplier supplier, String resource, K key, String metadataField) throws HabitatServiceException {
		if (isOverwriteableByUser(key)) {
			lowLevelApi.removeMetadata(supplier, resource, key, metadataField);
		} else {
			logger.error("Unauthorized remove request on " + key.toString());
			throw new HabitatSecurityException("Unauthorized request on " + key.toString());
		}

	}

	@Override
	public void replaceMetadata(String resource, K key, StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		if (isOverwriteableByUser(key))
			lowLevelApi.replaceMetadata(resource, key, metadata);
		 else {
			logger.error("Unauthorized replace request on " + key.toString());
			throw new HabitatSecurityException("Unauthorized request on " + key.toString());
		 }
	}

	@Override
	public void replaceMetadata(TransactionSupplier supplier, String resource, K key, 
			StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		if (isOverwriteableByUser(key))
			lowLevelApi.replaceMetadata(supplier, resource, key, metadata);
		 else {
			logger.error("Unauthorized replace request on " + key.toString());
			throw new HabitatSecurityException("Unauthorized request on " + key.toString());
		 }
	}

	@Override
	public void store(String resource, K key, B value, StructuredData<K, ? extends V> metadata) 
			throws HabitatServiceException {
		if (isNewOrOverwritableByUser(key)) {
			lowLevelApi.store(resource, key, value, metadata);
			permission.grantUserPermissionOn(key.toString(), PermissionApi.AdminPermission, auth.getSessionUsername());
			permission.grantUserPermissionOn(key.toString(), PermissionApi.WritePermission, auth.getSessionUsername());
		} else {
			logger.error("Unauthorized store request on " + key.toString());
			throw new HabitatSecurityException("Unauthorized request on " + key.toString());
		}
	}

	@Override
	public void store(TransactionSupplier supplier, String resource, K key, B value, 
			StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		if (isNewOrOverwritableByUser(key)) {
			lowLevelApi.store(supplier, resource, key, value, metadata);
			permission.grantUserPermissionOn(key.toString(), PermissionApi.AdminPermission, auth.getSessionUsername());
			permission.grantUserPermissionOn(key.toString(), PermissionApi.WritePermission, auth.getSessionUsername());
		} else {
			logger.error("Unauthorized store request on " + key.toString());
			throw new HabitatSecurityException("Unauthorized request on " + key.toString());
		}
	}

	@Override
	public void storeReference(String resource, K key, String refToValue, StructuredData<K, ? extends V> metadata)
			throws HabitatServiceException {
		if (isNewOrOverwritableByUser(key)) {
			lowLevelApi.storeReference(resource, key, refToValue, metadata);
		} else {
			logger.error("Unauthorized store request on " + key.toString());
			throw new HabitatSecurityException("Unauthorized request on " + key.toString());
		}
	}

	@Override
	public void storeReference(TransactionSupplier supplier, String resource, K key, String refToValue,
			StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		if (isNewOrOverwritableByUser(key)) {
			lowLevelApi.storeReference(supplier, resource, key, refToValue, metadata);
		} else {
			logger.error("Unauthorized store request on " + key.toString());
			throw new HabitatSecurityException("Unauthorized request on " + key.toString());
		}
	}

	@Override
	public void storeFile(String resource, K key, InputStream file, 
			int length, StructuredData<K, ? extends V> metadata)
			throws HabitatServiceException {
		if (isNewOrOverwritableByUser(key)) {
			lowLevelApi.storeFile(resource, key, file, length, metadata);
		} else {
			logger.error("Unauthorized store request on " + key.toString());
			throw new HabitatSecurityException("Unauthorized request on " + key.toString());
		}
	}

	@Override
	public void storeFile(TransactionSupplier supplier, String resource, K key, InputStream file,
			int length, StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		if (isNewOrOverwritableByUser(key)) {
			lowLevelApi.storeFile(supplier, resource, key, file, length, metadata);
		} else {
			logger.error("Unauthorized store request on " + key.toString());
			throw new HabitatSecurityException("Unauthorized request on " + key.toString());
		}
	}

	@Override
	public void storeUrlContents(String resource, K key, URL url, StructuredData<K, ? extends V> metadata)
			throws HabitatServiceException {
		if (isNewOrOverwritableByUser(key)) {
			lowLevelApi.storeUrlContents(resource, key, url, metadata);
		} else {
			logger.error("Unauthorized store request on " + key.toString());
			throw new HabitatSecurityException("Unauthorized request on " + key.toString());
		}
	}

	@Override
	public void storeUrlContents(TransactionSupplier supplier, String resource, K key, URL url,
			StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		if (isNewOrOverwritableByUser(key)) {
			lowLevelApi.storeUrlContents(supplier, resource, key, url, metadata);
		} else {
			logger.error("Unauthorized store request on " + key.toString());
			throw new HabitatSecurityException("Unauthorized request on " + key.toString());
		}
	}
}
