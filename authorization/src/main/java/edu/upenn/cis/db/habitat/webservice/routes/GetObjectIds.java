/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import spark.Request;
import spark.Response;
import spark.Route;

import com.google.gson.Gson;

import edu.upenn.cis.db.habitat.core.api.PermissionApi;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;
import edu.upenn.cis.db.habitat.permissions.PermissionServices;
import edu.upenn.cis.db.habitat.secure.ProtectedAuthBackend;

@Api(tags = PermissionServices.PLUGIN_NAME)
@Path("/perms/name/{permname}/user/{username}")
@Produces("application/json")
public class GetObjectIds implements Route {
	private final static Logger logger = LogManager.getLogger(GetObjectIds.class);
	
	private final ProtectedAuthBackend backend;
	private final PermissionApi perms; 
	private final PermissionServices caller;
	
	public GetObjectIds(ProtectedAuthBackend backend, PermissionApi perms, PermissionServices services) {
		this.backend = backend;
		this.perms = perms;
		caller = services;
	}
	
	@GET
	@ApiOperation(value = "Gets the object ids for which the user has the given permission", nickname="GetObjectIds",
			authorizations = { @Authorization(value="jwt")})
	@ApiImplicitParams({ //
		@ApiImplicitParam(required = true, dataType="string", name="permname", paramType = "path"), //
		@ApiImplicitParam(required = true, dataType="string", name="username", paramType = "path"), //
	}) //
	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "Success", response=List.class), //
			@ApiResponse(code = 400, message = "Invalid input data", response=ResponseError.class), //
			@ApiResponse(code = 401, message = "Unauthorized", response=ResponseError.class), //
	})
	@Override
	public Object handle(
			@ApiParam(hidden=true) Request request, 
			@ApiParam(hidden=true) Response response) throws Exception {
		String permission = request.params("permname");
		String username = request.params("username");
    	
    	if (username == null || username.isEmpty()) {
    		response.status(400);
    		return new ResponseError("Illegal username specified");
    	}

    	logger.debug("Request to get object ids with permission " + permission + " for " + username);
		
    	Map<String,Object> info = caller.getProfileMap(request, response);
    	if (info.containsKey("username")) {
    		String requestor = (String)info.get("username");
    		if (!(backend.isAdminFor(requestor, username) ||
    				requestor.equals(username))) {
        		response.status(401);
        		return new ResponseError("Can only get details for users you administer or are in");
    		}
    	} else {
    		response.status(401);
    		return new ResponseError("Cannot find credentials -- are you logged in?");
    	}

    	Set<String> ret = perms.getObjectIds(permission, username);

    	if (ret != null) {
    		final String json = new Gson().toJson(ret);
    		return ret;
    	} else {
    		response.status(401);
    		return new ResponseError("Unable to look up object ids for " + username);
    	}
	}

}
