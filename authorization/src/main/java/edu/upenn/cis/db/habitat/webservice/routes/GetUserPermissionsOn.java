/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;

import edu.upenn.cis.db.habitat.core.api.PermissionApi;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;
import edu.upenn.cis.db.habitat.permissions.PermissionServices;
import edu.upenn.cis.db.habitat.secure.ProtectedAuthBackend;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import spark.Request;
import spark.Response;
import spark.Route;

@Api(tags = PermissionServices.PLUGIN_NAME)
@Path("/perms/objects/{object}/user/{username}")
@Produces("application/json")
public class GetUserPermissionsOn implements Route {
	final static Logger logger = LogManager.getLogger(GetUserPermissionsOn.class);
	
	final ProtectedAuthBackend backend;
	final PermissionApi perms; 
	final PermissionServices caller;
	
	public GetUserPermissionsOn(ProtectedAuthBackend backend, PermissionApi perms, PermissionServices services) {
		this.backend = backend;
		this.perms = perms;
		caller = services;
	}
	
	@GET
	@ApiOperation(value = "Gets the user's permissions on an object", nickname="GetUserPermissionsOn",
			authorizations = { @Authorization(value="jwt")})
	@ApiImplicitParams({ //
		@ApiImplicitParam(required = true, dataType="string", name="object", paramType = "path"), //
		@ApiImplicitParam(required = true, dataType="string", name="username", paramType = "path"), //
	}) //
	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "Success", response=HashSet.class), //
			@ApiResponse(code = 400, message = "Invalid input data", response=ResponseError.class), //
			@ApiResponse(code = 401, message = "Unauthorized", response=ResponseError.class), //
	})
	@Override
	public Object handle(
			@ApiParam(hidden=true) Request request, 
			@ApiParam(hidden=true) Response response) throws Exception {
		String object = request.params("object");
		String username = request.params("user");
    	
    	if (username == null || username.isEmpty()) {
    		response.status(400);
    		return new ResponseError("Illegal organization specified");
    	}

    	logger.debug("Request to get permissions on " + object + " for " + username);
		
    	Map<String,Object> info = caller.getProfileMap(request, response);
    	if (info.containsKey("username")) {
    		String requestor = (String)info.get("username");
    		if (!(backend.isAdminFor(requestor, username) ||
    				requestor.equals(username))) {
        		response.status(401);
        		return new ResponseError("Can only get details for users you administer or are in");
    		}
    		if (!(ProtectedAuthBackend.isSiteAdmin(requestor) || 
    				perms.hasPermission(object, PermissionApi.ReadPermission, requestor))) {
        		response.status(401);
        		return new ResponseError("Can only get details for objects can access");
    		}
    	} else {
    		response.status(401);
    		return new ResponseError("Cannot find credentials -- are you logged in?");
    	}

    	Set<String> ret = perms.getUserPermissionsOn(object, username);

    	if (ret != null) {
    		response.body(new Gson().toJson(ret));
    		return true;
    	} else {
    		response.status(401);
    		return new ResponseError("Unable to look up groups for " + username);
    	}
	}

}
