/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.db.habitat.auth.AuthServices;
import edu.upenn.cis.db.habitat.core.api.UserApi;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import spark.Request;
import spark.Response;
import spark.Route;

@Api(tags = AuthServices.PLUGIN_NAME)
@Path("/auth/services/{service}/user/{username}")
@Produces("application/json")
public class IsRegistered implements Route {
	final static Logger logger = LogManager.getLogger(IsRegistered.class);
	
	UserApi backend;
	AuthServices caller;
	
	public IsRegistered(UserApi backend, AuthServices services) {
		this.backend = backend;
		caller = services;
	}
	
	@GET
	@ApiOperation(value = "Returns whether a user credential is valid for a service", nickname="IsRegistered",
			authorizations = { @Authorization(value="jwt")})
	@ApiImplicitParams({ //
		@ApiImplicitParam(required = true, dataType="string", name="service", paramType = "path"), //
		@ApiImplicitParam(required = true, dataType="string", name="username", paramType = "path") //
	}) //
	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "Success", response=Boolean.class), //
			@ApiResponse(code = 400, message = "Invalid input data", response=ResponseError.class), //
			@ApiResponse(code = 401, message = "Unauthorized", response=ResponseError.class), //
	})
	@Override
	public Object handle(
			@ApiParam(hidden=true) Request request, 
			@ApiParam(hidden=true) Response response) throws Exception {
    	String username = request.params("username");
    	String service = request.params("service");
    	
    	if (username == null || username.isEmpty() || service == null || service.isEmpty()) {
    		response.status(400);
    		return new ResponseError("Invalid parameters");
    	}
    	
    	logger.debug("Request to lookup up user " + username + " in " + service);

    	if (backend.isUserValid(username, service))
    		return Boolean.valueOf(true);
    	else
    		return Boolean.valueOf(false);
	}

}
