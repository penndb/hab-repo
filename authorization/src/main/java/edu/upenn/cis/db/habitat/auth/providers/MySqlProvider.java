/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.auth.providers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import edu.upenn.cis.db.habitat.Config;

public class MySqlProvider implements RdbmsProvider {
	String url = "jdbc:mysql://" + Config.getRdbmsHost() + "/habitat_security";
	Connection conn;
	final static Logger logger = LogManager.getLogger(MySqlProvider.class);


	@Override
	public DSLContext get() {
		try {
			conn = DriverManager.getConnection(url, 
					Config.getPostgresUser(), 
					Config.getRdbmsPassword());
			
			return DSL.using(conn, SQLDialect.MYSQL);
		} catch (SQLException e) {
			logger.error("SQL Error in connecting to DBMS backend: " + e.getMessage());
			return null;
		}
	}

}
