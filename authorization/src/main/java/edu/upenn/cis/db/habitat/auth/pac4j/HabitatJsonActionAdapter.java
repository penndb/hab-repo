/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.auth.pac4j;

import static edu.upenn.cis.db.habitat.core.webservice.JsonUtil.toJson;
import static spark.Spark.halt;

import javax.ws.rs.core.MediaType;

import org.pac4j.core.context.HttpConstants;
import org.pac4j.sparkjava.DefaultHttpActionAdapter;
import org.pac4j.sparkjava.SparkWebContext;

import edu.upenn.cis.db.habitat.core.webservice.ResponseError;

public class HabitatJsonActionAdapter extends DefaultHttpActionAdapter {

	@Override
	public Object adapt(int code, SparkWebContext context) {
		if (code == HttpConstants.UNAUTHORIZED
				|| code == HttpConstants.FORBIDDEN) {
			context.setResponseContentType(MediaType.APPLICATION_JSON);
			final ResponseError error = new ResponseError(
					"Sorry, You Aren't Authorized");
			halt(code, toJson(error));
		} else {
			return super.adapt(code, context);
		}
		return null;
	}
}
