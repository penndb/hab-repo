/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.auth.backends.plugins;

import org.jooq.DSLContext;

import com.google.inject.AbstractModule;

import edu.upenn.cis.db.habitat.auth.providers.TemporaryDbProvider;
import edu.upenn.cis.db.habitat.auth.data.tables.Credentials;
import edu.upenn.cis.db.habitat.auth.data.tables.GroupMembers;
import edu.upenn.cis.db.habitat.auth.data.tables.GroupPermissions;
import edu.upenn.cis.db.habitat.auth.data.tables.Groups;
import edu.upenn.cis.db.habitat.auth.data.tables.Organizations;
import edu.upenn.cis.db.habitat.auth.data.tables.PermissionEntails;
import edu.upenn.cis.db.habitat.auth.data.tables.Permissions;
import edu.upenn.cis.db.habitat.auth.data.tables.UserPermissions;
import edu.upenn.cis.db.habitat.auth.data.tables.Users;

public class TemporaryDbModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(DSLContext.class).toProvider(TemporaryDbProvider.class);
	}

	public static void emptyDatabase(DSLContext dslContext) {
		// Using delete instead of truncate, because you cannot truncate a H2
		// table if it has a foreign key:
		// http://www.h2database.com/html/grammar.html#truncate_table
		dslContext.delete(PermissionEntails.PERMISSION_ENTAILS).execute();
		dslContext.delete(UserPermissions.USER_PERMISSIONS).execute();
		dslContext.delete(Credentials.CREDENTIALS).execute();
		dslContext.delete(GroupMembers.GROUP_MEMBERS).execute();
		dslContext.delete(GroupPermissions.GROUP_PERMISSIONS).execute();
		dslContext.delete(Organizations.ORGANIZATIONS).execute();
		dslContext.delete(Users.USERS).execute();
		dslContext.delete(Permissions.PERMISSIONS).execute();
		dslContext.delete(Groups.GROUPS).execute();
	}
}
