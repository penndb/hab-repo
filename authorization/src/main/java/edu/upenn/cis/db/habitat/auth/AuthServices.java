/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.auth;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pac4j.core.profile.CommonProfile;

import spark.Request;
import spark.Response;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.inject.Inject;

import edu.upenn.cis.db.habitat.core.api.PermissionApi;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.core.webservice.RestPlugin;
import edu.upenn.cis.db.habitat.core.webservice.RouteSpecifier;
import edu.upenn.cis.db.habitat.secure.ProtectedAuthBackend;
import edu.upenn.cis.db.habitat.util.SparkUserProfiles;
import edu.upenn.cis.db.habitat.webservice.routes.AddCredential;
import edu.upenn.cis.db.habitat.webservice.routes.AddGroup;
import edu.upenn.cis.db.habitat.webservice.routes.AddOrganization;
import edu.upenn.cis.db.habitat.webservice.routes.AddSubgroupToGroup;
import edu.upenn.cis.db.habitat.webservice.routes.AddUserToGroup;
import edu.upenn.cis.db.habitat.webservice.routes.AddUserToOrganization;
import edu.upenn.cis.db.habitat.webservice.routes.CreateJsonWebToken;
import edu.upenn.cis.db.habitat.webservice.routes.CreateNewUser;
import edu.upenn.cis.db.habitat.webservice.routes.GetGroupFromId;
import edu.upenn.cis.db.habitat.webservice.routes.GetGroupId;
import edu.upenn.cis.db.habitat.webservice.routes.GetGroupIdsForUser;
import edu.upenn.cis.db.habitat.webservice.routes.GetGroupsForUser;
import edu.upenn.cis.db.habitat.webservice.routes.GetOrganizationFromId;
import edu.upenn.cis.db.habitat.webservice.routes.GetOrganizationId;
import edu.upenn.cis.db.habitat.webservice.routes.GetParentGroupIds;
import edu.upenn.cis.db.habitat.webservice.routes.GetParentGroups;
import edu.upenn.cis.db.habitat.webservice.routes.GetUserInfo;
import edu.upenn.cis.db.habitat.webservice.routes.IsRegistered;
import edu.upenn.cis.db.habitat.webservice.routes.IsValidCredential;
import edu.upenn.cis.db.habitat.webservice.routes.UpdateUser;

/**
 * Main API for user-oriented services: organizations, groups, users, permissions.
 * Also has support for federated credentials and session tokens.
 * 
 * @author zives
 *
 */
public class AuthServices implements RestPlugin {
	public static final String PLUGIN_NAME = "authentication";
	final static Logger logger = LogManager.getLogger(AuthServices.class);
	
	final private ProtectedAuthBackend backend;
	final private CreateJsonWebToken createTokenRoute;
	final private CreateNewUser createNewUser;
	final private GetUserInfo getUserInfo;
	final private AddCredential addCredential;
	final private UpdateUser updateUser;
	final private IsRegistered isRegistered;
	final private IsValidCredential isValidCredential;
	final private AddGroup addGroup;
	final private AddOrganization addOrganization;
	final private AddUserToGroup addUserToGroup;
	final private AddUserToOrganization addUserToOrganization;
	final private AddSubgroupToGroup addSubgroupToGroup;
	final private GetGroupFromId getGroupFromId;
	final private GetOrganizationFromId getOrganizationFromId;
	final private GetGroupId getGroupId;
	final private GetOrganizationId getOrganizationId;
	final private GetGroupsForUser getGroupsForUser;
	final private GetGroupIdsForUser getGroupIdsForUser;
	final private GetParentGroups getParentGroups;
	final private GetParentGroupIds getParentGroupIds;
	
	@Inject public AuthServices(ProtectedAuthBackend backend, PermissionApi perms) {
		this.backend = (ProtectedAuthBackend)backend;
		
		createTokenRoute = new CreateJsonWebToken(backend, this);
		createNewUser = new CreateNewUser(backend, perms, this);
		getUserInfo = new GetUserInfo(this.backend, this);
		addCredential = new AddCredential(this.backend, this);
		updateUser = new UpdateUser(backend, this);
		isRegistered = new IsRegistered(backend, this);
		isValidCredential = new IsValidCredential(backend, this);
		addGroup = new AddGroup(backend, this);
		addOrganization = new AddOrganization(backend, this);
		addUserToGroup = new AddUserToGroup(this.backend, this);
		addSubgroupToGroup = new AddSubgroupToGroup(backend, this);
		addUserToOrganization = new AddUserToOrganization(backend, this);
		getGroupFromId = new GetGroupFromId(backend, this);
		getOrganizationFromId = new GetOrganizationFromId(backend, this);
		getGroupId = new GetGroupId(backend, this);
		getOrganizationId = new GetOrganizationId(backend, this);
		getGroupsForUser = new GetGroupsForUser(backend, this);
		getGroupIdsForUser = new GetGroupIdsForUser(backend, this);
		getParentGroups = new GetParentGroups(backend, this);
		getParentGroupIds = new GetParentGroupIds(backend, this);
	}

	public boolean addUser(Map<String,Object> request, String service, String token) {
		try {
			if (!backend.addUserAndOrganization((String)request.get("username"), 
					(String)request.get("firstname"), 
					(String)request.get("lastname"), 
					(String)request.get("street1"), 
					(String)request.get("street2"), 
					(String)request.get("city"), 
					(String)request.get("state"), 
					(String)request.get("zip"), 
					(String)request.get("country"), 
					(String)request.get("phone"), 
					(String)request.get("email"), 
					(String)request.get("title"), 
					(String)request.get("organization"),
					"", ""))
					return false;

			if (service != null && token != null)
				backend.addCredential((String)request.get("username"), service, 
					"http", token);
		} catch (HabitatServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return true;
		}
		return true;
	}
	
	public Map<String, Object> getProfileMap(Request request, Response response) {
		List<CommonProfile> profiles = SparkUserProfiles.getProfiles(request, response);

		final Map<String,Object> map = new HashMap<>();
		
		for (CommonProfile prof: profiles) {
			map.put("username", prof.getDisplayName());
			map.put("firstname", prof.getFirstName());
			map.put("lastname", prof.getFamilyName());
			map.put("email", prof.getEmail());
			map.put("token", prof.getAttribute("access_token"));
			map.putAll(prof.getAttributes());
		}
		map.put("profiles", profiles);
		
		return map;
	}
	
	@Override
	public Multimap<String, RouteSpecifier> getSpecifiers() {
		Multimap<String, RouteSpecifier> ret = HashMultimap.create();

		///// Request a session token
		
		ret.put("/tokens/:username", new RouteSpecifier(
				RouteSpecifier.RequestType.POST,
				false,
				createTokenRoute
				));

		//// User creation / credentials / info
		ret.put("/credentials/:username", new RouteSpecifier(
				RouteSpecifier.RequestType.POST,
				false,
				createNewUser
				));
		
		ret.put("/services/:service/user/:username", new RouteSpecifier(
				RouteSpecifier.RequestType.POST,
				true,
				addCredential
				));

		ret.put("/services/:service/user/:username", new RouteSpecifier(
				RouteSpecifier.RequestType.GET,
				true,
				isRegistered
				));
		ret.put("/services/local/user/:username/credential", new RouteSpecifier(
				RouteSpecifier.RequestType.POST,
				true,
				isValidCredential
				));

		ret.put("/credentials/:username/info", new RouteSpecifier(
				RouteSpecifier.RequestType.GET,
				true,
				getUserInfo
				));
		ret.put("/credentials/:username/groups", new RouteSpecifier(
				RouteSpecifier.RequestType.GET,
				true,
				getGroupsForUser
				));
		ret.put("/credentials/:username/groups/id", new RouteSpecifier(
				RouteSpecifier.RequestType.GET,
				true,
				getGroupIdsForUser
				));

		ret.put("/credentials/:username", new RouteSpecifier(
				RouteSpecifier.RequestType.PUT,
				true,
				updateUser
				));
		
		//// Groups
		ret.put("/groups/:groups/user/:user", new RouteSpecifier(
				RouteSpecifier.RequestType.POST,
				true,
				addUserToGroup
				));
		ret.put("/groups/:groups/group/:subgroup", new RouteSpecifier(
				RouteSpecifier.RequestType.POST,
				true,
				addSubgroupToGroup
				));
		ret.put("/groups/id/:groupId", new RouteSpecifier(
				RouteSpecifier.RequestType.GET,
				true,
				getGroupFromId
				));
		ret.put("/groups/name/:groupName", new RouteSpecifier(
				RouteSpecifier.RequestType.GET,
				true,
				getGroupId
				));
		ret.put("/groups/name/:groupName/parents/name", new RouteSpecifier(
				RouteSpecifier.RequestType.GET,
				true,
				getParentGroups
				));
		ret.put("/groups/id/:groupId/parents/id", new RouteSpecifier(
				RouteSpecifier.RequestType.GET,
				true,
				getParentGroupIds
				));
		
		
		//// Organizations
		ret.put("/organizations/:organization/group/:group", new RouteSpecifier(
				RouteSpecifier.RequestType.POST,
				true,
				addGroup
				));

		ret.put("/organizations/:organization/user/:user", new RouteSpecifier(
				RouteSpecifier.RequestType.POST,
				true,
				addUserToOrganization
				));

		ret.put("/organizations/id/:orgId", new RouteSpecifier(
				RouteSpecifier.RequestType.GET,
				true,
				getOrganizationFromId
				));

		ret.put("/organizations/name/:orgName", new RouteSpecifier(
				RouteSpecifier.RequestType.GET,
				true,
				getOrganizationId
				));

		ret.put("/organizations/:organization", new RouteSpecifier(
				RouteSpecifier.RequestType.POST,
				true,
				addOrganization
				));

		return ret;
	}

}
