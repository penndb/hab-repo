/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.util;

import java.util.List;

import org.apache.shiro.authc.credential.DefaultPasswordService;
import org.apache.shiro.crypto.hash.DefaultHashService;
import org.apache.shiro.util.SimpleByteSource;
import org.pac4j.core.credentials.password.PasswordEncoder;
import org.pac4j.core.credentials.password.ShiroPasswordEncoder;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.sparkjava.SparkWebContext;

import edu.upenn.cis.db.habitat.Config;
import spark.Request;
import spark.Response;

public class SparkUserProfiles {
	static PasswordEncoder encoder = null;
	
	public static PasswordEncoder getEncoder() {
		if (encoder == null) {
			DefaultHashService hash = new DefaultHashService();
			hash.setPrivateSalt(new SimpleByteSource(Config.getSalt()));
			DefaultPasswordService pwdService = new DefaultPasswordService();
			pwdService.setHashService(hash);
			encoder = new ShiroPasswordEncoder(pwdService);
		}
		return encoder;
	}
	
	public static List<CommonProfile> getProfiles(final Request request, final Response response) {
		final SparkWebContext context = new SparkWebContext(request, response);
		final ProfileManager<CommonProfile> manager = new ProfileManager<>(context);
		return manager.getAll(true);
	}	

	public static String hash(String username, String token) {
		return getEncoder().encode(token);
	}
	
	public static boolean isMatch(String hash, String username, String token) {
		return getEncoder().matches(token, hash);
	}

}
