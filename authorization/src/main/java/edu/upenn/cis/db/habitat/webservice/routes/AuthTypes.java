/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

public class AuthTypes {
	public static class WebToken {
		public String token;
	}
	
	public static class UserCredentials {
//		public String service;
//		public String protocol;
		public String password;
	}
	
	public static class GroupDetails {
		public String parentGroup;
	}
	
	public static class OrgDetails {
		String street1;
		String street2;
		String city;
		String state;
		String zip;
		String country;
		String phone;
		String web;
	}

	
	public static class UserInfo {
		public String username;
		public String email;
		public String password;
		public String firstname;
		public String lastname;
		public String street1;
		public String street2;
		public String city;
		public String state;
		public String zip;
		public String country;
		public String phone;
		public String title;
		public String organization;
	}
}
