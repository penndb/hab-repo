/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.auth.pac4j;

import java.util.Map;

import org.pac4j.core.authorization.generator.AuthorizationGenerator;
import org.pac4j.core.context.WebContext;
import org.pac4j.core.profile.CommonProfile;

import com.google.inject.Inject;

import edu.upenn.cis.db.habitat.core.api.UserApi;

public class HabitatAuthorizationGenerator implements AuthorizationGenerator<CommonProfile> {
	
	final UserApi userApi;
	
	@Inject
	public HabitatAuthorizationGenerator(UserApi api) {
		this.userApi = api;
	}

	@Override
	public CommonProfile generate(WebContext context, CommonProfile profile) {
		
		if (profile != null && !profile.getUsername().isEmpty()) {
			String username = profile.getUsername();
			
			try {
	    		Map<String,Object> userProperties = userApi.getUserInfo(username);
	    		for (String key: userProperties.keySet())
	    			profile.addAttribute(key, userProperties.get(key));
			} catch (Exception e) {
				
			}
    		
    		profile.setId(username);
		}
		return profile;
	}


}
