/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.jwt.config.signature.SecretSignatureConfiguration;
import org.pac4j.jwt.profile.JwtGenerator;
import org.pac4j.sparkjava.SparkWebContext;

import com.google.gson.Gson;

import edu.upenn.cis.db.habitat.Config;
import edu.upenn.cis.db.habitat.auth.AuthServices;
import edu.upenn.cis.db.habitat.core.api.UserApi;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;
import edu.upenn.cis.db.habitat.util.SparkUserProfiles;
import edu.upenn.cis.db.habitat.webservice.routes.AuthTypes.UserCredentials;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Session;

@Api(tags = AuthServices.PLUGIN_NAME)
@Path("/auth/tokens/{username}")
@Produces("application/json")
public class CreateJsonWebToken implements Route {
	final static Logger logger = LogManager.getLogger(CreateJsonWebToken.class);
	
	UserApi backend;
	
	public CreateJsonWebToken(UserApi backend, AuthServices authServices) {
		this.backend = backend;
	}

	@POST
	@ApiOperation(value = "Requests a new token", nickname="GetTokenRoute")
	@ApiImplicitParams({ //
		@ApiImplicitParam(required = true, dataType="string", name="username", paramType = "path"), //
		@ApiImplicitParam(required = true, dataTypeClass=AuthTypes.UserCredentials.class, 
			name="credentials", paramType = "body"), //
	}) //
	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "Success", response=AuthTypes.WebToken.class), //
			@ApiResponse(code = 400, message = "Invalid input data", response=ResponseError.class), //
			@ApiResponse(code = 401, message = "Unauthorized", response=ResponseError.class), //
	})
	@Override
	public Object handle(
			@ApiParam(hidden=true) Request request, 
			@ApiParam(hidden=true) Response response) throws Exception {
		final SparkWebContext context = new SparkWebContext(request, response);
		final ProfileManager<CommonProfile> manager = new ProfileManager<CommonProfile>(context);
		
		UserCredentials credentials;
		String token = null;
		try {
			credentials = new Gson().fromJson(request.body(), UserCredentials.class);
//			service = credentials.service;
//			if (service == null || service.isEmpty())
//				service = "local";
			token = credentials.password;
		} catch (Exception e) {
			response.status(400);
			return new ResponseError("Unable to parse message body for password");
		}

		String username = request.params("username");
		
		if (username == null || username.isEmpty()) {
			response.status(400);
			return new ResponseError("Unspecified username or service");
		}
		
		if (username == null || username.length() < 1 || token == null || token.length() < 1) {
			response.status(400);
			return new ResponseError("Unable to parse message body for password");
		}

		try {
			if (backend.isUserCredentialValid(
					username,
//					service, 
					token)) {
				JwtGenerator<CommonProfile> generator = 
						new JwtGenerator<CommonProfile>(new SecretSignatureConfiguration(
								Config.getSalt()));
				
				
	    		List<CommonProfile> profiles = SparkUserProfiles.getProfiles(request, response);
	    		
	    		if (profiles.isEmpty()) {
	    			profiles = new ArrayList<CommonProfile>();
	    			profiles.add(new CommonProfile());
	    		}
	    		// Load the user profile and all properties
	    		final CommonProfile profile = profiles.get(0);
	    		Map<String,Object> userProperties = backend.getUserInfo(username);
	    		for (String key: userProperties.keySet())
	    			profile.addAttribute(key, userProperties.get(key));
	    		
	    		profile.setId(username);
	    		
				token = generator.generate(profile);
				request.session(true);
	    		final Map<String,Object> map = new HashMap<>();
	    		map.put("token", token);
	    		Session session = request.session(true);
	    		map.put("sessionId", session.id());
	    		return map;
			}
		} catch (HabitatServiceException se) {
			logger.debug(se.getMessage());
		}
		response.status(401);
		return new ResponseError("No user with id '%s' found or unauthorized", request.params("username"));
	}

}
