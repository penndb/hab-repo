/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.auth.backends.plugins;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.inject.AbstractModule;
import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * Defines a scope for a Habitat request
 * 
 * @author John Frommeyer
 *
 */
public class HabitatRequestScopeModule extends AbstractModule {
	
	public static final String HABITAT_REQUEST_SCOPE_NAME = "habitatRequestScope";

	@FunctionalInterface
	public static interface HabitatRequest {
		
		void makeRequest() throws HabitatServiceException;
	}

	public static final class ScopedHabitatRequest implements HabitatRequest {
		private final SimpleScope scope;
		private final String username;
		private final HabitatRequest request;

		public ScopedHabitatRequest(
				SimpleScope scope,
				String username,
				HabitatRequest request) {
			this.scope = checkNotNull(scope);
			this.username = checkNotNull(username);
			this.request = checkNotNull(request);
		}

		@Override
		public void makeRequest() throws HabitatServiceException{
			try {
				scope.enter();
				scope.seed(Key.get(String.class, Names.named("username")), username);
				request.makeRequest();
			} finally {
				scope.exit();
			}
		}

	}
	
	@Provides
	@Named("username")
	@HabitatRequestScoped
	String provideUsername() {
		throw new IllegalStateException("username must be manually seeded");
	}

	public void configure() {
		SimpleScope habReqScope = new SimpleScope();

		// tell Guice about the scope
		bindScope(HabitatRequestScoped.class, habReqScope);

		// make our scope instance injectable
		bind(SimpleScope.class)
				.annotatedWith(Names.named(HABITAT_REQUEST_SCOPE_NAME))
				.toInstance(habReqScope);
	}
}
