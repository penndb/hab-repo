/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.permissions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.pac4j.core.profile.CommonProfile;

import spark.Request;
import spark.Response;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.inject.Inject;

import edu.upenn.cis.db.habitat.core.api.PermissionApi;
import edu.upenn.cis.db.habitat.core.api.StorageApi;
import edu.upenn.cis.db.habitat.core.api.UserApi;
import edu.upenn.cis.db.habitat.core.webservice.RestPlugin;
import edu.upenn.cis.db.habitat.core.webservice.RouteSpecifier;
import edu.upenn.cis.db.habitat.secure.ProtectedAuthBackend;
import edu.upenn.cis.db.habitat.util.SparkUserProfiles;
import edu.upenn.cis.db.habitat.webservice.routes.AddPermission;
import edu.upenn.cis.db.habitat.webservice.routes.GetGroupPermissionsOn;
import edu.upenn.cis.db.habitat.webservice.routes.GetObjectIds;
import edu.upenn.cis.db.habitat.webservice.routes.GetPermissionFromId;
import edu.upenn.cis.db.habitat.webservice.routes.GetPermissionId;
import edu.upenn.cis.db.habitat.webservice.routes.GetUserPermissionsOn;
import edu.upenn.cis.db.habitat.webservice.routes.GrantGroupPermissionOn;
import edu.upenn.cis.db.habitat.webservice.routes.GrantUserPermissionOn;
import edu.upenn.cis.db.habitat.webservice.routes.RevokeGroupPermissionOn;
import edu.upenn.cis.db.habitat.webservice.routes.RevokeUserPermissionOn;

public class PermissionServices implements RestPlugin {
	public static final String PLUGIN_NAME = "permission";
	final UserApi userAuth;
	final PermissionApi perms;
	final StorageApi<String,Object,?> storage;
	
	final AddPermission addPermission;
	final GetGroupPermissionsOn getGroupPermissionsOn;
	final GetUserPermissionsOn getUserPermissionsOn;
	
	final GrantUserPermissionOn grantUserPermissionOn;
	final RevokeUserPermissionOn revokeUserPermissionOn;
	final GrantGroupPermissionOn grantGroupPermissionOn;
	final RevokeGroupPermissionOn revokeGroupPermissionOn;
	
	final GetPermissionFromId getPermissionFromId;
	final GetPermissionId getPermissionId;
	
	private final GetObjectIds getObjectIds;

	@Inject
	public PermissionServices(ProtectedAuthBackend userAuthorization, PermissionApi perms, 
			StorageApi<String,Object,?> storage) {
		userAuth = userAuthorization;
		this.perms = perms;
		this.storage = storage; 
		
		this.addPermission = new AddPermission(userAuthorization, perms, this);
		this.getUserPermissionsOn = new GetUserPermissionsOn(userAuthorization, perms, this);
		this.getGroupPermissionsOn = new GetGroupPermissionsOn(userAuthorization, perms, this);

		this.grantUserPermissionOn = new GrantUserPermissionOn(userAuthorization, perms, this);
		this.revokeUserPermissionOn = new RevokeUserPermissionOn(userAuthorization, perms, this);
		this.grantGroupPermissionOn = new GrantGroupPermissionOn(userAuthorization, perms, this);
		this.revokeGroupPermissionOn = new RevokeGroupPermissionOn(userAuthorization, perms, this);

		
		getPermissionFromId = new GetPermissionFromId(userAuthorization, perms, this);
		getPermissionId = new GetPermissionId(userAuthorization, perms, this);
		
		getObjectIds = new GetObjectIds(userAuthorization, perms, this);
	}
	
	public Map<String, Object> getProfileMap(Request request, Response response) {
		List<CommonProfile> profiles = SparkUserProfiles.getProfiles(request, response);

		final Map<String,Object> map = new HashMap<>();
		
		for (CommonProfile prof: profiles) {
			map.put("username", prof.getDisplayName());
			map.put("firstname", prof.getFirstName());
			map.put("lastname", prof.getFamilyName());
			map.put("email", prof.getEmail());
			map.put("token", prof.getAttribute("access_token"));
			map.putAll(prof.getAttributes());
		}
		map.put("profiles", profiles);
		
		return map;
	}
	

	@Override
	public Multimap<String, RouteSpecifier> getSpecifiers() {
		Multimap<String, RouteSpecifier> ret = HashMultimap.create();
		
		/*
	public boolean hasPermission(String object, int permId, String username) throws HabitatServiceException;
	
	public boolean hasPermission(String object, String permission, String username) throws HabitatServiceException;

		 */
		
		ret.put("/objects/:object/user/:user/:permission", new RouteSpecifier(
				RouteSpecifier.RequestType.POST,
				false,
				grantUserPermissionOn
				));

		ret.put("/objects/:object/group/:group/:permission", new RouteSpecifier(
				RouteSpecifier.RequestType.POST,
				false,
				grantGroupPermissionOn
				));

		ret.put("/objects/:object/user/:user/:permission", new RouteSpecifier(
				RouteSpecifier.RequestType.DELETE,
				false,
				revokeUserPermissionOn
				));

		ret.put("/objects/:object/group/:group/:permission", new RouteSpecifier(
				RouteSpecifier.RequestType.DELETE,
				false,
				revokeGroupPermissionOn
				));

		ret.put("/id/:id/name", new RouteSpecifier(
				RouteSpecifier.RequestType.GET,
				false,
				getPermissionFromId
				));

		ret.put("/name/:name/id", new RouteSpecifier(
				RouteSpecifier.RequestType.GET,
				false,
				getPermissionId
				));

		ret.put("/objects/:object/user/:user", new RouteSpecifier(
				RouteSpecifier.RequestType.GET,
				false,
				getUserPermissionsOn
				));

		ret.put("/objects/:object/group/:group", new RouteSpecifier(
				RouteSpecifier.RequestType.GET,
				false,
				getGroupPermissionsOn
				));

		ret.put("/types/:permname", new RouteSpecifier(
				RouteSpecifier.RequestType.POST,
				false,
				addPermission
				));
		
		ret.put("/name/:permname/user/:username", new RouteSpecifier(
				RouteSpecifier.RequestType.GET, 
				true, 
				getObjectIds));
		
		return ret;
	}

}
