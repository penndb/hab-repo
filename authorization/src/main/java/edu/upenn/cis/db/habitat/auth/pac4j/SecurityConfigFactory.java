/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.auth.pac4j;

import java.util.concurrent.TimeUnit;

import org.pac4j.core.authorization.authorizer.RequireAnyRoleAuthorizer;
import org.pac4j.core.authorization.generator.AuthorizationGenerator;
import org.pac4j.core.client.Clients;
import org.pac4j.core.config.Config;
import org.pac4j.core.config.ConfigFactory;
import org.pac4j.core.context.WebContext;
import org.pac4j.core.credentials.TokenCredentials;
import org.pac4j.core.credentials.authenticator.Authenticator;
import org.pac4j.core.credentials.authenticator.LocalCachingAuthenticator;
import org.pac4j.core.http.HttpActionAdapter;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.store.Store;
import org.pac4j.http.client.direct.HeaderClient;
import org.pac4j.jwt.config.signature.SecretSignatureConfiguration;
import org.pac4j.jwt.credentials.authenticator.JwtAuthenticator;
import org.pac4j.oauth.client.GitHubClient;
import org.pac4j.oauth.client.Google2Client;
import org.pac4j.sparkjava.SparkWebContext;

import com.google.inject.Inject;

import edu.upenn.cis.db.habitat.core.api.UserApi;
import spark.TemplateEngine;

public class SecurityConfigFactory implements ConfigFactory {
    private final TemplateEngine templateEngine;
    private final UserApi userAuthentication;

    @Inject
    public SecurityConfigFactory(final TemplateEngine templateEngine,
    		AuthorizationGenerator<?> authGen, final UserApi userAuthentication) {
        this.templateEngine = templateEngine;
        this.userAuthentication = userAuthentication;
    }

    @Override
    public Config build(final Object... parameters) {
    	final GitHubClient githubClient = new GitHubClient("280ad9ca0a30206dafa0",
    			"06c545622146f1cb7b82f5b6ae8cc17705665498");
    	final Google2Client googleClient = new Google2Client("1041279770671-01icnticmcbh8nd11fvn7hieriio0lsc.apps.googleusercontent.com",
    			"gvDsvalehjv1GiMTlfgXQxJk");
        
//        final FormClient formClient = new FormClient("http://localhost:8080/loginForm", new SimpleTestUsernamePasswordAuthenticator());
//        final IndirectBasicAuthClient indirectBasicAuthClient = new IndirectBasicAuthClient(new SimpleTestUsernamePasswordAuthenticator());
//
//        final DirectBasicAuthClient directBasicAuthClient = new DirectBasicAuthClient(new SimpleTestUsernamePasswordAuthenticator());
//
    	final LocalCachingAuthenticator<TokenCredentials> auth = new LocalCachingAuthenticator<>(new JwtAuthenticator(
    			new SecretSignatureConfiguration(edu.upenn.cis.db.habitat.Config.getSalt())),
    			10000,
    			15,
    			TimeUnit.MINUTES);
    	final Authenticator<TokenCredentials> tokenAuthenticator = (TokenCredentials credentials, WebContext ctx) -> {
            
            auth.validate(credentials, ctx);
            final Store<TokenCredentials, CommonProfile> store = auth.getStore();
            CommonProfile profile = store.get(credentials);
            ctx.setRequestAttribute("username", 
            		profile.getId());
        };
        final HeaderClient headerClient = new HeaderClient("api_key", tokenAuthenticator);
        // REST authent with JWT for a token passed in the url as the token parameter
//        HeaderClient parameterClient = new HeaderClient("api_key", 
//        		new JwtAuthenticator(new SecretSignatureConfiguration(edu.upenn.cis.db.habitat.Config.getSalt())));
        final String callbackUrl = edu.upenn.cis.db.habitat.Config.getServerUrl() + "/callback";
        final Clients clients = new Clients(callbackUrl, 
        		googleClient, githubClient, headerClient);//,
//                formClient, indirectBasicAuthClient, directBasicAuthClient, 
//                headerClient);
        
        clients.setDefaultClient(googleClient);

        final Config config = new Config(clients);
        config.addAuthorizer("admin", new RequireAnyRoleAuthorizer("ROLE_ADMIN"));
//        config.addAuthorizer("auth", new IsAuthenticatedAuthorizer());
//        config.addAuthorizer("custom", new HabitatAuthorizer());
//        config.addMatcher("excludedPath", new PathMatcher().excludePath("/user/signup"));
        final HttpActionAdapter<Object, SparkWebContext> httpActionAdapter = newHttpActionAdapter(parameters);
        config.setHttpActionAdapter(httpActionAdapter);
        return config;
    }
    
	private HttpActionAdapter<Object, SparkWebContext> newHttpActionAdapter(
			Object... parameters) {
		for (final Object parameter : parameters) {
			if (parameter instanceof HttpActionAdapter) {
				@SuppressWarnings("unchecked")
				final HttpActionAdapter<Object, SparkWebContext> adapter = (HttpActionAdapter<Object, SparkWebContext>) parameter;
				return adapter;
			}
		}
		return new HabitatActionAdapter(templateEngine);
	}
}
