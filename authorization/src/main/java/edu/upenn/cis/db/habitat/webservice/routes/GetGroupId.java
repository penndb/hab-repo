/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.db.habitat.auth.AuthServices;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;
import edu.upenn.cis.db.habitat.secure.ProtectedAuthBackend;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import spark.Request;
import spark.Response;
import spark.Route;

@Api(tags = AuthServices.PLUGIN_NAME)
@Path("/auth/groups/name/{groupName}")
@Produces("application/json")
public class GetGroupId implements Route {
	final static Logger logger = LogManager.getLogger(GetGroupId.class);
	
	ProtectedAuthBackend backend;
	AuthServices caller;
	
	public GetGroupId(ProtectedAuthBackend backend, AuthServices services) {
		this.backend = backend;
		caller = services;
	}
	
	@GET
	@ApiOperation(value = "Gets a group's ID from its name", nickname="GetGroupId",
			authorizations = { @Authorization(value="jwt")})
	@ApiImplicitParams({ //
		@ApiImplicitParam(required = true, dataType="string", name="groupName", paramType = "path"), //
	}) //
	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "Success", response=Integer.class), //
			@ApiResponse(code = 400, message = "Invalid input data", response=ResponseError.class), //
			@ApiResponse(code = 401, message = "Unauthorized", response=ResponseError.class), //
	})
	@Override
	public Object handle(
			@ApiParam(hidden=true) Request request, 
			@ApiParam(hidden=true) Response response) throws Exception {
    	String groupName = request.params("groupName");
    	
    	if (groupName == null) {
    		response.status(400);
    		return new ResponseError("Illegal organization specified");
    	}

    	logger.debug("Request to get group " + groupName);
    	logger.debug(request.body());
		if (groupName == null || groupName.isEmpty()) {
			response.status(400);
			return new ResponseError("Invalid group");
		}
		
    	Map<String,Object> info = caller.getProfileMap(request, response);
    	if (info.containsKey("username")) {
    		if (!(backend.isAdminForGroup(groupName) ||
    				backend.isUserInGroup((String)info.get("username"), 
    						groupName))) {
        		response.status(401);
        		return new ResponseError("Can only get details for groups you administer or are in");
    		}
    	} else {
    		response.status(401);
    		return new ResponseError("Cannot find credentials -- are you logged in?");
    	}
    	
    	Integer id = backend.getGroupIdFrom(groupName); 
    	if (id != null && id > 0) {
    		response.body(String.valueOf(id));
    		return true;
    	} else {
    		response.status(401);
    		return new ResponseError("Unable to look up group " + groupName);
    	}
	}

}
