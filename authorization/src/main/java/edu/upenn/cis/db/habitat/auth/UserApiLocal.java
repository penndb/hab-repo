/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.auth;

import edu.upenn.cis.db.habitat.core.api.UserApi;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;


public interface UserApiLocal extends UserApi {
	public void initialize(String namespace) throws HabitatServiceException;

	public void createTables() throws HabitatServiceException;
	
	/**
	 * Register this user
	 * @param user
	 * @return 
	 */
	public boolean addUser(
			String username, 
			String first, 
			String last, 
			String street1,
			String street2, 
			String city, 
			String state, 
			String zip, 
			String country, 
			String phone, 
			String primaryEmail, 
			String title, 
			String organization)
					 throws HabitatServiceException;
	
	boolean addUserAndOrganization(
			String username, 
			String firstname, 
			String lastname, 
			String street1, 
			String street2,
			String city, 
			String state, 
			String zip, 
			String country, 
			String phone, 
			String userEmail, 
			String title,
			String organization, 
			String orgPhone, 
			String orgWeb) throws HabitatServiceException;
	

}
