/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;

import edu.upenn.cis.db.habitat.auth.AuthServices;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;
import edu.upenn.cis.db.habitat.secure.ProtectedAuthBackend;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import spark.Request;
import spark.Response;
import spark.Route;

@Api(tags = AuthServices.PLUGIN_NAME)
@Path("/auth/groups/id/{groupId}/parents/id")
@Produces("application/json")
public class GetParentGroupIds implements Route {
	final static Logger logger = LogManager.getLogger(GetParentGroupIds.class);
	
	ProtectedAuthBackend backend;
	AuthServices caller;
	
	public GetParentGroupIds(ProtectedAuthBackend backend, AuthServices services) {
		this.backend = backend;
		caller = services;
	}
	
	@GET
	@ApiOperation(value = "Gets the IDs of parent groups", nickname="GetParentGroupIds",
			authorizations = { @Authorization(value="jwt")})
	@ApiImplicitParams({ //
		@ApiImplicitParam(required = true, dataType="integer", name="groupId", paramType = "path"), //
	}) //
	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "Success", response=HashSet.class), //
			@ApiResponse(code = 400, message = "Invalid input data", response=ResponseError.class), //
			@ApiResponse(code = 401, message = "Unauthorized", response=ResponseError.class), //
	})
	@Override
	public Object handle(
			@ApiParam(hidden=true) Request request, 
			@ApiParam(hidden=true) Response response) throws Exception {
		Integer groupId = Integer.valueOf(request.params("groupId"));
    	
    	if (groupId == null || groupId < 0) {
    		response.status(400);
    		return new ResponseError("Illegal organization specified");
    	}

    	logger.debug("Request to get parent groups for " + groupId);
		
    	Map<String,Object> info = caller.getProfileMap(request, response);
    	if (info.containsKey("username")) {
    		String requestor = (String)info.get("username");
    		if (!(backend.isAdminForGroup(requestor, groupId) ||
    				backend.isUserInGroup(requestor, groupId))) {
        		response.status(401);
        		return new ResponseError("Can only get details for groups you administer or are in");
    		}
    	} else {
    		response.status(401);
    		return new ResponseError("Cannot find credentials -- are you logged in?");
    	}

    	Set<Integer> ret = backend.getParentGroupIds(groupId);

    	if (ret != null) {
    		response.body(new Gson().toJson(ret));
    		return true;
    	} else {
    		response.status(401);
    		return new ResponseError("Unable to look up parent groups for " + groupId);
    	}
	}

}
