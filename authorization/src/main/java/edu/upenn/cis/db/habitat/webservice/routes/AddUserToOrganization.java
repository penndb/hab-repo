/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import java.util.Map;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.db.habitat.auth.AuthServices;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;
import edu.upenn.cis.db.habitat.secure.ProtectedAuthBackend;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import spark.Request;
import spark.Response;
import spark.Route;

@Api(tags = AuthServices.PLUGIN_NAME)
@Path("/auth/organizations/{orgId}/user/{username}")
@Produces("application/json")
public class AddUserToOrganization implements Route {
	final static Logger logger = LogManager.getLogger(AddUserToOrganization.class);
	
	ProtectedAuthBackend backend;
	AuthServices caller;
	
	public AddUserToOrganization(ProtectedAuthBackend backend, AuthServices services) {
		this.backend = backend;
		caller = services;
	}
	
	@POST
	@ApiOperation(value = "Adds a user to an organization", nickname="AddUserToOrganization",
			authorizations = { @Authorization(value="jwt")})
	@ApiImplicitParams({ //
		@ApiImplicitParam(required = true, dataType="integer", name="orgId", paramType = "path"), //
		@ApiImplicitParam(required = true, dataType="string", name="username", paramType = "path"), //
	}) //
	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "Success", response=Integer.class), //
			@ApiResponse(code = 400, message = "Invalid input data", response=ResponseError.class), //
			@ApiResponse(code = 401, message = "Unauthorized", response=ResponseError.class), //
	})
	@Override
	public Object handle(
			@ApiParam(hidden=true) Request request, 
			@ApiParam(hidden=true) Response response) throws Exception {
    	Integer orgId = Integer.valueOf(request.params("organization"));
    	String username = request.params("user");
    	
    	if (orgId == null) {
    		response.status(400);
    		return new ResponseError("Illegal organization ID provided");
    	}

    	logger.debug("Request to add user " + username + " to organization " + orgId);
		
		if (username == null || username.isEmpty()) {
			response.status(400);
			return new ResponseError("Invalid user name");
		}
		
    	Map<String,Object> info = caller.getProfileMap(request, response);
    	if (!info.containsKey("username")) {
    		response.status(401);
    		return new ResponseError("Invalid connection/session -- are you logged in?");
    	}
    	String requestor = (String)info.get("username");
		if (!backend.isUserValid(requestor, "local") ||
				!backend.isAdminForOrganization(orgId)) {
    		response.status(401);
    		return new ResponseError("Can only add valid users to an organization you administer");
		}
		
    	if (backend.addUserToOrganization(username, orgId)) { 
    		return true;
    	} else {
    		response.status(401);
    		return new ResponseError("Unable to add user to organization " + orgId);
    	}
	}

}
