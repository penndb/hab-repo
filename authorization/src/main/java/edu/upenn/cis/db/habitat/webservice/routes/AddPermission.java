/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import java.util.Map;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.db.habitat.core.api.PermissionApi;
import edu.upenn.cis.db.habitat.core.api.UserApi;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;
import edu.upenn.cis.db.habitat.permissions.PermissionServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import spark.Request;
import spark.Response;
import spark.Route;

@Api(tags = PermissionServices.PLUGIN_NAME)
@Path("/perms/types/{permname}")
@Produces("application/json")
public class AddPermission implements Route {
	final static Logger logger = LogManager.getLogger(AddPermission.class);
	
	PermissionApi perms;
	UserApi users;
	PermissionServices caller;
	
	public AddPermission(UserApi users, PermissionApi perms, PermissionServices services) {
		this.perms = perms;
		this.users = users;
		caller = services;
	}
	
	@POST
	@ApiOperation(value = "Adds a new labeled permission type", nickname="AddPermission",
			authorizations = { @Authorization(value="jwt")})
	@ApiImplicitParams({ //
		@ApiImplicitParam(required = true, dataType="string", name="permname", paramType = "path") //
	}) //
	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "Success", response=Integer.class), //
			@ApiResponse(code = 400, message = "Invalid input data", response=ResponseError.class), //
			@ApiResponse(code = 401, message = "Unauthorized", response=ResponseError.class), //
	})
	@Override
	public Object handle(
			@ApiParam(hidden=true) Request request, 
			@ApiParam(hidden=true) Response response) throws Exception {
    	String permname = request.params("permname");
    	
    	logger.debug("Request to add permission called " + permname);
		
		if (permname == null || permname.isEmpty()) {
			response.status(400);
			return new ResponseError("Invalid permission name");
		}
		
		// TODO: revisit whether *just anyone* can create permissions?
    	Map<String,Object> info = caller.getProfileMap(request, response);
    	if (info.containsKey("username")) {
    		
        	String requestor = (String)info.get("username");
        	
        	if (!users.isUserValid(requestor, "local")) {
        		response.status(401);
        		return new ResponseError("Unauthorized to create permission " + permname);
        	}
    	}
    	
    	int id = perms.addPermission(permname); 
    	if (id >= 0) {
    		response.body(Integer.toString(id));
    		return true;
    	} else {
    		response.status(401);
    		return new ResponseError("Unable to create permission " + permname);
    	}
	}

}
