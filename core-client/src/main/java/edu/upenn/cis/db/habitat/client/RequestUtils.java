/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.client;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.GetRequest;
import com.mashape.unirest.request.HttpRequestWithBody;

import edu.upenn.cis.db.habitat.Config;

public class RequestUtils {
	public static GetRequest createAuthenticatedGet(String path, String token) {
		return Unirest.get(Config.getServerUrl() + path)
				.header("accept", "application/json")
				.header("Content-Type", "application/json")
				.header("api_key", token);
	}

	public static HttpRequestWithBody createAuthenticatedPost(String path, String token) {
		return Unirest.post(Config.getServerUrl() + path)
				.header("accept", "application/json")
				.header("Content-Type", "application/json")
				.header("api_key", token);
	}

	public static HttpRequestWithBody createAuthenticatedPut(String path, String token) {
		return Unirest.put(Config.getServerUrl() + path)
				.header("accept", "application/json")
				.header("Content-Type", "application/json")
				.header("api_key", token);
	}
}
