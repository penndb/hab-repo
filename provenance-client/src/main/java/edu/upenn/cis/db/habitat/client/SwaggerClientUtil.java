/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.client;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import edu.upenn.cis.db.habitat.core.webservice.HabitatSecurityException;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.webservice.swagger.ApiClient;
import edu.upenn.cis.db.habitat.webservice.swagger.ApiException;
import edu.upenn.cis.db.habitat.webservice.swagger.auth.ApiKeyAuth;

/**
 * @author John Frommeyer
 *
 */
public class SwaggerClientUtil {
	public static HabitatServiceException handleApiException(
			String message,
			ApiException e) {
		final Status status = Response.Status.fromStatusCode(e.getCode());
		final String extendedMessage = String.format(
				"%s: HTTP status: %s(%d), response body: [%s]",
				message,
				status,
				e.getCode(),
				e.getResponseBody());
		if (status != null && (status.equals(Status.FORBIDDEN)
				|| status.equals(Status.UNAUTHORIZED))) {
			return new HabitatSecurityException(extendedMessage);
		}
		return new HabitatServiceException(extendedMessage, e);
	}

	public static void authorizeSwaggerClient(
			String sessionToken,
			ApiClient swaggerApiClient) {
		final ApiKeyAuth apiKeyAuth = (ApiKeyAuth) swaggerApiClient
				.getAuthentication("jwt");
		apiKeyAuth.setApiKey(sessionToken);
	}

	private SwaggerClientUtil() {}
}
