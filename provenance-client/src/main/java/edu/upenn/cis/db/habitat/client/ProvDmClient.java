/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.client;

import static com.google.common.base.Preconditions.checkNotNull;
import static edu.upenn.cis.db.habitat.client.SwaggerClientUtil.authorizeSwaggerClient;
import static edu.upenn.cis.db.habitat.client.SwaggerClientUtil.handleApiException;
import static java.util.Collections.emptyList;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.common.annotations.VisibleForTesting;
import com.mashape.unirest.http.exceptions.UnirestException;

import edu.upenn.cis.db.habitat.core.api.ProvDmApi;
import edu.upenn.cis.db.habitat.core.api.ProvenanceGraphApi;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.model.ProvSpecifierModel;
import edu.upenn.cis.db.habitat.core.type.provdm.Attribute;
import edu.upenn.cis.db.habitat.core.type.provdm.NodeModel;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvDmRelation;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvQualifiedNameToken;
import edu.upenn.cis.db.habitat.core.type.provdm.QualifiedName;
import edu.upenn.cis.db.habitat.core.type.provdm.RelationModel;
import edu.upenn.cis.db.habitat.core.type.provdm.StringAttribute;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.webservice.swagger.ApiException;

/**
 * @author John Frommeyer
 *
 */
public class ProvDmClient implements ProvDmApi {

	private final UserClient userClient;
	private final edu.upenn.cis.db.habitat.webservice.swagger.client.ProvDmApi provDmSwagger;
	private final ProvenanceClient provenanceApi;

	public ProvDmClient(String username, String password)
			throws HabitatServiceException {
		try {
			checkNotNull(username);
			checkNotNull(password);
			provDmSwagger = new edu.upenn.cis.db.habitat.webservice.swagger.client.ProvDmApi();
			userClient = new UserClient(username, password);
			provenanceApi = new ProvenanceClient(userClient);
			authorizeClients();
		} catch (UnirestException e) {
			throw new HabitatServiceException("error creating UserClient", e);
		}
	}

	@VisibleForTesting
	public ProvDmClient(
			String username,
			String password,
			String host,
			int port)
			throws HabitatServiceException {
		this(username, password);
		final String basePath = "http://" + host + ":" + port;
		provenanceApi.getSwaggerClient().getApiClient().setBasePath(basePath);
		provDmSwagger.getApiClient().setBasePath(basePath);
	}

	private void authorizeClients() {
		final String sessionToken = userClient.getSessionToken();
		authorizeSwaggerClient(sessionToken, provDmSwagger.getApiClient());
	}

	public ProvenanceGraphApi getProvenanceClient() {
		return provenanceApi;
	}

	@Override
	public void createProvenanceGraph(String resourceName)
			throws HabitatServiceException {
		provenanceApi.createProvenanceGraph(resourceName);
	}

	@Override
	public void createOrResetProvenanceGraph(String resourceName) 
			throws HabitatServiceException {
		provenanceApi.createOrResetProvenanceGraph(resourceName);
	}

	@Override
	public void storeNode(String graph,
			QualifiedName provId,
			NodeModel nodeModel) throws HabitatServiceException {
		checkNotNull(graph);
		checkNotNull(provId);
		checkNotNull(nodeModel);
		try {
			provDmSwagger.storeNode(graph, provId.toString(), nodeModel);
		} catch (ApiException e) {
			throw handleApiException("error storing node", e);
		}
	}

	@Override
	public void entity(String graph,
			QualifiedName provId,
			List<Attribute> attributes,
			ProvSpecifierModel location)
			throws HabitatServiceException {
		final NodeModel nodeModel = NodeModel.newEntity(attributes, location);
		storeNode(graph, provId, nodeModel);
	}

	@Override
	public void entity(String graph,
			QualifiedName provId,
			ProvSpecifierModel location)
			throws HabitatServiceException {
		final NodeModel nodeModel = NodeModel.newEntity(location);
		storeNode(graph, provId, nodeModel);
	}

	@Override
	public void collection(String graph, QualifiedName provId, List<Attribute> attributes, ProvSpecifierModel location)
			throws HabitatServiceException {
		final NodeModel nodeModel = NodeModel.newCollection(attributes, location);
		storeNode(graph, provId, nodeModel);
	}

	@Override
	public void collection(String graph, QualifiedName provId, ProvSpecifierModel location)
			throws HabitatServiceException {
		final NodeModel nodeModel = NodeModel.newCollection(location);
		storeNode(graph, provId, nodeModel);
	}

	
	@Override
	public void activity(String graph,
			QualifiedName provId,
			List<Attribute> attributes, Date startTime, Date endTime,
			ProvSpecifierModel location) throws HabitatServiceException {
		final NodeModel nodeModel = NodeModel.newActivity(attributes,
				startTime, endTime, location);
		storeNode(graph, provId, nodeModel);
	}

	@Override
	public void activity(String graph,
			QualifiedName provId,
			Date startTime,
			Date endTime,
			ProvSpecifierModel location)
			throws HabitatServiceException {
		activity(graph, provId, emptyList(), startTime, endTime, location);
	}

	@Override
	public void agent(String graph,
			QualifiedName provId,
			List<Attribute> attributes, ProvSpecifierModel location)
			throws HabitatServiceException {
		final NodeModel nodeModel = NodeModel.newAgent(attributes, location);
		storeNode(graph, provId, nodeModel);
	}

	@Override
	public void agent(String graph,
			QualifiedName provId,
			ProvSpecifierModel location)
			throws HabitatServiceException {
		agent(graph, provId, emptyList(), location);
	}

	@Override
	public void storeRelation(
			String graph,
			RelationModel relationModel,
			String label)
			throws HabitatServiceException {
		checkNotNull(graph);
		checkNotNull(relationModel);
		checkNotNull(label);
		try {
			provDmSwagger.storeRelation(graph, relationModel, label);
		} catch (ApiException e) {
			throw new HabitatServiceException("cannot store relation", e);
		}
	}

	@Override
	public void wasGeneratedBy(String graph,
			QualifiedName entityId,
			QualifiedName activityId,
			QualifiedName relationId,
			Date generationTime,
			List<Attribute> attributes)
			throws HabitatServiceException {
		final RelationModel relation = RelationModel.newWasGeneratedBy(entityId,
				activityId, attributes,
				relationId, generationTime);
		storeRelation(graph, relation, ProvDmRelation.GENERATION.getProvNRelation());
	}

	@Override
	public void wasGeneratedBy(String graph,
			QualifiedName entityId,
			QualifiedName activityId,
			QualifiedName relationId,
			Date generationTime) throws HabitatServiceException {
		wasGeneratedBy(graph, entityId, activityId, relationId, generationTime,
				emptyList());
	}

	@Override
	public void used(String graph,
			QualifiedName activityId,
			QualifiedName entityId,
			QualifiedName relationId,
			Date usageTime,
			List<Attribute> attributes)
			throws HabitatServiceException {
		final RelationModel relationModel = RelationModel.newUsed(activityId, entityId,
				attributes, relationId, usageTime);
		storeRelation(graph, relationModel, ProvDmRelation.USAGE.getProvNRelation());
	}

	@Override
	public void used(String graph, QualifiedName activityId,
			QualifiedName entityId, QualifiedName relationId,
			Date usageTime)
			throws HabitatServiceException {
		used(graph, activityId, entityId, relationId, usageTime, emptyList());
	}

	@Override
	public void wasInformedBy(String graph,
			QualifiedName informedActivityId,
			QualifiedName informantActivityId,
			QualifiedName relationId,
			List<Attribute> attributes)
			throws HabitatServiceException {
		final RelationModel relationModel = RelationModel
				.newWasInformedBy(informedActivityId, informantActivityId,
						attributes, relationId);
		storeRelation(graph, relationModel, ProvDmRelation.COMMUNICATION.getProvNRelation());
	}

	@Override
	public void wasInformedBy(String graph,
			QualifiedName informedActivityId,
			QualifiedName informantActivityId,
			QualifiedName relationId)
			throws HabitatServiceException {
		wasInformedBy(graph, informedActivityId, informantActivityId,
				relationId, emptyList());
	}

	@Override
	public void wasStartedBy(String graph,
			QualifiedName startedActivityId,
			QualifiedName triggerEntityId,
			QualifiedName relationId,
			Date time,
			List<Attribute> attributes)
			throws HabitatServiceException {
		wasStartedBy(graph, startedActivityId, triggerEntityId, null,
				relationId, time, attributes);
	}

	@Override
	public void wasStartedBy(String graph,
			QualifiedName startedActivityId,
			QualifiedName triggerEntityId,
			QualifiedName relationId, Date time)
			throws HabitatServiceException {
		wasStartedBy(graph, startedActivityId, triggerEntityId, null,
				relationId, time, emptyList());
	}

	@Override
	public void wasStartedBy(String graph,
			QualifiedName startedActivityId,
			QualifiedName triggerEntityId,
			QualifiedName starterActivityId,
			QualifiedName relationId,
			Date time,
			List<Attribute> attributes)
			throws HabitatServiceException {
		final RelationModel relationModel = RelationModel.newWasStartedBy(
				startedActivityId, triggerEntityId, attributes,
				starterActivityId, relationId, time);
		storeRelation(graph, relationModel, ProvDmRelation.START.getProvNRelation());
	}

	@Override
	public void wasStartedBy(String graph,
			QualifiedName startedActivityId,
			QualifiedName triggerEntityId,
			QualifiedName starterActivityId,
			QualifiedName relationId, Date time)
			throws HabitatServiceException {
		wasStartedBy(graph, startedActivityId, triggerEntityId,
				starterActivityId, relationId, time, emptyList());
	}

	@Override
	public void wasEndedBy(String graph,
			QualifiedName endedActivityId,
			QualifiedName triggerEntityId,
			QualifiedName relationId,
			Date time,
			List<Attribute> attributes)
			throws HabitatServiceException {
		wasEndedBy(graph, endedActivityId, triggerEntityId, null, relationId,
				time, attributes);
	}

	@Override
	public void wasEndedBy(String graph,
			QualifiedName endedActivityId,
			QualifiedName triggerEntityId,
			QualifiedName relationId,
			Date time)
			throws HabitatServiceException {
		wasEndedBy(graph, endedActivityId, triggerEntityId, null, relationId,
				time, emptyList());
	}

	@Override
	public void wasEndedBy(String graph,
			QualifiedName endedActivityId,
			QualifiedName triggerEntityId,
			QualifiedName enderActivityId,
			QualifiedName relationId,
			Date time,
			List<Attribute> attributes)
			throws HabitatServiceException {
		final RelationModel relationModel = RelationModel.newWasEndedBy(endedActivityId,
				triggerEntityId, attributes, enderActivityId, relationId, time);
		storeRelation(graph, relationModel, ProvDmRelation.END.getProvNRelation());
	}

	@Override
	public void wasEndedBy(String graph,
			QualifiedName endedActivityId,
			QualifiedName triggerEntityId,
			QualifiedName enderActivityId,
			QualifiedName relationId,
			Date time)
			throws HabitatServiceException {
		wasEndedBy(graph, endedActivityId, triggerEntityId, enderActivityId,
				relationId, time, emptyList());
	}

	@Override
	public void wasInvalidatedBy(String graph,
			QualifiedName invalidatedEntityId,
			QualifiedName invalidatorActivityId,
			QualifiedName relationId,
			Date time, List<Attribute> attributes)
			throws HabitatServiceException {
		final RelationModel relationModel = RelationModel.newWasInvalidatedBy(
				invalidatedEntityId, invalidatorActivityId, attributes,
				relationId, time);
		storeRelation(graph, relationModel, ProvDmRelation.INVALIDATION.getProvNRelation());
	}

	@Override
	public void wasInvalidatedBy(String graph,
			QualifiedName invalidatedEntityId,
			QualifiedName invalidatorActivityId,
			QualifiedName relationId,
			Date time) throws HabitatServiceException {
		wasInvalidatedBy(graph, invalidatedEntityId, invalidatorActivityId,
				relationId, time, emptyList());
	}

	@Override
	public void wasDerivedFrom(String graph,
			QualifiedName generatedEntityId,
			QualifiedName usedEntityId,
			QualifiedName activityId,
			QualifiedName relationId,
			QualifiedName generationId,
			QualifiedName usageId,
			List<Attribute> attributes)
			throws HabitatServiceException {
		final RelationModel relationModel = RelationModel.newWasDerivedFrom(
				generatedEntityId,
				usedEntityId,
				attributes,
				activityId,
				relationId,
				generationId,
				usageId);
		storeRelation(graph, relationModel, ProvDmRelation.DERIVATION.getProvNRelation());
	}

	@Override
	public void wasDerivedFrom(String graph,
			QualifiedName generatedEntityId,
			QualifiedName usedEntityId, QualifiedName activityId,
			QualifiedName relationId, QualifiedName generationId,
			QualifiedName usageId) throws HabitatServiceException {
		wasDerivedFrom(graph, generatedEntityId, usedEntityId, activityId,
				relationId, generationId, usageId, emptyList());
	}

	@Override
	public void wasDerivedFrom(String graph,
			QualifiedName generatedEntityId,
			QualifiedName usedEntityId, QualifiedName relationId,
			QualifiedName generationId, QualifiedName usageId,
			List<Attribute> attributes)
			throws HabitatServiceException {
		wasDerivedFrom(graph, generatedEntityId, usedEntityId, null,
				relationId, generationId, usageId, attributes);
	}

	@Override
	public void wasDerivedFrom(String graph,
			QualifiedName generatedEntityId,
			QualifiedName usedEntityId, QualifiedName relationId,
			QualifiedName generationId, QualifiedName usageId)
			throws HabitatServiceException {
		wasDerivedFrom(graph, generatedEntityId, usedEntityId, null,
				relationId, generationId, usageId, emptyList());
	}

	@Override
	public void wasAttributedTo(String graph, QualifiedName entityId,
			QualifiedName agentId, QualifiedName relationId,
			List<Attribute> attributes)
			throws HabitatServiceException {
		final RelationModel relationModel = RelationModel.newWasAttributedTo(entityId,
				agentId, attributes, relationId);
		storeRelation(graph, relationModel, ProvDmRelation.ATTRIBUTION.getProvNRelation());
	}

	@Override
	public void wasAttributedTo(String graph, QualifiedName entityId,
			QualifiedName agentId, QualifiedName relationId)
			throws HabitatServiceException {
		wasAttributedTo(graph, entityId, agentId, relationId, emptyList());
	}

	@Override
	public void wasAssociatedWith(String graph,
			QualifiedName activityId,
			QualifiedName agentId,
			QualifiedName planId,
			QualifiedName relationId,
			List<Attribute> attributes)
			throws HabitatServiceException {
		final RelationModel relationModel = RelationModel.newWasAssociatedWith(
				activityId, agentId, attributes, planId, relationId);
		storeRelation(graph, relationModel, ProvDmRelation.ASSOCIATION.getProvNRelation());
	}

	@Override
	public void wasAssociatedWith(String graph,
			QualifiedName activityId,
			QualifiedName agentId,
			QualifiedName planId,
			QualifiedName relationId) throws HabitatServiceException {
		wasAssociatedWith(graph, activityId, agentId, planId, relationId,
				emptyList());
	}

	@Override
	public void wasAssociatedWith(String graph, QualifiedName activityId,
			QualifiedName agentId, QualifiedName relationId,
			List<Attribute> attributes)
			throws HabitatServiceException {
		wasAssociatedWith(graph, activityId, agentId, null, relationId,
				attributes);
	}

	@Override
	public void wasAssociatedWith(String graph, QualifiedName activityId,
			QualifiedName agentId, QualifiedName relationId)
			throws HabitatServiceException {
		wasAssociatedWith(graph, activityId, agentId, null, relationId,
				emptyList());
	}

	@Override
	public void actedOnBehalfOf(String graph,
			QualifiedName delegateAgentId,
			QualifiedName responsibleAgentId,
			QualifiedName activityId,
			QualifiedName relationId,
			List<Attribute> attributes)
			throws HabitatServiceException {
		final RelationModel relationModel = RelationModel.newActedOnBehalfOf(
				delegateAgentId, responsibleAgentId, attributes, activityId,
				relationId);
		storeRelation(graph, relationModel, ProvDmRelation.DELEGATION.getProvNRelation());
	}

	@Override
	public void actedOnBehalfOf(String graph,
			QualifiedName delegateAgentId,
			QualifiedName responsibleAgentId,
			QualifiedName activityId,
			QualifiedName relationId) throws HabitatServiceException {
		actedOnBehalfOf(graph, delegateAgentId, responsibleAgentId, activityId,
				relationId, emptyList());
	}

	@Override
	public void actedOnBehalfOf(String graph,
			QualifiedName delegateAgentId,
			QualifiedName responsibleAgentId,
			QualifiedName relationId,
			List<Attribute> attributes)
			throws HabitatServiceException {
		actedOnBehalfOf(graph, delegateAgentId, responsibleAgentId, null,
				relationId, attributes);
	}

	@Override
	public void actedOnBehalfOf(String graph,
			QualifiedName delegateAgentId,
			QualifiedName responsibleAgentId,
			QualifiedName relationId)
			throws HabitatServiceException {
		actedOnBehalfOf(graph, delegateAgentId, responsibleAgentId, null,
				relationId, emptyList());
	}

	@Override
	public void wasInfluencedBy(String graph, QualifiedName influenceeId,
			QualifiedName influencerId, QualifiedName relationId,
			List<Attribute> attributes)
			throws HabitatServiceException {
		final RelationModel relationModel = RelationModel.newWasInfluencedBy(
				influenceeId, influencerId, attributes, relationId);
		storeRelation(graph, relationModel, ProvDmRelation.INFLUENCE.getProvNRelation());
	}

	@Override
	public void wasInfluencedBy(String graph, QualifiedName influenceeId,
			QualifiedName influencerId, QualifiedName relationId)
			throws HabitatServiceException {
		wasInfluencedBy(graph, influenceeId, influencerId, relationId,
				emptyList());
	}

	@Override
	public void specializationOf(String graph,
			QualifiedName specificEntity,
			QualifiedName genericEntity) throws HabitatServiceException {
		final RelationModel relationModel = RelationModel.newSpecializationOf(
				specificEntity, genericEntity);
		storeRelation(graph, relationModel, ProvDmRelation.SPECIALIZATION.getProvNRelation());
	}

	@Override
	public void alternateOf(String graph,
			QualifiedName alternate1, QualifiedName alternate2)
			throws HabitatServiceException {
		final RelationModel relationModel = RelationModel.newAlternateOf(
				alternate1,
				alternate2);
		storeRelation(graph, relationModel, ProvDmRelation.ALTERNATE.getProvNRelation());
	}

	@Override
	public void hadMember(String graph, QualifiedName collection,
			QualifiedName entity)
			throws HabitatServiceException {
		final RelationModel relationModel = RelationModel
				.newHasMember(collection, entity);
		storeRelation(graph, relationModel, ProvDmRelation.MEMBERSHIP
				.getProvNRelation());
	}

	@Override
	public void annotateNode(String graph, QualifiedName sourceId, QualifiedName annotId, QualifiedName key,
			String value) throws HabitatServiceException {
		final NodeModel nodeModel = NodeModel.newAnnotation(key, value);
		storeNode(graph, annotId, nodeModel);
		
		final RelationModel relationModel = RelationModel
				.newWasAnnotatedBy(annotId, sourceId, key, value);
		storeRelation(graph, relationModel, ProvDmRelation.ANNOTATED.getProvNRelation());
	}

	@Override
	public List<Attribute> getNodeAnnotations(String graph, QualifiedName provId) throws HabitatServiceException {
		final ProvQualifiedNameToken provToken = new ProvQualifiedNameToken(
				provId);
		Iterable<ProvToken> connected = provenanceApi.getConnectedTo(graph, provToken, ProvDmRelation.ANNOTATED.getProvNRelation());
		List<Attribute> ret = new ArrayList<>();
		
		for (ProvToken ann: connected) {
			TupleWithSchema<String> str = provenanceApi.getProvenanceData(graph, ann);
			for (String key: str.getKeys())
				ret.add(new StringAttribute(QualifiedName.from(key), str.getValue(key).toString()));
		}
		return ret;
	}

	@Override
	public List<QualifiedName> getNodesFrom(String graph,
			QualifiedName provId, String edgeLabel) throws HabitatServiceException {
		
		final ProvQualifiedNameToken provToken = new ProvQualifiedNameToken(
				provId);
		Iterable<ProvToken> connected = provenanceApi.getConnectedFrom(graph, provToken, edgeLabel);
		List<QualifiedName> ret = new ArrayList<>();
		
		for (ProvToken ann: connected)
			ret.add(QualifiedName.from(ann.toString()));
		return ret;
	}

	@Override
	public List<QualifiedName> getNodesTo(String graph,
			QualifiedName provId, String edgeLabel) throws HabitatServiceException {
		
		final ProvQualifiedNameToken provToken = new ProvQualifiedNameToken(
				provId);
		Iterable<ProvToken> connected = provenanceApi.getConnectedTo(graph, provToken, edgeLabel);
		List<QualifiedName> ret = new ArrayList<>();
		
		for (ProvToken ann: connected)
			ret.add(QualifiedName.from(ann.toString()));
		return ret;
	}
}
