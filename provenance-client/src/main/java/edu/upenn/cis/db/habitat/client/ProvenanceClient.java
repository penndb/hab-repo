/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.client;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static edu.upenn.cis.db.habitat.client.SwaggerClientUtil.handleApiException;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.annotations.VisibleForTesting;
import com.mashape.unirest.http.exceptions.UnirestException;

import edu.upenn.cis.db.habitat.core.api.ProvenanceGraphApi;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.model.ProvEdgeSetModel;
import edu.upenn.cis.db.habitat.core.type.model.ProvNodeMapModel;
import edu.upenn.cis.db.habitat.core.type.model.ProvSpecifierModel;
import edu.upenn.cis.db.habitat.core.type.model.ProvTokenSetModel;
import edu.upenn.cis.db.habitat.core.type.model.StoreLinkModel;
import edu.upenn.cis.db.habitat.core.type.model.StoreNodeModel;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate;
import edu.upenn.cis.db.habitat.core.type.model.TupleWithSchemaModel;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.webservice.swagger.ApiException;

/**
 * @author John Frommeyer
 *
 */
public class ProvenanceClient implements ProvenanceGraphApi {

	private final UserClient userClient;
	private final edu.upenn.cis.db.habitat.webservice.swagger.client.ProvenanceApi swaggerClient;

	public ProvenanceClient(String username, String password)
			throws HabitatServiceException {
		try {
			checkNotNull(username);
			checkNotNull(password);
			swaggerClient = new edu.upenn.cis.db.habitat.webservice.swagger.client.ProvenanceApi();
			userClient = new UserClient(username, password);
			authorizeClient();
		} catch (UnirestException e) {
			throw new HabitatServiceException("error creating UserClient", e);
		}
	}

	public ProvenanceClient(
			UserClient userClient) {
		this.swaggerClient = new edu.upenn.cis.db.habitat.webservice.swagger.client.ProvenanceApi();
		this.userClient = checkNotNull(userClient);
		authorizeClient();
	}

	@VisibleForTesting
	public ProvenanceClient(
			String username,
			String password,
			String host,
			int port)
			throws HabitatServiceException {
		this(username, password);
		final String basePath = "http://" + host + ":" + port;
		swaggerClient.getApiClient().setBasePath(basePath);
	}

	private void authorizeClient() {
		final String sessionToken = userClient.getSessionToken();
		SwaggerClientUtil.authorizeSwaggerClient(sessionToken,
				swaggerClient.getApiClient());
	}

	public edu.upenn.cis.db.habitat.webservice.swagger.client.ProvenanceApi getSwaggerClient() {
		return swaggerClient;
	}

	@Override
	public void storeProvenanceNode(String resource, ProvToken token,
			ProvSpecifier location)
			throws HabitatServiceException {
		checkNotNull(location);
		final String stringToken = token.toString();
		final ProvSpecifierModel locationModel = ProvSpecifierModel
				.from(location);
		final StoreNodeModel body = new StoreNodeModel(locationModel, null);
		try {
			swaggerClient.storeProvenanceNode(resource, stringToken, body);
		} catch (ApiException e) {
			throw handleApiException("error storing node", e);
		}
	}

	@Override
	public void storeProvenanceNode(
			String resource,
			ProvToken token,
			TupleWithSchema<String> tuple,
			ProvSpecifier location)
			throws HabitatServiceException {
		checkNotNull(tuple);
		checkNotNull(location);
		final String stringToken = token.toString();
		final ProvSpecifierModel locationModel = ProvSpecifierModel
				.from(location);
		final TupleWithSchemaModel tupleModel = TupleWithSchemaModel
				.from(tuple);
		final StoreNodeModel body = new StoreNodeModel(locationModel,
				tupleModel);
		try {
			swaggerClient.storeProvenanceNode(resource, stringToken, body);
		} catch (ApiException e) {
			throw handleApiException("error storing node", e);
		}
	}

	@Override
	public ProvSpecifier getProvenanceLocation(String resource, ProvToken token)
			throws HabitatServiceException {
		final String stringToken = token.toString();
		try {
			final ProvSpecifierModel specifierModel = swaggerClient
					.getProvenanceLocation(resource, stringToken);
			return specifierModel.toProvSpecifier();
		} catch (ApiException e) {
			throw handleApiException(
					"error getting provenance location", e);
		}
	}

	@Override
	public TupleWithSchema<String> getProvenanceData(String resource,
			ProvToken token)
			throws HabitatServiceException {
		final String stringToken = token.toString();
		try {
			final TupleWithSchemaModel tupleModel = swaggerClient
					.getProvenanceData(resource, stringToken);
			return tupleModel == null ? null : tupleModel.toTupleWithSchema();
		} catch (ApiException e) {
			throw handleApiException("error getting provenance data",
					e);
		}
	}

	@Override
	public void storeProvenanceLink(
			String resource,
			ProvToken from,
			String label,
			ProvToken to)
			throws HabitatServiceException {
		checkNotNull(from);
		checkNotNull(to);
		checkNotNull(label);
		try {
			final StoreLinkModel body = StoreLinkModel.from(
					from,
					to,
					label);
			swaggerClient.storeProvenanceLink(resource, body);
		} catch (ApiException e) {
			throw handleApiException("error getting provenance data",
					e);
		}
	}

	@Override
	public void storeProvenanceLink(
			String resource,
			ProvToken from,
			String label,
			ProvToken to,
			TupleWithSchema<String> tuple) throws HabitatServiceException {
		checkNotNull(from);
		checkNotNull(to);
		checkNotNull(tuple);
		try {
			final StoreLinkModel body = StoreLinkModel.from(
					from,
					label,
					to,
					tuple);
			swaggerClient.storeProvenanceLink(resource, body);
		} catch (ApiException e) {
			throw handleApiException("error getting provenance data",
					e);
		}
	}

	@Override
	public void storeProvenanceLink(
			String resource,
			ProvToken fromLeft,
			ProvToken fromRight,
			String label,
			ProvToken to) throws HabitatServiceException {
		checkNotNull(fromLeft);
		checkNotNull(fromRight);
		checkNotNull(to);
		checkNotNull(label);
		try {
			final StoreLinkModel body = StoreLinkModel.from(
					fromLeft,
					fromRight,
					to,
					label);
			swaggerClient.storeProvenanceLink(resource, body);
		} catch (ApiException e) {
			throw handleApiException("error getting provenance data",
					e);
		}

	}

	@Override
	public void storeProvenanceLink(
			String resource,
			ProvToken fromLeft,
			ProvToken fromRight,
			String label,
			ProvToken to,
			TupleWithSchema<String> tuple)
			throws HabitatServiceException {
		checkNotNull(fromLeft);
		checkNotNull(fromRight);
		checkNotNull(to);
		checkNotNull(tuple);
		try {
			final StoreLinkModel body = StoreLinkModel.from(
					fromLeft,
					fromRight,
					label,
					to,
					tuple);
			swaggerClient.storeProvenanceLink(resource, body);
		} catch (ApiException e) {
			throw handleApiException("error getting provenance data",
					e);
		}
	}

	@Override
	public void storeProvenanceLink(
			String resource,
			List<ProvToken> inputProv,
			String label,
			ProvToken to) throws HabitatServiceException {
		checkNotNull(inputProv);
		checkArgument(!inputProv.isEmpty());
		checkNotNull(to);
		checkNotNull(label);
		try {
			final StoreLinkModel body = StoreLinkModel.from(
					inputProv,
					to,
					label);
			swaggerClient.storeProvenanceLink(resource, body);
		} catch (ApiException e) {
			throw handleApiException("error getting provenance data",
					e);
		}
	}

	@Override
	public void storeProvenanceLink(
			String resource,
			List<ProvToken> inputProv,
			String label,
			ProvToken to,
			TupleWithSchema<String> tuple) throws HabitatServiceException {
		checkNotNull(inputProv);
		checkArgument(!inputProv.isEmpty());
		checkNotNull(to);
		checkNotNull(tuple);
		try {
			final StoreLinkModel body = StoreLinkModel.from(
					inputProv,
					label,
					to,
					tuple);
			swaggerClient.storeProvenanceLink(resource, body);
		} catch (ApiException e) {
			throw handleApiException("error getting provenance data",
					e);
		}
	}

	@Override
	public Iterable<ProvToken> getConnectedTo(String resource, ProvToken to)
			throws HabitatServiceException {
		checkNotNull(to);
		try {
			final ProvTokenSetModel inNeighbors = swaggerClient
					.getConnectedTo(resource, to.toString(), null);
			return inNeighbors.toProvTokens();
		} catch (ApiException e) {
			throw handleApiException("error retrieving incoming neighbors", e);
		}
	}

	@Override
	public Iterable<ProvToken> getConnectedTo(String resource, ProvToken to,
			String label)
			throws HabitatServiceException {
		checkNotNull(to);
		try {
			final ProvTokenSetModel inNeighbors = swaggerClient
					.getConnectedTo(resource, to.toString(), label);
			return inNeighbors.toProvTokens();
		} catch (ApiException e) {
			throw handleApiException("error retrieving incoming neighbors", e);
		}
	}

	@Override
	public Iterable<TupleWithSchema<String>> getEdgesTo(String resource,
			ProvToken to)
			throws HabitatServiceException {
		throw new UnsupportedOperationException("not supported");
	}

	@Override
	public Set<ProvToken> getConnectedFrom(String resource, ProvToken from)
			throws HabitatServiceException {
		checkNotNull(from);
		try {
			final ProvTokenSetModel outNeighbors = swaggerClient
					.getConnectedFrom(resource, from.toString(), null);
			return outNeighbors.toProvTokens();
		} catch (ApiException e) {
			throw handleApiException("error retrieving outgoing neighbors", e);
		}
	}

	@Override
	public Set<ProvToken> getConnectedFrom(String resource, ProvToken from,
			String label)
			throws HabitatServiceException {
		checkNotNull(from);
		try {
			final ProvTokenSetModel outNeighbors = swaggerClient
					.getConnectedFrom(resource, from.toString(), label);
			return outNeighbors.toProvTokens();
		} catch (ApiException e) {
			throw handleApiException("error retrieving outgoing neighbors", e);
		}
	}

	@Override
	public Iterable<TupleWithSchema<String>> getEdgesFrom(String resource,
			ProvToken from)
			throws HabitatServiceException {
		throw new UnsupportedOperationException("not supported");
	}

	@Override
	public Map<String, StructuredData<String, Object>> getProvenanceNodes(
			String resource)
			throws HabitatServiceException {
		try {
			final ProvNodeMapModel nodes = swaggerClient
					.getProvenanceNodes(resource);
			return nodes.toProvNodeMap();
		} catch (ApiException e) {
			throw handleApiException("error retrieving outgoing edges", e);
		}
	}

	@Override
	public Map<String, List<TupleWithSchema<String>>> getEdgeMapFrom(
			String resource, ProvToken from) throws HabitatServiceException {
		checkNotNull(from);
		try {
			final ProvEdgeSetModel edges = swaggerClient
					.getEdgesFrom(resource, from.toString());
			return edges.toProvEdgeMap();
		} catch (ApiException e) {
			throw handleApiException("error retrieving outgoing edges", e);
		}
	}

	@Override
	public Map<String, List<TupleWithSchema<String>>> getEdgeMapTo(
			String resource, ProvToken to) throws HabitatServiceException {
		checkNotNull(to);
		try {
			final ProvEdgeSetModel edges = swaggerClient
					.getEdgesTo(resource, to.toString());
			return edges.toProvEdgeMap();
		} catch (ApiException e) {
			throw handleApiException("error retrieving outgoing edges", e);
		}
	}

	@Override
	public void createProvenanceGraph(String resourceName)
			throws HabitatServiceException {
		if (resourceName == null) {
			throw new HabitatServiceException("graph name cannot be null");
		}
		final String trimmedName = resourceName.trim();
		if (trimmedName.isEmpty()) {
			throw new HabitatServiceException("graph name cannot be empty");
		}
		try {
			swaggerClient.createProvenanceGraph(trimmedName);
		} catch (ApiException e) {
			throw handleApiException("error creating provenance graph",
					e);
		}

	}

	@Override
	public void createOrResetProvenanceGraph(String resourceName)
			throws HabitatServiceException {
		if (resourceName == null) {
			throw new HabitatServiceException("graph name cannot be null");
		}
		final String trimmedName = resourceName.trim();
		if (trimmedName.isEmpty()) {
			throw new HabitatServiceException("graph name cannot be empty");
		}
		try {
			swaggerClient.createOrResetProvenanceGraph(trimmedName);
		} catch (ApiException e) {
			throw handleApiException("error creating provenance graph",
					e);
		}

	}

	@Override
	public List<SubgraphInstance> getSubgraphs(
			String resourceName,
			SubgraphTemplate template,
			int limit,
			Long since) throws HabitatServiceException {
		checkNotNull(resourceName);
		checkNotNull(template);
		try {
//			return swaggerClient.getSubgraphs(resourceName, template,
//					Integer.valueOf(limit), since);
			return swaggerClient.getSubgraphs(resourceName, template, Integer.valueOf(limit), since);
		} catch (ApiException e) {
			throw handleApiException("error retrieving subgraphs", e);
		}

	}

	public void storeSubgraph(String resourceName, SubgraphInstance subgraph)
			throws HabitatServiceException {
		checkNotNull(resourceName);
		checkNotNull(subgraph);
		try {
			swaggerClient.storeSubgraph(resourceName, subgraph);
		} catch (ApiException e) {
			throw handleApiException("error storing subgraph", e);
		}
	}

	@Override
	public void storeSubgraphTemplate(String resourceName,
			SubgraphTemplate template)
			throws HabitatServiceException {
		checkNotNull(resourceName);
		checkNotNull(template);
		try {
			swaggerClient.storeSubgraphTemplate(resourceName, template);
		} catch (ApiException e) {
			throw handleApiException("error storing subgraph template", e);
		}

	}

	@Override
	public SubgraphTemplate getSubgraphTemplate(String resourceName)
			throws HabitatServiceException {
		checkNotNull(resourceName);
		try {
			return swaggerClient.getSubgraphTemplate(resourceName);
		} catch (ApiException e) {
			throw handleApiException("error getting subgraph template", e);
		}

	}
}
