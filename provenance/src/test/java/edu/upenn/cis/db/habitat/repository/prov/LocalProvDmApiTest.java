/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.repository.prov;

import org.junit.Before;
import org.junit.Rule;
// import org.neo4j.test.TestGraphDatabaseFactory;

import com.google.inject.Guice;
import com.google.inject.Injector;

import edu.upenn.cis.db.habitat.auth.backends.plugins.UnprotectedStorageModule;
import edu.upenn.cis.db.habitat.core.api.ProvDmApi;
import edu.upenn.cis.db.habitat.provenance.ProvDmStorageModule;
import edu.upenn.cis.db.habitat.provenance.ProvGraphStorageModule;
import edu.upenn.cis.db.habitat.provenance.provdm.test.AbstractProvDmApiTest;
import edu.upenn.cis.db.habitat.repository.storage.neo4j.ConfigurableNeoStorageModule;
import edu.upenn.cis.db.habitat.repository.storage.neo4j.Neo4JStore;
import edu.upenn.cis.db.habitat.provenance.provdm.test.Neo4jBoltRule;

public class LocalProvDmApiTest extends AbstractProvDmApiTest {

	@Rule
	public Neo4jBoltRule neo4j = new Neo4jBoltRule();

	@Before
	public void getProvDmApi() {
		final Injector injector = Guice
				.createInjector(
						new ConfigurableNeoStorageModule(neo4j.getBoltUri(),
								null, null),
						new ProvGraphStorageModule(),
						new ProvDmStorageModule(),
						new UnprotectedStorageModule(Neo4JStore.class)
				);
		store = injector.getInstance(ProvDmApi.class);
		super.neo4j = neo4j;
	}

}
