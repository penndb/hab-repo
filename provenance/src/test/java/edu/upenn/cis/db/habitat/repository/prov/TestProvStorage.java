/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.repository.prov;

import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.google.common.collect.ImmutableList;
import com.google.inject.Guice;
import com.google.inject.Injector;

import edu.upenn.cis.db.habitat.auth.backends.plugins.UnprotectedStorageModule;
import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.BooleanField;
import edu.upenn.cis.db.habitat.core.type.DoubleField;
import edu.upenn.cis.db.habitat.core.type.Field;
import edu.upenn.cis.db.habitat.core.type.IntField;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifierField;
import edu.upenn.cis.db.habitat.core.type.ProvStringToken;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.ProvTokenField;
import edu.upenn.cis.db.habitat.core.type.ProvUniqueToken;
import edu.upenn.cis.db.habitat.core.type.Schema;
import edu.upenn.cis.db.habitat.core.type.StringField;
import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvDmType;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.provenance.ProvStorageModule;
import edu.upenn.cis.db.habitat.repository.storage.neo4j.ConfigurableNeoStorageModule;
import edu.upenn.cis.db.habitat.repository.storage.neo4j.Neo4JStore;
import edu.upenn.cis.db.habitat.provenance.provdm.test.Neo4jBoltRule;

public class TestProvStorage {
	private ProvenanceApi store;

	@Rule
	public Neo4jBoltRule neo4j = new Neo4jBoltRule();

	@Before
	public void setup() {
		Injector injector = Guice.createInjector(
				new ConfigurableNeoStorageModule(neo4j.getBoltUri(),
						null,
						null),
				new ProvStorageModule(),
				new UnprotectedStorageModule(Neo4JStore.class));

		store = injector.getInstance(ProvenanceApi.class);
	}

	@Test
	public void test() throws HabitatServiceException {

		int count = 0;

		ProvToken first = new ProvStringToken("_TEST" + count++);
		List<Integer> interval = new ArrayList<>();
		interval.add(1);
		interval.add(2);
		BasicSchema s = new BasicSchema("s");
		s.addField("one", String.class);
		s.addField("two", Integer.class);
		BasicTuple tuple = s.createTuple();
		tuple.setValue("one", "First row");
		tuple.setValue("two", 13);
		BasicTuple tuple2 = s.createTuple();
		tuple2.setValue("one", "Second row");
		tuple2.setValue("two", 31);
		BasicTuple tuple3 = s.createTuple();
		tuple3.setValue("one", "Third row");
		tuple3.setValue("two", 99);

		store.storeProvenanceNode("", first, tuple, new ProvLocation("source",
				interval));

		ProvToken second = new ProvStringToken("_TEST" + count++);
		store.storeProvenanceNode("", second, tuple2, new ProvLocation(
				"source2",
				interval));

		ProvToken third = new ProvStringToken("_TEST" + count++);
		store.storeProvenanceNode("", third, tuple3, new ProvLocation(
				"source3",
				interval));

		TupleWithSchema<String> tupleAgain = store.getProvenanceData("", first);

		assertTrue(tupleAgain.getValue("one").equals(tuple.getValue("one")));
		assertTrue(tupleAgain.getValue("two").equals(tuple.getValue("two")));

		// Test edges
		store.storeProvenanceLink("", first, "OneLabel", second);
		store.storeProvenanceLink("", first, "TwoLabel", second);
		store.storeProvenanceLink("", second, "InvLabel", first);

		store.storeProvenanceLink("", first, "ThreeLabel", third);

		Set<ProvToken> results = StreamSupport.stream(
				store.getConnectedFrom("", first).spliterator(), true)
				.collect(Collectors.toSet());

		assertTrue(results.size() == 2);

		results = StreamSupport.stream(
				store.getConnectedFrom("", second).spliterator(), true)
				.collect(Collectors.toSet());
		assertTrue(results.size() == 1);

		results = StreamSupport.stream(
				store.getConnectedFrom("", third).spliterator(), true)
				.collect(Collectors.toSet());
		assertTrue(results.size() == 0);

		results = StreamSupport.stream(
				store.getConnectedFrom("", first, "OneLabel").spliterator(),
				true)
				.collect(Collectors.toSet());
		assertTrue(results.size() == 1);
		
		Map<String, StructuredData<String,Object>> results2 = store.getProvenanceNodes("");
		for (String k: results2.keySet()) {
			System.out.println(k);
			System.out.println(results2.get(k));
		}
	}

	@Test
	public void getProvLocationTest() throws HabitatServiceException {
		final ProvToken provToken = new ProvStringToken("_PROV_LOCATION_TEST");
		final ProvLocation provLocation = new ProvLocation(
				"provLocationTestSource", newArrayList(3, 4));

		store.storeProvenanceNode("", provToken, provLocation);

		final ProvSpecifier storedSpecifier = store
				.getProvenanceLocation("", provToken);
		assertTrue(storedSpecifier instanceof ProvLocation);
		final ProvLocation storedLocation = (ProvLocation) storedSpecifier;
		assertEquals(provLocation.getField(), storedLocation.getField());
		assertEquals(provLocation.getPosition(), storedLocation.getPosition());
	}

	@Test
	public void getProvenanceDataTest() throws IOException,
			HabitatServiceException {

		final ImmutableList<String> fieldNames = ImmutableList.of(
				"boolean_field_1",
				"double_field_1",
				"int_field_1",
				"provDmName",
				"prov_token_field_1",
				"prov_specifier_field_1");

		final ImmutableList<Field<?>> fields = ImmutableList.of(
				new BooleanField(true),
				new DoubleField(8.56),
				new IntField(76),
				new StringField(ProvDmType.ENTITY.name()),
				new ProvTokenField(new ProvUniqueToken()),
				new ProvSpecifierField(new ProvLocation(
						"provLocationTestSource2", newArrayList(1, 2, 3))));

		final List<Class<?>> fieldTypes = fields.stream()
				.map((Field<?> field) -> field.getType())
				.collect(Collectors.toList());

		final BasicSchema expectedSchema = new BasicSchema(
				"_GET_PROV_DATA_TEST_SCHEMA",
				fieldNames,
				fieldTypes);
		expectedSchema.addLookupKeyAt(fieldNames.size() - 2);
		final BasicTuple expectedTuple = expectedSchema.createTuple(fields);

		final ProvToken token = new ProvUniqueToken();
		final ProvLocation location = new ProvLocation(
				"getProvDataTestLocation", newArrayList(7, 8));

		store.storeProvenanceNode("", token, expectedTuple, location);

		final TupleWithSchema<String> actualTuple = store.getProvenanceData("",
				token);

		final Schema<String, Class<? extends Object>> actualSchemaSuper = actualTuple
				.getSchema();

		assertTrue(actualSchemaSuper instanceof BasicSchema);
		final BasicSchema actualSchema = (BasicSchema) actualSchemaSuper;

		assertEquals(expectedSchema.getArity(), actualSchema.getArity());
		assertEquals(expectedSchema.getName(), actualSchema.getName());
		assertEquals(expectedSchema.getTypes(), actualSchema.getTypes());
		assertEquals(expectedSchema.getKeys(), actualSchema.getKeys());
		assertEquals(expectedSchema.getLookupKeys(),
				actualSchema.getLookupKeys());

		assertEquals(expectedTuple.getFields().size(), actualTuple.getFields()
				.size());
		for (int i = 0; i < expectedTuple.getFields().size(); i++) {
			final Field<? extends Object> expectedField = expectedTuple
					.getFields().get(i);
			final Field<? extends Object> actualField = actualTuple.getFields()
					.get(i);
			assertEquals(expectedField.getType(), actualField.getType());
			if (expectedField.getValue() instanceof ProvLocation) {
				// No .equals for ProvLocation
				final ProvLocation expectedLocation = (ProvLocation) expectedField
						.getValue();
				final ProvLocation actualLocation = (ProvLocation) actualField
						.getValue();
				assertEquals(expectedLocation.getField(),
						actualLocation.getField());
				assertEquals(expectedLocation.getPosition(),
						actualLocation.getPosition());
				assertEquals(expectedLocation.getStream(),
						actualLocation.getStream());
			} else {
				assertEquals(expectedField.getValue(), actualField.getValue());
			}
		}
	}

	@Test
	public void getIncomingTest() throws HabitatServiceException {
		final String resource = "directCallIncomingTest";
		final String uuid = UUID.randomUUID().toString();

		final String targetProvTokenValue = uuid + ".target";
		final String source1ProvTokenValue = uuid + ".source1";
		final String source2ProvTokenValue = uuid + ".source2";

		final ProvToken targetProvToken = new ProvStringToken(
				targetProvTokenValue);
		final ProvToken source1ProvToken = new ProvStringToken(
				source1ProvTokenValue);
		final ProvToken source2ProvToken = new ProvStringToken(
				source2ProvTokenValue);

		final ProvLocation targetLocation = new ProvLocation(
				targetProvTokenValue + ".location", newArrayList(3, 4));
		final ProvLocation source1Location = new ProvLocation(
				source1ProvTokenValue + ".location", newArrayList(8, 9));
		final ProvLocation source2Location = new ProvLocation(
				source2ProvTokenValue + ".location", newArrayList(11, 13));

		store.storeProvenanceNode(resource, targetProvToken, targetLocation);
		store.storeProvenanceNode(resource, source1ProvToken, source1Location);
		store.storeProvenanceNode(resource, source2ProvToken, source2Location);

		final String expectedLabel1 = "l"
				+ UUID.randomUUID().toString().replace("-", "x");
		store.storeProvenanceLink(resource, source1ProvToken,
				expectedLabel1, targetProvToken);

		final String expectedLabel2 = "l"
				+ UUID.randomUUID().toString().replace("-", "x");
		store.storeProvenanceLink(resource, source2ProvToken,
				expectedLabel2, targetProvToken);

		final Iterable<ProvToken> inNeighborsWithLabel1 = store
				.getConnectedTo(resource, targetProvToken, expectedLabel1);
		final ProvToken actualNeighborWithLabel1 = getOnlyElement(
				inNeighborsWithLabel1, null);
		assertNotNull(actualNeighborWithLabel1);
		assertEquals(source1ProvToken, actualNeighborWithLabel1);

		final Iterable<ProvToken> inNeighborsWithLabel2 = store
				.getConnectedTo(resource, targetProvToken, expectedLabel2);
		final ProvToken actualNeighborWithLabel2 = getOnlyElement(
				inNeighborsWithLabel2, null);
		assertNotNull(actualNeighborWithLabel2);
		assertEquals(source2ProvToken, actualNeighborWithLabel2);

		final Iterable<ProvToken> inNeighborsNoLabel = store
				.getConnectedTo(resource, targetProvToken);
		final Set<ProvToken> actualNeighborNoLabel = newHashSet(
				inNeighborsNoLabel);
		assertEquals(2, actualNeighborNoLabel.size());
		assertEquals(newHashSet(source1ProvToken, source2ProvToken),
				actualNeighborNoLabel);

		final Map<String, List<TupleWithSchema<String>>> actualInEdges = store
				.getEdgeMapTo(resource, targetProvToken);
		assertEquals(2, actualInEdges.size());
		final Set<String> actualInEdgeLabels = actualInEdges
				.values()
				.stream()
				.map(tuple -> (String) getOnlyElement(tuple).get("label")
						.getValue())
				.collect(Collectors.toSet());
		assertEquals(newHashSet(expectedLabel1, expectedLabel2),
				actualInEdgeLabels);

		final String wrongLabel = UUID.randomUUID().toString();
		final Iterable<ProvToken> inNeighborsWrongLabel = store
				.getConnectedTo(resource, targetProvToken, wrongLabel);
		final ProvToken actualNeighborWrongLabel = getOnlyElement(
				inNeighborsWrongLabel, null);
		assertNull(actualNeighborWrongLabel);

		final Iterable<ProvToken> inNeighborsWrongDirection = store
				.getConnectedTo(resource, source1ProvToken);
		final ProvToken actualNeighborWrongDirection = getOnlyElement(
				inNeighborsWrongDirection, null);
		assertNull(actualNeighborWrongDirection);

		final Map<String, List<TupleWithSchema<String>>> actualInEdgesWrongDirection = store
				.getEdgeMapTo(resource, source1ProvToken);
		assertTrue(actualInEdgesWrongDirection.isEmpty());
	}

	@After
	public void tearDown() {}
}
