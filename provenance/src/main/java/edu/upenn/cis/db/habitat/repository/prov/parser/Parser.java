/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.repository.prov.parser;

import java.io.File;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.db.habitat.repository.prov.parser.data.Doc;

public class Parser {
	final static Logger logger = LogManager.getLogger(Parser.class);

	public static Doc parse(String path) {
    	SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
    	Doc doc = new Doc();
	    try {
	        SAXParser saxParser = saxParserFactory.newSAXParser();
	        SaxHandler parser = new SaxHandler();
	        saxParser.parse(path, parser);
	        
	        
	        doc.setActivityList(parser.activityList);
	        doc.setActedOnBehalfOfList(parser.actedOnBehalfOfList);
	        doc.setAgentList(parser.agentList);
	        doc.setEntityList(parser.entityList);
	        doc.setUsedList(parser.usedList);
	        doc.setWasAssociatedWithList(parser.wasAssociatedWithList);
	        doc.setWasAttributedToList(parser.wasAttributedToList);
	        doc.setWasDerivedFromList(parser.wasDerivedFromList);
	        doc.setWasEndedByList(parser.wasEndedByList);
	        doc.setWasGeneratedByList(parser.wasGeneratedByList);
	        doc.setWasStartedByList(parser.wasStartedByList);
	        doc.setSpecializationOfList(parser.specializationOfList);
	        doc.setAlternateOfList(parser.alternateOfList);
	        return doc;
	    } catch(Exception e){
	    	return null;
	    }
	}

	public static void main(String[] args) {
		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
	    try {
	        SAXParser saxParser = saxParserFactory.newSAXParser();
	        SaxHandler parser = new SaxHandler();
	        saxParser.parse(new File("/Users/sanjeetphatak/Documents/primer-prov-xml-examples-tz.provx"), parser);
	        
	        Doc doc = new Doc();
	        doc.setActivityList(parser.activityList);
	        doc.setActedOnBehalfOfList(parser.actedOnBehalfOfList);
	        doc.setAgentList(parser.agentList);
	        doc.setEntityList(parser.entityList);
	        doc.setUsedList(parser.usedList);
	        doc.setWasAssociatedWithList(parser.wasAssociatedWithList);
	        doc.setWasAttributedToList(parser.wasAttributedToList);
	        doc.setWasDerivedFromList(parser.wasDerivedFromList);
	        doc.setWasEndedByList(parser.wasEndedByList);
	        doc.setWasGeneratedByList(parser.wasGeneratedByList);
	        doc.setWasStartedByList(parser.wasStartedByList);
	        
	    } catch (Exception e) {
	        e.printStackTrace();
	    }

	}

}
