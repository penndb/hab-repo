/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.provenance;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Set;
import java.util.function.Supplier;

import spark.Service;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;

import edu.upenn.cis.db.habitat.core.api.ProvenanceGraphApi;
import edu.upenn.cis.db.habitat.core.type.model.ProvSpecifierTypeAdapterFactory;
import edu.upenn.cis.db.habitat.core.type.model.TupleWithSchemaTypeAdapterFactory;
import edu.upenn.cis.db.habitat.core.webservice.RestPlugin;
import edu.upenn.cis.db.habitat.core.webservice.RouteSpecifier;
import edu.upenn.cis.db.habitat.webservice.routes.CreateOrResetProvenanceGraph;
import edu.upenn.cis.db.habitat.webservice.routes.CreateProvenanceGraph;
import edu.upenn.cis.db.habitat.webservice.routes.ExceptionHandlingRoute;
import edu.upenn.cis.db.habitat.webservice.routes.GetConnectedFrom;
import edu.upenn.cis.db.habitat.webservice.routes.GetConnectedTo;
import edu.upenn.cis.db.habitat.webservice.routes.GetEdgesFrom;
import edu.upenn.cis.db.habitat.webservice.routes.GetEdgesTo;
import edu.upenn.cis.db.habitat.webservice.routes.GetProvenanceData;
import edu.upenn.cis.db.habitat.webservice.routes.GetProvenanceLocation;
import edu.upenn.cis.db.habitat.webservice.routes.GetProvenanceNodes;
import edu.upenn.cis.db.habitat.webservice.routes.GetSubgraphTemplate;
import edu.upenn.cis.db.habitat.webservice.routes.GetSubgraphs;
import edu.upenn.cis.db.habitat.webservice.routes.RouteWithInfo;
import edu.upenn.cis.db.habitat.webservice.routes.StoreProvenanceLink;
import edu.upenn.cis.db.habitat.webservice.routes.StoreProvenanceNode;
import edu.upenn.cis.db.habitat.webservice.routes.StoreSubgraph;
import edu.upenn.cis.db.habitat.webservice.routes.StoreSubgraphTemplate;

public class ProvServices implements RestPlugin {

	@VisibleForTesting
	public static final class GsonFactory implements Supplier<Gson> {

		@Override
		public Gson get() {
			return new GsonBuilder()
					.registerTypeAdapterFactory(
							new ProvSpecifierTypeAdapterFactory())
					.registerTypeAdapterFactory(
							new TupleWithSchemaTypeAdapterFactory())
					.create();

		}

	}

	private final Set<RouteWithInfo> operations;
	private final ProvenanceGraphApi prov;
	private final Service spark;
	private final Gson gson = new GsonFactory().get();

	@Inject
	public ProvServices(ProvenanceGraphApi storage, Service spark) {
		this.spark = checkNotNull(spark);
		prov = checkNotNull(storage);
		this.operations = ImmutableSet.of(
				new CreateProvenanceGraph(prov, this.spark),
				new CreateOrResetProvenanceGraph(prov, this.spark),
				new StoreProvenanceNode(prov, this.spark, gson),
				new GetProvenanceData(prov, this.spark),
				new GetProvenanceLocation(prov, this.spark),
				new StoreProvenanceLink(prov, this.spark, gson),
				new GetConnectedFrom(prov, this.spark),
				new GetConnectedTo(prov, this.spark),
				new GetEdgesFrom(prov, this.spark),
				new GetEdgesTo(prov, this.spark),
				new GetProvenanceNodes(prov, this.spark),
				new GetSubgraphs(prov, this.spark, gson),
				new StoreSubgraph(prov, this.spark, gson),
				new StoreSubgraphTemplate(prov, this.spark, gson),
				new GetSubgraphTemplate(prov, this.spark));
	}

	@Override
	public Multimap<String, RouteSpecifier> getSpecifiers() {
		Multimap<String, RouteSpecifier> ret = HashMultimap.create();
		for (final RouteWithInfo operation : operations) {
			ret.put(operation.getSparkSubpath(),
					new RouteSpecifier(operation.getRequestType(),
							operation.isSecure(),
							new ExceptionHandlingRoute(spark, operation)));
		}
		return ret;
	}
}
