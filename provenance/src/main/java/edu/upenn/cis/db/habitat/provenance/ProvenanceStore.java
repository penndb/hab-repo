/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.provenance;

import static java.util.stream.Collectors.groupingByConcurrent;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toConcurrentMap;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.annotation.Nullable;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.base.Strings;
import com.google.inject.Inject;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.type.Field;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.ProvStringToken;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.StructuredValue;
import edu.upenn.cis.db.habitat.core.type.TransactionSupplier;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.provenance.marshalling.MarshalProvAndTuple;
import edu.upenn.cis.db.habitat.provenance.marshalling.MarshalProvSpecifier;
import edu.upenn.cis.db.habitat.repository.storage.StorageApiWithMetadataLocal;
import edu.upenn.cis.db.habitat.util.ObjectSerialization;

/**
 * Provenance store using Habitat StorageApi
 * 
 * By default, encodes non-scalar data using JSON
 * 
 * @author zives
 *
 */
public class ProvenanceStore implements ProvenanceApi {
	final static Logger logger = LogManager.getLogger(ProvenanceStore.class);
	
	final StorageApiWithMetadataLocal<String,Object, ?> store;
	
	// Serialize pairs of (provenance, tuple)
	final MarshalProvAndTuple pairSerializer;
	
	// Serialize pairs of (provenance, tuple)
	final MarshalProvSpecifier provSerializer;
	
	@Inject
	public ProvenanceStore(
		StorageApiWithMetadataLocal<String,Object, ?> store) {
		this.store = store;

		provSerializer = new MarshalProvSpecifier("location"); 
		pairSerializer = new MarshalProvAndTuple("location", "tuple");
	}

//	@Override
//	public void annotateProvenanceNode(String resource, ProvQualifiedNameToken idToken, String key, Serializable value)
//			throws HabitatServiceException {
//		StructuredData<String,Object> obj = new StructuredData<>();
//		
//		obj.put(key, value);
//		store.addMetadata(resource, idToken.toString(), 
//				obj);
//	}
//
//	@Override
//	public Serializable getProvenanceNodeAnnotation(String resource, ProvQualifiedNameToken idToken, String key)
//			throws HabitatServiceException {
//		// TODO Auto-generated method stub
//		return null;
//	}

	@Override
	public void storeProvenanceNode(String resource, ProvToken token, ProvSpecifier location) throws HabitatServiceException {
		store.addMetadata(resource, token.toString(), 
				provSerializer.getSerializedObject(location));
	}

	@Override
	public void storeProvenanceNode(String resource,
			ProvToken token, 
			TupleWithSchema<String> tuple, 
			ProvSpecifier location) throws HabitatServiceException {
		storeProvenanceNode(resource, token, tuple, location, null);
	}

	protected void storeProvenanceNode(String resource,
			ProvToken token,
			TupleWithSchema<String> tuple,
			ProvSpecifier location,
			@Nullable TransactionSupplier transactionSupplier)
			throws HabitatServiceException {
		final String tokenString = token.toString();
		final StructuredData<String, Object> serializedObject = pairSerializer
				.getSerializedObject(
				new ImmutablePair<>(location, tuple));
		
		// Optional PROV name
		final Field<? extends Object> provDmNameField = tuple.get("{http://pennprovenance.net/ns/prov#}provDmName");
		final String label = provDmNameField == null ? null : provDmNameField.toString();
		if (transactionSupplier != null) {
			store.addMetadata(transactionSupplier,
					resource,
					tokenString,
					serializedObject,
					label);
		} else {
			store.addMetadata(resource,
					tokenString,
					serializedObject,
					label);
		}
	}
	
	@Override
	public ProvSpecifier getProvenanceLocation(String resource, ProvToken token) throws HabitatServiceException {
		StructuredData<String,Object> metadata = store.getMetadata(resource, token.toString());
		if (metadata == null) {
			throw new NoSuchProvTokenException(token);
		}
		
		return pairSerializer.getFirst(metadata);
	}

	@Override
	public TupleWithSchema<String> getProvenanceData(String resource, ProvToken token) throws HabitatServiceException {
		StructuredData<String, Object> combinedTuple = store.getMetadata(resource, token.toString());
		if (combinedTuple == null) {
			throw new NoSuchProvTokenException(token);
		}
		return pairSerializer.getSecond(combinedTuple);
	}

	@Override
	public void storeProvenanceLink(String resource, ProvToken from, String label, ProvToken to) throws HabitatServiceException {
		StructuredData<String,Object> data = new StructuredValue<>();
		data.put("label", label);
		store.link(resource, from.toString(), label, to.toString(), data);
	}

	@Override
	public void storeProvenanceLink(String resource, ProvToken from, String label, ProvToken to, TupleWithSchema<String> tuple) throws HabitatServiceException {
		storeProvenanceLink(resource, from, label, to, tuple, null);
	}
	
	protected void storeProvenanceLink(
			String resource,
			ProvToken from,
			String label,
			ProvToken to,
			TupleWithSchema<String> tuple,
			@Nullable TransactionSupplier transactionSupplier)
			throws HabitatServiceException {
		final String fromToken = from.toString();
		final String toToken = to.toString();
		final StructuredData<String, Object> tupleData = ObjectSerialization
				.getObjectDataFromTuple(tuple);
		if (transactionSupplier != null) {
			store.link(
					transactionSupplier,
					resource,
					fromToken,
					label,
					toToken,
					tupleData);
		} else {
			store.link(
					resource,
					fromToken,
					label,
					toToken,
					tupleData);
		}
	}

	@Override
	public void storeProvenanceLink(String resource, ProvToken fromLeft, ProvToken fromRight, String label, ProvToken to) throws HabitatServiceException {
		storeProvenanceLink(resource, fromLeft, label, to);
		storeProvenanceLink(resource, fromRight, label, to);
	}

	@Override
	public void storeProvenanceLink(String resource, ProvToken fromLeft, ProvToken fromRight, 
			String label, ProvToken to,
			TupleWithSchema<String> tuple) throws HabitatServiceException {
		storeProvenanceLink(resource, fromLeft, label, to, tuple);
		storeProvenanceLink(resource, fromRight, label, to, tuple);
	}

	@Override
	public void storeProvenanceLink(String resource, List<ProvToken> inputProv, String label, ProvToken to) throws HabitatServiceException {
		for (ProvToken prov: inputProv)
			storeProvenanceLink(resource, prov, label, to);
	}

	@Override
	public void storeProvenanceLink(String resource, List<ProvToken> inputProv, String label, ProvToken to, 
			TupleWithSchema<String> tuple) throws HabitatServiceException {
		for (ProvToken prov: inputProv)
			storeProvenanceLink(resource, prov, label, to, tuple);
	}
	
	ProvToken getToken(String tok) {
//		return new ProvIntToken(Integer.valueOf(tok));
		return new ProvStringToken(tok);
	}

	@Override
	public List<ProvToken> getConnectedTo(String resource, ProvToken to) throws HabitatServiceException {
		Iterable<Pair<String,StructuredData<String,Object>>> links = store.getLinksTo(resource, to.toString());
		
		return StreamSupport.stream(links.spliterator(), true)
				.map(tuple -> getToken(tuple.getLeft())).collect(Collectors.toList());
	}

	@Override
	public List<TupleWithSchema<String>> getEdgesTo(String resource, ProvToken to) throws HabitatServiceException {
		Iterable<Pair<String,StructuredData<String,Object>>> links = store.getLinksTo(resource, to.toString());

		return StreamSupport.stream(links.spliterator(), true)
				.map(keyedTuple -> 
				ObjectSerialization.getTupleFromObjectData("edges", keyedTuple.getRight()))
				.collect(Collectors.toList());
	}

	@Override
	public List<ProvToken> getConnectedFrom(String resource, ProvToken from) throws HabitatServiceException {
		Iterable<Pair<String,StructuredData<String,Object>>> links = store.getLinksFrom(resource, from.toString());
		
		return StreamSupport.stream(links.spliterator(), true)
				.map(tuple -> getToken(tuple.getLeft())).collect(Collectors.toList());
	}

	@Override
	public List<ProvToken> getConnectedFrom(String resource, ProvToken from, String label) throws HabitatServiceException {
		Iterable<Pair<String,StructuredData<String,Object>>> links = store.getLinksFrom(resource, from.toString());

		return StreamSupport.stream(links.spliterator(), true)
				.filter(tuple -> tuple.getRight().get("label").equals(label))
				.map(tuple -> getToken(tuple.getLeft())).collect(Collectors.toList());
	}

	@Override
	public List<ProvToken> getConnectedTo(String resource, ProvToken from, String label) throws HabitatServiceException {
		Iterable<Pair<String,StructuredData<String,Object>>> links = store.getLinksTo(resource, from.toString());

		return StreamSupport.stream(links.spliterator(), true)
				.filter(tuple -> tuple.getRight().get("label").equals(label))
				.map(tuple -> getToken(tuple.getLeft())).collect(Collectors.toList());
	}

	@Override
	public List<TupleWithSchema<String>> getEdgesFrom(String resource, ProvToken from) throws HabitatServiceException {
		Iterable<Pair<String,StructuredData<String,Object>>> links = store.getLinksFrom(resource, from.toString());
		
		return StreamSupport.stream(links.spliterator(), true)
				.map(keyedTuple -> 
				ObjectSerialization.getTupleFromObjectData("edges", keyedTuple.getRight()))
				.collect(Collectors.toList());
	}
	
	@Override
	public Map<String, List<TupleWithSchema<String>>> getEdgeMapFrom(
			String resource,
			ProvToken from) throws HabitatServiceException {
		final String normalizedResource = Strings.nullToEmpty(resource);
		Iterable<Pair<String, StructuredData<String, Object>>> links = store
				.getLinksFrom(normalizedResource, from.toString());

		return createEdgeMap(links);
	}
	
	@Override
	public Map<String, List<TupleWithSchema<String>>> getEdgeMapTo(
			String resource,
			ProvToken to) throws HabitatServiceException {
		final String normalizedResource = Strings.nullToEmpty(resource);
		Iterable<Pair<String,StructuredData<String,Object>>> links = store.getLinksTo(normalizedResource, to.toString());
		
		return createEdgeMap(links);
	}

	@Override
	public Map<String, StructuredData<String, Object>> getProvenanceNodes(String resource)
			throws HabitatServiceException {
		final String requestedResource = Strings.nullToEmpty(resource);
		final Iterable<Pair<String, StructuredData<String, Object>>> metadata = store.getMetadataForResource(requestedResource);
		return StreamSupport.stream(metadata.spliterator(), true)
				.filter(pair -> pair.getRight().containsKey("location"))
				.collect(toConcurrentMap(Pair::getLeft, Pair::getRight));
	}

	private Map<String, List<TupleWithSchema<String>>> createEdgeMap(Iterable<Pair<String, StructuredData<String, Object>>> links) {
		return StreamSupport.stream(links.spliterator(), true)
				.collect(
						groupingByConcurrent(
								Pair::getLeft,
								mapping(keyedTuple ->
										ObjectSerialization
												.getTupleFromObjectData(
														"edges",
														keyedTuple.getRight()),
										toList())));	
	}

}
