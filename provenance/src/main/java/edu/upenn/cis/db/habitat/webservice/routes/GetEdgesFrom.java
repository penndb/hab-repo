/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import static com.google.common.base.Preconditions.checkNotNull;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.PROV_TOKEN_PATH_PARAM;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.RESOURCE_QUERY_PARAM;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.model.ProvEdgeSetModel;
import edu.upenn.cis.db.habitat.core.type.model.ProvTokenModel;
import spark.Request;
import spark.Response;
import spark.Service;

/**
 * @author John Frommeyer
 *
 */
public class GetEdgesFrom extends GetEdgesFromWsMethod implements
		RouteWithInfo {

	private final Logger logger = LogManager.getLogger(getClass());
	private final ProvenanceApi backend;
	private final Service spark;

	public GetEdgesFrom(ProvenanceApi backend, Service spark) {
		super();
		this.backend = checkNotNull(backend);
		this.spark = checkNotNull(spark);
	}

	@Override
	public ProvEdgeSetModel handle(
			Request request,
			Response response) throws Exception {
		final String m = "handle(...)";

		final String pathToken = getPathParam(spark, request,
				PROV_TOKEN_PATH_PARAM);
		logger.debug("{}: Got ProvToken from path {}", m, pathToken);

		final String resource = getPathParam(spark, request,
				RESOURCE_QUERY_PARAM);
		logger.debug("{}: Got resource from path {}", m, resource);

		final ProvEdgeSetModel tokenSet = getEdgesFrom(resource, pathToken);
		response.status(HttpServletResponse.SC_OK);

		return tokenSet;
	}

	@Override
	public ProvEdgeSetModel getEdgesFrom(String resource, String pathToken)
			throws Exception {
		final String m = "getEdgesFrom(...)";
		final ProvToken provToken = ProvTokenModel.toProvToken(pathToken);
		logger.debug("{}: Extracted provToken: [{}] of type {}", m,
				provToken, provToken.getClass());

		Map<String, List<TupleWithSchema<String>>> edgesFrom = backend
				.getEdgeMapFrom(resource, provToken);
		final ProvEdgeSetModel outEdges = ProvEdgeSetModel
				.from(edgesFrom);
		logger.debug("{}: returning out edges: {} for resource {}", m,
				outEdges, resource);
		return outEdges;
	}
}
