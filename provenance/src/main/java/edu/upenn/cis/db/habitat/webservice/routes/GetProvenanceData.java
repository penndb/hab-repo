/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import static com.google.common.base.Preconditions.checkNotNull;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.PROV_TOKEN_PATH_PARAM;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.RESOURCE_PATH_PARAM;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import spark.Request;
import spark.Response;
import spark.Service;
import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.model.ProvTokenModel;
import edu.upenn.cis.db.habitat.core.type.model.TupleWithSchemaModel;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;

/**
 * @author John Frommeyer
 *
 */
public class GetProvenanceData extends GetProvenanceDataWsMethod implements
		RouteWithInfo {

	private final Logger logger = LogManager.getLogger(getClass());
	private final ProvenanceApi backend;
	private final Service spark;

	public GetProvenanceData(ProvenanceApi backend, Service spark) {
		super();
		this.backend = checkNotNull(backend);
		this.spark = checkNotNull(spark);
	}

	@Override
	public TupleWithSchemaModel handle(
			Request request,
			Response response) throws Exception {
		final String m = "handle(...)";

		final String resource = getPathParam(spark, request, RESOURCE_PATH_PARAM);
		logger.debug("{}: Got resource from path {}", m, resource);
		
		final String pathToken = getPathParam(spark, request, PROV_TOKEN_PATH_PARAM);
		logger.debug("{}: Got ProvToken from path {}", m, pathToken);

		if (pathToken == null) {
			throw halt(spark, HttpServletResponse.SC_BAD_REQUEST,
					new ResponseError(
							"request missing provToken"));
		}

		final TupleWithSchemaModel tupleModel = getProvenanceData(resource, pathToken);
		response.status(HttpServletResponse.SC_OK);

		return tupleModel;
	}

	@Override
	public TupleWithSchemaModel getProvenanceData(String resource, String pathToken)
			throws Exception {
		final String m = "getProvenanceData(...)";
		final ProvToken provToken = ProvTokenModel.toProvToken(pathToken);
		logger.debug("{}: Extracted provToken: [{}] of type {}", m,
				provToken, provToken.getClass());

		final TupleWithSchema<String> tuple = backend
				.getProvenanceData(resource, provToken);

		final TupleWithSchemaModel tupleModel = tuple == null ? null
				: TupleWithSchemaModel
						.from(tuple);
		return tupleModel;
	}
}
