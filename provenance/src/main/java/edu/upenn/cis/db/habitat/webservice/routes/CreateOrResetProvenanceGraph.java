/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import static com.google.common.base.Preconditions.checkNotNull;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.RESOURCE_PATH_PARAM;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import spark.Request;
import spark.Response;
import spark.Service;
import edu.upenn.cis.db.habitat.core.api.ProvenanceGraphApi;

/**
 * @author John Frommeyer
 *
 */
public class CreateOrResetProvenanceGraph extends CreateOrResetProvenanceGraphWsMethod
		implements
		RouteWithInfo {

	private final Logger logger = LogManager.getLogger(getClass());
	private final ProvenanceGraphApi backend;
	private final Service spark;

	public CreateOrResetProvenanceGraph(ProvenanceGraphApi backend,
			Service spark) {
		super();
		this.backend = checkNotNull(backend);
		this.spark = checkNotNull(spark);
	}

	@Override
	public Object handle(
			Request request,
			Response response) throws Exception {
		final String m = "handle(...)";

		final String resource = getPathParam(spark, request,
				RESOURCE_PATH_PARAM);
		logger.debug("{}: Got resource from path {}", m, resource);

		createOrResetProvenanceGraph(resource);
		response.status(HttpServletResponse.SC_NO_CONTENT);

		return null;

	}

	@Override
	public void createOrResetProvenanceGraph(
			String resource)
			throws Exception {
		final String m = "createOrReplaceProvenanceGraph(...)";
		backend.createOrResetProvenanceGraph(resource);
	}
}
