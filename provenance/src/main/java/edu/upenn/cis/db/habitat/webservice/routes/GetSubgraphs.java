/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import static com.google.common.base.Preconditions.checkNotNull;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.RESOURCE_QUERY_PARAM;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.SUBGRAPHS_LIMIT_QUERY_PARAM;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.SUBGRAPHS_SINCE_QUERY_PARAM;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import spark.Request;
import spark.Response;
import spark.Service;

import com.google.common.base.Stopwatch;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import edu.upenn.cis.db.habitat.core.api.ProvenanceGraphApi;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;

/**
 * @author John Frommeyer
 *
 */
public class GetSubgraphs extends GetSubgraphsWsMethod
		implements RouteWithInfo {

	private final Logger logger = LogManager.getLogger(getClass());
	private final ProvenanceGraphApi backend;
	private final Service spark;
	private final Gson gson;

	public GetSubgraphs(ProvenanceGraphApi backend,
			Service spark,
			Gson gson) {
		super();
		this.backend = checkNotNull(backend);
		this.spark = checkNotNull(spark);
		this.gson = checkNotNull(gson);
	}

	public List<SubgraphInstance> handle(
			Request request,
			Response response) throws Exception {
		final String m = "handle(...)";
		response.header("Content-Encoding", "gzip");
		final String resource = getPathParam(
				spark,
				request,
				RESOURCE_QUERY_PARAM);
		final int limit = getIntQueryParam(spark, request,
				SUBGRAPHS_LIMIT_QUERY_PARAM, 400);
		if (limit <= 0) {
			throw halt(spark, HttpServletResponse.SC_BAD_REQUEST,
					new ResponseError("limit must be > 0"));
		}

		Optional<Long> sinceOpt = getLongQueryParam(spark, request,
				SUBGRAPHS_SINCE_QUERY_PARAM);
		if (sinceOpt.isPresent() && sinceOpt.get() < 0) {
			throw halt(spark, HttpServletResponse.SC_BAD_REQUEST,
					new ResponseError("since must be >= 0"));
		}
		final String jsonBody = request.body();
		logger.debug("{}: Got body {}", m, jsonBody);
		final SubgraphTemplate template = gson.fromJson(jsonBody,
				SubgraphTemplate.class);
		validateBody(template);
		
		JsonParser parser = new JsonParser();
		JsonElement root = parser.parse(jsonBody);
		
		JsonElement since = root.getAsJsonObject().get("since");
		
		if (since != null && !since.isJsonNull())
			sinceOpt = Optional.ofNullable(since.getAsLong());
		
		final List<SubgraphInstance> subgraphs = getSubgraphs(
				resource,
				template,
				limit,
				sinceOpt.orElse(null));
		response.status(HttpServletResponse.SC_OK);
		return subgraphs;
	}

	@Override
	public List<SubgraphInstance> getSubgraphs(String resource,
			SubgraphTemplate template,
			int limit,
			Long since)
			throws Exception {
		final String m = "getSubgraphs(...)";
		final Stopwatch stopwatch = Stopwatch.createStarted();
		final List<SubgraphInstance> subgraphs = backend.getSubgraphs(
				resource,
				template,
				limit,
				since);
		stopwatch.stop();
		logger.info("{}: Retrieved {} subgraphs for graph [{}]: {}", m,
				subgraphs.size(), resource, stopwatch);
		return subgraphs;
	}

	private void validateBody(SubgraphTemplate body) {
		if (body == null) {
			throw halt(spark, HttpServletResponse.SC_BAD_REQUEST,
					new ResponseError("missing template"));
		}
		if (body.getRanks() == null || body.getRanks().isEmpty()) {
			throw halt(spark, HttpServletResponse.SC_BAD_REQUEST,
					new ResponseError("missing ranks"));
		}
		if (body.getLinks() == null) {
			throw halt(spark, HttpServletResponse.SC_BAD_REQUEST,
					new ResponseError("missing links"));
		}
	}
}
