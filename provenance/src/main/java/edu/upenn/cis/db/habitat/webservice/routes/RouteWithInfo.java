/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import spark.HaltException;
import spark.Request;
import spark.Route;
import spark.Service;
import edu.upenn.cis.db.habitat.core.webservice.JsonUtil;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;
import edu.upenn.cis.db.habitat.webservice.WsMethodInfo;

/**
 * @author John Frommeyer
 *
 */
public interface RouteWithInfo extends WsMethodInfo, Route {

	public static HaltException haltWithResponseError(Service spark, int code,
			ResponseError errorBody) {
		return spark.halt(code, JsonUtil.toJson(errorBody));
	}

	default public HaltException halt(Service spark, int code,
			ResponseError errorBody) {
		return RouteWithInfo.haltWithResponseError(spark, code, errorBody);
	}

	default public String getPathParam(Service spark, Request request,
			String key) {
		final String param = request.params(key);

		if (param == null) {
			throw halt(spark, HttpServletResponse.SC_BAD_REQUEST,
					new ResponseError(
							"request missing " + key));
		}
		return param;
	}

	default public Optional<String> getQueryParam(Request request, String key) {
		final String param = request.queryParams(key);

		return Optional.ofNullable(param);
	}

	default public Optional<Integer> getIntQueryParam(
			Service spark,
			Request request, 
			String key) {
		final Optional<String> strParam = getQueryParam(request, key);
		if (!strParam.isPresent()) {
			return Optional.empty();
		}
		final String strValue = strParam.get();
		Integer intValue = null;
		try {
			intValue = Integer.valueOf(strValue);
		} catch (NumberFormatException e) {
			throw halt(
					spark,
					HttpServletResponse.SC_BAD_REQUEST,
					new ResponseError(
							String.format(
									"param %s should be an integer, got %s",
									key, strValue)));
		}
		return Optional.of(intValue);
	}
	
	default public int getIntQueryParam(
			Service spark,
			Request request, 
			String key,
			int defaultValue) {
		final Optional<Integer> intParam = getIntQueryParam(spark, request, key);
		return intParam.isPresent() ? intParam.get() : defaultValue;
	}
	
	default public Optional<Long> getLongQueryParam(
			Service spark,
			Request request, 
			String key) {
		final Optional<String> strParam = getQueryParam(request, key);
		if (!strParam.isPresent()) {
			return Optional.empty();
		}
		final String strValue = strParam.get();
		Long longValue = null;
		try {
			longValue = Long.valueOf(strValue);
		} catch (NumberFormatException e) {
			throw halt(
					spark,
					HttpServletResponse.SC_BAD_REQUEST,
					new ResponseError(
							String.format(
									"param %s should be an long, got %s",
									key, strValue)));
		}
		return Optional.of(longValue);
	}
}
