/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.repository.prov.serializers;

import java.util.Map;
import java.util.function.Supplier;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class HtmlProv {
	final static Logger logger = LogManager.getLogger(HtmlProv.class);
	
	public static StringBuilder getHome() {
		logger.info("Handling request to display homepage");
		StringBuilder out = new StringBuilder();

		out.append("<html><head>");
		out.append("<style>");
		out.append("table, th, td {");
		out.append("border: 1px solid black;");
		out.append("}");
		out.append("</style>");
		out.append("</head><title>Habitat</title></head><body>\n");
		
		out.append("<h1>Welcome to the Habitat Platform!</h1>");
		
		out.append("<ul>");
		out.append("<a href=\"/upload\">Upload provenance (PROV-DM or PROV-N) data</a>");
		out.append("<a href=\"/display\">Display provenance data</a>");
		out.append("</ul>");

		out.append("</body>");
		out.append("</html>");
		return out;
	}

	public static StringBuilder getDisplay(Supplier<? extends Iterable<? extends Map<String,Object>>> agentSupplier,
					Supplier<? extends Iterable<? extends Map<String,Object>>> entitySupplier,
					Supplier<? extends Iterable<? extends Map<String,Object>>> activitySupplier,
					Supplier<? extends Iterable<? extends Map<String,Object>>> assocSupplier
			){
		logger.info("Handling request to display content");

		StringBuilder out = new StringBuilder();

		out.append("<html><head>");
		out.append("<style>");
		out.append("table, th, td {");
		out.append("border: 1px solid black;");
		out.append("}");
		out.append("</style>");
		out.append("</head><title>Info</TITLE></head><body>\n");



		//Agent table
		out.append("<center><b>Agent<b></center><br><br>");
		out.append("<center><table style=\"width:100%\">\n");
		out.append("<table><tr>");
		out.append("<br><br>");
		out.append("<th>ID</th>");
		out.append("<th>Name</th>");
		out.append("<th>Type</th>");
		out.append("<th>Given Name</th>");
		out.append("<th>Mbox</th>");
		out.append("</tr>");

		agentSupplier.get().forEach(record -> {
			out.append("<tr>");
			out.append("<td>"+ record.get("id").toString() +"</td>");
			out.append("<td>"+ record.get("name").toString() +"</td>");
			out.append("<td>"+ record.get("type").toString() +"</td>");
			out.append("<td>"+ record.get("givenName").toString() +"</td>");
			out.append("<td>"+ record.get("mbox").toString() +"</td>");
			out.append("</tr>");
		});
		
		out.append("</table></center>");

		entitySupplier.get().forEach(record -> {
     	  out.append("<tr>");
     	  out.append("<td>"+ record.get("id").toString() +"</td>");
     	  out.append("<td>"+ record.get("title").toString() +"</td>");
     	  out.append("<td>"+ record.get("type").toString() +"</td>");
     	  out.append("</tr>");
        });
	    		
   		out.append("</table></center>");
   		
        out.append("<br><br><center><b>Activity<b></center><br><br>");
		out.append("<center><table style=\"width:100%\">\n");
		out.append("<table><tr>");
		out.append("<br><br>");
		out.append("<th>ID</th>");
		out.append("<th>StartTime</th>");
		out.append("<th>EndTime</th>");
		out.append("</tr>");
	    		
		activitySupplier.get().forEach(record -> {
     	  out.append("<tr>");
     	  out.append("<td>"+ record.get("id").toString() +"</td>");
     	  out.append("<td>"+ record.get("starttime").toString() +"</td>");
     	  out.append("<td>"+ record.get("endtime").toString() +"</td>");
     	  out.append("</tr>");
        });
		out.append("</table></center>");
	    		
	    		
	    		
        out.append("<br><br><center><b>Activity<b></center><br><br>");
		out.append("<center><table style=\"width:100%\">\n");
		out.append("<table><tr>");
		out.append("<br><br>");
		out.append("<th>ID</th>");
		out.append("<th>StartTime</th>");
		out.append("<th>EndTime</th>");
		out.append("</tr>");
	    		
		assocSupplier.get().forEach(record -> {
     	  out.append("<tr>");
     	  out.append("<td>"+ record.get("id").toString() +"</td>");
     	  out.append("<td>"+ record.get("starttime").toString() +"</td>");
     	  out.append("<td>"+ record.get("endtime").toString() +"</td>");
     	  out.append("</tr>");
        });
	        
		out.append("</body>");
		out.append("</html>");
		return out;
	    
	}
}
