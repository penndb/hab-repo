/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.provenance.marshalling;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;

import edu.upenn.cis.db.habitat.core.api.StorageApi;
import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.StructuredValue;
import edu.upenn.cis.db.habitat.util.ObjectSerialization;
public class MarshalObject<T> implements StorageApi.ObjectSerializer<T, 
StructuredData<String,Object>> {
	final static Logger logger = LogManager.getLogger(MarshalObject.class);
	
	final Class<T> theClass;
	final String propertyName;
	
	public MarshalObject(String propertyName, Class<T> theClass) {
		this.theClass = theClass;
		this.propertyName = propertyName;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T getObject(StructuredData<String, Object> serializedObject) {
		T ret = null;
		try {
//			ret = jsonMapper.readValue(serializedObject.get(propertyName), theClass);
			ret = (T)ObjectSerialization.getObject(serializedObject.get(propertyName));
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("Could not de-serialize prov location: " + e.getMessage());
		}
		
		return ret;
	}

	@Override
	public StructuredData<String, Object> getSerializedObject(T object) {
		StructuredData<String,Object> ret = new StructuredValue<>();
		
		// JSONify
		try {
			ret.put(propertyName, ObjectSerialization.putObject(object));//jsonMapper.writeValueAsString(object));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			logger.error("Could not serialize " + object + ": " + e.getMessage());
		}
		
		return ret;
	}

}