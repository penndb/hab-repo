/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.repository.prov.parser.data;

public class AlternateOf {
	private String alternate1;
	private String alternate2;
	public String getAlternate1() {
		return alternate1;
	}
	public void setAlternate1(String alternate1) {
		this.alternate1 = alternate1;
	}
	public String getAlternate2() {
		return alternate2;
	}
	public void setAlternate2(String alternate2) {
		this.alternate2 = alternate2;
	}
}
