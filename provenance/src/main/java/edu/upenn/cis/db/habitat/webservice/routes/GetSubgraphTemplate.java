/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import static com.google.common.base.Preconditions.checkNotNull;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.RESOURCE_QUERY_PARAM;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.base.Stopwatch;

import edu.upenn.cis.db.habitat.core.api.ProvenanceGraphApi;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;
import spark.Request;
import spark.Response;
import spark.Service;

/**
 * @author John Frommeyer
 *
 */
public class GetSubgraphTemplate extends GetSubgraphTemplateWsMethod
		implements RouteWithInfo {

	private final Logger logger = LogManager.getLogger(getClass());
	private final ProvenanceGraphApi backend;
	private final Service spark;

	public GetSubgraphTemplate(ProvenanceGraphApi backend,
			Service spark) {
		super();
		this.backend = checkNotNull(backend);
		this.spark = checkNotNull(spark);
	}

	public SubgraphTemplate handle(
			Request request,
			Response response) throws Exception {
		final String m = "handle(...)";
		response.header("Content-Encoding", "gzip");
		final String resource = getPathParam(
				spark,
				request,
				RESOURCE_QUERY_PARAM);

		final SubgraphTemplate template = getSubgraphTemplate(
				resource);
		if (template == null) {
			throw halt(spark, HttpServletResponse.SC_NOT_FOUND,
					new ResponseError("no subgraph template for %s", resource));
		}
		response.status(HttpServletResponse.SC_OK);
		return template;
	}

	@Override
	public SubgraphTemplate getSubgraphTemplate(String resource)
			throws Exception {
		final String m = "getSubgraphTemplate(...)";
		final Stopwatch stopwatch = Stopwatch.createStarted();
		final SubgraphTemplate template = backend.getSubgraphTemplate(resource);
		stopwatch.stop();
		logger.info("{}: Retrieved subgraph template for graph [{}]: {}", m,
				resource, stopwatch);
		return template;
	}

}
