/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.provenance;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.base.Stopwatch;
import com.google.gson.Gson;
import com.google.inject.Inject;

import edu.upenn.cis.db.habitat.core.api.ProvenanceGraphApi;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.StructuredValue;
import edu.upenn.cis.db.habitat.core.type.TransactionSupplier;
import edu.upenn.cis.db.habitat.core.type.model.ProvTokenModel;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance.LinkInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance.NodeInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance.RankInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.repository.storage.GraphStore;

/**
 * Implementation of the ProvenanceGraphApi
 * 
 * @author John Frommeyer
 *
 */
public class ProvenanceGraphStore extends ProvenanceStore implements
		ProvenanceGraphApi {

	public final static String SUBGRAPH_TEMPLATE_KEY = "subgraph-template.json";

	private final static Gson gson = new Gson();

	private final Logger logger = LogManager.getLogger(getClass());

	private final class StoreSubgraphFunction implements
			Function<TransactionSupplier, Boolean> {

		private final String resourceName;
		private final SubgraphInstance subgraph;
		private HabitatServiceException habitatServiceException;

		public StoreSubgraphFunction(String resourceName,
				SubgraphInstance subgraph) {
			this.resourceName = checkNotNull(resourceName);
			this.subgraph = checkNotNull(subgraph);
		}

		/**
		 * Returns the current HabitatServiceException if last call to apply
		 * raised one and returned false. Otherwise this will throw an
		 * IllegalStateException.
		 * 
		 * @return
		 */
		public HabitatServiceException getHabitatServiceException() {
			checkState(habitatServiceException != null);
			return habitatServiceException;
		}

		@Override
		public Boolean apply(TransactionSupplier transactionSupplier) {
			try {
				for (final RankInstance rankInstance : subgraph
						.getRanks()) {
					for (final NodeInstance nodeInstance : rankInstance.nodes) {
						ProvenanceGraphStore.this
								.storeProvenanceNode(
										resourceName,
										ProvTokenModel.toProvToken(nodeInstance
												.getId()),
										nodeInstance.getTuple()
												.toTupleWithSchema(),
										new ProvLocation(),
										transactionSupplier);
					}
				}

				for (final LinkInstance linkInstance : subgraph
						.getLinks()) {
					ProvenanceGraphStore.this.storeProvenanceLink(
							resourceName,
							ProvTokenModel.toProvToken(linkInstance
									.getSourceId()),
							linkInstance.getType(),
							ProvTokenModel.toProvToken(linkInstance
									.getTargetId()),
							linkInstance.getTuple().toTupleWithSchema(),
							transactionSupplier);
				}
			} catch (HabitatServiceException e) {
				this.habitatServiceException = e;
				return Boolean.FALSE;
			}
			this.habitatServiceException = null;
			return Boolean.TRUE;
		}
	};

	private final GraphStore<String> graphStore;

	@Inject
	public ProvenanceGraphStore(GraphStore<String> graphStore) {
		super(graphStore);
		this.graphStore = graphStore;
	}

	@Override
	public void createProvenanceGraph(String resourceName)
			throws HabitatServiceException {
		graphStore.createGraph(resourceName);
	}

	@Override
	public void createOrResetProvenanceGraph(String resourceName)
			throws HabitatServiceException {
		graphStore.createOrResetGraph(resourceName);
	}

	@Override
	public List<SubgraphInstance> getSubgraphs(
			String resourceName,
			SubgraphTemplate template,
			int limit,
			Long since) throws HabitatServiceException {
		final String m = "getSubgraphs(...)";
		final Stopwatch stopwatch = Stopwatch.createStarted();
		final Iterable<SubgraphInstance> subgraphs = graphStore.getSubgraphs(
				resourceName,
				template,
				limit,
				since);
		final List<SubgraphInstance> subgraphList = StreamSupport.stream(
				subgraphs.spliterator(), true).collect(
						Collectors.toList());
		stopwatch.stop();
		logger.info("{}: Returning {} subgraphs: {}", m, subgraphList.size(),
				stopwatch);
		return subgraphList;
	}

	@Override
	public void storeSubgraph(
			String resourceName,
			SubgraphInstance subgraph) throws HabitatServiceException {
		final String m = "storeSubgraph(...)";
		final Stopwatch stopwatch = Stopwatch.createStarted();
		final StoreSubgraphFunction storeSubgraphFn = new StoreSubgraphFunction(
				resourceName,
				subgraph);
		final boolean result = graphStore.execTransaction(resourceName,
				storeSubgraphFn);
		stopwatch.stop();
		logger.info("{}: Stored subgraph to graph [{}]: {}", m, resourceName,
				stopwatch);
		if (!result) {
			throw storeSubgraphFn.getHabitatServiceException();
		}

	}

	@Override
	public void storeSubgraphTemplate(String resourceName,
			SubgraphTemplate template)
			throws HabitatServiceException {
		final String m = "storeSubgraphTemplate(...)";
		final byte[] templateBytes = gson.toJson(template)
				.getBytes(StandardCharsets.UTF_8);
		final ByteArrayInputStream templateStream = new ByteArrayInputStream(
				templateBytes);

		store.storeFile(resourceName,
				SUBGRAPH_TEMPLATE_KEY,
				templateStream,
				templateBytes.length,
				new StructuredValue<>());
		logger.info("{}: Stored template {} for graph {}", m,
				SUBGRAPH_TEMPLATE_KEY,
				resourceName);

	}

	@Override
	public SubgraphTemplate getSubgraphTemplate(String resourceName)
			throws HabitatServiceException {
		final String m = "getSubgraphTemplate(...)";
		final String templateString = (String) store.getData(resourceName,
				SUBGRAPH_TEMPLATE_KEY);
		if (templateString == null) {
			logger.info("{}: No template found for graph {}", m,
					resourceName);
			return null;
		}
		final SubgraphTemplate template = gson.fromJson(templateString, SubgraphTemplate.class);
		logger.info("{}: Returning template found for graph {}", m,
				resourceName);
		return template;
	}

}
