/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.provenance.provdm;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Set;

import spark.Service;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;

import edu.upenn.cis.db.habitat.core.api.ProvDmApi;
import edu.upenn.cis.db.habitat.core.type.model.ProvSpecifierTypeAdapterFactory;
import edu.upenn.cis.db.habitat.core.type.provdm.AttributeTypeAdapterFactory;
import edu.upenn.cis.db.habitat.core.webservice.RestPlugin;
import edu.upenn.cis.db.habitat.core.webservice.RouteSpecifier;
import edu.upenn.cis.db.habitat.webservice.routes.ExceptionHandlingRoute;
import edu.upenn.cis.db.habitat.webservice.routes.RouteWithInfo;
import edu.upenn.cis.db.habitat.webservice.routes.provdm.StoreNode;
import edu.upenn.cis.db.habitat.webservice.routes.provdm.StoreRelation;

public class ProvDmServices implements RestPlugin {

	private final Set<RouteWithInfo> operations;
	private final ProvDmApi prov;
	private final Service spark;
	private final Gson gson = new GsonBuilder()
			.registerTypeAdapterFactory(new AttributeTypeAdapterFactory())
			.registerTypeAdapterFactory(new ProvSpecifierTypeAdapterFactory())
			.create();

	@Inject
	public ProvDmServices(ProvDmApi storage, Service spark) {
		this.spark = checkNotNull(spark);
		prov = checkNotNull(storage);
		this.operations = ImmutableSet.of(
				new StoreNode(prov, this.spark, gson),
				new StoreRelation(prov, this.spark, gson));
	}

	@Override
	public Multimap<String, RouteSpecifier> getSpecifiers() {
		Multimap<String, RouteSpecifier> ret = HashMultimap.create();
		for (final RouteWithInfo operation : operations) {
			ret.put(operation.getSparkSubpath(),
					new RouteSpecifier(operation.getRequestType(),
							operation.isSecure(),
							new ExceptionHandlingRoute(spark, operation)));
		}
		return ret;
	}
}
