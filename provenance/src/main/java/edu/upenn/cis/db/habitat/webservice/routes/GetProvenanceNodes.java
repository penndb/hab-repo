/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import static com.google.common.base.Preconditions.checkNotNull;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.RESOURCE_QUERY_PARAM;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import spark.Request;
import spark.Response;
import spark.Service;
import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.model.ProvNodeMapModel;

/**
 * @author John Frommeyer
 *
 */
public class GetProvenanceNodes extends GetProvenanceNodesWsMethod
		implements RouteWithInfo {

	private final Logger logger = LogManager.getLogger(getClass());
	private final ProvenanceApi backend;
	private final Service spark;

	public GetProvenanceNodes(ProvenanceApi backend,
			Service spark) {
		super();
		this.backend = checkNotNull(backend);
		this.spark = checkNotNull(spark);
	}

	public ProvNodeMapModel handle(
			Request request,
			Response response) throws Exception {
		final String m = "handle(...)";
		response.header("Content-Encoding", "gzip");
		final String resource = getPathParam(
				spark,
				request,
				RESOURCE_QUERY_PARAM);
		final ProvNodeMapModel nodes = getProvenanceNodes(resource);
		response.status(HttpServletResponse.SC_OK);
		return nodes;
	}

	@Override
	public ProvNodeMapModel getProvenanceNodes(String resource)
			throws Exception {
		final String m = "getProvenanceNodes(...)";
		final Map<String, StructuredData<String, Object>> nodes = backend
				.getProvenanceNodes(resource);
		logger.debug("{}: Retrieved ProvTokens for resource: [{}]", m, resource);
		final ProvNodeMapModel nodesModel = ProvNodeMapModel.from(nodes);
		return nodesModel;
	}
}
