/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.repository.prov.serializers;

import static org.neo4j.driver.v1.Values.parameters;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;

import edu.upenn.cis.db.habitat.repository.prov.parser.Parser;
import edu.upenn.cis.db.habitat.repository.prov.parser.data.ActedOnBehalfOf;
import edu.upenn.cis.db.habitat.repository.prov.parser.data.Activity;
import edu.upenn.cis.db.habitat.repository.prov.parser.data.Agent;
import edu.upenn.cis.db.habitat.repository.prov.parser.data.AlternateOf;
import edu.upenn.cis.db.habitat.repository.prov.parser.data.Doc;
import edu.upenn.cis.db.habitat.repository.prov.parser.data.Entity;
import edu.upenn.cis.db.habitat.repository.prov.parser.data.SpecializationOf;
import edu.upenn.cis.db.habitat.repository.prov.parser.data.Used;
import edu.upenn.cis.db.habitat.repository.prov.parser.data.WasAssociatedWith;
import edu.upenn.cis.db.habitat.repository.prov.parser.data.WasAttributedTo;
import edu.upenn.cis.db.habitat.repository.prov.parser.data.WasDerivedFrom;
import edu.upenn.cis.db.habitat.repository.prov.parser.data.WasGeneratedBy;
import edu.upenn.cis.db.habitat.repository.storage.neo4j.Neo4JStore;

public class Neo4JProv {
	Neo4JStore store;
	final static Logger logger = LogManager.getLogger(Neo4JProv.class);
	
	public Neo4JProv(Neo4JStore store) {
		this.store = store;
	}
	
    public void insert(String path) {
    	insert(Parser.parse(path));
    }
    
    public void insert(Doc doc) {
        try (Session session = store.getSession()) {
        	
        	for(String a : doc.getAgentList().keySet()){
        		
        		Agent agent = doc.getAgentList().get(a);
        		session.run( "CREATE (agent:Agent {id: {id}, name: {name}, type: {type}, givenName: {givenName}, mbox: {mbox}}) "+ 
        					 "RETURN agent",
        		        parameters( "id", agent.getId(), "name", agent.getName(), "type", agent.getType(),
        		        		"givenName", agent.getGivenName(), "mbox", agent.getMbox() ) );
        		
        
        	}
        	
        	for(String a : doc.getActivityList().keySet()){
        		Activity activity = doc.getActivityList().get(a);
        		session.run( "CREATE (activity:Activity {id: {id}, startTime: {startTime}, endTime: {endTime}}) " + 
        					 "RETURN activity" ,
        		        parameters( "id", activity.getId(), "startTime", activity.getStartTime(), "endTime", 
        		        		activity.getEndTime()) );
        	}
        	
        	for(String a : doc.getEntityList().keySet()){
        		Entity entity = doc.getEntityList().get(a);
        		session.run( "CREATE (entity:Entity {id: {id}, title: {title}, type: {type}}) "+
        					 "RETURN entity",
        		        parameters( "id", entity.getId(), "title", entity.getTitle(), "type", entity.getType()) );
        	}
        	  
        	for(Used u : doc.getUsedList()){
        		String query = "MATCH (e:Entity), (a:Activity)"+
        						"WHERE e.id='" + u.getEntity() + "' AND a.id='" + u.getActivity() + "'" +
        						"CREATE (e)-[used:USED{role: \"" + u.getRole() + "\"}]->(a)"+
        						"RETURN e,used,a";
        		session.run(query);
        	}
        	
        	for(WasGeneratedBy w : doc.getWasGeneratedByList()){
        		String query = "MATCH (e:Entity), (a:Activity)"+
        						"WHERE e.id ='"+ w.getEntity() + "' AND a.id ='" + w.getActivity() + "'" +
								"CREATE (e)-[was:WAS_GENERATED_BY{time: \""+ w.getTime()+"\", role: \""+ w.getRole()+"\"}]->(a)"+
								"RETURN e,was,a";
        		session.run(query);
        	}
        	
        	for(WasDerivedFrom w : doc.getWasDerivedFromList()){
        		String query = "MATCH (e:Entity), (u:Entity)" +
								"WHERE e.id='" + w.getUsedEntity() + "' AND u.id='" + w.getGeneratedEntity() +"'" +
        						"CREATE (e)-[was:WAS_DERIVED_FROM{type: \""+ w.getType()+"\"}]->(u)"+
								"RETURN e,was,u";
        		session.run(query);
        	}
        	
        	for(WasAttributedTo w : doc.getWasAttributedToList()){
        		String query = "MATCH (e:Entity), (a:Agent)"+
								"WHERE e.id='" + w.getEntity() + "' AND a.id ='" + w.getAgent() + "'" +
        						"CREATE (e)-[was:WAS_ATTRIBUTED_TO]->(a)"+
								"RETURN e,was,a";
        		session.run(query);
        	}
        	
        	for(ActedOnBehalfOf a : doc.getActedOnBehalfOfList()){
        		String query = "MATCH (e:Agent), (a:Agent)"+
								"WHERE e.id='" + a.getResponsible() + "' AND a.id='" + a.getDelegate() + "'" +
        						"CREATE (e)-[was:ACTED_ON_BEHALF_OF]->(a)"+
								"RETURN e,was,a";
        		session.run(query);
        	}
        	
        	for(WasAssociatedWith w : doc.getWasAssociatedWithList()){
        		String query = "MATCH (e:Activity), (a:Agent)"+
								"WHERE e.id ='" + w.getActivity() + "' AND a.id='" + w.getAgent() +"'"+
        						"CREATE (e)-[was:WAS_ASSOCIATED_WITH{plan: \""+ w.getPlan()+"\", role: \""+ w.getRole()+"\"}]->(a)"+
								"RETURN e,was,a";
        		session.run(query);
        	}
        	
        	for(SpecializationOf a : doc.getSpecializationOfList()){
        		String query = "MATCH (e:Entity), (a:Entity)"+
								"WHERE e.id='" + a.getSpecificEntity() + "' AND a.id='" + a.getGeneralEntity() +"'" + 
        						"CREATE (e)-[was:SPECIALIZATION_OF]->(a)"+
								"RETURN e,was,a";
        		session.run(query);
        	}
        	
        	for(AlternateOf a : doc.getAlternateOfList()){
        		String query = "MATCH (e:Entity {id:\""+ a.getAlternate1() + "\"}), (a:Entity {id:\""+ a.getAlternate2()+"\"})"+
								"WHERE e.id='"+a.getAlternate1() + "' AND a.id='" + a.getAlternate2() +"'"+
								"CREATE (e)-[was:ALTERNATE_OF]->(a)"+
								"RETURN e,was,a";
        		session.run(query);
        	}
               //session.close();
               //driver.close();
      
               

        }
    }
    
    public static Supplier<List<Map<String,Object>>> getQueryResultsAsMap(Session session, 
    		String query) {
    	return () -> {
    		StatementResult result;
    		result = session.run(query);

    		List<Map<String,Object>> ret = new ArrayList<>();
    		while (result.hasNext()){
    			Record record = result.next();
    			ret.add(record.asMap());
    		}

    		return ret;
    	};
    }

    public String query(){
    	logger.info("Handling request to display content");
    	
        try (Session session = store.getSession()) {
        	return HtmlProv.getDisplay(
        			getQueryResultsAsMap(session,
	        				"MATCH (a:Agent) " +
	        						"RETURN a.id AS id, a.name AS name, a.type AS type, a.givenName AS givenName, "
	        						+ "a.getMbox AS mbox"), 
        			getQueryResultsAsMap(session,
	        				"MATCH (e:Entity) " +
	        	                    "RETURN e.id AS id, e.title AS title, e.type AS type"),
        			getQueryResultsAsMap(session,
        					"MATCH (a:Activity) " +
	        	                    "RETURN a.id AS id, a.startTime AS startime, a.endTime AS endtime"),
        			getQueryResultsAsMap(session,
        					"MATCH (a:WAS_ASSOCIATED_WITH) " +
        		                    "RETURN a.id AS id, a.role AS role, a.plan AS plan")
	    	).toString();
        }
    }

    // This is wrong
//    public void reconstruct(String fileName){
//        StatementResult result;
//        StringBuilder out = new StringBuilder();
//
//        try{
//    	    PrintWriter writer = new PrintWriter(fileName, "UTF-8");
//    	    writer.println("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
//    	    writer.println("<prov:document xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"");
//    	    writer.println("\t"+ "xmlns:prov=\"http://www.w3.org/ns/prov#\" xmlns:exc=\"http://www.example.org/\"");
//    	    writer.println("\t" + "xmlns:ex=\"http://example.org/\" xmlns:dct=\"http://purl.org/dc/terms/");
//    	    writer.println("\t" + "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:foaf=\"http://xmlns.com/foaf/0.1/\">");
//            
//    	    try (Session session = store.getSession()) {
//            	String foafQuery = 
//              		   "MATCH (a:Agent) " +
//                                 "RETURN a.id AS id, a.name AS name, a.type AS type, a.givenName AS givenName, "
//                                 + "a.getMbox AS mbox";
//            	result = session.run(foafQuery);
//        		
//        		
//                while (result.hasNext()){
//             	   Record record = result.next();
//             	  out.append("<tr>");
//             	  out.append("<td>"+ record.get("id").asString() +"</td>");
//             	  out.append("<td>"+ record.get("name").asString() +"</td>");
//             	  out.append("<td>"+ record.get("type").asString() +"</td>");
//             	  out.append("<td>"+ record.get("givenName").asString() +"</td>");
//             	  out.append("<td>"+ record.get("mbox").asString() +"</td>");
//             	  out.append("</tr>");
//                }
//                
//                out.append("</table></center>");
//                
//                foafQuery = "MATCH (e:Entity) " +
//                        "RETURN e.id AS id, e.title AS title, e.type AS type";
//                
//                
//                out.append("<br><br><center><b>Entity<b></center><br><br>");
//        		out.append("<center><table style=\"width:100%\">\n");
//        		out.append("<table><tr>");
//        		out.append("<br><br>");
//        		out.append("<th>ID</th>");
//        		out.append("<th>Title</th>");
//        		out.append("<th>Type</th>");
//        		out.append("</tr>");
//        		
//        		result = session.run(foafQuery,parameters( "name", "Derek" ) );
//                
//        		while (result.hasNext()){
//             	   Record record = result.next();
//             	  out.append("<tr>");
//             	  out.append("<td>"+ record.get("id").asString() +"</td>");
//             	  out.append("<td>"+ record.get("title").asString() +"</td>");
//             	  out.append("<td>"+ record.get("type").asString() +"</td>");
//             	  out.append("</tr>");
//                }
//        		
//        		out.append("</table></center>");
//        		//Activity
//        		foafQuery = "MATCH (a:Activity) " +
//                        "RETURN a.id AS id, a.startTime AS startime, a.endTime AS endtime";
//                
//                
//                out.append("<br><br><center><b>Activity<b></center><br><br>");
//        		out.append("<center><table style=\"width:100%\">\n");
//        		out.append("<table><tr>");
//        		out.append("<br><br>");
//        		out.append("<th>ID</th>");
//        		out.append("<th>StartTime</th>");
//        		out.append("<th>EndTime</th>");
//        		out.append("</tr>");
//        		
//        		result = session.run(foafQuery,parameters( "name", "Derek" ) );
//                
//        		while (result.hasNext()){
//             	   Record record = result.next();
//             	  out.append("<tr>");
//             	  out.append("<td>"+ record.get("id").asString() +"</td>");
//             	  out.append("<td>"+ record.get("starttime").asString() +"</td>");
//             	  out.append("<td>"+ record.get("endtime").asString() +"</td>");
//             	  out.append("</tr>");
//                }
//        		out.append("</table></center>");
//        		
//        		
//        		
//        		foafQuery = "MATCH (a:WAS_ASSOCIATED_WITH) " +
//                        "RETURN a.id AS id, a.role AS role, a.plan AS plan";
//                
//                
//                out.append("<br><br><center><b>Activity<b></center><br><br>");
//        		out.append("<center><table style=\"width:100%\">\n");
//        		out.append("<table><tr>");
//        		out.append("<br><br>");
//        		out.append("<th>ID</th>");
//        		out.append("<th>StartTime</th>");
//        		out.append("<th>EndTime</th>");
//        		out.append("</tr>");
//        		
//        		result = session.run(foafQuery);
//                
//        		while (result.hasNext()){
//             	   Record record = result.next();
//             	  out.append("<tr>");
//             	  out.append("<td>"+ record.get("id").asString() +"</td>");
//             	  out.append("<td>"+ record.get("starttime").asString() +"</td>");
//             	  out.append("<td>"+ record.get("endtime").asString() +"</td>");
//             	  out.append("</tr>");
//                }
//        		out.append("</table></center>");
//            }
//    	    writer.println("");
//    	    writer.println("");
//    	    writer.println("");
//    	    writer.println("");
//    	    writer.println("");
//    	    writer.println("");
//    	    writer.println("");
//    	    writer.println("");
//    	    writer.println("");
//    	    writer.println("");
//    	    writer.println("");
//    	    writer.println("");
//    	    writer.println("");
//    	    writer.println("");
//    	    writer.println("");
//    	    
//    	    
//    	    writer.close();
//    	} catch (IOException e) {
//    	   // do something
//    	}
//	}


}
