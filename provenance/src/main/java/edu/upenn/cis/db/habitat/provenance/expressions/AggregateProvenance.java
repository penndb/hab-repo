/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.provenance.expressions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import edu.upenn.cis.db.habitat.core.type.NaryProvExpression;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;

public class AggregateProvenance implements ProvenanceAggregator {
	
	@Override
	public ProvSpecifier apply(List<ProvSpecifier> u) {
		List<ProvSpecifier> items = new ArrayList<ProvSpecifier>();
		
		items.addAll(u);
		Collections.sort(items);
		
		return new NaryProvExpression(ProvSpecifier.ProvOperation.PROV_MULT, items);
	}
}
