/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes.provdm;

import static com.google.common.base.Preconditions.checkNotNull;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.RESOURCE_PATH_PARAM;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.LABEL_PATH_PARAM;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import spark.Request;
import spark.Response;
import spark.Service;

import com.google.gson.Gson;

import edu.upenn.cis.db.habitat.core.api.ProvDmApi;
import edu.upenn.cis.db.habitat.core.type.provdm.RelationModel;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;
import edu.upenn.cis.db.habitat.webservice.routes.RouteWithInfo;

/**
 * @author John Frommeyer
 *
 */
public class StoreRelation extends StoreRelationWsMethod
		implements
		RouteWithInfo {

	private final Logger logger = LogManager.getLogger(getClass());
	private final ProvDmApi backend;
	private final Service spark;
	private final Gson gson;

	public StoreRelation(ProvDmApi backend, Service spark, Gson gson) {
		this.backend = checkNotNull(backend);
		this.spark = checkNotNull(spark);
		this.gson = checkNotNull(gson);
	}

	@Override
	public Object handle(
			Request request,
			Response response) throws Exception {
		final String m = "handle(...)";

		final String resource = getPathParam(spark, request,
				RESOURCE_PATH_PARAM);
		logger.debug("{}: Got resource: {}", m, resource);

		final String label = getPathParam(spark, request,
				LABEL_PATH_PARAM);
		logger.debug("{}: Got label: {}", m, resource);

		final String jsonBody = request.body();
		logger.debug("{}: Got body {}", m, jsonBody);

		final RelationModel body = gson.fromJson(jsonBody,
				RelationModel.class);
		validateBody(body);
		storeRelation(resource, body, label);
		response.status(HttpServletResponse.SC_NO_CONTENT);

		return null;

	}

	@Override
	public void storeRelation(String resource,
			RelationModel relation, String label)
			throws Exception {
		backend.storeRelation(resource, relation, label);
	}

	private void validateBody(RelationModel body) {
		if (body.getType() == null) {
			throw halt(spark, HttpServletResponse.SC_BAD_REQUEST,
					new ResponseError("missing relation"));
		}
		if (body.getSubjectId() == null) {
			throw halt(spark, HttpServletResponse.SC_BAD_REQUEST,
					new ResponseError("missing subject ID"));
		}
		if (body.getObjectId() == null) {
			throw halt(spark, HttpServletResponse.SC_BAD_REQUEST,
					new ResponseError("missing object token"));
		}
	}
}
