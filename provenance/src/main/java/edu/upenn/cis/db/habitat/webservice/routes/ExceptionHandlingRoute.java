/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import spark.HaltException;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Service;

import com.google.gson.JsonSyntaxException;

import edu.upenn.cis.db.habitat.core.webservice.HabitatSecurityException;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;
import edu.upenn.cis.db.habitat.provenance.NoSuchProvTokenException;

/**
 * @author John Frommeyer
 *
 */
public class ExceptionHandlingRoute implements Route {

	private final Route wrappedRoute;
	private final Service spark;
	private final Logger logger = LogManager.getLogger(getClass());

	public ExceptionHandlingRoute(Service spark, Route wrappedRoute) {
		this.spark = checkNotNull(spark);
		this.wrappedRoute = checkNotNull(wrappedRoute);
	}

	@Override
	public Object handle(Request request, Response response) throws Exception {
		final String m = "handle(...)";
		try {
			return wrappedRoute.handle(request, response);
		} catch (NoSuchProvTokenException e) {
			logger.info(m + ": returning NOT FOUND");
			throw halt(HttpServletResponse.SC_NOT_FOUND,
					e,
					"Not found");
		} catch (HabitatSecurityException e) {
			logger.error(m + ": returning UNAUTHORIZED", e);
			throw halt(HttpServletResponse.SC_UNAUTHORIZED,
					e,
					"Unauthorized");
		} catch (HabitatServiceException | JsonSyntaxException e) {
			logger.error(m + ": returning BAD REQUEST", e);
			throw halt(HttpServletResponse.SC_BAD_REQUEST,
					e,
					"Bad request");
		} catch (HaltException e) {
			logger.error(m + ": halt error: " + e.body(), e);
			throw e;
		} catch (RuntimeException e) {
			logger.error(m + ": returning INTERNAL SERVICE ERROR", e);
			throw halt(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					e,
					"Internal service error");
		}

	}

	private HaltException halt(int code, Exception e, String defaultMsg) {
		final ResponseError errorBody = toResponseError(e, defaultMsg);
		return RouteWithInfo.haltWithResponseError(spark, code, errorBody);
	}

	private static ResponseError toResponseError(Exception e, String defaultMsg) {
		checkNotNull(defaultMsg);
		final String exceptionMsg = e.getMessage();
		final String msg = exceptionMsg == null
				|| exceptionMsg.trim().isEmpty() ? defaultMsg : exceptionMsg;
		return new ResponseError(msg);
	}
}
