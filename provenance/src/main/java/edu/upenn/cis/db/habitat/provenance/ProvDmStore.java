/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.provenance;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.emptyList;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.annotation.Nullable;

import com.google.common.annotations.VisibleForTesting;
import com.google.inject.Inject;

import edu.upenn.cis.db.habitat.core.api.ProvDmApi;
import edu.upenn.cis.db.habitat.core.api.ProvenanceGraphApi;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.StringField;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.model.ProvSpecifierModel;
import edu.upenn.cis.db.habitat.core.type.provdm.Attribute;
import edu.upenn.cis.db.habitat.core.type.provdm.NodeModel;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvDmConstants;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvDmRelation;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvDmRelation.AdditionalProperty;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvQualifiedNameToken;
import edu.upenn.cis.db.habitat.core.type.provdm.QualifiedName;
import edu.upenn.cis.db.habitat.core.type.provdm.RelationModel;
import edu.upenn.cis.db.habitat.core.type.provdm.StringAttribute;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * @author John Frommeyer
 *
 */
public class ProvDmStore implements ProvDmApi {

	@VisibleForTesting
	public static final String PROV_DM_TYPE_KEY = ProvDmConstants.PROV_DM_TYPE_QN
			.toString();
	@VisibleForTesting
	public static final String PROV_DM_TIME_KEY = RelationModel.PROV_DM_TIME_QN
			.toString();
	@VisibleForTesting
	public static final String PROV_DM_RELATION_ID_KEY = RelationModel.PROV_DM_RELATION_ID_QN
			.toString();
	@VisibleForTesting
	public static final String PROV_DM_GENERATION_ID_KEY = RelationModel.PROV_DM_GENERATION_ID_QN
			.toString();
	@VisibleForTesting
	public static final String PROV_DM_USAGE_ID_KEY = RelationModel.PROV_DM_USAGE_ID_QN
			.toString();

	private final ProvenanceGraphApi provApi;

	@Inject
	public ProvDmStore(ProvenanceGraphApi provApi) {
		this.provApi = provApi;
	}

	@Override
	public void createProvenanceGraph(String graphName)
			throws HabitatServiceException {
		final String resource = validateName(graphName);
		provApi.createProvenanceGraph(resource);
	}

	@Override
	public void createOrResetProvenanceGraph(String graphName)
			throws HabitatServiceException {
		final String resource = validateName(graphName);
		provApi.createOrResetProvenanceGraph(resource);
	}
	
	@Override
	public void annotateNode(String graph,
			QualifiedName sourceId,
			QualifiedName annotId,
			QualifiedName key,
			String value) throws HabitatServiceException {
		final NodeModel nodeModel = NodeModel.newAnnotation(key, value);
		storeNode(graph, annotId, nodeModel);
		
		final RelationModel relationModel = RelationModel
				.newWasAnnotatedBy(annotId, sourceId, key, value);
		storeRelation(graph, relationModel, ProvDmRelation.ANNOTATED.getProvNRelation());
	}
//	
//	@Override
//	public Serializable getNodeAnnotation(String graph,
//			QualifiedName provId,
//			String key) throws HabitatServiceException {
//		checkNotNull(key);
//		final String resource = validateName(graph);
//		final ProvQualifiedNameToken idToken = new ProvQualifiedNameToken(
//				provId);
//		return provApi.getProvenanceNodeAnnotation(resource, idToken, key);
//	}
	

	@Override
	public void storeNode(String graph,
			QualifiedName provId,
			NodeModel nodeModel) throws HabitatServiceException {
		checkNotNull(nodeModel);
		final String resource = validateName(graph);
		final ProvQualifiedNameToken idToken = new ProvQualifiedNameToken(
				provId);
		final TupleWithSchema<String> tuple = nodeModel.toTupleWithSchema();
		final ProvSpecifier specifier = nodeModel.getLocation() == null
				? new ProvLocation()
				: nodeModel.getLocation().toProvSpecifier();
		provApi.storeProvenanceNode(resource, idToken, tuple, specifier);

	}

	@Override
	public void entity(String graph,
			QualifiedName provId,
			List<Attribute> attributes,
			ProvSpecifierModel location)
			throws HabitatServiceException {
		final NodeModel nodeModel = NodeModel.newEntity(attributes, location);
		storeNode(graph, provId, nodeModel);
	}

	@Override
	public void entity(String graph,
			QualifiedName provId,
			ProvSpecifierModel location)
			throws HabitatServiceException {
		entity(graph, provId, emptyList(), location);
	}

	@Override
	public void collection(String graph,
			QualifiedName provId,
			List<Attribute> attributes,
			ProvSpecifierModel location)
			throws HabitatServiceException {
		final NodeModel nodeModel = NodeModel.newCollection(attributes, location);
		storeNode(graph, provId, nodeModel);
	}

	@Override
	public void collection(String graph,
			QualifiedName provId,
			ProvSpecifierModel location)
			throws HabitatServiceException {
		collection(graph, provId, emptyList(), location);
	}
	
	@Override
	public void activity(String graph,
			QualifiedName provId,
			List<Attribute> attributes,
			Date startTime,
			Date endTime,
			ProvSpecifierModel location)
			throws HabitatServiceException {
		final NodeModel nodeModel = NodeModel.newActivity(
				attributes,
				startTime,
				endTime,
				location);
		storeNode(graph, provId, nodeModel);
	}

	@Override
	public void activity(String graph,
			QualifiedName provId,
			Date startTime,
			Date endTime,
			ProvSpecifierModel location)
			throws HabitatServiceException {
		activity(graph, provId, emptyList(), startTime, endTime, location);
	}

	@Override
	public void agent(String graph,
			QualifiedName provId,
			List<Attribute> attributes,
			ProvSpecifierModel location)
			throws HabitatServiceException {
		final NodeModel nodeModel = NodeModel.newAgent(attributes, location);
		storeNode(graph, provId, nodeModel);
	}

	@Override
	public void agent(String graph,
			QualifiedName provId,
			ProvSpecifierModel location)
			throws HabitatServiceException {
		agent(graph, provId, emptyList(), location);
	}

	@Override
	public void storeRelation(
			String graph,
			RelationModel relationModel,
			String label)
			throws HabitatServiceException {
		createRelation(
				graph,
				relationModel,
				label);
	}

	@Override
	public void wasGeneratedBy(
			String graph,
			QualifiedName entityId,
			QualifiedName activityId,
			QualifiedName relationId,
			Date generationTime,
			List<Attribute> attributes)
			throws HabitatServiceException {
		final RelationModel relationModel = RelationModel.newWasGeneratedBy(
				entityId,
				activityId,
				attributes,
				relationId,
				generationTime);
		storeRelation(graph, relationModel, ProvDmRelation.GENERATION.getProvNRelation());
	}

	@Override
	public void wasGeneratedBy(String graph,
			QualifiedName entityId,
			QualifiedName activityId,
			QualifiedName relationId,
			Date generationTime)
			throws HabitatServiceException {
		wasGeneratedBy(graph, entityId, activityId, relationId, generationTime,
				emptyList());
	}

	@Override
	public void used(String graph,
			QualifiedName activityId,
			QualifiedName entityId,
			QualifiedName relationId,
			Date usageTime,
			List<Attribute> attributes)
			throws HabitatServiceException {
		final RelationModel relationModel = RelationModel.newUsed(activityId, entityId,
				attributes, relationId, usageTime);
		storeRelation(graph, relationModel, ProvDmRelation.USAGE.getProvNRelation());
	}

	@Override
	public void used(String graph,
			QualifiedName activityId,
			QualifiedName entityId,
			QualifiedName relationId,
			Date usageTime) throws HabitatServiceException {
		used(graph, activityId, entityId, relationId, usageTime, emptyList());
	}

	@Override
	public void wasInformedBy(String graph,
			QualifiedName informedActivityId,
			QualifiedName informantActivityId,
			QualifiedName relationId,
			List<Attribute> attributes)
			throws HabitatServiceException {
		final RelationModel relationModel = RelationModel
				.newWasInformedBy(informedActivityId, informantActivityId,
						attributes, relationId);
		storeRelation(graph, relationModel, ProvDmRelation.COMMUNICATION.getProvNRelation());
	}

	@Override
	public void wasInformedBy(String graph,
			QualifiedName informedActivityId,
			QualifiedName informantActivityId,
			QualifiedName relationId)
			throws HabitatServiceException {
		wasInformedBy(graph, informedActivityId, informantActivityId,
				relationId, emptyList());
	}

	@Override
	public void wasStartedBy(String graph,
			QualifiedName startedActivityId,
			QualifiedName triggerEntityId,
			QualifiedName starterActivityId,
			QualifiedName relationId,
			Date time,
			List<Attribute> attributes)
			throws HabitatServiceException {
		final RelationModel relationModel = RelationModel.newWasStartedBy(
				startedActivityId, triggerEntityId, attributes,
				starterActivityId, relationId, time);
		storeRelation(graph, relationModel, ProvDmRelation.START.getProvNRelation());
	}

	@Override
	public void wasStartedBy(String graph,
			QualifiedName startedActivityId,
			QualifiedName triggerEntityId,
			QualifiedName starterActivityId,
			QualifiedName relationId,
			Date time) throws HabitatServiceException {
		wasStartedBy(
				graph,
				startedActivityId,
				triggerEntityId,
				starterActivityId,
				relationId,
				time,
				emptyList());
	}

	@Override
	public void wasStartedBy(String graph,
			QualifiedName startedActivityId,
			QualifiedName triggerEntityId,
			QualifiedName relationId,
			Date time,
			List<Attribute> attributes)
			throws HabitatServiceException {
		wasStartedBy(
				graph,
				startedActivityId,
				triggerEntityId,
				null,
				relationId,
				time,
				attributes);
	}

	@Override
	public void wasStartedBy(String graph,
			QualifiedName startedActivityId,
			QualifiedName triggerEntityId,
			QualifiedName relationId,
			Date time) throws HabitatServiceException {
		wasStartedBy(graph, startedActivityId,
				triggerEntityId, relationId,
				time, emptyList());
	}

	@Override
	public void wasEndedBy(String graph,
			QualifiedName endedActivityId,
			QualifiedName triggerEntityId,
			QualifiedName enderActivityId,
			QualifiedName relationId,
			Date time,
			List<Attribute> attributes)
			throws HabitatServiceException {
		final RelationModel relationModel = RelationModel.newWasEndedBy(endedActivityId,
				triggerEntityId, attributes, enderActivityId, relationId, time);
		storeRelation(graph, relationModel, ProvDmRelation.END.getProvNRelation());
	}

	@Override
	public void wasEndedBy(String graph,
			QualifiedName endedActivityId,
			QualifiedName triggerEntityId,
			QualifiedName enderActivityId,
			QualifiedName relationId,
			Date time) throws HabitatServiceException {
		wasEndedBy(
				graph,
				endedActivityId,
				triggerEntityId,
				enderActivityId,
				relationId,
				time,
				emptyList());
	}

	@Override
	public void wasEndedBy(String graph,
			QualifiedName endedActivityId,
			QualifiedName triggerEntityId,
			QualifiedName relationId,
			Date time,
			List<Attribute> attributes)
			throws HabitatServiceException {
		wasEndedBy(
				graph,
				endedActivityId,
				triggerEntityId,
				null,
				relationId,
				time,
				attributes);
	}

	@Override
	public void wasEndedBy(String graph,
			QualifiedName endedActivityId,
			QualifiedName triggerEntityId,
			QualifiedName relationId,
			Date time) throws HabitatServiceException {
		wasEndedBy(graph, endedActivityId,
				triggerEntityId, relationId,
				time, emptyList());
	}

	@Override
	public void wasInvalidatedBy(String graph,
			QualifiedName invalidatedEntityId,
			QualifiedName invalidatorActivityId,
			QualifiedName relationId,
			Date time,
			List<Attribute> attributes)
			throws HabitatServiceException {
		final RelationModel relationModel = RelationModel.newWasInvalidatedBy(
				invalidatedEntityId, invalidatorActivityId, attributes,
				relationId, time);
		storeRelation(graph, relationModel, ProvDmRelation.INVALIDATION.getProvNRelation());
	}

	@Override
	public void wasInvalidatedBy(String graph,
			QualifiedName invalidatedEntityId,
			QualifiedName invalidatorActivityId,
			QualifiedName relationId,
			Date time) throws HabitatServiceException {
		wasInvalidatedBy(graph, invalidatedEntityId, invalidatorActivityId,
				relationId, time, emptyList());
	}

	@Override
	public void wasDerivedFrom(String graph,
			QualifiedName generatedEntityId,
			QualifiedName usedEntityId,
			QualifiedName activityId,
			QualifiedName relationId,
			QualifiedName generationId,
			QualifiedName usageId,
			List<Attribute> attributes)
			throws HabitatServiceException {
		final RelationModel relationModel = RelationModel.newWasDerivedFrom(
				generatedEntityId, usedEntityId, attributes, activityId,
				relationId, generationId, usageId);
		storeRelation(graph, relationModel, ProvDmRelation.DERIVATION.getProvNRelation());
	}

	@Override
	public void wasDerivedFrom(String graph,
			QualifiedName generatedEntityId,
			QualifiedName usedEntityId,
			QualifiedName activityId,
			QualifiedName relationId,
			QualifiedName generationId,
			QualifiedName usageId) throws HabitatServiceException {
		wasDerivedFrom(graph,
				generatedEntityId,
				usedEntityId,
				activityId,
				relationId,
				generationId,
				usageId,
				emptyList());
	}

	@Override
	public void wasDerivedFrom(String graph,
			QualifiedName generatedEntityId,
			QualifiedName usedEntityId,
			QualifiedName relationId,
			QualifiedName generationId,
			QualifiedName usageId,
			List<Attribute> attributes)
			throws HabitatServiceException {
		wasDerivedFrom(graph,
				generatedEntityId,
				usedEntityId,
				null,
				relationId,
				generationId,
				usageId,
				attributes);
	}

	@Override
	public void wasDerivedFrom(String graph,
			QualifiedName generatedEntityId,
			QualifiedName usedEntityId,
			QualifiedName relationId,
			QualifiedName generationId,
			QualifiedName usageId)
			throws HabitatServiceException {
		wasDerivedFrom(graph,
				generatedEntityId,
				usedEntityId,
				null,
				relationId,
				generationId,
				usageId,
				emptyList());
	}

	@Override
	public void wasAttributedTo(String graph,
			QualifiedName entityId,
			QualifiedName agentId,
			QualifiedName relationId,
			List<Attribute> attributes)
			throws HabitatServiceException {
		final RelationModel relationModel = RelationModel.newWasAttributedTo(entityId,
				agentId, attributes, relationId);
		storeRelation(graph, relationModel, ProvDmRelation.ATTRIBUTION.getProvNRelation());
	}

	@Override
	public void wasAttributedTo(String graph,
			QualifiedName entityId,
			QualifiedName agentId,
			QualifiedName relationId)
			throws HabitatServiceException {
		wasAttributedTo(graph, entityId, agentId, relationId, emptyList());
	}

	@Override
	public void wasAssociatedWith(
			String graph,
			QualifiedName activityId,
			QualifiedName agentId,
			QualifiedName planId,
			QualifiedName relationId,
			List<Attribute> attributes)
			throws HabitatServiceException {
		final RelationModel relationModel = RelationModel.newWasAssociatedWith(
				activityId, agentId, attributes, planId, relationId);
		storeRelation(graph, relationModel, ProvDmRelation.ASSOCIATION.getProvNRelation());
	}

	@Override
	public void wasAssociatedWith(
			String graph,
			QualifiedName activityId,
			QualifiedName agentId,
			QualifiedName planId,
			QualifiedName relationId) throws HabitatServiceException {
		wasAssociatedWith(graph, activityId, agentId, planId, relationId,
				emptyList());
	}

	@Override
	public void wasAssociatedWith(String graph,
			QualifiedName activityId,
			QualifiedName agentId,
			QualifiedName relationId,
			List<Attribute> attributes)
			throws HabitatServiceException {
		wasAssociatedWith(graph, activityId, agentId, null, relationId,
				attributes);
	}

	@Override
	public void wasAssociatedWith(String graph,
			QualifiedName activityId,
			QualifiedName agentId,
			QualifiedName relationId)
			throws HabitatServiceException {
		wasAssociatedWith(graph, activityId, agentId, null, relationId,
				emptyList());
	}

	@Override
	public void actedOnBehalfOf(String graph,
			QualifiedName delegateAgentId,
			QualifiedName responsibleAgentId,
			QualifiedName activityId,
			QualifiedName relationId,
			List<Attribute> attributes)
			throws HabitatServiceException {
		final RelationModel relationModel = RelationModel.newActedOnBehalfOf(
				delegateAgentId, responsibleAgentId, attributes, activityId,
				relationId);
		storeRelation(graph, relationModel, ProvDmRelation.DELEGATION.getProvNRelation());
	}

	@Override
	public void actedOnBehalfOf(String graph,
			QualifiedName delegateAgentId,
			QualifiedName responsibleAgentId,
			QualifiedName activityId,
			QualifiedName relationId) throws HabitatServiceException {
		actedOnBehalfOf(graph,
				delegateAgentId,
				responsibleAgentId,
				activityId,
				relationId,
				emptyList());
	}

	@Override
	public void actedOnBehalfOf(String graph,
			QualifiedName delegateAgentId,
			QualifiedName responsibleAgentId,
			QualifiedName relationId,
			List<Attribute> attributes)
			throws HabitatServiceException {
		actedOnBehalfOf(graph, delegateAgentId, responsibleAgentId, null,
				relationId, attributes);
	}

	@Override
	public void actedOnBehalfOf(String graph,
			QualifiedName delegateAgentId,
			QualifiedName responsibleAgentId,
			QualifiedName relationId)
			throws HabitatServiceException {
		actedOnBehalfOf(graph, delegateAgentId, responsibleAgentId, null,
				relationId, emptyList());
	}

	@Override
	public void wasInfluencedBy(
			String graph,
			QualifiedName influenceeId,
			QualifiedName influencerId,
			QualifiedName relationId,
			List<Attribute> attributes)
			throws HabitatServiceException {
		final RelationModel relationModel = RelationModel.newWasInfluencedBy(
				influenceeId, influencerId, attributes, relationId);
		storeRelation(graph, relationModel, ProvDmRelation.INFLUENCE.getProvNRelation());
	}

	@Override
	public void wasInfluencedBy(String graph,
			QualifiedName influenceeId,
			QualifiedName influencerId,
			QualifiedName relationId)
			throws HabitatServiceException {
		wasInfluencedBy(graph, influenceeId, influencerId, relationId,
				emptyList());
	}

	@Override
	public void specializationOf(String graph,
			QualifiedName specificEntity,
			QualifiedName genericEntity) throws HabitatServiceException {
		final RelationModel relationModel = RelationModel.newSpecializationOf(
				specificEntity, genericEntity);
		storeRelation(graph, relationModel, ProvDmRelation.SPECIALIZATION.getProvNRelation());
	}

	@Override
	public void alternateOf(String graph,
			QualifiedName alternate1,
			QualifiedName alternate2) throws HabitatServiceException {
		final RelationModel relationModel = RelationModel.newAlternateOf(alternate1,
				alternate2);
		storeRelation(graph, relationModel, ProvDmRelation.ALTERNATE.getProvNRelation());
	}

	@Override
	public void hadMember(String graph,
			QualifiedName collection,
			QualifiedName entity)
			throws HabitatServiceException {
		final RelationModel relationModel = RelationModel
				.newHasMember(collection, entity);
		storeRelation(graph, relationModel, ProvDmRelation.MEMBERSHIP.getProvNRelation());
	}

	private void createRelation(
			final String graph,
			final RelationModel relationModel,
			String label)
			throws HabitatServiceException {
		checkNotNull(relationModel);
		final String resource = validateName(graph);
		final ProvQualifiedNameToken subjectToken = new ProvQualifiedNameToken(
				relationModel.getSubjectId());
		final ProvQualifiedNameToken objectToken = new ProvQualifiedNameToken(
				relationModel.getObjectId());
		final TupleWithSchema<String> tuple = relationModel.toTupleWithSchema();
		
		tuple.getSchema().addField("label", String.class);
		tuple.put("label", new StringField(label));
		
		final QualifiedName secondaryObjectId = relationModel
				.getSecondaryObjectId();
		if (secondaryObjectId == null) {
			provApi.storeProvenanceLink(resource, subjectToken, label, objectToken,
					tuple);
		} else {
			createTernaryRelation(
					resource,
					relationModel.getType(),
					subjectToken,
					objectToken,
					secondaryObjectId,
					tuple,
					relationModel.getRelationId(),
					label);
		}
	}

	private void createTernaryRelation(
			final String resource,
			ProvDmRelation relation,
			ProvQualifiedNameToken subjectToken,
			ProvQualifiedNameToken objectToken,
			QualifiedName secondaryObjectId,
			final TupleWithSchema<String> tuple,
			@Nullable QualifiedName relationId,
			String label)
			throws HabitatServiceException {
		if (!relation
				.hasAdditionalProperty(AdditionalProperty.SECONDARY_OBJECT)) {
			throw new HabitatServiceException(
					String.format(
							"relation %s does not have a secondary object",
							relation.name()));
		}
		final ProvQualifiedNameToken secondaryObjectToken = new ProvQualifiedNameToken(
				secondaryObjectId);
		final QualifiedName ternaryRelationId = relationId == null
				? new QualifiedName(
						ProvDmConstants.PENN_PROV_NAMESPACE, UUID
								.randomUUID()
								.toString())
				: relationId;
		final ProvQualifiedNameToken ternaryRelationToken = new ProvQualifiedNameToken(
				ternaryRelationId);
		provApi.storeProvenanceNode(
				resource,
				ternaryRelationToken,
				tuple,
				new ProvLocation());
		provApi.storeProvenanceLink(
				resource,
				subjectToken,
				label,
				ternaryRelationToken,
				tuple);
		provApi.storeProvenanceLink(
				resource,
				ternaryRelationToken,
				label,
				objectToken,
				tuple);
		provApi.storeProvenanceLink(
				resource,
				ternaryRelationToken,
				label,
				secondaryObjectToken,
				tuple);
	}

	private static String validateName(String graph)
			throws HabitatServiceException {
		if (graph == null) {
			throw new HabitatServiceException("graph name cannot be null");
		}
		final String trimmed = graph.trim();
		if (trimmed.isEmpty()) {
			throw new HabitatServiceException(
					"graph name cannot be empty or all whitespace");
		}
		return trimmed;
	}

	@Override
	public List<Attribute> getNodeAnnotations(String graph, QualifiedName provId) throws HabitatServiceException {
		final ProvQualifiedNameToken provToken = new ProvQualifiedNameToken(
				provId);
		Iterable<ProvToken> connected = provApi.getConnectedTo(graph, provToken, ProvDmRelation.ANNOTATED.getProvNRelation());
		List<Attribute> ret = new ArrayList<>();
		
		for (ProvToken ann: connected) {
			TupleWithSchema<String> str = provApi.getProvenanceData(graph, ann);
			for (String key: str.getKeys())
				ret.add(new StringAttribute(QualifiedName.from(key), str.getValue(key).toString()));
		}
		return ret;
	}

	@Override
	public List<QualifiedName> getNodesFrom(String graph,
			QualifiedName provId, String edgeLabel) throws HabitatServiceException {
		
		final ProvQualifiedNameToken provToken = new ProvQualifiedNameToken(
				provId);
		Iterable<ProvToken> connected = provApi.getConnectedFrom(graph, provToken, edgeLabel);
		List<QualifiedName> ret = new ArrayList<>();
		
		for (ProvToken ann: connected)
			ret.add(QualifiedName.from(ann.toString()));
		return ret;
	}

	@Override
	public List<QualifiedName> getNodesTo(String graph,
			QualifiedName provId, String edgeLabel) throws HabitatServiceException {
		
		final ProvQualifiedNameToken provToken = new ProvQualifiedNameToken(
				provId);
		Iterable<ProvToken> connected = provApi.getConnectedTo(graph, provToken, edgeLabel);
		List<QualifiedName> ret = new ArrayList<>();
		
		for (ProvToken ann: connected)
			ret.add(QualifiedName.from(ann.toString()));
		return ret;
	}
}
