/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import static com.google.common.base.Preconditions.checkNotNull;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.PROV_TOKEN_PATH_PARAM;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.RESOURCE_PATH_PARAM;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import spark.Request;
import spark.Response;
import spark.Service;

import com.google.gson.Gson;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.model.ProvTokenModel;
import edu.upenn.cis.db.habitat.core.type.model.StoreNodeModel;
import edu.upenn.cis.db.habitat.core.type.model.TupleWithSchemaModel;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;

/**
 * @author John Frommeyer
 *
 */
public class StoreProvenanceNode extends StoreProvenanceNodeWsMethod implements
		RouteWithInfo {

	private final Logger logger = LogManager.getLogger(getClass());
	private final ProvenanceApi backend;
	private final Service spark;
	private final Gson gson;

	public StoreProvenanceNode(ProvenanceApi backend, Service spark, Gson gson) {
		super();
		this.backend = checkNotNull(backend);
		this.spark = checkNotNull(spark);
		this.gson = checkNotNull(gson);
	}

	@Override
	public Object handle(
			Request request,
			Response response) throws Exception {
		final String m = "handle(...)";

		final String resource = getPathParam(spark, request,
				RESOURCE_PATH_PARAM);
		logger.debug("{}: Got resource from path {}", m, resource);

		final String pathToken = getPathParam(spark, request,
				PROV_TOKEN_PATH_PARAM);
		logger.debug("{}: Got ProvToken from path {}", m, pathToken);

		final String jsonBody = request.body();
		logger.debug("{}: Got body {}", m, jsonBody);

		final StoreNodeModel body = gson.fromJson(jsonBody,
				StoreNodeModel.class);

		if (body.provSpecifier == null) {
			throw halt(spark, HttpServletResponse.SC_BAD_REQUEST,
					new ResponseError(
							"request missing provLocation"));
		}
		storeProvenanceNode(resource, pathToken, body);
		response.status(HttpServletResponse.SC_NO_CONTENT);

		return null;

	}

	@Override
	public void storeProvenanceNode(
			String resource,
			String provTokenValue,
			StoreNodeModel storeNodeModel)
			throws Exception {
		final String m = "storeProvenanceNode(...)";
		final ProvToken provToken = ProvTokenModel.toProvToken(provTokenValue);
		logger.debug("{}: Extracted provToken: [{}] of type {}", m,
				provToken, provToken.getClass());
		final ProvSpecifier provLocation = storeNodeModel.provSpecifier
				.toProvSpecifier();
		logger.debug("{}: Extracted provLocation: [{}]", m,
				provLocation);

		final TupleWithSchemaModel tupleWithSchemaModel = storeNodeModel.tupleWithSchema;
		if (tupleWithSchemaModel == null) {
			backend.storeProvenanceNode(resource, provToken, provLocation);
		} else {
			final TupleWithSchema<String> tupleWithSchema = tupleWithSchemaModel
					.toTupleWithSchema();
			logger.debug("{}: Extracted tupleWithSchema: [{}]", m,
					tupleWithSchema);
			backend.storeProvenanceNode(
					resource,
					provToken,
					tupleWithSchema,
					provLocation);
		}
	}
}
