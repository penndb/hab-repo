/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import static com.google.common.base.Preconditions.checkNotNull;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.RESOURCE_PATH_PARAM;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import spark.Request;
import spark.Response;
import spark.Service;

import com.google.gson.Gson;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.model.ProvTokenModel;
import edu.upenn.cis.db.habitat.core.type.model.StoreLinkModel;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;

/**
 * @author John Frommeyer
 *
 */
public class StoreProvenanceLink extends StoreProvenanceLinkWsMethod implements
		RouteWithInfo {

	private final Logger logger = LogManager.getLogger(getClass());
	private final ProvenanceApi backend;
	private final Service spark;
	private final Gson gson;

	public StoreProvenanceLink(ProvenanceApi backend, Service spark, Gson gson) {
		super();
		this.backend = checkNotNull(backend);
		this.spark = checkNotNull(spark);
		this.gson = checkNotNull(gson);
	}

	@Override
	public Object handle(
			Request request,
			Response response) throws Exception {
		final String m = "handle(...)";

		final String resource = getPathParam(spark, request, RESOURCE_PATH_PARAM);
		logger.debug("{}: Got resource: {}", m, resource);

		final String jsonBody = request.body();
		logger.debug("{}: Got body {}", m, jsonBody);

		final StoreLinkModel body = gson.fromJson(jsonBody,
				StoreLinkModel.class);
		validateBody(body);
		storeProvenanceLink(resource, body);
		response.status(HttpServletResponse.SC_NO_CONTENT);

		return null;

	}

	@Override
	public void storeProvenanceLink(String resource,
			StoreLinkModel storeLinkModel)
			throws Exception {
		final String m = "storeProvenanceLink(...)";

		final List<ProvTokenModel> fromModels = storeLinkModel.from;
		final List<ProvToken> from = new ArrayList<>();
		for (final ProvTokenModel fromModel : fromModels) {
			from.add(fromModel.toProvSpecifier());
		}
		logger.debug("{}: Extracted from ProvTokens: [{}]", m,
				from);

		final ProvToken to = storeLinkModel.to.toProvSpecifier();
		logger.debug("{}: Extracted to ProvToken: [{}]", m,
				to);

		if (storeLinkModel.tupleWithSchema == null) {
			logger.debug("{}: Extracted link label: [{}]", m,
					storeLinkModel.label);
			backend.storeProvenanceLink(resource, from,
					storeLinkModel.label, to);
		} else {
			final TupleWithSchema<String> tupleWithSchema = storeLinkModel.tupleWithSchema
					.toTupleWithSchema();
			logger.debug("{}: Extracted link tupleWithSchema: [{}]", m,
					tupleWithSchema);
			backend.storeProvenanceLink(resource, from, storeLinkModel.label, to, 
					tupleWithSchema);
		}
	}

	private void validateBody(StoreLinkModel body) {
		if (body.from == null || body.from.isEmpty()) {
			throw halt(spark, HttpServletResponse.SC_BAD_REQUEST,
					new ResponseError("missing from tokens"));
		}
		if (body.to == null) {
			throw halt(spark, HttpServletResponse.SC_BAD_REQUEST,
					new ResponseError("missing to token"));
		}
		if (body.label == null && body.tupleWithSchema == null) {
			throw halt(spark, HttpServletResponse.SC_BAD_REQUEST,
					new ResponseError("must provide lable or tupleWithSchema"));
		}
	}
}
