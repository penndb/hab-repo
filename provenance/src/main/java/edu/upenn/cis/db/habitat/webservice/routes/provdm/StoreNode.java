/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes.provdm;

import static com.google.common.base.Preconditions.checkNotNull;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.PROV_TOKEN_PATH_PARAM;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.RESOURCE_PATH_PARAM;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import spark.Request;
import spark.Response;
import spark.Service;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import edu.upenn.cis.db.habitat.core.api.ProvDmApi;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.model.ProvTokenModel;
import edu.upenn.cis.db.habitat.core.type.provdm.NodeModel;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvQualifiedNameToken;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;
import edu.upenn.cis.db.habitat.webservice.routes.RouteWithInfo;

/**
 * @author John Frommeyer
 *
 */
public class StoreNode extends StoreNodeWsMethod implements
		RouteWithInfo {

	private final Logger logger = LogManager.getLogger(getClass());
	private final ProvDmApi backend;
	private final Service spark;
	private final Gson gson;

	public StoreNode(ProvDmApi backend, Service spark, Gson gson) {
		super();
		this.backend = checkNotNull(backend);
		this.spark = checkNotNull(spark);
		this.gson = checkNotNull(gson);
	}

	@Override
	public Object handle(
			Request request,
			Response response) throws Exception {
		final String m = "handle(...)";

		final String resource = getPathParam(spark, request,
				RESOURCE_PATH_PARAM);
		logger.debug("{}: Got resource from path {}", m, resource);

		final String pathToken = getPathParam(spark, request,
				PROV_TOKEN_PATH_PARAM);
		logger.debug("{}: Got ProvToken from path {}", m, pathToken);

		final String jsonBody = request.body();
		logger.debug("{}: Got body {}", m, jsonBody);

		try {
			final NodeModel body = gson.fromJson(jsonBody,
					NodeModel.class);
	
			if (body.getType() == null) {
				throw halt(spark, HttpServletResponse.SC_BAD_REQUEST,
						new ResponseError(
								"request missing PROV DM type"));
			}
			storeNode(resource, pathToken, body);
			response.status(HttpServletResponse.SC_NO_CONTENT);
	
			return null;
		} catch (JsonSyntaxException ie) {
			logger.error("Unable to store!");
			ie.printStackTrace();
			return null;
		}
	}

	@Override
	public void storeNode(
			String resource,
			String provTokenValue,
			NodeModel storeNodeModel)
			throws Exception {
		final String m = "entity(...)";
		final ProvToken provToken = ProvTokenModel.toProvToken(provTokenValue);
		if (!(provToken instanceof ProvQualifiedNameToken)) {
			throw halt(spark, HttpServletResponse.SC_BAD_REQUEST,
					new ResponseError(
							"ProvToken must be a qualified name"));
		}
		final ProvQualifiedNameToken qnToken = (ProvQualifiedNameToken) provToken;

		logger.debug("{}: Extracted provToken: [{}]", m,
				qnToken);
		backend.storeNode(resource, qnToken.getValue(), storeNodeModel);
	}
}
