/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.provenance.marshalling;

import java.io.IOException;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;

import edu.upenn.cis.db.habitat.core.api.StorageApi;
import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.StructuredValue;
import edu.upenn.cis.db.habitat.util.ObjectSerialization;

public class MarshalPair<T1,T2> implements StorageApi.ObjectSerializer<Pair<T1,T2>, 
StructuredData<String,Object>> {
	final static Logger logger = LogManager.getLogger(MarshalPair.class);
	
	final Class<T1> leftClass;
	final Class<T2> rightClass;
	final String leftProperty;
	final String rightProperty;
	
	public MarshalPair(String leftProperty, Class<T1> leftClass,
			String rightProperty, Class<T2> rightClass) {

		this.leftClass = leftClass;
		this.leftProperty = leftProperty;
		this.rightClass = rightClass;
		this.rightProperty = rightProperty;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Pair<T1,T2> getObject(StructuredData<String, Object> serializedObject) {
		T1 left = null;
		T2 right = null;
		try {
//			left = jsonMapper.readValue(serializedObject.get(leftProperty), leftClass);
//			right = jsonMapper.readValue(serializedObject.get(rightProperty), rightClass);
			left = (T1)ObjectSerialization.getObject(serializedObject.get(leftProperty));
			right = (T2)ObjectSerialization.getObject(serializedObject.get(rightProperty));
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("Could not de-serialize prov location: " + e.getMessage());
		}
		
		return new ImmutablePair<>(left, right);
	}
	
	@SuppressWarnings("unchecked")
	public T1 getFirst(StructuredData<String, Object> serializedObject) {
		T1 left = null;
		try {
//			left = jsonMapper.readValue(serializedObject.get(leftProperty), leftClass);
			left = (T1)ObjectSerialization.getObject(serializedObject.get(leftProperty));
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("Could not de-serialize prov location: " + e.getMessage());
		}
		
		return left;
	}

	@SuppressWarnings("unchecked")
	public T2 getSecond(StructuredData<String, Object> serializedObject) {
		T2 right = null;
		try {
//			right = jsonMapper.readValue(serializedObject.get(rightProperty), rightClass);
			right = (T2)ObjectSerialization.getObject(serializedObject.get(rightProperty));
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("Could not de-serialize prov location: " + e.getMessage());
		}
		
		return right;
	}

//	@Override
//	public StructuredData<String, String> getSerializedObjectAsString(String name, Pair<T1,T2> pair) {
//		StructuredData<String,String> ret = new StructuredData<String,String>();
//		
//		// JSONify
//		try {
//			ret.put(leftProperty, jsonMapper.writeValueAsString(pair.getLeft()));
// 			ret.put(rightProperty, jsonMapper.writeValueAsString(pair.getRight()));
//		} catch (JsonProcessingException e) {
//			e.printStackTrace();
//			logger.error("Could not serialize " + pair + ": " + e.getMessage());
//		}
//		
//		return ret;
//	}

	@Override
	public StructuredData<String, Object> getSerializedObject(Pair<T1,T2> pair) {
		StructuredData<String,Object> ret = new StructuredValue<>();
		
		// JSONify
		try {
			ret.put(leftProperty, ObjectSerialization.putObject(pair.getLeft()));
 			ret.put(rightProperty, ObjectSerialization.putObject(pair.getRight()));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			logger.error("Could not serialize " + pair, e);
		}
		
		return ret;
	}
}