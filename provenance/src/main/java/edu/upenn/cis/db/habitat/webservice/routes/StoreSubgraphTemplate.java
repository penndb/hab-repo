/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import static com.google.common.base.Preconditions.checkNotNull;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.RESOURCE_QUERY_PARAM;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.base.Stopwatch;
import com.google.gson.Gson;

import edu.upenn.cis.db.habitat.core.api.ProvenanceGraphApi;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;
import spark.Request;
import spark.Response;
import spark.Service;

/**
 * @author John Frommeyer
 *
 */
public class StoreSubgraphTemplate extends StoreSubgraphTemplateWsMethod
		implements RouteWithInfo {

	private final Logger logger = LogManager.getLogger(getClass());
	private final ProvenanceGraphApi backend;
	private final Service spark;
	private final Gson gson;

	public StoreSubgraphTemplate(ProvenanceGraphApi backend,
			Service spark,
			Gson gson) {
		super();
		this.backend = checkNotNull(backend);
		this.spark = checkNotNull(spark);
		this.gson = checkNotNull(gson);
	}

	public Object handle(
			Request request,
			Response response) throws Exception {
		final String m = "handle(...)";
		final String resource = getPathParam(
				spark,
				request,
				RESOURCE_QUERY_PARAM);
		final String jsonBody = request.body();
		logger.debug("{}: Got body {}", m, jsonBody);
		final SubgraphTemplate subgraph = gson.fromJson(jsonBody,
				SubgraphTemplate.class);
		validateBody(subgraph);
		storeSubgraphTemplate(resource,
				subgraph);
		response.status(HttpServletResponse.SC_OK);
		return null;
	}

	@Override
	public void storeSubgraphTemplate(String resource,
			SubgraphTemplate template)
			throws Exception {
		final String m = "storeSubgraph(...)";
		final Stopwatch stopwatch = Stopwatch.createStarted();
		backend.storeSubgraphTemplate(resource,
				template);
		stopwatch.stop();
		logger.info("{}: Stored subgraph template for resource [{}]: {}", m,
				resource, stopwatch);
	}

	private void validateBody(SubgraphTemplate body) {
		if (body == null) {
			throw halt(spark, HttpServletResponse.SC_BAD_REQUEST,
					new ResponseError("missing instance"));
		}
		if (body.getRanks() == null || body.getRanks().isEmpty()) {
			throw halt(spark, HttpServletResponse.SC_BAD_REQUEST,
					new ResponseError("missing ranks"));
		}
		if (body.getLinks() == null) {
			throw halt(spark, HttpServletResponse.SC_BAD_REQUEST,
					new ResponseError("missing links"));
		}
	}
}
