/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import edu.upenn.cis.db.habitat.core.type.Field;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;

/**
 * @author John Frommeyer
 *
 */
public class BasicTupleSerializer extends
		StdSerializer<TupleWithSchema<String>> {

	public BasicTupleSerializer() {
		super(TupleWithSchema.class, true);
	}

	private static final long serialVersionUID = 1L;

	@Override
	public void serialize(
			TupleWithSchema<String> value,
			JsonGenerator gen,
			SerializerProvider provider) throws IOException {
		final String m = "serialize(...)";
		gen.writeObjectField("schema", value.getSchema());
		gen.writeArrayFieldStart("tuple");
		for (final String key : value.getKeys()) {
			final Field<? extends Object> field = value.get(key);
			gen.writeStartArray(2);
			provider.defaultSerializeValue(key, gen);
			provider.defaultSerializeValue(field, gen);
			gen.writeEndArray();
		}
		gen.writeEndArray();
	}

	@Override
	public void serializeWithType(TupleWithSchema<String> value,
			JsonGenerator gen, SerializerProvider serializers,
			TypeSerializer typeSer) throws IOException {
		typeSer.writeTypePrefixForObject(value, gen);
		serialize(value, gen, serializers);
		typeSer.writeTypeSuffixForObject(value, gen);
	}

}
