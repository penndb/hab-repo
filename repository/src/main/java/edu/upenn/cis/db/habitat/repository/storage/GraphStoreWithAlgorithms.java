/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.repository.storage;

import org.apache.commons.lang3.tuple.Pair;

import edu.upenn.cis.db.habitat.core.type.TransactionSupplier;

/**
 * Graph store with support for graph influence algorithms
 * 
 * @author zives
 *
 * @param <K>
 */
public interface GraphStoreWithAlgorithms<K> extends GraphStore<K> {

	/**
	 * Gets the betweenness score for each node, based on its out-edges.
	 * Returns the top-k scoring nodes.
	 * 
	 * @param resource
	 * @param edgeRestriction
	 * @param k
	 * @return
	 */
	public Iterable<Pair<String,Double>> getTopKBetweennessNodes(
			String resource, 
			String edgeRestriction, 
			int k);

	/**
	 * Gets the betweenness score for each node, based on its out-edges.
	 * Returns the top-k scoring nodes.
	 * 
	 * @param resource
	 * @param edgeRestriction
	 * @param k
	 * @return
	 */
	public Iterable<Pair<String,Double>> getTopKBetweennessNodes(
			TransactionSupplier supplier,
			String resource, 
			String edgeRestriction, 
			int k);

	/**
	 * Gets the closeness score for each node, based on its out-edges.
	 * Returns the top-k scoring nodes.
	 * 
	 * @param resource
	 * @param edgeRestriction
	 * @param k
	 * @return
	 */
	public Iterable<Pair<String,Double>> getTopKClosenessNodes(
			String resource, 
			String edgeRestriction, 
			int k);

	/**
	 * Gets the closeness score for each node, based on its out-edges.
	 * Returns the top-k scoring nodes.
	 * 
	 * @param resource
	 * @param edgeRestriction
	 * @param k
	 * @return
	 */
	public Iterable<Pair<String,Double>> getTopKClosenessNodes(
			TransactionSupplier supplier,
			String resource, 
			String edgeRestriction, 
			int k);

	/**
	 * Gets the PageRank score for each node, based on its out-edges.
	 * Returns the top-k scoring nodes.
	 * 
	 * @param resource
	 * @param edgeRestriction
	 * @param k
	 * @return
	 */
	public Iterable<Pair<String,Double>> getTopKPageRankNodes(
			String resource, 
			int k);

	/**
	 * Gets the PageRank score for each node, based on its out-edges.
	 * Returns the top-k scoring nodes.
	 * 
	 * @param resource
	 * @param edgeRestriction
	 * @param k
	 * @return
	 */
	public Iterable<Pair<String,Double>> getTopKPageRankNodes(
			TransactionSupplier supplier,
			String resource, 
			int k);
	
}
