/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.repository.storage;

import java.io.InputStream;
import java.net.URL;
import java.sql.SQLException;
import java.util.Map;
import java.util.function.Function;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import com.google.inject.Inject;

import edu.upenn.cis.db.habitat.core.api.StorageApiWithMetadata;
import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.TransactionSupplier;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.util.MapIterableKeyTuple;

public class HybridStore<K extends String, V extends Object, B> implements StorageApiWithMetadata<K, V, B> {
	final GraphStore<K> graph;			// String -> object
	final BlobStore<B> blob;			// String -> byte[]
	
	final Function<Object,V> converter;
	
	@Inject
	public HybridStore(
			GraphStore<K> graph,
			BlobStore<B> blob
			) {
		this.graph = graph;
		this.blob = blob;
		this.converter = null;
	}

	@Inject
	public HybridStore(
			GraphStore<K> graph,
			BlobStore<B> blob,
			Function<Object,V> converter
			) {
		this.graph = graph;
		this.blob = blob;
		this.converter = converter;
	}

	@Override
	public void addMetadata(String resource, K key, StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		graph.addMetadata(resource, key, metadata);
	}

	@Override
	public void addMetadata(TransactionSupplier supplier, String resource, K key, StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		graph.addMetadata(supplier, resource, key, metadata);
	}

	@Override
	public void addMetadata(String resource, K key, StructuredData<K, ? extends V> metadata,
			String specialLabel) throws HabitatServiceException {
		graph.addMetadata(resource, key, metadata, specialLabel);
	}

	@Override
	public void addMetadata(TransactionSupplier supplier, String resource, K key, StructuredData<K, ? extends V> metadata,
			String specialLabel) throws HabitatServiceException {
		graph.addMetadata(supplier, resource, key, metadata, specialLabel);
	}

	@Override
	public void connect() throws SQLException {
		graph.connect();
		blob.connect();
	}

	@Override
	public boolean execTransaction(String resource, Function<TransactionSupplier,Boolean> transactionFn) throws HabitatServiceException {
		return blob.execTransaction(resource, transactionFn) && 
				graph.execTransaction(resource, transactionFn);
	}

	@Override
	public int getCount(String resource) throws HabitatServiceException {
		return graph.getCount(resource);
	}

	@Override
	public B getData(String resource, K key) throws HabitatServiceException {
		return blob.getData(resource, key.toString());
	}

	@Override
	public B getData(TransactionSupplier supplier, String resource, K key) {
		throw new UnsupportedOperationException();
	}

	@Override
//	public List<K> getKeysMatching(String metadataField, Predicate<Object> pred) {
	public Iterable<K> getKeysMatching(String resource, String metadataField, Map<K,? extends V> match) throws HabitatServiceException {
		return graph.getKeysMatching(resource, metadataField, match);
	}

	@Override
	public Iterable<K> getKeysMatching(TransactionSupplier supplier, String resource, String metadataField, Map<K, ? extends V> match) throws HabitatServiceException {
		return graph.getKeysMatching(supplier, resource, metadataField, match);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Iterable<Pair<K, StructuredData<K,V>>> getLinksFrom(String resource, K key1) throws HabitatServiceException {
		if (converter == null)
			return new MapIterableKeyTuple<K,Object,V>(graph.getLinksFrom(resource, key1),
					pair -> new ImmutablePair<K,StructuredData<K,V>>(pair.getLeft(), (StructuredData<K,V>)pair.getRight()));
		else
			throw new UnsupportedOperationException("We don't yet convert tuple values");	}

	@SuppressWarnings("unchecked")
	@Override
	public Iterable<Pair<K, StructuredData<K,V>>> getLinksFrom(TransactionSupplier supplier, String resource, K key1)
			throws HabitatServiceException {
		if (converter == null)
			return new MapIterableKeyTuple<K,Object,V>(graph.getLinksFrom(supplier, resource, key1),
					pair -> new ImmutablePair<K,StructuredData<K,V>>(pair.getLeft(), (StructuredData<K,V>)pair.getRight()));
		else
			throw new UnsupportedOperationException("We don't yet convert tuple values");	}

	@SuppressWarnings("unchecked")
	@Override
	public Iterable<Pair<K, StructuredData<K,V>>> getLinksTo(String resource, K key1) throws HabitatServiceException {
		if (converter == null)
			return new MapIterableKeyTuple<K,Object,V>(graph.getLinksTo(resource, key1),
					pair -> new ImmutablePair<K,StructuredData<K,V>>(pair.getLeft(), (StructuredData<K,V>)pair.getRight()));
		else
			throw new UnsupportedOperationException("We don't yet convert tuple values");	}

	@SuppressWarnings("unchecked")
	@Override
	public Iterable<Pair<K, StructuredData<K,V>>> getLinksTo(TransactionSupplier supplier, String resource, K key1)
			throws HabitatServiceException {
		if (converter == null)
			return new MapIterableKeyTuple<K,Object,V>(graph.getLinksTo(supplier, resource, key1),
					pair -> new ImmutablePair<K,StructuredData<K,V>>(pair.getLeft(), (StructuredData<K,V>)pair.getRight()));
		else
			throw new UnsupportedOperationException("We don't yet convert tuple values");	}

	@SuppressWarnings("unchecked")
	@Override
	public StructuredData<K,V> getMetadata(String resource, K key) throws HabitatServiceException {
		if (converter != null)
			return graph.getMetadata(resource, key).<V>createConverted(converter);
		else
			return (StructuredData<K, V>) graph.getMetadata(resource, key);
	}

	@SuppressWarnings("unchecked")
	@Override
	public StructuredData<K, V> getMetadata(TransactionSupplier supplier, String resource, K key) throws HabitatServiceException {
		if (converter != null)
			return graph.getMetadata(supplier, resource, key).<V>createConverted(converter);
		else
			return (StructuredData<K, V>) graph.getMetadata(supplier, resource, key);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Iterable<Pair<K,StructuredData<K,V>>> getMetadataForResource(String resource) throws HabitatServiceException {
		if (converter == null)
			return new MapIterableKeyTuple<K,Object,V>(graph.getMetadataForResource(resource),
					pair -> new ImmutablePair<K,StructuredData<K,V>>(pair.getLeft(), (StructuredData<K,V>)pair.getRight()));
		else
			throw new UnsupportedOperationException("We don't yet convert tuple values");	
	}

	@SuppressWarnings("unchecked")
	@Override
	public Iterable<Pair<K,StructuredData<K,V>>> getMetadataForResource(TransactionSupplier supplier, String resource)
			throws HabitatServiceException {
		if (converter == null)
			return new MapIterableKeyTuple<K,Object,V>(graph.getMetadataForResource(supplier, resource),
					pair -> new ImmutablePair<K,StructuredData<K,V>>(pair.getLeft(), (StructuredData<K,V>)pair.getRight()));
		else
			throw new UnsupportedOperationException("We don't yet convert tuple values");
	}

	@Override
	public edu.upenn.cis.db.habitat.core.api.StorageApi.StoreClass getStoreClass() {
		return edu.upenn.cis.db.habitat.core.api.StorageApi.StoreClass.ANNOTATED_BLOB;
	}

	@Override
	public boolean isDataLegal(StructuredData<K, ? extends V> metadata) {
		return graph.isDataLegal(metadata);
	}

	@Override
	public boolean isSuitableForMetadata() {
		return graph.isSuitableForMetadata() || blob.isSuitableForMetadata();
	}

	@Override
	public boolean isSuitableToStore(Object obj) {
		return graph.isSuitableToStore(obj) || blob.isSuitableToStore(obj);
	}

	@Override
	public void link(String resource, K key1, String label, K key2, 
			StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		graph.link(resource, key1, label, key2, metadata);
	}

	@Override
	public void link(TransactionSupplier supplier, String resource, K key1, 
			String label, K key2, StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		graph.link(supplier, resource, key1, label, key2, metadata);
	}

	@Override
	public void removeKey(String resource, K key) throws HabitatServiceException {
		blob.removeKey(resource, key.toString());
		graph.removeKey(resource, key);
	}

	@Override
	public void removeKey(TransactionSupplier supplier, String resource, K key) throws HabitatServiceException {
		blob.removeKey(supplier, resource, key.toString());
		graph.removeKey(supplier, resource, key);
	}

	@Override
	public void removeMetadata(String resource, K key, String metadataField) throws HabitatServiceException {
		graph.removeMetadata(resource, key, metadataField);
	}

	@Override
	public void removeMetadata(TransactionSupplier supplier, String resource, K key, String metadataField) throws HabitatServiceException {
		graph.removeMetadata(resource, key, metadataField);
	}
	
	@Override
	public void replaceMetadata(String resource, K key, StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		graph.replaceMetadata(resource, key, metadata);
	}

	@Override
	public void replaceMetadata(TransactionSupplier supplier, String resource, K key, StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		graph.replaceMetadata(supplier, resource, key, metadata);
	}

	@Override
	public void store(String resource, K key, B value, StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		blob.store(resource, key.toString(), value, null);
		graph.store(resource, key, key.toString(), metadata);
	}

	@Override
	public void store(TransactionSupplier supplier,String resource,  K key, B value, 
			StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		blob.store(supplier, resource, key.toString(), value, null);
		graph.store(supplier, resource, key, key.toString(), metadata);
	}

	@Override
	public void storeReference(String resource, K key, String refToValue, StructuredData<K, ? extends V> metadata)
			throws HabitatServiceException {
		graph.storeReference(resource, key, refToValue, metadata);
	}

	@Override
	public void storeReference(TransactionSupplier supplier, String resource, K key, String refToValue,
			StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		graph.storeReference(supplier, resource, key, refToValue, metadata);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void storeFile(String resource, K key, InputStream file, int inputLength,
			StructuredData<K, ? extends V> metadata)
			throws HabitatServiceException {
		blob.storeFile(resource, key.toString(), file, inputLength,
				(StructuredData<String, ? extends Object>) metadata);
		graph.addMetadata(resource, key, metadata);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void storeFile(TransactionSupplier supplier, String resource, K key, InputStream file,
			int inputLength, StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		blob.storeFile(supplier, resource, key.toString(), file, inputLength,
				(StructuredData<String, ? extends V>) metadata);
		graph.addMetadata(supplier , resource, key, metadata);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void storeUrlContents(String resource, K key, URL url, 
			StructuredData<K, ? extends V> metadata)
			throws HabitatServiceException {
		blob.storeUrlContents(resource, key.toString(), url, (StructuredData<String, ? extends Object>) metadata);
		graph.addMetadata(resource, key, metadata);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void storeUrlContents(TransactionSupplier supplier, String resource, K key, URL url,
			StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		blob.storeUrlContents(supplier, resource, key.toString(), url, 
				(StructuredData<String, ? extends Object>) metadata);
		graph.addMetadata(supplier, resource, key, metadata);
	}

}
