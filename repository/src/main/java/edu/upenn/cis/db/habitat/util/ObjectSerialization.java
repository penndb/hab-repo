/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.StringEncoding;
import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.StructuredValue;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * Main object serialization API, which goes between tuples (etc) and Maps
 * 
 * @author ZacharyIves
 *
 */
public class ObjectSerialization {
	final static ObjectMapper mapper;
	final static Logger logger = LogManager
			.getLogger(ObjectSerialization.class);

	static {
		try {
			final ObjectMapper mapperTemp = new ObjectMapper()
					.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
			final SimpleModule module = new SimpleModule("TupleModule",
					new Version(1, 0, 0, null, null, null));
			module.addSerializer(new BasicTupleSerializer());
			module.addDeserializer(BasicTuple.class,
					new BasicTupleDeserializer());
			mapperTemp.registerModule(module);
			mapper = mapperTemp;
		} catch (Throwable e) {
			throw new ExceptionInInitializerError(e);
		}
	}

	/**
	 * Given a map of serialized objects (one key/ serialized object per field),
	 * convert back to a StructuredData tuple. As necessary we'll de-serialize
	 * each object.
	 * 
	 * @param metadata
	 * @param properties
	 * @throws HabitatServiceException
	 */
	public static StructuredData<String, Object> deserialize(
			Map<String, ? extends Object> properties)
			throws IOException {
		StructuredData<String, Object> ret = new StructuredValue<>();

		for (String key : properties.keySet()) {
			Object v = properties.get(key);

			ret.put(key, getObject(v));
		}

		return ret;
	}

	/**
	 * Given a serialized object, unmarshall it back into the original object.
	 * If the serialized object was a scalar-object, we don't convert. Else if
	 * it's in JSON (as indicated by a leading backslash and a brace or bracket)
	 * then de-serialize it.
	 * 
	 * @param v
	 * @return
	 * @throws IOException
	 */
	public static Object getObject(Object v) throws IOException {
		if (v instanceof StringEncoding
				|| (v instanceof String && (((String) v).startsWith("\\{") || ((String) v)
						.startsWith("\\[")))) {
			final String encodedString = v instanceof StringEncoding
					? ((StringEncoding) v).getValue()
					: (String) v;
			String realValue = encodedString.substring(1);
			Object o = mapper.readValue(realValue, Object.class);
			return o;
		}
		return v;
	}

	/**
	 * Disallow certain fields from storage
	 * 
	 * @param metadata
	 * @return
	 */
	public static boolean isTupleLegal(
			StructuredData<String, ? extends Object> metadata) {
		if (metadata.containsKey("_key") || metadata.containsKey("_resource")
				|| metadata.containsKey("_value")
				|| metadata.containsKey("_valuePtr"))
			return false;
		return true;
	}

	/**
	 * Takes an object and converts it to a string. The string representation is
	 * (1) string content if we have a scalar-object or string, or (2) the JSON
	 * representation if it's something else. The JSON representation is
	 * prefixed with a backslash, and it includes info about the Java type used.
	 * 
	 * @param v
	 * @return
	 * @throws JsonProcessingException
	 */
	public static Object putObject(Object v) throws JsonProcessingException {
		if (v instanceof String && ((String) v).charAt(0) == '\\')
			throw new RuntimeException(
					"Illegal for string to start with a backslash");

		// If not scalar, convert to JSON
		if (v instanceof Long || v instanceof String || v instanceof Boolean ||
				v instanceof Double || v instanceof Float || v instanceof Short
				|| v instanceof Integer || v instanceof StringEncoding)
			return v;
		else {
			return new StringEncoding("\\" + mapper.writeValueAsString(v));
		}
	}

	/**
	 * Takes an object and converts it to a string. The string representation is
	 * (1) string content if we have a string, or (2) the JSON representation if
	 * it's something else. The JSON representation is prefixed with a
	 * backslash, and it includes info about the Java type used.
	 * 
	 * @param v
	 * @return
	 * @throws JsonProcessingException
	 */
	public static String putObjectAsString(Object v)
			throws JsonProcessingException {
		if (v instanceof String && ((String) v).charAt(0) == '\\')
			throw new RuntimeException(
					"Illegal for string to start with a backslash");

		// If not scalar, convert to JSON
		if (v instanceof String || v instanceof StringEncoding)
			return v.toString();
		else {
			return "\\" + mapper.writeValueAsString(v);
		}
	}

	/**
	 * Given a structured tuple as metadata, turn it into a map of serialized
	 * objects (one key/ serialized object per field in the StructuredData)
	 * 
	 * @param metadata
	 * @param properties
	 * @throws HabitatServiceException
	 */
	public static void serializeStructuredData(
			StructuredData<String, ? extends Object> metadata,
			Map<String, Object> properties)
			throws HabitatServiceException {
		if (!isTupleLegal(metadata)) {
			throw new HabitatServiceException("Cannot store " + metadata
					+ " due to illegal metadata");
		}

		if (metadata != null)
			for (Object k : metadata.keySet()) {
				Object v = metadata.get(k);

				try {
					properties.put(k.toString(), putObject(v));
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new HabitatServiceException(e.getMessage());
				}
			}

	}

	/**
	 * Convert a TupleWithSchema into a bunch of key/string values in a
	 * StructuredData, using JSON as necessary to handle non-String data from
	 * the original tuple
	 * 
	 * @param tuple
	 * @return
	 */
	public static StructuredData<String, String> getStringDataFromTuple(
			TupleWithSchema<String> tuple) {
		StructuredData<String, String> ret = new StructuredValue<>();

		for (String key : tuple.getKeys()) {
			try {
				ret.put(key.toString(), ObjectSerialization
						.putObjectAsString(tuple.getValue(key)));
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.error("Unable to serialize " + key);
			}
		}

		return ret;
	}

	/**
	 * Convert a TupleWithSchema into a bunch of key/serialized objects in a
	 * StructuredData, using JSON as necessary to handle non-String data from
	 * the original tuple
	 * 
	 * @param tuple
	 * @return
	 */
	public static StructuredData<String, Object> getObjectDataFromTuple(
			TupleWithSchema<String> tuple) {
		StructuredData<String, Object> ret = new StructuredValue<>();

		for (String key : tuple.getKeys()) {
			try {
				ret.put(key.toString(),
						ObjectSerialization.putObject(tuple.getValue(key)));
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.error("Unable to serialize " + key);
			}
		}

		return ret;
	}

	/**
	 * Given a tuple of key/serialized objects, de-serialize it back into a
	 * TupleWithSchema.
	 * 
	 * @param name
	 * @param data
	 * @return
	 */
	public static TupleWithSchema<String> getTupleFromObjectData(String name,
			StructuredData<String, Object> data) {
		BasicSchema schema = new BasicSchema(name);
		final Map<String, Object> deserializedData = new HashMap<>();
		for (Map.Entry<String, Object> serializedEntry : data.entrySet()) {
			try {
				final String key = serializedEntry.getKey();
				final Object serializedValue = serializedEntry.getValue();
				final Object deserializedValue = getObject(serializedValue);
				deserializedData.put(key, deserializedValue);
				schema.addField(key, deserializedValue.getClass());
			} catch (IOException e) {
				logger.error("Unable to serialize " + serializedEntry.getKey(),
						e);
			}
		}

		BasicTuple ret = schema.createTuple();

		for (Map.Entry<String, Object> deserializedEntry : deserializedData
				.entrySet()) {
			ret.setValue(deserializedEntry.getKey(),
					deserializedEntry.getValue());
		}

		return ret;
	}
}
