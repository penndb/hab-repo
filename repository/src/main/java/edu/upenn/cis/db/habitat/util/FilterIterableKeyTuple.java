/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.util;

import java.util.Iterator;
import java.util.function.Predicate;

import org.apache.commons.lang3.tuple.Pair;

import edu.upenn.cis.db.habitat.core.type.StructuredData;

/**
 * Take a collection of key/tuple iterable items, and return an iterable collection
 * of filtered items.
 * 
 * @author zives
 *
 * @param <K>
 * @param <V>
 */
public class FilterIterableKeyTuple<K,V> implements Iterable<Pair<K,StructuredData<K,V>>> {
	final Iterable<Pair<K, StructuredData<K,V>>> baseIter;
	final Predicate<Pair<K,StructuredData<K,V>>> fn;
	
	public FilterIterableKeyTuple(Iterable<Pair<K, StructuredData<K,V>>> baseIter, 
			Predicate<Pair<K,StructuredData<K,V>>> fn) {
		this.baseIter = baseIter;
		this.fn = fn;
	}

	@Override
	public Iterator<Pair<K, StructuredData<K,V>>> iterator() {
		return new FilterIteratorKeyTuple<K,V>(baseIter, fn);
	}
}