/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.repository.type;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import edu.upenn.cis.db.habitat.Config;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;

public class StructuredCSVWriter implements Collection<MutableTupleWithSchema<String>>, TableWithVariableSchema {
	Set<BasicSchema> schemas = new HashSet<>();
	
	PrintWriter writer;
	List<MutableTupleWithSchema<String>> linesWritten = new ArrayList<>();
	int rows = 0;
	
	final String output;
	
	final int bufLen;
	
	public StructuredCSVWriter(String output) throws IOException {
		this.output = output;
		
		writer = new PrintWriter(new BufferedWriter(new FileWriter(output)));
		
		bufLen = Config.getWriteBufferLength();
	}

	public StructuredCSVWriter(String output, BasicSchema initialSchema) throws IOException {
		this(output);
		addSchema(initialSchema);
	}

	@Override
	public void addSchema(BasicSchema newSchema) {
		schemas.add(newSchema);
	}

	@Override
	public Set<BasicSchema> getSchemas() {
		return schemas;
	}

	@Override
	public int size() {
		return rows;
	}

	@Override
	public boolean isEmpty() {
		return linesWritten.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return linesWritten.contains(o);
	}

	@Override
	public Iterator<MutableTupleWithSchema<String>> iterator() {
		return linesWritten.iterator();
	}

	@Override
	public Object[] toArray() {
		return linesWritten.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return linesWritten.toArray(a);
	}

	@Override
	public boolean add(MutableTupleWithSchema<String> e) {
		if (linesWritten.isEmpty())
			writer.println(e.getSchema().toCSV());

		// Trim the buffer of rows
		while (bufLen > -1 && 
				linesWritten.size() >= bufLen)
			linesWritten.remove(0);
			
		linesWritten.add(e);
		rows++;
		
		writer.println(e.toCSV());
		return true;
	}

	@Override
	public boolean remove(Object o) {
		throw new UnsupportedOperationException("Cannot remove record from structured file");
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return linesWritten.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends MutableTupleWithSchema<String>> c) {
		for (MutableTupleWithSchema<String> tuple: c)
			if (!add(tuple))
				return false;
		
		return true;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		throw new UnsupportedOperationException("Cannot remove records from structured file");
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException("Cannot remove records from structured file");
	}

	@Override
	public void clear() {
		linesWritten.clear();
		writer.close();
		try {
			writer = new PrintWriter(new BufferedWriter(new FileWriter(output)));
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to reset file");
		}
	}

	public void close() {
		writer.close();
	}
}
