/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.util;

import java.util.Iterator;
import java.util.function.Function;

import org.apache.commons.lang3.tuple.Pair;

import edu.upenn.cis.db.habitat.core.type.StructuredData;

public class MapIterableKeyTuple<K,V,V2> implements Iterable<Pair<K,StructuredData<K,V2>>> {
	final Iterable<Pair<K, StructuredData<K,V>>> baseIter;
	final Function<Pair<K,StructuredData<K,V>>,Pair<K,StructuredData<K,V2>>> fn;
	
	public MapIterableKeyTuple(Iterable<Pair<K, StructuredData<K,V>>> baseIter, 
			Function<Pair<K,StructuredData<K,V>>,Pair<K,StructuredData<K,V2>>> fn) {
		this.baseIter = baseIter;
		this.fn = fn;
	}

	@Override
	public Iterator<Pair<K, StructuredData<K,V2>>> iterator() {
		return new MapIteratorKeyTuple<K,V,V2>(baseIter, fn);
	}
}