/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.repository.type;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.google.common.collect.Multiset;

import edu.upenn.cis.db.habitat.core.api.StorageApi;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.Schema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

public class RelationalTableWithSchema implements TableWithSchema, Multiset<MutableTupleWithSchema<String>> {
	String name;
	BasicSchema schema;
	StorageApi<String,Object,?> storage;
	
	public RelationalTableWithSchema(String name, BasicSchema schema,
			StorageApi<String,Object,?> storageSystem) {
		this.schema = schema;
		this.name = name;
	}

	@Override
	public int size() {
		try {
			return storage.getCount(name);
		} catch (HabitatServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	}
	@Override
	public void addSchema(BasicSchema newSchema) {
		schema = newSchema;
	}

	@Override
	public Set<BasicSchema> getSchemas() {
		Set<BasicSchema> ret = new HashSet<>();
		ret.add(schema);
		return ret;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean contains(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterator<MutableTupleWithSchema<String>> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean add(MutableTupleWithSchema<String> e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean remove(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(Collection<? extends MutableTupleWithSchema<String>> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int add(MutableTupleWithSchema<String> arg0, int arg1) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int count(Object arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Set<MutableTupleWithSchema<String>> elementSet() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<com.google.common.collect.Multiset.Entry<MutableTupleWithSchema<String>>> entrySet() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int remove(Object arg0, int arg1) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int setCount(MutableTupleWithSchema<String> arg0, int arg1) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean setCount(MutableTupleWithSchema<String> arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Schema<String, Class> getSchema() {
		// TODO Auto-generated method stub
		return null;
	}

}
