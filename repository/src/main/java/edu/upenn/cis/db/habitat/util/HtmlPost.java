/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * From https://stackoverflow.com/questions/28158475/parse-post-body-to-java-object-using-spark
 * @author Pablo Martinez Gomez
 *
 */
public class HtmlPost {
	  public static Map<String, Object> asMap(String urlencoded) throws UnsupportedEncodingException {
		    return asMap(urlencoded, "UTF-8");
		  }

		  @SuppressWarnings("unchecked")
		  public static Map<String, Object> asMap(String urlencoded, String encoding) throws UnsupportedEncodingException {

		    Map<String, Object> map = new LinkedHashMap<>();

		    for (String keyValue : urlencoded.trim().split("&")) {

		      String[] tokens = keyValue.trim().split("=");
		      String key = tokens[0];
		      String value = tokens.length == 1 ? null : URLDecoder.decode(tokens[1], encoding);

		      String[] keys = key.split("\\.");
		      Map<String, Object> pointer = map;

		      for (int i = 0; i < keys.length - 1; i++) {

		        String currentKey = keys[i];
		        Map<String, Object> nested = (Map<String, Object>) pointer.get(keys[i]);

		        if (nested == null) {
		          nested = new LinkedHashMap<>();
		        }

		        pointer.put(currentKey, nested);
		        pointer = nested;
		      }

		      pointer.put(keys[keys.length - 1], value);
		    }

		    return map;
		  }}
