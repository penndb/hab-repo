/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.util;

import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.Field;

/**
 * @author John Frommeyer
 *
 */
public class BasicTupleDeserializer extends StdDeserializer<BasicTuple> {

	public BasicTupleDeserializer() {
		super(BasicTuple.class);
	}

	private static final long serialVersionUID = 1L;

	@Override
	public BasicTuple deserialize(JsonParser p,
			DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		final JsonDeserializer<Object> fieldDeserializer = ctxt
				.findRootValueDeserializer(ctxt.getTypeFactory().constructType(
						Field.class));
		BasicSchema schema = null;
		ImmutableMap<String, Field<?>> orderedMap = null;

		final String firstFieldName = p.nextFieldName();
		if (firstFieldName == null) {
			throw new JsonMappingException(p,
					"missing expected fields 'schema' and 'tuple'");
		} else if (firstFieldName.equals("schema")) {
			schema = parseSchema(p, ctxt);
		} else if (firstFieldName.equals("tuple")) {
			orderedMap = parseTuple(fieldDeserializer, p, ctxt);
		} else {
			throw new JsonMappingException(p, "Unknown field in Tuple: ["
					+ firstFieldName + "]. Expected 'schema' and 'tuple'");
		}

		final String secondFieldName = p.nextFieldName();
		if (secondFieldName == null) {
			final String missingField = firstFieldName.equals("schema") ? "tuple"
					: "schema";
			throw new JsonMappingException(p,
					"missing expected field '" + missingField + "'");
		} else if (secondFieldName.equals("schema") && schema == null) {
			schema = parseSchema(p, ctxt);
		} else if (secondFieldName.equals("tuple") && orderedMap == null) {
			orderedMap = parseTuple(fieldDeserializer, p, ctxt);
		} else if (secondFieldName.equals("schema")) {
			throw new JsonMappingException(p, "found two schemas in Tuple");
		} else if (secondFieldName.equals("tuple")) {
			throw new JsonMappingException(p, "found two tuples in Tuple");
		} else {
			throw new JsonMappingException(p, "Unknown field in Tuple: ["
					+ secondFieldName + "]. Expected 'schema' and 'tuple'");
		}

		final JsonToken endObject = p.nextToken();
		if (endObject != JsonToken.END_OBJECT) {
			throw new JsonMappingException(p, "expected end object, got: "
					+ p.getText());
		}

		final ArrayList<Field<?>> fields = new ArrayList<>(orderedMap.values());
		BasicTuple tuple = schema.createTuple(fields);
		return tuple;
	}

	private BasicSchema parseSchema(JsonParser p, DeserializationContext ctxt)
			throws IOException, JsonMappingException {
		p.nextToken();
		if (!p.isExpectedStartArrayToken()) {
			throw new JsonMappingException(p,
					"expected start array to deserialize BasicSchema");
		}
		final BasicSchema schema = ctxt.readValue(p, BasicSchema.class);
		return schema;
	}

	/**
	 * Expects tuple to look like [[ fieldName1, field1], [fieldName2, field2],
	 * ... ]
	 * 
	 * @param p
	 * @param ctxt
	 * @return
	 * @throws IOException
	 */
	private ImmutableMap<String, Field<?>> parseTuple(
			JsonDeserializer<Object> fieldDeserializer,
			JsonParser p,
			DeserializationContext ctxt)
			throws IOException {
		final Builder<String, Field<?>> builder = ImmutableMap.builder();
		p.nextToken();
		if (!p.isExpectedStartArrayToken()) {
			throw new JsonMappingException(p, "expected start of array");
		}

		while (true) {
			JsonToken token = p.nextToken();
			if (token == JsonToken.START_ARRAY) {
				parsePair(fieldDeserializer, builder, p, ctxt);
			} else if (token == JsonToken.END_ARRAY) {
				break;
			} else {
				throw new JsonMappingException(p, "unexpected token: "
						+ p.getText());
			}
		}

		return builder.build();
	}

	private void parsePair(
			JsonDeserializer<Object> fieldDeserializer,
			ImmutableMap.Builder<String, Field<?>> builder,
			JsonParser p,
			DeserializationContext ctxt) throws IOException {
		final String key = p.nextTextValue();
		p.nextToken();
		if (!p.isExpectedStartArrayToken()) {
			throw new JsonMappingException(p,
					"expected start array to deserialize Field");
		}
		final Field<?> value = (Field<?>) fieldDeserializer
				.deserialize(p, ctxt);
		builder.put(key, value);
		final JsonToken pairEnd = p.nextToken();
		if (pairEnd != JsonToken.END_ARRAY) {
			throw new JsonMappingException(p, "expected end of array for pair");
		}

	}

}
