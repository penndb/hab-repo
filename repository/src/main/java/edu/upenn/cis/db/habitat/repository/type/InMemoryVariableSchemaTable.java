/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.repository.type;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.MutableTupleWithSchema;

public class InMemoryVariableSchemaTable extends ArrayList<MutableTupleWithSchema<String>> 
		implements TableWithVariableSchema {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	Set<BasicSchema> schemas = new HashSet<>();
	
	public InMemoryVariableSchemaTable(BasicSchema schema) {
		this.schemas.add(schema);
	}

	public InMemoryVariableSchemaTable(Collection<BasicSchema> schemas) {
		this.schemas.addAll(schemas);
	}

	@Override
	public Set<BasicSchema> getSchemas() {
		return schemas;
	}

	@Override
	public void addSchema(BasicSchema newSchema) {
		schemas.add(newSchema);
	}
	
	@Override
	public boolean add(MutableTupleWithSchema<String> tuple) {
//		if (size() > 100000 && size() % 100000 == 0) {
//			System.out.println(schemas.iterator().next().toString() + " at " + size());
//		}
		return super.add(tuple);
	}
	
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		
		for (BasicSchema sch: schemas)
			builder.append(sch.toCSV() + "\n");
		
		super.forEach(tuple -> { builder.append(tuple.toCSV() + "\n"); });
		
		builder.append("" + size() + " tuples\n");
		
		return builder.toString();
	}
}
