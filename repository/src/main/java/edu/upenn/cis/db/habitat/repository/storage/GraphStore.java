/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.repository.storage;

import javax.annotation.Nullable;

import edu.upenn.cis.db.habitat.core.type.TransactionSupplier;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

public interface GraphStore<K> extends StorageApiWithMetadataLocal<K,Object,Object> {
	
	/**
	 * Creates a new graph, and assigns admin permissions to the logged-in user
	 * 
	 * @param resourceName Name of the graph resource
	 * @throws HabitatServiceException
	 */
	public void createGraph(String resourceName) throws HabitatServiceException;
	

	/**
	 * If the graph doesn't exist, creates a new graph, and assigns admin 
	 * permissions to the logged-in user.
	 * Else if the graph exists, clears it out.
	 * 
	 * @param resourceName Name of the graph resource
	 * @throws HabitatServiceException
	 */
	public void createOrResetGraph(String resourceName) throws HabitatServiceException;


	/**
	 * Returns a representation of resource as a sequence of instances of subgraph template
	 * 
	 * @param resource
	 * @param template
	 * @return 
	 * @throws HabitatServiceException 
	 */
	Iterable<SubgraphInstance> getSubgraphs(
			String resource, 
			SubgraphTemplate template,
			int limit,
			@Nullable Long since) throws HabitatServiceException;
	/**
	 * Returns a representation of resource as a sequence of instances of subgraph template
	 * 
	 * @param resource
	 * @param template
	 */
	Iterable<SubgraphInstance> getSubgraphs(
			TransactionSupplier supplier,
			String resource,
			SubgraphTemplate template,
			int limit,
			@Nullable Long since) throws HabitatServiceException;


}
