/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.util;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;

import com.google.common.collect.ImmutableList;

import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.BooleanField;
import edu.upenn.cis.db.habitat.core.type.DoubleField;
import edu.upenn.cis.db.habitat.core.type.Field;
import edu.upenn.cis.db.habitat.core.type.IntField;
import edu.upenn.cis.db.habitat.core.type.MultiField;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifierField;
import edu.upenn.cis.db.habitat.core.type.ProvTokenField;
import edu.upenn.cis.db.habitat.core.type.ProvUniqueToken;
import edu.upenn.cis.db.habitat.core.type.Schema;
import edu.upenn.cis.db.habitat.core.type.StringEncoding;
import edu.upenn.cis.db.habitat.core.type.StringField;
import edu.upenn.cis.db.habitat.core.type.provdm.QualifiedName;
import edu.upenn.cis.db.habitat.core.type.provdm.QualifiedNameField;
import edu.upenn.cis.db.habitat.test.TestUtil;

/**
 * @author John Frommeyer
 *
 */
public class ObjectSerializationTest {

	@Test
	public void basicTupleTest() throws IOException {

		final ImmutableList<String> fieldNames = ImmutableList.of(
				"boolean_field_1",
				"double_field_1",
				"int_field_1",
				"string_field_1",
				"prov_token_field_1",
				"prov_specifier_field_1",
				"qualified_name_field_1",
				"multi_field_int_1",
				"multi_field_qn_1");

		final QualifiedName expectedQualifiedName = TestUtil.newQualifiedName();
		final ImmutableList<Field<?>> fields = ImmutableList.of(
				new BooleanField(true),
				new DoubleField(8.56),
				new IntField(76),
				new StringField("xyx"),
				new ProvTokenField(new ProvUniqueToken()),
				new ProvSpecifierField(new ProvLocation(
						"provLocationTestSource2", newArrayList(1, 2, 3))),
				new QualifiedNameField(expectedQualifiedName),
				new MultiField<>(newHashSet(1, 2, 3)),
				new MultiField<>(newHashSet(TestUtil.newQualifiedName(),
						TestUtil.newQualifiedName())));

		final List<Class<?>> fieldTypes = fields.stream()
				.map((Field<?> field) -> field.getType())
				.collect(Collectors.toList());

		final BasicSchema expectedSchema = new BasicSchema(
				"_OBJECT_SERIALIZATION_TEST_SCHEMA",
				fieldNames,
				fieldTypes);
		expectedSchema.addLookupKeyAt(fieldNames.size() - 2);
		final BasicTuple expectedTuple = expectedSchema.createTuple(fields);

		final Object serialized = ObjectSerialization.putObject(expectedTuple);
		assertTrue(serialized instanceof StringEncoding);
		final StringEncoding actualTupleEncoded = (StringEncoding) serialized;

		// System.out.println(actualTupleEncoded.getValue());

		final Object actualTupleObject = ObjectSerialization
				.getObject(actualTupleEncoded);

		assertTrue(actualTupleObject instanceof BasicTuple);
		final BasicTuple actualTuple = (BasicTuple) actualTupleObject;

		final Schema<String, Class<? extends Object>> actualSchemaSuper = actualTuple
				.getSchema();

		assertTrue(actualSchemaSuper instanceof BasicSchema);
		final BasicSchema actualSchema = (BasicSchema) actualSchemaSuper;

		assertSchemesEqual(expectedSchema, actualSchema);

		assertEquals(expectedTuple.getFields().size(), actualTuple.getFields()
				.size());
		for (int i = 0; i < expectedTuple.getFields().size(); i++) {
			final Field<? extends Object> expectedField = expectedTuple
					.getFields().get(i);
			final Field<? extends Object> actualField = actualTuple.getFields()
					.get(i);
			assertEquals(expectedField.getType(), actualField.getType());
			if (expectedField.getValue() instanceof ProvLocation) {
				// No .equals for ProvLocation
				final ProvLocation expectedLocation = (ProvLocation) expectedField
						.getValue();
				final ProvLocation actualLocation = (ProvLocation) actualField
						.getValue();
				assertEquals(expectedLocation.getField(),
						actualLocation.getField());
				assertEquals(expectedLocation.getPosition(),
						actualLocation.getPosition());
				assertEquals(expectedLocation.getStream(),
						actualLocation.getStream());
			} else {
				assertEquals(expectedField.getValue(), actualField.getValue());
			}
		}
	}

	@Test
	public void basicSchemaTest() throws IOException {

		final ImmutableList<String> fieldNames = ImmutableList.of(
				"boolean_field_1",
				"double_field_1",
				"int_field_1",
				"string_field_1",
				"prov_token_field_1",
				"prov_specifier_field_1",
				"qualified_name_field_1",
				"multi_field_int_1",
				"multi_field_qn_1");

		final QualifiedName expectedQualifiedName = TestUtil.newQualifiedName();
		final ImmutableList<Field<?>> fields = ImmutableList.of(
				new BooleanField(true),
				new DoubleField(8.56),
				new IntField(76),
				new StringField("xyx"),
				new ProvTokenField(new ProvUniqueToken()),
				new ProvSpecifierField(new ProvLocation(
						"provLocationTestSource2", newArrayList(1, 2, 3))),
				new QualifiedNameField(expectedQualifiedName),
				new MultiField<>(newHashSet(1, 2, 3)),
				new MultiField<>(newHashSet(TestUtil.newQualifiedName(),
						TestUtil.newQualifiedName())));

		final List<Class<?>> fieldTypes = fields.stream()
				.map((Field<?> field) -> field.getType())
				.collect(Collectors.toList());

		final BasicSchema expectedSchema = new BasicSchema(
				"_OBJECT_SERIALIZATION_TEST_SCHEMA",
				fieldNames,
				ImmutableList.copyOf(fieldTypes));
		expectedSchema.addLookupKeyAt(2);
		expectedSchema.addLookupKeyAt(3);

		final Object serialized = ObjectSerialization.putObject(expectedSchema);
		assertTrue(serialized instanceof StringEncoding);
		final StringEncoding actualSchemaEncoded = (StringEncoding) serialized;

		// System.out.println(actualSchemaEncoded.getValue());

		final Object actualSchemaObject = ObjectSerialization
				.getObject(actualSchemaEncoded);
		assertTrue(actualSchemaObject instanceof BasicSchema);

		final BasicSchema actualSchema = (BasicSchema) actualSchemaObject;

		assertSchemesEqual(expectedSchema, actualSchema);

	}

	private void assertSchemesEqual(final BasicSchema expectedSchema,
			final BasicSchema actualSchema) {
		assertEquals(expectedSchema.getArity(), actualSchema.getArity());
		assertEquals(expectedSchema.getName(), actualSchema.getName());
		assertEquals(expectedSchema.getTypes(), actualSchema.getTypes());
		assertEquals(expectedSchema.getKeys(), actualSchema.getKeys());
		assertEquals(expectedSchema.getLookupKeys(),
				actualSchema.getLookupKeys());
	}

}
