/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.repository.storage.s3;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import edu.upenn.cis.db.habitat.core.type.StructuredData;

/**
 * Iterator for all items returned from S3 storage
 * 
 * @author zives
 *
 */
public class S3BucketListIterable implements Iterable<Pair<String,StructuredData<String,Object>>> {
	final AmazonS3 client;
	final ObjectListing listing;
	
	public S3BucketListIterable(ObjectListing listing, AmazonS3 client) {
		this.client = client;
		this.listing = listing;
	}

	@Override
	public Iterator<Pair<String, StructuredData<String,Object>>> iterator() {
		List<S3ObjectSummary> objects = listing.getObjectSummaries();

		return new S3BucketListIterator(objects, client);
	}

}
