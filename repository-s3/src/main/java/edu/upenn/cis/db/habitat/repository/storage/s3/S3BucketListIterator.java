/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.repository.storage.s3;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.StructuredValue;
import edu.upenn.cis.db.habitat.util.ObjectSerialization;

/**
 * Iterator for all items returned from S3 storage
 * 
 * @author zives
 *
 */
public class S3BucketListIterator implements Iterator<Pair<String,StructuredData<String,Object>>> {
	final AmazonS3 client;
	final Iterator<S3ObjectSummary> iter;
	
	S3ObjectSummary next = null;
	
	public S3BucketListIterator(Iterable<S3ObjectSummary> objects, AmazonS3 client) {
		this.client = client;
		this.iter = objects.iterator();
		getNext();
	}
	
	boolean getNext() {
		if (!iter.hasNext())
			return false;
		
		next = iter.next();
		
		return true;
	}

	@Override
	public boolean hasNext() {
		if (next != null)
			return true;
		if (!iter.hasNext())
			return false;
		else
			return getNext();
	}

	@Override
	public Pair<String, StructuredData<String,Object>> next() {
		if (next != null) {
			String key = next.getKey();
			ObjectMetadata md = client.getObjectMetadata(next.getBucketName(), key);
			
			StructuredData<String,Object> sd = new StructuredValue<>();
			Map<String,String> values = md.getUserMetadata();
			for (String k: values.keySet()) {
				String v = values.get(k);
				try {
					Object o = ObjectSerialization.getObject(v);
					sd.put(k,  o);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			next = null;
			getNext();
			return new ImmutablePair<>(key, sd);
		} else
			return null;
	}


}
