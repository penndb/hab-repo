/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.repository.storage.s3;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSCredentialsProviderChain;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.auth.SystemPropertiesCredentialsProvider;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.securitytoken.AWSSecurityTokenService;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClientBuilder;
import com.amazonaws.services.securitytoken.model.Credentials;
import com.amazonaws.services.securitytoken.model.GetSessionTokenRequest;
import com.amazonaws.services.securitytoken.model.GetSessionTokenResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.io.ByteStreams;

import alex.mojaki.s3upload.MultiPartOutputStream;
import alex.mojaki.s3upload.StreamTransferManager;
import edu.upenn.cis.db.habitat.Config;
import edu.upenn.cis.db.habitat.core.api.StorageApi;
import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.StructuredValue;
import edu.upenn.cis.db.habitat.core.type.TransactionSupplier;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.repository.storage.BlobStore;
import edu.upenn.cis.db.habitat.repository.storage.ObjectDirectory;
import edu.upenn.cis.db.habitat.util.ObjectSerialization;

/**
 * Amazon S3 storage provider
 * 
 * @author zives
 *
 */
public class S3Store implements BlobStore<byte[]> {
	
	static int PORT = 9000;
	
	final static Logger logger = LogManager.getLogger(S3Store.class);
	AWSSecurityTokenService stsClient;
	AmazonS3 s3;
	AWSCredentialsProvider creds;
	final String mainBucket;
	Bucket rootBucket;
	final boolean readWrite;
	
	boolean isMinio = false;
	
	public S3Store(ObjectDirectory directory, String bucket, boolean readWrite) throws SQLException, IOException {
		BasicAWSCredentials awsCreds = new BasicAWSCredentials(
				Config.getAwsUser(), Config.getAwsPassword());

		creds = new AWSCredentialsProviderChain(
				new AWSStaticCredentialsProvider(awsCreds),
//                new InstanceProfileCredentialsProvider(true),
//            		new EnvironmentVariableCredentialsProvider(),
                new SystemPropertiesCredentialsProvider()
                    );
		
		this.readWrite = readWrite;
		
		this.mainBucket = bucket;
		
		connect();
	}

	public S3Store(ObjectDirectory directory, String bucket, String user, String password,
			boolean readWrite) throws SQLException, IOException {
		BasicAWSCredentials awsCreds = new BasicAWSCredentials(
				user, password);

		creds = new AWSCredentialsProviderChain(
				new AWSStaticCredentialsProvider(awsCreds)
                    );
		
		this.readWrite = readWrite;
		
		this.mainBucket = bucket;
		
		connect();
	}
	
	@Override
	public void addMetadata(String resource, String key, StructuredData<String, ? extends Object> metadata) throws HabitatServiceException {
		String path = getResourcePath(resource, key);
		ObjectMetadata oldMeta = s3.getObjectMetadata(mainBucket, path);
		ObjectMetadata metadataCopy = new ObjectMetadata();
		for (Object k: metadata.keySet()) {
			Object v = metadata.get(k);
			try {
				metadataCopy.addUserMetadata(k.toString(), ObjectSerialization.putObject(v).toString());
			} catch (JsonProcessingException e) {
				e.printStackTrace();
				logger.debug("Unable to serialize: " + e.getMessage());
			}
		}
		for (String k: oldMeta.getUserMetadata().keySet()) {
			String v = oldMeta.getUserMetadata().get(k);
			metadataCopy.addUserMetadata(k, v);
		}
			
		CopyObjectRequest request = new CopyObjectRequest(mainBucket, path, mainBucket, path)
		      .withSourceBucketName(mainBucket)
		      .withSourceKey(path)
		      .withNewObjectMetadata(metadataCopy);

		s3.copyObject(request);
	}

	@Override
	public void addMetadata(TransactionSupplier supplier, String resource, String key, 
			StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		addMetadata(resource, key, metadata);
	}

	@Override
	public void addMetadata(String resource, String key, StructuredData<String, ? extends Object> metadata, 
			String specialLabel) throws HabitatServiceException {
		String path = getResourcePath(resource, key);
		ObjectMetadata oldMeta = s3.getObjectMetadata(mainBucket, path);
		ObjectMetadata metadataCopy = new ObjectMetadata();
		for (Object k: metadata.keySet()) {
			Object v = metadata.get(k);
			try {
				metadataCopy.addUserMetadata(k.toString(), ObjectSerialization.putObject(v).toString());
			} catch (JsonProcessingException e) {
				e.printStackTrace();
				logger.debug("Unable to serialize: " + e.getMessage());
			}
		}
		for (String k: oldMeta.getUserMetadata().keySet()) {
			String v = oldMeta.getUserMetadata().get(k);
			metadataCopy.addUserMetadata(k, v);
		}
			
		CopyObjectRequest request = new CopyObjectRequest(mainBucket, path, mainBucket, path)
		      .withSourceBucketName(mainBucket)
		      .withSourceKey(path)
		      .withNewObjectMetadata(metadataCopy);

		s3.copyObject(request);
	}

	@Override
	public void addMetadata(TransactionSupplier supplier, String resource, String key, 
			StructuredData<String, ? extends Object> metadata, String specialLabel)
			throws HabitatServiceException {
		addMetadata(resource, key, metadata, specialLabel);
	}

	@Override
	public void connect() throws SQLException {
		EndpointConfiguration endpoint = new EndpointConfiguration(Config.getAwsHost(), Config.getAwsRegion());
		ClientConfiguration clientConfig = new ClientConfiguration();
		clientConfig.setProtocol(Protocol.HTTP);
		
		// Minio / test case
		if (Config.getAwsHost().contains("127.0.0.1") || Config.getAwsHost().contains("localhost")) {
			
			s3 = AmazonS3ClientBuilder.standard().withCredentials(creds)
					.withEndpointConfiguration(endpoint)
					.withClientConfiguration(clientConfig)
					.build();
			
			isMinio = true;

		// Full AWS; let's use a session token request and STS with this one
		} else {
			stsClient = AWSSecurityTokenServiceClientBuilder.standard().withCredentials(creds)
					.build();
			
			GetSessionTokenRequest sessionTokenRequest = new GetSessionTokenRequest();
			GetSessionTokenResult sessionTokenResult = stsClient.getSessionToken(sessionTokenRequest);
			
			Credentials sessionCreds = sessionTokenResult.getCredentials();
			BasicSessionCredentials sessionCredentials =
					new BasicSessionCredentials(sessionCreds.getAccessKeyId(),
							sessionCreds.getSecretAccessKey(),
							sessionCreds.getSessionToken());
			
			s3 = AmazonS3ClientBuilder.standard().withCredentials(
					new AWSStaticCredentialsProvider(sessionCredentials))
					.withEndpointConfiguration(endpoint)
					.withClientConfiguration(clientConfig)
					.build();
			
			isMinio = false;
		}
		
		// Create bucket if need be
		if (!s3.doesBucketExist(mainBucket)) {
			rootBucket = s3.createBucket(mainBucket);
		} else {
			List<Bucket> buckets = s3.listBuckets();
	        for (Bucket b : buckets) {
	            if (b.getName().equals(mainBucket)) {
	                rootBucket = b;
	            }
	        }
		}
	}

	@Override
	public boolean execTransaction(String resource, Function<TransactionSupplier, Boolean> transactionFn)
			throws HabitatServiceException {
		throw new UnsupportedOperationException("S3 storage does not support transactions");
	}

	@Override
	public int getCount(String resource) throws HabitatServiceException {
		Iterator<Pair<String,StructuredData<String,Object>>> iter = 
				this.getMetadataForResource(resource).iterator();
		
		int count = 0;
		while (iter.hasNext()) {
			iter.next();
			count++;
		}
		return count;
	}

	@Override
	public byte[] getData(String resource, String key) throws HabitatServiceException {
		try (S3Object obj = s3.getObject(mainBucket, getResourcePath(resource, key))) {
			try (S3ObjectInputStream stream = obj.getObjectContent()) {
				return ByteStreams.toByteArray(stream);
			}
		} catch (Exception e) {
			throw new HabitatServiceException(e.getMessage());
		}
	}

	@Override
	public byte[] getData(TransactionSupplier supplier, String resource, String key) throws HabitatServiceException {
		return getData(resource, key);
	}
	
	@Override
	public String getDatabase() {
		return mainBucket;
	}

	@Override
	public String getHost() {
		return edu.upenn.cis.db.habitat.Config.getAwsHost();
	}

	@Override
	public StructuredData<String, Object> getMetadata(String resource, String key) throws HabitatServiceException {
		String path = getResourcePath(resource, key);
		ObjectMetadata md = s3.getObjectMetadata(mainBucket, path);
		Map<String,String> values = md.getUserMetadata();
		StructuredData<String,Object> ret = new StructuredValue<>();
		for (String k: values.keySet()) {
			String v = values.get(k);
			try {
				Object o = ObjectSerialization.getObject(v);
				ret.put(k,  o);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return ret;
	}

	@Override
	public StructuredData<String, Object> getMetadata(TransactionSupplier supplier, String resource, String key)
			throws HabitatServiceException {
		return getMetadata(resource, key);
	}

	@Override
	public Iterable<Pair<String,StructuredData<String,Object>>> getMetadataForResource(String resource)
			throws HabitatServiceException {
		
		ObjectListing ol = s3.listObjects(mainBucket, getResourcePath(resource));
		
		return new S3BucketListIterable(ol, s3);
	}

	@Override
	public Iterable<Pair<String,StructuredData<String,Object>>> getMetadataForResource(TransactionSupplier supplier,
			String resource) throws HabitatServiceException {
		return getMetadataForResource(resource);
	}

	@Override
	public int getPort() {
		return PORT;
	}

	protected String getResourcePath(String resource) {
		if (File.separatorChar == '\\') {
			return Paths.get(resource).toString().replace('\\',  '/');
		} else
			return Paths.get(resource).toString();
	}
	
	protected String getResourcePath(String resource, String key) {
		if (File.separatorChar == '\\') {
			return Paths.get(resource, key).toString().replace('\\',  '/');
		} else
			return Paths.get(resource, key).toString();
	}

	@Override
	public StoreClass getStoreClass() {
		return StorageApi.StoreClass.GRAPH;
	}

	@Override
	public String getStoreUuid() {
		return "s3-001-" + mainBucket;
	}

	@Override
	public String getSuperuser() {
		return edu.upenn.cis.db.habitat.Config.getAwsUser();
	}

	@Override
	public boolean isDataLegal(StructuredData<String, ? extends Object> metadata) {
		return true;
	}

	@Override
	public boolean isSuitableForMetadata() {
		return false;
	}

	@Override
	public boolean isSuitableToStore(Object obj) {
		return true;
	}

	@Override
	public void removeKey(String resource, String key) throws HabitatServiceException {
		s3.deleteObject(mainBucket, getResourcePath(resource, key));
	}

	@Override
	public void removeKey(TransactionSupplier supplier, String resource, String key) throws HabitatServiceException {
		removeKey(resource, key);
	}

	@Override
	public void removeMetadata(String resource, String key, String metadataField) throws HabitatServiceException {
		String path = getResourcePath(resource, key);
		ObjectMetadata oldMeta = s3.getObjectMetadata(mainBucket, path);
		ObjectMetadata metadataCopy = new ObjectMetadata();
		for (String k: oldMeta.getUserMetadata().keySet()) {
			String v = oldMeta.getUserMetadata().get(k);
			if (!v.equals(metadataField))
				metadataCopy.addUserMetadata(k, v);
		}
			
		CopyObjectRequest request = new CopyObjectRequest(mainBucket, path, mainBucket, path)
		      .withSourceBucketName(mainBucket)
		      .withSourceKey(path)
		      .withNewObjectMetadata(metadataCopy);

		s3.copyObject(request);
	}

	@Override
	public void removeMetadata(TransactionSupplier supplier, String resource, String key, String metadataField)
			throws HabitatServiceException {
		removeMetadata(resource, key, metadataField);
	}

	@Override
	public void replaceMetadata(String resource, String key, StructuredData<String, ? extends Object> metadata) throws HabitatServiceException {
		String path = getResourcePath(resource, key);

		ObjectMetadata metadataCopy = new ObjectMetadata();
		for (Object k: metadata.keySet()) {
			Object v = metadata.get(k);
			try {
				metadataCopy.addUserMetadata(k.toString(), ObjectSerialization.putObject(v).toString());
			} catch (JsonProcessingException e) {
				e.printStackTrace();
				logger.debug("Unable to serialize: " + e.getMessage());
			}
		}
			
		CopyObjectRequest request = new CopyObjectRequest(mainBucket, path, mainBucket, path)
		      .withSourceBucketName(mainBucket)
		      .withSourceKey(path)
		      .withNewObjectMetadata(metadataCopy);

		s3.copyObject(request);
	}

	@Override
	public void replaceMetadata(TransactionSupplier supplier, String resource, String key, StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		replaceMetadata(resource, key, metadata);
	}

	@Override
	public void store(String resource, String key, byte[] value, StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		try {
			String fileName = getResourcePath(resource, key);
            InputStream stream = new ByteArrayInputStream(value);
            ObjectMetadata meta = new ObjectMetadata();
            meta.setContentLength(value.length);
            meta.setContentType("application/octet-stream");
            s3.putObject(mainBucket, fileName, stream, meta);
            
            if (!isMinio) {
	            s3.setObjectAcl(mainBucket, fileName, CannedAccessControlList.BucketOwnerFullControl);
	            s3.setObjectAcl(mainBucket, fileName, CannedAccessControlList.AwsExecRead);
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
		
	}

	@Override
	public void store(TransactionSupplier supplier, String resource, String key, byte[] value, StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		store(resource, key, value, metadata);
	}

	@Override
	public void storeFile(String resource, String key, InputStream stream, int streamLength, StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
//		try {
//            ObjectMetadata meta = new ObjectMetadata();
//            meta.setContentLength(streamLength);
//            meta.setContentType("application/octet-stream");
//            s3.putObject(mainBucket, key, stream, meta);
//            
//            if (!isMinio) {
//	            s3.setObjectAcl(mainBucket, key, CannedAccessControlList.BucketOwnerFullControl);
//	            s3.setObjectAcl(mainBucket, key, CannedAccessControlList.AwsExecRead);
//            }
//            addMetadata(resource, key, metadata);
//        } catch (Exception ex) {
//            System.out.println(ex);
//        }	
		int numStreams = 1;
	    int numUploadThreads = 1;
	    int queueCapacity = 2;
	    int partSize = 5;

	    // Upload the stream as a multipart upload
	    final StreamTransferManager manager = 
	    		new StreamTransferManager(mainBucket, key, s3, numStreams,
	            numUploadThreads, queueCapacity, partSize);
	    final List<MultiPartOutputStream> streams = manager.getMultiPartOutputStreams();
	    
	    try {
			final int TRANSFER_SIZE = 1048576;
			byte[] buffer = new byte[TRANSFER_SIZE];
			int read = 0;
			do {
				read = stream.read(buffer);
				streams.get(0).write(buffer, 0, read);
            	streams.get(0).checkSize();
			} while (read > 0);
			streams.get(0).close();
			
            if (!isMinio) {
	            s3.setObjectAcl(mainBucket, key, CannedAccessControlList.BucketOwnerFullControl);
	            s3.setObjectAcl(mainBucket, key, CannedAccessControlList.AwsExecRead);
            }
            addMetadata(resource, key, metadata);
	    } catch (IOException e) {
	    	e.printStackTrace();
	    	logger.error(e.getMessage());
	    	throw new HabitatServiceException(e.getMessage());
	    } catch (InterruptedException e) {
	    	e.printStackTrace();
	    	logger.error(e.getMessage());
	    	throw new HabitatServiceException(e.getMessage());
	    } finally {
		    // Finishing off
		    manager.complete();
	    }
	}

	@Override
	public void storeFile(TransactionSupplier supplier, String resource, String key, 
			InputStream file, int streamLength, 
			StructuredData<String, ? extends Object> metadata) throws HabitatServiceException {
		storeFile(resource, key, file, streamLength, metadata);
	}
	
	@Override
	public void storeUrlContents(String resource, String key, URL url, StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		InputStream input;
		/*		
		// Alternate code: create temp file, download, write to S3
		try {
			File scratchFile = File.createTempFile("habitat", ".tmph");
			try {
				input = url.openStream();
				Files.copy(input, scratchFile.toPath());
		
				storeFile(resource, key, new FileInputStream(scratchFile), 
						(int)scratchFile.length(), metadata);
			} finally {
				if (scratchFile.exists())
					scratchFile.delete();
			}
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("Could not upload file from URL " + url.toString());
		} */
		int numStreams = 1;
	    int numUploadThreads = 1;
	    int queueCapacity = 2;
	    int partSize = 5;

	    // Upload the stream as a multipart upload
	    final StreamTransferManager manager = 
	    		new StreamTransferManager(mainBucket, key, s3, numStreams,
	            numUploadThreads, queueCapacity, partSize);
	    final List<MultiPartOutputStream> streams = manager.getMultiPartOutputStreams();
	    
	    try {
			input = url.openStream();

			storeFile(resource, key, input, 0, metadata);
	    } catch (IOException e) {
			e.printStackTrace();
			logger.error("Could not upload file from URL " + url.toString());
			throw new HabitatServiceException(e.getMessage());
	    }
	}

	@Override
	public void storeUrlContents(TransactionSupplier supplier, String resource, String key, URL url,
			StructuredData<String, ? extends Object> metadata) throws HabitatServiceException {
		storeUrlContents(resource, key, url, metadata);
	}

	
}