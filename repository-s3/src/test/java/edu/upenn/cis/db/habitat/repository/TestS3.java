/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.repository;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.UUID;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.db.habitat.core.api.StorageApi;
import edu.upenn.cis.db.habitat.core.type.ProvIntToken;
import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.StructuredValue;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.repository.storage.ObjectDirectory;
import edu.upenn.cis.db.habitat.repository.storage.s3.S3Store;

public class TestS3 {
	static boolean isInited = false;
	
	S3Store store;
	
	@Before
	public void setup() throws SQLException, IOException {
		store = new S3Store(new ObjectDirectory() {

			@Override
			public URL getUrlFromAlias(String alias) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getAliasFromUrl(URL url) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public StorageApi getApiFor(String namespace) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getNameOf(StorageApi api) {
				// TODO Auto-generated method stub
				return null;
			} }, "habitat1", true);
	}

	@Test
	public void testStoreObject() throws SQLException, IOException, HabitatServiceException {
		
		String obj = UUID.randomUUID().toString();
		store.store("first", obj, new byte[] {33, 22, 14, 44, 55}, 
				new StructuredValue<String,Object>());
		
		StructuredData<String,Object> metadata = new StructuredValue<>();
		metadata.put("alpha", 32);
		metadata.put("beta", "xyz");
		metadata.put("gamma", new ProvIntToken(79));
		store.addMetadata("first", obj, metadata);
		
		System.out.println(metadata);

		Iterator<Pair<String, StructuredData<String,Object>>> iter = store.getMetadataForResource("first").iterator();
		
		while (iter.hasNext()) {
			Pair<String, StructuredData<String,Object>> rec = iter.next();
			System.out.println(rec.getLeft() + ": " + rec.getRight());
		}
		
		byte[] arr = store.getData("first", obj);
		
		StructuredData<String,Object> metadata2 = store.getMetadata("first", obj);
		
		System.out.println(metadata2);
		
		assertTrue(arr.length == 5);
		assertTrue(arr[1] == 22);
	}

}
