#!/bin/bash
# Copyright 2019 Trustees of the University of Pennsylvania
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

start_neo4j () {
    export HABSTORE_DATA=$HABSTORE/data
    export HABSTORE_LOGS=$HABSTORE/logs
    if [ ! -d $HABSTORE_DATA ]; then
        mkdir $HABSTORE_DATA
    fi
    if [ ! -d $HABSTORE_LOGS ]; then
        mkdir $HABSTORE_LOGS
    fi
    # See https://neo4j.com/developer/docker/
    docker run --name hab-neo4j --publish=7474:7474 --publish=7687:7687 \
    --volume=$HABSTORE_DATA:/data \
    --volume=$HABSTORE_LOGS:/logs \
    -d neo4j:3.5
}

start_postgres () {
    #export POSTGRES_PASSWORD=###
    export POSTGRES_USER=postgres
    export POSTGRES_DATA=$HOME/db
    if [ ! -d $POSTGRES_DATA ]; then
        mkdir $POSTGRES_DATA
    fi
    # See https://hub.docker.com/_/postgres/
    docker run --name hab-postgres --publish=5432:5432 \
    --volume=$POSTGRES_DATA:/data \
    -e POSTGRES_USER=$POSTGRES_USER \
    -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD \
    -e PGDATA=/data \
    -d postgres
}

# See https://github.com/timescale/timescaledb
# Note the remapping to port 9432
#docker run -d \
#  --name timescaledb \
#  -v $POSTGRES_DATA/ts:/var/lib/postgresql/data \
#  -p 9432:5432 \
#  -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD \
#  -e PGDATA=/var/lib/postgresql/data/timescaledb \
#  timescale/timescaledb postgres \
#  -cshared_preload_libraries=timescaledb

start_minio () {
    export MINIO_DATA=$HOME/minio
    if [ ! -d $MINIO_DATA ]; then
        mkdir $MINIO_DATA
    fi
    # See https://hub.docker.com/r/minio/minio/
    docker run --name hab-minio \
    -e "MINIO_ACCESS_KEY=AKIAIOSFODNN7HABITAT" \
    -e "MINIO_SECRET_KEY=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" \
    -v $MINIO_DATA:/export \
    -p 9000:9000 minio/minio server /export
}
# See https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html
#docker run -p 9200:9200 -e "http.host=0.0.0.0" \
#       -e "transport.host=127.0.0.1" \
#       docker.elastic.co/elasticsearch/elasticsearch:5.4.1

# HDFS + Spark + YARN. See https://github.com/sequenceiq/docker-spark
#docker run -it -p 8088:8088 -p 8042:8042 -p 4040:4040 -h sandbox sequenceiq/spark:1.6.0 bash

# To-do: use docker to run Jupyter with a link to each of these

export HABSTORE=$HOME/habitat
if [ ! -d $HABSTORE ]; then
    mkdir $HABSTORE
fi

if [ "$#" == "0" ]; then
    start_neo4j
    start_postgres
    start_minio
else
    while (( "$#" )); do
        case "$1" in
            neo4j)
                start_neo4j
            ;;
            
            postgres)
                start_postgres
            ;;
            
            minio)
                start_minio
            ;;
            
            *)
                echo $"Unknown container: $1"
                echo "Legal containers are neo4j, postgres, minio. If no containers are listed, all three will be started."
			;;
        esac
        shift
    done
fi
