# README

Habitat repository back-end and client interfaces.  It includes the following, separable services:

1. data storage and annotation (by object ID)
1. security and authorization (federated with OAuth and JSON Web Tokens)
1. provenance

All can be used by a local library, or behind a Web app that includes them as plugins with separate HTTP routes.  There are separate Maven modules for the client vs the back-end implementations, for each of **provenance**, **authorization** (and **-client**), and the main **repository** (and **-client**).

Additionally, there is a Web/REST app in **webapp**.  It is a REST API plus a skeleton of an OAuth web site.  

You will need to ensure the following packages are installed:

1. Neo4J (unless you are compiling `engine_only`)
1. PostgreSQL
1. Minio (unless you are compiling `engine_only`)

Next, log into PostgreSQL (via `sudo -u postgres psql`) and `create database habitat_security`.  Then run `\i create_user_security.sql`.

Go to `core/src/main/resources/config.yaml`.  Edit the user IDs and passwords for `rdbms` to match a root account in PostgreSQL; for `graph` to match the root account for Neo4J; for `blob` to match the settings for Minio.

Build everything via ``mvn clean install``.  You can run:

``java -jar webapp/target/webapp-0.0.1-SNAPSHOT.jar``

for the main Web app and REST server.  If you want to build without the test cases, you may run ``mvn clean install -DskipTests``.  Under Eclipse after running Maven, you may need to go to the **repository** project and add ``target/generated-sources/jooq`` to the build path.

Once you have the basic build running, it is **highly recommended** you change the PostgreSQL, Neo4J, and Minio credentials.

The project is modularized as follows:

1. We separate the project into a series of modules:  **core** represents the abstract interfaces; **repository** represents the storage system(s); **auth** represents authorization; **provenance** represents provenance storage; These are put together in a final **webapp**.  [Further segmentation will ultimately occur as we separate the REST services from the Web interface.]
1. The Web app uses **Spark Framework**, which has an embedded Jetty but uses **routes** that map to microservices.
1. We use **Mustache** as our templating library for static Web pages.  See ``webapp/src/main/resources/public`` for the pages (and images).
1. The Web app is based on a pluggable architecture, using Guice.  Each plugin can register its own routes, 
and specify which ones are to be auth-protected.
1. We make use of **Swagger** to publish all REST APIs.

  * **/swagger** will return an OpenAPI JSON document, describing all APIs.  You can copy and paste this into http://editor.swagger.io/ to get full documentation.
  * **/user is mapped to the UserServices (auth) plugin** in ``auth``.  Right now this is largely a Web-based API but REST calls will soon be added.
  * /provenance is mapped to the ProvServices (provenance storage) that is not yet fleshed out but in ``provenance``.
  * /data will ultimately be mapped to the data services in ``repository``.

# REST API Docs

Please see the Swagger-generated MarkDown files for the [definitions](webapp/docs/definitions.md) and [paths](webapp/docs/paths.md).

# Main modules

## Security and authorization services (**authorization** and **authorization-client**):

* Security objects (users, groups, organizations) and permissions.  See (from core services) the interfaces ``edu.upenn.cis.db.habitat.core.api.UserApi`` and ``...PermissionApi``.
  * We use ``PAC4J`` to handle most of the federated authentication, and Shiro to handle password hashing.
* User authentication (including Google and groundwork for GitHub).  Also SHA-512 encodes a password for local logins and account changes.  Done through the webapp.
* User authorization (permissions on objects) with arbitrary named permissions, nested group support with most specific permissions, for arbitrary named objects.

## Data services (**repository** and **repository-client**):

* See the ``edu.upenn.cis.db.habitat.core.api.StorageApi`` interface.
* We use **JOOQ** as our interface to SQL databases.  JOOQ provides a near-literal-SQL interface in Java.  We do not use true ORMs.
* Currently the GraphStore is implemented, using Neo4J via its REST interface.  Transactions are supported.

## Provenance services:

* Currently we support node and edge insertion / deletion with tuples through the interfaces ``edu.upenn.cis.db.habitat.core.api.ProvenanceApi`` and ``edu.upenn.cis.db.habitat.core.api.ProvenanceGraphApi``.
* We also support PROV-DM entities, agents, etc. through the ``edu.upenn.cis.db.habitat.core.api.ProvDmApi`` interface.
* We will soon add calls for PROV-XML/PROV-N documents.

See the [provenance graph API guide](https://bitbucket.org/penndb/hab-repo/wiki/ProvenanceGraphApi%20Guide) for more information.

# Setup

Install Eclipse, Maven, and Docker.  For the most convenient, self-contained setup:

1. Add a ``POSTGRES_PASSWORD`` environment variable, possibly to your ``~/.bash_profile``.  By default this is ``habitat1`.
1. Run ``bash start-dbs.sh`` to set up and launch neo4j, Postgres, Minio, and other services.
1. Go to ``localhost:7474`` in your browser and set a Neo4J password.  You may want to set ``NEO4J_PASSWORD`` in ~/.bash_profile or make this match ``POSTGRES_PASSWORD``.
1. Update ``core/src/main/resources/config.yaml`` to make sure your passwords and database hosts/ports/etc. match.  You can set the config values to use system environment variables if you prefer.

Next you'll need to run:

1. ``psql -h localhost -U postgres -c 'create database habitat_security'`` to create the SQL database for users and permissions.
1.  ``psql -h localhost -U postgres -d habitat_security -f create_user_security.sql`` to build the SQL tables used for recording users, permissions, etc.  These need to exist for the JOOQ code generator to create corresponding Java classes.

Run ``mvn clean install`` to build the various JARs.  For the final project (**webapp**), we build a "fat jar" for the final result, which is large but nicely self-contained.  **If you are
building `hab-repo` as a support module for other tools, e.g.,
PROVision, you may want to ``mvn -P engine-_only clean install``
which will skip the classes that provide an abstraction layer
over MinIO / S3.**

If the main project fails during authorization, try `cd`ing
into `repository` and running `mvn install`, then backing out
and running `mvn ... clean install -rf :authorization`.

Run ``java -jar webapp/target/webapp-0.0.1-SNAPSHOT.jar`` to launch the Web app.  It needs Postgres and Neo4J to be installed.  You can go to ``localhost:8080`` in your browser to log in via Google OAuth.

