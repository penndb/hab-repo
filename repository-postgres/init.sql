-- create the tables
-- \include authorization/src/main/sql/postgresql/create_user_security.sql
CREATE DATABASE habitat_security;
GRANT ALL PRIVILEGES ON DATABASE habitat_security TO postgres;
\c habitat_security;

create table organizations(id serial primary key,
				name varchar(40),
				street1 varchar(40),
				street2 varchar(40),
				city varchar(42),
				state varchar(20),
				zip varchar(15),
				country varchar(30),
				phone varchar(20),
				web varchar(30)
				);

create table users(userid serial primary key,
				username varchar(20) NOT NULL,
				firstname varchar(30),
				lastname varchar(30),
				street1 varchar(40),
				street2 varchar(40),
				city varchar(42),
				state varchar(20),
				zip varchar(15),
				country varchar(30),
				phone varchar(20),
				primaryEmail varchar(40) NOT NULL,
				title varchar(20),
				organization integer,
				FOREIGN KEY (organization) references organizations,
				UNIQUE(username)
				);

create table groups(group_id serial primary key,
				org_id integer NOT NULL,
				name varchar(30),
				parent_group_id integer,
				FOREIGN KEY (org_id) references organizations,
				FOREIGN KEY (parent_group_id) references groups
				);

create table credentials(userid integer NOT NULL,
				service varchar(30) NOT NULL,
				type_or_protocol varchar(10),
				token varchar(80),
				PRIMARY KEY(userid, service),
				FOREIGN KEY (userid) references users
				);
				
create table permissions(perm_id serial NOT NULL,
				perm_name varchar(20),
				PRIMARY KEY(perm_id),
				unique(perm_name));
				
create table permission_entails(perm_id integer NOT NULL,
       	     			derived_perm_id integer NOT NULL,
				FOREIGN KEY(perm_id) references permissions,
				FOREIGN KEY(derived_perm_id) references permissions);
				
create table group_permissions(group_id integer NOT NULL,
				object_id varchar(32) NOT NULL,
				perm_type integer,
				PRIMARY KEY(group_id, object_id),
				FOREIGN KEY(group_id) references groups,
				FOREIGN KEY(perm_type) references permissions
				);

create table user_permissions(user_id integer NOT NULL,
				object_id varchar(32) NOT NULL,
				perm_type integer,
				FOREIGN KEY(user_id) references users,
				FOREIGN KEY(perm_type) references permissions
				);

create table group_members(group_id integer NOT NULL,
				user_id integer NOT NULL,
				PRIMARY KEY (group_id, user_id),
				FOREIGN KEY (group_id) references groups,
				FOREIGN KEY (user_id) references users);
				

create table organization_members(org_id integer NOT NULL,
				user_id integer NOT NULL,
				PRIMARY KEY (org_id, user_id),
				FOREIGN KEY (org_id) references organizations,
				FOREIGN KEY (user_id) references users);


insert into permissions(perm_name)
       values ('Read');

insert into permissions(perm_name)
       values ('Write');

insert into permissions(perm_name)
       values ('Grant');

insert into permissions(perm_name)
       values ('Admin');


insert into permission_entails(perm_id, derived_perm_id)
       select p1.perm_id, p2.perm_id
       from permissions p1, permissions p2
       where p1.perm_name='Admin' and p2.perm_name='Write';

insert into permission_entails(perm_id, derived_perm_id)
       select p1.perm_id, p2.perm_id
       from permissions p1, permissions p2
       where p1.perm_name='Admin' and p2.perm_name='Read';

insert into permission_entails(perm_id, derived_perm_id)
       select p1.perm_id, p2.perm_id
       from permissions p1, permissions p2
       where p1.perm_name='Admin' and p2.perm_name='Grant';

insert into permission_entails(perm_id, derived_perm_id)
       select p1.perm_id, p2.perm_id
       from permissions p1, permissions p2
       where p1.perm_name='Grant' and p2.perm_name='Write';

insert into permission_entails(perm_id, derived_perm_id)
       select p1.perm_id, p2.perm_id
       from permissions p1, permissions p2
       where p1.perm_name='Grant' and p2.perm_name='Read';

insert into permission_entails(perm_id, derived_perm_id)
       select p1.perm_id, p2.perm_id
       from permissions p1, permissions p2
       where p1.perm_name='Write' and p2.perm_name='Read';

-- Fix token column length
-- \include authorization/src/main/sql/postgresql/alter_credentials_token.sql
alter table credentials alter column token type varchar(255);

