/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.repository;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.Iterator;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.StructuredValue;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.repository.storage.postgresql.PostgresStore;

public class PostgresTest {
	PostgresStore store = null;
	private int testCount = 0;
	
	@Before
	public void setup() throws SQLException {
		store = new PostgresStore();
	}

	@Test
	public void testMetadata() throws HabitatServiceException {
		StructuredData<String,Object> myData = new StructuredValue<>();
		
		myData.put("Field1", "testMetadata");
		myData.put("Field2", new Integer(34));
		
		String graph = "";
		store.execTransaction(graph, s -> {
			try {
				store.removeKey(s, graph, "1234");
				store.addMetadata(s, graph, "1234", myData);
			} catch (HabitatServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				assertTrue(false);
			}
			
			StructuredData<String, Object> data;
			try {
				data = store.getMetadata(s, graph, "1234");
				assertTrue(data.get("Field1").equals("testMetadata"));
			} catch (HabitatServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				assertTrue(false);
			}
			return true;
		});
		
	}

	@Test
	public void testAddMetadata() throws HabitatServiceException {
		StructuredData<String,Object> myData = new StructuredValue<>();
		
		myData.put("Field1", "testMetadata2");
		myData.put("Field2", Integer.valueOf(35));
		
		String graph = "";
		store.execTransaction(graph, s -> {
			try {
				store.removeKey(s, graph, "01234");
				store.addMetadata(s, graph, "01234", myData);
			} catch (HabitatServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				assertTrue(false);
			}
	
			StructuredData<String,Object> newData = new StructuredValue<>();
			newData.put("Field3", "testMetadata2");
			try {
				store.addMetadata(s, graph, "01234", newData);
			} catch (HabitatServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				assertTrue(false);
			}
			
			StructuredData<String, Object> data;
			try {
				data = store.getMetadata(s, graph, "01234");
				assertTrue(data.get("Field1").equals("testMetadata2"));
				assertTrue(data.get("Field3").equals("testMetadata2"));
				assertTrue(data.get("Field2").equals(Integer.valueOf(35)));
			} catch (HabitatServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				assertTrue(false);
			}
			
			return true;
		});
	}

	@Test
	public void testReplaceMetadata() throws HabitatServiceException {
		StructuredData<String,Object> myData = new StructuredValue<>();
		
		myData.put("Field1", "testMetadata3");
		myData.put("Field2", new Integer(34));
		
		String graph = "";
		store.execTransaction(graph, s -> {
			try {
				store.removeKey(s, graph, "012345");
				store.addMetadata(s, graph, "012345", myData);
			} catch (HabitatServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				assertTrue(false);
			}

			StructuredData<String,Object> newData = new StructuredValue<>();
			newData.put("Field3", "testMetadata3");
			try {
				store.replaceMetadata(s, graph, "012345", newData);
			} catch (HabitatServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				assertTrue(false);
			}
			
			StructuredData<String, Object> data;
			try {
				data = store.getMetadata(s, graph, "012345");
				assertFalse(data.isEmpty());
				Object actualField3 = data.get(("Field3"));
				assertNotNull(actualField3);
				assertTrue(actualField3.equals("testMetadata3"));
				assertTrue(!data.containsKey("Field1"));
			} catch (HabitatServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				assertTrue(false);
			}
			
			return true;
		});
	}

	@Test
	public void testData() throws HabitatServiceException {
		StructuredData<String,Object> myData = new StructuredValue<>();
		
		myData.put("Field1", "testData");
		myData.put("Field2", Integer.valueOf(34));
		
		String value = "LiteralValue";
		String graph = "";
		store.execTransaction(graph, s -> {
			try {
				store.removeKey(s, graph, "12345");
				store.store(s, graph, "12345", value, myData);
				Object result = store.getData(s, graph, "12345");
				assertTrue(result.equals(value));
			} catch (HabitatServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				assertTrue(false);
			} catch (UnsupportedOperationException u) {
				System.err.println("Warning: unimplemented data store");
			}
			

			return true;
		});
	}

	@Test
	public void testRemoveMetadata() throws HabitatServiceException {
		StructuredData<String,Object> myData = new StructuredValue<>();
		
		myData.put("Field1", "testMetadata3");
		myData.put("Field2", new Integer(34));
		
		String graph = "";
		store.execTransaction(graph, s -> {
			try {
				store.removeKey(s, graph, "0123457");
				store.addMetadata(s, graph, "0123457", myData);
				store.removeMetadata(s, graph, "0123457", "Field1");
			} catch (HabitatServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				assertTrue(false);
			}
			
	
			StructuredData<String, Object> data;
			try {
				data = store.getMetadata(s, graph, "0123457");
				assertTrue(!data.containsKey("Field1"));
			} catch (HabitatServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				assertTrue(false);
			}
			return true;
		});
	}

	@Test
	public void testLink() throws HabitatServiceException {
		StructuredData<String,Object> myData = new StructuredValue<>();
		
		myData.put("Field1", "test");
		myData.put("Field2", Integer.valueOf(34));
		
		String graph = "";
		store.execTransaction(graph, s -> {
			try {
				store.removeKey(s, graph, "12346");
				store.addMetadata(s, graph, "12346", myData);
			} catch (HabitatServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				assertTrue(false);
			}
	
			StructuredData<String,Object> myData2 = new StructuredValue<>();
			
			myData2.put("Field3", "test");
			myData2.put("Field4", Integer.valueOf(45));
			
			try {
				store.removeKey(s, graph, "4567");
				store.addMetadata(s, graph, "4567", myData2);
			} catch (HabitatServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				assertTrue(false);
			}
			
			try {
				store.link(s, graph, "12346", "edge", "4567", null);
			} catch (HabitatServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				Iterable<Pair<String,StructuredData<String,Object>>> links = store.getLinksFrom(s, graph, "12346");
				
				Iterator<Pair<String,StructuredData<String,Object>>> iter = links.iterator();
				assertTrue(iter.hasNext());
				
				Pair<String,StructuredData<String,Object>> pair = iter.next();
				
				assertTrue(pair.getLeft().equals("4567"));
				
				assertFalse(iter.hasNext());
			} catch (HabitatServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			StructuredData<String,Object> tuple = new StructuredValue<String,Object>();
			tuple.put("sample1", Integer.valueOf(123));
			tuple.put("sample2", "345");
			try {
				store.link(s, graph, "12346", "edge2", "4567", tuple);
			} catch (HabitatServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				Iterable<Pair<String,StructuredData<String,Object>>> links = store.getLinksFrom(s, graph, "12346");
				
				Iterator<Pair<String,StructuredData<String,Object>>> iter = links.iterator();
				assertTrue(iter.hasNext());
				
				Pair<String,StructuredData<String,Object>> pair = iter.next();
				assertTrue(pair.getLeft().equals("4567"));
				
				System.out.println (pair.getRight().toString());
				while (iter.hasNext()) {
					pair = iter.next();
					System.out.println (pair.getRight().toString());
				}
				
				//assertFalse(iter.hasNext());
			} catch (HabitatServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				Iterable<Pair<String,StructuredData<String,Object>>> links = store.getLinksTo(s, graph, "4567");
				
				Iterator<Pair<String,StructuredData<String,Object>>> iter = links.iterator();
				assertTrue(iter.hasNext());

				Pair<String,StructuredData<String,Object>> pair = iter.next();
				
				assertTrue(pair.getLeft().equals("12346"));
				
				assertTrue(iter.hasNext());
			} catch (HabitatServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return true;
		});
	}
	
	//@After
	public void testFinal() throws HabitatServiceException {
		Iterable<Pair<String,StructuredData<String,Object>>> nodes = store.getMetadataForResource("");
		
		Iterator<Pair<String,StructuredData<String,Object>>> iter = nodes.iterator();
		
		int count = 0;
		while (iter.hasNext()) {
//			System.out.println(iter.next());
			iter.next();
			count++;
		}
		System.out.println(count);
		
		assertTrue(count >= ++testCount);
	}
}
