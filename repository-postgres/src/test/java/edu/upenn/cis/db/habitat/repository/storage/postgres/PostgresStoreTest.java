/*
 * Copyright 2019 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.repository.storage.postgres;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;

import com.google.common.io.ByteSource;
import com.google.common.io.Resources;

import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.StructuredValue;
import edu.upenn.cis.db.habitat.repository.storage.postgresql.PostgresStore;

/**
 * @author John Frommeyer
 *
 */
public class PostgresStoreTest {

	private PostgresStore store;

	@Before
	public void setup() throws SQLException {
		store = new PostgresStore();
	}

//	@Test
	public void storeFile() throws Exception {
		final ByteSource templateSource = Resources.asByteSource(
				getClass().getResource("/subgraph-template.json"));

		final String expectedContent = templateSource
				.asCharSource(StandardCharsets.UTF_8).read();

		final InputStream storedStream = templateSource.openBufferedStream();
		store.storeFile("graph-with-file", "subgraph-template.json",
				storedStream,
				expectedContent.length(), new StructuredValue<>());
		boolean exception = false;
		try {
			storedStream.read();
		} catch (IOException e) {
			exception = true;
		}
		// Server should close stream, so we should see an exception.
		assertTrue(exception);
	}

//	@Test
	public void getData() throws Exception {
		final ByteSource templateSource = Resources.asByteSource(
				getClass().getResource("/subgraph-template.json"));

		final String expectedContent = templateSource
				.asCharSource(StandardCharsets.UTF_8).read();

		final InputStream storedStream = templateSource.openBufferedStream();
		store.storeFile("graph-with-file", "subgraph-template.json",
				storedStream,
				expectedContent.length(), new StructuredValue<>());

		final Object actualData = store.getData("graph-with-file",
				"subgraph-template.json");
		assertTrue(actualData instanceof String);
		assertEquals(expectedContent, actualData);

	}
	
	@Test
	public void getDataMissingData() throws Exception {
		final Object noData = store.getData("graph-without-file",
				"subgraph-template.json");
		assertNull(noData);

	}
}
