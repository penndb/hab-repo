/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.repository.storage.postgres.test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.ImmutableList;

import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate.NodeInfo;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvDmRelation;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvDmType;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.repository.storage.postgresql.PostgresStore;

/**
 * @author John Frommeyer
 *
 */
public class SubgraphQueryTest {

	public static void main(String[] args) throws SQLException,
			HabitatServiceException, JsonProcessingException {
		final SubgraphTemplate template = getTemplate();
		final Long since = 0L;
		final String graph = "test-graph";
		final PostgresStore store = new PostgresStore();
		final Iterable<SubgraphInstance> subgraphs = store.getSubgraphs(
				graph,
				template,
				400,
				since);
		for (final SubgraphInstance subgraph : subgraphs) {
			System.out.println(subgraph);
		}
		store.close();
	}

	private static SubgraphTemplate getTemplate() {
		final List<List<NodeInfo>> ranks = new ArrayList<>();
		final List<SubgraphTemplate.NodeInfo> inputNodes = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			inputNodes.add(new SubgraphTemplate.NodeInfo("input" + i,
					ProvDmType.ENTITY.name(), false));
		}
		ranks.add(inputNodes);
		ranks.add(ImmutableList.of(
				new SubgraphTemplate.NodeInfo("window",
						ProvDmType.COLLECTION.name(), false)));
		ranks.add(ImmutableList.of(
				new SubgraphTemplate.NodeInfo("activity", ProvDmType.ACTIVITY.name(),
						false),
				new SubgraphTemplate.NodeInfo("derivation",
						ProvDmRelation.DERIVATION.name(), false)));
		ranks.add(
				ImmutableList.of(
						new SubgraphTemplate.NodeInfo("output",
								ProvDmType.ENTITY.name(), true)));

		final List<SubgraphTemplate.LinkInfo> links = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			links.add(new SubgraphTemplate.LinkInfo("window", "input" + i,
					ProvDmRelation.MEMBERSHIP.name()));
		}

		links.add(new SubgraphTemplate.LinkInfo("activity", "window",
				ProvDmRelation.USAGE.name()));
		links.add(new SubgraphTemplate.LinkInfo("activity", "window",
				ProvDmRelation.START.name()));
		links.add(new SubgraphTemplate.LinkInfo("derivation", "window",
				ProvDmRelation.DERIVATION.name()));
		links.add(new SubgraphTemplate.LinkInfo("derivation", "activity",
				ProvDmRelation.DERIVATION.name()));
		links.add(new SubgraphTemplate.LinkInfo("output", "derivation",
				ProvDmRelation.DERIVATION.name()));
		links.add(new SubgraphTemplate.LinkInfo("output", "activity",
				ProvDmRelation.GENERATION.name()));

		final SubgraphTemplate template = new SubgraphTemplate(ranks, links);
		return template;
	}
}
