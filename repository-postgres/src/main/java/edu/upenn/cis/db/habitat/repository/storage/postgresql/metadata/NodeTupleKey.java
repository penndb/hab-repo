/*
 * Copyright 2021 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.repository.storage.postgresql.metadata;

import java.util.Objects;

public class NodeTupleKey {
	public final String resource;
	public final String nodeKey;
	public final String tupleName;

	public NodeTupleKey(String resource, String nodeKey, String tupleName) {
		this.resource = resource;
		this.nodeKey = nodeKey;
		this.tupleName = tupleName;
	}

	@Override
	public int hashCode() {
		return Objects.hash(nodeKey, resource, tupleName);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof NodeTupleKey)) {
			return false;
		}
		NodeTupleKey other = (NodeTupleKey) obj;
		return Objects.equals(nodeKey, other.nodeKey)
				&& Objects.equals(resource, other.resource)
				&& Objects.equals(tupleName, other.tupleName);
	}
}
