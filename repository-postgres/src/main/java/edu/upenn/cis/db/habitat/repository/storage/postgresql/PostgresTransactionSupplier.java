package edu.upenn.cis.db.habitat.repository.storage.postgresql;

import org.jooq.DSLContext;

import edu.upenn.cis.db.habitat.core.type.TransactionSupplier;

public class PostgresTransactionSupplier implements TransactionSupplier {
	DSLContext context;
	
	public PostgresTransactionSupplier(DSLContext conn) {
		context = conn;
	}

	@Override
	public DSLContext getTransactionObject() {
		return context;
	}
}
