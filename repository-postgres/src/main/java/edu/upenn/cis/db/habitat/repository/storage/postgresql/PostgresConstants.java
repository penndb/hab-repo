/*
 * Copyright 2021 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.repository.storage.postgresql;

import static edu.upenn.cis.db.habitat.repository.storage.postgresql.PostgresStore.EDGES;
import static edu.upenn.cis.db.habitat.repository.storage.postgresql.PostgresStore.EDGE_PROPS;
import static edu.upenn.cis.db.habitat.repository.storage.postgresql.PostgresStore.NODES;
import static edu.upenn.cis.db.habitat.repository.storage.postgresql.PostgresStore.SCHEMAS;
import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;
import static org.jooq.impl.DSL.table;

import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Table;

/**
 * 
 * @author John Frommeyer
 *
 */
public class PostgresConstants {
	public static final Table<Record> HAB_NODE = table(name(NODES));
	public static final Table<Record> HAB_EDGE = table(name(EDGES));
	public static final Table<Record> HAB_EDGEPROP = table(name(EDGE_PROPS));
	public static final Table<Record> HAB_SCHEMA = table(name(SCHEMAS));
	
	public static final Field<String> HAB_NODE_KEY = field(name(NODES, "_key"),
			String.class);
	public static final Field<String> HAB_NODE_RESOURCE = field(
			name(NODES, "_resource"), String.class);
	public static final Field<String> HAB_NODE_LABEL = field(
			name(NODES, "label"), String.class);
	public static final Field<Integer> HAB_NODE_SCHEMA = field(
			name(NODES, "schema_key"), Integer.class);

	public static final Field<Integer> HAB_EDGE_KEY = field(name(EDGES, "_key"),
			Integer.class);
	public static final Field<String> HAB_EDGE_FROM = field(
			name(EDGES, "_from"), String.class);
	public static final Field<String> HAB_EDGE_TO = field(name(EDGES, "_to"),
			String.class);
	public static final Field<String> HAB_EDGE_RESOURCE = field(
			name(EDGES, "_resource"), String.class);
	public static final Field<String> HAB_EDGE_LABEL = field(
			name(EDGES, "label"), String.class);

	public static final Field<String> HAB_EDGEPROP_LABEL = field(
			name(EDGE_PROPS, "label"), String.class);
	public static final Field<String> HAB_EDGEPROP_VALUE = field(
			name(EDGE_PROPS, "value"), String.class);

	public static final Field<Integer> HAB_SCHEMA_KEY = field(
			name(SCHEMAS, "_key"),
			Integer.class);
	public static final Field<String> HAB_SCHEMA_RESOURCE = field(
			name(SCHEMAS, "_resource"), String.class);
	public static final Field<String> HAB_SCHEMA_NAME = field(
			name(SCHEMAS, "name"),
			String.class);
	public static final Field<String> HAB_SCHEMA_VALUE = field(
			name(SCHEMAS, "value"), String.class);

	private PostgresConstants() {}
}