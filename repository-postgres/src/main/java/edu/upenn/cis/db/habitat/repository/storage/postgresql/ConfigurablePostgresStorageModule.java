/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.repository.storage.postgresql;

import java.net.URI;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import com.google.inject.util.Providers;

import edu.upenn.cis.db.habitat.Config;

/**
 *
 */
public class ConfigurablePostgresStorageModule extends AbstractModule {

	private final URI connectionUri;
	private final String user;
	private final String password;

	public ConfigurablePostgresStorageModule(
			URI connectionUri,
			String user,
			String password) {
		this.connectionUri = connectionUri;
		this.user = user;
		this.password = password;
	}

	public ConfigurablePostgresStorageModule() {
		this(URI.create("jdbc:postgresql://" + Config.getRdbmsHost() + ":5432/"),
				Config.getPostgresUser(),
				Config.getRdbmsPassword());
	}

	@Override
	protected void configure() {

		bind(URI.class)
				.annotatedWith(
						Names.named(PostgresStore.POSTGRES_URI_PARAM))
				.toInstance(connectionUri);
		bind(String.class)
				.annotatedWith(Names.named(PostgresStore.POSTGRES_USER_PARAM))
				.toProvider(Providers.of(user));
		bind(String.class)
				.annotatedWith(Names.named(PostgresStore.POSTGRES_PASSWORD_PARAM))
				.toProvider(Providers.of(password));

		// bind(new TypeLiteral<GraphStore<String>>() {
		// }).to(Neo4JStore.class);

		// bind(new TypeLiteral<StorageApiWithMetadataLocal<String, Object, ?>>() {
		// })
		// 		.to(Neo4JStore.class);

	}
}