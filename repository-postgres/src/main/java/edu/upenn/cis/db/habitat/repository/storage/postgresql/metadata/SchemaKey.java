package edu.upenn.cis.db.habitat.repository.storage.postgresql.metadata;

import java.util.Objects;

import org.jooq.Record;

import edu.upenn.cis.db.habitat.core.type.BasicSchema;

public class SchemaKey {
	public final String name;
	public final String resource;

	public SchemaKey(String resource, String name) {
		this.resource = resource;
		this.name = name;
	}
	
	public SchemaKey(String resource, BasicSchema sch) {
		this.resource = resource;
		this.name = sch.getName();
	}

	public static SchemaKey fromRecord(Record record) {
		return new SchemaKey(record.get(1, String.class),
				record.get(2, String.class));
	}

	public static Integer schemaIdFromRecord(Record r) {
		return r.get(0, Integer.class);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, resource);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof SchemaKey)) {
			return false;
		}
		SchemaKey other = (SchemaKey) obj;
		return Objects.equals(name, other.name)
				&& Objects.equals(resource, other.resource);
	}

	@Override
	public String toString() {
		return "SchemaKey [name=" + name + ", resource=" + resource + "]";
	}
	
	
}