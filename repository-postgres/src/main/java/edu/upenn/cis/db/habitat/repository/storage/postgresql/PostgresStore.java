/************************************************
 * Copyright 2019-21 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.repository.storage.postgresql;

import static org.jooq.impl.DSL.condition;
import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;
import static org.jooq.impl.DSL.select;
import static org.jooq.impl.DSL.selectFrom;
import static org.jooq.impl.DSL.table;
import static org.jooq.impl.DSL.val;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import javax.annotation.Nullable;
import javax.sql.DataSource;

import org.apache.commons.dbcp2.ConnectionFactory;
import org.apache.commons.dbcp2.DriverManagerConnectionFactory;
import org.apache.commons.dbcp2.PoolableConnection;
import org.apache.commons.dbcp2.PoolableConnectionFactory;
import org.apache.commons.dbcp2.PoolingDataSource;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.Configuration;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Record10;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.name.Named;

import edu.upenn.cis.db.habitat.core.api.StorageApi;
import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.DateField;
import edu.upenn.cis.db.habitat.core.type.DoubleField;
import edu.upenn.cis.db.habitat.core.type.Field;
import edu.upenn.cis.db.habitat.core.type.IntField;
import edu.upenn.cis.db.habitat.core.type.LongField;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.ProvStringToken;
import edu.upenn.cis.db.habitat.core.type.StringEncoding;
import edu.upenn.cis.db.habitat.core.type.StringField;
import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.StructuredValue;
import edu.upenn.cis.db.habitat.core.type.TransactionSupplier;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.repository.storage.GraphStore;
import edu.upenn.cis.db.habitat.repository.storage.postgresql.metadata.SchemaKey;
import edu.upenn.cis.db.habitat.util.ObjectSerialization;

public class PostgresStore implements // RelationStore,
		GraphStore<String> {
	public static final String POSTGRES_URI_PARAM = "PostgresUri";
	public static final String POSTGRES_USER_PARAM = "PostgresUser";
	public static final String POSTGRES_PASSWORD_PARAM = "PostgresPassword";

	static final String NODES = "Hab_Node";
	static final String SCHEMAS = "Hab_Schema";
	static final String EDGES = "Hab_Edge";
	static final String NODE_PROPS = "Hab_NodeProp";
	static final String EDGE_PROPS = "Hab_EdgeProp";
	
	/** Generic Object Mapper when we know the types */
	static ObjectMapper om = new ObjectMapper();

	final URI postgresUri;
	final String postgresUser;
	final String postgresPassword;

	private final Map<SchemaKey, String> schemaKeyToUuid = new ConcurrentHashMap<>();

	final static Logger logger = LogManager.getLogger(PostgresStore.class);

	private static DataSource pool;

	@Inject
	public PostgresStore(
			@Named(POSTGRES_URI_PARAM) URI pgUri,
			@Nullable @Named(POSTGRES_USER_PARAM) String pgUser,
			@Nullable @Named(POSTGRES_PASSWORD_PARAM) String pgPassword)
			throws SQLException {
		postgresUri = pgUri;
		postgresUser = pgUser;
		postgresPassword = pgPassword;

		init(pgUri, pgUser, pgPassword);
		loadSchemas();
	}

	/**
	 * Instantiate any tables that are missing in the graph
	 * 
	 * @throws SQLException
	 */
	private static void createTables() throws SQLException {
		final DSLContext context = DSL.using(pool,
				SQLDialect.POSTGRES);
		
		context
		.createTableIfNotExists(SCHEMAS)
		.column("_key", SQLDataType.VARCHAR(80).nullable(false))
		.column("name", SQLDataType.VARCHAR(80).nullable(false))
		.column("_resource", SQLDataType.VARCHAR(80).nullable(false))
		.column("value", SQLDataType.VARCHAR.nullable(false))
		.constraints(
			DSL.primaryKey("_key"),
			DSL.unique("name", "_resource"))
		.execute();
		
		context
				.createTableIfNotExists(NODES)
				.column("_key", SQLDataType.VARCHAR(80).nullable(false))
				.column("_resource", SQLDataType.VARCHAR(80).nullable(false))
				.column("_created", SQLDataType.INTEGER.identity(true))
				.column("label", SQLDataType.VARCHAR(80).nullable(true))
				.constraints(
						DSL.primaryKey("_key", "_resource"))
				.execute();

		context
				.createTableIfNotExists(EDGES)
				.column("_key", SQLDataType.INTEGER.identity(true))
				.column("_from", SQLDataType.VARCHAR(80).nullable(false))
				.column("_to", SQLDataType.VARCHAR(80).nullable(false))
				.column("_resource", SQLDataType.VARCHAR(80).nullable(false))
				.column("_created", SQLDataType.INTEGER.identity(true))
				.column("label", SQLDataType.VARCHAR(80).nullable(true))
				.constraints(
						DSL.primaryKey("_key", "_resource"),
						DSL.unique("_to", "_from", "label", "_resource"),
						DSL.foreignKey("_from", "_resource").references(NODES)
								.onDeleteCascade(),
						DSL.foreignKey("_to", "_resource").references(NODES)
								.onDeleteCascade())
				.execute();
		
		
		
		context
				.createTableIfNotExists(NODE_PROPS)
				.column("_key", SQLDataType.VARCHAR(80).nullable(false))
				.column("_resource", SQLDataType.VARCHAR(80).nullable(false))
				.column("type", SQLDataType.VARCHAR(80).nullable(true))
				.column("label", SQLDataType.VARCHAR(80).nullable(false))
				.column("value", SQLDataType.VARCHAR.nullable(true))
				.column("code", SQLDataType.CHAR.nullable(true))
				.column("ivalue", SQLDataType.INTEGER.nullable(true))
				.column("lvalue", SQLDataType.BIGINT.nullable(true))
				.column("dvalue", SQLDataType.DOUBLE.nullable(true))
				.column("fvalue", SQLDataType.FLOAT.nullable(true))
				.column("tvalue", SQLDataType.DATE.nullable(true))
				.column("index", SQLDataType.INTEGER.nullable(true))
				.constraints(
						// Remove this if we allow duplicate labels
						DSL.primaryKey("_key", "_resource", "label"),
						DSL.foreignKey("_key", "_resource").references(NODES)
								.onDeleteCascade(),
						DSL.unique("_key", "_resource", "index"))
				.execute();

		context
				.createTableIfNotExists(EDGE_PROPS)
				.column("_key", SQLDataType.INTEGER.nullable(false))
				.column("_resource", SQLDataType.VARCHAR(80).nullable(false))
				.column("type", SQLDataType.VARCHAR(80).nullable(true))
				.column("label", SQLDataType.VARCHAR(80).nullable(false))
				.column("value", SQLDataType.VARCHAR.nullable(true))
				.column("code", SQLDataType.CHAR.nullable(true))
				.column("ivalue", SQLDataType.INTEGER.nullable(true))
				.column("lvalue", SQLDataType.BIGINT.nullable(true))
				.column("dvalue", SQLDataType.DOUBLE.nullable(true))
				.column("fvalue", SQLDataType.FLOAT.nullable(true))
				.column("tvalue", SQLDataType.DATE.nullable(true))
				.column("index", SQLDataType.INTEGER.nullable(true))
				.constraints(
						// Remove this if we allow duplicate labels
						DSL.primaryKey("_key", "_resource", "label"),
						DSL.foreignKey("_key", "_resource").references(EDGES)
								.onDeleteCascade())
				.execute();

		// TODO: consider case where value is an overflow?
	}
	
	private void loadSchemas() {
		DSLContext context = getContextFromSupplier(null);
		
		Result<org.jooq.Record> recs = context.select()
				.from(table(name(SCHEMAS)))
				.fetch();
		for (org.jooq.Record rec : recs) {

			SchemaKey skey = new SchemaKey(rec.get("_resource").toString(), rec.get("_key").toString());
			String name = rec.get("name").toString();
			
			schemaKeyToUuid.put(skey, name);
		}
	}

	/**
	 * Main default initialization
	 * 
	 * @throws SQLException
	 */
	public PostgresStore() throws SQLException {
		this(
				URI.create("jdbc:postgresql://"
						+ edu.upenn.cis.db.habitat.Config.getRdbmsHost() +
						":5432/"),
				edu.upenn.cis.db.habitat.Config.getPostgresUser(),
				edu.upenn.cis.db.habitat.Config.getRdbmsPassword());
	}

	@Override
	public Iterable<String> getKeysMatching(String resource,
			String metadataField,
			Map<String, ? extends Object> match)
			throws HabitatServiceException {
		DSLContext context = getContextFromSupplier(null);
		return getKeysMatching(
				new PostgresTransactionSupplier(context), resource,
				metadataField, match);

	}

	@Override
	public Iterable<String> getKeysMatching(TransactionSupplier supplier,
			String resource, String metadataField,
			Map<String, ? extends Object> match)
			throws HabitatServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException(
				"Support for matching on key-value pairs not ye supported");
	}

	@Override
	public Iterable<Pair<String, StructuredData<String, Object>>> getLinksFrom(
			String resource, String key1)
			throws HabitatServiceException {
		DSLContext context = getContextFromSupplier(null);
		return getLinksFrom(
				new PostgresTransactionSupplier(context), resource, key1);
	}

	@Override
	public Iterable<Pair<String, StructuredData<String, Object>>> getLinksFrom(
			TransactionSupplier supplier,
			String resource, String key1) throws HabitatServiceException {
		List<Pair<String, StructuredData<String, Object>>> ret = new ArrayList<>();

		// LOJ edges with properties
		DSLContext context = (DSLContext) supplier.getTransactionObject();

		Result<org.jooq.Record> recs = context.select()
				.from(table(name(EDGES))
						.leftJoin(table(name(EDGE_PROPS)))
						.on(condition(name(EDGES) + "._key = "
								+ name(EDGE_PROPS) + "._key")))
				.where(condition(name(EDGES) + "._from = ?", key1))
				.fetch();

		org.jooq.Record last = null;
		StructuredData<String, Object> edgeTuple = new StructuredValue<>();
		StructuredData<String, Object> contentTuple = new StructuredValue<>();
		String label = null;
		String node = null;

		// Iterate over all edges
		for (org.jooq.Record rec : recs) {

			label = (String) rec.get("label");
			node = (String) rec.get(2);

			// No child properties; also means this is a new edge or destination
			if (rec.get(6) == null) {
				edgeTuple.put(label, null);
				ret.add(new ImmutablePair<String, StructuredData<String, Object>>(
						node, edgeTuple));

			} else {

				// If we are starting a new edge or destination, we need a new
				// record
				if (last == null || !(rec.get(2).equals(last.get(2))
						&& rec.get(5).equals(last.get(5)))) {
					edgeTuple = new StructuredValue<>();
					ret.add(new ImmutablePair<String, StructuredData<String, Object>>(
							node, edgeTuple));
					contentTuple = new StructuredValue<>();
					edgeTuple.put(label, contentTuple);
				}

				// Append based on the data
				String code = (String) rec.get(11);
				Object value = null;
				if (code == null) {
					try {
						value = ObjectSerialization.getObject(rec.get(10));
					} catch (IllegalArgumentException | IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (code.equals("S")) {
					value = rec.get(10);
				} else if (code.equals("I")) {
					value = rec.get(12);
				} else if (code.equals("L")) {
					value = rec.get(13);
				} else if (code.equals("D")) {
					value = rec.get(14);
				} else if (code.equals("F")) {
					value = rec.get(15);
				} else if (code.equals("T")) {
					value = rec.get(16);
				}

				contentTuple.put((String) rec.get(9), value);
			}
			last = rec;
		}
		return ret;
	}

	@Override
	public Iterable<Pair<String, StructuredData<String, Object>>> getLinksTo(
			String resource, String key1)
			throws HabitatServiceException {
		DSLContext context = getContextFromSupplier(null);
		return getLinksTo(
				new PostgresTransactionSupplier(context), resource, key1);

	}

	@Override
	public Iterable<Pair<String, StructuredData<String, Object>>> getLinksTo(
			TransactionSupplier supplier,
			String resource, String key1) throws HabitatServiceException {
		List<Pair<String, StructuredData<String, Object>>> ret = new ArrayList<>();

		// LOJ edges with properties
		DSLContext context = (DSLContext) supplier.getTransactionObject();

		Result<org.jooq.Record> recs = context.select()
				.from(table(name(EDGES))
						.leftJoin(table(name(EDGE_PROPS)))
						.on(condition(name(EDGES) + "._key = "
								+ name(EDGE_PROPS) + "._key")))
				.where(condition(name(EDGES) + "._to = ?", key1))
				.fetch();

		org.jooq.Record last = null;
		StructuredData<String, Object> edgeTuple = new StructuredValue<>();
		StructuredData<String, Object> contentTuple = new StructuredValue<>();
		String label = null;
		String node = null;

		// Iterate over all edges
		for (org.jooq.Record rec : recs) {

			label = (String) rec.get("label");
			node = (String) rec.get(1);

			// No child properties; also means this is a new edge or source
			if (rec.get(6) == null) {
				edgeTuple.put(label, null);
				ret.add(new ImmutablePair<String, StructuredData<String, Object>>(
						node, edgeTuple));

			} else {

				// If we are starting a new edge or source, we need a new record
				if (last == null || !(rec.get(1).equals(last.get(1))
						&& rec.get(5).equals(last.get(5)))) {
					edgeTuple = new StructuredValue<>();
					ret.add(new ImmutablePair<String, StructuredData<String, Object>>(
							node, edgeTuple));
					contentTuple = new StructuredValue<>();
					edgeTuple.put(label, contentTuple);
				}

				// Append based on the data
				String code = (String) rec.get(11);
				Object value = null;
				if (code == null) {
					try {
						value = ObjectSerialization.getObject(rec.get(10));
					} catch (IllegalArgumentException | IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (code.equals("S")) {
					value = rec.get(10);
				} else if (code.equals("I")) {
					value = rec.get(12);
				} else if (code.equals("L")) {
					value = rec.get(13);
				} else if (code.equals("D")) {
					value = rec.get(14);
				} else if (code.equals("F")) {
					value = rec.get(15);
				} else if (code.equals("T")) {
					value = rec.get(16);
				}

				contentTuple.put((String) rec.get(9), value);
			}
			last = rec;
		}
		return ret;
	}

	@Override
	public void link(String resource, String key1, String label, String key2,
			StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		DSLContext context = getContextFromSupplier(null);
		link(
				new PostgresTransactionSupplier(context), resource, key1, label,
				key2, metadata);

	}

	@Override
	public void link(TransactionSupplier supplier, String resource, String from,
			String label, String to,
			StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		DSLContext conn = (DSLContext) supplier.getTransactionObject();
		conn.transaction(configuration -> {
			DSL.using(configuration).insertInto(table(name(EDGES)))
					.columns(field(name("_from"), String.class),
							field(name("label"), String.class),
							field(name("_to"), String.class),
							field(name("_resource"), String.class))
					.select(
							select(val(from), val(label), val(to),
									val(resource))
											.whereNotExists(
													selectFrom(
															table(name(EDGES)))
																	.where(condition(
																			"_from = ?",
																			from)
																					.and(condition(
																							"_to = ?",
																							to))
																					.and(condition(
																							"label = ?",
																							label))
																					.and(condition(
																							"_resource = ?",
																							resource)))))
					.execute();

			// TODO: query for the id
			Record1<Integer> res = DSL.using(configuration)
					.select(field(name("_key"), Integer.class))
					.from(table(name(EDGES)))
					.where(condition("_from = ?", from)
							.and(condition("_to = ?", to))
							.and(condition("label = ?", label))
							.and(condition("_resource = ?", resource)))
					.fetchOne();
			Integer key = res.value1();

			try {
				if (metadata != null) {
					writeStructure(configuration, EDGE_PROPS, resource, key, metadata);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	@Override
	public void storeReference(String resource, String key, String refToValue,
			StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		DSLContext context = getContextFromSupplier(null);
		storeReference(
				new PostgresTransactionSupplier(context), resource, key,
				refToValue, metadata);
	}

	@Override
	public void storeReference(TransactionSupplier supplier, String resource,
			String key, String refToValue,
			StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();

	}

	@Override
	public void addMetadata(String resource, String key,
			StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		DSLContext conn = this.getContextFromSupplier(null);
		addMetadata(new PostgresTransactionSupplier(conn), resource, key,
				metadata);
	}

	@Override
	public void addMetadata(String resource,
			String key,
			StructuredData<String, ? extends Object> metadata,
			String specialLabel) throws HabitatServiceException {
		DSLContext conn = this.getContextFromSupplier(null);
		addMetadata(new PostgresTransactionSupplier(conn), resource, key,
				metadata, specialLabel);
	}
	
	@Override
	public void addMetadata(TransactionSupplier supplier, String resource,
			String key,
			StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {

		DSLContext conn = (DSLContext) supplier.getTransactionObject();
		conn.transaction(configuration -> {
			DSL.using(configuration).insertInto(table(name(NODES)))
					.columns(field(name("_key"), String.class),
							field(name("_resource"), String.class))
					.select(
							select(val(key), val(resource))
									.whereNotExists(
											selectFrom(table(name(NODES)))
													.where(condition("_key = ?",
															key).and(
																	condition(
																			"_resource = ?",
																			resource)))))
					.execute();

			writeStructure(configuration, NODE_PROPS, resource, key, metadata);
		});
	}

	@Override
	public void addMetadata(TransactionSupplier supplier, String resource,
			String key,
			final StructuredData<String, ? extends Object> metadata,
			String specialLabel) throws HabitatServiceException {
		DSLContext conn = (DSLContext) supplier.getTransactionObject();
		conn.transaction(configuration -> {
			DSL.using(configuration).insertInto(table(name(NODES)))
					.columns(field(name("_key"), String.class),
							field(name("_resource"), String.class),
							field(name("label"), String.class))
					.select(
							select(val(key), val(resource), val(specialLabel))
									.whereNotExists(
											selectFrom(table(name(NODES)))
													.where(condition("_key = ?",
															key).and(
																	condition(
																			"_resource = ?",
																			resource)))))
					.execute();
			writeStructure(configuration, NODE_PROPS, resource, key, metadata);
		});
	}

	@Override
	public void connect() throws SQLException {
		try {
			pool = setupDataSource(postgresUri, postgresUser, postgresPassword);
		} catch (Exception e) {
			logger.error("Unable to connect to " + getHost() + " as "
					+ getSuperuser(), e);
			throw new SQLException("Unable to connect to " + getHost() + " as "
					+ getSuperuser());
		}
	}

	@Override
	public boolean execTransaction(String resource,
			Function<TransactionSupplier, Boolean> transactionFn)
			throws HabitatServiceException {
		final DSLContext conn = DSL.using(pool, SQLDialect.POSTGRES);
		final Integer ret = conn.transactionResult(configuration -> {
			final DSLContext context = DSL.using(configuration);
			return transactionFn
					.apply(new PostgresTransactionSupplier(context));
		}) ? 1 : 0;
		return ret != 0;
	}

	@Override
	public int getCount(String resource) throws HabitatServiceException {
		DSLContext context = DSL.using(pool, SQLDialect.POSTGRES);
		return context.fetchCount(table(name(NODES)),
				DSL.condition("_resource =", resource));
	}

	@Override
	public Object getData(String resource, String key)
			throws HabitatServiceException {
		DSLContext context = getContextFromSupplier(null);
		return getData(
				new PostgresTransactionSupplier(context), resource, key);
	}

	@Override
	public Object getData(TransactionSupplier supplier, String resource,
			String key) throws HabitatServiceException {
		StructuredData<String, Object> ret = getMetadata(supplier, resource,
				key);

		return ret.get("_value");
	}

	@Override
	public StructuredData<String, Object> getMetadata(String resource,
			String key) throws HabitatServiceException {
		DSLContext context = getContextFromSupplier(null);
		StructuredData<String, Object> ret = getMetadata(
				new PostgresTransactionSupplier(context), resource, key);

		return ret;
	}

	@Override
	public StructuredData<String, Object> getMetadata(
			TransactionSupplier supplier, String resource, String key)
			throws HabitatServiceException {
		DSLContext context = getContextFromSupplier(supplier);

		Result<Record10<String, String, String, String, Integer, Long, Double, Float, java.sql.Date, Integer>> results = context
				.select(field(name("label"), String.class),
						field(name("type"), String.class),
						field(name("value"), String.class),
						field(name("code"), String.class),
						field(name("ivalue"), Integer.class),
						field(name("lvalue"), Long.class),
						field(name("dvalue"), Double.class),
						field(name("fvalue"), Float.class),
						field(name("tvalue"), java.sql.Date.class),
						field(name("index"), Integer.class))
				.from(table(name(NODE_PROPS)))
				.where(condition("_key = ?", key)
						.and(condition("_resource = ?", resource)))
				.fetch();

		StructuredData<String, Object> res = new StructuredValue<>();

		for (Record10<String, String, String, String, Integer, Long, Double, Float, java.sql.Date, Integer> rec : results) {
			try {
				String code = rec.value4();
				Object value = null;
				if (code == null) {
					value = ObjectSerialization.getObject(rec.value3());
				} else if (code.equals("S")) {
					value = rec.value3();
				} else if (code.equals("I")) {
					value = rec.value5();
				} else if (code.equals("L")) {
					value = rec.value6();
				} else if (code.equals("D")) {
					value = rec.value7();
				} else if (code.equals("F")) {
					value = rec.value8();
				} else if (code.equals("T")) {
					value = rec.value9();
				} else if (code.equals("P")) {
					value = om.readValue(rec.value3(), ProvLocation.class);
				} else if (code.equals("p")) {
					value = om.readValue(rec.value3(), ProvStringToken.class);
				} else if (code.equals("t")) {
					BasicSchema sch = readSchema(context, resource, rec.value3());
					value = new BasicTuple(sch, readTuple(context, NODE_PROPS, resource, key, sch));
				} else
					throw new UnsupportedOperationException("Unknown code");
				res.put(rec.value1(), value);
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return res;
	}

	@Override
	public Iterable<Pair<String, StructuredData<String, Object>>> getMetadataForResource(
			String resource)
			throws HabitatServiceException {
		DSLContext context = getContextFromSupplier(null);
		Iterable<Pair<String, StructuredData<String, Object>>> ret = getMetadataForResource(
				new PostgresTransactionSupplier(context), resource);

		return ret;
	}

	@Override
	public Iterable<Pair<String, StructuredData<String, Object>>> getMetadataForResource(
			TransactionSupplier supplier,
			String resource) throws HabitatServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException(
				"We have not yet implemented an on-demand iterator for structured nodes and getMetadataForResource");
	}

	@Override
	public StoreClass getStoreClass() {
		return StorageApi.StoreClass.GRAPH;
	}

	@Override
	public boolean isSuitableForMetadata() {
		return true;
	}

	@Override
	public boolean isSuitableToStore(Object obj) {
		Object val = null;
		try {
			val = ObjectSerialization.putObject(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return false;
		}
		if (val != null
				&& (val instanceof String && ((String) val)
						.length() > edu.upenn.cis.db.habitat.Config
								.getMaxLength()))
			return false;

		return true;
	}

	@Override
	public void removeKey(String resource, String key)
			throws HabitatServiceException {
		DSLContext conn = getContextFromSupplier(null);
		removeKey(new PostgresTransactionSupplier(conn), resource, key);
	}

	@Override
	public void removeKey(TransactionSupplier supplier, String resource,
			String key) throws HabitatServiceException {
		DSLContext conn = (DSLContext) supplier.getTransactionObject();

		// Should delete-cascade
		conn.transaction(configuration -> {
			DSL.using(configuration).deleteFrom(table(name(NODES)))
					.where(condition("_key = ?", key)
							.and(condition("_resource = ?", resource)))
					.execute();
		});
	}

	@Override
	public void removeMetadata(String resource, String key,
			String metadataField) throws HabitatServiceException {
		DSLContext conn = getContextFromSupplier(null);
		removeMetadata(new PostgresTransactionSupplier(conn), resource, key,
				metadataField);
	}

	@Override
	public void removeMetadata(TransactionSupplier supplier, String resource,
			String key, String metadataField)
			throws HabitatServiceException {
		DSLContext conn = (DSLContext) supplier.getTransactionObject();

		conn.transaction(configuration -> {
			DSL.using(configuration).deleteFrom(table(name(NODE_PROPS)))
					.where(condition("_key = ?", key)
							.and(condition("_resource = ?", resource)
									.and("label = ?", metadataField)))
					.execute();
		});
	}

	@Override
	public void replaceMetadata(String resource, String key,
			StructuredData<String, ? extends Object> metadata)
					throws HabitatServiceException {
		DSLContext context = getContextFromSupplier(null);
		replaceMetadata(
				new PostgresTransactionSupplier(context), resource, key,
				metadata);
	}

	@Override
	public void replaceMetadata(TransactionSupplier supplier, String resource,
			String key,
			StructuredData<String, ? extends Object> metadata)
					throws HabitatServiceException {
		DSLContext conn = (DSLContext) supplier.getTransactionObject();
		conn.transaction(configuration -> {
			try {
				DSL.using(configuration).deleteFrom(table(name(NODE_PROPS)))
				.where(condition("_key = ?", key)
						.and(condition("_resource = ?", resource)))
				.execute();
				
				writeStructure(configuration, NODE_PROPS, resource, key, metadata);

			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	@Override
	public void store(String resource, String key, Object value,
			StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		DSLContext context = getContextFromSupplier(null);
		store(
				new PostgresTransactionSupplier(context), resource, key, value,
				metadata);
	}

	@Override
	public void store(TransactionSupplier supplier, String resource, String key,
			Object value,
			StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		Object val = null;
		try {
			val = ObjectSerialization.putObject(value);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			throw new HabitatServiceException(e.getMessage());
		}
		if (val != null
				&& (val instanceof String && ((String) val)
						.length() > edu.upenn.cis.db.habitat.Config
								.getMaxLength()))
			throw new RuntimeException(
					"Value exceeds specified limit: do not store CLOB/BLOB values");

		addMetadata(supplier, resource, key, metadata);
		StructuredData<String, Object> md2 = new StructuredValue<>();
		md2.put("_value", value);
		addMetadata(supplier, resource, key, md2);
	}

	@Override
	public void storeFile(String resource, String key, InputStream file,
			int streamLength,
			StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		DSLContext context = getContextFromSupplier(null);
		storeFile(
				new PostgresTransactionSupplier(context), resource, key, file,
				streamLength, metadata);
	}

	@Override
	public void storeFile(TransactionSupplier supplier, String resource,
			String key, InputStream file, int streamLength,
			StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void storeUrlContents(String resource, String key, URL url,
			StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		DSLContext context = getContextFromSupplier(null);
		storeUrlContents(
				new PostgresTransactionSupplier(context), resource, key, url,
				metadata);
	}

	@Override
	public void storeUrlContents(TransactionSupplier supplier, String resource,
			String key, URL url,
			StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		throw new UnsupportedOperationException();

	}

	@Override
	public boolean isDataLegal(
			StructuredData<String, ? extends Object> metadata) {
		return ObjectSerialization.isTupleLegal(metadata);
	}

	@Override
	public String getStoreUuid() {
		return "postgres-1";
	}

	@Override
	public String getSuperuser() {
		return postgresUser;
	}

	@Override
	public String getHost() {
		return postgresUri.getHost();
	}

	@Override
	public int getPort() {
		return postgresUri.getPort();
	}

	@Override
	public String getDatabase() {
		return postgresUri.getPath();
	}

	@Override
	public void createGraph(String resourceName)
			throws HabitatServiceException {
		// For now: we don't do anything because the resourceName is a field

	}

	@Override
	public void createOrResetGraph(String resource)
			throws HabitatServiceException {
		// Should delete-cascade
		DSLContext context = getContextFromSupplier(null);
		context.transaction(configuration -> {
			DSL.using(configuration).deleteFrom(table(name(NODES)))
					.where(condition("_resource = ?", resource))
					.execute();
			DSL.using(configuration).deleteFrom(table(name(EDGES)))
					.where(condition("_resource = ?", resource))
					.execute();
		});
	}

	@Override
	public Iterable<SubgraphInstance> getSubgraphs(String resource,
			SubgraphTemplate template, int limit, Long since)
			throws HabitatServiceException {
		DSLContext context = getContextFromSupplier(null);
		return getSubgraphs(
				new PostgresTransactionSupplier(context), resource, template,
				limit, since);
	}

	@Override
	public Iterable<SubgraphInstance> getSubgraphs(TransactionSupplier supplier,
			String resource,
			SubgraphTemplate template, int limit, Long since)
			throws HabitatServiceException {
		throw new UnsupportedOperationException();
	}

	public void close() {
	}

	private static synchronized void init(URI postgresUri, String postgresUser,
			String postgresPassword) throws SQLException {
		if (pool == null) {
			pool = setupDataSource(postgresUri, postgresUser, postgresPassword);
			createTables();
		}
	}

	public static DataSource setupDataSource(URI postgresUri,
			String postgresUser, String postgresPassword) {
		ConnectionFactory cf = new DriverManagerConnectionFactory(
				postgresUri.toString(),
				postgresUser, postgresPassword);

		PoolableConnectionFactory pcf = new PoolableConnectionFactory(cf, null);

		GenericObjectPoolConfig<PoolableConnection> poolConfig = new GenericObjectPoolConfig<>();
		poolConfig.setMaxTotal(35);
		ObjectPool<PoolableConnection> connectionPool = new GenericObjectPool<>(
				pcf, poolConfig);

		pcf.setPool(connectionPool);

		return new PoolingDataSource<PoolableConnection>(connectionPool);
	}

	private synchronized DSLContext getContextFromSupplier(
			TransactionSupplier supplier) {
		if (supplier == null)
			return DSL.using(pool, SQLDialect.POSTGRES);
		if (supplier instanceof PostgresTransactionSupplier)
			return ((PostgresTransactionSupplier) supplier)
					.getTransactionObject();
		else
			throw new UnsupportedOperationException(
					"Unable to use non-PostgreSQL session supplier");
	}

	private List<Field<? extends Object>> readTuple(DSLContext context, String table, String resource, String key, BasicSchema sch) {
		List<Field<? extends Object>> fields = new ArrayList<>();
		
		Result<Record> recs = context
				.select()
				.from(name(table))
				.where(condition(name(table) + "._key = ?", key)
						.and(condition(name(table) + 
						"._resource = ?",
						resource))
					.and(condition(name(table) + 
						".index >= ?",
						0))
					.and(condition(name(table) + 
							".index <= ?",
							sch.getArity())
					))
				.fetch();
		
		int i = 0;
		for (Record rec: recs) {
			String code = rec.get("code").toString();
			if (code.equals("S")) {
				fields.add(new StringField(rec.get("value").toString()));
			} else if (code.equals("I")) {
				fields.add(new IntField(Integer.valueOf(rec.get("ivalue").toString())));
			} else if (code.equals("L")) {
				fields.add(new LongField(Long.valueOf(rec.get("ivalue").toString())));
			} else if (code.equals("D")) {
				fields.add(new DoubleField(Double.valueOf(rec.get("dvalue").toString())));
			} else if (code.equals("F")) {
				fields.add(new DoubleField(Double.valueOf(rec.get("fvalue").toString())));
			} else if (code.equals("T")) {
				fields.add(new DateField(java.sql.Date.valueOf(rec.get("tvalue").toString())));
			} else
				throw new UnsupportedOperationException("Unable to decode " + sch.getTypeAt(i).getName());
		}
		
		return fields;
	}

	private List<Field<? extends Object>> readTuple(DSLContext context, String table, String resource, Integer key, BasicSchema sch) {
		List<Field<? extends Object>> fields = new ArrayList<>();
		
		Result<Record> recs = context
				.select()
				.from(name(table))
				.where(condition(name(table) + "._key = ?", key)
						.and(condition(name(table) + 
						"._resource = ?",
						resource))
					.and(condition(name(table) + 
						".index >= ?",
						0))
					.and(condition(name(table) + 
							".index <= ?",
							sch.getArity())
					))
				.fetch();
		
		int i = 0;
		for (Record rec: recs) {
			String code = rec.get("code").toString();
			if (code.equals("S")) {
				fields.add(new StringField(rec.get("value").toString()));
			} else if (code.equals("I")) {
				fields.add(new IntField(Integer.valueOf(rec.get("ivalue").toString())));
			} else if (code.equals("L")) {
				fields.add(new LongField(Long.valueOf(rec.get("ivalue").toString())));
			} else if (code.equals("D")) {
				fields.add(new DoubleField(Double.valueOf(rec.get("dvalue").toString())));
			} else if (code.equals("F")) {
				fields.add(new DoubleField(Double.valueOf(rec.get("fvalue").toString())));
			} else if (code.equals("T")) {
				fields.add(new DateField(java.sql.Date.valueOf(rec.get("tvalue").toString())));
			} else
				throw new UnsupportedOperationException("Unable to decode " + sch.getTypeAt(i).getName());
		}
		
		return fields;
	}

	private BasicSchema readSchema(DSLContext context, String resource, String value) throws JsonParseException, JsonMappingException, IllegalArgumentException, IOException {
		Record rec = context
			.select()
			.from(name(SCHEMAS))
			.where((condition(name(SCHEMAS) + 
					"._resource = ?",
					resource))
				.and(condition(name(SCHEMAS) + 
					".name = ?",
					value)
				))
			.fetchOne();

		return om.readValue(rec.get("value").toString(), BasicSchema.class);
	}

	
	/**
	 * Write a metadata structure as properties.  Assumes a key that's a string
	 * 
	 * @param configuration
	 * @param table
	 * @param resource
	 * @param key
	 * @param metadata
	 * @throws HabitatServiceException
	 */
	private void writeStructure(
			final Configuration configuration,
			final String table,
			final String resource, 
			final String key, 
			final StructuredData<String, ? extends Object> metadata) throws HabitatServiceException {
		try {
			Integer inx = null;
			
			if (metadata instanceof BasicTuple)
				inx = 0;
			
			for (String prop : metadata.keySet()) {
				String value = null;

				Object md = metadata.get(prop);
				if (md instanceof StringEncoding)
					md = ObjectSerialization.getObject(md);
				else if (md instanceof Field)
					md = ((Field)md).getValue();
				
				String code = null;
				String type = null;
				Integer ivalue = null;
				Long lvalue = null;
				Double dvalue = null;
				Float fvalue = null;
				java.sql.Date tvalue = null;
				if (md instanceof String) {
					value = (String) md;
					code = "S";
				} else if (md instanceof Integer) {
					ivalue = (Integer) md;
					code = "I";
				} else if (md instanceof Long) {
					lvalue = (Long) md;
					code = "L";
				} else if (md instanceof Double) {
					dvalue = (Double) md;
					code = "D";
				} else if (md instanceof Float) {
					fvalue = (Float) md;
					code = "F";
				} else if (md instanceof java.sql.Date) {
					tvalue = (java.sql.Date) md;
					code = "T";
				} else if (md instanceof ProvLocation) {
					value = om.writeValueAsString(md); 
					code = "P";
				} else if (md instanceof ProvStringToken) {
					value = om.writeValueAsString(md); 
					code = "p";
				} else if (md instanceof BasicTuple) {
					writeStructure(configuration, table, resource, key, (BasicTuple)md);
//					value = om.writeValueAsString(((BasicTuple)md).getSchema());
					
					BasicSchema sch = ((BasicTuple)md).getSchema();
					SchemaKey sk = new SchemaKey(resource, sch);
					if (schemaKeyToUuid.containsKey(sk)) {
						value = schemaKeyToUuid.get(sk);
					} else {
						value = UUID.randomUUID().toString();
						
						writeSchema(configuration, resource, value, sch);
						schemaKeyToUuid.put(sk, value);
					}
					code = "s";
				} else {
					value = ObjectSerialization
							.putObject(metadata.get(prop)).toString();

					type = md.getClass().getName();
				}

				DSL.using(configuration).insertInto(table(name(table)))
						.columns(field(name("_key"), String.class),
								field(name("_resource"), String.class),
								field(name("label"), String.class),
								field(name("type"), String.class),
								field(name("value"), String.class),
								field(name("code"), String.class),
								field(name("ivalue"), Integer.class),
								field(name("lvalue"), Long.class),
								field(name("dvalue"), Double.class),
								field(name("fvalue"), Float.class),
								field(name("tvalue"), java.sql.Date.class),
								field(name("index"), Integer.class))
						.select(
								select(val(key), val(resource), val(prop),
										val(type), val(value),
										val(code), val(ivalue), val(lvalue),
										val(dvalue), val(fvalue),
										val(tvalue), val(inx))
												.whereNotExists(
														selectFrom(table(
																name(table)))
																		.where(condition(
																				"_key = ?",
																				key)
																						.and(condition(
																								"_resource = ?",
																								resource)
																										.and(condition(
																												"label = ?",
																												prop))))))
						.onConflictDoNothing()
						.execute();

				if (inx != null)
					inx = inx + 1;
			}
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			throw new HabitatServiceException(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Write a metadata structure as properties.  Assumes a key that's an Integer.
	 * 
	 * @param configuration
	 * @param table
	 * @param resource
	 * @param key
	 * @param metadata
	 * @throws HabitatServiceException
	 */
	private void writeStructure(
			final Configuration configuration,
			final String table,
			final String resource, 
			final Integer key, 
			final StructuredData<String, ? extends Object> metadata) throws HabitatServiceException {
		try {
			Integer inx = null;
			
			if (metadata instanceof BasicTuple)
				inx = 0;
			
			for (String prop : metadata.keySet()) {
				String value = null;

				Object md = metadata.get(prop);
				if (md instanceof StringEncoding)
					md = ObjectSerialization.getObject(md);
				else if (md instanceof Field)
					md = ((Field)md).getValue();
				
				String code = null;
				String type = null;
				Integer ivalue = null;
				Long lvalue = null;
				Double dvalue = null;
				Float fvalue = null;
				java.sql.Date tvalue = null;
				if (md instanceof String) {
					value = (String) md;
					code = "S";
				} else if (md instanceof Integer) {
					ivalue = (Integer) md;
					code = "I";
				} else if (md instanceof Long) {
					lvalue = (Long) md;
					code = "L";
				} else if (md instanceof Double) {
					dvalue = (Double) md;
					code = "D";
				} else if (md instanceof Float) {
					fvalue = (Float) md;
					code = "F";
				} else if (md instanceof java.sql.Date) {
					tvalue = (java.sql.Date) md;
					code = "T";
				} else if (md instanceof ProvLocation) {
					value = om.writeValueAsString(md); 
					code = "P";
				} else if (md instanceof ProvStringToken) {
					value = om.writeValueAsString(md); 
					code = "p";
				} else if (md instanceof BasicTuple) {
					writeStructure(configuration, table, resource, key, (BasicTuple)md);
					value = om.writeValueAsString(((BasicTuple)md).getSchema()); 
					code = "s";
				} else {
					value = ObjectSerialization
							.putObject(md).toString();

					type = md.getClass().getName();
				}

				DSL.using(configuration).insertInto(table(name(table)))
						.columns(field(name("_key"), Integer.class),
								field(name("_resource"), String.class),
								field(name("label"), String.class),
								field(name("type"), String.class),
								field(name("value"), String.class),
								field(name("code"), String.class),
								field(name("ivalue"), Integer.class),
								field(name("lvalue"), Long.class),
								field(name("dvalue"), Double.class),
								field(name("fvalue"), Float.class),
								field(name("tvalue"), java.sql.Date.class),
								field(name("index"), Integer.class))
						.select(
								select(val(key), val(resource), val(prop),
										val(type), val(value),
										val(code), val(ivalue), val(lvalue),
										val(dvalue), val(fvalue),
										val(tvalue), val(inx))
												.whereNotExists(
														selectFrom(table(
																name(table)))
																		.where(condition(
																				"_key = ?",
																				key)
																						.and(condition(
																								"_resource = ?",
																								resource)
																										.and(condition(
																												"label = ?",
																												prop))))))
						.onConflictDoNothing()
						.execute();

				if (inx != null)
					inx = inx + 1;
			}
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			throw new HabitatServiceException(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Write a schema (serialized) to the schema table
	 * 
	 * @param configuration
	 * @param resource
	 * @param value
	 * @param sch
	 * @throws JsonProcessingException
	 */
	private void writeSchema(
			final Configuration configuration,
			final String resource,
			final String value, 
			final BasicSchema sch) throws JsonProcessingException {
		
		DSL.using(configuration)
			.insertInto(table(name(SCHEMAS)))
			.columns(field(name("_key"), String.class),
					field(name("_resource"), String.class),
					field(name("name"), String.class),
					field(name("value"), String.class))
			.values(val(sch.getName()), 
					val(resource), 
					val(value), 
					val(om.writeValueAsString(sch)))
			.onConflictDoNothing()
			.execute();
			
	}

	
}