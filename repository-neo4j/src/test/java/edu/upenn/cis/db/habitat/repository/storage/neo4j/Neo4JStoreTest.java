/*
 * Copyright 2019 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.repository.storage.neo4j;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.neo4j.graphdb.Transaction;
// import org.neo4j.driver.v1.Transaction;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterator;

import com.google.common.collect.Lists;
import com.google.common.io.ByteSource;
import com.google.common.io.Resources;

import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.StructuredValue;
import edu.upenn.cis.db.habitat.repository.storage.neo4j.test.Neo4jBoltRule;

/**
 * @author John Frommeyer
 *
 */
public class Neo4JStoreTest {

	@Rule
	public Neo4jBoltRule neo4j = new Neo4jBoltRule();
	private Neo4JStore store;

	@Before
	public void setup() throws SQLException {
		store = new Neo4JStore(neo4j.getBoltUri(), null, null);
	}

	@Test
	public void storeFile() throws Exception {
		final ByteSource templateSource = Resources.asByteSource(
				getClass().getResource("/subgraph-template.json"));

		final String expectedContent = templateSource
				.asCharSource(StandardCharsets.UTF_8).read();

		final InputStream storedStream = templateSource.openBufferedStream();
		store.storeFile("graph-with-file", "subgraph-template.json",
				storedStream,
				expectedContent.length(), new StructuredValue<>());
		boolean exception = false;
		try {
			storedStream.read();
		} catch (IOException e) {
			exception = true;
		}
		// Server should close stream, so we should see an exception.
		assertTrue(exception);

		neo4j.runAssertionInTx((db) -> {
			Transaction tx = db.beginTx();
			try {
				final ResourceIterator<Node> allNodes = tx.getAllNodes().iterator();
				assertTrue(allNodes.hasNext());
				final Node actualNode = allNodes.next();
				final List<Label> labels = Lists
						.newArrayList(actualNode.getLabels());
				assertTrue(labels.size() == 1);
				final Label label = labels.get(0);
				assertEquals("file", label.name());
				final String actualContent = (String) actualNode
						.getProperty("_content");
				assertEquals(expectedContent, actualContent);
				assertFalse(allNodes.hasNext());
			} finally {
				tx.close();
			}
		});

	}

	@Test
	public void getData() throws Exception {
		final ByteSource templateSource = Resources.asByteSource(
				getClass().getResource("/subgraph-template.json"));

		final String expectedContent = templateSource
				.asCharSource(StandardCharsets.UTF_8).read();

		final InputStream storedStream = templateSource.openBufferedStream();
		store.storeFile("graph-with-file", "subgraph-template.json",
				storedStream,
				expectedContent.length(), new StructuredValue<>());

		final Object actualData = store.getData("graph-with-file",
				"subgraph-template.json");
		assertTrue(actualData instanceof String);
		assertEquals(expectedContent, actualData);

	}
	
	@Test
	public void getDataMissingData() throws Exception {
		final Object noData = store.getData("graph-without-file",
				"subgraph-template.json");
		assertNull(noData);

	}
}
