/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.repository.storage.neo4j.test;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;
import org.neo4j.configuration.connectors.BoltConnector;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.dbms.api.DatabaseManagementService;
import org.neo4j.dbms.api.DatabaseManagementServiceBuilder;

import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.util.ObjectSerialization;

/**
 * 
 * Intended to be used with a builder obtained by
 * {@code TestGraphDatabaseFactory()
				.newImpermanentDatabaseBuilder()} which requires the following
 * test dependencies:
 * 
 * <pre>
 * {@code
 *        <dependency>
 *    		    <groupId>org.neo4j</groupId>
 *   		    <artifactId>neo4j-bolt</artifactId>
 *       		<version>${neo4j.version}</version>
 *       		<scope>test</scope>
 *     	</dependency>
 *   		<dependency>
 *      			<groupId>org.neo4j</groupId>
 *      			<artifactId>neo4j-kernel</artifactId>
 *      			<version>${neo4j.version}</version>
 *      			<type>test-jar</type>
 *      			<scope>test</scope>
 *     	</dependency>
 *     	<dependency>
 *      			<groupId>org.neo4j</groupId>
 *      			<artifactId>neo4j-io</artifactId>
 *      			<version>${neo4j.version}</version>
 *      			<type>test-jar</type>
 *      			<scope>test</scope>
 *     	</dependency>
 * }
 * </pre>
 * 
 * @author John Frommeyer
 *
 */
public class Neo4jBoltRule implements TestRule {

	DatabaseManagementService manager;
	final DatabaseManagementServiceBuilder databaseBuilder;

	@FunctionalInterface
	public static interface Assertion {
		void runAssertion(GraphDatabaseService graphDatabase) throws Exception;
	}

	private URI boltUri;
	private GraphDatabaseService graphDatabase;

	public Neo4jBoltRule() {
		this.databaseBuilder = 
					new DatabaseManagementServiceBuilder(DB_PATH)
						.setConfig(BoltConnector.enabled, true);
		//new TestGraphDatabaseFactory()
		// 		.newImpermanentDatabaseBuilder();
	}

	public Neo4jBoltRule(DatabaseManagementServiceBuilder builder) {
		this.databaseBuilder = checkNotNull(builder);
	}

	public URI getBoltUri() {
		return boltUri;
	}

	public GraphDatabaseService getGraphDatabase() {
		return graphDatabase;
	}

	public void cleanDatabase() {
		graphDatabase.executeTransactionally("MATCH (n) DETACH DELETE n");
	}

	public void runAssertionInTx(Assertion assertion) throws Exception {
		try (final Transaction tx = graphDatabase.beginTx()) {
			assertion.runAssertion(graphDatabase);
			tx.commit();
		}
	}

	public List<Node> getExpectedNodesOrFail(int expectedCount) {
		Transaction tx = graphDatabase.beginTx();
		try {
			return getExpectedItemsOrFail(expectedCount, tx
					.getAllNodes()
					.iterator());
		} finally {
			tx.close();
		}
	}

	public List<Relationship> getExpectedRelationshipsOrFail(
			int expectedCount) {
		Transaction tx = graphDatabase.beginTx();
		try {
			return getExpectedItemsOrFail(expectedCount, tx
					.getAllRelationships().iterator());
		} finally {
			tx.close();
		}
	}

	public static String getProvTokenValue(Node node) {
		final String strValue = (String) node.getProperty("_key", null);
		assertNotNull(strValue);
		return strValue;
	}

	public static <T> T getRelationshipValue(
			Relationship link,
			String key,
			Class<T> expectedClass) {
		final Object strValue = link.getProperty(key, null);
		assertNotNull("missing " + key, strValue);
		try {
			final Object objValue = ObjectSerialization.getObject(strValue);
			assertTrue(String.format("%s:%s is %s. Expected %s", key, objValue,
					objValue.getClass(), expectedClass),
					expectedClass.isAssignableFrom(objValue.getClass()));
			return expectedClass.cast(objValue);
		} catch (IOException e) {
			throw new AssertionError("deserialization failed", e);
		}
	}

	@SuppressWarnings("unchecked")
	public static <T> Set<T> getRelationshipSetValue(
			Relationship link,
			String key) {
		return getRelationshipValue(link, key, Set.class);
	}

	@SuppressWarnings("unchecked")
	public static TupleWithSchema<String> getTuple(Node node)
			throws IOException {
		final Object tupleStrProp = node.getProperty("tuple", null);
		assertNotNull("missing tuple", tupleStrProp);
		final Object tupleObj = ObjectSerialization.getObject(tupleStrProp);
		assertTrue("tuple not a TupleWithSchema",
				tupleObj instanceof TupleWithSchema);
		return (TupleWithSchema<String>) tupleObj;
	}

	public static ProvSpecifier getLocation(Node node) throws IOException {
		final Object locationStrProp = node.getProperty("location", null);
		assertNotNull("missing location", locationStrProp);
		final Object locationObj = ObjectSerialization
				.getObject(locationStrProp);
		assertTrue("location not a ProvSpecifier",
				locationObj instanceof ProvSpecifier);
		return (ProvSpecifier) locationObj;
	}

	private <T> List<T> getExpectedItemsOrFail(int expectedCount,
			ResourceIterator<T> iter) {
		checkArgument(expectedCount >= 0);
		final List<T> items = new ArrayList<>();
		int count = 0;
		while (count < expectedCount && iter.hasNext()) {
			count++;
			items.add(iter.next());
		}
		assertTrue(String.format("Expected %d items, got %d",
				expectedCount, count), count == expectedCount);
		assertFalse(String.format("Only expected %d items. Got more.",
				expectedCount), iter.hasNext());
		return items;
	}

	private static final Path DB_PATH = Paths.get("target/neo4j-test-db");

	@Override
	public Statement apply(Statement base, Description description) {
		return new Statement() {

			@Override
			public void evaluate() throws Throwable {
				final InetSocketAddress address = findFreePort();

				//final BoltConnector bolt = new BoltConnector("0");
				// databaseBuilder.setConfig(bolt.type, ConnectorType.BOLT.name());

				// databaseBuilder.setConfig(bolt.enabled, true)

				final String hostAndPort = String.format("%s:%d",
						address.getHostName(), address.getPort());

				manager = databaseBuilder
						.setConfig(BoltConnector.listen_address, new SocketAddress(hostAndPort))
						.build();
						
				graphDatabase = manager.database("testdb");

				boltUri = URI.create(String.format("bolt://%s", hostAndPort));

				try {
					base.evaluate();
				} finally {
					manager.shutdown();
				}

			}

		};
	}

	private static InetSocketAddress findFreePort()
			throws IOException {
		try (final ServerSocket socket = new ServerSocket(0)) {
			socket.setReuseAddress(true);
			final int port = socket.getLocalPort();
			return new InetSocketAddress("localhost", port);
		}
	}

}
