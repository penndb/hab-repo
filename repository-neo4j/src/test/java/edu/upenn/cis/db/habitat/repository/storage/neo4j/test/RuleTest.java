/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.repository.storage.neo4j.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;

/**
 * Test the TestRule's convenience methods.
 * 
 * @author John Frommeyer
 *
 */
public class RuleTest {

	@Rule
	public final Neo4jBoltRule neo4j = new Neo4jBoltRule();

	@Test
	public void testEmptyGraph() throws Exception {
		neo4j.runAssertionInTx((db) -> {
			final List<Node> emptyNodeList = neo4j.getExpectedNodesOrFail(0);
			assertTrue(emptyNodeList.isEmpty());

			final List<Relationship> emptyLinkList = neo4j
					.getExpectedRelationshipsOrFail(0);
			assertTrue(emptyLinkList.isEmpty());

			boolean nodeException = false;
			try {
				neo4j.getExpectedNodesOrFail(1);
			} catch (AssertionError e) {
				nodeException = true;
			}
			assertTrue(nodeException);

			boolean linkException = false;
			try {
				neo4j.getExpectedRelationshipsOrFail(1);
			} catch (AssertionError e) {
				linkException = true;
			}
			assertTrue(linkException);
		});
	}

	@Test
	public void testNonEmptyGraph() throws Exception {
		// Setup
		neo4j.runAssertionInTx((db) -> {
			final Transaction tx = db.beginTx();
			try {
				final Node source = tx.createNode(Label.label("SOURCE"));
				final Node target = tx.createNode(Label.label("TARGET"));
				source.createRelationshipTo(target,
						RelationshipType.withName("LINK"));
			} finally {
				tx.commit();
			}
		});

		// Test
		neo4j.runAssertionInTx((db) -> {
			final List<Node> nodeList = neo4j.getExpectedNodesOrFail(2);
			assertEquals(2, nodeList.size());

			final List<Relationship> linkList = neo4j
					.getExpectedRelationshipsOrFail(1);
			assertEquals(1, linkList.size());

			boolean nodeExceptionEmpty = false;
			try {
				neo4j.getExpectedNodesOrFail(0);
			} catch (AssertionError e) {
				nodeExceptionEmpty = true;
			}
			assertTrue(nodeExceptionEmpty);

			boolean nodeExceptionTooMany = false;
			try {
				neo4j.getExpectedNodesOrFail(3);
			} catch (AssertionError e) {
				nodeExceptionTooMany = true;
			}
			assertTrue(nodeExceptionTooMany);

			boolean nodeExceptionTooFew = false;
			try {
				neo4j.getExpectedNodesOrFail(1);
			} catch (AssertionError e) {
				nodeExceptionTooFew = true;
			}
			assertTrue(nodeExceptionTooFew);

			boolean linkException = false;
			try {
				neo4j.getExpectedRelationshipsOrFail(0);
			} catch (AssertionError e) {
				linkException = true;
			}
			assertTrue(linkException);
		});
	}

}
