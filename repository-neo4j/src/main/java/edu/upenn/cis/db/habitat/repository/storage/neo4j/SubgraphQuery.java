/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.repository.storage.neo4j;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Iterables.transform;
import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static java.util.Collections.sort;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Nullable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Statement;
import org.neo4j.driver.v1.Value;
import org.neo4j.driver.v1.types.Node;
import org.neo4j.driver.v1.types.Path;
import org.neo4j.driver.v1.types.Relationship;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Joiner;
import com.google.common.collect.ComparisonChain;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;
import com.google.common.collect.Sets;

import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.StructuredValue;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance.LinkInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance.NodeInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance.RankInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate.LinkInfo;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate.NodeInfo;
import edu.upenn.cis.db.habitat.core.type.model.TupleWithSchemaModel;
import edu.upenn.cis.db.habitat.util.ObjectSerialization;

/**
 * @author John Frommeyer
 *
 */
public class SubgraphQuery {

	/**
	 * Incompatible with String.equals()
	 * 
	 * @author John Frommeyer
	 *
	 */
	private static final class TemplateIdComparator implements
			Comparator<String> {

		private final Map<String, NodeInfoWithLocation> nodeLookup;

		public TemplateIdComparator(
				Map<String, NodeInfoWithLocation> nodeLookup) {
			this.nodeLookup = nodeLookup;
		}

		@Override
		public int compare(String left, String right) {
			final NodeInfoWithLocation leftInfo = checkNotNull(this.nodeLookup
					.get(left));
			final NodeInfoWithLocation rightInfo = checkNotNull(this.nodeLookup
					.get(right));
			return ComparisonChain
					.start()
					.compare(leftInfo.idLocation.rankIndex,
							rightInfo.idLocation.rankIndex)
					.compare(leftInfo.idLocation.nodeIndex,
							rightInfo.idLocation.nodeIndex)
					.result();
		}

	}

	@VisibleForTesting
	public static final class NodeIdLocation {
		public final int rankIndex;
		public final int nodeIndex;

		public NodeIdLocation(int rankIndex, int nodeIndex) {
			this.rankIndex = rankIndex;
			this.nodeIndex = nodeIndex;
		}

	}

	private static final class NodeInfoWithLocation {
		public final NodeInfo info;
		public final NodeIdLocation idLocation;

		public NodeInfoWithLocation(
				NodeInfo info,
				int rankIndex,
				int nodeIndex) {
			this.info = info;
			this.idLocation = new NodeIdLocation(rankIndex, nodeIndex);
		}

	}

	private static final class LinkEndpointKey {

		public static enum NodeRole {
			SOURCE, TARGET
		}

		public final String endpointId;
		public final NodeRole endpointRole;
		public final String linkType;

		public LinkEndpointKey(String nodeId,
				NodeRole nodeRole,
				String linkType) {
			this.endpointId = nodeId;
			this.endpointRole = nodeRole;
			this.linkType = linkType;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((endpointId == null) ? 0 : endpointId.hashCode());
			result = prime * result
					+ ((endpointRole == null) ? 0 : endpointRole.hashCode());
			result = prime * result
					+ ((linkType == null) ? 0 : linkType.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof LinkEndpointKey)) {
				return false;
			}
			LinkEndpointKey other = (LinkEndpointKey) obj;
			if (endpointId == null) {
				if (other.endpointId != null) {
					return false;
				}
			} else if (!endpointId.equals(other.endpointId)) {
				return false;
			}
			if (endpointRole != other.endpointRole) {
				return false;
			}
			if (linkType == null) {
				if (other.linkType != null) {
					return false;
				}
			} else if (!linkType.equals(other.linkType)) {
				return false;
			}
			return true;
		}

	}

	private final static Joiner commaJoiner = Joiner.on(", ");
	private final static Joiner commaNewlineJoiner = Joiner.on(",\n");
	private final static Joiner andJoiner = Joiner.on(" and\n");

	private final Logger logger = LogManager.getLogger(getClass());

	private final SubgraphTemplate template;
	private final Map<String, NodeInfoWithLocation> templateIdToNodeInfo;
	private final Map<String, NodeInfo> templateIdToOptionalNode;
	private final TemplateIdComparator templateIdComparator;

	public SubgraphQuery(SubgraphTemplate template) {
		this.template = checkNotNull(template);

		final Builder<String, NodeInfoWithLocation> builder = ImmutableMap
				.builder();
		final Builder<String, NodeInfo> optionalNodeBuilder = ImmutableMap
				.builder();
		for (int r = 0; r < this.template.getRanks().size(); r++) {
			final List<NodeInfo> rank = this.template.getRanks().get(r);
			for (int n = 0; n < rank.size(); n++) {
				final NodeInfo node = rank.get(n);
				builder.put(node.getId(), new NodeInfoWithLocation(
						node,
						r,
						n));
				if (node.isOptional()) {
					optionalNodeBuilder.put(node.getId(), node);
				}
			}
		}
		this.templateIdToNodeInfo = builder.build();
		this.templateIdToOptionalNode = optionalNodeBuilder.build();
		this.templateIdComparator = new TemplateIdComparator(
				templateIdToNodeInfo);
	}

	public Statement getStatement(String graph, int limit, long since) {
		final String m = "getStatement(...)";
		checkArgument(limit > 0, "limit must be greater than 0: " + limit);
		checkArgument(since >= 0,
				"since must be greater than or equal to 0: " + since);
		/*
		 * Why is this so complicated?
		 * 
		 * Because of optional nodes, we would like to do a union with an order
		 * by and limit on the overall result. However, Neo4j 3.x has no way to
		 * do this post-union processing, so we are using collect here as
		 * suggested by this post
		 * https://neo4j.com/blog/cypher-union-query-using-collect-clause.
		 * 
		 * Post-union processing is supported in Neo4j 4.x with CALL { }, so
		 * when we upgrade we may be able to simplify this and
		 * NeoSubgraphIterator again.
		 */
		final List<String> pathVars = new ArrayList<>();
		final List<String> pathPatterns = new ArrayList<>();
		final List<String> rowMapEntries = new ArrayList<>();
		final List<String> rowMapDotAccess = new ArrayList<>();

		final Map<String, LinkInfo> pathVarToLink = new HashMap<>();

		int pathCount = 0;
		for (final LinkInfo link : template.getLinks()) {

			final String pathVar = "sq_path__" + pathCount;
			pathVars.add(pathVar);
			pathVarToLink.put(pathVar, link);
			rowMapEntries.add(format("%1$s: %1$s", pathVar));
			rowMapDotAccess.add(format("paths.%1$s as %1$s", pathVar));
			pathPatterns.add(linkToParameterizedPattern(pathVar, link));
			pathCount++;
		}
		if (template.getOrderBy() != null) {
			for (final String orderByNode : template.getOrderBy()) {
				rowMapEntries.add(format("%1$s: %1$s", orderByNode));
				rowMapDotAccess.add(format("paths.%1$s as %1$s", orderByNode));
			}
		}

		final StringBuilder sb = new StringBuilder();
		sb.append("match ");
		sb.append(commaNewlineJoiner.join(pathPatterns));

		final List<String> nodeConditions = getParameterizedNodeConditions(
				templateIdToNodeInfo.keySet(), template.getLinks(),
				emptyList());
		if (!nodeConditions.isEmpty()) {
			sb.append("\nwhere ");
			sb.append(andJoiner.join(nodeConditions));
		}

		int subqueryIndex = 0;
		String subqueryCollectionName = format("rows_%d", subqueryIndex);
		sb.append(format("\nwith collect({ %s }) as %s",
				commaJoiner.join(rowMapEntries),
				subqueryCollectionName));

		for (final Set<String> omitted : Sets
				.powerSet(templateIdToOptionalNode.keySet())) {
			if (!omitted.isEmpty()) {
				final String previousSubqueryCollectionName = subqueryCollectionName;
				subqueryIndex++;
				subqueryCollectionName = format("rows_%d", subqueryIndex);
				sb.append(getSubquery(omitted,
						pathVars,
						pathVarToLink,
						previousSubqueryCollectionName,
						subqueryCollectionName));
			}
		}

		sb.append(format("\nunwind %s as paths", subqueryCollectionName));
		sb.append(format("\nwith %s", commaJoiner.join(rowMapDotAccess)));

		sb.append("\nreturn ");
		sb.append(commaJoiner.join(pathVars));
		sb.append(getOrderByClause(template.getOrderBy()));
		sb.append(" limit {limit}");

		final String query = sb.toString();
		final Map<String, Object> parameters = ImmutableMap.of("graph",
				graph,
				"createdAfter",
				since,
				"limit",
				limit);
		logger.debug("{}: returning query: {} and parameters: {}", m, query,
				parameters);
		return new Statement(query, parameters);
	}

	private String getSubquery(
			Set<String> excludedNodes,
			List<String> pathVars,
			Map<String, LinkInfo> pathVarToLink,
			String previousSubqueryCollectionName,
			String subqueryCollectionName) {

		final List<String> pathPatterns = new ArrayList<>();
		final List<String> rowMapEntries = new ArrayList<>();
		final List<String> rowMapDotAccess = new ArrayList<>();
		final List<LinkInfo> includedLinks = new ArrayList<>();
		final List<LinkInfo> excludedLinks = new ArrayList<>();

		for (final String pathVar : pathVars) {
			rowMapDotAccess.add(format("paths.%1$s as %1$s", pathVar));
			final LinkInfo link = pathVarToLink.get(pathVar);
			if (excludedNodes.contains(link.getSourceId())
					|| excludedNodes.contains(link.getTargetId())) {
				rowMapEntries.add(format("%s: null", pathVar));
				excludedLinks.add(link);
			} else {
				rowMapEntries.add(format("%1$s: %1$s", pathVar));
				pathPatterns.add(linkToParameterizedPattern(pathVar, link));
				includedLinks.add(link);
			}
		}
		// Assuming that it is illegal to order by an optional node.
		if (template.getOrderBy() != null) {
			for (final String orderByNode : template.getOrderBy()) {
				rowMapEntries.add(format("%1$s: %1$s", orderByNode));
				rowMapDotAccess.add(format("paths.%1$s as %1$s", orderByNode));
			}
		}

		final StringBuilder sb = new StringBuilder();
		sb.append("\noptional match ");
		sb.append(commaNewlineJoiner.join(pathPatterns));

		final Set<String> includedNodes = Sets
				.difference(templateIdToNodeInfo.keySet(), excludedNodes);
		final List<String> nodeConditions = getParameterizedNodeConditions(
				includedNodes, includedLinks, excludedLinks);
		if (!nodeConditions.isEmpty()) {
			sb.append("\nwhere ");
			sb.append(andJoiner.join(nodeConditions));
		}
		sb.append(format("\nwith %s + collect({ %s }) as %s",
				previousSubqueryCollectionName,
				commaJoiner.join(rowMapEntries),
				subqueryCollectionName));

		return sb.toString();
	}

	private static String getOrderByClause(@Nullable List<String> orderBy) {
		if (orderBy == null || orderBy.isEmpty()) {
			return "";
		}
		final Iterable<String> orderByCreated = transform(orderBy,
				n -> n + "._created");
		return String.format(" order by %s", commaJoiner.join(orderByCreated));
	}

	public Optional<SubgraphInstance> recordToInstance(Record row)
			throws IOException {
		int links = -1;
		long maxTimestamp = Long.MIN_VALUE;
		final List<RankInstance> ranks = new ArrayList<>();
		final List<LinkInstance> linkInstances = new ArrayList<>();
		final Map<String, NodeInstance> templateIdToNode = new HashMap<>();
		for (final Value value : row.values()) {
			links++;
			if (value.isNull()) {
				continue;
			}
			final Path path = value.asPath();
			final Iterator<Node> nodeIter = path.nodes().iterator();

			final Node startNode = nodeIter.next();
			final Node endNode = nodeIter.next();
			checkState(!nodeIter.hasNext());

			final Value startCreated = startNode.get("_created");
			if (startCreated != null) {
				maxTimestamp = Long.max(maxTimestamp, startCreated.asLong());
			}
			final LinkInfo linkTemplate = template.getLinks().get(links);
			NodeInstance startNodeInstance = templateIdToNode
					.get(linkTemplate.getSourceId());
			if (startNodeInstance == null) {
				startNodeInstance = neoNodeToInstance(startNode);
				templateIdToNode.put(linkTemplate.getSourceId(),
						startNodeInstance);
			}

			final Value endCreated = endNode.get("_created");
			if (endCreated != null) {
				maxTimestamp = Long.max(maxTimestamp, endCreated.asLong());
			}
			NodeInstance endNodeInstance = templateIdToNode
					.get(linkTemplate.getTargetId());
			if (endNodeInstance == null) {
				endNodeInstance = neoNodeToInstance(endNode);
				templateIdToNode.put(linkTemplate.getTargetId(),
						endNodeInstance);
			}

			final Relationship link = path.relationships().iterator()
					.next();
			final StructuredData<String, Object> linkData = new StructuredValue<>();
			linkData.putAll(link.asMap());
			final TupleWithSchema<String> linkTuple = ObjectSerialization
					.getTupleFromObjectData("Link", linkData);
			LinkInstance linkInstance = new LinkInstance(
					startNodeInstance.getId(),
					endNodeInstance.getId(),
					link.type(),
					TupleWithSchemaModel.from(linkTuple));
			linkInstances.add(linkInstance);

		}

		for (final List<NodeInfo> rank : template.getRanks()) {
			final List<NodeInstance> nodeInstances = new ArrayList<>();
			for (NodeInfo nodeTemplate : rank) {
				final NodeInstance nodeInstance = templateIdToNode
						.get(nodeTemplate.getId());
				if (nodeInstance != null) {
					nodeInstances.add(nodeInstance);
				}
			}
			if (!nodeInstances.isEmpty()) {
				final RankInstance rankInstance = new RankInstance(
						nodeInstances);
				ranks.add(rankInstance);
			}
		}
		if (ranks.isEmpty()) {
			return Optional.empty();
		}
		final SubgraphInstance instance = new SubgraphInstance(
				ranks,
				linkInstances,
				maxTimestamp);
		return Optional.of(instance);
	}

	private NodeInstance neoNodeToInstance(Node node) throws IOException {
		final Value keyValue = node.get("_key");
		final Value tupleValue = node.get("tuple");
		@SuppressWarnings("unchecked")
		final TupleWithSchema<String> tuple = (TupleWithSchema<String>) ObjectSerialization
				.getObject(tupleValue.asString());
		final NodeInstance nodeInstance = new NodeInstance(
				keyValue.asString(),
				TupleWithSchemaModel.from(tuple));
		return nodeInstance;
	}

	private static String linkToParameterizedPattern(
			String pathVar,
			LinkInfo link) {
		final String pattern = String
				.format("%s=(%s:meta { _resource: {graph} })-[r%s:`%s`]->(%s:meta { _resource: {graph} })",
						pathVar,
						link.getSourceId(),
						pathVar,
						link.getType(),
						link.getTargetId());
		return pattern;
	}

	private List<String> getParameterizedNodeConditions(
			Set<String> includedNodeIds,
			Iterable<LinkInfo> includedLinks,
			Iterable<LinkInfo> excludedLinks) {
		final List<String> nodeConditions = new ArrayList<>();
		for (final String includedNodeId : includedNodeIds) {
			final NodeInfoWithLocation node = templateIdToNodeInfo
					.get(includedNodeId);
			if (node.info.useSince()) {
				nodeConditions.add(String.format(
						"%s._created > {createdAfter}",
						node.info.getId()));
			}
		}

		final List<String> pairOrderings = new ArrayList<>();
		final Map<LinkEndpointKey, Set<String>> endpointToAdjacentNodeIds = new HashMap<>();
		for (final LinkInfo includedLink : includedLinks) {
			final String sourceId = includedLink.getSourceId();
			final String targetId = includedLink.getTargetId();
			final String linkType = includedLink.getType();

			final LinkEndpointKey sourceKey = new LinkEndpointKey(
					sourceId,
					LinkEndpointKey.NodeRole.SOURCE,
					linkType);
			final LinkEndpointKey targetKey = new LinkEndpointKey(
					targetId,
					LinkEndpointKey.NodeRole.TARGET,
					linkType);

			endpointToAdjacentNodeIds
					.computeIfAbsent(sourceKey,
							key -> new HashSet<>())
					.add(targetId);
			endpointToAdjacentNodeIds
					.computeIfAbsent(targetKey,
							key -> new HashSet<>())
					.add(sourceId);

		}

		endpointToAdjacentNodeIds
				.values()
				.stream()
				.distinct()
				.forEach(
						similarIdSet -> {
							if (similarIdSet.size() > 1) {
								final List<String> similarIds = new ArrayList<>(
										similarIdSet);
								sort(similarIds, templateIdComparator);
								for (int i = 0; i < similarIds.size()
										- 1; i++) {
									final String left = similarIds.get(i);
									final String right = similarIds.get(i + 1);
									final String pairOrdering = String
											.format("(%1$s._created < %2$s._created)",
													left, right);
									pairOrderings.add(pairOrdering);
								}
							}
						});

		nodeConditions.addAll(pairOrderings);

		final List<String> excludedLinkClauses = new ArrayList<>();
		for (final LinkInfo excludedLink : excludedLinks) {
			final String sourceId = includedNodeIds.contains(
					excludedLink.getSourceId())
							? excludedLink.getSourceId()
							: "";
			final String targetId = includedNodeIds.contains(
					excludedLink.getTargetId())
							? excludedLink.getTargetId()
							: "";
			excludedLinkClauses.add(format(
					"(not (%s:meta { _resource: {graph} })-[:`%s`]->(%s:meta { _resource: {graph} }))",
					sourceId,
					excludedLink.getType(),
					targetId));
		}
		nodeConditions.addAll(excludedLinkClauses);
		return nodeConditions;
	}

}
