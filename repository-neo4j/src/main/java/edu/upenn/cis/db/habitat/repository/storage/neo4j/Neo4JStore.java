/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.repository.storage.neo4j;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.neo4j.driver.v1.Values.parameters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;

import javax.annotation.Nullable;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.neo4j.driver.v1.AuthToken;
import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Config;
import org.neo4j.driver.v1.Config.EncryptionLevel;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.Statement;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.Transaction;
import org.neo4j.driver.v1.TransactionWork;
import org.neo4j.driver.v1.Value;
import org.neo4j.driver.v1.summary.ResultSummary;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.io.CharStreams;
import com.google.inject.Inject;
import com.google.inject.name.Named;

import edu.upenn.cis.db.habitat.core.api.StorageApi;
import edu.upenn.cis.db.habitat.core.type.StringEncoding;
import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.TransactionSupplier;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.repository.storage.GraphStoreWithAlgorithms;
import edu.upenn.cis.db.habitat.util.ObjectSerialization;

public class Neo4JStore implements GraphStoreWithAlgorithms<String> {
	public static final String NEO4J_BOLT_URI_PARAM = "Neo4jBoltUri";
	public static final String NEO4J_USER_PARAM = "Neo4jUser";
	public static final String NEO4J_PASSWORD_PARAM = "Neo4jPassword";

	static EncryptionLevel CRYPT = Config.EncryptionLevel.NONE;

	final static Logger logger = LogManager.getLogger(Neo4JStore.class);

	Config neoConfig = null;

	Driver driver = null;

	private final URI boltUri;
	private final String neo4jUser;
	private final String neo4jPassword;
	private static final AtomicLong created = new AtomicLong();

	@Inject
	public Neo4JStore(
			@Named(NEO4J_BOLT_URI_PARAM) URI boltUri,
			@Nullable @Named(NEO4J_USER_PARAM) String neo4jUser,
			@Nullable @Named(NEO4J_PASSWORD_PARAM) String neo4jPassword)
			throws SQLException {
		this.boltUri = checkNotNull(boltUri);
		this.neo4jUser = neo4jUser;
		this.neo4jPassword = neo4jPassword;
		connect();
		/*try {
			final StatementResult dropOldConstraintResult = getSession().run(
					"DROP CONSTRAINT ON (m:meta) ASSERT m._key IS UNIQUE");

			dropOldConstraintResult.consume();
		} catch (Exception e) {
			// Going to assume this is because the old constraint does not
			// exist.
			logger.warn("Exception trying to drop old uniqueness constraint on meta._key. If this exception is that there is no such constraint, then it can be ignored.", e);
		}*/
		try {
			final StatementResult constraintResult = getSession().run(
					"CREATE CONSTRAINT ON (m:meta) ASSERT m._composite_key IS UNIQUE");
			// Consume the result before returning to ensure that this query is
			// finished before any user transactions are started.
			// Otherwise, subject to
			// 'Database constraints have changed (txId=xxxxx) after this
			// transaction (txId=yyyyy) started, which is not yet supported'
			// errors.
			constraintResult.consume();
			final StatementResult resourceIndexResult = getSession().run(
					"CREATE INDEX ON :meta(_resource)");
			resourceIndexResult.consume();

			final StatementResult keyAndResourceIndexResult = getSession().run(
					"CREATE INDEX ON :meta(_key, _resource)");
			keyAndResourceIndexResult.consume();

			final StatementResult createdConstraintResult = getSession()
					.run(
							"CREATE CONSTRAINT ON (m:meta) ASSERT m._created IS UNIQUE");
			createdConstraintResult.consume();
			// Lock in case we have multiple constructors
			// And also use compare-and-set in case we are modifying
			// the graph while we call the constructor
			//
			// If the value has changed, we are going to assume that it
			// is already set to a higher value than the one we just queried
			// for
			synchronized (created) {
				long expectedValue = created.get();
				final StatementResult maxCreatedResult = getSession().run(
						"MATCH (n:meta) RETURN max(n._created)");
				final Value maxCreatedValue = maxCreatedResult.single().get(0);
				final long maxCreated = maxCreatedValue.isNull() ? 0
						: maxCreatedValue.asLong();
				if (expectedValue < maxCreated
						&& created.compareAndSet(expectedValue, maxCreated))
					logger.info("Neo4J store initialized with new Node ID "
							+ maxCreated);
			}
		} catch (Exception e) {
			logger.warn("could not create index", e);
		}
	}

	public Neo4JStore() throws SQLException {
		this(
				URI.create("bolt://"
						+ edu.upenn.cis.db.habitat.Config.getNeo4jHost()),
				edu.upenn.cis.db.habitat.Config.getNeo4jUser(),
				edu.upenn.cis.db.habitat.Config.getNeo4jPassword());
	}
	
	private static String getCompositeKey(String resource, String key) {
		return resource + ":" + key;
	}

	@Override
	public void addMetadata(String resource, String key,
			StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		Transaction trans = getTransactionFromSupplier(null);
		try {
			addMetadata(new Neo4JTransactionSupplier(trans), resource, key,
					metadata);
			trans.success();
		} finally {
			trans.close();
		}
	}

	@Override
	public void addMetadata(String resource, String key,
			StructuredData<String, ? extends Object> metadata,
			String specialLabel)
			throws HabitatServiceException {
		Transaction trans = getTransactionFromSupplier(null);
		try {
			addMetadata(new Neo4JTransactionSupplier(trans), resource, key,
					metadata, specialLabel);
			trans.success();
		} finally {
			trans.close();
		}
	}
	
	@Override
	public void addMetadata(TransactionSupplier supplier, String resource,
			String key, StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		Map<String, Object> properties = new HashMap<>();

		serializeStructuredDataForNeo4J(metadata, properties);
		properties.put("_resource", resource);

		getTransactionFromSupplier(supplier)
				.run(
						"MERGE (n:meta {_key: {key}, _resource: {resource} }) ON CREATE SET n._created = {created}, n._composite_key = {compositeKey} SET n += {props} ",
						parameters("key", key, "resource", resource, "props", properties, "created",
								created.incrementAndGet(),
								"compositeKey", getCompositeKey(resource, key)));
	}

	@Override
	public void addMetadata(TransactionSupplier supplier, String resource,
			String key, StructuredData<String, ? extends Object> metadata,
			String specialLabel)
			throws HabitatServiceException {
		
		// If there's no label, just do the normal
		if (specialLabel == null || specialLabel.isBlank()) {
			addMetadata(supplier, resource, key, metadata);
			return;
		}
		
		Map<String, Object> properties = new HashMap<>();

		serializeStructuredDataForNeo4J(metadata, properties);
		properties.put("_resource", resource);

		getTransactionFromSupplier(supplier)
				.run(
						"MERGE (n:meta {_key: {key}, _resource: {resource} }) ON CREATE SET n._created = {created}, n._composite_key = {compositeKey} SET n += {props} SET n :" + specialLabel,
						parameters("key", key, "resource", resource, "props", properties, "created",
								created.incrementAndGet(),
								"compositeKey",
								getCompositeKey(resource, key)));
	}

	@Override
	public void connect() throws SQLException {
		try {
			neoConfig = CRYPT.equals(EncryptionLevel.NONE)
					? Config.build().withoutEncryption().toConfig()
					: Config.build().withEncryption().toConfig();
			final AuthToken authToken = this.neo4jUser == null
					? AuthTokens.none()
					: AuthTokens.basic(this.neo4jUser,
							this.neo4jPassword);
			driver = GraphDatabase.
					driver(this.boltUri,
							authToken,
							neoConfig);
		} catch (Exception e) {
			logger.error("Unable to connect to " + getHost() + " as "
					+ getSuperuser(), e);
			throw new SQLException("Unable to connect to " + getHost() + " as "
					+ getSuperuser());
		}
	}

	@VisibleForTesting
	public void close() {
		driver.close();
	}

	@Override
	public boolean execTransaction(String resource,
			Function<TransactionSupplier, Boolean> transactionFn) {
		try (final Session session = getSession())
		{
			session.writeTransaction(new TransactionWork<Integer>()
			{
				@Override
				public Integer execute(Transaction tx)
				{
					try {
						Integer ret = transactionFn
								.apply(new Neo4JTransactionSupplier(tx)) ? 1
								: 0;

						if (ret == 1)
							tx.success();
						return ret;
					} finally {
						tx.close();
					}
				}
			});
		}
		return true;
	}

	@Override
	public int getCount(String resource) throws HabitatServiceException {
		Iterator<Pair<String, StructuredData<String, Object>>> iter =
				this.getMetadataForResource(resource).iterator();

		int count = 0;
		while (iter.hasNext()) {
			iter.next();
			count++;
		}
		return count;
	}

	@Override
	public Object getData(String resource, String key) {
		Transaction trans = getTransactionFromSupplier(null);
		try {
			Object ret = getData(new Neo4JTransactionSupplier(trans), resource,
					key);
			trans.success();
			return ret;
		} finally {
			trans.close();
		}
	}

	@Override
	public Object getData(TransactionSupplier supplier, String resource,
			String key) {

		final Transaction transaction = getTransactionFromSupplier(supplier);
		StatementResult result = transaction
				.run(
						"MATCH (n:meta) WHERE n._key = {key} AND n._resource = {resource} "
								+
								"RETURN n._value",
						parameters("key", key, "resource", resource));
		if (!result.hasNext()) {
			result = transaction
					.run(
							"MATCH (n:file) WHERE n._key = {key} AND n._resource = {resource} "
									+
									"RETURN n._content",
							parameters("key", key, "resource", resource));
		}

		if (!result.hasNext())
			return null;
		else {
			return new NeoNodeValueIterable(result).iterator().next();
		}
	}

	@Override
	public String getDatabase() {
		return boltUri.getHost() + ":" + boltUri.getPort();
	}

	@Override
	public String getHost() {
		return boltUri.getHost() + ":" + boltUri.getPort();
	}

	@Override
	public Iterable<String> getKeysMatching(String resource,
			String metadataField, Map<String, ? extends Object> match) {
		Transaction trans = getTransactionFromSupplier(null);
		try {
			Iterable<String> ret = getKeysMatching(
					new Neo4JTransactionSupplier(trans),
					resource, metadataField, match);

			trans.success();
			return ret;
		} finally {
			trans.close();
		}
	}

	@Override
	public Iterable<String> getKeysMatching(TransactionSupplier supplier,
			String resource, String metadataField,
			Map<String, ? extends Object> match) {
		Map<String, Object> match2 = new HashMap<>(match);
		match2.put("_resource", resource);
		StatementResult result = getTransactionFromSupplier(supplier).run(
				"MATCH (n:meta {prop}) " +
						"RETURN n._key",
				parameters("prop", match2));

		if (!result.hasNext())
			return null;
		else {
			// List<String> ret = new ArrayList<>();
			// while (result.hasNext()) {
			// Record res = result.next();
			// ret.add(res.get(0).asString());
			// }
			// return ret;
			return new NeoNodeIdIterable(result);
		}
	}

	@Override
	public Iterable<Pair<String, StructuredData<String, Object>>> getLinksFrom(
			String resource, String key1)
			throws HabitatServiceException {
		Transaction trans = getTransactionFromSupplier(null);
		try {
			Iterable<Pair<String, StructuredData<String, Object>>> ret = getLinksFrom(
					new Neo4JTransactionSupplier(trans),
					resource, key1);

			trans.success();
			return ret;
		} finally {
			trans.close();
		}
	}

	@Override
	public Iterable<Pair<String, StructuredData<String, Object>>> getLinksFrom(
			TransactionSupplier supplier,
			String resource, String key)
			throws HabitatServiceException {
		StatementResult result = getTransactionFromSupplier(supplier).run(
				"MATCH (n:meta {_key: {key}, _resource: {resource} } )  " +
						"MATCH (n) - [e] -> (m:meta { _resource: {resource} } ) " +
						"RETURN m._key, e",
				parameters("key", key, "resource", resource));

		return new NeoEdgeIterable(result);
	}

	@Override
	public Iterable<Pair<String, StructuredData<String, Object>>> getLinksTo(
			String resource, String key1) throws HabitatServiceException {
		Transaction trans = getTransactionFromSupplier(null);
		try {
			Iterable<Pair<String, StructuredData<String, Object>>> ret = getLinksTo(
					new Neo4JTransactionSupplier(trans),
					resource, key1);

			trans.success();
			return ret;
		} finally {
			trans.close();
		}

	}

	@Override
	public Iterable<Pair<String, StructuredData<String, Object>>> getLinksTo(
			TransactionSupplier supplier, String resource, String key)
			throws HabitatServiceException {
		StatementResult result = getTransactionFromSupplier(supplier).run(
				"MATCH (n:meta {_key: {key}, _resource: {resource} } )  " +
						"MATCH (m:meta {_resource: {resource} } ) - [e] -> (n) " +
						"RETURN m._key, e",
				parameters("key", key, "resource", resource));

		return new NeoEdgeIterable(result);
	}

	@Override
	public StructuredData<String, Object> getMetadata(String resource,
			String key) throws HabitatServiceException {
		Transaction trans = getTransactionFromSupplier(null);
		try {
			StructuredData<String, Object> ret = getMetadata(
					new Neo4JTransactionSupplier(trans), resource, key);

			trans.success();
			return ret;
		} finally {
			trans.close();
		}
	}

	@Override
	public StructuredData<String, Object> getMetadata(
			TransactionSupplier supplier, String resource, String key)
			throws HabitatServiceException {
		StatementResult result = getTransactionFromSupplier(supplier)
				.run(
						"MATCH (n:meta) WHERE n._key = {key} AND n._resource = {resource} "
								+
								"RETURN *",
						parameters("key", key, "resource", resource));

		if (result.hasNext())
			return new NeoNodeIterable(result).iterator().next();
		else
			return null;
	}

	@Override
	public Iterable<Pair<String, StructuredData<String, Object>>> getMetadataForResource(
			String resource)
			throws HabitatServiceException {
		Transaction trans = getTransactionFromSupplier(null);
		try {
			Iterable<Pair<String, StructuredData<String, Object>>> ret =
					getMetadataForResource(new Neo4JTransactionSupplier(trans),
							resource);

			trans.success();
			return ret;
		} finally {
			trans.close();
		}
	}

	@Override
	public Iterable<Pair<String, StructuredData<String, Object>>> getMetadataForResource(
			TransactionSupplier supplier,
			String resource) throws HabitatServiceException {
		StatementResult result = getTransactionFromSupplier(supplier).run(
				"MATCH (n:meta) WHERE n._resource = {resource} " +
						"RETURN *",
				parameters("resource", resource));

		return new NeoKeyedNodeIterable(result);
	}

	@Override
	public int getPort() {
		return boltUri.getPort();
	}

	public synchronized Session getSession() {
		return driver.session();
	}

	@Override
	public StoreClass getStoreClass() {
		return StorageApi.StoreClass.GRAPH;
	}

	@Override
	public String getStoreUuid() {
		return "neo4j-1";
	}

	@Override
	public String getSuperuser() {
		return neo4jUser;
	}

	private synchronized Transaction getTransactionFromSupplier(
			TransactionSupplier supplier) {
		if (supplier == null)
			return getSession().beginTransaction();
		if (supplier instanceof Neo4JTransactionSupplier)
			return ((Neo4JTransactionSupplier) supplier).getTransactionObject();
		else
			throw new UnsupportedOperationException(
					"Unable to use non-Neo4J session supplier");
	}

	@Override
	public boolean isDataLegal(StructuredData<String, ? extends Object> metadata) {
		return ObjectSerialization.isTupleLegal(metadata);
	}

	@Override
	public boolean isSuitableForMetadata() {
		return true;
	}

	@Override
	public boolean isSuitableToStore(Object obj) {
		Object val = null;
		try {
			val = ObjectSerialization.putObject(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return false;
		}
		if (val != null
				&& (val instanceof String && ((String) val).length() > edu.upenn.cis.db.habitat.Config
						.getMaxLength()))
			return false;

		return true;
	}

	@Override
	public void link(String resource, String key1, String label, String key2,
			StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		Transaction trans = getTransactionFromSupplier(null);
		try {
			link(new Neo4JTransactionSupplier(trans), resource, key1, label, key2,
					metadata);
			trans.success();
		} finally {
			trans.close();
		}
	}

	@Override
	public void link(TransactionSupplier supplier, String resource,
			String key1, String label, String key2,
			StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		StatementResult result = null;
		if (metadata != null) {
			Map<String, Object> properties = new HashMap<>();
			serializeStructuredDataForNeo4J(metadata, properties);

			result = getTransactionFromSupplier(supplier)
					.run("MATCH (n:meta {_key: {key1}, _resource: {resource} } ) MATCH (m:meta {_key: {key2}, _resource: {resource} } ) "
							+
							"CREATE (n)-[e:`" + label + "`]->(m) SET e = {props}",
							parameters("key1", key1, "key2", key2, "props",
									properties, "resource", resource));
		} else {
			result = getTransactionFromSupplier(supplier)
					.run("MATCH (n:meta {_key: {key1}, _resource: {resource} } ) MATCH (m:meta {_key: {key2}, _resource: {resource} } ) "
							+
							"CREATE (n)-[e:`" + label + "`]->(m) ",
							parameters("key1", key1, "key2", key2, "resource",
									resource));
		}
		final int created = result.summary().counters().relationshipsCreated();
		if (created == 0) {
			throw new HabitatServiceException("Missing endpoint: Endpoint "
					+ key1 + " or " + key2 + " does not exist.");
		}
	}

	@Override
	public void removeKey(String resource, String key) {
		Transaction trans = getTransactionFromSupplier(null);
		try {
			removeKey(new Neo4JTransactionSupplier(trans), resource, key);
			trans.success();
		} finally {
			trans.close();
		}
	}

	@Override
	public void removeKey(TransactionSupplier supplier, String resource,
			String key) {
		getTransactionFromSupplier(supplier)
				.run(
						"MATCH (n:meta) WHERE n._key = {key} AND n._resource = {resource} "
								+
								"DETACH DELETE n",
						parameters("key", key, "resource", resource));
	}

	@Override
	public void removeMetadata(String resource, String key, String metadataField) {
		Transaction trans = getTransactionFromSupplier(null);
		try {
			removeMetadata(new Neo4JTransactionSupplier(trans), resource, key,
					metadataField);
			trans.success();
		} finally {
			trans.close();
		}
	}

	@Override
	public void removeMetadata(TransactionSupplier supplier, String resource,
			String key, String metadataField) {
		Map<String, Object> properties = new HashMap<>();
		properties.put("_key", key);

		getTransactionFromSupplier(supplier).run(
				"MERGE (n:meta {_key: {key}, _resource: {resource} }) REMOVE n." + metadataField,
				parameters("key", key, "resource", resource));

	}

	@Override
	public void replaceMetadata(String resource, String key,
			StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		Transaction trans = getTransactionFromSupplier(null);
		try {
			replaceMetadata(new Neo4JTransactionSupplier(trans), resource, key,
					metadata);
			trans.success();
		} finally {
			trans.close();
		}
	}

	@Override
	public void replaceMetadata(TransactionSupplier supplier, String resource,
			String key,
			StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		Map<String, Object> properties = new HashMap<>();
		serializeStructuredDataForNeo4J(metadata, properties);

		properties.put("_key", key);
		properties.put("_resource", resource);

		getTransactionFromSupplier(supplier)
				.run("MERGE (n:meta {_key: {key}, _resource: {resource} }) SET n = {props} ",
						parameters("key", key, "resource", resource, "props",
								properties));

		addMetadata(supplier, resource, key, metadata);
	}

	@Override
	public void store(String resource, String key, Object value,
			StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		Transaction trans = getTransactionFromSupplier(null);
		try {
			store(new Neo4JTransactionSupplier(trans), resource, key, value,
					metadata);
			trans.success();
		} finally {
			trans.close();
		}
	}

	@Override
	public void store(TransactionSupplier supplier, String resource,
			String key, Object value,
			StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {

		Object val = null;
		try {
			val = ObjectSerialization.putObject(value);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			throw new HabitatServiceException(e.getMessage());
		}
		if (val != null
				&& (val instanceof String && ((String) val).length() > edu.upenn.cis.db.habitat.Config
						.getMaxLength()))
			throw new RuntimeException(
					"Value exceeds specified limit: do not store CLOB/BLOB values");

		addMetadata(supplier, resource, key, metadata);
		if (val != null) {
			getTransactionFromSupplier(supplier).run(
					"MERGE (n:meta { _key: {key}, _resource: {resource} }) " +
							"SET n._value = {value}",
					parameters("key", key,
							"resource", resource,
							"value", value));

		}
	}

	@Override
	public void storeReference(String resource, String key, String refToValue,
			StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		Transaction trans = getTransactionFromSupplier(null);
		try {
			storeReference(new Neo4JTransactionSupplier(trans), resource, key,
					refToValue, metadata);
			trans.success();
		} finally {
			trans.close();
		}
	}

	@Override
	public void storeReference(TransactionSupplier supplier, String resource,
			String key, String refToValue,
			StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		Map<String, Object> properties = new HashMap<>();

		serializeStructuredDataForNeo4J(metadata, properties);
		properties.put("_resource", resource);
		properties.put("_valuePtr", refToValue);

		getTransactionFromSupplier(supplier).run(
				"MERGE (n:meta {_key: {key}, _resource: {resource} }) SET n += {props} ",
				parameters("key", key, "resource", resource, "props", properties));
	}

	@Override
	public void storeFile(String resource, String key, InputStream file,
			int streamLength,
			StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		Transaction trans = getTransactionFromSupplier(null);
		try {
			storeFile(new Neo4JTransactionSupplier(trans), resource, key, file,
					streamLength, metadata);
			trans.success();
		} finally {
			trans.close();
		}
	}

	@Override
	public void storeFile(TransactionSupplier supplier, String resource,
			String key,
			InputStream file, int streamLength,
			StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {

		final Map<String, Object> properties = new HashMap<>();

		serializeStructuredDataForNeo4J(metadata, properties);
		properties.put("_resource", resource);
		
		String content = null;
		try (final BufferedReader reader = new BufferedReader(new InputStreamReader(file))) {
			content = CharStreams.toString(reader);
			properties.put("_content", content);
		} catch (IOException e) {
			throw new HabitatServiceException("Exception closing input stream", e);
		}
		
		getTransactionFromSupplier(supplier)
		.run(
				"MERGE (n:file {_key: {key}, _resource: {resource}}) SET n += {props}",
				parameters("key", key, "resource", resource, "props", properties, "resource", resource));
	}

	@Override
	public void storeUrlContents(String resource, String key, URL url,
			StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		Transaction trans = getTransactionFromSupplier(null);
		try {
			storeUrlContents(new Neo4JTransactionSupplier(trans), resource,
					key, url, metadata);
			trans.success();
		} finally {
			trans.close();
		}
	}

	@Override
	public void storeUrlContents(TransactionSupplier supplier, String resource,
			String key, URL url,
			StructuredData<String, ? extends Object> metadata)
			throws HabitatServiceException {
		// TODO Auto-generated method stub

	}

	@Override
	public void createGraph(String resourceName) {
		// This does nothing in Neo4J, though it could create a root

	}

	@Override
	public void createOrResetGraph(String resourceName) {
		final String m = "createOrResetGraph(...)";
		Transaction trans = getTransactionFromSupplier(null);
		Neo4JTransactionSupplier supplier = new Neo4JTransactionSupplier(trans);
		try {
			final StatementResult deleteResult = getTransactionFromSupplier(supplier).run(
					"MATCH (n { _resource: {resource} }) " +
							"DETACH DELETE n",
					parameters("resource", resourceName));
			final ResultSummary summary = deleteResult.summary();
			logger.info("{}: deleted {} nodes", m, summary.counters().nodesDeleted());
			trans.success();
		} finally {
			trans.close();
		}
	}
	
	@Override
	public Iterable<Pair<String, Double>> getTopKBetweennessNodes(
			String resource,
			String edgeRestriction,
			int k) {
		Transaction trans = getTransactionFromSupplier(null);
		try {
			Iterable<Pair<String, Double>> ret =
					getTopKBetweennessNodes(
							new Neo4JTransactionSupplier(trans), resource,
							edgeRestriction, k);
			trans.success();
			return ret;
		} finally {
			trans.close();
		}
	}

	@Override
	public Iterable<Pair<String, Double>> getTopKBetweennessNodes(
			TransactionSupplier supplier,
			String resource,
			String edgeRestriction, int k) {
		StatementResult result = getTransactionFromSupplier(supplier).run(
				"MATCH (n:meta) " +
						"WHERE n._resource = {resource} " +
						"WITH COLLECT (n) AS nodes " +
						"CALL apoc.algo.betweenness(["
						+ (edgeRestriction == null ? edgeRestriction : "") +
						"],nodes,'OUTGOING') YIELD n, score" +
						"RETURN n._id, score " +
						"ORDER BY score DESC " +
						"LIMIT " + k,
				parameters("resource", resource));

		if (!result.hasNext())
			return null;
		else {
			return new NeoRankedNodeIterable(result);
		}
	}

	@Override
	public Iterable<Pair<String, Double>> getTopKClosenessNodes(
			String resource,
			String edgeRestriction,
			int k) {
		Transaction trans = getTransactionFromSupplier(null);
		try {
			Iterable<Pair<String, Double>> ret =
					getTopKClosenessNodes(new Neo4JTransactionSupplier(trans),
							resource, edgeRestriction, k);
			trans.success();
			return ret;
		} finally {
			trans.close();
		}
	}

	@Override
	public Iterable<Pair<String, Double>> getTopKClosenessNodes(
			TransactionSupplier supplier,
			String resource,
			String edgeRestriction, int k) {
		StatementResult result = getTransactionFromSupplier(supplier).run(
				"MATCH (n:meta) " +
						"WHERE n._resource = {resource} " +
						"WITH COLLECT (n) AS nodes " +
						"CALL apoc.algo.closeness(["
						+ (edgeRestriction == null ? edgeRestriction : "") +
						"],nodes,'OUTGOING') YIELD n, score" +
						"RETURN n._id, score " +
						"ORDER BY score DESC " +
						"LIMIT " + k,
				parameters("resource", resource));

		if (!result.hasNext())
			return null;
		else {
			return new NeoRankedNodeIterable(result);
		}
	}

	@Override
	public Iterable<Pair<String, Double>> getTopKPageRankNodes(
			String resource,
			int k) {
		Transaction trans = getTransactionFromSupplier(null);
		try {
			Iterable<Pair<String, Double>> ret = getTopKPageRankNodes(
					new Neo4JTransactionSupplier(trans), resource, k);
			trans.success();
			return ret;
		} finally {
			trans.close();
		}
	}

	@Override
	public Iterable<Pair<String, Double>> getTopKPageRankNodes(
			TransactionSupplier supplier,
			String resource,
			int k) {
		StatementResult result = getTransactionFromSupplier(supplier).run(
				"MATCH (n:meta) " +
						"WHERE n._resource = {resource} " +
						"WITH COLLECT (n) AS nodes " +
						"CALL apoc.algo.pageRank() YIELD n, score" +
						"RETURN n._id, score " +
						"ORDER BY score DESC " +
						"LIMIT " + k,
				parameters("resource", resource));

		if (!result.hasNext())
			return null;
		else {
			return new NeoRankedNodeIterable(result);
		}
	}

	@Override
	public Iterable<SubgraphInstance> getSubgraphs(String resource,
			SubgraphTemplate template,
			int limit,
			Long since) throws HabitatServiceException {
		Transaction trans = getTransactionFromSupplier(null);
		try {
			final Iterable<SubgraphInstance> subgraphs = getSubgraphs(
					new Neo4JTransactionSupplier(trans), resource,
					template,
					limit,
					since);
			trans.success();
			return subgraphs;
		} finally {
			trans.close();
		}
	}

	@Override
	public Iterable<SubgraphInstance> getSubgraphs(
			TransactionSupplier supplier,
			String resource,
			SubgraphTemplate template,
			int limit,
			Long since) throws HabitatServiceException {
		if (since == null) {
			return singletonList(new SubgraphInstance(emptyList(), emptyList(),
					created.get()));
		}
		final SubgraphQuery subgraphQuery = new SubgraphQuery(template);
		final Statement query = subgraphQuery
				.getStatement(resource, limit, since);
		final StatementResult result = getTransactionFromSupplier(supplier)
				.run(query);
		return new NeoSubgraphIterable(result, subgraphQuery);
	}

	private static void serializeStructuredDataForNeo4J(
			StructuredData<String, ? extends Object> metadata,
			Map<String, Object> properties)
			throws HabitatServiceException {
		ObjectSerialization.serializeStructuredData(metadata, properties);

		// Replace string-encoded objects with actual strings before we try
		// to save in Neo4J
		for (String key2 : properties.keySet())
			if (properties.get(key2) instanceof StringEncoding)
				properties.put(key2, properties.get(key2).toString());
	}
}
