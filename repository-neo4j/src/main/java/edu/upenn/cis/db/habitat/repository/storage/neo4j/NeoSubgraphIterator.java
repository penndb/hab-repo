/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.repository.storage.neo4j;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.StatementResult;

import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance;

/**
 * @author John Frommeyer
 *
 */
public class NeoSubgraphIterator implements Iterator<SubgraphInstance> {
	/*
	 * Why is this so complicated?
	 * 
	 * Because of optional nodes, we would like to do a union with an order by
	 * and limit on the overall result. However, Neo4j 3.x has no way to do
	 * post-union processing, so we are using a collect call in SubgraphQuery as
	 * suggested by this post
	 * https://neo4j.com/blog/cypher-union-query-using-collect-clause.
	 * 
	 * A drawback of this, is that some rows only contain null paths and do not
	 * correspond to a subgraph. So we have to complicate this Iterator to skip
	 * over such rows.
	 * 
	 * Post-union processing is supported in Neo4j 4.x with CALL { }, so when we upgrade we may be able to
	 * simplify this again.
	 */

	private final StatementResult resultSet;
	private final SubgraphQuery subgraphQuery;
	private SubgraphInstance next = null;

	public NeoSubgraphIterator(StatementResult resultSet,
			SubgraphQuery subgraphQuery) {
		this.resultSet = checkNotNull(resultSet);
		this.subgraphQuery = checkNotNull(subgraphQuery);
	}

	@Override
	public boolean hasNext() {
		next = getNext();
		return next != null;
	}

	@Override
	public SubgraphInstance next() {
		if (next == null) {
			next = getNext();
			if (next == null) {
				throw new NoSuchElementException("subgraph instance exhausted");
			}
		}
		final SubgraphInstance thisNext = next;
		next = null;
		return thisNext;
	}

	private SubgraphInstance getNext() {
		SubgraphInstance next = null;
		try {
			while (resultSet.hasNext()) {
				final Record row = resultSet.next();
				final Optional<SubgraphInstance> instance = subgraphQuery
						.recordToInstance(row);
				if (instance.isPresent()) {
					next = instance.get();
					break;
				}

			}
			return next;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
