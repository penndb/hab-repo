/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.repository.storage.neo4j;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.StatementResult;

import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.util.ObjectSerialization;

public class NeoKeyedNodeIterator implements Iterator<Pair<String,StructuredData<String,Object>>> {
	final StatementResult set;
	
	public NeoKeyedNodeIterator (StatementResult set) {
		this.set = set;
	}

	@Override
	public boolean hasNext() {
		return set.hasNext();
	}

	@Override
	public Pair<String,StructuredData<String,Object>> next() {
		if (!set.hasNext())
			return null;
		
		Record rec = set.next();
		Map<String,Object> res = rec.get(0).asMap();
		try {
			StructuredData<String, Object> ret = ObjectSerialization.deserialize(res);
			
			ret.remove("_resource");

			String key = (String)ret.remove("_key");
			ret.remove("_value");
			return new ImmutablePair<>(key, ret);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
}