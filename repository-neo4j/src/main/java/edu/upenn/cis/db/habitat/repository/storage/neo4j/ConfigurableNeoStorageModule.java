/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.repository.storage.neo4j;

import java.net.URI;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import com.google.inject.util.Providers;

import edu.upenn.cis.db.habitat.Config;

/**
 *
 */
public class ConfigurableNeoStorageModule extends AbstractModule {

	private final URI boltUri;
	private final String neo4jUser;
	private final String neo4jPassword;

	public ConfigurableNeoStorageModule(
			URI boltUri,
			String neo4jUser,
			String neo4jPassword) {
		this.boltUri = boltUri;
		this.neo4jUser = neo4jUser;
		this.neo4jPassword = neo4jPassword;
	}

	public ConfigurableNeoStorageModule() {
		this(URI.create("bolt://" + Config.getNeo4jHost()),
				Config.getNeo4jUser(),
				Config.getNeo4jPassword());
	}

	@Override
	protected void configure() {

		bind(URI.class)
				.annotatedWith(
						Names.named(Neo4JStore.NEO4J_BOLT_URI_PARAM))
				.toInstance(boltUri);
		bind(String.class)
				.annotatedWith(Names.named(Neo4JStore.NEO4J_USER_PARAM))
				.toProvider(Providers.of(neo4jUser));
		bind(String.class)
				.annotatedWith(Names.named(Neo4JStore.NEO4J_PASSWORD_PARAM))
				.toProvider(Providers.of(neo4jPassword));

		// bind(new TypeLiteral<GraphStore<String>>() {
		// }).to(Neo4JStore.class);

		// bind(new TypeLiteral<StorageApiWithMetadataLocal<String, Object, ?>>() {
		// })
		// 		.to(Neo4JStore.class);

	}
}