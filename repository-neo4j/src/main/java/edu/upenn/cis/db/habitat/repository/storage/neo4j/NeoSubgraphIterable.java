/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.repository.storage.neo4j;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Iterator;

import org.neo4j.driver.v1.StatementResult;

import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance;

/**
 * @author John Frommeyer
 *
 */
public class NeoSubgraphIterable implements Iterable<SubgraphInstance> {

	private final StatementResult resultSet;
	private final SubgraphQuery subgraphQuery;

	public NeoSubgraphIterable(StatementResult resultSet,
			SubgraphQuery subgraphQuery) {
		this.subgraphQuery = checkNotNull(subgraphQuery);
		this.resultSet = checkNotNull(resultSet);
	}

	@Override
	public Iterator<SubgraphInstance> iterator() {
		return new NeoSubgraphIterator(resultSet, subgraphQuery);

	}

}
