/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.repository.storage.neo4j;

import java.net.URI;
import java.sql.SQLException;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.name.Names;

import edu.upenn.cis.db.habitat.Config;
import edu.upenn.cis.db.habitat.core.api.StorageApi;
import edu.upenn.cis.db.habitat.core.api.StorageApiWithMetadata;
import edu.upenn.cis.db.habitat.repository.storage.StorageApiLocal;
import edu.upenn.cis.db.habitat.repository.storage.StorageApiWithMetadataLocal;

/**
 * Use this to replace StorageModule if you want protected storage
 * 
 * @author zives
 *
 */
public class NeoStorageModule  extends AbstractModule {
	
	@Override
	protected void configure() {
		bind(URI.class)
			.annotatedWith(Names.named(Neo4JStore.NEO4J_BOLT_URI_PARAM))
			.toInstance(URI.create("bolt://" + Config.getNeo4jHost()));

		bindConstant()
			.annotatedWith(Names.named(Neo4JStore.NEO4J_USER_PARAM))
			.to(Config.getNeo4jUser());
		
		bindConstant()
			.annotatedWith(Names.named(Neo4JStore.NEO4J_PASSWORD_PARAM))
			.to(Config.getNeo4jPassword());

		bind(StorageApiLocal.class).to(Neo4JStore.class);
		bind(StorageApi.class).to(Neo4JStore.class);
		bind(StorageApiWithMetadata.class).to(Neo4JStore.class);
		bind(StorageApiWithMetadataLocal.class).to(Neo4JStore.class);
		try {
			Neo4JStore store = new Neo4JStore();
			bind(new TypeLiteral<StorageApi<String,Object,?>>() {}).toInstance(store);
			bind(new TypeLiteral<StorageApiLocal<String,Object,?>>() {}).toInstance(store);
			bind(new TypeLiteral<StorageApiWithMetadata<String,Object,?>>() {}).toInstance(store);
			bind(new TypeLiteral<StorageApiWithMetadataLocal<String,Object,?>>() {}).toInstance(store);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}