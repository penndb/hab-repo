/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.client;

import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.inject.Inject;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;

import edu.upenn.cis.db.habitat.core.api.PermissionApi;
import edu.upenn.cis.db.habitat.core.api.UserApi;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * This isn't fully implemented
 * 
 * @author ZacharyIves
 *
 */
@Deprecated
public class PermissionClient implements PermissionApi {
	final static Logger logger = LogManager.getLogger(PermissionClient.class);

	UserApi users;
	
	@Inject
	public PermissionClient(UserApi users) {
		this.users = users;
	}

	@Override
	public int addPermission(String name) throws HabitatServiceException {
		logger.info("Adding permission named " + name);

		HttpResponse<String> result;
		try {
			result =
					RequestUtils.createAuthenticatedPost("/permissions/types/{permname}", getSessionToken())
					.routeParam("permname", name)
			.asString();
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new HabitatServiceException(e.getMessage());
		}
		
		if (result.getStatus() == 200) {
			return Integer.valueOf(result.getBody());
		} else
			throw new HabitatServiceException("Error: " + result.getStatus() + " " + result.getStatusText());
	}

	@Override
	public int getPermissionIdFrom(String permission) throws HabitatServiceException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Set<String> getUserPermissionsOn(String object, String username) throws HabitatServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean grantGroupPermissionOn(String object, String permission, String groupname) throws HabitatServiceException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean grantUserPermissionOn(String object, String permission, String username) throws HabitatServiceException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean revokeGroupPermissionOn(String object, String permission, String groupname) throws HabitatServiceException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean revokeUserPermissionOn(String object, String permission, String username) throws HabitatServiceException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasPermission(String object, int permId, String username) throws HabitatServiceException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasPermission(String object, String permission, String username) throws HabitatServiceException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getSessionToken() {
		return users.getSessionToken();
	}
	
	@Override
	public String getSessionUsername() {
		return users.getSessionUsername();
	}

	@Override
	public boolean isClaimedAsObjectId(String object) throws HabitatServiceException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void removeKey(String object) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getPermissionFromId(int permissionId) throws HabitatServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<String> getGroupPermissionsOn(String object, String username) throws HabitatServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addEntailedPermission(String myPermission, String entailedPermission) throws HabitatServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getMaxObjectNameLength() {
		throw new UnsupportedOperationException("Operation not supported.");
	}

	@Override
	public Set<String> getObjectIds(String permission, String username) {
		return null;
	}

	@Override
	public boolean existsForUse(String resourceName, String username) {
		// TODO Auto-generated method stub
		return false;
	}
}
