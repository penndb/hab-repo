/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.client;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import edu.upenn.cis.db.habitat.Config;
import edu.upenn.cis.db.habitat.core.api.UserApi;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.core.webservice.JsonUtil;

public class UserClient implements UserApi {
	final static Logger logger = LogManager.getLogger(UserClient.class);
	
	/**
	 * Registers a new user, without requiring a login session
	 * 
	 * @param username
	 * @param valueMap
	 * @return
	 * @throws UnirestException
	 */
	@SuppressWarnings("unchecked")
	public static Map<String,Object> createNewUser(
			String username, 
			String password, 
			Map<String,Object> valueMap) throws UnirestException {
		logger.info("Client creating new user " + username);
		valueMap.put("service", "local");
		valueMap.put("password", password);
		HttpResponse<String> result = 
				Unirest.post(Config.getServerUrl() + "/auth/credentials/{username}")
			.routeParam("username", username)
			.header("accept", "application/json")
			.header("Content-Type", "application/json")
			.body(new Gson().toJson(valueMap))
			.asString();
		
		if (result.getStatus() == 200) {
			return new Gson().fromJson(result.getBody(), Map.class);
		} else
			throw new UnirestException("Error: " + result.getStatus() + " " + result.getStatusText());
	}
	final String thisUser;
	
	
	String token = null;

	public UserClient(String username, String password) throws UnirestException {
		Map<String,String> object = new HashMap<>();
		object.put("password", password);
		
		thisUser = username;
		
		logger.info("Client connecting as " + username);
		HttpResponse<String> result = Unirest.post(Config.getServerUrl() + "/auth/tokens/{username}")
			.routeParam("username", username)
			.header("accept", "application/json")
			.header("Content-Type", "application/json")
			.body(new Gson().toJson(object))
			.asString();
		
		if (result.getStatus() == 200) {
			Map<String,Object> json = new Gson().fromJson(result.getBody(), Map.class);
			
			token = (String)json.get("token");
			logger.debug("Received token: " + result.toString());
		} else {
			throw new UnirestException(result.getStatus() + " " + result.getStatusText());
		}
	}

	@Override
	public boolean addCredential(String username, String service, String typeOrProtocol, String password)
			throws HabitatServiceException {
		logger.info("Adding credential for user " + username);
		Map<String,Object> valueMap = new HashMap<>();
		valueMap.put("service",service);
		valueMap.put("typeOrProtocol", typeOrProtocol);
		valueMap.put("password", password);
		HttpResponse<String> result;
		try {
			result =
					RequestUtils.createAuthenticatedPost("/auth/services/{service}/user/{username}", getSessionToken())
					.routeParam("service", service)
			.routeParam("username", username)
			.body(new Gson().toJson(valueMap))
			.asString();
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new HabitatServiceException(e.getMessage());
		}
		
		if (result.getStatus() == 200) {
			return true;
		} else
			throw new HabitatServiceException("Error: " + result.getStatus() + " " + result.getStatusText());
	}

	@Override
	public int addGroup(int orgId, String name, String parentGroup) throws HabitatServiceException {
		logger.info("Adding group " + name);
		Map<String,Object> valueMap = new HashMap<>();
		valueMap.put("parentGroup",parentGroup);
		HttpResponse<String> result;
		try {
			result =
					RequestUtils.createAuthenticatedPost("/auth/organizations/{orgId}/group/{groupname}", getSessionToken())
					.routeParam("orgId", Integer.toString(orgId))
					.routeParam("groupname", name)
			.body(new Gson().toJson(valueMap))
			.asString();
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new HabitatServiceException(e.getMessage());
		}
		
		if (result.getStatus() == 200) {
			return Integer.valueOf(result.getBody());
		} else
			throw new HabitatServiceException("Error: " + result.getStatus() + " " + result.getStatusText());
	}

	@Override
	public int addOrganization(String name, String street1, String street2, String city, String state, String zip,
			String country, String phone, String web) throws HabitatServiceException {
		logger.info("Adding organization " + name);
		Map<String,Object> valueMap = new HashMap<>();
		valueMap.put("street1",street1);
		valueMap.put("street2",street2);
		valueMap.put("city",city);
		valueMap.put("state",state);
		valueMap.put("zip",zip);
		valueMap.put("country",country);
		valueMap.put("phone",phone);
		valueMap.put("web",web);
		HttpResponse<String> result;
		try {
			result =
					RequestUtils.createAuthenticatedPost("/auth/organizations/{orgname}", getSessionToken())
					.routeParam("orgname", name)
			.body(new Gson().toJson(valueMap))
			.asString();
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new HabitatServiceException(e.getMessage());
		}
		
		if (result.getStatus() == 200) {
			return Integer.valueOf(result.getBody());
		} else
			throw new HabitatServiceException("Error: " + result.getStatus() + " " + result.getStatusText());
	}

	@Override
	public boolean addSubgroupToGroup(String subgroup, String group) throws HabitatServiceException {
		logger.info("Adding subgroup " + subgroup + " to group " + group);
		HttpResponse<String> result;
		try {
			result =
					RequestUtils.createAuthenticatedPost("/auth/groups/{groupname}/group/{subgroup}", getSessionToken())
					.routeParam("groupname", group)
					.routeParam("subgroup", subgroup)
			.asString();
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new HabitatServiceException(e.getMessage());
		}
		
		if (result.getStatus() == 200) {
			return true;
		} else
			throw new HabitatServiceException("Error: " + result.getStatus() + " " + result.getStatusText());
	}

	@Override
	public boolean addUserToGroup(String username, String group) throws HabitatServiceException {
		logger.info("Adding user " + username + " to group " + group);
		HttpResponse<String> result;
		try {
			result =
					RequestUtils.createAuthenticatedPost("/auth/groups/{groupname}/user/{username}", getSessionToken())
					.routeParam("groupname", group)
					.routeParam("username", username)
			.asString();
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new HabitatServiceException(e.getMessage());
		}
		
		if (result.getStatus() == 200) {
			return true;
		} else
			throw new HabitatServiceException("Error: " + result.getStatus() + " " + result.getStatusText());
	}

	@Override
	public boolean addUserToOrganization(String username, int orgid) throws HabitatServiceException {
		logger.info("Adding user " + username + " to organization " + orgid);
		HttpResponse<String> result;
		try {
			result =
					RequestUtils.createAuthenticatedPost("/auth/organizations/{orgId}/user/{username}", getSessionToken())
					.routeParam("orgId", String.valueOf(orgid))
					.routeParam("username", username)
			.asString();
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new HabitatServiceException(e.getMessage());
		}
		
		if (result.getStatus() == 200) {
			return true;
		} else
			throw new HabitatServiceException("Error: " + result.getStatus() + " " + result.getStatusText());
	}

	@Override
	public String getGroupFromId(Integer id) throws HabitatServiceException {
		logger.info("Getting name of group " + id);
		HttpResponse<String> result;
		try {
			result =
					RequestUtils.createAuthenticatedGet("/auth/groups/id/{groupId}", getSessionToken())
					.routeParam("groupId", String.valueOf(id))
			.asString();
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new HabitatServiceException(e.getMessage());
		}
		
		if (result.getStatus() == 200) {
			return result.getBody();
		} else
			throw new HabitatServiceException("Error: " + result.getStatus() + " " + result.getStatusText());
	}

	@Override
	public Integer getGroupIdFrom(String groupName) throws HabitatServiceException {
		logger.info("Getting ID of group " + groupName);
		HttpResponse<String> result;
		try {
			result =
					RequestUtils.createAuthenticatedGet("/auth/groups/name/{groupName}", getSessionToken())
					.routeParam("groupName", groupName)
			.asString();
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new HabitatServiceException(e.getMessage());
		}
		
		if (result.getStatus() == 200) {
			return Integer.valueOf(result.getBody());
		} else
			throw new HabitatServiceException("Error: " + result.getStatus() + " " + result.getStatusText());
	}

	@Override
	public Set<Integer> getGroupIdsForUser(String username) throws HabitatServiceException {
		logger.info("Getting group IDs for " + username);
		HttpResponse<String> result;
		try {
			result =
					RequestUtils.createAuthenticatedGet("/auth/credentials/{username}/groups/id", getSessionToken())
					.routeParam("username", String.valueOf(username))
			.asString();
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new HabitatServiceException(e.getMessage());
		}
		
		if (result.getStatus() == 200) {
			return (Set<Integer>) new Gson().fromJson(result.getBody(), HashSet.class);
		} else
			throw new HabitatServiceException("Error: " + result.getStatus() + " " + result.getStatusText());
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<String> getGroupsForUser(String username) throws HabitatServiceException {
		logger.info("Getting groups for " + username);
		HttpResponse<String> result;
		try {
			result =
					RequestUtils.createAuthenticatedGet("/auth/credentials/{username}/groups", getSessionToken())
					.routeParam("username", String.valueOf(username))
			.asString();
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new HabitatServiceException(e.getMessage());
		}
		
		if (result.getStatus() == 200) {
			return (Set<String>) new Gson().fromJson(result.getBody(), HashSet.class);
		} else
			throw new HabitatServiceException("Error: " + result.getStatus() + " " + result.getStatusText());
	}
	
	@Override
	public String getOrganizationFromId(Integer id) throws HabitatServiceException {
		logger.info("Getting name of organization " + id);
		HttpResponse<String> result;
		try {
			result =
					RequestUtils.createAuthenticatedGet("/auth/organizations/id/{orgId}", getSessionToken())
					.routeParam("orgId", String.valueOf(id))
			.asString();
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new HabitatServiceException(e.getMessage());
		}
		
		if (result.getStatus() == 200) {
			return result.getBody();
		} else
			throw new HabitatServiceException("Error: " + result.getStatus() + " " + result.getStatusText());
	}


	@Override
	public Integer getOrganizationIdFrom(String organization) throws HabitatServiceException {
		logger.info("Getting ID of organization " + organization);
		HttpResponse<String> result;
		try {
			result =
					RequestUtils.createAuthenticatedGet("/auth/organizations/name/{orgName}", getSessionToken())
					.routeParam("orgName", organization)
			.asString();
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new HabitatServiceException(e.getMessage());
		}
		
		if (result.getStatus() == 200) {
			return Integer.valueOf(result.getBody());
		} else
			throw new HabitatServiceException("Error: " + result.getStatus() + " " + result.getStatusText());
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<Integer> getParentGroupIds(Integer group) throws HabitatServiceException {
		logger.info("Getting parent group IDs for " + group);
		HttpResponse<String> result;
		try {
			result =
					RequestUtils.createAuthenticatedGet("/auth/groups/id/{groupId}/parents/id", getSessionToken())
					.routeParam("groupId", String.valueOf(group))
			.asString();
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new HabitatServiceException(e.getMessage());
		}
		
		if (result.getStatus() == 200) {
			return (Set<Integer>) new Gson().fromJson(result.getBody(), HashSet.class);
		} else
			throw new HabitatServiceException("Error: " + result.getStatus() + " " + result.getStatusText());
	}

	@Override
	public Set<String> getParentGroups(String group) throws HabitatServiceException {
		logger.info("Getting parent groups for " + group);
		HttpResponse<String> result;
		try {
			result =
					RequestUtils.createAuthenticatedGet("/auth/groups/name/{groupName}/parents/name", getSessionToken())
					.routeParam("groupName", String.valueOf(group))
			.asString();
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new HabitatServiceException(e.getMessage());
		}
		
		if (result.getStatus() == 200) {
			return (Set<String>) new Gson().fromJson(result.getBody(), HashSet.class);
		} else
			throw new HabitatServiceException("Error: " + result.getStatus() + " " + result.getStatusText());
	}

	@Override
	public String getSessionToken() {
		return token;
	}

	@Override
	public String getSessionUsername() {
		return thisUser;
	}

	
	@Override
	public String getUserFromId(Integer id) throws HabitatServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getUserIdFrom(String username) throws HabitatServiceException {
		Map<String, Object> userInfo = getUserInfo(username);
		return Integer.valueOf(userInfo.get("userid").toString());
	}

	@Override
	public Map<String, Object> getUserInfo(String username) throws HabitatServiceException {
		logger.debug("Adding credential for user " + username);
		
		HttpResponse<String> result;
		try {
			result =
					RequestUtils.createAuthenticatedGet("/auth/credentials/{username}/info", getSessionToken())
			.routeParam("username", username)
			.asString();
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new HabitatServiceException(e.getMessage());
		}
		
		if (result.getStatus() == 200) {
			return new Gson().fromJson(result.getBody().toString(), Map.class);
		} else
			throw new HabitatServiceException("Error: " + result.getStatus() + " " + result.getStatusText());
	}

	@Override
	public boolean isUserCredentialValid(String username, String password) throws HabitatServiceException {
		logger.info("Testing for user " + username);
		Map<String,Object> valueMap = new HashMap<>();
		valueMap.put("password", "password");
		HttpResponse<String> result;
		try {
			result =
					RequestUtils.createAuthenticatedPost("/auth/services/local/user/{username}/credential", getSessionToken())
			.routeParam("username", username)
			.body(JsonUtil.toJson(valueMap))
			.asString();
		} catch (UnirestException e) {
			e.printStackTrace();
			throw new HabitatServiceException(e.getMessage());
		}
		
		if (result.getStatus() == 200) {
			return result.getBody().equals("true");
		} else
			throw new HabitatServiceException("Error: " + result.getStatus() + " " + result.getStatusText());
	}

	@Override
	public boolean isUserInGroup(String username, int groupId) throws HabitatServiceException {
		return false;
	}

	@Override
	public boolean isUserInGroup(String username, String group) throws HabitatServiceException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isUserInOrganization(String username, int orgId) throws HabitatServiceException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isUserValid(String username, String service) throws HabitatServiceException {
		logger.info("Testing for user " + username);
		Map<String,Object> valueMap = new HashMap<>();
		HttpResponse<String> result;
		try {
			result =
					RequestUtils.createAuthenticatedGet("/auth/services/{service}/user/{username}", getSessionToken())
			.routeParam("username", username)
			.routeParam("service", service)
			.asString();
		} catch (UnirestException e) {
			e.printStackTrace();
			throw new HabitatServiceException(e.getMessage());
		}
		
		if (result.getStatus() == 200) {
			return result.getBody().equals("true");
		} else
			throw new HabitatServiceException("Error: " + result.getStatus() + " " + result.getStatusText());
	}

	@Override
	public boolean updateCredential(String username, String service, String typeOrProtocol, String oldToken,
			String token) throws HabitatServiceException {
		Map<String,Object> valueMap = new HashMap<>();
		
		valueMap.put("username", username);
		valueMap.put("oldPassword", oldToken);
		valueMap.put("newPassword", token);
		
		logger.info("Client updating credentials for " + username + " in service " + service);
		HttpResponse<JsonNode> result;
		try {
			result = 
					RequestUtils.createAuthenticatedPut("/auth/credentials/{username}/{service}", getSessionToken())
				.routeParam("username", username)
				.routeParam("service", service)
				.body(new Gson().toJson(valueMap))
				.asJson();
		} catch (UnirestException e) {
			logger.error(e.getMessage());
			return false;
		}
		
		if (result.getStatus() == 200) {
			return true;
		} else
			return false;
	}

	@Override
	public boolean updateUser(String username, String first, String last, String street1, String street2, String city,
			String state, String zip, String country, String phone, String primaryEmail, String title,
			String organization) {
		
		Map<String,Object> valueMap = new HashMap<>();
		
		valueMap.put("username", username);
		valueMap.put("firstname", first);
		valueMap.put("lastname", last);
		valueMap.put("email", primaryEmail);
		valueMap.put("street1", street1);
		valueMap.put("street2", street2);
		valueMap.put("city", city);
		valueMap.put("state", state);
		valueMap.put("zip", state);
		valueMap.put("country", state);
		valueMap.put("phone", state);
		valueMap.put("title", title);
		valueMap.put("organization", title);
		valueMap.put("title", title);
		
		logger.info("Client updating user " + username);
		HttpResponse<JsonNode> result;
		try {
			result = 
					RequestUtils.createAuthenticatedPut("/auth/credentials/{username}", getSessionToken())
				.routeParam("username", username)
				.body(new Gson().toJson(valueMap))
				.asJson();
		} catch (UnirestException e) {
			logger.error(e.getMessage());
			return false;
		}
		
		if (result.getStatus() == 200) {
			return true;
		} else
			return false;
	}

}
