
<a name="definitions"></a>
## Definitions

<a name="addcredential"></a>
### AddCredential
*Type* : object


<a name="addgroup"></a>
### AddGroup
*Type* : object


<a name="addorganization"></a>
### AddOrganization
*Type* : object


<a name="attribute"></a>
### Attribute
A PROV attribute


|Name|Schema|
|---|---|
|**name**  <br>*required*|[QualifiedName](#qualifiedname)|
|**type**  <br>*required*|enum (BOOLEAN, DOUBLE, INTEGER, STRING, LONG, QUALIFIED_NAME)|
|**value**  <br>*required*|object|


<a name="booleanattribute"></a>
### BooleanAttribute
*Polymorphism* : Inheritance  
*Discriminator* : type


|Name|Schema|
|---|---|
|**name**  <br>*required*|[QualifiedName](#qualifiedname)|
|**type**  <br>*required*|enum (BOOLEAN)|
|**value**  <br>*required*|boolean|


<a name="booleanfieldmodel"></a>
### BooleanFieldModel
*Polymorphism* : Inheritance  
*Discriminator* : type


|Name|Schema|
|---|---|
|**name**  <br>*required*|string|
|**type**  <br>*required*|enum (BOOLEAN)|
|**value**  <br>*required*|boolean|


<a name="createjsonwebtoken"></a>
### CreateJsonWebToken
*Type* : object


<a name="createnewuser"></a>
### CreateNewUser
*Type* : object


<a name="doubleattribute"></a>
### DoubleAttribute
*Polymorphism* : Inheritance  
*Discriminator* : type


|Name|Schema|
|---|---|
|**name**  <br>*required*|[QualifiedName](#qualifiedname)|
|**type**  <br>*required*|enum (DOUBLE)|
|**value**  <br>*required*|number (double)|


<a name="doublefieldmodel"></a>
### DoubleFieldModel
*Polymorphism* : Inheritance  
*Discriminator* : type


|Name|Schema|
|---|---|
|**name**  <br>*required*|string|
|**type**  <br>*required*|enum (DOUBLE)|
|**value**  <br>*required*|number (double)|


<a name="fieldmodel"></a>
### FieldModel
A field in a TupleModel


|Name|Schema|
|---|---|
|**name**  <br>*required*|string|
|**type**  <br>*required*|enum (BOOLEAN, DOUBLE, INTEGER, STRING, PROV_TOKEN, PROV_SPECIFIER, LONG, QUALIFIED_NAME, MULTI)|
|**value**  <br>*required*|object|


<a name="idmodel"></a>
### IDModel

|Name|Schema|
|---|---|
|**id**  <br>*optional*|string|


<a name="intattribute"></a>
### IntAttribute
*Polymorphism* : Inheritance  
*Discriminator* : type


|Name|Schema|
|---|---|
|**name**  <br>*required*|[QualifiedName](#qualifiedname)|
|**type**  <br>*required*|enum (INTEGER)|
|**value**  <br>*required*|integer (int32)|


<a name="integerfieldmodel"></a>
### IntegerFieldModel
*Polymorphism* : Inheritance  
*Discriminator* : type


|Name|Schema|
|---|---|
|**name**  <br>*required*|string|
|**type**  <br>*required*|enum (INTEGER)|
|**value**  <br>*required*|integer (int32)|


<a name="isvalidcredential"></a>
### IsValidCredential
*Type* : object


<a name="linkinfo"></a>
### LinkInfo

|Name|Schema|
|---|---|
|**sourceId**  <br>*optional*|string|
|**targetId**  <br>*optional*|string|
|**type**  <br>*optional*|string|


<a name="linkinstance"></a>
### LinkInstance

|Name|Schema|
|---|---|
|**sourceId**  <br>*optional*|string|
|**targetId**  <br>*optional*|string|
|**tuple**  <br>*optional*|[TupleWithSchemaModel](#tuplewithschemamodel)|
|**type**  <br>*optional*|string|


<a name="longattribute"></a>
### LongAttribute
*Polymorphism* : Inheritance  
*Discriminator* : type


|Name|Schema|
|---|---|
|**name**  <br>*required*|[QualifiedName](#qualifiedname)|
|**type**  <br>*required*|enum (LONG)|
|**value**  <br>*required*|integer (int64)|


<a name="longfieldmodel"></a>
### LongFieldModel
*Polymorphism* : Inheritance  
*Discriminator* : type


|Name|Schema|
|---|---|
|**name**  <br>*required*|string|
|**type**  <br>*required*|enum (LONG)|
|**value**  <br>*required*|integer (int64)|


<a name="multifieldmodel"></a>
### MultiFieldModel
*Polymorphism* : Inheritance  
*Discriminator* : type


|Name|Schema|
|---|---|
|**name**  <br>*required*|string|
|**type**  <br>*required*|enum (MULTI)|
|**value**  <br>*required*|< object > array|


<a name="nodeinfo"></a>
### NodeInfo

|Name|Schema|
|---|---|
|**id**  <br>*optional*|string|
|**optional**  <br>*optional*|boolean|
|**type**  <br>*optional*|string|
|**useSince**  <br>*optional*|boolean|


<a name="nodeinstance"></a>
### NodeInstance

|Name|Schema|
|---|---|
|**id**  <br>*optional*|string|
|**tuple**  <br>*optional*|[TupleWithSchemaModel](#tuplewithschemamodel)|


<a name="nodemodel"></a>
### NodeModel
an Entity, Activity, or Agent in the PROV model


|Name|Description|Schema|
|---|---|---|
|**attributes**  <br>*optional*||< [Attribute](#attribute) > array|
|**endTime**  <br>*optional*|it is an error to include end time unless type is ACTIVITY|string (date-time)|
|**location**  <br>*optional*||[ProvSpecifierModel](#provspecifiermodel)|
|**startTime**  <br>*optional*|it is an error to include start time unless type is ACTIVITY|string (date-time)|
|**type**  <br>*required*||enum (ENTITY, BUNDLE, COLLECTION, EMPTY_COLLECTION, PLAN, ACTIVITY, AGENT, ORGANIZATION, PERSON, SOFTWARE_AGENT)|


<a name="provedgemodel"></a>
### ProvEdgeModel

|Name|Schema|
|---|---|
|**endpointProvToken**  <br>*required*|string|
|**tupleWithSchema**  <br>*optional*|[TupleWithSchemaModel](#tuplewithschemamodel)|


<a name="provedgesetmodel"></a>
### ProvEdgeSetModel

|Name|Schema|
|---|---|
|**edges**  <br>*required*|< [ProvEdgeModel](#provedgemodel) > array|


<a name="provexpressionmodel"></a>
### ProvExpressionModel
*Polymorphism* : Composition


|Name|Schema|
|---|---|
|**operands**  <br>*optional*|< [ProvSpecifierModel](#provspecifiermodel) > array|
|**operation**  <br>*required*|string|


<a name="provlocationmodel"></a>
### ProvLocationModel
*Polymorphism* : Composition


|Name|Schema|
|---|---|
|**field**  <br>*required*|string|
|**position**  <br>*required*|< integer (int32) > array|
|**stream**  <br>*optional*|[IDModel](#idmodel)|


<a name="provnodemapmodel"></a>
### ProvNodeMapModel

|Name|Schema|
|---|---|
|**nodes**  <br>*required*|< string, [StoreNodeModel](#storenodemodel) > map|


<a name="provspecifierfieldmodel"></a>
### ProvSpecifierFieldModel
*Polymorphism* : Inheritance  
*Discriminator* : type


|Name|Schema|
|---|---|
|**name**  <br>*required*|string|
|**type**  <br>*required*|enum (PROV_SPECIFIER)|
|**value**  <br>*required*|[ProvSpecifierModel](#provspecifiermodel)|


<a name="provspecifiermodel"></a>
### ProvSpecifierModel
Either a ProvTokenModel, ProvLocationModel, or ProvExpressionModel

*Type* : object


<a name="provtokenfieldmodel"></a>
### ProvTokenFieldModel
*Polymorphism* : Inheritance  
*Discriminator* : type


|Name|Schema|
|---|---|
|**name**  <br>*required*|string|
|**type**  <br>*required*|enum (PROV_TOKEN)|
|**value**  <br>*required*|[ProvTokenModel](#provtokenmodel)|


<a name="provtokenmodel"></a>
### ProvTokenModel
*Polymorphism* : Composition


|Name|Schema|
|---|---|
|**tokenValue**  <br>*required*|string|


<a name="provtokensetmodel"></a>
### ProvTokenSetModel

|Name|Schema|
|---|---|
|**tokens**  <br>*required*|< [ProvTokenModel](#provtokenmodel) > array|


<a name="qualifiedname"></a>
### QualifiedName

|Name|Schema|
|---|---|
|**localPart**  <br>*optional*|string|
|**namespace**  <br>*optional*|string|


<a name="qualifiednameattribute"></a>
### QualifiedNameAttribute
*Polymorphism* : Inheritance  
*Discriminator* : type


|Name|Schema|
|---|---|
|**name**  <br>*required*|[QualifiedName](#qualifiedname)|
|**type**  <br>*required*|enum (QUALIFIED_NAME)|
|**value**  <br>*required*|[QualifiedName](#qualifiedname)|


<a name="qualifiednamefieldmodel"></a>
### QualifiedNameFieldModel
*Polymorphism* : Inheritance  
*Discriminator* : type


|Name|Schema|
|---|---|
|**name**  <br>*required*|string|
|**type**  <br>*required*|enum (QUALIFIED_NAME)|
|**value**  <br>*required*|[QualifiedName](#qualifiedname)|


<a name="rankinstance"></a>
### RankInstance

|Name|Schema|
|---|---|
|**nodes**  <br>*optional*|< [NodeInstance](#nodeinstance) > array|


<a name="relationmodel"></a>
### RelationModel
a relation


|Name|Description|Schema|
|---|---|---|
|**attributes**  <br>*optional*|It is an error to include attributes in a MEMEBERSHIP, ALTERNATE, or SPECIALIZATION relation|< [Attribute](#attribute) > array|
|**generationId**  <br>*optional*|It is an error to include a generation id in a relation other than DERIVATION|[QualifiedName](#qualifiedname)|
|**objectId**  <br>*required*||[QualifiedName](#qualifiedname)|
|**relationId**  <br>*optional*|It is an error to include attributes in a MEMEBERSHIP, ALTERNATE, or SPECIALIZATION relation|[QualifiedName](#qualifiedname)|
|**secondaryObjectId**  <br>*optional*|It is an error to include a secondary object in a relation other than ASSOCIATION, DERIVATION, START, END, or DELEGATION|[QualifiedName](#qualifiedname)|
|**subjectId**  <br>*required*||[QualifiedName](#qualifiedname)|
|**time**  <br>*optional*|It is an error to include time in a relation other than GENERATION, USAGE, START, END, or INVALIDATION|string (date-time)|
|**type**  <br>*required*||enum (GENERATION, USAGE, COMMUNICATION, START, END, INVALIDATION, DERIVATION, ATTRIBUTION, ASSOCIATION, DELEGATION, INFLUENCE, SPECIALIZATION, ALTERNATE, ANNOTATED, MEMBERSHIP)|
|**usageId**  <br>*optional*|It is an error to include a usage id in a relation other than DERIVATION|[QualifiedName](#qualifiedname)|


<a name="responseerror"></a>
### ResponseError

|Name|Schema|
|---|---|
|**message**  <br>*optional*|string|


<a name="storelinkmodel"></a>
### StoreLinkModel

|Name|Schema|
|---|---|
|**from**  <br>*required*|< [ProvTokenModel](#provtokenmodel) > array|
|**label**  <br>*optional*|string|
|**to**  <br>*required*|[ProvTokenModel](#provtokenmodel)|
|**tupleWithSchema**  <br>*optional*|[TupleWithSchemaModel](#tuplewithschemamodel)|


<a name="storenodemodel"></a>
### StoreNodeModel

|Name|Schema|
|---|---|
|**provSpecifier**  <br>*required*|[ProvSpecifierModel](#provspecifiermodel)|
|**tupleWithSchema**  <br>*optional*|[TupleWithSchemaModel](#tuplewithschemamodel)|


<a name="stringattribute"></a>
### StringAttribute
*Polymorphism* : Inheritance  
*Discriminator* : type


|Name|Schema|
|---|---|
|**name**  <br>*required*|[QualifiedName](#qualifiedname)|
|**type**  <br>*required*|enum (STRING)|
|**value**  <br>*required*|string|


<a name="stringfieldmodel"></a>
### StringFieldModel
*Polymorphism* : Inheritance  
*Discriminator* : type


|Name|Schema|
|---|---|
|**name**  <br>*required*|string|
|**type**  <br>*required*|enum (STRING)|
|**value**  <br>*required*|string|


<a name="subgraphinstance"></a>
### SubgraphInstance

|Name|Schema|
|---|---|
|**links**  <br>*optional*|< [LinkInstance](#linkinstance) > array|
|**maxTimestamp**  <br>*optional*|integer (int64)|
|**ranks**  <br>*optional*|< [RankInstance](#rankinstance) > array|


<a name="subgraphtemplate"></a>
### SubgraphTemplate

|Name|Schema|
|---|---|
|**links**  <br>*optional*|< [LinkInfo](#linkinfo) > array|
|**orderBy**  <br>*optional*|< string > array|
|**ranks**  <br>*optional*|< < [NodeInfo](#nodeinfo) > array > array|


<a name="tuplewithschemamodel"></a>
### TupleWithSchemaModel
a tuple


|Name|Schema|
|---|---|
|**lookupKeys**  <br>*required*|< string > array|
|**schemaName**  <br>*required*|string|
|**tuple**  <br>*required*|< [FieldModel](#fieldmodel) > array|


<a name="updateuser"></a>
### UpdateUser
*Type* : object


<a name="userinfo"></a>
### UserInfo

|Name|Schema|
|---|---|
|**city**  <br>*optional*|string|
|**country**  <br>*optional*|string|
|**email**  <br>*optional*|string|
|**firstname**  <br>*optional*|string|
|**lastname**  <br>*optional*|string|
|**organization**  <br>*optional*|string|
|**password**  <br>*optional*|string|
|**phone**  <br>*optional*|string|
|**state**  <br>*optional*|string|
|**street1**  <br>*optional*|string|
|**street2**  <br>*optional*|string|
|**title**  <br>*optional*|string|
|**username**  <br>*optional*|string|
|**zip**  <br>*optional*|string|


<a name="webtoken"></a>
### WebToken

|Name|Schema|
|---|---|
|**token**  <br>*optional*|string|



