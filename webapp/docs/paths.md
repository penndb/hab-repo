
<a name="paths"></a>
## Paths

<a name="createnewuser"></a>
### Creates a new user
```
POST /auth/credentials/{username}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**username**  <br>*required*|string|
|**Body**|**userfields**  <br>*required*|[CreateNewUser](#createnewuser)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|[UserInfo](#userinfo)|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* authentication


#### Example HTTP request

##### Request body
```
json :
{
  "application/json" : "{email:'my@mail',password='password',firstname='Bob',lastname='Smith',organization='Penn'}"
}
```


<a name="updateuser"></a>
### Updates user properties
```
PUT /auth/credentials/{username}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**username**  <br>*required*|string|
|**Body**|**userfields**  <br>*required*|[UpdateUser](#updateuser)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|[UserInfo](#userinfo)|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* authentication


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


#### Example HTTP request

##### Request body
```
json :
{
  "application/json" : "{email:'my@mail',password='password',firstname='Bob',lastname='Smith',organization='Penn'}"
}
```


<a name="getgroupsforuser"></a>
### Gets the groups in which a user is directly a member
```
GET /auth/credentials/{username}/groups
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**username**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|object|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* authentication


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="getgroupidsforuser"></a>
### Gets the IDs of groups in which a user is directly a member
```
GET /auth/credentials/{username}/groups/id
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**username**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|object|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* authentication


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="getuserinfo"></a>
### Gets a user's info
```
GET /auth/credentials/{username}/info
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**username**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|[UserInfo](#userinfo)|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* authentication


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="getgroupfromid"></a>
### Gets a group name from its integer ID
```
GET /auth/groups/id/{groupId}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**groupId**  <br>*required*|integer|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|object|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* authentication


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="getparentgroupids"></a>
### Gets the IDs of parent groups
```
GET /auth/groups/id/{groupId}/parents/id
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**groupId**  <br>*required*|integer|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|object|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* authentication


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="getgroupid"></a>
### Gets a group's ID from its name
```
GET /auth/groups/name/{groupName}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**groupName**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|object|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* authentication


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="getparentgroups"></a>
### Gets the groups in which a user is directly a member
```
GET /auth/groups/name/{groupName}/parents/name
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**groupName**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|object|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* authentication


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="addsubgrouptogroup"></a>
### Adds a subgroup to a group
```
POST /auth/groups/{groupname}/group/{subgroup}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**groupname**  <br>*required*|string|
|**Path**|**subgroup**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|object|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* authentication


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="addusertogroup"></a>
### Adds a user to a group
```
POST /auth/groups/{groupname}/user/{username}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**groupname**  <br>*required*|string|
|**Path**|**username**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|object|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* authentication


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="getorganizationfromid"></a>
### Gets an organization name from its integer ID
```
GET /auth/organizations/id/{orgId}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**orgId**  <br>*required*|integer|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|object|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* authentication


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="getorganizationid"></a>
### Gets an organization ID from its name
```
GET /auth/organizations/name/{orgName}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**orgName**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|object|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* authentication


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="addusertoorganization"></a>
### Adds a user to an organization
```
POST /auth/organizations/{orgId}/user/{username}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**orgId**  <br>*required*|integer|
|**Path**|**username**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|object|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* authentication


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="addgroup"></a>
### Adds a permissions group to an organization
```
POST /auth/organizations/{organization}/group/{group}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**group**  <br>*required*|string|
|**Path**|**organization**  <br>*required*|integer|
|**Body**|**details**  <br>*required*|[AddGroup](#addgroup)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|object|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* authentication


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


#### Example HTTP request

##### Request body
```
json :
{
  "application/json" : "{parentGroupId:1}"
}
```


<a name="addorganization"></a>
### Adds an organization
```
POST /auth/organizations/{orgname}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**orgname**  <br>*required*|string|
|**Body**|**details**  <br>*required*|[AddOrganization](#addorganization)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|object|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* authentication


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


#### Example HTTP request

##### Request body
```
json :
{
  "application/json" : "{street1:'s1',street2:'s2',city:'c',state:'st',zip:'zip',country:'c',phone:'p',web:'http://'}"
}
```


<a name="isvalidcredential"></a>
### Returns whether a local user credential is valid
```
POST /auth/services/local/user/{username}/credential
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**username**  <br>*required*|string|
|**Body**|**credentials**  <br>*required*|[IsValidCredential](#isvalidcredential)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|object|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* authentication


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="addcredential"></a>
### Adds a service credential for the user
```
POST /auth/services/{service}/user/{username}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**service**  <br>*required*|string|
|**Path**|**username**  <br>*required*|string|
|**Body**|**credentials**  <br>*required*|[AddCredential](#addcredential)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|[UserInfo](#userinfo)|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* authentication


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


#### Example HTTP request

##### Request body
```
json :
{
  "application/json" : "{service:'local',password='password'}"
}
```


<a name="isregistered"></a>
### Returns whether a user credential is valid for a service
```
GET /auth/services/{service}/user/{username}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**service**  <br>*required*|string|
|**Path**|**username**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|object|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* authentication


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="gettokenroute"></a>
### Requests a new token
```
POST /auth/tokens/{username}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**username**  <br>*required*|string|
|**Body**|**credentials**  <br>*required*|[CreateJsonWebToken](#createjsonwebtoken)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|[WebToken](#webtoken)|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* authentication


<a name="getpermissionfromid"></a>
### Gets a permission's name from its integer ID
```
GET /perms/id/{id}/name
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**id**  <br>*required*|integer|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|object|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* permission


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="getpermissionid"></a>
### Gets a permission's ID from its name
```
GET /perms/name/{name}/id
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**name**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|object|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* permission


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="getobjectids"></a>
### Gets the object ids for which the user has the given permission
```
GET /perms/name/{permname}/user/{username}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**permname**  <br>*required*|string|
|**Path**|**username**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|object|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* permission


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="getgrouppermissionson"></a>
### Gets the group's permissions on an object
```
GET /perms/objects/{object}/group/{groupName}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**groupName**  <br>*required*|string|
|**Path**|**object**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|object|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* permission


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="grantgrouppermissionon"></a>
### Grants a group a permission on an object
```
POST /perms/objects/{object}/group/{groupName}/{permname}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**groupName**  <br>*required*|string|
|**Path**|**object**  <br>*required*|string|
|**Path**|**permname**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|object|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* permission


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="revokegrouppermissionon"></a>
### Revokes a group a permission on an object
```
DELETE /perms/objects/{object}/group/{groupName}/{permname}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**groupName**  <br>*required*|string|
|**Path**|**object**  <br>*required*|string|
|**Path**|**permname**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|object|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* permission


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="getuserpermissionson"></a>
### Gets the user's permissions on an object
```
GET /perms/objects/{object}/user/{username}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**object**  <br>*required*|string|
|**Path**|**username**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|object|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* permission


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="grantuserpermissionon"></a>
### Grants a user a permission on an object
```
POST /perms/objects/{object}/user/{username}/{permname}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**object**  <br>*required*|string|
|**Path**|**permname**  <br>*required*|string|
|**Path**|**username**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|object|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* permission


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="revokeuserpermissionon"></a>
### Revokes a user a permission on an object
```
DELETE /perms/objects/{object}/user/{username}/{permname}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**object**  <br>*required*|string|
|**Path**|**permname**  <br>*required*|string|
|**Path**|**username**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|object|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* permission


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="addpermission"></a>
### Adds a new labeled permission type
```
POST /perms/types/{permname}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**permname**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|object|
|**400**|Invalid input data|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Produces

* `application/json`


#### Tags

* permission


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="storerelation"></a>
### Store a relation between PROV DM tokens
```
POST /provdm/graphs/{resource}/links/{label}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**label**  <br>*required*|string|
|**Path**|**resource**  <br>*required*|string|
|**Body**|**body**  <br>*required*|[RelationModel](#relationmodel)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**204**|Link created|No Content|
|**400**|Bad request|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Tags

* provDm


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="storenode"></a>
### Store a PROV DM node
```
PUT /provdm/graphs/{resource}/nodes/{token}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**resource**  <br>*required*|string|
|**Path**|**token**  <br>*required*|string|
|**Body**|**body**  <br>*required*|[NodeModel](#nodemodel)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**204**|Node created|No Content|
|**400**|Bad request|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Tags

* provDm


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="createorresetprovenancegraph"></a>
### Create a provenance graph if it doesn't exist, or overwrite it if it does
```
PUT /provenance/graphs/reset/{resource}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**resource**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**204**|Graph created or replaced|No Content|
|**400**|Bad request|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Tags

* provenance


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="createprovenancegraph"></a>
### Create a provenance graph
```
PUT /provenance/graphs/{resource}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**resource**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**204**|Graph created|No Content|
|**400**|Bad request|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Tags

* provenance


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="storeprovenancelink"></a>
### Store a provenance link between tokens
```
POST /provenance/graphs/{resource}/links
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**resource**  <br>*required*|string|
|**Body**|**body**  <br>*required*|[StoreLinkModel](#storelinkmodel)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**204**|Link created|No Content|
|**400**|Bad request|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Tags

* provenance


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="getprovenancenodes"></a>
### Get the provenance graph's nodes
```
GET /provenance/graphs/{resource}/nodes
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**resource**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|nodes retrieved|[ProvNodeMapModel](#provnodemapmodel)|
|**400**|Bad request|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|
|**404**|Node not found|[ResponseError](#responseerror)|


#### Tags

* provenance


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="getprovenancedata"></a>
### Get the tuple associated with a provenance token
```
GET /provenance/graphs/{resource}/nodes/{token}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**resource**  <br>*required*|string|
|**Path**|**token**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Node retrieved|[TupleWithSchemaModel](#tuplewithschemamodel)|
|**400**|Bad request|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|
|**404**|Node not found|[ResponseError](#responseerror)|


#### Tags

* provenance


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="storeprovenancenode"></a>
### Store a provenance token with its location
```
PUT /provenance/graphs/{resource}/nodes/{token}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**resource**  <br>*required*|string|
|**Path**|**token**  <br>*required*|string|
|**Body**|**body**  <br>*required*|[StoreNodeModel](#storenodemodel)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**204**|Node created|No Content|
|**400**|Bad request|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|


#### Tags

* provenance


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="getedgesto"></a>
### Get the incoming edges of the given prov token
```
GET /provenance/graphs/{resource}/nodes/{token}/links/in
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**resource**  <br>*required*|string|
|**Path**|**token**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|incoming links retrieved|[ProvEdgeSetModel](#provedgesetmodel)|
|**400**|Bad request|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|
|**404**|Node not found|[ResponseError](#responseerror)|


#### Tags

* provenance


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="getedgesfrom"></a>
### Get the outgoing edges of the given prov token
```
GET /provenance/graphs/{resource}/nodes/{token}/links/out
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**resource**  <br>*required*|string|
|**Path**|**token**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|outgoing links retrieved|[ProvEdgeSetModel](#provedgesetmodel)|
|**400**|Bad request|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|
|**404**|Node not found|[ResponseError](#responseerror)|


#### Tags

* provenance


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="getprovenancelocation"></a>
### Get the location of a provenance token
```
GET /provenance/graphs/{resource}/nodes/{token}/location
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**resource**  <br>*required*|string|
|**Path**|**token**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Specifier retrieved|[ProvSpecifierModel](#provspecifiermodel)|
|**400**|Bad request|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|
|**404**|Node not found|[ResponseError](#responseerror)|


#### Tags

* provenance


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="getconnectedto"></a>
### Get the incoming neighbors of the given prov token
```
GET /provenance/graphs/{resource}/nodes/{token}/neighbors/in
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**resource**  <br>*required*|string|
|**Path**|**token**  <br>*required*|string|
|**Query**|**label**  <br>*optional*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|incoming neighbors retrieved|[ProvTokenSetModel](#provtokensetmodel)|
|**400**|Bad request|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|
|**404**|Node not found|[ResponseError](#responseerror)|


#### Tags

* provenance


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="getconnectedfrom"></a>
### Get the outgoing neighbors of the given prov token
```
GET /provenance/graphs/{resource}/nodes/{token}/neighbors/out
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**resource**  <br>*required*|string|
|**Path**|**token**  <br>*required*|string|
|**Query**|**label**  <br>*optional*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|outgoing neighbors retrieved|[ProvTokenSetModel](#provtokensetmodel)|
|**400**|Bad request|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|
|**404**|Node not found|[ResponseError](#responseerror)|


#### Tags

* provenance


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="getsubgraphs"></a>
### Get a provenance graph as a sequence of subgraphs
```
POST /provenance/graphs/{resource}/subgraphs
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**resource**  <br>*required*|string|
|**Query**|**limit**  <br>*required*|integer (int32)|
|**Query**|**since**  <br>*optional*|integer (int64)|
|**Body**|**body**  <br>*required*|[SubgraphTemplate](#subgraphtemplate)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|subgraphs retrieved|< [SubgraphInstance](#subgraphinstance) > array|
|**400**|Bad request|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|
|**404**|Node not found|[ResponseError](#responseerror)|


#### Tags

* provenance


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="storesubgraph"></a>
### Store a subgraph to a provenance graph
```
POST /provenance/graphs/{resource}/subgraphs/store
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**resource**  <br>*required*|string|
|**Body**|**body**  <br>*required*|[SubgraphInstance](#subgraphinstance)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|subgraph stored|No Content|
|**400**|Bad request|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|
|**404**|Node not found|[ResponseError](#responseerror)|


#### Tags

* provenance


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="storesubgraphtemplate"></a>
### Store a subgraph template for a provenance graph
```
POST /provenance/graphs/{resource}/subgraphs/template
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**resource**  <br>*required*|string|
|**Body**|**body**  <br>*required*|[SubgraphTemplate](#subgraphtemplate)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|subgraph template stored|No Content|
|**400**|Bad request|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|
|**404**|Node not found|[ResponseError](#responseerror)|


#### Tags

* provenance


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|


<a name="getsubgraphtemplate"></a>
### Get the subgraph template of a provenance graph
```
GET /provenance/graphs/{resource}/subgraphs/template
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**resource**  <br>*required*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|subgraph template retrieved|[SubgraphTemplate](#subgraphtemplate)|
|**400**|Bad request|[ResponseError](#responseerror)|
|**401**|Unauthorized|[ResponseError](#responseerror)|
|**404**|Node not found|[ResponseError](#responseerror)|


#### Tags

* provenance


#### Security

|Type|Name|
|---|---|
|**Unknown**|**[jwt](#jwt)**|



