# Swagger spec for Habitat REST API


<a name="overview"></a>
## Overview
REST API for Habitat Hypothesis Base


### Version information
*Version* : v1


### Contact information
*Contact* : Zachary Ives  
*Contact Email* : zives@cis.upenn.edu


### License information
*License* : Apache 2.0 License  
*License URL* : https://www.apache.org/licenses/LICENSE-2.0  
*Terms of service* : http://www.db.cis.upenn.edu


### URI scheme
*Host* : localhost:8080  
*BasePath* : /v1  
*Schemes* : HTTP, HTTPS


### Tags

* authentication
* permission
* provDm
* provenance



