/*
 * Copyright 2018 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pac4j.core.context.Cookie;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.core.profile.definition.CommonProfileDefinition;
import org.pac4j.jwt.config.signature.SecretSignatureConfiguration;
import org.pac4j.jwt.profile.JwtGenerator;
import org.pac4j.sparkjava.SecurityFilter;
import org.pac4j.sparkjava.SparkWebContext;

import spark.Filter;
import spark.Request;
import spark.Response;
import spark.Service;
import spark.staticfiles.StaticFilesConfiguration;

import com.google.common.collect.ImmutableMap;
import com.google.common.net.UrlEscapers;

import edu.upenn.cis.db.habitat.Config;

/**
 * @author John Frommeyer
 *
 */
public class SinglePageAppsConfiguration {

	public static final class SinglePageAppFilter implements Filter {
		private final Logger logger = LogManager.getLogger(getClass());
		private final String staticPathSlash;
		private final String staticPath;
		private final String externalLocation;
		private final SecurityFilter baseFilter;
		private final org.pac4j.core.config.Config pac4jConfig;
		private final StaticFilesConfiguration staticHandler;

		public SinglePageAppFilter(StaticFilesConfiguration staticHandler,
				org.pac4j.core.config.Config pac4jConfig,
				String externalLocation,
				SecurityFilter baseFilter,
				String staticPath) {
			this.staticHandler = checkNotNull(staticHandler);
			this.pac4jConfig = checkNotNull(pac4jConfig);
			this.externalLocation = checkNotNull(externalLocation);
			this.baseFilter = checkNotNull(baseFilter);
			this.staticPath = checkNotNull(staticPath);
			this.staticPathSlash = staticPath + "/";
		}

		@Override
		public void handle(Request request, Response response) throws Exception {
			final String m = "handle(...)";
			final String requestPath = request.pathInfo();
			logger.info("{}: static request: {}", m, requestPath);
			if (requestPath.equals(staticPathSlash)) {
				response.redirect(staticPath, 301);
			} else {
				final Path localPath = Paths.get(externalLocation,
						requestPath);
				if (!Files.exists(localPath)) {
					// If someone types in a path in our SPA we need to
					// redirect to base path so that javascript can load the
					// app. An URL rewrite would be better since with the
					// redirect we lose the intended destination. That would
					// require customizing the embedded Jetty server.
					response.redirect(staticPath, 301);
				} else {
					baseFilter.handle(request, response);
					// If we are here, then we've been authenticated.
					final SparkWebContext context = new SparkWebContext(
							request, response, pac4jConfig.getSessionStore());
					final ProfileManager<CommonProfile> manager = new ProfileManager<>(
							context);
					if (manager.isAuthenticated()) {
						Optional<CommonProfile> optProfile = manager
								.get(true);
						if (!optProfile.isPresent()) {
							logger.warn(
									"{}: Authenticated request to {} contains no profile. No api_key will be sent.",
									m, requestPath);
						} else {
							final CommonProfile profile = optProfile.get();
							final CommonProfile apiKeyProfile = createApiKeyProfile(profile);
							final Cookie usernameCookie = createCookie(
									"username",
									UrlEscapers
											.urlPathSegmentEscaper()
											.escape(apiKeyProfile.getId()),
									staticPath);
							final JwtGenerator<CommonProfile> jwtGenerator =
									new JwtGenerator<>(
											new SecretSignatureConfiguration(
													Config.getSalt()));
							final String apiKey = jwtGenerator
									.generate(apiKeyProfile);
							final Cookie apiKeyCookie = createCookie(
									"api_key",
									apiKey,
									staticPath);
							context.addResponseCookie(usernameCookie);
							context.addResponseCookie(apiKeyCookie);
						}
						staticHandler.consume(request.raw(), response.raw());
					}

				}
			}

		}

		/**
		 * Converts profile to a basic CommonProfile for JWT ApiKey creation.
		 * (Otherwise, Pac4J causes a ClassCastException when trying to read a
		 * Google2Profile from JWT)
		 * 
		 * @param profile
		 * @return
		 */
		private CommonProfile createApiKeyProfile(CommonProfile profile) {

			final CommonProfile copy = new CommonProfile();
			copy.setClientName("HeaderClient");

			// In the most likely scenario, the incoming profile is a
			// Google2Profile with a display name matching the username in our
			// DB.
			// And the incoming profile's id will be a Google numeric ID which
			// we don't have in our database.
			// So we use the incoming display name as the id in the profile
			// we'll return.
			copy.setId(profile.getDisplayName());
			copy.addAttribute(
					CommonProfileDefinition.DISPLAY_NAME,
					profile.getDisplayName());
			copy.addAttribute(
					CommonProfileDefinition.EMAIL,
					profile.getEmail());
			copy.addAttribute(
					CommonProfileDefinition.FAMILY_NAME,
					profile.getFamilyName());
			copy.addAttribute(
					CommonProfileDefinition.FIRST_NAME,
					profile.getFirstName());
			copy.addAttribute(
					CommonProfileDefinition.GENDER,
					profile.getGender());
			copy.addAttribute(
					CommonProfileDefinition.LOCALE,
					profile.getLocale());
			copy.addAttribute(
					CommonProfileDefinition.LOCATION,
					profile.getLocation());
			copy.addAttribute(
					CommonProfileDefinition.PICTURE_URL,
					profile.getPictureUrl());
			copy.addAttribute(
					CommonProfileDefinition.PROFILE_URL,
					profile.getProfileUrl());

			copy.setLinkedId(profile.getLinkedId());
			copy.setRemembered(profile.isRemembered());
			copy.addPermissions(profile.getPermissions());
			copy.addRoles(profile.getRoles());
			return copy;
		}

		private Cookie createCookie(String name, String value, String path) {
			final Cookie usernameCookie = new Cookie(
					name, value);
			usernameCookie.setDomain("");
			usernameCookie.setPath(path);
			usernameCookie.setSecure(false);
			usernameCookie.setHttpOnly(false);
			usernameCookie.setMaxAge(100);
			return usernameCookie;
		}

	}

	private final Logger logger = LogManager.getLogger(getClass());
	// Preserve insertion order.
	private final ImmutableMap<String, Filter> appPathToFilter;

	public SinglePageAppsConfiguration(String externalLocation,
			SecurityFilter baseFilter,
			org.pac4j.core.config.Config pac4jConfig) {
		final String m = "SinglePageAppsConfiguration(...)";
		final ImmutableMap.Builder<String, Filter> mapBuilder = ImmutableMap
				.builder();
		StaticFilesConfiguration staticHandler = StaticFilesConfiguration
				.create();
		staticHandler.configureExternal(externalLocation);
		staticHandler.setExpireTimeSeconds(600L);
		File[] directories = new File(externalLocation)
				.listFiles(File::isDirectory);
		for (final File directory : directories) {
			final String staticPath = "/" + directory.getName();
			final String staticPathStar = staticPath + "/*";
			logger.info("{}: securing static path {}", m, staticPathStar);

			final Filter staticFilter = new SinglePageAppFilter(
					staticHandler,
					pac4jConfig,
					externalLocation,
					baseFilter,
					staticPath);
			mapBuilder.put(staticPath, staticFilter);
			mapBuilder.put(staticPathStar, staticFilter);
		}
		appPathToFilter = mapBuilder.build();
	}

	public void configureFilters(Service spark) {
		for (Map.Entry<String, Filter> pathFilter : appPathToFilter.entrySet()) {
			spark.before(pathFilter.getKey(), pathFilter.getValue());
		}
	}
}
