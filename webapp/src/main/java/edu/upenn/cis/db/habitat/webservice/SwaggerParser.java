/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.webservice;

import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.models.Swagger;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.db.habitat.Config;

/**
 * 
 * @author serol, https://serol.ro/posts/2016/swagger_sparkjava/
 *
 */
public class SwaggerParser {

	public static String getSwaggerJson(String packageName) throws JsonProcessingException {
		Swagger swagger = getSwagger(packageName);
		String json = swaggerToJson(swagger);
		return json;
	}

	public static Swagger getSwagger(String packageName) {
		BeanConfig beanConfig = new BeanConfig();
		beanConfig.setHost(Config.getServer());
		beanConfig.setResourcePackage(packageName);
		beanConfig.setScan(true);
		Swagger swagger = beanConfig.getSwagger();

		return swagger;
	}

	public static String swaggerToJson(Swagger swagger) throws JsonProcessingException {
		ObjectMapper objectMapper = io.swagger.util.Json.mapper();
		objectMapper.setSerializationInclusion(Include.NON_EMPTY);
		String json = objectMapper.writeValueAsString(swagger);
		return json;
	}

}