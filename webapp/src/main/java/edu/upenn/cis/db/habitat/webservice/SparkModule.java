/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice;

import static com.google.common.base.Preconditions.checkNotNull;
import spark.Service;

import com.google.inject.AbstractModule;

/**
 * Allows us to inject a partially configured Spark Service without relying on
 * Spark's static API.
 * 
 * @author John Frommeyer
 *
 */
public class SparkModule extends AbstractModule {

	private final Service spark;

	public SparkModule(Service spark) {
		this.spark = checkNotNull(spark);
	}

	@Override
	protected void configure() {
		bind(Service.class).toInstance(spark);
	}

}
