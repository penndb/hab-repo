/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice;

import static com.google.common.base.Preconditions.checkNotNull;
import static edu.upenn.cis.db.habitat.core.webservice.JsonUtil.json;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pac4j.core.authorization.generator.AuthorizationGenerator;
import org.pac4j.sparkjava.CallbackRoute;
import org.pac4j.sparkjava.LogoutRoute;
import org.pac4j.sparkjava.SecurityFilter;
import org.reflections.ReflectionsException;

import com.google.common.collect.Multimap;
import com.google.inject.Inject;
import com.google.inject.Key;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

import edu.upenn.cis.db.habitat.Config;
import edu.upenn.cis.db.habitat.auth.backends.plugins.HabitatRequestScopeModule;
import edu.upenn.cis.db.habitat.auth.backends.plugins.SimpleScope;
import edu.upenn.cis.db.habitat.auth.pac4j.HabitatJsonActionAdapter;
import edu.upenn.cis.db.habitat.auth.pac4j.SecurityConfigFactory;
import edu.upenn.cis.db.habitat.core.api.UserApi;
import edu.upenn.cis.db.habitat.core.webservice.RestPlugin;
import edu.upenn.cis.db.habitat.core.webservice.RouteSpecifier;
import edu.upenn.cis.db.habitat.util.SparkUserProfiles;
import io.swagger.annotations.ApiKeyAuthDefinition;
import io.swagger.annotations.ApiKeyAuthDefinition.ApiKeyLocation;
import io.swagger.annotations.Contact;
import io.swagger.annotations.Info;
import io.swagger.annotations.SecurityDefinition;
import io.swagger.annotations.SwaggerDefinition;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.Service;
import spark.TemplateEngine;

@SwaggerDefinition(info = @Info(description = "Habitat API", //
		version = "V1.1.2", //
		title = "Habitat repository and authorization API", //
		contact = @Contact(name = "Zack Ives", url = "https://bitbucket.org/penndb/hab-repo")), //
		schemes = { SwaggerDefinition.Scheme.HTTP, SwaggerDefinition.Scheme.HTTPS }, //
		consumes = { "application/json" }, //
		produces = { "application/json" },
		securityDefinition = @SecurityDefinition(
				apiKeyAuthDefinitions =
				@ApiKeyAuthDefinition(name = "api_key",
						key = "jwt",
						in = ApiKeyLocation.HEADER)
		))
public class RestService {
	final static Logger logger = LogManager.getLogger(RestService.class);
	public static final String APP_PACKAGE = "edu.upenn.cis.db.habitat.webservice";
	
	private final SecurityConfigFactory scf;
	private final TemplateEngine templateEngine;
	
	org.pac4j.core.config.Config config;

	Map<String, RestPlugin> plugins;
	UserApi backend;

	private static String requestInfoToString(Request request) {
	    StringBuilder sb = new StringBuilder();
	    sb.append(request.requestMethod());
	    sb.append(" " + request.url());
	    sb.append(" " + request.body());
	    return sb.toString();
	}
	
	final SecurityFilter filter;
	private final SecurityFilter noTemplateFilter;
	private final Service spark;
	private final SimpleScope requestScope;
	
	@Inject
	public RestService(Map<String, RestPlugin> plugins, 
			UserApi backend,
			TemplateEngine engine, 
			AuthorizationGenerator<?> generator, 
			Service spark,
			@Named(HabitatRequestScopeModule.HABITAT_REQUEST_SCOPE_NAME) SimpleScope requestScope) {
		this.spark = checkNotNull(spark);
		this.requestScope = checkNotNull(requestScope);
		this.backend = backend;
		this.plugins = plugins;
		
		scf = new SecurityConfigFactory(engine, generator, backend);
		this.templateEngine = engine;
		
		config = scf.build();
		final CallbackRoute callback = new CallbackRoute(config, null, true);
		filter = new SecurityFilter(config, "Google2Client,GitHubClient,HeaderClient");
		SecurityFilter gitOnly = new SecurityFilter(config, "GitHubClient");
		SecurityFilter googOnly = new SecurityFilter(config, "Google2Client");
		final org.pac4j.core.config.Config noTemplateConfig = scf.build(new HabitatJsonActionAdapter());
		noTemplateFilter = new SecurityFilter(noTemplateConfig, "HeaderClient");

		spark.staticFileLocation("/public");
		spark.staticFiles.expireTime(600L);
		
		final String externalLocation = Config.getExternalLocation();
		if (externalLocation != null) {
			final SinglePageAppsConfiguration singlePageApps = new SinglePageAppsConfiguration(
					externalLocation, 
					filter, 
					config);
			singlePageApps.configureFilters(spark);
		}
		
		spark.get("/callback", callback);
		spark.post("/callback", callback);
		spark.get("/", RestService::index, templateEngine);
		spark.get("/index.html", RestService::index, templateEngine);
		spark.get("/index.htm", RestService::index, templateEngine);
		spark.get("/index.php", RestService::index, templateEngine);


		if (System.getenv("noSwagger") == null)
			try {
				// Build swagger json description
				final String swaggerJson = SwaggerParser.getSwaggerJson(APP_PACKAGE);
				spark.get("/swagger", (req, res) -> {
					return swaggerJson;
				});
			} catch (ReflectionsException re) {
				System.err.println(re.getMessage());
			} catch (Exception e) {
				System.err.println(e);
				e.printStackTrace();
			}
		
		final LogoutRoute localLogout = new LogoutRoute(config, "/?defaulturlafterlogout");
		localLogout.setDestroySession(true);
		spark.get("/logout", localLogout);

	    spark.before((request, response) -> {
	        logger.info(requestInfoToString(request));
	    });
		spark.before("/web/github/*", gitOnly);
		spark.before("/web/google/*", googOnly);
//		Spark.before("/", filter);
//		Spark.before("/form", new SecurityFilter(config, "FormClient"));
	    spark.after((request, response) -> {
	        logger.info("End of " + requestInfoToString(request));
	    });

	}
	

	/**
	 * For each plugin, concatenate the base path and the 
	 */
	public void createRoutes(Map<String, RestPlugin> plugins) {
		Set<String> secureServices = new HashSet<>();
		Set<String> insecureServices = new HashSet<>();
		
		for (String subRoute: plugins.keySet()) {
			RestPlugin plugin = plugins.get(subRoute);

			Multimap<String, RouteSpecifier> specifiers = plugin.getSpecifiers();
			spark.path(subRoute, () -> {
				specifiers.entries().forEach(entry -> {
					final String path = entry.getKey();
					final RouteSpecifier specifier = entry.getValue();
					synchronized (this) {
					switch (specifier.getRequestType()) {
					case DELETE:
						if (specifier.isTemplateRoute())
							spark.delete(path, specifier.getTemplateRoute(), templateEngine);
						else
							spark.delete(path, specifier.getRoute(), json());
						logger.debug("Added DELETE service at " + subRoute + path);
						break;
					case GET:
						if (specifier.isTemplateRoute())
							spark.get(path, specifier.getTemplateRoute(), templateEngine);
						else
							spark.get(path, specifier.getRoute(), json());
						logger.debug("Added GET service at " + subRoute + path);
						break;
					case POST:
						if (specifier.isTemplateRoute())
							spark.post(path, specifier.getTemplateRoute(), templateEngine);
						else
							spark.post(path, specifier.getRoute(), json());
						logger.debug("Added POST service at " + subRoute + path);
						break;
					case PUT:
						if (specifier.isTemplateRoute())
							spark.put(path, specifier.getTemplateRoute(), templateEngine);
						else
							spark.put(path, specifier.getRoute(), json());
						logger.debug("Added PUT service at " + subRoute + path);
						break;
					}
					if (!specifier.isTemplateRoute())
						spark.after(path, (req, res) -> {
							res.type("application/json");
						});
					
					if (specifier.isSecure()) {
						logger.debug("Securing " + subRoute + path);
						secureServices.add(subRoute + path);
						
//						if (insecureServices.contains(subRoute + path))
//							throw new RuntimeException("Secure service " + subRoute + path + " will mask insecure one");
						
						spark.before(path, (Request request, Response response) -> {
							if (request.requestMethod().equals(specifier.getRequestType().toString()))
								if (specifier.isTemplateRoute()) {
									filter.handle(request, response);
								} else {
									noTemplateFilter.handle(request, response);
								}
						});
					} else {
						insecureServices.add(subRoute + path);
//						if (secureServices.contains(subRoute + path))
//							throw new RuntimeException("Insecure service " + subRoute + path + " will mask secure one");
					}
					}
				});
				
			});

		}
		spark.before((Request request, Response response) -> {
			final String username = request.attribute("username");
			logger.debug("Got user {} from {} at path {}", 
					username, 
					request.requestMethod(), 
					request.pathInfo());
			requestScope.enter();
			requestScope.seed(Key.get(String.class, Names.named("username")), username);
		});
		spark.afterAfter((Request request, Response response) -> {
			requestScope.exit();
		});
		
	}
	
	public void call(){
		createRoutes(plugins);
	}
	
	public void stop() {
		spark.stop();
	}
	
	public void awaitInitialization() {
		spark.awaitInitialization();
	}
	
	public int activeThreadCount() {
		return spark.activeThreadCount();
	}

	/**
	 * Unprotected index -- no session
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	private static ModelAndView index(final Request request, final Response response) {
		if (!SparkUserProfiles.getProfiles(request, response).isEmpty()) {
			return protectedIndex(request, response);
		} else {
			final Map<String,Object> map = new HashMap<>();
			map.put("profiles", SparkUserProfiles.getProfiles(request, response));
			request.session(true);
			map.put("sessionId", request.session().id());
			return new ModelAndView(map, "index.mustache");
		}
	}
	
	private static ModelAndView protectedIndex(final Request request, final Response response) {
		final Map<String,Object> map = new HashMap<>();
		map.put("profiles", SparkUserProfiles.getProfiles(request, response));
		request.session(true);
		map.put("sessionId", request.session().id());
		return new ModelAndView(map, "protectedIndex.mustache");
	}
}
