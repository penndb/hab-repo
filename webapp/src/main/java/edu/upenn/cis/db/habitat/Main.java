package edu.upenn.cis.db.habitat;

/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import spark.Service;

import com.google.inject.Guice;
import com.google.inject.Injector;

import edu.upenn.cis.db.habitat.auth.AuthBrowserModule;
import edu.upenn.cis.db.habitat.auth.AuthServiceModule;
import edu.upenn.cis.db.habitat.auth.backends.plugins.AuthModule;
import edu.upenn.cis.db.habitat.auth.backends.plugins.HabitatRequestScopeModule;
import edu.upenn.cis.db.habitat.auth.backends.plugins.PostgresAuthModule;
import edu.upenn.cis.db.habitat.auth.backends.plugins.ProtectedStorageModule;
import edu.upenn.cis.db.habitat.permissions.PermissionServiceModule;
import edu.upenn.cis.db.habitat.provenance.ProvServiceModule;
import edu.upenn.cis.db.habitat.provenance.provdm.ProvDmServiceModule;
import edu.upenn.cis.db.habitat.repository.storage.neo4j.ConfigurableNeoStorageModule;
import edu.upenn.cis.db.habitat.repository.storage.neo4j.Neo4JStore;
import edu.upenn.cis.db.habitat.repository.storage.postgresql.ConfigurablePostgresStorageModule;
import edu.upenn.cis.db.habitat.repository.storage.postgresql.PostgresStore;
import edu.upenn.cis.db.habitat.webservice.RestService;
import edu.upenn.cis.db.habitat.webservice.SparkModule;

/**
 * Mainline module: launch the main Web service interfaces and host environment
 * in the Spark framework. Pull in all plugins and register them, adding
 * appropriate authorization checks.
 * 
 * @author zives
 *
 */
public class Main {

	final static Logger logger = LogManager.getLogger(Main.class);

	public static void main(String[] args) throws IOException {
		if (args.length > 0) {
			final String configPath = args[0];
			Config.setConfigFromFileSystem(configPath);
		}
		final Service spark = Service.ignite();
		spark.port(Config.getServerPort());
		spark.threadPool(Config.getMaxThreads(), 2, Config.getTimeoutMsec());

		Injector injector = Guice.createInjector(
				new HabitatRequestScopeModule(),
				// Generic authorization
				new AuthModule(),
				// Web app authorization
				new AuthBrowserModule(),
				// Permissions
				new PermissionServiceModule(),
				// Authorization using Postgres backend
				new PostgresAuthModule(),
				// Provenance services
				new ProvServiceModule(),
				new ProvDmServiceModule(),
				// User auth services
				new AuthServiceModule(),
				// Storage services
				new ConfigurablePostgresStorageModule(),
				new ProtectedStorageModule(PostgresStore.class),
				new SparkModule(spark));
		

		System.out.println("Habitat Repo launching on port "
				+ Config.getServerPort() + "...");
		RestService web = injector.getInstance(RestService.class);
		web.call();
	}

}
