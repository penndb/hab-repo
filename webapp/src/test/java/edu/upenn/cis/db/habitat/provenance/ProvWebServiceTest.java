/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.provenance;

import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static java.lang.String.format;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.mashape.unirest.http.exceptions.UnirestException;

import edu.upenn.cis.db.habitat.Config;
import edu.upenn.cis.db.habitat.TestsRequiringSpark;
import edu.upenn.cis.db.habitat.client.ProvenanceClient;
import edu.upenn.cis.db.habitat.client.UserClient;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.Field;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.ProvStringToken;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.Schema;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate.LinkInfo;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate.NodeInfo;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvDmRelation;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvDmType;
import edu.upenn.cis.db.habitat.core.webservice.HabitatSecurityException;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.test.TestUtil;

/**
 * @author John Frommeyer
 *
 */
public class ProvWebServiceTest {

	private static final String username = "provWsTestUser";
	private static final String password = "provWsTestUserPassword";

	private static final String username2 = "provWsTestUser2";
	private static final String password2 = "provWsTestUserPassword2";

	@BeforeClass
	public static void requireSuite() throws Exception {

		Assume.assumeTrue(
				"These tests require the Spark server and must be run as part of the "
						+ TestsRequiringSpark.class
						+ " test suite",
				TestsRequiringSpark.serverIsRunning);

	}

	@Before
	public void createTables()
			throws HabitatServiceException, UnirestException {
		TestsRequiringSpark.cleanDatabase();
		TestsRequiringSpark.createTables();
		final Map<String, Object> valueMap1 = new HashMap<>();
		valueMap1.put("email", "provWsTestUser@mail");
		UserClient.createNewUser(
				username,
				password,
				valueMap1);

		final Map<String, Object> valueMap2 = new HashMap<>();
		valueMap2.put("email", "provWsTestUser2@mail");
		UserClient.createNewUser(
				username2,
				password2,
				valueMap2);
	}

	@Test
	public void storeProvenanceDataTest() throws HabitatServiceException {
		final String resource = "storeDataTest";
		final ProvenanceClient client = new ProvenanceClient(username,
				password, "localhost", Config.getServerPort());
		final ProvToken provToken = new ProvStringToken(
				"PROV_WS_TEST.STORE_PROV_DATA");
		final ProvLocation expectedLocation = new ProvLocation(
				"provWsTest.storeProvDataSource", newArrayList(3, 4));
		client.createProvenanceGraph(resource);
		client.storeProvenanceNode(resource, provToken, expectedLocation);

		final ProvSpecifier actualSpecifier = client
				.getProvenanceLocation(resource, provToken);
		assertTrue(actualSpecifier instanceof ProvLocation);
		final ProvLocation actualLocation = (ProvLocation) actualSpecifier;

		assertEquals(expectedLocation.getField(), actualLocation.getField());
		assertEquals(expectedLocation.getStream(),
				actualLocation.getStream());
		assertEquals(expectedLocation.getPosition(),
				actualLocation.getPosition());

		final TupleWithSchema<String> acutalData = client
				.getProvenanceData(resource, provToken);
		assertNull(acutalData);

	}

	@Test
	public void getProvenanceDataTest() throws IOException,
			HabitatServiceException {
		final String resource = "getDataTest";
		final BasicTuple expectedTuple = TestUtil.newTestTuple();
		final String tokenValue = RandomStringUtils.randomAlphanumeric(32);
		final ProvToken token = new ProvStringToken(tokenValue);
		final ProvLocation location = new ProvLocation(
				"ws.getProvenanceDataTestLocation", newArrayList(7, 8));

		final ProvenanceClient client = new ProvenanceClient(username,
				password, "localhost", Config.getServerPort());
		client.createProvenanceGraph(resource);
		client.storeProvenanceNode(resource, token, expectedTuple, location);

		final TupleWithSchema<String> actualTuple = client
				.getProvenanceData(resource, token);

		TestUtil.assertTuplesEquals(expectedTuple, actualTuple);
	}

	@Test
	public void authzTest() throws IOException,
			HabitatServiceException {
		final String resource = "authzTest";
		final BasicTuple expectedTuple = TestUtil.newTestTuple();
		final String tokenValue = RandomStringUtils.randomAlphanumeric(32);
		final ProvToken token = new ProvStringToken(tokenValue);
		final ProvLocation location = new ProvLocation(
				"ws.getProvenanceDataTestLocation", newArrayList(7, 8));

		final ProvenanceClient client1 = new ProvenanceClient(username,
				password, "localhost", Config.getServerPort());
		client1.createProvenanceGraph(resource);
		client1.storeProvenanceNode(resource, token, expectedTuple, location);

		final ProvenanceClient client2 = new ProvenanceClient(username2,
				password2, "localhost", Config.getServerPort());
		boolean exception = false;
		try {
			client2
					.getProvenanceData(resource, token);
		} catch (HabitatSecurityException e) {
			exception = true;
		}
		assertTrue(exception);
	}

	@Test
	public void getOutgoingTest() throws HabitatServiceException {
		final String resource = "outgoingTest";
		final String idPrefix = RandomStringUtils
				.randomAlphanumeric(32 - ".target2"
						.length());
		final ProvenanceClient client = new ProvenanceClient(
				username,
				password,
				"localhost",
				Config.getServerPort());
		final String sourceProvTokenValue = idPrefix + ".source";
		final String target1ProvTokenValue = idPrefix + ".target1";
		final String target2ProvTokenValue = idPrefix + ".target2";

		final ProvToken sourceProvToken = new ProvStringToken(
				sourceProvTokenValue);
		final ProvToken target1ProvToken = new ProvStringToken(
				target1ProvTokenValue);
		final ProvToken target2ProvToken = new ProvStringToken(
				target2ProvTokenValue);

		final ProvLocation sourceLocation = new ProvLocation(
				sourceProvTokenValue + ".location", newArrayList(3, 4));
		final ProvLocation target1Location = new ProvLocation(
				target1ProvTokenValue + ".location", newArrayList(8, 9));
		final ProvLocation target2Location = new ProvLocation(
				target2ProvTokenValue + ".location", newArrayList(11, 13));

		client.createProvenanceGraph(resource);

		client.storeProvenanceNode(resource, sourceProvToken, sourceLocation);
		client.storeProvenanceNode(resource, target1ProvToken, target1Location);
		client.storeProvenanceNode(resource, target2ProvToken, target2Location);

		final String expectedLabel1 = "LABEL1";
		client.storeProvenanceLink(resource, sourceProvToken,
				expectedLabel1, target1ProvToken);

		final String expectedLabel2 = "LABEL2";
		client.storeProvenanceLink(resource, sourceProvToken,
				expectedLabel2, target2ProvToken);

		final Iterable<ProvToken> outNeighborsWithLabel1 = client
				.getConnectedFrom(resource, sourceProvToken, expectedLabel1);
		final ProvToken actualNeighborWithLabel1 = getOnlyElement(
				outNeighborsWithLabel1, null);
		assertNotNull(actualNeighborWithLabel1);
		assertEquals(target1ProvToken, actualNeighborWithLabel1);

		final Iterable<ProvToken> outNeighborsWithLabel2 = client
				.getConnectedFrom(resource, sourceProvToken, expectedLabel2);
		final ProvToken actualNeighborWithLabel2 = getOnlyElement(
				outNeighborsWithLabel2, null);
		assertNotNull(actualNeighborWithLabel2);
		assertEquals(target2ProvToken, actualNeighborWithLabel2);

		final Iterable<ProvToken> outNeighborsNoLabel = client
				.getConnectedFrom(resource, sourceProvToken);
		final Set<ProvToken> actualNeighborNoLabel = newHashSet(
				outNeighborsNoLabel);
		assertEquals(2, actualNeighborNoLabel.size());
		assertEquals(newHashSet(target1ProvToken, target2ProvToken),
				actualNeighborNoLabel);

		final Map<String, List<TupleWithSchema<String>>> actualOutEdges = client
				.getEdgeMapFrom(resource, sourceProvToken);
		assertEquals(2, actualOutEdges.size());
		final Set<String> actualOutEdgeLabels = actualOutEdges
				.values()
				.stream()
				.map(tuples -> (String) getOnlyElement(tuples).get("label")
						.getValue())
				.collect(Collectors.toSet());
		assertEquals(newHashSet(expectedLabel1, expectedLabel2),
				actualOutEdgeLabels);

		final String wrongLabel = UUID.randomUUID().toString();
		final Iterable<ProvToken> outNeighborsWrongLabel = client
				.getConnectedFrom(resource, sourceProvToken, wrongLabel);
		final ProvToken actualNeighborWrongLabel = getOnlyElement(
				outNeighborsWrongLabel, null);
		assertNull(actualNeighborWrongLabel);

		final Iterable<ProvToken> outNeighborsWrongDirection = client
				.getConnectedFrom(resource, target1ProvToken);
		final ProvToken actualNeighborWrongDirection = getOnlyElement(
				outNeighborsWrongDirection, null);
		assertNull(actualNeighborWrongDirection);

		final Map<String, List<TupleWithSchema<String>>> actualOutEdgesWrongDirection = client
				.getEdgeMapFrom(resource, target1ProvToken);
		assertTrue(actualOutEdgesWrongDirection.isEmpty());

	}

	@Test
	public void getEdgesFromWithDataTest() throws HabitatServiceException {
		final String resource = "getEdgesFromDataTest";
		final String idPrefix = RandomStringUtils
				.randomAlphanumeric(32 - ".target2"
						.length());
		final ProvenanceClient client = new ProvenanceClient(
				username,
				password,
				"localhost",
				Config.getServerPort());
		final String sourceProvTokenValue = idPrefix + ".source";
		final String target1ProvTokenValue = idPrefix + ".target1";
		final String target2ProvTokenValue = idPrefix + ".target2";

		final ProvToken sourceProvToken = new ProvStringToken(
				sourceProvTokenValue);
		final ProvToken target1ProvToken = new ProvStringToken(
				target1ProvTokenValue);
		final ProvToken target2ProvToken = new ProvStringToken(
				target2ProvTokenValue);

		final ProvLocation sourceLocation = new ProvLocation(
				sourceProvTokenValue + ".location", newArrayList(3, 4));
		final ProvLocation target1Location = new ProvLocation(
				target1ProvTokenValue + ".location", newArrayList(8, 9));
		final ProvLocation target2Location = new ProvLocation(
				target2ProvTokenValue + ".location", newArrayList(11, 13));

		client.createProvenanceGraph(resource);
		client.storeProvenanceNode(resource, sourceProvToken, sourceLocation);
		client.storeProvenanceNode(resource, target1ProvToken, target1Location);
		client.storeProvenanceNode(resource, target2ProvToken, target2Location);

		final BasicTuple expectedData1 = TestUtil.newTestTuple();
		client.storeProvenanceLink(resource, sourceProvToken, "link",
				target1ProvToken,
				expectedData1);

		final BasicTuple expectedData2 = TestUtil.newTestTuple();
		client.storeProvenanceLink(resource, sourceProvToken, "link",
				target2ProvToken,
				expectedData2);

		final Map<String, List<TupleWithSchema<String>>> actualOutEdges = client
				.getEdgeMapFrom(resource, sourceProvToken);
		assertEquals(2, actualOutEdges.size());

		final TupleWithSchema<String> actualData1 = getOnlyElement(
				actualOutEdges
						.get(target1ProvTokenValue));
		assertNotNull(actualData1);
		assertContainsSameInfo(expectedData1, actualData1);

		final TupleWithSchema<String> actualData2 = getOnlyElement(
				actualOutEdges
						.get(target2ProvTokenValue));
		assertNotNull(actualData2);
		assertContainsSameInfo(expectedData2, actualData2);

		final Map<String, List<TupleWithSchema<String>>> actualOutEdgesWrongDirection = client
				.getEdgeMapFrom(resource, target1ProvToken);
		assertTrue(actualOutEdgesWrongDirection.isEmpty());

	}

	@Test
	public void getEdgesToWithDataTest() throws HabitatServiceException {
		final String resource = "edgesToDataTest";
		final String idPrefix = RandomStringUtils
				.randomAlphanumeric(32 - ".source2"
						.length());
		final ProvenanceClient client = new ProvenanceClient(
				username,
				password,
				"localhost",
				Config.getServerPort());
		final String targetProvTokenValue = idPrefix + ".target";
		final String source1ProvTokenValue = idPrefix + ".source1";
		final String source2ProvTokenValue = idPrefix + ".source2";

		final ProvToken targetProvToken = new ProvStringToken(
				targetProvTokenValue);
		final ProvToken source1ProvToken = new ProvStringToken(
				source1ProvTokenValue);
		final ProvToken source2ProvToken = new ProvStringToken(
				source2ProvTokenValue);

		final ProvLocation targetLocation = new ProvLocation(
				targetProvTokenValue + ".location", newArrayList(3, 4));
		final ProvLocation source1Location = new ProvLocation(
				source1ProvTokenValue + ".location", newArrayList(8, 9));
		final ProvLocation source2Location = new ProvLocation(
				source2ProvTokenValue + ".location", newArrayList(11, 13));

		client.createProvenanceGraph(resource);
		client.storeProvenanceNode(resource, targetProvToken, targetLocation);
		client.storeProvenanceNode(resource, source1ProvToken, source1Location);
		client.storeProvenanceNode(resource, source2ProvToken, source2Location);

		final BasicTuple expectedData1 = TestUtil.newTestTuple();
		client.storeProvenanceLink(resource, source1ProvToken, "link",
				targetProvToken,
				expectedData1);

		final BasicTuple expectedData2 = TestUtil.newTestTuple();
		client.storeProvenanceLink(resource, source2ProvToken, "link",
				targetProvToken,
				expectedData2);

		final Map<String, List<TupleWithSchema<String>>> actualInEdges = client
				.getEdgeMapTo(resource, targetProvToken);
		assertEquals(2, actualInEdges.size());

		final TupleWithSchema<String> actualData1 = getOnlyElement(actualInEdges
				.get(source1ProvTokenValue));
		assertContainsSameInfo(expectedData1, actualData1);

		final TupleWithSchema<String> actualData2 = getOnlyElement(actualInEdges
				.get(source2ProvTokenValue));
		assertContainsSameInfo(expectedData2, actualData2);

		final Map<String, List<TupleWithSchema<String>>> actualInEdgesWrongDirection = client
				.getEdgeMapTo(resource, source1ProvToken);
		assertTrue(actualInEdgesWrongDirection.isEmpty());

	}

	@Test
	public void getIncomingTest() throws HabitatServiceException {
		final String resource = "incomingTest";
		final String idPrefix = RandomStringUtils
				.randomAlphanumeric(32 - ".source2"
						.length());
		final ProvenanceClient client = new ProvenanceClient(
				username,
				password,
				"localhost",
				Config.getServerPort());
		final String targetProvTokenValue = idPrefix + ".target";
		final String source1ProvTokenValue = idPrefix + ".source1";
		final String source2ProvTokenValue = idPrefix + ".source2";

		final ProvToken targetProvToken = new ProvStringToken(
				targetProvTokenValue);
		final ProvToken source1ProvToken = new ProvStringToken(
				source1ProvTokenValue);
		final ProvToken source2ProvToken = new ProvStringToken(
				source2ProvTokenValue);

		final ProvLocation targetLocation = new ProvLocation(
				targetProvTokenValue + ".location", newArrayList(3, 4));
		final ProvLocation source1Location = new ProvLocation(
				source1ProvTokenValue + ".location", newArrayList(8, 9));
		final ProvLocation source2Location = new ProvLocation(
				source2ProvTokenValue + ".location", newArrayList(11, 13));

		client.createProvenanceGraph(resource);
		client.storeProvenanceNode(resource, targetProvToken, targetLocation);
		client.storeProvenanceNode(resource, source1ProvToken, source1Location);
		client.storeProvenanceNode(resource, source2ProvToken, source2Location);

		final String expectedLabel1 = "LABEL1";
		client.storeProvenanceLink(resource, source1ProvToken,
				expectedLabel1, targetProvToken);

		final String expectedLabel2 = "LABEL2";
		client.storeProvenanceLink(resource, source2ProvToken,
				expectedLabel2, targetProvToken);

		final Iterable<ProvToken> inNeighborsWithLabel1 = client
				.getConnectedTo(resource, targetProvToken, expectedLabel1);
		final ProvToken actualNeighborWithLabel1 = getOnlyElement(
				inNeighborsWithLabel1, null);
		assertNotNull(actualNeighborWithLabel1);
		assertEquals(source1ProvToken, actualNeighborWithLabel1);

		final Iterable<ProvToken> inNeighborsWithLabel2 = client
				.getConnectedTo(resource, targetProvToken, expectedLabel2);
		final ProvToken actualNeighborWithLabel2 = getOnlyElement(
				inNeighborsWithLabel2, null);
		assertNotNull(actualNeighborWithLabel2);
		assertEquals(source2ProvToken, actualNeighborWithLabel2);

		final Iterable<ProvToken> inNeighborsNoLabel = client
				.getConnectedTo(resource, targetProvToken);
		final Set<ProvToken> actualNeighborNoLabel = newHashSet(
				inNeighborsNoLabel);
		assertEquals(2, actualNeighborNoLabel.size());
		assertEquals(newHashSet(source1ProvToken, source2ProvToken),
				actualNeighborNoLabel);

		final Map<String, List<TupleWithSchema<String>>> actualInEdges = client
				.getEdgeMapTo(resource, targetProvToken);
		assertEquals(2, actualInEdges.size());
		final Set<String> actualInEdgeLabels = actualInEdges
				.values()
				.stream()
				.map(tuple -> (String) getOnlyElement(tuple).get("label")
						.getValue())
				.collect(Collectors.toSet());
		assertEquals(newHashSet(expectedLabel1, expectedLabel2),
				actualInEdgeLabels);

		final String wrongLabel = UUID.randomUUID().toString();
		final Iterable<ProvToken> inNeighborsWrongLabel = client
				.getConnectedTo(resource, targetProvToken, wrongLabel);
		final ProvToken actualNeighborWrongLabel = getOnlyElement(
				inNeighborsWrongLabel, null);
		assertNull(actualNeighborWrongLabel);

		final Iterable<ProvToken> inNeighborsWrongDirection = client
				.getConnectedTo(resource, source1ProvToken);
		final ProvToken actualNeighborWrongDirection = getOnlyElement(
				inNeighborsWrongDirection, null);
		assertNull(actualNeighborWrongDirection);

		final Map<String, List<TupleWithSchema<String>>> actualInEdgesWrongDirection = client
				.getEdgeMapTo(resource, source1ProvToken);
		assertTrue(actualInEdgesWrongDirection.isEmpty());
	}

	@Test
	public void getSubgraphs() throws HabitatServiceException {
		final List<List<NodeInfo>> ranks = new ArrayList<>();
		final List<NodeInfo> inputRank = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			inputRank.add(new NodeInfo("input" + i, ProvDmType.ENTITY.name(),
					false));
		}
		ranks.add(inputRank);
		ranks.add(singletonList(new NodeInfo("window",
				ProvDmType.COLLECTION.name(), true)));

		final List<LinkInfo> links = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			links.add(new LinkInfo("window", "input" + i,
					ProvDmRelation.MEMBERSHIP.name()));
		}
		final SubgraphTemplate initialTemplate = new SubgraphTemplate(ranks,
				links);
		final String graph = "subgraphTest";
		final ProvenanceClient client = new ProvenanceClient(
				username,
				password,
				"localhost",
				Config.getServerPort());
		client.createProvenanceGraph(graph);
		final List<SubgraphInstance> initialActualInstances = client
				.getSubgraphs(graph, initialTemplate, 400, null);
		assertEquals(1, initialActualInstances.size());
		final SubgraphInstance initialActualInstance = initialActualInstances
				.get(0);
		assertTrue(initialActualInstance.getRanks().isEmpty());
		assertTrue(initialActualInstance.getLinks().isEmpty());

		final SubgraphInstance expectedInstance = TestUtil
				.templateToTestInstance(initialTemplate, emptyMap());

		client.storeSubgraph(graph, expectedInstance);

		final List<SubgraphInstance> actualInstances = client
				.getSubgraphs(graph, initialTemplate, 400,
						initialActualInstance.getMaxTimestamp());
		TestUtil.assertSubgraphInstancesEqual(
				singletonList(expectedInstance), actualInstances);
		final long previousMax = actualInstances.get(0).getMaxTimestamp();

		// Next subgraph will overlap with the first
		final SubgraphInstance nextExpectedInstance = TestUtil
				.templateToTestInstance(initialTemplate, ImmutableMap.of(
						"input0",
						Optional.of(expectedInstance.getRanks().get(0).nodes
								.get(3)),
						"input1",
						Optional.of(expectedInstance.getRanks().get(0).nodes
								.get(4))));
		client.storeSubgraph(graph, nextExpectedInstance);
		final List<SubgraphInstance> nextActualInstances = client
				.getSubgraphs(graph, initialTemplate, 400, previousMax);
		TestUtil.assertSubgraphInstancesEqual(
				singletonList(nextExpectedInstance),
				nextActualInstances);
	}

	private void assertContainsSameInfo(
			Schema<String, ? extends Class<?>> expected,
			Schema<String, ? extends Class<?>> actual) {

		assertEquals(expected.getArity(), actual.getArity());

		assertEquals(ImmutableSet.copyOf(expected.getTypes()),
				ImmutableSet.copyOf(actual.getTypes()));

		assertEquals(ImmutableSet.copyOf(expected.getKeys()),
				ImmutableSet.copyOf(actual.getKeys()));
	}

	private void assertContainsSameInfo(
			TupleWithSchema<String> expected,
			TupleWithSchema<String> actual) {
		assertContainsSameInfo(
				expected.getSchema(), actual.getSchema());
		assertEquals(expected.size(), actual.size());
		for (final Map.Entry<String, Field<? extends Object>> expectedEntry : expected
				.entrySet()) {

			final Field<? extends Object> actualField = actual
					.get(expectedEntry.getKey());
			if (actualField == null) {
				throw new AssertionError(format(
						"expected contains entry: [%s], missing from actual",
						expectedEntry));
			}
			TestUtil.assertFieldsEqual(expectedEntry.getValue(), actualField);

		}
	}

}
