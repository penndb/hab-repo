/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.repository.prov;

import static com.google.common.collect.Maps.transformValues;
import static java.lang.String.format;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.jooq.DSLContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;
import com.mashape.unirest.http.exceptions.UnirestException;

import edu.upenn.cis.db.habitat.auth.UserApiLocal;
import edu.upenn.cis.db.habitat.auth.backends.plugins.AuthModule;
import edu.upenn.cis.db.habitat.auth.backends.plugins.HabitatRequestScopeModule;
import edu.upenn.cis.db.habitat.auth.backends.plugins.HabitatRequestScopeModule.HabitatRequest;
import edu.upenn.cis.db.habitat.auth.backends.plugins.HabitatRequestScopeModule.ScopedHabitatRequest;
import edu.upenn.cis.db.habitat.auth.backends.plugins.ProtectedStorageModule;
import edu.upenn.cis.db.habitat.auth.backends.plugins.SimpleScope;
import edu.upenn.cis.db.habitat.auth.backends.plugins.TemporaryDbModule;
import edu.upenn.cis.db.habitat.core.api.ProvenanceGraphApi;
import edu.upenn.cis.db.habitat.core.api.UserApi;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance.NodeInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate.LinkInfo;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate.NodeInfo;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvDmRelation;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvDmType;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.provenance.ProvGraphStorageModule;
import edu.upenn.cis.db.habitat.repository.storage.neo4j.ConfigurableNeoStorageModule;
import edu.upenn.cis.db.habitat.repository.storage.neo4j.Neo4JStore;
import edu.upenn.cis.db.habitat.repository.storage.neo4j.SubgraphQuery.NodeIdLocation;
import edu.upenn.cis.db.habitat.test.TestUtil;

public class SubgraphTest {

	private ProvenanceGraphApi store;
	private SimpleScope habitatRequestScope;
	private DSLContext dslContext;

	@Rule
	public Neo4jBoltRule neo4j = new Neo4jBoltRule();

	private static final String username1 = "protectedStoreUser1";
	private static final String password1 = "protectedStoreUserPassword1";
	private static final Gson gson = new GsonBuilder().create();

	@Before
	public void setupGuiceAndRdms() throws UnirestException,
			HabitatServiceException {
		final Injector injector = Guice.createInjector(
				new ConfigurableNeoStorageModule(neo4j.getBoltUri(), null,
						null),
				new TemporaryDbModule(),
				new AuthModule(),
				new HabitatRequestScopeModule(),
				new ProtectedStorageModule(Neo4JStore.class),
				new ProvGraphStorageModule());
		dslContext = injector.getInstance(DSLContext.class);
		store = injector.getInstance(ProvenanceGraphApi.class);
		final UserApiLocal userApiLocalBackend = injector
				.getInstance(UserApiLocal.class);
		userApiLocalBackend.createTables();
		habitatRequestScope = injector
				.getInstance(Key.get(
						SimpleScope.class,
						Names.named(
								HabitatRequestScopeModule.HABITAT_REQUEST_SCOPE_NAME)));

		runScopedRequest(
				UserApi.Root,
				() -> {
					final boolean addUser1 = userApiLocalBackend
							.addUserAndOrganization(
									username1,
									null,
									null,
									null,
									null,
									null,
									null,
									null,
									null,
									null,
									"provWsTestUser@mail",
									null,
									null,
									"", "");
					assertTrue(addUser1);
					final boolean addCred1 = userApiLocalBackend.addCredential(
							username1,
							"local",
							"http",
							password1);
					assertTrue(addCred1);
				});
	}

	@Test
	public void getSubgraphs() throws HabitatServiceException {
		final List<List<NodeInfo>> ranks = new ArrayList<>();
		final List<NodeInfo> inputRank = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			inputRank.add(new NodeInfo("input" + i, ProvDmType.ENTITY.name(),
					false));
		}
		ranks.add(inputRank);
		ranks.add(singletonList(new NodeInfo("window",
				ProvDmType.COLLECTION.name(), true)));

		final List<LinkInfo> links = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			links.add(new LinkInfo("window", "input" + i,
					ProvDmRelation.MEMBERSHIP.name()));
		}

		final String graph = "subgraphTest";

		final SubgraphTemplate initialTemplate = new SubgraphTemplate(ranks,
				links);
		final Map<String, NodeIdLocation> templateIdToLocator = ImmutableMap
				.of(
						"input0",
						new NodeIdLocation(0, 3),
						"input1",
						new NodeIdLocation(0, 4));
		runTest(graph, initialTemplate, templateIdToLocator);

	}

	@Test
	public void getSubgraphs2() throws HabitatServiceException {
		final List<List<NodeInfo>> ranks = new ArrayList<>();
		final List<NodeInfo> childrenRank = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			childrenRank
					.add(new NodeInfo("child" + i, ProvDmType.ENTITY.name(),
							true));
		}
		ranks.add(childrenRank);
		ranks.add(singletonList(new NodeInfo("parent",
				ProvDmType.ENTITY.name(), false)));

		final List<LinkInfo> links = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			links.add(new LinkInfo("child" + i, "parent",
					ProvDmRelation.DERIVATION.name()));
		}

		final String graph = "subgraphTest2";

		final SubgraphTemplate initialTemplate = new SubgraphTemplate(ranks,
				links);
		final Map<String, NodeIdLocation> templateIdToLocator = ImmutableMap
				.of(
						"parent",
						new NodeIdLocation(1, 0));
		runTest(graph, initialTemplate, templateIdToLocator);
	}

	@Test
	public void getSubgraphs3() throws HabitatServiceException {
		final List<List<NodeInfo>> ranks = new ArrayList<>();
		final List<NodeInfo> childrenRank = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			childrenRank
					.add(new NodeInfo("child" + i, ProvDmType.ENTITY.name(),
							true));
		}
		for (int i = 0; i < 2; i++) {
			childrenRank
					.add(new NodeInfo("alt" + i, ProvDmType.ENTITY.name(),
							true));
		}
		ranks.add(childrenRank);
		ranks.add(singletonList(new NodeInfo("base",
				ProvDmType.ENTITY.name(), false)));

		final List<LinkInfo> links = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			links.add(new LinkInfo("child" + i, "base",
					ProvDmRelation.DERIVATION.name()));
		}
		for (int i = 0; i < 2; i++) {
			links.add(new LinkInfo("alt" + i, "base",
					ProvDmRelation.ALTERNATE.name()));
		}

		final String graph = "subgraphTest3";

		final SubgraphTemplate initialTemplate = new SubgraphTemplate(ranks,
				links);

		final Map<String, NodeIdLocation> templateIdToLocator = ImmutableMap
				.of(
						"base",
						new NodeIdLocation(1, 0));
		runTest(graph, initialTemplate, templateIdToLocator);
	}

	@Test
	public void getSubgraphs4() throws HabitatServiceException {
		final List<List<NodeInfo>> ranks = new ArrayList<>();
		final List<NodeInfo> childrenRank = new ArrayList<>();
		for (int i = 0; i < 4; i++) {
			childrenRank
					.add(new NodeInfo("child" + i, ProvDmType.ENTITY.name(),
							true));
		}
		for (int i = 0; i < 2; i++) {
			childrenRank
					.add(new NodeInfo("alt" + i, ProvDmType.ENTITY.name(),
							true));
		}
		ranks.add(childrenRank);
		ranks.add(singletonList(new NodeInfo("base",
				ProvDmType.ENTITY.name(), false)));

		final List<LinkInfo> links = new ArrayList<>();
		for (int i = 0; i < 4; i++) {
			links.add(new LinkInfo("child" + i, "base",
					ProvDmRelation.DERIVATION.name()));
		}
		for (int i = 0; i < 2; i++) {
			links.add(new LinkInfo("base", "alt" + i,
					ProvDmRelation.ALTERNATE.name()));
		}

		final String graph = "subgraphTest4";

		final SubgraphTemplate initialTemplate = new SubgraphTemplate(ranks,
				links);
		final Map<String, NodeIdLocation> templateIdToLocator = ImmutableMap
				.of(
						"base",
						new NodeIdLocation(1, 0));
		runTest(graph, initialTemplate, templateIdToLocator);
	}

	@Test
	public void getSubgraphs5() throws HabitatServiceException {
		final List<List<NodeInfo>> ranks = new ArrayList<>();
		ranks.add(singletonList(new NodeInfo("a",
				ProvDmType.ENTITY.name(), true)));
		ranks.add(singletonList(new NodeInfo("b",
				ProvDmType.ENTITY.name(), true)));
		ranks.add(singletonList(new NodeInfo("c",
				ProvDmType.ENTITY.name(), true)));

		final List<LinkInfo> links = new ArrayList<>();
		links.add(new LinkInfo("a", "b",
				ProvDmRelation.DERIVATION.name()));
		links.add(new LinkInfo("b", "c",
				ProvDmRelation.DERIVATION.name()));

		final String graph = "subgraphTest5";

		final SubgraphTemplate initialTemplate = new SubgraphTemplate(ranks,
				links);
		final Map<String, NodeIdLocation> templateIdToLocator = emptyMap();
		runTest(graph, initialTemplate, templateIdToLocator);
	}

	@Test
	public void getSubgraphs6() throws HabitatServiceException {
		final List<List<NodeInfo>> ranks = new ArrayList<>();
		ranks.add(singletonList(new NodeInfo("a",
				ProvDmType.ENTITY.name(), true)));
		ranks.add(singletonList(new NodeInfo("b",
				ProvDmType.ENTITY.name(), true)));
		ranks.add(singletonList(new NodeInfo("c",
				ProvDmType.ENTITY.name(), false)));

		final List<LinkInfo> links = new ArrayList<>();
		links.add(new LinkInfo("a", "b",
				ProvDmRelation.DERIVATION.name()));
		links.add(new LinkInfo("b", "c",
				ProvDmRelation.DERIVATION.name()));

		final String graph = "subgraphTest6";

		final SubgraphTemplate initialTemplate = new SubgraphTemplate(ranks,
				links);
		final Map<String, NodeIdLocation> templateIdToLocator = ImmutableMap
				.of("c",
						new NodeIdLocation(2, 0));
		runTest(graph, initialTemplate, templateIdToLocator);
	}

	@Test
	public void getSubgraphs7() throws HabitatServiceException {
		final List<List<NodeInfo>> ranks = new ArrayList<>();
		ranks.add(ImmutableList.of(
				new NodeInfo("a1",
						ProvDmType.ENTITY.name(), false),
				new NodeInfo("a2",
						ProvDmType.ENTITY.name(), false)));
		ranks.add(ImmutableList.of(
				new NodeInfo("b1",
						ProvDmType.ENTITY.name(), true),
				new NodeInfo("b2",
						ProvDmType.ENTITY.name(), true)));
		ranks.add(singletonList(
				new NodeInfo("c",
						ProvDmType.ENTITY.name(), true)));

		final List<LinkInfo> links = new ArrayList<>();
		links.add(new LinkInfo("a1", "b1",
				ProvDmRelation.DERIVATION.name()));
		links.add(new LinkInfo("b1", "c",
				ProvDmRelation.DERIVATION.name()));

		links.add(new LinkInfo("a2", "b2",
				ProvDmRelation.DERIVATION.name()));
		links.add(new LinkInfo("b2", "c",
				ProvDmRelation.DERIVATION.name()));

		final String graph = "subgraphTest7";

		final SubgraphTemplate initialTemplate = new SubgraphTemplate(ranks,
				links);
		final Map<String, NodeIdLocation> templateIdToLocator = ImmutableMap
				.of(
						"a1",
						new NodeIdLocation(0, 1));
		runTest(graph, initialTemplate, templateIdToLocator);
	}

	@Test
	public void getSubgraphs8() throws HabitatServiceException {
		final List<List<NodeInfo>> ranks = new ArrayList<>();
		ranks.add(ImmutableList.of(
				new NodeInfo("a1",
						ProvDmType.ENTITY.name(), true),
				new NodeInfo("a2",
						ProvDmType.ENTITY.name(), true)));
		ranks.add(ImmutableList.of(
				new NodeInfo("b1",
						ProvDmType.ENTITY.name(), true),
				new NodeInfo("b2",
						ProvDmType.ENTITY.name(), true)));
		ranks.add(singletonList(
				new NodeInfo("c",
						ProvDmType.ENTITY.name(), true)));

		final List<LinkInfo> links = new ArrayList<>();
		links.add(new LinkInfo("a1", "b1",
				ProvDmRelation.DERIVATION.name()));
		links.add(new LinkInfo("b1", "c",
				ProvDmRelation.DERIVATION.name()));

		links.add(new LinkInfo("a2", "b2",
				ProvDmRelation.DERIVATION.name()));
		links.add(new LinkInfo("b2", "c",
				ProvDmRelation.DERIVATION.name()));

		final String graph = "subgraphTest8";

		final SubgraphTemplate initialTemplate = new SubgraphTemplate(ranks,
				links);
		final Map<String, NodeIdLocation> templateIdToLocator = emptyMap();
		runTest(graph, initialTemplate, templateIdToLocator);
	}

	@Test
	public void getSubgraphs9() throws HabitatServiceException {
		final List<List<NodeInfo>> ranks = new ArrayList<>();
		ranks.add(ImmutableList.of(
				new NodeInfo("a1",
						ProvDmType.ENTITY.name(), false),
				new NodeInfo("a2",
						ProvDmType.ENTITY.name(), false)));
		ranks.add(ImmutableList.of(
				new NodeInfo("b",
						ProvDmType.ENTITY.name(), true)));
		ranks.add(singletonList(
				new NodeInfo("c",
						ProvDmType.ENTITY.name(), true)));

		final List<LinkInfo> links = new ArrayList<>();
		links.add(new LinkInfo("a1", "b",
				ProvDmRelation.DERIVATION.name()));
		links.add(new LinkInfo("a1", "c",
				ProvDmRelation.DERIVATION.name()));

		links.add(new LinkInfo("a2", "b",
				ProvDmRelation.DERIVATION.name()));
		links.add(new LinkInfo("a2", "c",
				ProvDmRelation.DERIVATION.name()));

		final String graph = "subgraphTest9";

		final SubgraphTemplate initialTemplate = new SubgraphTemplate(ranks,
				links);
		final Map<String, NodeIdLocation> templateIdToLocator = ImmutableMap
				.of(
						"a1",
						new NodeIdLocation(0, 1));
		runTest(graph, initialTemplate, templateIdToLocator);
	}

	@Test
	public void getSubgraphs10() throws HabitatServiceException {
		final List<List<NodeInfo>> ranks = new ArrayList<>();
		ranks.add(ImmutableList.of(
				new NodeInfo("a1",
						ProvDmType.ENTITY.name(), false),
				new NodeInfo("a2",
						ProvDmType.ENTITY.name(), false)));
		ranks.add(ImmutableList.of(
				new NodeInfo("b",
						ProvDmType.ENTITY.name(), true)));
		ranks.add(singletonList(
				new NodeInfo("c",
						ProvDmType.ENTITY.name(), true)));

		final List<LinkInfo> links = new ArrayList<>();
		links.add(new LinkInfo("a1", "b",
				ProvDmRelation.DERIVATION.name()));
		links.add(new LinkInfo("a1", "c",
				ProvDmRelation.ALTERNATE.name()));

		links.add(new LinkInfo("a2", "b",
				ProvDmRelation.DERIVATION.name()));
		links.add(new LinkInfo("a2", "c",
				ProvDmRelation.ALTERNATE.name()));

		final String graph = "subgraphTest10";

		final SubgraphTemplate initialTemplate = new SubgraphTemplate(ranks,
				links);
		final Map<String, NodeIdLocation> templateIdToLocator = ImmutableMap
				.of(
						"a1",
						new NodeIdLocation(0, 1));
		runTest(graph, initialTemplate, templateIdToLocator);
	}

	@Test
	public void getSubgraphs11() throws HabitatServiceException {
		final List<List<NodeInfo>> ranks = new ArrayList<>();
		ranks.add(ImmutableList.of(
				new NodeInfo("a1",
						ProvDmType.ENTITY.name(), false),
				new NodeInfo("a2",
						ProvDmType.ENTITY.name(), false),
				new NodeInfo("a3",
						ProvDmType.ENTITY.name(), false)));
		ranks.add(ImmutableList.of(
				new NodeInfo("b1",
						ProvDmType.ENTITY.name(), true),
				new NodeInfo("b2",
						ProvDmType.ENTITY.name(), true)));

		final List<LinkInfo> links = new ArrayList<>();
		links.add(new LinkInfo("a1", "b1",
				ProvDmRelation.DERIVATION.name()));
		links.add(new LinkInfo("a2", "b1",
				ProvDmRelation.DERIVATION.name()));

		links.add(new LinkInfo("a2", "b2",
				ProvDmRelation.DERIVATION.name()));
		links.add(new LinkInfo("a3", "b2",
				ProvDmRelation.DERIVATION.name()));

		final String graph = "subgraphTest11";

		final SubgraphTemplate initialTemplate = new SubgraphTemplate(ranks,
				links);
		final Map<String, NodeIdLocation> templateIdToLocator = ImmutableMap
				.of(
						"a1",
						new NodeIdLocation(0, 1));
		runTest(graph, initialTemplate, templateIdToLocator);
	}

	@Test
	public void getSubgraphs12() throws HabitatServiceException {
		final List<List<NodeInfo>> ranks = new ArrayList<>();
		ranks.add(ImmutableList.of(
				new NodeInfo("a1",
						ProvDmType.ENTITY.name(), false),
				new NodeInfo("a2",
						ProvDmType.ENTITY.name(), false),
				new NodeInfo("a3",
						ProvDmType.ENTITY.name(), false)));
		ranks.add(ImmutableList.of(
				new NodeInfo("b1",
						ProvDmType.ENTITY.name(), true),
				new NodeInfo("b2",
						ProvDmType.ENTITY.name(), true)));

		final List<LinkInfo> links = new ArrayList<>();
		links.add(new LinkInfo("a1", "b1",
				ProvDmRelation.DERIVATION.name()));
		links.add(new LinkInfo("a2", "b1",
				ProvDmRelation.DERIVATION.name()));

		links.add(new LinkInfo("a2", "b2",
				ProvDmRelation.ALTERNATE.name()));
		links.add(new LinkInfo("a3", "b2",
				ProvDmRelation.ALTERNATE.name()));

		final String graph = "subgraphTest12";

		final SubgraphTemplate initialTemplate = new SubgraphTemplate(ranks,
				links);
		final Map<String, NodeIdLocation> templateIdToLocator = ImmutableMap
				.of(
						"a1",
						new NodeIdLocation(0, 1));
		runTest(graph, initialTemplate, templateIdToLocator);
	}

	@Test
	public void getSubgraphs13() throws HabitatServiceException, IOException {

		try (final InputStream templateStream = getClass().getResourceAsStream(
				"/two-outputs.json");
				final Reader templateReader = new InputStreamReader(
						templateStream);) {
			final SubgraphTemplate initialTemplate = gson.fromJson(
					templateReader, SubgraphTemplate.class);

			final String graph = "subgraphTest13";

			final Map<String, NodeIdLocation> templateIdToLocator = ImmutableMap
					.of(
							"input0",
							new NodeIdLocation(0, 3));
			final ImmutableList<SubgraphInstance> expectedInstances = runTest(
					graph, initialTemplate, templateIdToLocator);

			// This template has an orderBy field, so check order is as
			// expected.
			runScopedRequest(
					username1,
					() -> {
						final List<SubgraphInstance> actualInstances = store
								.getSubgraphs(graph,
										initialTemplate,
										400,
										0L);
						TestUtil.assertSubgraphInstancesEqual(
								expectedInstances, actualInstances);
					});
		}

	}

	@Test
	public void getSubgraphsOptionalNode() throws Exception {
		final List<List<NodeInfo>> ranks = new ArrayList<>();

		ranks.add(singletonList(new NodeInfo("input", ProvDmType.ENTITY.name(),
				false)));
		ranks.add(singletonList(new NodeInfo("activity",
				ProvDmType.ACTIVITY.name(), true)));
		ranks.add(singletonList(new NodeInfo("output", ProvDmType.ENTITY.name(),
				true, true)));

		final List<LinkInfo> links = new ArrayList<>();
		links.add(
				new LinkInfo("activity", "input",
						ProvDmRelation.USAGE.name()));
		links.add(
				new LinkInfo("output", "activity",
						ProvDmRelation.GENERATION.name()));

		final String graph = "optionalOutputGraph";

		final SubgraphTemplate initialTemplate = new SubgraphTemplate(ranks,
				links,
				singletonList("activity"));

		try {
			final long[] previousMax = new long[1];
			runScopedRequest(
					username1,
					() -> {
						store.createProvenanceGraph(graph);
						final List<SubgraphInstance> actualInstances = store
								.getSubgraphs(graph, initialTemplate, 400,
										null);
						assertEquals(1, actualInstances.size());
						final SubgraphInstance actualInstance = actualInstances
								.get(0);
						assertTrue(actualInstance.getRanks().isEmpty());
						assertTrue(actualInstance.getLinks().isEmpty());
						previousMax[0] = actualInstance.getMaxTimestamp();
					});

			final long initialMaxTimestamp = previousMax[0];

			final SubgraphInstance expectedInstance = TestUtil
					.templateToTestInstance(initialTemplate, emptyMap());

			storeSubgraphInstance(graph, expectedInstance);

			// Use map to define node overlap between instances.
			final Map<String, Optional<NodeInstance>> templateIdToInstanceId2 = ImmutableMap
					.of(
							"output", Optional.empty(),
							"input",
							Optional.of(expectedInstance.getRanks().get(0).nodes
									.get(0)));
			final SubgraphInstance nextExpectedInstance = TestUtil
					.templateToTestInstance(initialTemplate,
							templateIdToInstanceId2);
			storeSubgraphInstance(graph, nextExpectedInstance);

			runScopedRequest(
					username1,
					() -> {
						final List<SubgraphInstance> actualInstances = store
								.getSubgraphs(graph, initialTemplate, 400,
										initialMaxTimestamp);
						TestUtil.assertSubgraphInstancesEqual(
								ImmutableList.of(expectedInstance,
										nextExpectedInstance),
								actualInstances);
					});
		} catch (AssertionError e) {
			neo4j.runAssertionInTx(db -> {
				final Transaction tx = db.beginTx();
				try {

					for (final Node n :tx.getAllNodes()) {
						System.out.println(format("%d: %s: %s %s", n.getId(),
						n.getLabels(),
						n.getProperty("_key"), n.getProperty("_created")));
					}
					for (final Relationship r : tx.getAllRelationships()) {
						System.out.println(r);
					}
				} finally {
					tx.commit();
				}
			});
			throw e;
		}

	}

	@Test
	public void getSubgraphsOptionalNodeIncrementalPresentFirst()
			throws Exception {
		final List<List<NodeInfo>> ranks = new ArrayList<>();

		ranks.add(singletonList(new NodeInfo("input", ProvDmType.ENTITY.name(),
				false)));
		ranks.add(singletonList(new NodeInfo("activity",
				ProvDmType.ACTIVITY.name(), true)));
		ranks.add(singletonList(new NodeInfo("output", ProvDmType.ENTITY.name(),
				true, true)));

		final List<LinkInfo> links = new ArrayList<>();
		links.add(
				new LinkInfo("activity", "input",
						ProvDmRelation.USAGE.name()));
		links.add(
				new LinkInfo("output", "activity",
						ProvDmRelation.GENERATION.name()));

		final String graph = "optionalIncrementalPresentGraph";

		final SubgraphTemplate initialTemplate = new SubgraphTemplate(ranks,
				links,
				singletonList("activity"));

		try {
			final long[] previousMax = new long[1];
			runScopedRequest(
					username1,
					() -> {
						store.createProvenanceGraph(graph);
						final List<SubgraphInstance> actualInstances = store
								.getSubgraphs(graph, initialTemplate, 400,
										null);
						assertEquals(1, actualInstances.size());
						final SubgraphInstance actualInstance = actualInstances
								.get(0);
						assertTrue(actualInstance.getRanks().isEmpty());
						assertTrue(actualInstance.getLinks().isEmpty());
						previousMax[0] = actualInstance.getMaxTimestamp();
					});
			final long initialMaxTimeStamp = previousMax[0];

			final SubgraphInstance expectedInstance = TestUtil
					.templateToTestInstance(initialTemplate, emptyMap());

			storeSubgraphInstance(graph, expectedInstance);

			runScopedRequest(username1, () -> {
				final List<SubgraphInstance> actualInstances = store
						.getSubgraphs(graph, initialTemplate,
								400, previousMax[0]);
				TestUtil.assertSubgraphInstancesEqual(
						singletonList(expectedInstance), actualInstances);
				previousMax[0] = actualInstances.get(0).getMaxTimestamp();
			});

			// Use map to define node overlap between instances.
			final Map<String, Optional<NodeInstance>> templateIdToInstanceId2 = ImmutableMap
					.of(
							"output", Optional.empty(),
							"input",
							Optional.of(expectedInstance.getRanks().get(0).nodes
									.get(0)));
			final SubgraphInstance nextExpectedInstance = TestUtil
					.templateToTestInstance(initialTemplate,
							templateIdToInstanceId2);
			storeSubgraphInstance(graph, nextExpectedInstance);

			runScopedRequest(username1, () -> {
				final List<SubgraphInstance> actualInstances = store
						.getSubgraphs(graph, initialTemplate,
								400, previousMax[0]);
				TestUtil.assertSubgraphInstancesEqual(
						singletonList(nextExpectedInstance), actualInstances);
			});

			runScopedRequest(
					username1,
					() -> {
						final List<SubgraphInstance> actualInstances = store
								.getSubgraphs(graph, initialTemplate, 400,
										initialMaxTimeStamp);
						TestUtil.assertSubgraphInstancesEqual(
								ImmutableList.of(expectedInstance,
										nextExpectedInstance),
								actualInstances);
					});

		} catch (AssertionError e) {
			neo4j.runAssertionInTx(db -> {
				final Transaction tx = db.beginTx();
				try {
					for (final Node n : tx.getAllNodes()) {
						System.out.println(format("%d: %s: %s %s", n.getId(),
								n.getLabels(),
								n.getProperty("_key"), n.getProperty("_created")));
					}
					for (final Relationship r : tx.getAllRelationships()) {
						System.out.println(r);
					}
				} finally {
					tx.commit();
				}
			});
			throw e;
		}

	}

	@Test
	public void getSubgraphsOptionalNodeIncrementalAbsentFirst()
			throws Exception {
		final List<List<NodeInfo>> ranks = new ArrayList<>();

		ranks.add(singletonList(new NodeInfo("input", ProvDmType.ENTITY.name(),
				false)));
		ranks.add(singletonList(new NodeInfo("activity",
				ProvDmType.ACTIVITY.name(), true)));
		ranks.add(singletonList(new NodeInfo("output", ProvDmType.ENTITY.name(),
				true, true)));

		final List<LinkInfo> links = new ArrayList<>();
		links.add(
				new LinkInfo("activity", "input",
						ProvDmRelation.USAGE.name()));
		links.add(
				new LinkInfo("output", "activity",
						ProvDmRelation.GENERATION.name()));

		final String graph = "optionalIncrementalAbsentGraph";

		final SubgraphTemplate initialTemplate = new SubgraphTemplate(ranks,
				links,
				singletonList("activity"));

		try {
			final long[] previousMax = new long[1];
			runScopedRequest(
					username1,
					() -> {
						store.createProvenanceGraph(graph);
						final List<SubgraphInstance> actualInstances = store
								.getSubgraphs(graph, initialTemplate, 400,
										null);
						assertEquals(1, actualInstances.size());
						final SubgraphInstance actualInstance = actualInstances
								.get(0);
						assertTrue(actualInstance.getRanks().isEmpty());
						assertTrue(actualInstance.getLinks().isEmpty());
						previousMax[0] = actualInstance.getMaxTimestamp();
					});
			final long initialMaxTimestamp = previousMax[0];

			final SubgraphInstance expectedInstance = TestUtil
					.templateToTestInstance(initialTemplate,
							ImmutableMap.of("output", Optional.empty()));

			storeSubgraphInstance(graph, expectedInstance);

			runScopedRequest(username1, () -> {
				final List<SubgraphInstance> actualInstances = store
						.getSubgraphs(graph, initialTemplate,
								400, previousMax[0]);
				TestUtil.assertSubgraphInstancesEqual(
						singletonList(expectedInstance), actualInstances);
				previousMax[0] = actualInstances.get(0).getMaxTimestamp();
			});

			// Use map to define node overlap between instances.
			final Map<String, Optional<NodeInstance>> templateIdToInstanceId2 = ImmutableMap
					.of("input",
							Optional.of(expectedInstance.getRanks().get(0).nodes
									.get(0)));
			final SubgraphInstance nextExpectedInstance = TestUtil
					.templateToTestInstance(initialTemplate,
							templateIdToInstanceId2);
			storeSubgraphInstance(graph, nextExpectedInstance);

			runScopedRequest(username1, () -> {
				final List<SubgraphInstance> actualInstances = store
						.getSubgraphs(graph, initialTemplate,
								400, previousMax[0]);
				TestUtil.assertSubgraphInstancesEqual(
						singletonList(nextExpectedInstance), actualInstances);
			});

			runScopedRequest(
					username1,
					() -> {
						final List<SubgraphInstance> actualInstances = store
								.getSubgraphs(graph, initialTemplate, 400,
										initialMaxTimestamp);
						TestUtil.assertSubgraphInstancesEqual(
								ImmutableList.of(expectedInstance,
										nextExpectedInstance),
								actualInstances);
					});
		} catch (AssertionError e) {
			neo4j.runAssertionInTx(db -> {
				final Transaction tx = db.beginTx();
				try {
					for (final Node n : tx.getAllNodes()) {
						System.out.println(format("%d: %s: %s %s", n.getId(),
								n.getLabels(),
								n.getProperty("_key"), n.getProperty("_created")));
					}
					for (final Relationship r : tx.getAllRelationships()) {
						System.out.println(r);
					}
				} finally {
					tx.commit();
				}
			});
			throw e;
		}

	}

	private ImmutableList<SubgraphInstance> runTest(
			String graph,
			SubgraphTemplate initialTemplate,
			Map<String, NodeIdLocation> templateIdToLocator)
			throws HabitatServiceException {

		final ImmutableList.Builder<SubgraphInstance> expectedInstances = ImmutableList
				.builder();

		final long[] previousMax = new long[1];
		runScopedRequest(
				username1,
				() -> {
					store.createProvenanceGraph(graph);
					final List<SubgraphInstance> actualInstances = store
							.getSubgraphs(graph, initialTemplate, 400, null);
					assertEquals(1, actualInstances.size());
					final SubgraphInstance actualInstance = actualInstances
							.get(0);
					assertTrue(actualInstance.getRanks().isEmpty());
					assertTrue(actualInstance.getLinks().isEmpty());
					previousMax[0] = actualInstance.getMaxTimestamp();
				});

		final SubgraphInstance expectedInstance = TestUtil
				.templateToTestInstance(initialTemplate, emptyMap());

		storeSubgraphInstance(graph, expectedInstance);
		expectedInstances.add(expectedInstance);

		runScopedRequest(
				username1,
				() -> {
					final List<SubgraphInstance> actualInstances = store
							.getSubgraphs(graph, initialTemplate, 400,
									previousMax[0]);
					TestUtil.assertSubgraphInstancesEqual(
							singletonList(expectedInstance), actualInstances);
					previousMax[0] = actualInstances.get(0).getMaxTimestamp();
				});

		// Use map to define node overlap between instances.
		final Map<String, Optional<NodeInstance>> templateIdToInstanceId = transformValues(
				templateIdToLocator,
				locator -> Optional.of(expectedInstance.getRanks()
						.get(locator.rankIndex).nodes.get(locator.nodeIndex)));
		final SubgraphInstance nextExpectedInstance = TestUtil
				.templateToTestInstance(initialTemplate,
						templateIdToInstanceId);
		storeSubgraphInstance(graph, nextExpectedInstance);
		expectedInstances.add(nextExpectedInstance);
		runScopedRequest(
				username1,
				() -> {
					final List<SubgraphInstance> actualInstances = store
							.getSubgraphs(graph, initialTemplate, 400,
									previousMax[0]);
					TestUtil.assertSubgraphInstancesEqual(
							singletonList(nextExpectedInstance),
							actualInstances);
				});

		return expectedInstances.build();

	}

	private void storeSubgraphInstance(final String graph,
			final SubgraphInstance expectedInstance)
			throws HabitatServiceException {
		runScopedRequest(
				username1,
				() -> {
					store.storeSubgraph(graph, expectedInstance);
				});
	}

	private void runScopedRequest(String user, HabitatRequest req)
			throws HabitatServiceException {
		new ScopedHabitatRequest(
				habitatRequestScope,
				user,
				req).makeRequest();
	}

	@After
	public void tearDown() throws IOException {
		TemporaryDbModule.emptyDatabase(dslContext);
	}
}
