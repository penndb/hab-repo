/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice;

import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Sets.newHashSet;
import io.swagger.models.HttpMethod;
import io.swagger.models.Path;
import io.swagger.models.Swagger;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;

import edu.upenn.cis.db.habitat.core.webservice.RestPlugin;
import edu.upenn.cis.db.habitat.core.webservice.RouteSpecifier;

/**
 * Do the Swagger and Spark paths match?
 * 
 * @author John Frommeyer
 *
 */
public class SwaggerConformTest {

	private static final class MethodPath {
		final String method;
		final String path;

		public MethodPath(String method, String path) {
			this.method = method.toUpperCase();
			this.path = path.toLowerCase();
		}

		@Override
		public String toString() {
			return method + " " + path;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((method == null) ? 0 : method.hashCode());
			result = prime * result + ((path == null) ? 0 : path.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof MethodPath)) {
				return false;
			}
			MethodPath other = (MethodPath) obj;
			if (method == null) {
				if (other.method != null) {
					return false;
				}
			} else if (!method.equals(other.method)) {
				return false;
			}
			if (path == null) {
				if (other.path != null) {
					return false;
				}
			} else if (!path.equals(other.path)) {
				return false;
			}
			return true;
		}

	}

	private static final String pathVariable = "/path_param_variable";

	private Map<String, RestPlugin> pluginMap;
	
	private RestPluginMapTestUtil restPluginTestUtil;

	private Swagger swagger;

	@Before
	public void init() {
		restPluginTestUtil = new RestPluginMapTestUtil();
		pluginMap = restPluginTestUtil.pluginMap;
		swagger = SwaggerParser
				.getSwagger(RestService.APP_PACKAGE);
	}

	@Test
	public void methodAndPathsConformTest() {

		final Map<String, Path> swaggerPaths = swagger.getPaths();
		final Set<MethodPath> swaggerMethodPaths = gatherSwaggerMethodPaths(swaggerPaths);

		for (final Map.Entry<String, RestPlugin> pluginMapEntry : pluginMap
				.entrySet()) {
			final String pluginPathPrefix = pluginMapEntry.getKey();
			final RestPlugin plugin = pluginMapEntry.getValue();
			final Set<MethodPath> pluginMethodPaths = gatherPluginMethodPaths(
					pluginPathPrefix,
					plugin);
			final Set<MethodPath> swaggerMethodPathsWithPrefix = getSwaggerMethodPathsForPlugin(
					pluginPathPrefix,
					swaggerMethodPaths);
			assertMethodPathsEqual(
					plugin,
					pluginMethodPaths,
					swaggerMethodPathsWithPrefix);

		}
	}

	private void assertMethodPathsEqual(
			RestPlugin plugin,
			Set<MethodPath> fromPlugin,
			Set<MethodPath> fromSwagger) {
		if (fromPlugin.equals(fromSwagger)) {
			return;
		}
		final SetView<MethodPath> missingFromSwagger = Sets.difference(
				fromPlugin, fromSwagger);
		final SetView<MethodPath> missingFromPlugin = Sets.difference(
				fromSwagger, fromPlugin);
		throw new AssertionError(
				String.format(
						"RestPlugin %s implemented serices do not match Swagger documentation.\nServices undocumented in Swagger: %s\nServices not configured by plugin: %s",
						plugin.getClass(),
						missingFromSwagger,
						missingFromPlugin));
	}

	private Set<MethodPath> gatherSwaggerMethodPaths(
			Map<String, Path> swaggerPathMap) {
		final Set<MethodPath> methodPaths = new HashSet<>();
		for (final Map.Entry<String, Path> swaggerEntry : swaggerPathMap
				.entrySet()) {
			final String path = convertJaxRsPath(swaggerEntry.getKey());
			final Set<HttpMethod> methods = swaggerEntry.getValue()
					.getOperationMap().keySet();
			for (final HttpMethod method : methods) {
				methodPaths.add(new MethodPath(
						method.toString(),
						path));
			}

		}
		return methodPaths;
	}

	private static Set<MethodPath> gatherPluginMethodPaths(
			String pluginPathPrefix,
			RestPlugin plugin) {
		final Set<MethodPath> methodPaths = new HashSet<>();
		for (final Map.Entry<String, RouteSpecifier> specifierEntry : plugin
				.getSpecifiers().entries()) {
			final String path = convertSparkPath(specifierEntry.getKey());
			final RouteSpecifier speficier = specifierEntry.getValue();
			methodPaths.add(new MethodPath(
					speficier.getRequestType().toString(),
					pluginPathPrefix
							+ path));
		}
		return methodPaths;
	}

	private Set<MethodPath> getSwaggerMethodPathsForPlugin(
			String pluginPathPrefix, Set<MethodPath> swaggerMethodPaths) {
		final Set<MethodPath> matches = newHashSet(filter(swaggerMethodPaths,
				(MethodPath methodPath) -> methodPath.path
						.startsWith(pluginPathPrefix)));
		return matches;
	}

	private static String convertSparkPath(String sparkPath) {
		return sparkPath.replaceAll("/:[^/]+", pathVariable);
	}

	private static String convertJaxRsPath(String jaxRsPath) {
		return jaxRsPath.replaceAll("/\\{[^}]+\\}", pathVariable);
	}
}
