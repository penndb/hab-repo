/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.MediaType;

import org.junit.Before;
import org.junit.Test;
import org.mockito.AdditionalMatchers;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import spark.Request;
import spark.RequestResponseFactory;
import spark.Response;
import spark.Route;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.CharMatcher;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;

import edu.upenn.cis.db.habitat.auth.UserApiLocal;
import edu.upenn.cis.db.habitat.auth.backends.plugins.HabitatRequestScopeModule;
import edu.upenn.cis.db.habitat.auth.backends.plugins.HabitatRequestScopeModule.ScopedHabitatRequest;
import edu.upenn.cis.db.habitat.auth.backends.plugins.SimpleScope;
import edu.upenn.cis.db.habitat.core.api.UserApi;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.model.BooleanFieldModel;
import edu.upenn.cis.db.habitat.core.type.model.DoubleFieldModel;
import edu.upenn.cis.db.habitat.core.type.model.IntegerFieldModel;
import edu.upenn.cis.db.habitat.core.type.model.ProvLocationModel;
import edu.upenn.cis.db.habitat.core.type.model.ProvSpecifierFieldModel;
import edu.upenn.cis.db.habitat.core.type.model.ProvTokenFieldModel;
import edu.upenn.cis.db.habitat.core.type.model.ProvTokenModel;
import edu.upenn.cis.db.habitat.core.type.model.StoreLinkModel;
import edu.upenn.cis.db.habitat.core.type.model.StoreNodeModel;
import edu.upenn.cis.db.habitat.core.type.model.StringFieldModel;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance.NodeInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance.RankInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance.LinkInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate.LinkInfo;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate.NodeInfo;
import edu.upenn.cis.db.habitat.core.type.model.TupleWithSchemaModel;
import edu.upenn.cis.db.habitat.core.type.provdm.NodeModel;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvDmRelation;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvDmType;
import edu.upenn.cis.db.habitat.core.type.provdm.RelationModel;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.core.webservice.JsonUtil;
import edu.upenn.cis.db.habitat.core.webservice.RestPlugin;
import edu.upenn.cis.db.habitat.core.webservice.RouteSpecifier;
import edu.upenn.cis.db.habitat.core.webservice.RouteSpecifier.RequestType;
import edu.upenn.cis.db.habitat.test.TestUtil;
import edu.upenn.cis.db.habitat.webservice.routes.GetSubgraphs;
import edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants;
import edu.upenn.cis.db.habitat.webservice.routes.StoreProvenanceLink;
import edu.upenn.cis.db.habitat.webservice.routes.StoreProvenanceNode;
import edu.upenn.cis.db.habitat.webservice.routes.StoreSubgraphTemplate;
import edu.upenn.cis.db.habitat.webservice.routes.provdm.ProvDmRouteConstants;
import edu.upenn.cis.db.habitat.webservice.routes.provdm.StoreRelation;

/**
 * Do the the Route handler methods try to access path params with the correct
 * keys?
 * 
 * @author John Frommeyer
 *
 */
public class PathParamTest {

	private Map<String, RestPlugin> pluginMap;

	private Injector injector;

	private SimpleScope habitatRequestScope;

	private Random random = new Random();

	private RestPluginMapTestUtil restPluginTestUtil;

	@Before
	public void init() {
		restPluginTestUtil = new RestPluginMapTestUtil();
		pluginMap = restPluginTestUtil.pluginMap;
		injector = restPluginTestUtil.injector;
		habitatRequestScope = injector
				.getInstance(
						Key.get(
								SimpleScope.class,
								Names.named(
										HabitatRequestScopeModule.HABITAT_REQUEST_SCOPE_NAME)));
	}

	@Test
	public void sparkPathParamTest() throws Exception {
		for (final Map.Entry<String, RestPlugin> prefixPluginEntry : pluginMap
				.entrySet()) {
			final String prefix = prefixPluginEntry.getKey();
			final RestPlugin plugin = prefixPluginEntry.getValue();
			for (Map.Entry<String, RouteSpecifier> pathRouteSpecifierEntry : plugin
					.getSpecifiers().entries()) {
				final String path = prefix + pathRouteSpecifierEntry.getKey();
				final RouteSpecifier routeSpecifier = pathRouteSpecifierEntry
						.getValue();
				final RequestType method = routeSpecifier.getRequestType();
				final Route route = routeSpecifier.getRoute();
				assertPathParamsCorrect(prefix, method, path, route);
			}
		}
	}

	private void assertPathParamsCorrect(
			String pluginPrefix,
			RequestType method,
			String path,
			Route realRoute)
			throws Exception {
		final int expectedPathParamCount = CharMatcher.is(':').countIn(path);
		if (expectedPathParamCount == 0) {
			return;
		}
		final Pattern pathParamPattern = Pattern.compile("/:([^:/]+)");
		final Matcher pathParamMatcher = pathParamPattern.matcher(path);
		final Set<String> pathParamNames = new HashSet<>();
		while (pathParamMatcher.find()) {
			pathParamNames.add(pathParamMatcher.group(1));
		}

		// Testing the test
		assertEquals(expectedPathParamCount, pathParamNames.size());

		// Some services require calling user to be admin
		when(restPluginTestUtil.mockUserApiLocal.getSessionUsername())
				.thenReturn(
						UserApiLocal.Root);

		when(
				restPluginTestUtil.mockProvenanceGraphApi
						.getProvenanceLocation(
								ArgumentMatchers.anyString(),
								ArgumentMatchers
										.any(ProvToken.class)))
												.thenReturn(
														new ProvLocation(
																"PathParamTest",
																newArrayList(1,
																		2)));
		when(restPluginTestUtil.mockProvenanceGraphApi
				.getSubgraphTemplate(ArgumentMatchers.anyString())).thenReturn(
						new SubgraphTemplate(emptyList(), emptyList()));

		final HttpSession httpSession = mock(HttpSession.class);
		final HttpServletRequest rawRequest = mock(HttpServletRequest.class);
		when(rawRequest.getSession()).thenReturn(httpSession);

		when(rawRequest.getCharacterEncoding()).thenReturn(
				StandardCharsets.UTF_8.name());
		when(rawRequest.getContentType())
				.thenReturn(MediaType.APPLICATION_JSON);
		final String requestBody = getRequestBody(pluginPrefix, method.name(),
				path);
		final ServletInputStream servletInputStream = new StringServletInputStream(
				requestBody);
		when(rawRequest.getInputStream()).thenReturn(servletInputStream);

		final Request realRequest = RequestResponseFactory.create(rawRequest);
		final Request spyRequest = spy(realRequest);

		// Sometimes path param values are converted to Integers. So to make
		// sure
		// we don't get a NumberFormatException we make all the values
		// integers.
		// Good enough for now except for /provdm paths which expect prov token
		// to be a qualified name token.
		if (path.startsWith(ProvDmRouteConstants.PLUGIN_PATH + "/")) {
			when(
					spyRequest
							.params(ProvenanceRouteConstants.PROV_TOKEN_PATH_PARAM))
									.thenReturn(
											TestUtil.newQualifiedName()
													.toString());
			when(
					spyRequest
							.params(AdditionalMatchers
									.not(ArgumentMatchers
											.eq(ProvenanceRouteConstants.PROV_TOKEN_PATH_PARAM))))
													.thenReturn(
															String.valueOf(
																	random.nextInt()));
		} else {
			when(spyRequest.params(Mockito.anyString())).thenReturn(
					String.valueOf(random.nextInt()));
		}
		final HttpServletResponse rawResponse = mock(HttpServletResponse.class);

		final Response realResponse = RequestResponseFactory
				.create(rawResponse);

		new ScopedHabitatRequest(
				habitatRequestScope,
				UserApi.Root,
				() -> {
					try {
						realRoute.handle(spyRequest, realResponse);
					} catch (Exception e) {
						throw new HabitatServiceException(String.format(
								"error running %s %s", method, path), e);
					}
				}).makeRequest();

		final ArgumentCaptor<String> usedPathParamCaptor = ArgumentCaptor
				.forClass(String.class);
		// This will fail if handler gets param map with Request.params()
		// instead of looking up params by name with Request.params(name).
		verify(spyRequest, atLeastOnce()).params(
				usedPathParamCaptor.capture());
		final Set<String> usedPathParamNames = newHashSet(usedPathParamCaptor
				.getAllValues());
		if (!pathParamNames.containsAll(usedPathParamNames)) {
			final SetView<String> badNames = Sets.difference(
					usedPathParamNames, pathParamNames);
			final String message = String
					.format("Route handler implementation for path %s %s uses invalid path param names: %s",
							method,
							path,
							badNames);
			throw new AssertionError(message);
		}

	}

	private String getRequestBody(
			String pluginPrefix,
			String method,
			String path)
			throws JsonProcessingException {
		if (pluginPrefix.equals(ProvenanceRouteConstants.PLUGIN_PATH)) {
			if (method.equals(StoreProvenanceNode.REQUEST_TYPE)
					&& path.endsWith(StoreProvenanceNode.SPARK_SUBPATH)) {
				final StoreNodeModel model = new StoreNodeModel(
						new ProvLocationModel(null, "storeProvNode",
								new Integer[0]),
						new TupleWithSchemaModel(
								"mySchema",
								ImmutableList.of("my_int"),
								ImmutableList
										.of(
												new IntegerFieldModel("my_int",
														6),
												new ProvSpecifierFieldModel(
														"my_specifier",
														new ProvLocationModel(
																null,
																"storeProvNodeLocation",
																ImmutableList
																		.of(6,
																				7,
																				89)
																		.toArray(
																				new Integer[0]))),
												new DoubleFieldModel(
														"my_double",
														13.556),
												new BooleanFieldModel(
														"my_boolean",
														false),
												new StringFieldModel(
														"my_string",
														"This is a string"),
												new ProvTokenFieldModel(
														"my_token",
														new ProvTokenModel(UUID
																.randomUUID()
																.toString())))));
				final String json = JsonUtil.toJson(model);
				return json;
			} else if (method.equals(StoreProvenanceLink.REQUEST_TYPE)
					&& path.endsWith(StoreProvenanceLink.SPARK_SUBPATH)) {
				final StoreLinkModel model = new StoreLinkModel(
						singletonList(new ProvTokenModel(UUID.randomUUID()
								.toString())),
						new ProvTokenModel(UUID.randomUUID().toString()),
						"storeLinkTest");
				final String json = JsonUtil.toJson(model);
				return json;
			} else if (method.equals(GetSubgraphs.REQUEST_TYPE)
					&& path.endsWith(GetSubgraphs.SPARK_SUBPATH)) {
				final SubgraphTemplate template = new SubgraphTemplate(
						singletonList(
								ImmutableList.of(
										new NodeInfo("source",
												ProvDmType.ENTITY.name(),
												false),
										new NodeInfo("target",
												ProvDmType.ENTITY.name(),
												true))),
						singletonList(new LinkInfo(
								"source",
								"target",
								ProvDmRelation.INFLUENCE.name())));
				final String json = JsonUtil.toJson(template);
				return json;
			} else if (method.equals(GetSubgraphs.REQUEST_TYPE)
					&& path.endsWith(GetSubgraphs.SPARK_SUBPATH + "/store")) {
				final SubgraphInstance template = new SubgraphInstance(
						singletonList(
								new RankInstance(
										ImmutableList.of(
												new NodeInstance(
														"source",
														TestUtil.newTestTupleWithSchemaModel()),
												new NodeInstance(
														"target",
														TestUtil.newTestTupleWithSchemaModel())))),
						singletonList(new LinkInstance(
								"source",
								"target",
								ProvDmRelation.INFLUENCE.name(),
								TestUtil.newTestTupleWithSchemaModel())),
						0);
				final String json = JsonUtil.toJson(template);
				return json;
			} else if (method.equals(StoreSubgraphTemplate.REQUEST_TYPE)
					&& path.endsWith(StoreSubgraphTemplate.SPARK_SUBPATH)) {
				final SubgraphTemplate template = new SubgraphTemplate(
						singletonList(
								ImmutableList.of(
										new NodeInfo("source",
												ProvDmType.ENTITY.name(),
												false),
										new NodeInfo("target",
												ProvDmType.ENTITY.name(),
												true))),
						singletonList(new LinkInfo(
								"source",
								"target",
								ProvDmRelation.INFLUENCE.name())));
				final String json = JsonUtil.toJson(template);
				return json;
			}
		} else if (pluginPrefix.equals(ProvDmRouteConstants.PLUGIN_PATH)) {
			if (method.equals(HttpMethod.POST)
					&& path.endsWith(StoreRelation.SPARK_SUBPATH)) {
				final RelationModel body = RelationModel.newSpecializationOf(
						TestUtil.newQualifiedName(),
						TestUtil.newQualifiedName());
				return JsonUtil.toJson(body);
			} else if (method.equals(HttpMethod.PUT)) {
				final NodeModel body = new NodeModel(
						ProvDmType.SOFTWARE_AGENT, emptyList(), null, null,
						null);
				return JsonUtil.toJson(body);
			}
		}
		return "{}";
	}
}
