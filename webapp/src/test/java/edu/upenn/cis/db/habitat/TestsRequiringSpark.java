/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat;

import org.jooq.DSLContext;
import org.junit.ClassRule;
import org.junit.rules.ExternalResource;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.google.inject.Guice;
import com.google.inject.Injector;

import edu.upenn.cis.db.habitat.auth.AuthBrowserModule;
import edu.upenn.cis.db.habitat.auth.AuthServiceModule;
import edu.upenn.cis.db.habitat.auth.TestServices;
import edu.upenn.cis.db.habitat.auth.UserApiLocal;
import edu.upenn.cis.db.habitat.auth.backends.plugins.AuthModule;
import edu.upenn.cis.db.habitat.auth.backends.plugins.HabitatRequestScopeModule;
import edu.upenn.cis.db.habitat.auth.backends.plugins.ProtectedStorageModule;
import edu.upenn.cis.db.habitat.auth.backends.plugins.TemporaryDbModule;
import edu.upenn.cis.db.habitat.core.api.UserApi;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.provenance.ProvServiceModule;
import edu.upenn.cis.db.habitat.provenance.ProvWebServiceTest;
import edu.upenn.cis.db.habitat.provenance.provdm.ProvDmServiceModule;
import edu.upenn.cis.db.habitat.provenance.provdm.WebServiceProvDmApiTest;
import edu.upenn.cis.db.habitat.repository.prov.Neo4jBoltRule;
import edu.upenn.cis.db.habitat.repository.storage.neo4j.ConfigurableNeoStorageModule;
import edu.upenn.cis.db.habitat.repository.storage.neo4j.Neo4JStore;
import edu.upenn.cis.db.habitat.webservice.RestService;
import edu.upenn.cis.db.habitat.webservice.SparkModule;
import spark.Service;

/**
 * A test suite where we set up a Spark for the child test classes to call.
 * 
 * @author John Frommeyer
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ TestServices.class, ProvWebServiceTest.class,
		WebServiceProvDmApiTest.class })
public class TestsRequiringSpark {

	public static boolean serverIsRunning = false;
	private static UserApiLocal userApiLocalBackend;
	private static RestService restService;
	private static DSLContext dslContext;
	private static Service spark;

	@ClassRule
	public static final Neo4jBoltRule neo4j = new Neo4jBoltRule();

	@ClassRule
	public static final ExternalResource getExternalResource() {
		return new ExternalResource() {

			@Override
			protected void before() throws Throwable {
				spark = Service.ignite();
				spark.port(Config.getServerPort());
				Injector injector = Guice.createInjector(
						new HabitatRequestScopeModule(),
						new AuthModule(),
//						new AuthServiceModule(),
						new AuthBrowserModule(),
						new TemporaryDbModule(),
						new AuthServiceModule(),
						// Storage services
						new ConfigurableNeoStorageModule(neo4j.getBoltUri(),
								null, null),
						new ProtectedStorageModule(Neo4JStore.class),
						new ProvServiceModule(),
						new ProvDmServiceModule(),
						new SparkModule(spark));

				dslContext = injector.getInstance(DSLContext.class);

				restService = injector.getInstance(RestService.class);
				userApiLocalBackend = (UserApiLocal) injector
						.getInstance(UserApi.class);
				userApiLocalBackend.createTables();
				restService.call();
				restService.awaitInitialization();
				serverIsRunning = true;
			};

			@Override
			protected void after() {
				restService.stop();
				serverIsRunning = false;
			};
		};
	}

	public static UserApiLocal getUserApiLocal() {
		return userApiLocalBackend;
	}

	public static void createTables() throws HabitatServiceException {
		userApiLocalBackend.createTables();
	};

	public static void cleanDatabase() {
		neo4j.cleanDatabase();
		TemporaryDbModule.emptyDatabase(dslContext);
	}
}
