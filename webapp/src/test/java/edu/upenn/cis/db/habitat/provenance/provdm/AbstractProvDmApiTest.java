/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.provenance.provdm;

import static com.google.common.collect.Iterables.tryFind;
import static edu.upenn.cis.db.habitat.test.TestUtil.TEST_NAMESPACE;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import edu.upenn.cis.db.habitat.core.api.ProvDmApi;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.model.ProvLocationModel;
import edu.upenn.cis.db.habitat.core.type.provdm.Attribute;
import edu.upenn.cis.db.habitat.core.type.provdm.LongAttribute;
import edu.upenn.cis.db.habitat.core.type.provdm.NodeModel;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvDmConstants;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvDmRelation;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvDmType;
import edu.upenn.cis.db.habitat.core.type.provdm.QualifiedName;
import edu.upenn.cis.db.habitat.core.type.provdm.QualifiedNameAttribute;
import edu.upenn.cis.db.habitat.core.type.provdm.RelationModel;
import edu.upenn.cis.db.habitat.core.type.provdm.StringAttribute;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.provenance.ProvDmStore;
import edu.upenn.cis.db.habitat.repository.prov.Neo4jBoltRule;
import edu.upenn.cis.db.habitat.test.TestUtil;

public abstract class AbstractProvDmApiTest {

	private static final String GRAPH_RESOURCE = "provDmGraph";

	private static final Long longOne = 1L;

	protected ProvDmApi store;
	protected Neo4jBoltRule neo4j;

	@Test
	public void createGraph() throws HabitatServiceException {
		store.createProvenanceGraph(GRAPH_RESOURCE);

		boolean nullNameException = false;
		try {
			store.createProvenanceGraph(null);
		} catch (HabitatServiceException e) {
			nullNameException = true;
		}
		assertTrue(nullNameException);

		boolean emptyNameException = false;
		try {
			store.createProvenanceGraph(" \t");
		} catch (HabitatServiceException e) {
			emptyNameException = true;
		}
		assertTrue(emptyNameException);
	}

	@Test
	public void entityNoTuple() throws Exception {
		store.createProvenanceGraph(GRAPH_RESOURCE);
		final QualifiedName qualifiedName = new QualifiedName(
				TEST_NAMESPACE,
				"testEntity");
		final ProvLocationModel expectedLocation = TestUtil
				.newTestLocationModel();
		store.entity(GRAPH_RESOURCE, qualifiedName, emptyList(),
				expectedLocation);

		neo4j.runAssertionInTx((db) -> {
			final List<Node> actualNodes = neo4j.getExpectedNodesOrFail(1);
			final Node actualNode = actualNodes.get(0);
			assertEquals(qualifiedName.toString(),
					Neo4jBoltRule.getProvTokenValue(actualNode));
			final TupleWithSchema<String> actualTuple = Neo4jBoltRule
					.getTuple(actualNode);
			assertEquals(
					ProvDmType.ENTITY.name(),
					actualTuple.getValue(ProvDmConstants.PROV_DM_TYPE_QN
							.toString()));

			final ProvSpecifier actualLocation = Neo4jBoltRule
					.getLocation(actualNode);
			TestUtil.assertProvLocationsEqual(
					expectedLocation.toProvSpecifier(),
					actualLocation);
		});
	}

	@Test
	public void entity() throws Exception {
		store.createProvenanceGraph(GRAPH_RESOURCE);
		final QualifiedName provId = new QualifiedName(
				TEST_NAMESPACE,
				"testEntity");
		final ProvLocationModel expectedLocation = TestUtil
				.newTestLocationModel();
		final QualifiedName intAttrKey = new QualifiedName(
				TEST_NAMESPACE, "intAttr");
		final QualifiedName strAttrKey = new QualifiedName(
				TEST_NAMESPACE, "strAtrr");
		final List<Attribute> attributes = ImmutableList.of(
				new LongAttribute(intAttrKey, 1L),
				new StringAttribute(strAttrKey, "one"));
		store.entity(GRAPH_RESOURCE, provId, attributes, expectedLocation);

		neo4j.runAssertionInTx((db) -> {
			final List<Node> actualNodes = neo4j.getExpectedNodesOrFail(1);
			final Node actualNode = actualNodes.get(0);
			assertEquals(provId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualNode));
			final TupleWithSchema<String> actualTuple = Neo4jBoltRule
					.getTuple(actualNode);
			assertEquals(
					ProvDmType.ENTITY.name(),
					actualTuple.getValue(ProvDmConstants.PROV_DM_TYPE_QN
							.toString()));
			assertEquals(longOne,
					actualTuple.getValue(intAttrKey.toString()));
			assertEquals("one",
					actualTuple.getValue(strAttrKey.toString()));

			final ProvSpecifier actualLocation = Neo4jBoltRule
					.getLocation(actualNode);
			TestUtil.assertProvLocationsEqual(
					expectedLocation.toProvSpecifier(),
					actualLocation);
		});
	}

	@Test
	public void activity() throws IOException, Exception {
		store.createProvenanceGraph(GRAPH_RESOURCE);
		store.createProvenanceGraph(GRAPH_RESOURCE);

		final QualifiedName qualifiedName = new QualifiedName(
				TEST_NAMESPACE,
				"testActivity");
		final ProvLocationModel expectedLocation = TestUtil
				.newTestLocationModel();
		final Date startTime = new Date();
		final QualifiedName intAttrKey = new QualifiedName(
				TEST_NAMESPACE, "intAttr");
		final QualifiedName strAttrKey = new QualifiedName(
				TEST_NAMESPACE, "strAtrr");
		final List<Attribute> attributes = ImmutableList.of(
				new LongAttribute(intAttrKey, 1L),
				new StringAttribute(strAttrKey, "one"));

		store.activity(GRAPH_RESOURCE, qualifiedName, attributes, startTime,
				null, expectedLocation);

		neo4j.runAssertionInTx((db) -> {
			final List<Node> actualNodes = neo4j.getExpectedNodesOrFail(1);
			final Node actualNode = actualNodes.get(0);
			assertEquals(qualifiedName.toString(),
					Neo4jBoltRule.getProvTokenValue(actualNode));
			final TupleWithSchema<String> actualTuple = Neo4jBoltRule
					.getTuple(actualNode);
			assertEquals(
					ProvDmType.ACTIVITY.name(),
					actualTuple.getValue(ProvDmConstants.PROV_DM_TYPE_QN
							.toString()));
			assertEquals(longOne,
					actualTuple.getValue(intAttrKey.toString()));
			assertEquals("one",
					actualTuple.getValue(strAttrKey.toString()));
			assertEquals(startTime.getTime(),
					actualTuple.getValue(NodeModel.PROV_DM_START_TIME_QN
							.toString()));

			final ProvSpecifier actualLocation = Neo4jBoltRule
					.getLocation(actualNode);
			TestUtil.assertProvLocationsEqual(
					expectedLocation.toProvSpecifier(),
					actualLocation);
		});
	}

	@Test
	public void wasGeneratedBy() throws IOException, Exception {
		store.createProvenanceGraph(GRAPH_RESOURCE);
		final QualifiedName entityId = new QualifiedName(
				TEST_NAMESPACE,
				"testEntity");
		store.entity(GRAPH_RESOURCE,
				entityId,
				emptyList(), null);

		final QualifiedName activityId = new QualifiedName(
				TEST_NAMESPACE,
				"testActivity");
		store.activity(GRAPH_RESOURCE,
				activityId,
				null, null, null);

		final QualifiedName relationId = new QualifiedName(
				TEST_NAMESPACE,
				"testGeneration");
		final Date generationTime = new Date();
		final QualifiedName intAttrKey = new QualifiedName(
				TEST_NAMESPACE, "intAttr");
		final QualifiedName strAttrKey = new QualifiedName(
				TEST_NAMESPACE, "strAtrr");
		final ImmutableList<Attribute> attributes = ImmutableList
				.of(
						new LongAttribute(intAttrKey, 1L),
						new StringAttribute(strAttrKey, "one"));
		store.wasGeneratedBy(GRAPH_RESOURCE,
				entityId,
				activityId,
				relationId,
				generationTime,
				attributes);

		neo4j.runAssertionInTx((db) -> {
			final List<Relationship> actualLinks = neo4j
					.getExpectedRelationshipsOrFail(1);
			final Relationship actualLink = actualLinks.get(0);

			assertEquals(
					ProvDmRelation.GENERATION.name(),
					Neo4jBoltRule.getRelationshipValue(actualLink,
							ProvDmConstants.PROV_DM_TYPE_QN.toString(),
							String.class));
			assertEquals(
					longOne,
					Neo4jBoltRule.getRelationshipValue(actualLink, intAttrKey.toString(),
							Long.class));
			assertEquals(
					"one",
					Neo4jBoltRule.getRelationshipValue(actualLink, strAttrKey.toString(),
							String.class));
			assertEquals(
					generationTime.getTime(),
					Neo4jBoltRule.getRelationshipValue(actualLink,
							RelationModel.PROV_DM_TIME_QN.toString(),
							Long.class).longValue());
			assertEquals(
					relationId,
					Neo4jBoltRule.getRelationshipValue(actualLink,
							RelationModel.PROV_DM_RELATION_ID_QN.toString(),
							QualifiedName.class));

			final Node actualEntity = actualLink.getStartNode();
			assertEquals(entityId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualEntity));

			final Node actualActivity = actualLink.getEndNode();
			assertEquals(activityId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualActivity));
		});
	}

	@Test
	public void used() throws IOException, Exception {
		store.createProvenanceGraph(GRAPH_RESOURCE);
		final QualifiedName entityId = new QualifiedName(
				TEST_NAMESPACE,
				"testEntity");
		store.entity(GRAPH_RESOURCE,
				entityId,
				emptyList(), null);

		final QualifiedName activityId = new QualifiedName(
				TEST_NAMESPACE,
				"testActivity");
		store.activity(GRAPH_RESOURCE,
				activityId,
				null, null, null);

		final QualifiedName relationId = new QualifiedName(
				TEST_NAMESPACE,
				"testUsage");
		final Date usageTime = new Date();
		final QualifiedName intAttrKey = new QualifiedName(
				TEST_NAMESPACE, "intAttr");
		final QualifiedName strAttrKey = new QualifiedName(
				TEST_NAMESPACE, "strAtrr");
		final ImmutableList<Attribute> attributes = ImmutableList
				.of(
						new LongAttribute(intAttrKey, 1L),
						new StringAttribute(strAttrKey, "one"));
		store.used(GRAPH_RESOURCE,
				activityId,
				entityId,
				relationId,
				usageTime,
				attributes);

		neo4j.runAssertionInTx((db) -> {
			final List<Relationship> actualLinks = neo4j
					.getExpectedRelationshipsOrFail(1);
			final Relationship actualLink = actualLinks.get(0);

			assertEquals(
					ProvDmRelation.USAGE.name(),
					Neo4jBoltRule.getRelationshipValue(actualLink,
							ProvDmConstants.PROV_DM_TYPE_QN.toString(),
							String.class));
			assertEquals(
					longOne,
					Neo4jBoltRule.getRelationshipValue(actualLink, intAttrKey.toString(),
							Long.class));
			assertEquals(
					"one",
					Neo4jBoltRule.getRelationshipValue(actualLink, strAttrKey.toString(),
							String.class));
			assertEquals(
					usageTime.getTime(),
					Neo4jBoltRule.getRelationshipValue(actualLink,
							RelationModel.PROV_DM_TIME_QN.toString(),
							Long.class).longValue());
			assertEquals(
					relationId,
					Neo4jBoltRule.getRelationshipValue(actualLink,
							RelationModel.PROV_DM_RELATION_ID_QN.toString(),
							QualifiedName.class));

			final Node actualActivity = actualLink.getStartNode();
			assertEquals(activityId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualActivity));

			final Node actualEntity = actualLink.getEndNode();
			assertEquals(entityId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualEntity));
		});
	}

	@Test
	public void wasInformedBy() throws IOException, Exception {
		store.createProvenanceGraph(GRAPH_RESOURCE);
		final QualifiedName informedActivityId = new QualifiedName(
				TEST_NAMESPACE,
				"testInformedActivity");
		store.activity(GRAPH_RESOURCE,
				informedActivityId,
				null, null, null);

		final QualifiedName informantActivityId = new QualifiedName(
				TEST_NAMESPACE,
				"testInformantActivity");
		store.activity(GRAPH_RESOURCE,
				informantActivityId,
				null, null, null);

		final QualifiedName relationId = new QualifiedName(
				TEST_NAMESPACE,
				"testCommunication");
		final QualifiedName intAttrKey = new QualifiedName(
				TEST_NAMESPACE, "intAttr");
		final QualifiedName strAttrKey = new QualifiedName(
				TEST_NAMESPACE, "strAtrr");
		final ImmutableList<Attribute> attributes = ImmutableList
				.of(
						new LongAttribute(intAttrKey, 1L),
						new StringAttribute(strAttrKey, "one"));
		store.wasInformedBy(GRAPH_RESOURCE,
				informedActivityId,
				informantActivityId,
				relationId,
				attributes);

		neo4j.runAssertionInTx((db) -> {
			final List<Relationship> actualLinks = neo4j
					.getExpectedRelationshipsOrFail(1);
			final Relationship actualLink = actualLinks.get(0);

			assertEquals(
					ProvDmRelation.COMMUNICATION.name(),
					Neo4jBoltRule.getRelationshipValue(actualLink,
							ProvDmConstants.PROV_DM_TYPE_QN.toString(),
							String.class));
			assertEquals(
					longOne,
					Neo4jBoltRule.getRelationshipValue(actualLink, intAttrKey.toString(),
							Long.class));
			assertEquals(
					"one",
					Neo4jBoltRule.getRelationshipValue(actualLink, strAttrKey.toString(),
							String.class));
			assertEquals(
					relationId,
					Neo4jBoltRule.getRelationshipValue(actualLink,
							RelationModel.PROV_DM_RELATION_ID_QN.toString(),
							QualifiedName.class));

			final Node actualInformed = actualLink.getStartNode();
			assertEquals(informedActivityId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualInformed));

			final Node actualInformant = actualLink.getEndNode();
			assertEquals(informantActivityId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualInformant));
		});
	}

	@Test
	public void wasStartedBy() throws IOException, Exception {
		store.createProvenanceGraph(GRAPH_RESOURCE);
		final QualifiedName startedActivityId = new QualifiedName(
				TEST_NAMESPACE,
				"testStartedActivity");
		store.activity(GRAPH_RESOURCE,
				startedActivityId,
				null, null, null);

		final QualifiedName triggerEntityId = new QualifiedName(
				TEST_NAMESPACE,
				"testTriggerEntity");
		store.entity(GRAPH_RESOURCE,
				triggerEntityId,
				emptyList(), null);

		final QualifiedName relationId = new QualifiedName(
				TEST_NAMESPACE,
				"testStart");
		final Date time = new Date();
		final QualifiedName intAttrKey = new QualifiedName(
				TEST_NAMESPACE, "intAttr");
		final QualifiedName strAttrKey = new QualifiedName(
				TEST_NAMESPACE, "strAtrr");
		final ImmutableList<Attribute> attributes = ImmutableList
				.of(
						new LongAttribute(intAttrKey, 1L),
						new StringAttribute(strAttrKey, "one"));
		store.wasStartedBy(GRAPH_RESOURCE,
				startedActivityId,
				triggerEntityId,
				relationId,
				time,
				attributes);

		neo4j.runAssertionInTx((db) -> {
			final List<Relationship> actualLinks = neo4j
					.getExpectedRelationshipsOrFail(1);
			final Relationship actualLink = actualLinks.get(0);

			assertEquals(
					ProvDmRelation.START.name(),
					Neo4jBoltRule.getRelationshipValue(actualLink,
							ProvDmConstants.PROV_DM_TYPE_QN.toString(),
							String.class));
			assertEquals(
					longOne,
					Neo4jBoltRule.getRelationshipValue(actualLink, intAttrKey.toString(),
							Long.class));
			assertEquals(
					"one",
					Neo4jBoltRule.getRelationshipValue(actualLink, strAttrKey.toString(),
							String.class));
			assertEquals(
					time.getTime(),
					Neo4jBoltRule.getRelationshipValue(actualLink,
							RelationModel.PROV_DM_TIME_QN.toString(),
							Long.class).longValue());
			assertEquals(
					relationId,
					Neo4jBoltRule.getRelationshipValue(actualLink,
							RelationModel.PROV_DM_RELATION_ID_QN.toString(),
							QualifiedName.class));

			final Node actualStarted = actualLink.getStartNode();
			assertEquals(startedActivityId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualStarted));

			final Node actualTrigger = actualLink.getEndNode();
			assertEquals(triggerEntityId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualTrigger));
		});
	}

	@Test
	public void wasStartedByWithStarter() throws IOException, Exception {
		store.createProvenanceGraph(GRAPH_RESOURCE);
		final QualifiedName startedActivityId = new QualifiedName(
				TEST_NAMESPACE,
				"testStartedActivity");
		store.activity(GRAPH_RESOURCE,
				startedActivityId,
				null, null, null);

		final QualifiedName triggerEntityId = new QualifiedName(
				TEST_NAMESPACE,
				"testTriggerEntity");
		store.entity(GRAPH_RESOURCE,
				triggerEntityId,
				emptyList(), null);

		final QualifiedName starterActivityId = new QualifiedName(
				TEST_NAMESPACE,
				"testStarterActivity");
		store.activity(GRAPH_RESOURCE,
				starterActivityId,
				null, null, null);

		final QualifiedName relationId = new QualifiedName(
				TEST_NAMESPACE,
				"testStart");
		final Date time = new Date();
		final QualifiedName intAttrKey = new QualifiedName(
				TEST_NAMESPACE, "intAttr");
		final QualifiedName strAttrKey = new QualifiedName(
				TEST_NAMESPACE, "strAtrr");
		final ImmutableList<Attribute> attributes = ImmutableList
				.of(
						new LongAttribute(intAttrKey, 1L),
						new StringAttribute(strAttrKey, "one"));
		store.wasStartedBy(GRAPH_RESOURCE,
				startedActivityId,
				triggerEntityId,
				starterActivityId,
				relationId,
				time,
				attributes);

		neo4j.runAssertionInTx((db) -> {
			final List<Node> actualNodes = neo4j.getExpectedNodesOrFail(4);
			final Optional<Node> ternaryRelationNode = tryFind(actualNodes,
					n -> Neo4jBoltRule.getProvTokenValue(n).equals(relationId.toString()));
			assertTrue(ternaryRelationNode.isPresent());
			final TupleWithSchema<String> tuple = Neo4jBoltRule.getTuple(ternaryRelationNode
					.get());
			assertEquals(ProvDmRelation.START.name(),
					tuple.getValue(ProvDmConstants.PROV_DM_TYPE_QN.toString()));
			assertEquals(longOne,
					tuple.getValue(intAttrKey.toString()));
			assertEquals("one",
					tuple.getValue(strAttrKey.toString()));
			assertEquals(time.getTime(),
					tuple.getValue(RelationModel.PROV_DM_TIME_QN.toString()));
			assertEquals(relationId,
					tuple.getValue(RelationModel.PROV_DM_RELATION_ID_QN
							.toString()));

			final List<Relationship> actualLinks = neo4j
					.getExpectedRelationshipsOrFail(3);
			boolean subject = false;
			boolean object = false;
			boolean secondaryObject = false;
			for (final Relationship actualLink : actualLinks) {
				final Node actualLinkSource = actualLink.getStartNode();
				final String sourceId = Neo4jBoltRule.getProvTokenValue(actualLinkSource);
				final Node actualLinkTarget = actualLink.getEndNode();
				final String targetId = Neo4jBoltRule.getProvTokenValue(actualLinkTarget);

				if (sourceId.equals(startedActivityId.toString())) {
					assertEquals(relationId.toString(), targetId);
					assertEquals(
							ProvDmRelation.START.name(),
							Neo4jBoltRule.getRelationshipValue(actualLink,
									ProvDmConstants.PROV_DM_TYPE_QN.toString(),
									String.class));
					subject = true;
				} else if (targetId.equals(triggerEntityId.toString())) {
					assertEquals(relationId.toString(), sourceId);
					assertEquals(
							ProvDmRelation.START.name(),
							Neo4jBoltRule.getRelationshipValue(actualLink,
									ProvDmConstants.PROV_DM_TYPE_QN.toString(),
									String.class));
					object = true;
				} else if (targetId.equals(starterActivityId.toString())) {
					assertEquals(relationId.toString(), sourceId);
					final String label = Neo4jBoltRule.getRelationshipValue(
							actualLink,
							ProvDmConstants.PROV_DM_TYPE_QN.toString(),
							String.class);
					assertTrue(label.startsWith(ProvDmRelation.START.name()));
					secondaryObject = true;
				} else {
					assertTrue(false);
				}
			}
			assertTrue(subject && object && secondaryObject);
		});
	}

	@Test
	public void wasEndedBy() throws IOException, Exception {
		store.createProvenanceGraph(GRAPH_RESOURCE);
		final QualifiedName endedActivityId = new QualifiedName(
				TEST_NAMESPACE,
				"testEndedActivity");
		store.activity(GRAPH_RESOURCE,
				endedActivityId,
				null, null, null);

		final QualifiedName triggerEntityId = new QualifiedName(
				TEST_NAMESPACE,
				"testTriggerEntity");
		store.entity(GRAPH_RESOURCE,
				triggerEntityId,
				emptyList(), null);

		final QualifiedName relationId = new QualifiedName(
				TEST_NAMESPACE,
				"testEnd");
		final Date time = new Date();
		final QualifiedName intAttrKey = new QualifiedName(
				TEST_NAMESPACE, "intAttr");
		final QualifiedName strAttrKey = new QualifiedName(
				TEST_NAMESPACE, "strAtrr");
		final ImmutableList<Attribute> attributes = ImmutableList
				.of(
						new LongAttribute(intAttrKey, 1L),
						new StringAttribute(strAttrKey, "one"));
		store.wasEndedBy(GRAPH_RESOURCE,
				endedActivityId,
				triggerEntityId,
				relationId,
				time,
				attributes);

		neo4j.runAssertionInTx((db) -> {
			final List<Relationship> actualLinks = neo4j
					.getExpectedRelationshipsOrFail(1);
			final Relationship actualLink = actualLinks.get(0);

			assertEquals(
					ProvDmRelation.END.name(),
					Neo4jBoltRule.getRelationshipValue(actualLink,
							ProvDmConstants.PROV_DM_TYPE_QN.toString(),
							String.class));
			assertEquals(
					longOne,
					Neo4jBoltRule.getRelationshipValue(actualLink, intAttrKey.toString(),
							Long.class));
			assertEquals(
					"one",
					Neo4jBoltRule.getRelationshipValue(actualLink, strAttrKey.toString(),
							String.class));
			assertEquals(
					time.getTime(),
					Neo4jBoltRule.getRelationshipValue(actualLink,
							RelationModel.PROV_DM_TIME_QN.toString(),
							Long.class).longValue());
			assertEquals(
					relationId,
					Neo4jBoltRule.getRelationshipValue(actualLink,
							RelationModel.PROV_DM_RELATION_ID_QN.toString(),
							QualifiedName.class));

			final Node actualStarted = actualLink.getStartNode();
			assertEquals(endedActivityId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualStarted));

			final Node actualTrigger = actualLink.getEndNode();
			assertEquals(triggerEntityId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualTrigger));
		});
	}

	@Test
	public void wasEndedByWithEnder() throws IOException, Exception {
		final QualifiedName endedActivityId = new QualifiedName(
				TEST_NAMESPACE,
				"testEndedActivity");
		store.createProvenanceGraph(GRAPH_RESOURCE);
		store.activity(GRAPH_RESOURCE,
				endedActivityId,
				null, null, null);

		final QualifiedName triggerEntityId = new QualifiedName(
				TEST_NAMESPACE,
				"testTriggerEntity");
		store.entity(GRAPH_RESOURCE,
				triggerEntityId,
				emptyList(),
				null);

		final QualifiedName enderActivityId = new QualifiedName(
				TEST_NAMESPACE,
				"testEnderActivity");
		store.activity(GRAPH_RESOURCE,
				enderActivityId,
				null, null, null);

		final QualifiedName relationId = new QualifiedName(
				TEST_NAMESPACE,
				"testEnd");
		final Date time = new Date();
		final QualifiedName intAttrKey = new QualifiedName(
				TEST_NAMESPACE, "intAttr");
		final QualifiedName strAttrKey = new QualifiedName(
				TEST_NAMESPACE, "strAtrr");
		final ImmutableList<Attribute> attributes = ImmutableList
				.of(
						new LongAttribute(intAttrKey, longOne),
						new StringAttribute(strAttrKey,
								"one"));
		store.wasEndedBy(GRAPH_RESOURCE,
				endedActivityId,
				triggerEntityId,
				enderActivityId,
				relationId,
				time,
				attributes);

		neo4j.runAssertionInTx((db) -> {
			final List<Node> actualNodes = neo4j.getExpectedNodesOrFail(4);
			final Optional<Node> ternaryRelationNode = tryFind(actualNodes,
					n -> Neo4jBoltRule.getProvTokenValue(n).equals(relationId.toString()));
			assertTrue(ternaryRelationNode.isPresent());
			final TupleWithSchema<String> tuple = Neo4jBoltRule.getTuple(ternaryRelationNode
					.get());
			assertEquals(ProvDmRelation.END.name(),
					tuple.getValue(ProvDmConstants.PROV_DM_TYPE_QN.toString()));
			assertEquals(longOne,
					tuple.getValue(intAttrKey.toString()));
			assertEquals("one",
					tuple.getValue(strAttrKey.toString()));
			assertEquals(time.getTime(),
					tuple.getValue(RelationModel.PROV_DM_TIME_QN.toString()));
			assertEquals(relationId,
					tuple.getValue(RelationModel.PROV_DM_RELATION_ID_QN
							.toString()));

			final List<Relationship> actualLinks = neo4j
					.getExpectedRelationshipsOrFail(3);
			boolean subject = false;
			boolean object = false;
			boolean secondaryObject = false;
			for (final Relationship actualLink : actualLinks) {
				final Node actualLinkSource = actualLink.getStartNode();
				final String sourceId = Neo4jBoltRule.getProvTokenValue(actualLinkSource);
				final Node actualLinkTarget = actualLink.getEndNode();
				final String targetId = Neo4jBoltRule.getProvTokenValue(actualLinkTarget);

				if (sourceId.equals(endedActivityId.toString())) {
					assertEquals(relationId.toString(), targetId);
					assertEquals(
							ProvDmRelation.END.name(),
							Neo4jBoltRule.getRelationshipValue(actualLink,
									ProvDmConstants.PROV_DM_TYPE_QN.toString(),
									String.class));
					subject = true;
				} else if (targetId.equals(triggerEntityId.toString())) {
					assertEquals(relationId.toString(), sourceId);
					assertEquals(
							ProvDmRelation.END.name(),
							Neo4jBoltRule.getRelationshipValue(actualLink,
									ProvDmConstants.PROV_DM_TYPE_QN.toString(),
									String.class));
					object = true;
				} else if (targetId.equals(enderActivityId.toString())) {
					assertEquals(relationId.toString(), sourceId);
					final String label = Neo4jBoltRule.getRelationshipValue(
							actualLink,
							ProvDmConstants.PROV_DM_TYPE_QN.toString(),
							String.class);
					assertTrue(label.startsWith(ProvDmRelation.END.name()));
					secondaryObject = true;
				} else {
					assertTrue(false);
				}
			}
			assertTrue(subject && object && secondaryObject);
		});
	}

	@Test
	public void wasInvalidatedBy() throws IOException, Exception {
		store.createProvenanceGraph(GRAPH_RESOURCE);
		final QualifiedName subjectId = new QualifiedName(
				TEST_NAMESPACE,
				"testSubject");
		store.entity(GRAPH_RESOURCE,
				subjectId,
				emptyList(), null);

		final QualifiedName objectId = new QualifiedName(
				TEST_NAMESPACE,
				"testObject");
		store.activity(GRAPH_RESOURCE,
				objectId,
				null, null, null);

		final QualifiedName relationId = new QualifiedName(
				TEST_NAMESPACE,
				"testInvalidation");
		final Date time = new Date();
		final QualifiedName intAttrKey = new QualifiedName(
				TEST_NAMESPACE, "intAttr");
		final QualifiedName strAttrKey = new QualifiedName(
				TEST_NAMESPACE, "strAtrr");
		final ImmutableList<Attribute> attributes = ImmutableList
				.of(
						new LongAttribute(intAttrKey, 1L),
						new StringAttribute(strAttrKey, "one"));
		store.wasInvalidatedBy(GRAPH_RESOURCE,
				subjectId,
				objectId,
				relationId,
				time,
				attributes);

		neo4j.runAssertionInTx((db) -> {
			final List<Relationship> actualLinks = neo4j
					.getExpectedRelationshipsOrFail(1);
			final Relationship actualLink = actualLinks.get(0);

			assertEquals(
					ProvDmRelation.INVALIDATION.name(),
					Neo4jBoltRule.getRelationshipValue(actualLink,
							ProvDmConstants.PROV_DM_TYPE_QN.toString(),
							String.class));
			assertEquals(
					longOne,
					Neo4jBoltRule.getRelationshipValue(actualLink, intAttrKey.toString(),
							Long.class));
			assertEquals(
					"one",
					Neo4jBoltRule.getRelationshipValue(actualLink, strAttrKey.toString(),
							String.class));
			assertEquals(
					time.getTime(),
					Neo4jBoltRule.getRelationshipValue(actualLink,
							RelationModel.PROV_DM_TIME_QN.toString(),
							Long.class).longValue());
			assertEquals(
					relationId,
					Neo4jBoltRule.getRelationshipValue(actualLink,
							RelationModel.PROV_DM_RELATION_ID_QN.toString(),
							QualifiedName.class));

			final Node actualSubject = actualLink.getStartNode();
			assertEquals(subjectId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualSubject));

			final Node actualObject = actualLink.getEndNode();
			assertEquals(objectId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualObject));
		});
	}

	@Test
	public void wasDerivedFrom() throws IOException, Exception {
		store.createProvenanceGraph(GRAPH_RESOURCE);
		final QualifiedName subjectId = new QualifiedName(
				TEST_NAMESPACE,
				"testSubject");
		store.entity(GRAPH_RESOURCE,
				subjectId,
				emptyList(), null);

		final QualifiedName objectId = new QualifiedName(
				TEST_NAMESPACE,
				"testObject");
		store.entity(GRAPH_RESOURCE,
				objectId,
				emptyList(), null);

		final QualifiedName relationId = new QualifiedName(
				TEST_NAMESPACE,
				"testDerivation");
		final QualifiedName generationId = new QualifiedName(
				TEST_NAMESPACE,
				"testGeneration");
		final QualifiedName usageId = new QualifiedName(
				TEST_NAMESPACE,
				"testUsage");

		final QualifiedName intAttrKey = new QualifiedName(
				TEST_NAMESPACE, "intAttr");
		final QualifiedName strAttrKey = new QualifiedName(
				TEST_NAMESPACE, "strAtrr");
		final ImmutableList<Attribute> attributes = ImmutableList
				.of(
						new LongAttribute(intAttrKey, 1L),
						new StringAttribute(strAttrKey, "one"));
		store.wasDerivedFrom(GRAPH_RESOURCE,
				subjectId,
				objectId,
				relationId,
				generationId,
				usageId,
				attributes);

		neo4j.runAssertionInTx((db) -> {
			final List<Relationship> actualLinks = neo4j
					.getExpectedRelationshipsOrFail(1);
			final Relationship actualLink = actualLinks.get(0);

			assertEquals(
					ProvDmRelation.DERIVATION.name(),
					Neo4jBoltRule.getRelationshipValue(actualLink,
							ProvDmStore.PROV_DM_TYPE_KEY, String.class));
			assertEquals(
					longOne,
					Neo4jBoltRule.getRelationshipValue(actualLink, intAttrKey.toString(),
							Long.class));
			assertEquals(
					"one",
					Neo4jBoltRule.getRelationshipValue(actualLink, strAttrKey.toString(),
							String.class));
			assertEquals(
					relationId,
					Neo4jBoltRule.getRelationshipValue(actualLink,
							ProvDmStore.PROV_DM_RELATION_ID_KEY,
							QualifiedName.class));
			assertEquals(
					generationId,
					Neo4jBoltRule.getRelationshipValue(actualLink,
							ProvDmStore.PROV_DM_GENERATION_ID_KEY,
							QualifiedName.class));
			assertEquals(
					usageId,
					Neo4jBoltRule.getRelationshipValue(actualLink,
							ProvDmStore.PROV_DM_USAGE_ID_KEY,
							QualifiedName.class));

			final Node actualSubject = actualLink.getStartNode();
			assertEquals(subjectId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualSubject));

			final Node actualObject = actualLink.getEndNode();
			assertEquals(objectId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualObject));
		});
	}

	@Test
	public void wasDerivedFromWithActivity() throws IOException, Exception {
		store.createProvenanceGraph(GRAPH_RESOURCE);
		final QualifiedName subjectId = new QualifiedName(
				TEST_NAMESPACE,
				"testSubject");
		store.entity(GRAPH_RESOURCE,
				subjectId,
				emptyList(), null);

		final QualifiedName objectId = new QualifiedName(
				TEST_NAMESPACE,
				"testObject");
		store.entity(GRAPH_RESOURCE,
				objectId,
				emptyList(), null);

		final QualifiedName secondaryObjectId = new QualifiedName(
				TEST_NAMESPACE,
				"testSecondaryObject");
		store.activity(GRAPH_RESOURCE,
				secondaryObjectId,
				null, null, null);

		final QualifiedName relationId = new QualifiedName(
				TEST_NAMESPACE,
				"testDerivation");
		final QualifiedName generationId = new QualifiedName(
				TEST_NAMESPACE,
				"testGeneration");
		final QualifiedName usageId = new QualifiedName(
				TEST_NAMESPACE,
				"testUsage");

		final QualifiedName intAttrKey = new QualifiedName(
				TEST_NAMESPACE, "intAttr");
		final QualifiedName strAttrKey = new QualifiedName(
				TEST_NAMESPACE, "strAtrr");
		final ImmutableList<Attribute> attributes = ImmutableList
				.of(
						new LongAttribute(intAttrKey, 1L),
						new StringAttribute(strAttrKey, "one"));
		store.wasDerivedFrom(GRAPH_RESOURCE,
				subjectId,
				objectId,
				secondaryObjectId,
				relationId,
				generationId,
				usageId,
				attributes);

		neo4j.runAssertionInTx((db) -> {
			final List<Node> actualNodes = neo4j.getExpectedNodesOrFail(4);
			final Optional<Node> ternaryRelationNode = tryFind(actualNodes,
					n -> Neo4jBoltRule.getProvTokenValue(n).equals(relationId.toString()));
			assertTrue(ternaryRelationNode.isPresent());
			final TupleWithSchema<String> tuple = Neo4jBoltRule.getTuple(ternaryRelationNode
					.get());

			assertEquals(ProvDmRelation.DERIVATION.name(),
					tuple.getValue(ProvDmStore.PROV_DM_TYPE_KEY));
			assertEquals(longOne,
					tuple.getValue(intAttrKey.toString()));
			assertEquals("one",
					tuple.getValue(strAttrKey.toString()));
			assertEquals(
					relationId,
					tuple.getValue(ProvDmStore.PROV_DM_RELATION_ID_KEY));
			assertEquals(
					generationId,
					tuple.getValue(
							ProvDmStore.PROV_DM_GENERATION_ID_KEY));
			assertEquals(
					usageId,
					tuple.getValue(
							ProvDmStore.PROV_DM_USAGE_ID_KEY));

			final List<Relationship> actualLinks = neo4j
					.getExpectedRelationshipsOrFail(3);
			boolean subject = false;
			boolean object = false;
			boolean secondaryObject = false;
			for (final Relationship actualLink : actualLinks) {
				final Node actualLinkSource = actualLink.getStartNode();
				final String sourceId = Neo4jBoltRule.getProvTokenValue(actualLinkSource);
				final Node actualLinkTarget = actualLink.getEndNode();
				final String targetId = Neo4jBoltRule.getProvTokenValue(actualLinkTarget);

				if (sourceId.equals(subjectId.toString())) {
					assertEquals(relationId.toString(), targetId);
					assertEquals(
							ProvDmRelation.DERIVATION.name(),
							Neo4jBoltRule.getRelationshipValue(actualLink,
									ProvDmConstants.PROV_DM_TYPE_QN.toString(),
									String.class));
					subject = true;
				} else if (targetId.equals(objectId.toString())) {
					assertEquals(relationId.toString(), sourceId);
					assertEquals(
							ProvDmRelation.DERIVATION.name(),
							Neo4jBoltRule.getRelationshipValue(actualLink,
									ProvDmConstants.PROV_DM_TYPE_QN.toString(),
									String.class));
					object = true;
				} else if (targetId.equals(secondaryObjectId.toString())) {
					assertEquals(relationId.toString(), sourceId);
					final String label = Neo4jBoltRule.getRelationshipValue(
							actualLink,
							ProvDmConstants.PROV_DM_TYPE_QN.toString(),
							String.class);
					assertTrue(label.startsWith(ProvDmRelation.DERIVATION
							.name()));
					secondaryObject = true;
				} else {
					assertTrue(false);
				}
			}
			assertTrue(subject && object && secondaryObject);
		});
	}

	@Test
	public void revision() throws IOException, Exception {
		store.createProvenanceGraph(GRAPH_RESOURCE);
		final QualifiedName subjectId = new QualifiedName(
				TEST_NAMESPACE,
				"testSubject");
		store.entity(GRAPH_RESOURCE,
				subjectId,
				emptyList(), null);

		final QualifiedName objectId = new QualifiedName(
				TEST_NAMESPACE,
				"testObject");
		store.entity(GRAPH_RESOURCE,
				objectId,
				emptyList(), null);

		final QualifiedName relationId = new QualifiedName(
				TEST_NAMESPACE,
				"testDerivation");
		final QualifiedName generationId = new QualifiedName(
				TEST_NAMESPACE,
				"testGeneration");
		final QualifiedName usageId = new QualifiedName(
				TEST_NAMESPACE,
				"testUsage");

		final QualifiedName intAttrKey = new QualifiedName(
				TEST_NAMESPACE, "intAttr");
		final QualifiedName strAttrKey = new QualifiedName(
				TEST_NAMESPACE, "strAtrr");
		final ImmutableList<Attribute> attributes = ImmutableList
				.of(
						new LongAttribute(intAttrKey, 1L),
						new StringAttribute(strAttrKey, "one"),
						new QualifiedNameAttribute(
								ProvDmConstants.TYPE_ATTRIBUTE,
								ProvDmConstants.REVISION));
		store.wasDerivedFrom(GRAPH_RESOURCE,
				subjectId,
				objectId,
				relationId,
				generationId,
				usageId,
				attributes);

		neo4j.runAssertionInTx((db) -> {
			final List<Relationship> actualLinks = neo4j
					.getExpectedRelationshipsOrFail(1);
			final Relationship actualLink = actualLinks.get(0);

			assertEquals(
					ProvDmRelation.DERIVATION.name(),
					Neo4jBoltRule.getRelationshipValue(actualLink,
							ProvDmStore.PROV_DM_TYPE_KEY, String.class));
			assertEquals(
					longOne,
					Neo4jBoltRule.getRelationshipValue(actualLink, intAttrKey.toString(),
							Long.class));
			assertEquals(
					"one",
					Neo4jBoltRule.getRelationshipValue(actualLink, strAttrKey.toString(),
							String.class));
			assertEquals(
					ProvDmConstants.REVISION,
					Neo4jBoltRule.getRelationshipValue(actualLink,
							ProvDmConstants.TYPE_ATTRIBUTE.toString(),
							QualifiedName.class));
			assertEquals(
					relationId,
					Neo4jBoltRule.getRelationshipValue(actualLink,
							ProvDmStore.PROV_DM_RELATION_ID_KEY,
							QualifiedName.class));
			assertEquals(
					generationId,
					Neo4jBoltRule.getRelationshipValue(actualLink,
							ProvDmStore.PROV_DM_GENERATION_ID_KEY,
							QualifiedName.class));
			assertEquals(
					usageId,
					Neo4jBoltRule.getRelationshipValue(actualLink,
							ProvDmStore.PROV_DM_USAGE_ID_KEY,
							QualifiedName.class));

			final Node actualSubject = actualLink.getStartNode();
			assertEquals(subjectId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualSubject));

			final Node actualObject = actualLink.getEndNode();
			assertEquals(objectId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualObject));
		});
	}

	@Test
	public void agent() throws Exception {
		store.createProvenanceGraph(GRAPH_RESOURCE);
		final QualifiedName qualifiedName = new QualifiedName(
				TEST_NAMESPACE,
				"testAgent");
		final ProvLocationModel expectedLocation = TestUtil
				.newTestLocationModel();
		final QualifiedName intAttrKey = new QualifiedName(
				TEST_NAMESPACE, "intAttr");
		final QualifiedName strAttrKey = new QualifiedName(
				TEST_NAMESPACE, "strAtrr");
		final List<Attribute> attributes = ImmutableList.of(
				new LongAttribute(intAttrKey, 1L),
				new StringAttribute(strAttrKey, "one"));
		store.agent(GRAPH_RESOURCE, qualifiedName, attributes, expectedLocation);

		neo4j.runAssertionInTx((db) -> {
			final List<Node> actualNodes = neo4j.getExpectedNodesOrFail(1);
			final Node actualNode = actualNodes.get(0);
			assertEquals(qualifiedName.toString(),
					Neo4jBoltRule.getProvTokenValue(actualNode));
			final TupleWithSchema<String> actualTuple = Neo4jBoltRule.getTuple(actualNode);
			assertEquals(
					ProvDmType.AGENT.name(),
					actualTuple.getValue(ProvDmConstants.PROV_DM_TYPE_QN
							.toString()));
			assertEquals(longOne,
					actualTuple.getValue(intAttrKey.toString()));
			assertEquals("one",
					actualTuple.getValue(strAttrKey.toString()));

			final ProvSpecifier actualLocation = Neo4jBoltRule
					.getLocation(actualNode);
			TestUtil.assertProvLocationsEqual(
					expectedLocation.toProvSpecifier(),
					actualLocation);
		});
	}

	@Test
	public void softwareAgent() throws Exception {
		store.createProvenanceGraph(GRAPH_RESOURCE);
		final QualifiedName qualifiedName = new QualifiedName(
				TEST_NAMESPACE,
				"testSoftwareAgent");
		final ProvLocationModel expectedLocation = TestUtil
				.newTestLocationModel();

		store.agent(GRAPH_RESOURCE, qualifiedName,
				singletonList(new QualifiedNameAttribute(
						ProvDmConstants.TYPE_ATTRIBUTE,
						ProvDmConstants.SOFTWARE_AGENT)),
				expectedLocation);

		neo4j.runAssertionInTx((db) -> {
			final List<Node> actualNodes = neo4j.getExpectedNodesOrFail(1);
			final Node actualNode = actualNodes.get(0);
			assertEquals(qualifiedName.toString(),
					Neo4jBoltRule.getProvTokenValue(actualNode));
			final TupleWithSchema<String> actualTuple = Neo4jBoltRule
					.getTuple(actualNode);
			assertEquals(
					ProvDmType.AGENT.name(),
					actualTuple.getValue(ProvDmConstants.PROV_DM_TYPE_QN
							.toString()));
			assertEquals(ProvDmConstants.SOFTWARE_AGENT,
					actualTuple.getValue(ProvDmConstants.TYPE_ATTRIBUTE
							.toString()));

			final ProvSpecifier actualLocation = Neo4jBoltRule
					.getLocation(actualNode);
			TestUtil.assertProvLocationsEqual(
					expectedLocation.toProvSpecifier(),
					actualLocation);
		});
	}

	@Test
	public void wasAttributedTo() throws IOException, Exception {
		store.createProvenanceGraph(GRAPH_RESOURCE);
		final QualifiedName subjectId = new QualifiedName(
				TEST_NAMESPACE,
				"testSubject");
		store.entity(GRAPH_RESOURCE,
				subjectId,
				emptyList(), null);

		final QualifiedName objectId = new QualifiedName(
				TEST_NAMESPACE,
				"testObject");
		store.agent(GRAPH_RESOURCE,
				objectId,
				null);

		final QualifiedName relationId = new QualifiedName(
				TEST_NAMESPACE,
				"testAttribution");

		final QualifiedName intAttrKey = new QualifiedName(
				TEST_NAMESPACE, "intAttr");
		final QualifiedName strAttrKey = new QualifiedName(
				TEST_NAMESPACE, "strAtrr");
		final ImmutableList<Attribute> attributes = ImmutableList
				.of(
						new LongAttribute(intAttrKey, 1L),
						new StringAttribute(strAttrKey, "one"));
		store.wasAttributedTo(GRAPH_RESOURCE,
				subjectId,
				objectId,
				relationId,
				attributes);

		neo4j.runAssertionInTx((db) -> {
			final List<Relationship> actualLinks = neo4j
					.getExpectedRelationshipsOrFail(1);
			final Relationship actualLink = actualLinks.get(0);

			assertEquals(
					ProvDmRelation.ATTRIBUTION.name(),
					Neo4jBoltRule.getRelationshipValue(actualLink,
							ProvDmStore.PROV_DM_TYPE_KEY, String.class));
			assertEquals(
					longOne,
					Neo4jBoltRule.getRelationshipValue(actualLink, intAttrKey.toString(),
							Long.class));
			assertEquals(
					"one",
					Neo4jBoltRule.getRelationshipValue(actualLink, strAttrKey.toString(),
							String.class));
			assertEquals(
					relationId,
					Neo4jBoltRule.getRelationshipValue(actualLink,
							ProvDmStore.PROV_DM_RELATION_ID_KEY,
							QualifiedName.class));

			final Node actualSubject = actualLink.getStartNode();
			assertEquals(subjectId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualSubject));

			final Node actualObject = actualLink.getEndNode();
			assertEquals(objectId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualObject));
		});
	}

	@Test
	public void wasAssociatedWith() throws IOException, Exception {
		store.createProvenanceGraph(GRAPH_RESOURCE);
		final QualifiedName subjectId = new QualifiedName(
				TEST_NAMESPACE,
				"testSubject");
		store.activity(GRAPH_RESOURCE,
				subjectId,
				null, null, null);

		final QualifiedName objectId = new QualifiedName(
				TEST_NAMESPACE,
				"testObject");
		store.agent(GRAPH_RESOURCE,
				objectId,
				null);

		final QualifiedName relationId = new QualifiedName(
				TEST_NAMESPACE,
				"testAssociation");

		final QualifiedName intAttrKey = new QualifiedName(
				TEST_NAMESPACE, "intAttr");
		final QualifiedName strAttrKey = new QualifiedName(
				TEST_NAMESPACE, "strAtrr");
		final ImmutableList<Attribute> attributes = ImmutableList
				.of(
						new LongAttribute(intAttrKey, 1L),
						new StringAttribute(strAttrKey, "one"));
		store.wasAssociatedWith(GRAPH_RESOURCE,
				subjectId,
				objectId,
				relationId,
				attributes);

		neo4j.runAssertionInTx((db) -> {
			final List<Relationship> actualLinks = neo4j
					.getExpectedRelationshipsOrFail(1);
			final Relationship actualLink = actualLinks.get(0);

			assertEquals(
					ProvDmRelation.ASSOCIATION.name(),
					Neo4jBoltRule.getRelationshipValue(actualLink,
							ProvDmStore.PROV_DM_TYPE_KEY, String.class));
			assertEquals(
					longOne,
					Neo4jBoltRule.getRelationshipValue(actualLink, intAttrKey.toString(),
							Long.class));
			assertEquals(
					"one",
					Neo4jBoltRule.getRelationshipValue(actualLink, strAttrKey.toString(),
							String.class));
			assertEquals(
					relationId,
					Neo4jBoltRule.getRelationshipValue(actualLink,
							ProvDmStore.PROV_DM_RELATION_ID_KEY,
							QualifiedName.class));

			final Node actualSubject = actualLink.getStartNode();
			assertEquals(subjectId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualSubject));

			final Node actualObject = actualLink.getEndNode();
			assertEquals(objectId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualObject));
		});
	}

	@Test
	public void wasAssociatedWithWithPlan() throws IOException, Exception {
		store.createProvenanceGraph(GRAPH_RESOURCE);
		final QualifiedName subjectId = new QualifiedName(
				TEST_NAMESPACE,
				"testSubject");
		store.activity(GRAPH_RESOURCE,
				subjectId,
				null, null, null);

		final QualifiedName objectId = new QualifiedName(
				TEST_NAMESPACE,
				"testObject");
		store.agent(GRAPH_RESOURCE,
				objectId,
				null);

		final QualifiedName secondaryObjectId = new QualifiedName(
				TEST_NAMESPACE,
				"testSecondaryObject");
		final List<Attribute> planAttributes = ImmutableList
				.of(new QualifiedNameAttribute(
						ProvDmConstants.TYPE_ATTRIBUTE,
						ProvDmConstants.PLAN));

		store.entity(GRAPH_RESOURCE,
				secondaryObjectId,
				planAttributes, null);

		final QualifiedName relationId = new QualifiedName(
				TEST_NAMESPACE,
				"testAssociationWithPlan");

		final QualifiedName intAttrKey = new QualifiedName(
				TEST_NAMESPACE, "intAttr");
		final QualifiedName strAttrKey = new QualifiedName(
				TEST_NAMESPACE, "strAtrr");
		final ImmutableList<Attribute> attributes = ImmutableList
				.of(
						new LongAttribute(intAttrKey, 1L),
						new StringAttribute(strAttrKey, "one"));
		store.wasAssociatedWith(GRAPH_RESOURCE,
				subjectId,
				objectId,
				secondaryObjectId,
				relationId,
				attributes);

		neo4j.runAssertionInTx((db) -> {
			final List<Node> actualNodes = neo4j.getExpectedNodesOrFail(4);
			final Optional<Node> ternaryRelationNode = tryFind(actualNodes,
					n -> Neo4jBoltRule.getProvTokenValue(n).equals(relationId.toString()));
			assertTrue(ternaryRelationNode.isPresent());
			final TupleWithSchema<String> tuple = Neo4jBoltRule.getTuple(ternaryRelationNode
					.get());

			assertEquals(ProvDmRelation.ASSOCIATION.name(),
					tuple.getValue(ProvDmStore.PROV_DM_TYPE_KEY));
			assertEquals(longOne,
					tuple.getValue(intAttrKey.toString()));
			assertEquals("one",
					tuple.getValue(strAttrKey.toString()));
			assertEquals(
					relationId,
					tuple.getValue(ProvDmStore.PROV_DM_RELATION_ID_KEY));

			final List<Relationship> actualLinks = neo4j
					.getExpectedRelationshipsOrFail(3);
			boolean subject = false;
			boolean object = false;
			boolean secondaryObject = false;
			for (final Relationship actualLink : actualLinks) {
				final Node actualLinkSource = actualLink.getStartNode();
				final String sourceId = Neo4jBoltRule.getProvTokenValue(actualLinkSource);
				final Node actualLinkTarget = actualLink.getEndNode();
				final String targetId = Neo4jBoltRule.getProvTokenValue(actualLinkTarget);

				if (sourceId.equals(subjectId.toString())) {
					assertEquals(relationId.toString(), targetId);
					assertEquals(
							ProvDmRelation.ASSOCIATION.name(),
							Neo4jBoltRule.getRelationshipValue(actualLink,
									ProvDmConstants.PROV_DM_TYPE_QN.toString(),
									String.class));
					subject = true;
				} else if (targetId.equals(objectId.toString())) {
					assertEquals(relationId.toString(), sourceId);
					assertEquals(
							ProvDmRelation.ASSOCIATION.name(),
							Neo4jBoltRule.getRelationshipValue(actualLink,
									ProvDmConstants.PROV_DM_TYPE_QN.toString(),
									String.class));
					object = true;
				} else if (targetId.equals(secondaryObjectId.toString())) {
					assertEquals(relationId.toString(), sourceId);
					final String label = Neo4jBoltRule.getRelationshipValue(
							actualLink,
							ProvDmConstants.PROV_DM_TYPE_QN.toString(),
							String.class);
					assertTrue(label.startsWith(ProvDmRelation.ASSOCIATION
							.name()));
					secondaryObject = true;
				} else {
					assertTrue(false);
				}
			}
			assertTrue(subject && object && secondaryObject);
		});
	}

	@Test
	public void actedOnBehalfOf() throws IOException, Exception {
		store.createProvenanceGraph(GRAPH_RESOURCE);
		final QualifiedName subjectId = new QualifiedName(
				TEST_NAMESPACE,
				"testSubject");
		store.agent(GRAPH_RESOURCE,
				subjectId,
				null);

		final QualifiedName objectId = new QualifiedName(
				TEST_NAMESPACE,
				"testObject");
		store.agent(GRAPH_RESOURCE,
				objectId,
				null);

		final QualifiedName relationId = new QualifiedName(
				TEST_NAMESPACE,
				"testDelegation");

		final QualifiedName intAttrKey = new QualifiedName(
				TEST_NAMESPACE, "intAttr");
		final QualifiedName strAttrKey = new QualifiedName(
				TEST_NAMESPACE, "strAtrr");
		final ImmutableList<Attribute> attributes = ImmutableList
				.of(
						new LongAttribute(intAttrKey, 1L),
						new StringAttribute(strAttrKey, "one"));
		store.actedOnBehalfOf(GRAPH_RESOURCE,
				subjectId,
				objectId,
				relationId,
				attributes);

		neo4j.runAssertionInTx((db) -> {
			final List<Relationship> actualLinks = neo4j
					.getExpectedRelationshipsOrFail(1);
			final Relationship actualLink = actualLinks.get(0);

			assertEquals(
					ProvDmRelation.DELEGATION.name(),
					Neo4jBoltRule.getRelationshipValue(actualLink,
							ProvDmStore.PROV_DM_TYPE_KEY,
							String.class));
			assertEquals(
					longOne,
					Neo4jBoltRule.getRelationshipValue(actualLink, intAttrKey.toString(),
							Long.class));
			assertEquals(
					"one",
					Neo4jBoltRule.getRelationshipValue(actualLink, strAttrKey.toString(),
							String.class));
			assertEquals(
					relationId,
					Neo4jBoltRule.getRelationshipValue(actualLink,
							ProvDmStore.PROV_DM_RELATION_ID_KEY,
							QualifiedName.class));

			final Node actualSubject = actualLink.getStartNode();
			assertEquals(subjectId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualSubject));

			final Node actualObject = actualLink.getEndNode();
			assertEquals(objectId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualObject));
		});
	}

	@Test
	public void actedOnBehalfOfWithActivity() throws IOException, Exception {
		store.createProvenanceGraph(GRAPH_RESOURCE);
		final QualifiedName subjectId = new QualifiedName(
				TEST_NAMESPACE,
				"testSubject");
		store.agent(GRAPH_RESOURCE,
				subjectId,
				null);

		final QualifiedName objectId = new QualifiedName(
				TEST_NAMESPACE,
				"testObject");
		store.agent(GRAPH_RESOURCE,
				objectId,
				null);

		final QualifiedName secondaryObjectId = new QualifiedName(
				TEST_NAMESPACE,
				"testSecondaryObject");
		store.activity(GRAPH_RESOURCE,
				secondaryObjectId,
				null, null, null);

		final QualifiedName relationId = new QualifiedName(
				TEST_NAMESPACE,
				"testDelegationWithActivity");

		final QualifiedName intAttrKey = new QualifiedName(
				TEST_NAMESPACE, "intAttr");
		final QualifiedName strAttrKey = new QualifiedName(
				TEST_NAMESPACE, "strAtrr");
		final ImmutableList<Attribute> attributes = ImmutableList
				.of(
						new LongAttribute(intAttrKey, 1L),
						new StringAttribute(strAttrKey, "one"));
		store.actedOnBehalfOf(GRAPH_RESOURCE,
				subjectId,
				objectId,
				secondaryObjectId,
				relationId,
				attributes);

		neo4j.runAssertionInTx((db) -> {
			final List<Node> actualNodes = neo4j.getExpectedNodesOrFail(4);
			final Optional<Node> ternaryRelationNode = tryFind(actualNodes,
					n -> Neo4jBoltRule.getProvTokenValue(n).equals(relationId.toString()));
			assertTrue(ternaryRelationNode.isPresent());
			final TupleWithSchema<String> tuple = Neo4jBoltRule.getTuple(ternaryRelationNode
					.get());

			assertEquals(ProvDmRelation.DELEGATION.name(),
					tuple.getValue(ProvDmStore.PROV_DM_TYPE_KEY));
			assertEquals(longOne,
					tuple.getValue(intAttrKey.toString()));
			assertEquals("one",
					tuple.getValue(strAttrKey.toString()));
			assertEquals(
					relationId,
					tuple.getValue(ProvDmStore.PROV_DM_RELATION_ID_KEY));

			final List<Relationship> actualLinks = neo4j
					.getExpectedRelationshipsOrFail(3);
			boolean subject = false;
			boolean object = false;
			boolean secondaryObject = false;
			for (final Relationship actualLink : actualLinks) {
				final Node actualLinkSource = actualLink.getStartNode();
				final String sourceId = Neo4jBoltRule.getProvTokenValue(actualLinkSource);
				final Node actualLinkTarget = actualLink.getEndNode();
				final String targetId = Neo4jBoltRule.getProvTokenValue(actualLinkTarget);

				if (sourceId.equals(subjectId.toString())) {
					assertEquals(relationId.toString(), targetId);
					assertEquals(
							ProvDmRelation.DELEGATION.name(),
							Neo4jBoltRule.getRelationshipValue(actualLink,
									ProvDmConstants.PROV_DM_TYPE_QN.toString(),
									String.class));
					subject = true;
				} else if (targetId.equals(objectId.toString())) {
					assertEquals(relationId.toString(), sourceId);
					assertEquals(
							ProvDmRelation.DELEGATION.name(),
							Neo4jBoltRule.getRelationshipValue(actualLink,
									ProvDmConstants.PROV_DM_TYPE_QN.toString(),
									String.class));
					object = true;
				} else if (targetId.equals(secondaryObjectId.toString())) {
					assertEquals(relationId.toString(), sourceId);
					final String label = Neo4jBoltRule.getRelationshipValue(
							actualLink,
							ProvDmConstants.PROV_DM_TYPE_QN.toString(),
							String.class);
					assertTrue(label
							.startsWith(ProvDmRelation.DELEGATION.name()));
					secondaryObject = true;
				} else {
					assertTrue(false);
				}
			}
			assertTrue(subject && object && secondaryObject);
		});
	}

	@Test
	public void wasInfluencedBy() throws IOException, Exception {
		store.createProvenanceGraph(GRAPH_RESOURCE);
		final QualifiedName subjectId = new QualifiedName(
				TEST_NAMESPACE,
				"testSubject");
		store.agent(GRAPH_RESOURCE,
				subjectId,
				null);

		final QualifiedName objectId = new QualifiedName(
				TEST_NAMESPACE,
				"testObject");
		store.entity(GRAPH_RESOURCE,
				objectId,
				emptyList(),
				null);

		final QualifiedName relationId = new QualifiedName(
				TEST_NAMESPACE,
				"testInfluence");

		final QualifiedName intAttrKey = new QualifiedName(
				TEST_NAMESPACE, "intAttr");
		final QualifiedName strAttrKey = new QualifiedName(
				TEST_NAMESPACE, "strAtrr");
		final ImmutableList<Attribute> attributes = ImmutableList
				.of(
						new LongAttribute(intAttrKey, 1L),
						new StringAttribute(strAttrKey, "one"));
		store.wasInfluencedBy(GRAPH_RESOURCE,
				subjectId,
				objectId,
				relationId,
				attributes);

		neo4j.runAssertionInTx((db) -> {
			final List<Relationship> actualLinks = neo4j
					.getExpectedRelationshipsOrFail(1);
			final Relationship actualLink = actualLinks.get(0);

			assertEquals(
					ProvDmRelation.INFLUENCE.name(),
					Neo4jBoltRule.getRelationshipValue(actualLink,
							ProvDmStore.PROV_DM_TYPE_KEY,
							String.class));
			assertEquals(longOne,
					Neo4jBoltRule.getRelationshipValue(actualLink, intAttrKey.toString(),
							Long.class));
			assertEquals("one",
					Neo4jBoltRule.getRelationshipValue(actualLink, strAttrKey.toString(),
							String.class));
			assertEquals(
					relationId,
					Neo4jBoltRule.getRelationshipValue(actualLink,
							ProvDmStore.PROV_DM_RELATION_ID_KEY,
							QualifiedName.class));

			final Node actualSubject = actualLink.getStartNode();
			assertEquals(subjectId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualSubject));

			final Node actualObject = actualLink.getEndNode();
			assertEquals(objectId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualObject));
		});
	}

	@Test
	public void specializationOf() throws IOException, Exception {
		store.createProvenanceGraph(GRAPH_RESOURCE);
		final QualifiedName subjectId = new QualifiedName(
				TEST_NAMESPACE,
				"testSubject");
		store.entity(GRAPH_RESOURCE,
				subjectId,
				emptyList(), null);

		final QualifiedName objectId = new QualifiedName(
				TEST_NAMESPACE,
				"testObject");
		store.entity(GRAPH_RESOURCE,
				objectId,
				emptyList(), null);

		store.specializationOf(GRAPH_RESOURCE,
				subjectId, objectId);

		neo4j.runAssertionInTx((db) -> {
			final List<Relationship> actualLinks = neo4j
					.getExpectedRelationshipsOrFail(1);
			final Relationship actualLink = actualLinks.get(0);

			assertEquals(
					ProvDmRelation.SPECIALIZATION.name(),
					Neo4jBoltRule.getRelationshipValue(actualLink,
							ProvDmConstants.PROV_DM_TYPE_QN.toString(),
							String.class));

			final Node actualSubject = actualLink.getStartNode();
			assertEquals(subjectId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualSubject));

			final Node actualObject = actualLink.getEndNode();
			assertEquals(objectId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualObject));
		});
	}

	@Test
	public void alternateOf() throws IOException, Exception {
		store.createProvenanceGraph(GRAPH_RESOURCE);
		final QualifiedName subjectId = new QualifiedName(
				TEST_NAMESPACE,
				"testSubject");
		store.entity(GRAPH_RESOURCE,
				subjectId,
				emptyList(), null);

		final QualifiedName objectId = new QualifiedName(
				TEST_NAMESPACE,
				"testObject");
		store.entity(GRAPH_RESOURCE,
				objectId,
				emptyList(), null);

		store.alternateOf(GRAPH_RESOURCE,
				subjectId, objectId);

		neo4j.runAssertionInTx((db) -> {
			final List<Relationship> actualLinks = neo4j
					.getExpectedRelationshipsOrFail(1);
			final Relationship actualLink = actualLinks.get(0);

			assertEquals(
					ProvDmRelation.ALTERNATE.name(),
					Neo4jBoltRule.getRelationshipValue(actualLink,
							ProvDmConstants.PROV_DM_TYPE_QN.toString(),
							String.class));

			final Node actualSubject = actualLink.getStartNode();
			assertEquals(subjectId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualSubject));

			final Node actualObject = actualLink.getEndNode();
			assertEquals(objectId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualObject));
		});
	}

	@Test
	public void hadMember() throws IOException, Exception {
		store.createProvenanceGraph(GRAPH_RESOURCE);
		final QualifiedName subjectId = new QualifiedName(
				TEST_NAMESPACE,
				"testSubject");
		final List<Attribute> collectionAttributes = ImmutableList
				.of(new QualifiedNameAttribute(
						ProvDmConstants.TYPE_ATTRIBUTE,
						ProvDmConstants.COLLECTION));

		store.entity(GRAPH_RESOURCE,
				subjectId,
				collectionAttributes, null);

		final QualifiedName objectId = new QualifiedName(
				TEST_NAMESPACE,
				"testObject");
		store.entity(GRAPH_RESOURCE,
				objectId,
				emptyList(), null);

		store.hadMember(GRAPH_RESOURCE,
				subjectId, objectId);

		neo4j.runAssertionInTx((db) -> {
			final List<Relationship> actualLinks = neo4j
					.getExpectedRelationshipsOrFail(1);
			final Relationship actualLink = actualLinks.get(0);

			assertEquals(
					ProvDmRelation.MEMBERSHIP.name(),
					Neo4jBoltRule.getRelationshipValue(actualLink,
							ProvDmConstants.PROV_DM_TYPE_QN.toString(),
							String.class));

			final Node actualSubject = actualLink.getStartNode();
			assertEquals(subjectId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualSubject));

			final Node actualObject = actualLink.getEndNode();
			assertEquals(objectId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualObject));
		});
	}

	@Test
	public void repeatingAttributeKey() throws Exception {
		store.createProvenanceGraph(GRAPH_RESOURCE);
		final QualifiedName provId = new QualifiedName(
				TEST_NAMESPACE,
				"testRepeatingKey");
		final ProvLocationModel expectedLocation = TestUtil
				.newTestLocationModel();
		final QualifiedName anotherProvTypeAttr = new QualifiedName(
				TEST_NAMESPACE, "planSubtype");
		final List<Attribute> attributes = ImmutableList.of(
				new QualifiedNameAttribute(ProvDmConstants.TYPE_ATTRIBUTE,
						ProvDmConstants.PLAN),
				new QualifiedNameAttribute(ProvDmConstants.TYPE_ATTRIBUTE,
						anotherProvTypeAttr));
		store.entity(GRAPH_RESOURCE, provId, attributes, expectedLocation);

		neo4j.runAssertionInTx((db) -> {
			final List<Node> actualNodes = neo4j.getExpectedNodesOrFail(1);
			final Node actualNode = actualNodes.get(0);
			assertEquals(provId.toString(),
					Neo4jBoltRule.getProvTokenValue(actualNode));
			final TupleWithSchema<String> actualTuple = Neo4jBoltRule
					.getTuple(actualNode);
			assertEquals(
					ProvDmType.ENTITY.name(),
					actualTuple.getValue(ProvDmConstants.PROV_DM_TYPE_QN
							.toString()));
			final Object typesObj = actualTuple
					.getValue(ProvDmConstants.TYPE_ATTRIBUTE
							.toString());
			assertTrue(typesObj instanceof Set);
			@SuppressWarnings("unchecked")
			final Set<QualifiedName> types = (Set<QualifiedName>) typesObj;
			assertEquals(
					ImmutableSet.of(anotherProvTypeAttr, ProvDmConstants.PLAN),
					types);

			final ProvSpecifier actualLocation = Neo4jBoltRule
					.getLocation(actualNode);
			TestUtil.assertProvLocationsEqual(
					expectedLocation.toProvSpecifier(),
					actualLocation);
		});
	}

}
