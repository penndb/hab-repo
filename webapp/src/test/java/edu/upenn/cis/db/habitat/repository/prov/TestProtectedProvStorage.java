/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.repository.prov;

import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.jooq.DSLContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;
import com.mashape.unirest.http.exceptions.UnirestException;

import edu.upenn.cis.db.habitat.auth.UserApiLocal;
import edu.upenn.cis.db.habitat.auth.backends.plugins.AuthModule;
import edu.upenn.cis.db.habitat.auth.backends.plugins.HabitatRequestScopeModule;
import edu.upenn.cis.db.habitat.auth.backends.plugins.HabitatRequestScopeModule.HabitatRequest;
import edu.upenn.cis.db.habitat.auth.backends.plugins.HabitatRequestScopeModule.ScopedHabitatRequest;
import edu.upenn.cis.db.habitat.auth.backends.plugins.ProtectedStorageModule;
import edu.upenn.cis.db.habitat.auth.backends.plugins.SimpleScope;
import edu.upenn.cis.db.habitat.auth.backends.plugins.TemporaryDbModule;
import edu.upenn.cis.db.habitat.core.api.ProvenanceGraphApi;
import edu.upenn.cis.db.habitat.core.api.UserApi;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.ProvStringToken;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatSecurityException;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.provenance.ProvGraphStorageModule;
import edu.upenn.cis.db.habitat.repository.storage.neo4j.ConfigurableNeoStorageModule;
import edu.upenn.cis.db.habitat.repository.storage.neo4j.Neo4JStore;
import edu.upenn.cis.db.habitat.test.TestUtil;
import edu.upenn.cis.db.habitat.util.ObjectSerialization;

public class TestProtectedProvStorage {
	private ProvenanceGraphApi store;
	private SimpleScope habitatRequestScope;
	private DSLContext dslContext;

	@Rule
	public Neo4jBoltRule neo4j = new Neo4jBoltRule();

	private static final String username1 = "protectedStoreUser1";
	private static final String password1 = "protectedStoreUserPassword1";

	private static final String username2 = "protectedStoreUser2";
	private static final String password2 = "protectedStoreUserPassword2";

	private static final Random rnd = new Random();

	@Before
	public void setupGuiceAndRdms() throws UnirestException,
			HabitatServiceException {
		final Injector injector = Guice.createInjector(
				new ConfigurableNeoStorageModule(neo4j.getBoltUri(), null,
						null),
				new TemporaryDbModule(),
				new AuthModule(),
				new HabitatRequestScopeModule(),
				new ProtectedStorageModule(Neo4JStore.class),
				new ProvGraphStorageModule());
		dslContext = injector.getInstance(DSLContext.class);
		store = injector.getInstance(ProvenanceGraphApi.class);
		final UserApiLocal userApiLocalBackend = injector
				.getInstance(UserApiLocal.class);
		userApiLocalBackend.createTables();
		habitatRequestScope = injector
				.getInstance(Key.get(
						SimpleScope.class,
						Names.named(
								HabitatRequestScopeModule.HABITAT_REQUEST_SCOPE_NAME)));

		runScopedRequest(
				UserApi.Root,
				() -> {
					final boolean addUser1 = userApiLocalBackend
							.addUserAndOrganization(
									username1,
									null,
									null,
									null,
									null,
									null,
									null,
									null,
									null,
									null,
									"provWsTestUser@mail",
									null,
									null,
									"", "");
					assertTrue(addUser1);
					final boolean addCred1 = userApiLocalBackend.addCredential(
							username1,
							"local",
							"http",
							password1);
					assertTrue(addCred1);

					final boolean addUser2 = userApiLocalBackend
							.addUserAndOrganization(
									username2,
									null,
									null,
									null,
									null,
									null,
									null,
									null,
									null,
									null,
									"provWsTestUser2@mail",
									null,
									null,
									"", "");
					assertTrue(addUser2);

					final boolean addCred2 = userApiLocalBackend.addCredential(
							username2,
							"local",
							"http",
							password2);
					assertTrue(addCred2);
				});
	}

	@Test
	public void createGraphTest() throws HabitatServiceException {
		final String resource = "TestProtectedProvStore.create";

		runScopedRequest(username1, () -> {
			store.createProvenanceGraph(resource);
		});

		// Idempotent
		runScopedRequest(username1, () -> {
			store.createProvenanceGraph(resource);
		});

		runScopedRequest(username2, () -> {
			boolean exception = false;
			try {
				store
						.createProvenanceGraph(resource);
			} catch (HabitatSecurityException e) {
				exception = true;
			}
			assertTrue(exception);
		});
	}

	@Test
	public void createGraphBadNameTest() throws HabitatServiceException {
		runScopedRequest(username1, () -> {
			boolean exception = false;
			try {
				store
						.createProvenanceGraph(null);
			} catch (HabitatServiceException e) {
				exception = true;
			}
			assertTrue(exception);
		});

		runScopedRequest(username1, () -> {
			boolean exception = false;
			try {
				store
						.createProvenanceGraph("  ");
			} catch (HabitatServiceException e) {
				exception = true;
			}
			assertTrue(exception);
		});

		runScopedRequest(username1, () -> {
			boolean exception = false;
			try {
				// resource name will be too long
				store
						.createProvenanceGraph(UUID.randomUUID().toString());
			} catch (HabitatServiceException e) {
				exception = true;
			}
			assertTrue(exception);
		});
	}

	// Adapted from
	// https://github.com/junit-team/junit4/wiki/multithreaded-code-and-concurrency
	@Test
	public void createGraphConcurrent() throws InterruptedException,
			HabitatServiceException {
		final String resource = "TestProtectedProv.concurrent";
		final int numThreads = 10;
		final int maxTimeoutSeconds = 3;
		final List<Throwable> exceptions = Collections
				.synchronizedList(new ArrayList<Throwable>());
		final Set<String> successfulUsers = Collections
				.synchronizedSet(new HashSet<>());
		final ExecutorService threadPool = Executors
				.newFixedThreadPool(numThreads);

		try {
			final CountDownLatch allExecutorThreadsReady = new CountDownLatch(
					numThreads);
			final CountDownLatch afterInitBlocker = new CountDownLatch(1);
			final CountDownLatch allDone = new CountDownLatch(numThreads);

			for (int i = 0; i < numThreads; i++) {
				final String username = rnd.nextBoolean() ? username1
						: username2;
				threadPool.submit(new Runnable() {
					public void run() {
						allExecutorThreadsReady.countDown();
						try {
							afterInitBlocker.await();
							runScopedRequest(
									username, () -> {
										store.createProvenanceGraph(resource);
									});
							successfulUsers.add(username);
						} catch (final Throwable e) {
							exceptions.add(e);
						} finally {
							allDone.countDown();
						}
					}
				});
			}
			// wait until all threads are ready
			assertTrue(
					"Timeout initializing threads! Perform long lasting initializations before passing runnables to assertConcurrent",
					allExecutorThreadsReady.await(numThreads * 10,
							TimeUnit.MILLISECONDS));
			// start all test runners
			afterInitBlocker.countDown();
			assertTrue("timeout! More than" + maxTimeoutSeconds
					+ "seconds",
					allDone.await(maxTimeoutSeconds, TimeUnit.SECONDS));
		} finally {
			threadPool.shutdownNow();
		}
		exceptions
				.forEach(exception -> assertTrue(
						exception instanceof HabitatSecurityException));

		assertEquals("unexpected number of successful users: "
				+ successfulUsers.size(),
				1, successfulUsers.size());

		final String successfulUser = successfulUsers.iterator().next();
		final String unsuccessfulUser = successfulUser.equals(username1)
				? username2
				: username1;

		runScopedRequest(successfulUser, () -> {
			store.getProvenanceNodes(resource);
		});

		runScopedRequest(unsuccessfulUser, () -> {
			boolean exception = false;
			try {
				store
						.getProvenanceNodes(resource);
			} catch (HabitatSecurityException e) {
				exception = true;
			}
			assertTrue(exception);
		});
	}

	@Test
	public void storeProvNodeTest() throws Exception {
		final ProvToken provToken = new ProvStringToken(
				"_STORE_PROV_NODE_TEST");
		final ProvLocation provLocation = new ProvLocation(
				"storeProvNodeTestSource", newArrayList(3, 4));
		final String resource = "TestProtectedProvStore.storeNode";

		runScopedRequest(username1, () -> {
			store.createProvenanceGraph(resource);
			store.storeProvenanceNode(resource, provToken, provLocation);

			final ProvSpecifier actualSpecifier = store
					.getProvenanceLocation(resource, provToken);
			assertTrue(actualSpecifier instanceof ProvLocation);
			final ProvLocation actualLocation = (ProvLocation) actualSpecifier;
			assertEquals(provLocation.getField(), actualLocation.getField());
			assertEquals(provLocation.getPosition(),
					actualLocation.getPosition());
		});

		neo4j.runAssertionInTx((graphDatabase) -> {
			final List<Node> actualNodes = neo4j.getExpectedNodesOrFail(1);
			final Node actualNode = actualNodes.get(0);

			assertEquals(provToken.toString(),
					Neo4jBoltRule.getProvTokenValue(actualNode));
			assertTrue(actualNode.hasProperty("location"));
			final Object actualObject = ObjectSerialization
					.getObject(actualNode.getProperty("location"));
			assertTrue(actualObject instanceof ProvLocation);
			final ProvLocation actualLocation = (ProvLocation) actualObject;
			assertEquals(provLocation.getField(), actualLocation.getField());
			assertEquals(provLocation.getPosition(),
					actualLocation.getPosition());
			assertEquals(provLocation.getStream(), actualLocation.getStream());
		});
	}

	@Test
	public void storeProvNodeAuthz() throws HabitatServiceException {
		final ProvToken provToken = new ProvStringToken(
				"_STORE_PROV_NODE_AUTHZ");
		final ProvLocation provLocation = new ProvLocation(
				"storeProvNodeAuthzSource", newArrayList(3, 4));
		final String resource = "protectedProv.storeNodeAuthz";

		runScopedRequest(username1, () -> {
			store.createProvenanceGraph(resource);
			store.storeProvenanceNode(resource, provToken, provLocation);
		});

		runScopedRequest(username2, () -> {
			final ProvToken unauthzedToken = new ProvStringToken(
					"_STORE_PROV_NODE_AUTHZ_UNAUTHZED");
			final ProvLocation unauthzedLocation = new ProvLocation(
					"storeProvNodeAuthzSource.unauthzed", newArrayList(3, 4));
			boolean exception = false;
			try {
				store
						.storeProvenanceNode(resource, unauthzedToken,
								unauthzedLocation);
			} catch (HabitatSecurityException e) {
				exception = true;
			}
			assertTrue(exception);
		});
	}

	@Test
	public void storeProvNodeDataTest() throws HabitatServiceException {
		final ProvToken provToken = new ProvStringToken(
				"_STORE_PROV_NODE_DATA_TEST");
		final ProvLocation provLocation = new ProvLocation(
				"storeProvNodeDataTestSource", newArrayList(3, 4));
		final String resource = "TestProtectedProv.storeNodeData";

		runScopedRequest(username1, () -> {
			store.createProvenanceGraph(resource);
			store.storeProvenanceNode(resource, provToken, provLocation);

			final ProvSpecifier actualSpecifier = store
					.getProvenanceLocation(resource, provToken);
			assertTrue(actualSpecifier instanceof ProvLocation);
			final ProvLocation actualLocation = (ProvLocation) actualSpecifier;
			assertEquals(provLocation.getField(), actualLocation.getField());
			assertEquals(provLocation.getPosition(),
					actualLocation.getPosition());
		});

		runScopedRequest(username2,
				() -> {
					final ProvToken unauthzedToken = new ProvStringToken(
							"_STORE_PROV_NODE_DATA_TEST_UNAUTHZED");
					final ProvLocation unauthzedLocation = new ProvLocation(
							"storeProvNodeDataTestSource.unauthzed",
							newArrayList(3, 4));
					final BasicTuple unauthzedTuple = TestUtil.newTestTuple();
					boolean exception = false;
					try {
						store
								.storeProvenanceNode(resource,
										unauthzedToken,
										unauthzedTuple,
										unauthzedLocation);
					} catch (HabitatSecurityException e) {
						exception = true;
					}
					assertTrue(exception);
				});
	}

	@Test
	public void getProvLocationTest() throws HabitatServiceException {
		final ProvToken provToken = new ProvStringToken("_PROV_LOCATION_TEST");
		final ProvLocation provLocation = new ProvLocation(
				"provLocationTestSource", newArrayList(3, 4));
		final String resource = "TestProtectedProvStore.getLoc";

		runScopedRequest(username1, () -> {
			store.createProvenanceGraph(resource);
			store.storeProvenanceNode(resource, provToken, provLocation);

			final ProvSpecifier actualSpecifier = store
					.getProvenanceLocation(resource, provToken);
			assertTrue(actualSpecifier instanceof ProvLocation);
			final ProvLocation actualLocation = (ProvLocation) actualSpecifier;
			assertEquals(provLocation.getField(), actualLocation.getField());
			assertEquals(provLocation.getPosition(),
					actualLocation.getPosition());
		});

		runScopedRequest(username2, () -> {
			boolean exception = false;
			try {
				store
						.getProvenanceLocation(resource, provToken);
			} catch (HabitatSecurityException e) {
				exception = true;
			}
			assertTrue(exception);
		});
	}

	@Test
	public void getProvenanceDataTest() throws IOException,
			HabitatServiceException {
		final String resource = "TestProtectedProvStore.getData";
		final BasicTuple expectedTuple = TestUtil.newTestTuple();
		final String tokenValue = "NODE_WITH_DATA";
		final ProvToken token = new ProvStringToken(tokenValue);
		final ProvLocation location = new ProvLocation(
				"ws.getProvenanceDataTestLocation", newArrayList(7, 8));

		runScopedRequest(
				username1,
				() -> {
					store.createProvenanceGraph(resource);
					store.storeProvenanceNode(resource, token, expectedTuple,
							location);

					final TupleWithSchema<String> actualTuple = store
							.getProvenanceData(resource, token);
					TestUtil.assertTuplesEquals(expectedTuple, actualTuple);
				});

		runScopedRequest(
				username2,
				() -> {
					boolean exception = false;
					try {
						store.getProvenanceData(resource, token);
					} catch (HabitatSecurityException e) {
						exception = true;
					}
					assertTrue(exception);
				});

	}

	@Test
	public void getProvenanceNodesTest() throws IOException,
			HabitatServiceException {
		final String resource = "TestProtectedProvStore.getNodes";
		final String unusedResource = "TestProtectedProvStore.unused";
		final BasicTuple expectedTuple = TestUtil.newTestTuple();
		final String tokenValue = "NODE_WITH_DATA_NODES";
		final ProvToken token = new ProvStringToken(tokenValue);
		final ProvLocation location = new ProvLocation(
				"ws.getProvenanceNodesTestLocation", newArrayList(7, 8));

		runScopedRequest(
				username1,
				() -> {
					store.createProvenanceGraph(resource);
					store.storeProvenanceNode(resource, token, expectedTuple,
							location);

					store.createProvenanceGraph(unusedResource);

					final Map<String, StructuredData<String, Object>> actualNodes = store
							.getProvenanceNodes(resource);
					assertEquals(1, actualNodes.size());
					final StructuredData<String, Object> actualData = actualNodes
							.get(tokenValue);
					assertNotNull(actualData);

					final Map<String, StructuredData<String, Object>> noNodes = store
							.getProvenanceNodes(unusedResource);
					assertEquals(0, noNodes.size());
				});

		runScopedRequest(
				username2,
				() -> {
					boolean exception = false;
					try {
						store.getProvenanceNodes(resource);
					} catch (HabitatSecurityException e) {
						exception = true;
					}
					assertTrue(exception);

					exception = false;
					try {
						store.getProvenanceNodes(unusedResource);
					} catch (HabitatSecurityException e) {
						exception = true;
					}
					assertTrue(exception);
				});

	}

	@Test
	public void storeProvenanceLinkMissingEndpoint()
			throws Exception {
		final String resource = "TestProtectedProv.linkMissingEnd";
		final ProvToken from = new ProvStringToken("missingEndpoints.source");
		final ProvToken to = new ProvStringToken("missingEndpoints.target");
		final String label = "missingEndpoints_label";
		runScopedRequest(username1, () -> {
			store.createProvenanceGraph(resource);
			boolean exception = false;
			try {
				store.storeProvenanceLink(resource, from, label, to);
			} catch (HabitatServiceException e) {
				exception = true;
			}
			assertTrue(exception);
		});

		neo4j.runAssertionInTx((graphDatabase) -> {
			neo4j.getExpectedRelationshipsOrFail(0);
		});
	}

	@Test
	public void storeProvenanceLink() throws Exception {
		final String resource = "TestProtectedProv.storeLink";
		final ProvToken from = new ProvStringToken("storeLink.source");
		final ProvToken to = new ProvStringToken("storeLink.target");
		final String label = "storeLink_label";
		final ProvLocation sourceLocation = new ProvLocation(
				"storeLink.source.location", newArrayList(7, 8));
		final ProvLocation targetLocation = new ProvLocation(
				"storeLink.target.location", newArrayList(9, 10));
		runScopedRequest(
				username1,
				() -> {
					store.createProvenanceGraph(resource);
					store.storeProvenanceNode(resource, from, sourceLocation);
					store.storeProvenanceNode(resource, to, targetLocation);
					store.storeProvenanceLink(resource, from, label, to);
				});

		neo4j.runAssertionInTx((graphDatabase) -> {
			final List<Relationship> relations = neo4j
					.getExpectedRelationshipsOrFail(1);
			final Relationship actualLink = relations.get(0);
			assertEquals(label, actualLink.getProperty("label"));

			final Node sourceNode = actualLink.getStartNode();
			assertEquals(from.toString(), Neo4jBoltRule.getProvTokenValue(sourceNode));

			final Node targetNode = actualLink.getEndNode();
			assertEquals(to.toString(), Neo4jBoltRule.getProvTokenValue(targetNode));
		});
	}

	@Test
	public void storeProvenanceLinkAuthz() throws Exception {
		final String resource = "TestProtectedProv.storeLinkAuthz";
		final ProvToken from = new ProvStringToken("storeLinkAuthz.source");
		final ProvToken to = new ProvStringToken("storeLinkAuthz.target");
		final String label = "storeLinkAuthz_label";
		final ProvLocation sourceLocation = new ProvLocation(
				"storeLinkAuthz.source.location", newArrayList(7, 8));
		final ProvLocation targetLocation = new ProvLocation(
				"storeLinkAuthz.target.location", newArrayList(9, 10));
		runScopedRequest(
				username1,
				() -> {
					store.createProvenanceGraph(resource);
					store.storeProvenanceNode(resource, from, sourceLocation);
					store.storeProvenanceNode(resource, to, targetLocation);
					store.storeProvenanceLink(resource, from, label, to);
				});

		runScopedRequest(
				username2,
				() -> {
					boolean exception = false;
					try {
						store.storeProvenanceLink(resource, from, label, to);
					} catch (HabitatSecurityException e) {
						exception = true;
					}
					assertTrue(exception);
				});

		neo4j.runAssertionInTx((graphDatabase) -> {
			neo4j.getExpectedRelationshipsOrFail(1);
		});
	}

	@Test
	public void storeProvenanceLinkAuthzOwnEndpoints()
			throws Exception {
		final String user1Resource = "protectedProv.storeLinkAuthz1";

		runScopedRequest(
				username1,
				() -> {
					store.createProvenanceGraph(user1Resource);
				});

		final String user2Resource = "protectedProv.storeLinkAuthz2";
		final ProvToken from = new ProvStringToken("storeLinkAuthz2.source");
		final ProvToken to = new ProvStringToken("storeLinkAuthz2.target");
		final String label = "storeLinkAuthz2.label";
		final ProvLocation sourceLocation = new ProvLocation(
				"storeLinkAuthz2.source.location", newArrayList(7, 8));
		final ProvLocation targetLocation = new ProvLocation(
				"storeLinkAuthz2.target.location", newArrayList(9, 10));
		runScopedRequest(
				username2,
				() -> {
					store.createProvenanceGraph(user2Resource);
					store.storeProvenanceNode(user2Resource, from,
							sourceLocation);
					store.storeProvenanceNode(user2Resource, to,
							targetLocation);
					boolean exception = false;
					try {
						store.storeProvenanceLink(user1Resource, from, label,
								to);
					} catch (HabitatSecurityException e) {
						exception = true;
					}
					assertTrue(exception);
				});

		neo4j.runAssertionInTx((graphDatabase) -> {
			neo4j.getExpectedRelationshipsOrFail(0);
		});
	}

	@Test
	public void storeProvenanceLinkMulti() throws Exception {
		final String resource = "TestProtectedProv.storeLinkMulti";
		final ProvToken from = new ProvStringToken("storeLinkMulti.source");
		final ProvToken to = new ProvStringToken("storeLinkMulti.target");
		final String label = "storeLinkMulti_label";
		final ProvLocation sourceLocation = new ProvLocation(
				"storeLinkMulti.source.location", newArrayList(7, 8));
		final ProvLocation targetLocation = new ProvLocation(
				"storeLinkMulti.target.location", newArrayList(9, 10));
		runScopedRequest(
				username1,
				() -> {
					store.createProvenanceGraph(resource);
					store.storeProvenanceNode(resource, from, sourceLocation);
					store.storeProvenanceNode(resource, to, targetLocation);

					store.storeProvenanceLink(resource, from, label, to);
					store.storeProvenanceLink(resource, from, label, to);
					store.storeProvenanceLink(resource, from, label, to);
				});

		neo4j.runAssertionInTx((graphDatabase) -> {
			final List<Relationship> actualLinks = neo4j
					.getExpectedRelationshipsOrFail(3);

			for (final Relationship actualLink : actualLinks) {
				assertEquals(label, actualLink.getProperty("label"));

				final Node sourceNode = actualLink
						.getStartNode();
				assertEquals(from.toString(),
						Neo4jBoltRule.getProvTokenValue(sourceNode));

				final Node targetNode = actualLink
						.getEndNode();
//				assertEquals(to.toString(),
//						neo4j.getProvTokenValue(targetNode));
				assertEquals(to.toString(), Neo4jBoltRule.getProvTokenValue(targetNode));
			}
		});
	}

	@Test
	public void getEdgeMapMulti() throws HabitatServiceException {
		final String resource = "TestProtectedProv.edgeMapMulti";
		final ProvToken from = new ProvStringToken("getEdgeMapMulti.source");
		final ProvToken to = new ProvStringToken("getEdgeMapMulti.target");
		final String label = "getEdgeMapMulti_label";
		final ProvLocation sourceLocation = new ProvLocation(
				"getEdgeMapMulti.source.location", newArrayList(7, 8));
		final ProvLocation targetLocation = new ProvLocation(
				"getEdgeMapMulti.target.location", newArrayList(9, 10));
		runScopedRequest(
				username1,
				() -> {
					store.createProvenanceGraph(resource);
					store.storeProvenanceNode(resource, from, sourceLocation);
					store.storeProvenanceNode(resource, to, targetLocation);

					store.storeProvenanceLink(resource, from, label, to);
					store.storeProvenanceLink(resource, from, label, to);
					store.storeProvenanceLink(resource, from, label, to);

					final Map<String, List<TupleWithSchema<String>>> targetMap = store
							.getEdgeMapFrom(resource, from);
					assertEquals(1, targetMap.size());
					final List<TupleWithSchema<String>> outgoingEdges = targetMap
							.get(to.toString());
					assertEquals(3, outgoingEdges.size());
					final TupleWithSchema<String> outgoingEdge = outgoingEdges
							.iterator().next();
					assertEquals(1, outgoingEdge.size());
					assertTrue(outgoingEdge.containsKey("label"));
					assertEquals(label, outgoingEdge.getValue("label"));

					final Map<String, List<TupleWithSchema<String>>> targetMapWrongWay = store
							.getEdgeMapFrom(resource, to);
					assertTrue(targetMapWrongWay.isEmpty());

					final Map<String, List<TupleWithSchema<String>>> sourceMap = store
							.getEdgeMapTo(resource, to);
					assertEquals(1, sourceMap.size());
					final List<TupleWithSchema<String>> incomingEdges = sourceMap
							.get(from.toString());
					assertEquals(3, incomingEdges.size());
					final TupleWithSchema<String> incomingEdge = incomingEdges
							.iterator().next();
					assertEquals(1, incomingEdge.size());
					assertTrue(incomingEdge.containsKey("label"));
					assertEquals(label, incomingEdge.getValue("label"));

					final Map<String, List<TupleWithSchema<String>>> sourceMapWrongWay = store
							.getEdgeMapTo(resource, from);
					assertTrue(sourceMapWrongWay.isEmpty());

				});
	}

	@Test
	public void getConnectedMulti() throws HabitatServiceException {
		final String resource = "TestProtectedProv.connectedMulti";
		final ProvToken from = new ProvStringToken("getConnectedMulti.source");
		final ProvToken to = new ProvStringToken("getConnectedMulti.target");
		final String label = "getConnectedMulti_label";
		final ProvLocation sourceLocation = new ProvLocation(
				"getConnectedMulti.source.location", newArrayList(7, 8));
		final ProvLocation targetLocation = new ProvLocation(
				"getConnectedMulti.target.location", newArrayList(9, 10));
		runScopedRequest(
				username1,
				() -> {
					store.createProvenanceGraph(resource);
					store.storeProvenanceNode(resource, from, sourceLocation);
					store.storeProvenanceNode(resource, to, targetLocation);
					final int edgeCount = 3;
					for (int i = 0; i < edgeCount; i++) {
						store.storeProvenanceLink(resource, from, label, to);
					}

					final Iterator<ProvToken> outgoingIter = store
							.getConnectedFrom(resource, from).iterator();
					for (int i = 0; i < edgeCount; i++) {
						assertTrue(String.format(
								"Expected %d outgoing nodes. Got %d",
								edgeCount, i),
								outgoingIter.hasNext());
						final ProvToken actualTo = outgoingIter.next();
						assertEquals(to, actualTo);
					}

					final Iterator<ProvToken> outgoingIterWrongWay = store
							.getConnectedFrom(resource, to).iterator();
					assertFalse(outgoingIterWrongWay.hasNext());

					final Iterator<ProvToken> incomingIter = store
							.getConnectedTo(resource, to).iterator();
					for (int i = 0; i < edgeCount; i++) {
						assertTrue(String.format(
								"Expected %d incoming nodes. Got %d",
								edgeCount, i),
								incomingIter.hasNext());
						final ProvToken actualFrom = incomingIter.next();
						assertEquals(from, actualFrom);
					}

					final Iterator<ProvToken> incomingIterWrongWay = store
							.getConnectedTo(resource, from).iterator();
					assertFalse(incomingIterWrongWay.hasNext());
				});
	}

	@Test
	public void getEdgesMulti() throws HabitatServiceException {
		final String resource = "TestProtectedProv.edgesMulti";
		final ProvToken from = new ProvStringToken("getEdgesMulti.source");
		final ProvToken to = new ProvStringToken("getEdgesMulti.target");
		final String label = "getEdgesMulti_label";
		final ProvLocation sourceLocation = new ProvLocation(
				"getEdgesMulti.source.location", newArrayList(7, 8));
		final ProvLocation targetLocation = new ProvLocation(
				"getEdgesMulti.target.location", newArrayList(9, 10));
		runScopedRequest(
				username1,
				() -> {
					store.createProvenanceGraph(resource);
					store.storeProvenanceNode(resource, from, sourceLocation);
					store.storeProvenanceNode(resource, to, targetLocation);
					final int edgeCount = 3;
					for (int i = 0; i < edgeCount; i++) {
						store.storeProvenanceLink(resource, from, label, to);
					}

					final Iterator<TupleWithSchema<String>> outgoingIter = store
							.getEdgesFrom(resource, from).iterator();
					for (int i = 0; i < edgeCount; i++) {
						assertTrue(String.format(
								"Expected %d outgoing edges. Got %d",
								edgeCount, i),
								outgoingIter.hasNext());
						final TupleWithSchema<String> actualOutgoing = outgoingIter
								.next();
						assertTrue(actualOutgoing.containsKey("label"));
						assertEquals(label, actualOutgoing.getValue("label"));
					}

					final Iterator<TupleWithSchema<String>> outgoingIterWrongWay = store
							.getEdgesFrom(resource, to).iterator();
					assertFalse(outgoingIterWrongWay.hasNext());

					final Iterator<TupleWithSchema<String>> incomingIter = store
							.getEdgesTo(resource, to).iterator();
					for (int i = 0; i < edgeCount; i++) {
						assertTrue(String.format(
								"Expected %d incoming edges. Got %d",
								edgeCount, i),
								incomingIter.hasNext());
						final TupleWithSchema<String> actualIncoming = incomingIter
								.next();
						assertTrue(actualIncoming.containsKey("label"));
						assertEquals(label, actualIncoming.getValue("label"));
					}

					final Iterator<TupleWithSchema<String>> incomingIterWrongWay = store
							.getEdgesTo(resource, from).iterator();
					assertFalse(incomingIterWrongWay.hasNext());
				});
	}

	private void runScopedRequest(String user, HabitatRequest req)
			throws HabitatServiceException {
		new ScopedHabitatRequest(
				habitatRequestScope,
				user,
				req).makeRequest();
	}

	@After
	public void tearDown() throws IOException {
		TemporaryDbModule.emptyDatabase(dslContext);
	}
}
