/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.repository.prov;

import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Lists.newArrayList;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.google.common.collect.EvictingQueue;
import com.google.common.collect.ImmutableList;

import edu.upenn.cis.db.habitat.Config;
import edu.upenn.cis.db.habitat.client.ProvDmClient;
import edu.upenn.cis.db.habitat.core.api.ProvDmApi;
import edu.upenn.cis.db.habitat.core.type.model.ProvLocationModel;
import edu.upenn.cis.db.habitat.core.type.provdm.Attribute;
import edu.upenn.cis.db.habitat.core.type.provdm.BooleanAttribute;
import edu.upenn.cis.db.habitat.core.type.provdm.DoubleAttribute;
import edu.upenn.cis.db.habitat.core.type.provdm.LongAttribute;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvDmConstants;
import edu.upenn.cis.db.habitat.core.type.provdm.QualifiedName;
import edu.upenn.cis.db.habitat.core.type.provdm.QualifiedNameAttribute;
import edu.upenn.cis.db.habitat.core.type.provdm.StringAttribute;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

public class CreateTestProv {

	private static final String exampleNs = "http://example.com/";

	private static final QualifiedName stringAttr = new QualifiedName(
			exampleNs,
			"string_attr");

	private static final QualifiedName qnAttr = new QualifiedName(exampleNs,
			"qn_attr");

	private static final QualifiedName longAttr = new QualifiedName(exampleNs,
			"long_attr");

	private static final QualifiedName doubleAttr = new QualifiedName(
			exampleNs, "double_attr");

	private static final QualifiedName booleanAttr = new QualifiedName(
			exampleNs,
			"boolean_attr");

	private static final QualifiedName windowTypeAttr = new QualifiedName(
			ProvDmConstants.PENN_PROV_NAMESPACE,
			"streamingWindow");

	private static final QualifiedName stepsFromInputAttr = new QualifiedName(
			ProvDmConstants.PENN_PROV_NAMESPACE, "pennDbStepsFromInput");

	private static final QualifiedName creationTimeAttr = new QualifiedName(
			ProvDmConstants.PENN_PROV_NAMESPACE, "pennDbCreationTime");

	private static final QualifiedName outputCountAttr = new QualifiedName(
			exampleNs,
			"output-count");
	private static long outputCount = 0;

	private static final class Window {
		private final int windowSize = 5;
		private final int windowOverlap = 2;
		private final EvictingQueue<QualifiedName> windowTokens = EvictingQueue
				.create(windowSize);
		private int added = 0;

		public Window() {
			checkState(windowSize > windowOverlap);
		}

		public boolean add(QualifiedName inputEntityProvId) {
			added++;
			return windowTokens.add(inputEntityProvId);
		}

		public boolean isFull() {
			if (added < windowSize) {
				return false;
			}
			return added % (windowSize - windowOverlap) == windowSize
					% (windowSize - windowOverlap);
		}

		public Iterable<QualifiedName> getAll() {
			return ImmutableList.copyOf(windowTokens);
		}
	}

	public static void main(String[] args) throws
			HabitatServiceException, InterruptedException {
		if (args.length < 3) {
			System.err.println("Required args: username password graphName");
			System.exit(1);
		}
		final String username = args[0];
		final String password = args[1];
		final String graph = args[2];
		final ProvDmClient provDm = new ProvDmClient(username,
				password,
				"localhost",
				Config.getServerPort());

		provDm.createProvenanceGraph(graph);

		final Window window = new Window();
		int windows = 0;
		while (true) {
			final QualifiedName token = storeInputEntity(provDm, graph);
			window.add(token);
			if (window.isFull()) {
				processWindow(provDm, window.getAll(), graph);
				windows++;
				System.out.println("created " + windows + " windows");
			}
			// Thread.sleep(100);
		}

	}

	private static QualifiedName processWindow(
			ProvDmApi store,
			Iterable<QualifiedName> windowMembers,
			String graph) throws HabitatServiceException,
			InterruptedException {

		// Thread.sleep(20);

		// Create window

		final ImmutableList<Attribute> windowAttributes = ImmutableList.of(
				new StringAttribute(stringAttr, "myWindow"),
				new LongAttribute(stepsFromInputAttr, 1L),
				new LongAttribute(creationTimeAttr, new Date()
						.getTime()),
				new QualifiedNameAttribute(ProvDmConstants.TYPE_ATTRIBUTE,
						ProvDmConstants.COLLECTION),
				new QualifiedNameAttribute(ProvDmConstants.TYPE_ATTRIBUTE,
						windowTypeAttr));

		final String uuid = UUID.randomUUID().toString();

		final QualifiedName windowProvId = new QualifiedName(exampleNs, uuid
				+ ".testWindow");
		final ProvLocationModel location = new ProvLocationModel(
				null,
				uuid + ".testWindowLocation", newArrayList(7, 8).toArray(
						new Integer[0]));

		store.entity(graph, windowProvId, windowAttributes, location);

		// Add inputs to window

		for (final QualifiedName member : windowMembers) {
			store.hadMember(graph, windowProvId, member);
		}

		// Add activity
		final QualifiedName activityProvId = storeActivity(store, graph);

		final List<Attribute> usedAttributes = ImmutableList.of(
				new StringAttribute(stringAttr, UUID.randomUUID().toString()));

		store.used(graph, activityProvId, windowProvId, null, null,
				usedAttributes);

		final List<Attribute> startedByAttributes = ImmutableList
				.of(
				new StringAttribute(stringAttr, UUID.randomUUID().toString()));

		store.wasStartedBy(graph, activityProvId, windowProvId, null, null,
				startedByAttributes);

		// Add output

		final QualifiedName outputProvId = storeOutputEntity(store, graph);

		final List<Attribute> generatedByAttributes =
				ImmutableList.of(
						new StringAttribute(stringAttr, UUID.randomUUID()
								.toString()));

		store.wasGeneratedBy(graph, outputProvId, activityProvId, null, null,
				generatedByAttributes);

		final List<Attribute> derivedFromAttributes = ImmutableList.of(
				new StringAttribute(stringAttr, UUID.randomUUID().toString()));

		store.wasDerivedFrom(graph, outputProvId, windowProvId, activityProvId,
				null, null, null, derivedFromAttributes);

		return windowProvId;

	}

	private static QualifiedName storeInputEntity(ProvDmApi store, String graph)
			throws HabitatServiceException {

		final ImmutableList<Attribute> attributes = ImmutableList.of(
				new BooleanAttribute(booleanAttr, true),
				new DoubleAttribute(
						doubleAttr, 8.56),
				new LongAttribute(longAttr,
						76L),
				new LongAttribute(stepsFromInputAttr, 0L),
				new LongAttribute(creationTimeAttr, new Date()
						.getTime()),
				new QualifiedNameAttribute(qnAttr, new QualifiedName(
						exampleNs, "myProp")),
				new StringAttribute(stringAttr, "myStringProp"));

		final String uuid = UUID.randomUUID().toString();

		final QualifiedName provId = new QualifiedName(exampleNs,
				uuid + ".testInputEntity");
		final ProvLocationModel location = new ProvLocationModel(
				null,
				uuid + ".testInputEntityLocation",
				newArrayList(7, 8).toArray(new Integer[0]));

		store.entity(graph, provId, attributes, location);

		return provId;

	}

	private static QualifiedName storeActivity(ProvDmApi store, String graph)
			throws HabitatServiceException {

		final ImmutableList<Attribute> attributes = ImmutableList.of(
				new BooleanAttribute(booleanAttr, true),
				new DoubleAttribute(doubleAttr, 8.56),
				new StringAttribute(stringAttr, "myActivity"),
				new LongAttribute(stepsFromInputAttr, 2L),
				new LongAttribute(creationTimeAttr, new Date()
						.getTime()),
				new QualifiedNameAttribute(qnAttr, new QualifiedName(
						exampleNs, "activityQnAttrValue")));

		final String uuid = UUID.randomUUID().toString();

		final QualifiedName provId = new QualifiedName(exampleNs, uuid
				+ ".testActivity");
		final ProvLocationModel location = new ProvLocationModel(
				null,
				uuid + ".testActivityLocation",
				newArrayList(7, 8).toArray(new Integer[0]));

		final Date startTime = new Date();

		store.activity(graph, provId, attributes, startTime, null, location);
		return provId;

	}

	private static QualifiedName storeOutputEntity(ProvDmApi store, String graph)
			throws HabitatServiceException {

		final ImmutableList<Attribute> attributes = ImmutableList.of(
				new BooleanAttribute(booleanAttr, true),
				new DoubleAttribute(doubleAttr, 8.56),
				new StringAttribute(stringAttr, "myOutputEntity"),
				new LongAttribute(stepsFromInputAttr, 3L),
				new LongAttribute(creationTimeAttr, new Date()
						.getTime()),
				new QualifiedNameAttribute(qnAttr, new QualifiedName(
						exampleNs, "myOutputEntityQnPropValue")),
				new LongAttribute(outputCountAttr, ++outputCount));

		final String uuid = UUID.randomUUID().toString();
		final QualifiedName provId = new QualifiedName(exampleNs, uuid
				+ ".testOutputEntity");
		final ProvLocationModel location = new ProvLocationModel(null,
				uuid + ".testOutputEntityLocation", newArrayList(7, 8).toArray(
						new Integer[0]));

		store.entity(graph, provId, attributes, location);
		return provId;

	}

}
