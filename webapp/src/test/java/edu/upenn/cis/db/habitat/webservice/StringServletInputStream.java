/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice;

import java.io.IOException;
import java.io.StringReader;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;

import org.postgresql.util.ReaderInputStream;

/**
 * @author John Frommeyer
 *
 */
public class StringServletInputStream extends ServletInputStream {

	private final ReaderInputStream inputStream;
	private int read = 0;
	private final int length;

	public StringServletInputStream(String content) {
		length = content.length();
		inputStream = new ReaderInputStream(new StringReader(content));
	}

	@Override
	public int available() {
		return length - read;
	}

	@Override
	public boolean isFinished() {
		return read >= length;
	}

	@Override
	public boolean isReady() {
		return true;
	}

	@Override
	public void setReadListener(ReadListener readListener) {}

	@Override
	public int read() throws IOException {
		read++;
		return inputStream.read();
	}

}
