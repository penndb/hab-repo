/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.repository.prov;

import static com.google.common.collect.Maps.transformValues;
import static java.util.Collections.emptyMap;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.Optional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import edu.upenn.cis.db.habitat.Config;
import edu.upenn.cis.db.habitat.client.ProvenanceClient;
import edu.upenn.cis.db.habitat.core.api.ProvenanceGraphApi;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphInstance.NodeInstance;
import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.repository.storage.neo4j.SubgraphQuery.NodeIdLocation;
import edu.upenn.cis.db.habitat.test.TestUtil;

public class CreateTestProvFromTemplate {

	private static Gson gson = new GsonBuilder().create();

	public static void main(String[] args)
			throws HabitatServiceException, InterruptedException,
			FileNotFoundException {
		if (args.length < 5) {
			System.err
					.println(
							"Required args: username password graphName pathToTemplate pathToIdLookup");
			System.exit(1);
		}
		final String username = args[0];
		final String password = args[1];
		final String graph = args[2];
		final String templatePath = args[3];
		final String idLookupPath = args[4];
		final FileReader templateReader = new FileReader(templatePath);
		final SubgraphTemplate template = gson.fromJson(templateReader,
				SubgraphTemplate.class);

		final FileReader idLookupReader = new FileReader(idLookupPath);
		final Type idLookupType = new TypeToken<Map<String, NodeIdLocation>>() {
		}.getType();
		final Map<String, NodeIdLocation> idLookup = gson.fromJson(
				idLookupReader,
				idLookupType);

		final ProvenanceGraphApi provDm = new ProvenanceClient(username,
				password,
				"localhost",
				Config.getServerPort());

		provDm.createProvenanceGraph(graph);

		final SubgraphInstance initialInstance = TestUtil
				.templateToTestInstance(template, emptyMap());
		int created = 0;
		provDm.storeSubgraph(graph, initialInstance);
		created++;
		System.out.println("created " + created + " instances");
		final SubgraphInstance[] previousInstance = { initialInstance };

		while (true) {
			final Map<String, Optional<NodeInstance>> instanceLookup = transformValues(
					idLookup,
					location -> Optional.of(previousInstance[0]
							.getRanks()
							.get(location.rankIndex).nodes
									.get(location.nodeIndex)));
			final SubgraphInstance nextInstance = TestUtil
					.templateToTestInstance(template, instanceLookup);
			provDm.storeSubgraph(graph, nextInstance);
			created++;
			System.out.println("created " + created + " instances");
			previousInstance[0] = nextInstance;
		}

	}

}
