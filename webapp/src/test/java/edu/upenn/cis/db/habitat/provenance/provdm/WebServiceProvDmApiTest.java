/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.provenance.provdm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mashape.unirest.http.exceptions.UnirestException;

import edu.upenn.cis.db.habitat.Config;
import edu.upenn.cis.db.habitat.TestsRequiringSpark;
import edu.upenn.cis.db.habitat.client.ProvDmClient;
import edu.upenn.cis.db.habitat.client.ProvenanceClient;
import edu.upenn.cis.db.habitat.client.UserClient;
import edu.upenn.cis.db.habitat.core.type.Field;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvDmConstants;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvDmType;
import edu.upenn.cis.db.habitat.core.type.provdm.ProvQualifiedNameToken;
import edu.upenn.cis.db.habitat.core.type.provdm.QualifiedName;
import edu.upenn.cis.db.habitat.core.webservice.HabitatSecurityException;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.test.TestUtil;

public class WebServiceProvDmApiTest extends AbstractProvDmApiTest {

	private static final String username = "provDmWsTestUser";
	private static final String password = "provDmWsTestUserPassword";

	private static final String username2 = "provDmWsTestUser2";
	private static final String password2 = "provDmWsTestUserPassword2";

	@BeforeClass
	public static void requireSuite() throws Exception {

		Assume.assumeTrue(
				"These tests require the Spark server and must be run as part of the "
						+ TestsRequiringSpark.class
						+ " test suite",
				TestsRequiringSpark.serverIsRunning);

	}

	@Before
	public void createTables() throws HabitatServiceException, UnirestException {
		TestsRequiringSpark.cleanDatabase();
		TestsRequiringSpark.createTables();
		final Map<String, Object> valueMap1 = new HashMap<>();
		valueMap1.put("email", "provDmWsTestUser@mail");
		UserClient.createNewUser(
				username,
				password,
				valueMap1);

		final Map<String, Object> valueMap2 = new HashMap<>();
		valueMap2.put("email", "provDmWsTestUser2@mail");
		UserClient.createNewUser(
				username2,
				password2,
				valueMap2);

		store = new ProvDmClient(
				username,
				password, "localhost",
				Config.getServerPort());
		super.neo4j = TestsRequiringSpark.neo4j;
	}

	@Test
	public void authzTest() throws IOException,
			HabitatServiceException {

		final String resource = "provDmAuthzTest";
		final QualifiedName provId = TestUtil.newQualifiedName();

		store.createProvenanceGraph(resource);
		store.entity(resource, provId, TestUtil.newTestAttributeList(),
				TestUtil.newTestLocationModel());

		final ProvenanceClient client2 = new ProvenanceClient(username2,
				password2, "localhost", Config.getServerPort());
		final ProvToken token = new ProvQualifiedNameToken(provId);
		boolean exception = false;
		try {
			client2
					.getProvenanceData(resource, token);
		} catch (HabitatSecurityException e) {
			exception = true;
		}
		assertTrue(exception);
	}

	@Test
	public void nullLocationTest() throws HabitatServiceException {
		final String resource = "nullLocationTest";
		final QualifiedName provId = TestUtil.newQualifiedName();

		store.createProvenanceGraph(resource);
		store.entity(resource, provId, null);

		final ProvenanceClient client2 = new ProvenanceClient(username,
				password,
				"localhost",
				Config.getServerPort());
		final ProvToken token = new ProvQualifiedNameToken(provId);

		TupleWithSchema<String> provenanceData = client2
				.getProvenanceData(resource, token);
		assertEquals(1, provenanceData.size());
		final Field<? extends Object> typeField = provenanceData
				.get(ProvDmConstants.PROV_DM_TYPE_QN.toString());
		assertEquals(ProvDmType.ENTITY.name(), typeField.getValue());

		final ProvSpecifier actualSpecifier = client2.getProvenanceLocation(
				resource, token);
		assertTrue(actualSpecifier instanceof ProvLocation);
		final ProvLocation actualLocation = (ProvLocation) actualSpecifier;
		assertNull(actualLocation.getStream());
		assertTrue(actualLocation.getField().isEmpty());
		assertTrue(actualLocation.getPosition().isEmpty());

		final Map<String, StructuredData<String, Object>> nodes = client2
				.getProvenanceNodes(resource);
		assertEquals(1, nodes.size());
		final StructuredData<String, Object> nodeData = nodes.get(provId
				.toString());
		assertNotNull(nodeData);

	}

}
