/************************************************
 * Copyright 2017 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.auth;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mashape.unirest.http.exceptions.UnirestException;

import edu.upenn.cis.db.habitat.TestsRequiringSpark;
import edu.upenn.cis.db.habitat.client.UserClient;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

public class TestServices {

	@BeforeClass
	public static void requireSuite() throws Exception {
		Assume.assumeTrue(
				"These tests require the Spark server and must be run as part of the "
						+ TestsRequiringSpark.class
						+ " test suite",
				TestsRequiringSpark.serverIsRunning);

	}

	@Before
	public void createTables() throws HabitatServiceException, UnirestException {
		TestsRequiringSpark.cleanDatabase();
		TestsRequiringSpark.createTables();
		Map<String, Object> valueMap = new HashMap<>();
		valueMap.put("password", "mypassword");
		valueMap.put("email", "john@smith");
		Map<String,Object> json = UserClient.createNewUser("John Smith",
				(String) valueMap.get("password"),
				valueMap);

		System.out.println(json);

		assertTrue(TestsRequiringSpark.getUserApiLocal().isUserValid(
				"John Smith", "local"));
		assertTrue(TestsRequiringSpark.getUserApiLocal().isUserCredentialValid(
				"John Smith", "mypassword"));

	}

	@Test
	public void testAuthRestClient() throws UnirestException,
			HabitatServiceException {
		// fail("Not yet implemented");

		UserClient client = new UserClient("John Smith", "mypassword");

		Map<String, Object> results = client.getUserInfo("John Smith");

		assertTrue(results.containsKey("primaryemail"));

		assertTrue(client.isUserValid("John Smith", "local"));

		client.addCredential("John Smith", "fake", "http", "qwerty");
		assertTrue(client.isUserValid("John Smith", "fake"));
		assertFalse(client.isUserValid("John Smith", "fake99"));

		assertTrue(TestsRequiringSpark.getUserApiLocal()
				.isUserCredentialValid("John Smith", "mypassword"));
		assertFalse(client.isUserCredentialValid("John Smith", "qbert"));

	}

	// TO TEST:
	// addGroup
	// updateUser
	// updateCredential (is this fully exposed?)

	// TO ADD:
	// remove credential?
	// adduserToGroup
	// addSubgroupToGroup
	// getGroupId

}
