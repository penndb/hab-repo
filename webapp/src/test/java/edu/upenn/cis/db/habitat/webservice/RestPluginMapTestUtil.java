/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice;

import static com.google.common.collect.Iterables.concat;
import static org.junit.Assert.assertEquals;

import java.util.Map;
import java.util.Set;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import spark.Service;

import com.google.common.collect.ImmutableSet;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Module;
import com.google.inject.TypeLiteral;
import com.google.inject.name.Names;
import com.google.inject.util.Modules;

import edu.upenn.cis.db.habitat.auth.AuthServiceModule;
import edu.upenn.cis.db.habitat.auth.UserApiLocal;
import edu.upenn.cis.db.habitat.auth.backends.plugins.HabitatRequestScopeModule;
import edu.upenn.cis.db.habitat.auth.backends.plugins.ProtectedStorageModule;
import edu.upenn.cis.db.habitat.core.api.PermissionApi;
import edu.upenn.cis.db.habitat.core.api.ProvenanceGraphApi;
import edu.upenn.cis.db.habitat.core.api.StorageApi;
import edu.upenn.cis.db.habitat.core.api.UserApi;
import edu.upenn.cis.db.habitat.core.webservice.RestPlugin;
import edu.upenn.cis.db.habitat.permissions.PermissionServiceModule;
import edu.upenn.cis.db.habitat.provenance.ProvServiceModule;
import edu.upenn.cis.db.habitat.provenance.provdm.ProvDmServiceModule;
import edu.upenn.cis.db.habitat.repository.storage.GraphStore;
import edu.upenn.cis.db.habitat.repository.storage.StorageApiWithMetadataLocal;
import edu.upenn.cis.db.habitat.repository.storage.StorageServiceModule;
import edu.upenn.cis.db.habitat.repository.storage.neo4j.Neo4JStore;
import edu.upenn.cis.db.habitat.secure.ProtectedGraphStore;

/**
 * 
 * @author John Frommeyer
 *
 */
public class RestPluginMapTestUtil {

	@Mock
	private StorageApiWithMetadataLocal<String, Object, Object> mockStorageApi;

	@Mock
	private Neo4JStore mockNeo4JStore;

	@Mock
	UserApiLocal mockUserApiLocal;

	@Mock
	private PermissionApi mockPermissionApi;

	@Mock
	ProvenanceGraphApi mockProvenanceGraphApi;
	
	@Mock
	private ProtectedGraphStore<String> mockProtectedGraphStore;

	Map<String, RestPlugin> pluginMap;

	Injector injector;

	public RestPluginMapTestUtil() {
		MockitoAnnotations.initMocks(this);
		pluginMap = initPluginMap();
	}

	private Map<String, RestPlugin> initPluginMap() {
		final Set<Module> habitatRequestModules = ImmutableSet.of(
				new ProtectedStorageModule(mockNeo4JStore.getClass()),
				new HabitatRequestScopeModule());
		final Set<Module> restPluginModules = ImmutableSet.of(
				new AuthServiceModule(),
				new StorageServiceModule(),
				new ProvServiceModule(),
				new PermissionServiceModule(),
				new ProvDmServiceModule());
		final Module testModule = Modules.override(
				concat(habitatRequestModules,
						restPluginModules
				)
				)
				.with(new AbstractModule() {

					@Override
					protected void configure() {
						bind(UserApiLocal.class).toInstance(
								mockUserApiLocal);

						bind(new TypeLiteral<StorageApi<String, Object, ?>>() {
						}).toInstance(mockStorageApi);
						
						bind(new TypeLiteral<StorageApiWithMetadataLocal<String, Object, ?>>() {
						}).toInstance(mockProtectedGraphStore);

						bind(new TypeLiteral<GraphStore<String>>() {
						})
								.annotatedWith(Names.named(ProtectedGraphStore.UNPROTECTED_GRAPH_STORE))
								.toInstance(mockNeo4JStore);

						bind(PermissionApi.class).toInstance(
								mockPermissionApi);

						bind(UserApi.class).toInstance(mockUserApiLocal);

						bind(ProvenanceGraphApi.class).toInstance(
								mockProvenanceGraphApi);
						bind(Service.class).toInstance(Service.ignite());
					}
				});
		injector = Guice
				.createInjector(testModule);

		final Map<String, RestPlugin> pluginMap = injector
				.getInstance(new Key<Map<String, RestPlugin>>() {
				});
		assertEquals(restPluginModules.size(), pluginMap.size());
		return pluginMap;
	}
}
