/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

/**
 * @author John Frommeyer
 *
 */
public class ProvenanceRouteConstants {

	public static final String SECURITY_DEF_KEY = "jwt";

	public static final String PLUGIN_NAME = "provenance";

	public static final String API_NAME = PLUGIN_NAME;

	public static final String PLUGIN_PATH = "/" + PLUGIN_NAME;

	public static final String NODES_RESOURCE = "nodes";

	public static final String NODES_PATH = "/" + NODES_RESOURCE;

	public static final String PROV_TOKEN_PATH_PARAM = "token";

	public static final String LABEL_QUERY_PARAM = "label";

	public static final String RESOURCE_QUERY_PARAM = "resource";

	public static final String RESOURCE_PATH_PARAM = "resource";

	public static final String RESOURCE_PATH_PARAM_2 = "resource2";

	public static final String PRUNE_PATH_PARAM = "prune";

	public static final String LABEL_PATH_PARAM = "label";

	public static final String LINKS_RESOURCE = "links";

	public static final String LINKS_PATH = "/" + LINKS_RESOURCE;

	public static final String GRAPHS_RESOURCE = "graphs";

	public static final String GRAPHS_PATH = "/" + GRAPHS_RESOURCE;

	public static final String RESET_GRAPHS = "/reset";
	
	public static final String SUBGRAPHS_RESOURCE = "subgraphs";

	public static final String SUBGRAPHS_PATH = "/" + SUBGRAPHS_RESOURCE;
	
	public static final String SUBGRAPHS_LIMIT_QUERY_PARAM = "limit";
	
	public static final String SUBGRAPHS_SINCE_QUERY_PARAM = "since";

	public static String swaggerToSpark(String swaggerPath) {
		return swaggerPath.replace("{", ":")
				.replace("}", "");
	}

}
