/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.API_NAME;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.GRAPHS_PATH;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.PLUGIN_PATH;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.RESOURCE_PATH_PARAM;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.SECURITY_DEF_KEY;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.SUBGRAPHS_PATH;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

import com.google.common.annotations.VisibleForTesting;

import edu.upenn.cis.db.habitat.core.type.model.SubgraphTemplate;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;
import edu.upenn.cis.db.habitat.webservice.ProvWsMethod;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

/**
 * @author John Frommeyer
 *
 */
@Api(tags = API_NAME)
@Path(PLUGIN_PATH)
public abstract class StoreSubgraphTemplateWsMethod extends ProvWsMethod {

	private static final String operationId = "storeSubgraphTemplate";
	public static final String swaggerPath = GRAPHS_PATH
			+ "/{"
			+ RESOURCE_PATH_PARAM
			+ "}"
			+ SUBGRAPHS_PATH
			+ "/template";
	@VisibleForTesting
	public static final String REQUEST_TYPE = HttpMethod.POST;
	@VisibleForTesting
	public static final String SPARK_SUBPATH = ProvenanceRouteConstants
			.swaggerToSpark(swaggerPath);

	public StoreSubgraphTemplateWsMethod() {
		super(SPARK_SUBPATH, REQUEST_TYPE, true);
	}

	@Path(swaggerPath)
	@ApiOperation(value = "Store a subgraph template for a provenance graph",
			nickname = operationId,
			authorizations = @Authorization(SECURITY_DEF_KEY),
			httpMethod = REQUEST_TYPE,
			produces = MediaType.APPLICATION_JSON)
	@ApiResponses({
			@ApiResponse(code = HttpServletResponse.SC_OK,
					message = "subgraph template stored"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND,
					message = "Node not found",
					response = ResponseError.class),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST,
					message = "Bad request",
					response = ResponseError.class),
			@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED,
					message = "Unauthorized",
					response = ResponseError.class) })
	public abstract void storeSubgraphTemplate(
			@PathParam(RESOURCE_PATH_PARAM) String resource,
			@ApiParam(required = true) SubgraphTemplate template)
			throws Exception;
}
