/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes.provdm;

/**
 * @author John Frommeyer
 *
 */
public class ProvDmRouteConstants {

	public static final String API_NAME = "provDm";
	
	public static final String PLUGIN_NAME = "provdm";

	public static final String PLUGIN_PATH = "/" + PLUGIN_NAME;

	public static final String ENTITIES_RESOURCE = "entities";
	public static final String ENTITIES_PATH = "/" + ENTITIES_RESOURCE;

	public static final String ACTIVITIES_RESOURCE = "activities";
	public static final String ACTIVITIES_PATH = "/" + ACTIVITIES_RESOURCE;

	public static final String AGENTS_RESOURCE = "agents";
	public static final String AGENTS_PATH = "/" + AGENTS_RESOURCE;
}
