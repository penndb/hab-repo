/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice;

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.db.habitat.core.webservice.RouteSpecifier.RequestType;

/**
 * @author John Frommeyer
 *
 */
public abstract class ProvWsMethod implements WsMethodInfo {

	private final String sparkSubPath;
	private final RequestType method;
	private final boolean secure;

	/**
	 * 
	 * @param sparkSubpath
	 * @param method
	 * @param secure
	 */
	public ProvWsMethod(
			String sparkSubpath,
			String method,
			boolean secure) {
		this.method = RequestType.valueOf(checkNotNull(method));
		this.secure = secure;
		this.sparkSubPath = checkNotNull(sparkSubpath);
	}

	@Override
	public String getSparkSubpath() {
		return sparkSubPath;
	}

	@Override
	public RequestType getRequestType() {
		return method;
	}

	@Override
	public boolean isSecure() {
		return secure;
	}
}
