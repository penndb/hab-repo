/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.webservice.routes;

import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.API_NAME;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.GRAPHS_PATH;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.LINKS_PATH;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.PLUGIN_PATH;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.RESOURCE_PATH_PARAM;
import static edu.upenn.cis.db.habitat.webservice.routes.ProvenanceRouteConstants.SECURITY_DEF_KEY;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

import com.google.common.annotations.VisibleForTesting;

import edu.upenn.cis.db.habitat.core.type.model.StoreLinkModel;
import edu.upenn.cis.db.habitat.core.webservice.ResponseError;
import edu.upenn.cis.db.habitat.webservice.ProvWsMethod;

/**
 * @author John Frommeyer
 *
 */
@Api(tags = API_NAME)
@Path(PLUGIN_PATH)
public abstract class StoreProvenanceLinkWsMethod extends ProvWsMethod {

	private static final String OPERATION_ID = "storeProvenanceLink";
	private static final String SWAGGER_PATH = GRAPHS_PATH
			+ "/{"
			+ RESOURCE_PATH_PARAM
			+ "}"
			+ LINKS_PATH
			;
	@VisibleForTesting
	public static final String REQUEST_TYPE = HttpMethod.POST;
	@VisibleForTesting
	public static final String SPARK_SUBPATH = ProvenanceRouteConstants
			.swaggerToSpark(SWAGGER_PATH);

	public StoreProvenanceLinkWsMethod() {
		super(SPARK_SUBPATH, REQUEST_TYPE, true);
	}

	@Path(SWAGGER_PATH)
	@ApiOperation(value = "Store a provenance link between tokens",
			nickname = OPERATION_ID,
			authorizations = @Authorization(SECURITY_DEF_KEY),
			httpMethod = REQUEST_TYPE,
			consumes = MediaType.APPLICATION_JSON,
			code = HttpServletResponse.SC_NO_CONTENT)
	@ApiResponses({
			@ApiResponse(code = HttpServletResponse.SC_NO_CONTENT,
					message = "Link created"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST,
					message = "Bad request",
					response = ResponseError.class),
			@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED,
					message = "Unauthorized",
					response = ResponseError.class) })
	public abstract void storeProvenanceLink(
			@PathParam(RESOURCE_PATH_PARAM) String resource,
			@ApiParam(required = true) StoreLinkModel storeLinkModel)
			throws Exception;

}
