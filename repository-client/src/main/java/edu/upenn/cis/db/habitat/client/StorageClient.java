/************************************************
 * Copyright 2019 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.db.habitat.client;

import java.io.InputStream;
import java.net.URL;
import java.sql.SQLException;
import java.util.Map;
import java.util.function.Function;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.inject.Inject;

import edu.upenn.cis.db.habitat.core.api.PermissionApi;
import edu.upenn.cis.db.habitat.core.api.StorageApiWithMetadata;
import edu.upenn.cis.db.habitat.core.api.UserApi;
import edu.upenn.cis.db.habitat.core.type.StructuredData;
import edu.upenn.cis.db.habitat.core.type.TransactionSupplier;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

public class StorageClient<K,V,B> implements StorageApiWithMetadata<K,V,B> {
	final static Logger logger = LogManager.getLogger(StorageClient.class);

	UserApi users;
	PermissionApi permissions;
	
	@Inject
	StorageClient(UserApi userCredentials, PermissionApi permissions) {
		this.users = userCredentials;
		this.permissions = permissions;
	}

	@Override
	public void addMetadata(String resource, K key, StructuredData<K, ? extends V> metadata)
			throws HabitatServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addMetadata(TransactionSupplier supplier, String resource, K key,
			StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void connect() throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean execTransaction(String resource, Function<TransactionSupplier, Boolean> transactionFn)
			throws HabitatServiceException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getCount(String resource) throws HabitatServiceException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public B getData(String resource, K key) throws HabitatServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public B getData(TransactionSupplier supplier, String resource, K key) throws HabitatServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<K> getKeysMatching(String resource, String metadataField, Map<K, ? extends V> match)
			throws HabitatServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<K> getKeysMatching(TransactionSupplier supplier, String resource, String metadataField,
			Map<K, ? extends V> match) throws HabitatServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<Pair<K, StructuredData<K, V>>> getLinksFrom(String resource, K key1) throws HabitatServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<Pair<K, StructuredData<K, V>>> getLinksFrom(TransactionSupplier supplier, String resource, K key1)
			throws HabitatServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<Pair<K, StructuredData<K, V>>> getLinksTo(String resource, K key1) throws HabitatServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<Pair<K, StructuredData<K, V>>> getLinksTo(TransactionSupplier supplier, String resource, K key1)
			throws HabitatServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StructuredData<K, V> getMetadata(String resource, K key) throws HabitatServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StructuredData<K, V> getMetadata(TransactionSupplier supplier, String resource, K key)
			throws HabitatServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<Pair<K, StructuredData<K, V>>> getMetadataForResource(String resource) throws HabitatServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<Pair<K, StructuredData<K, V>>> getMetadataForResource(TransactionSupplier supplier, String resource)
			throws HabitatServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public edu.upenn.cis.db.habitat.core.api.StorageApi.StoreClass getStoreClass() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isSuitableForMetadata() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isSuitableToStore(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void link(String resource, K key1, String label, K key2, StructuredData<K, ? extends V> metadata)
			throws HabitatServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void link(TransactionSupplier supplier, String resource, K key1, String label, K key2,
			StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeKey(String resource, K key) throws HabitatServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeKey(TransactionSupplier supplier, String resource, K key) throws HabitatServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeMetadata(String resource, K key, String metadataField) throws HabitatServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeMetadata(TransactionSupplier supplier, String resource, K key, String metadataField)
			throws HabitatServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void replaceMetadata(String resource, K key, StructuredData<K, ? extends V> metadata)
			throws HabitatServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void replaceMetadata(TransactionSupplier supplier, String resource, K key,
			StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void store(String resource, K key, B value, StructuredData<K, ? extends V> metadata)
			throws HabitatServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void store(TransactionSupplier supplier, String resource, K key, B value,
			StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void storeReference(String resource, K key, String refToValue, StructuredData<K, ? extends V> metadata)
			throws HabitatServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void storeReference(TransactionSupplier supplier, String resource, K key, String refToValue,
			StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void storeUrlContents(String resource, K key, URL url, StructuredData<K, ? extends V> metadata)
			throws HabitatServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void storeUrlContents(TransactionSupplier supplier, String resource, K key, URL url,
			StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isDataLegal(StructuredData<K, ? extends V> metadata) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void storeFile(String resource, K key, InputStream file, int streamLength,
			StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void storeFile(TransactionSupplier supplier, String resource, K key, InputStream file, int streamLength,
			StructuredData<K, ? extends V> metadata) throws HabitatServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addMetadata(String resource, K key, StructuredData<K, ? extends V> metadata, String specialLabel)
			throws HabitatServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addMetadata(TransactionSupplier supplier, String resource, K key,
			StructuredData<K, ? extends V> metadata, String specialLabel) throws HabitatServiceException {
		// TODO Auto-generated method stub
		
	}

}
